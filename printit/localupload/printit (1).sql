-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 03:45 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `printit`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_size`
--

CREATE TABLE `banner_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_size`
--

INSERT INTO `banner_size` (`id`, `name`, `status`, `created_at`) VALUES
(1, '12*17', '0', '2021-01-14 07:22:15'),
(2, '10*100', '0', '2021-01-11 14:32:10'),
(3, '10*10', '0', '2021-01-14 07:19:14');

-- --------------------------------------------------------

--
-- Table structure for table `banner_width`
--

CREATE TABLE `banner_width` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_width`
--

INSERT INTO `banner_width` (`id`, `name`, `status`, `created_at`) VALUES
(1, '12*16', '0', '2021-01-11 14:32:10'),
(2, '10*100', '0', '2021-01-11 14:32:10');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `service_type` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=normal.2=express',
  `order_type` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=custom print,2=quickprint,3=translation,4=notes',
  `project_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer` int(11) NOT NULL,
  `vendor` int(11) DEFAULT NULL,
  `print_type` int(11) DEFAULT NULL,
  `paper_type` int(11) DEFAULT NULL,
  `paper_size` int(11) DEFAULT NULL,
  `image_uploaded` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_copies` int(11) DEFAULT NULL,
  `width` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `printing_side` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_delivery` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=pickup,2=delivery',
  `amount` float DEFAULT NULL,
  `payment_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=pending,1=done',
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=saved,1=complete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commission`
--

CREATE TABLE `commission` (
  `id` int(11) NOT NULL,
  `percent` float NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `commission`
--

INSERT INTO `commission` (`id`, `percent`, `description`, `status`, `created_at`) VALUES
(1, 15, 'ddddddddd', '2', '2020-12-17 03:44:54'),
(2, 20.25, 'abc defkkkk', '0', '2020-12-17 04:53:47'),
(3, 10, 'new test', '0', '2020-12-17 04:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amount` float NOT NULL DEFAULT '0',
  `type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=fixed,1=%',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `users` int(11) NOT NULL DEFAULT '1',
  `used_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `code`, `amount`, `type`, `start_date`, `end_date`, `status`, `created_at`, `users`, `used_by`) VALUES
(1, 'test', 100, '1', '2020-12-16', '2020-12-19', '2', '2020-12-16 08:20:49', 1, 0),
(2, 'abc', 20, '0', '2020-12-02', '2020-12-17', '2', '2020-12-16 08:25:54', 1, 0),
(3, 'two', 12, '0', '2020-12-01', '2020-12-06', '0', '2020-12-16 09:47:02', 1, 0),
(4, 'one', 52.7, '1', '2020-12-01', '2020-12-31', '0', '2020-12-16 09:42:21', 1, 0),
(5, 'newyear', 50, '1', '2021-01-01', '2021-02-16', '0', '2021-01-05 11:12:57', 2, 0),
(6, 'fg', 45, '1', '2021-01-01', '2021-01-26', '0', '2021-01-05 11:58:02', 30, 16);

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE `customer_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_address`
--

INSERT INTO `customer_address` (`id`, `user_id`, `label`, `address`, `latitude`, `longitude`, `status`, `created_at`) VALUES
(1, 13, 'Home', 'Palasia, Indore, Madhya Pradesh, India', '22.724355', '75.8838944', '0', '2021-01-13 16:54:23'),
(8, 13, 'office 2', 'shekhar central', '', '', '0', '2021-01-14 15:45:24'),
(10, 13, 'New Office address', 'my new address', '', '', '0', '2021-01-14 17:04:34'),
(11, 13, 'mysweethome', 'abc', '', '', '0', '2021-01-25 14:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `extra_copies`
--

CREATE TABLE `extra_copies` (
  `id` int(11) NOT NULL,
  `copy_total` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1','2','') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=active,1=inactive,2=deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `extra_copies`
--

INSERT INTO `extra_copies` (`id`, `copy_total`, `created_at`, `status`) VALUES
(1, '42', '2020-12-20 07:59:15', '2'),
(2, '78', '2020-12-20 07:22:29', '0');

-- --------------------------------------------------------

--
-- Table structure for table `incharge_person`
--

CREATE TABLE `incharge_person` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `printery_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=active,2=delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incharge_person`
--

INSERT INTO `incharge_person` (`id`, `user_id`, `printery_id`, `fname`, `lname`, `email`, `mobile`, `created_at`, `status`) VALUES
(1, 42, 53, 'madhurii', 'gandhii', 'madhurigandhi@gmail.com', '9998889898', '2021-01-21 10:38:36', '0'),
(2, 42, 54, 'satyam', 'test', 'satyam@gmail.com', '8888888888888', '2021-01-21 15:12:22', '0'),
(3, 42, 55, 'vibhor', 'testing', 'vibhor@gmail.com', '4555555555555', '2021-01-21 15:12:22', '0'),
(4, 43, 56, 'abc', 'def', 'abc@gmail.com', '9998889898', '2021-01-25 10:13:14', '0'),
(5, 43, 57, 'ab', 'ee', 'ee@gmail.com', '9998989898', '2021-01-25 15:00:51', '0'),
(6, 43, 58, 'ab', 'ee', 'ee@gmail.com', '9998989898', '2021-01-25 15:14:55', '0'),
(7, 43, 59, 'ab', 'ee', 'ee@gmail.com', '9998989898', '2021-01-25 15:17:16', '0'),
(8, 43, 60, 'ee', 'ere', 'ere@ad.com', '5656565656', '2021-01-25 15:17:16', '0'),
(9, 43, 61, 'ab', 'ee', 'ee@gmail.com', '9998989898', '2021-01-25 15:25:30', '0'),
(10, 43, 62, 'ee', 'ere', 'ere@ad.com', '5656565656', '2021-01-25 15:25:30', '0'),
(11, 43, 63, 'ab', 'ee', 'ee@gmail.com', '9998989898', '2021-01-25 15:34:51', '0'),
(12, 43, 64, 'ee', 'ere', 'ere@ad.com', '5656565656', '2021-01-25 15:34:51', '0'),
(13, 43, 65, 'abcee', 'eee', 'adwe@a.co', '9898980000', '2021-01-25 15:34:51', '0'),
(14, 27, 16, 'soniya', 'kukreja', 'soniya@gmail.com', '9998889898', '2021-01-27 03:41:20', '0'),
(15, 27, 66, 'sona', 'k', 'sona@gmail.com', '8989888855', '2021-01-27 15:47:37', '0'),
(16, 6, 3, 'satyam', 'abc', 'satyam@gmail.com', '9887989898', '2021-01-28 07:53:51', '0');

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `amount` double NOT NULL,
  `type` enum('Monthly','Yearly','Quarterly','Half Yearly') COLLATE utf8_unicode_ci NOT NULL,
  `features` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `membership`
--

INSERT INTO `membership` (`id`, `title`, `description`, `amount`, `type`, `features`, `status`, `created_at`, `updated_at`) VALUES
(1, '0', 'asdfsadf', 1000, 'Half Yearly', '', '2', '2020-12-16 10:55:01', '0000-00-00 00:00:00'),
(2, 'test', 'asdf', 45, 'Quarterly', '', '2', '2020-12-16 11:01:37', '0000-00-00 00:00:00'),
(3, 'test two', 'asdf', 898956.2, 'Half Yearly', '', '0', '2020-12-17 01:44:17', '0000-00-00 00:00:00'),
(4, 'tes', 'ddddd', 78, 'Monthly', '', '0', '2020-12-16 11:16:07', '0000-00-00 00:00:00'),
(5, 'hg', 'jhgjg', 56, 'Half Yearly', '', '0', '2020-12-16 11:23:02', '0000-00-00 00:00:00'),
(6, 'hgjj', 'jhgjg', 56, 'Yearly', '', '0', '2020-12-16 11:23:51', '0000-00-00 00:00:00'),
(7, 'hgjjol', 'jhgjg', 56, 'Monthly', '', '0', '2020-12-16 11:24:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0=active,1=inactive',
  `is_delete` enum('0','1') NOT NULL COMMENT '0=active,1=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `service_type` enum('1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '1=normal.2=express',
  `order_type` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL COMMENT '1=custom print,2=quickprint,3=translation,4=notes',
  `project_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer` int(11) NOT NULL,
  `vendor` int(11) DEFAULT NULL,
  `print_type` int(11) DEFAULT NULL,
  `paper_type` int(11) DEFAULT NULL,
  `paper_size` int(11) DEFAULT NULL,
  `image_uploaded` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_original_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_rotation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_copies` int(11) DEFAULT NULL,
  `width` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `printing_side` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_delivery` enum('1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '1=pickup,2=delivery',
  `payment_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=pending,1=done',
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=saved,1=complete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `coupon_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coupon_amt` float DEFAULT NULL,
  `coupon_start` date DEFAULT NULL,
  `coupon_end` date DEFAULT NULL,
  `delivery_address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `progress_status` enum('being_printed','ready_for_pickup','delivered','waiting_for_proposal','proposal_received') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0=being',
  `amount` float NOT NULL DEFAULT '0',
  `applied_coupon_amt` float NOT NULL DEFAULT '0',
  `delivery_charge` float NOT NULL DEFAULT '0',
  `product_total` float NOT NULL DEFAULT '0',
  `order_total` float NOT NULL DEFAULT '0',
  `all_steps` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `step_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `service_type`, `order_type`, `project_name`, `customer`, `vendor`, `print_type`, `paper_type`, `paper_size`, `image_uploaded`, `img_original_name`, `image_rotation`, `no_of_copies`, `width`, `printing_side`, `pickup_delivery`, `payment_status`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `coupon_code`, `coupon_amt`, `coupon_start`, `coupon_end`, `delivery_address`, `progress_status`, `amount`, `applied_coupon_amt`, `delivery_charge`, `product_total`, `order_total`, `all_steps`, `step_url`) VALUES
(8, '1', '1', 'one', 13, 3, 1, 4, 5, '1611839302download.png', 'download.png', '', 2, NULL, NULL, '1', '0', '0', '2021-01-28 02:08:30', 0, '2021-01-28 17:06:19', 0, NULL, NULL, NULL, NULL, NULL, NULL, 15, 0, 0, 0, 0, 'a:1:{i:0;s:43:\"http://localhost/print_ci/print-type/poster\";}', 'http://localhost/print_ci/printry_shops');

-- --------------------------------------------------------

--
-- Table structure for table `order_address`
--

CREATE TABLE `order_address` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `c_add_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `block` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `avenue` varchar(255) DEFAULT NULL,
  `building` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `apartment` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `extra_instructions` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_address`
--

INSERT INTO `order_address` (`id`, `order_id`, `c_add_id`, `address`, `area`, `block`, `street`, `avenue`, `building`, `floor`, `apartment`, `mobile`, `extra_instructions`, `created_at`) VALUES
(1, 2, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 12:33:31'),
(2, 2, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 12:43:36'),
(3, 2, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 12:49:50'),
(4, 2, 10, 'my new address', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 13:04:34'),
(5, 2, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 13:10:59'),
(6, 2, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 14:24:08'),
(7, 3, 1, 'Palasia, Indore, Madhya Pradesh, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-16 14:52:17'),
(8, 17, 8, 'shekhar central', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-18 15:47:31'),
(9, 1, 1, 'abc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-25 14:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `prod_id` int(11) DEFAULT NULL,
  `prod_price` float DEFAULT NULL,
  `prod_qty` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=active,1=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `prod_id`, `prod_price`, `prod_qty`, `status`, `created_at`) VALUES
(1, 2, 4, 25, 3, '1', '2021-01-16 07:58:36'),
(2, 2, 3, 50, 1, '1', '2021-01-16 07:58:36'),
(3, 2, 4, 25, 3, '1', '2021-01-16 08:01:33'),
(4, 2, 3, 50, 1, '1', '2021-01-16 08:01:33'),
(5, 2, 4, 25, 3, '1', '2021-01-16 08:03:25'),
(6, 2, 3, 50, 1, '1', '2021-01-16 08:03:25'),
(7, 2, 4, 25, 3, '1', '2021-01-16 08:03:50'),
(8, 2, 3, 50, 1, '1', '2021-01-16 08:03:50'),
(9, 2, 4, 25, 2, '1', '2021-01-16 08:09:23'),
(10, 2, 3, 50, 1, '1', '2021-01-16 08:09:23'),
(11, 2, 4, 25, 2, '1', '2021-01-16 08:10:37'),
(12, 2, 3, 50, 1, '1', '2021-01-16 08:10:37'),
(13, 2, 4, 25, 2, '1', '2021-01-16 08:13:33'),
(14, 2, 3, 50, 1, '1', '2021-01-16 08:13:33'),
(15, 2, 4, 25, 2, '1', '2021-01-16 08:13:47'),
(16, 2, 3, 50, 1, '1', '2021-01-16 08:13:47'),
(17, 2, 4, 25, 2, '1', '2021-01-16 08:19:42'),
(18, 2, 3, 50, 1, '1', '2021-01-16 08:19:42'),
(19, 2, 4, 25, 2, '1', '2021-01-16 08:22:30'),
(20, 2, 3, 50, 1, '1', '2021-01-16 08:22:30'),
(21, 2, 4, 25, 2, '1', '2021-01-16 08:32:05'),
(22, 2, 3, 50, 1, '1', '2021-01-16 08:32:05'),
(23, 2, 4, 25, 2, '1', '2021-01-16 08:40:53'),
(24, 2, 3, 50, 1, '1', '2021-01-16 08:40:53'),
(25, 2, 4, 25, 2, '0', '2021-01-16 09:54:00'),
(26, 2, 3, 50, 1, '0', '2021-01-16 09:54:00'),
(27, 3, 4, 25, 3, '0', '2021-01-16 10:19:48'),
(28, 3, 3, 50, 1, '0', '2021-01-16 10:19:48'),
(29, 17, 4, 25, 3, '0', '2021-01-18 11:15:56'),
(30, 17, 3, 50, 1, '0', '2021-01-18 11:15:56'),
(31, 1, 4, 25, 4, '0', '2021-01-25 10:06:15'),
(32, 8, 1, 100, 3, '1', '2021-01-28 01:42:58'),
(33, 8, 1, 100, 3, '1', '2021-01-28 01:43:33'),
(34, 8, 1, 100, 3, '0', '2021-01-28 01:47:51');

-- --------------------------------------------------------

--
-- Table structure for table `paper_size`
--

CREATE TABLE `paper_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=in-active,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `paper_size`
--

INSERT INTO `paper_size` (`id`, `name`, `status`, `created_at`) VALUES
(4, '4*4', '0', '2020-12-20 11:27:06'),
(5, '6*6', '0', '2020-12-20 11:27:17'),
(6, 'A0', '0', '2021-01-27 11:22:58'),
(7, 'A1', '0', '2021-01-27 11:23:07'),
(8, 'A2', '0', '2021-01-27 11:23:15'),
(9, 'A4', '0', '2021-01-27 02:35:54'),
(10, 'A5', '0', '2021-01-27 02:36:04'),
(11, '2*85', '0', '2021-01-27 02:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `paper_type`
--

CREATE TABLE `paper_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `paper_type`
--

INSERT INTO `paper_type` (`id`, `name`, `description`, `status`, `created_at`) VALUES
(4, 'Glossy', 'glossy page description', '0', '2020-12-22 05:55:25'),
(5, 'Film', 'film page description test', '0', '2020-12-20 11:25:59'),
(6, 'Quality Paper', 'Quality paper short description', '0', '2021-01-19 08:37:06'),
(7, 'Indoor', 'Indoor paper type description in short is here', '0', '2021-01-19 08:22:44'),
(8, 'Outdoor', 'abc test', '0', '2021-01-27 02:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` date NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=expire'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`id`, `user_email`, `token`, `expiry`, `status`) VALUES
(1, 'soniya@gmail.com', '0b49eedc493a6af7f569d5c44a5cef6f', '2020-12-24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `price_combination`
--

CREATE TABLE `price_combination` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL COMMENT '1=custom print,2=quickprint,3=translation,4=notes',
  `print_type` int(11) NOT NULL,
  `paper_type_id` int(11) NOT NULL,
  `paper_size_id` int(11) NOT NULL,
  `printing_sides` enum('single','double') COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_copies` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` enum('colored','black_&_white') COLLATE utf8_unicode_ci DEFAULT NULL,
  `binding` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=active,1=inactive,2=delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `price_combination`
--

INSERT INTO `price_combination` (`id`, `service_id`, `print_type`, `paper_type_id`, `paper_size_id`, `printing_sides`, `extra_copies`, `banner_size`, `color`, `binding`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 4, 4, '', NULL, '', 'colored', '', '2021-01-25 01:21:56', '2021-01-25 17:51:56', '0'),
(2, 1, 2, 4, 4, 'single', NULL, '', 'colored', '', '2021-01-25 01:29:14', '2021-01-25 17:59:14', '2'),
(3, 1, 2, 4, 4, 'single', '200', '', 'colored', '', '2021-01-25 01:57:06', '2021-01-25 18:27:06', '0'),
(4, 1, 2, 4, 4, 'single', '300', '', 'colored', '', '2021-01-25 01:57:50', '2021-01-25 18:27:50', '0'),
(5, 1, 2, 4, 4, 'double', '100', '', 'colored', '', '2021-01-25 02:00:59', '2021-01-25 18:30:59', '0'),
(6, 1, 2, 4, 4, 'single', '100', '', '', '', '2021-01-25 02:01:21', '2021-01-25 18:31:21', '0'),
(7, 1, 3, 4, 4, '', '', '', '', '', '2021-01-25 02:04:38', '2021-01-25 18:34:38', '0'),
(8, 1, 3, 4, 5, '', '', '', '', '', '2021-01-25 02:05:20', '2021-01-25 18:35:20', '0'),
(9, 1, 4, 4, 0, '', '', '10*10', '', '', '2021-01-25 02:06:05', '2021-01-25 18:36:05', '2'),
(10, 1, 4, 4, 0, '', '', '10*20', '', '', '2021-01-25 02:10:55', '2021-01-25 18:40:55', '2'),
(11, 1, 4, 4, 0, '', '', '10*25', '', '', '2021-01-25 02:11:05', '2021-01-25 18:41:05', '0'),
(12, 1, 2, 5, 4, 'single', '100', '', 'colored', '', '2021-01-25 02:50:22', '2021-01-25 19:20:22', '0'),
(13, 1, 2, 5, 4, 'single', '200', '', 'colored', '', '2021-01-25 02:50:32', '2021-01-25 19:20:32', '0'),
(14, 1, 1, 4, 6, '', '', '', 'colored', '', '2021-01-27 11:24:05', '2021-01-27 11:24:05', '0'),
(15, 1, 1, 4, 7, '', '', '', 'colored', '', '2021-01-27 11:33:47', '2021-01-27 11:33:47', '0'),
(16, 1, 1, 4, 8, '', '', '', 'colored', '', '2021-01-27 11:34:35', '2021-01-27 11:34:35', '0'),
(17, 1, 2, 4, 9, 'single', '100', '', 'colored', '', '2021-01-27 02:36:52', '2021-01-27 14:36:52', '0'),
(18, 1, 2, 4, 9, 'double', '100', '', 'colored', '', '2021-01-27 02:37:26', '2021-01-27 14:37:26', '0'),
(19, 1, 3, 7, 11, '', '', '', '', '', '2021-01-27 02:49:00', '2021-01-27 14:49:00', '0'),
(20, 1, 3, 8, 11, '', '', '', '', '', '2021-01-27 02:50:06', '2021-01-27 14:50:06', '0'),
(21, 1, 4, 7, 0, '', '', '1', '', '', '2021-01-27 02:59:13', '2021-01-27 14:59:13', '0'),
(22, 1, 4, 8, 0, '', '', '1', '', '', '2021-01-27 02:59:39', '2021-01-27 14:59:39', '0');

-- --------------------------------------------------------

--
-- Table structure for table `price_range`
--

CREATE TABLE `price_range` (
  `id` int(11) NOT NULL,
  `print_management_id` int(11) NOT NULL,
  `copy_from` int(11) NOT NULL,
  `copy_to` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=active,1=deleted,',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_range`
--

INSERT INTO `price_range` (`id`, `print_management_id`, `copy_from`, `copy_to`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 50, 15, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37'),
(2, 1, 11, 100, 10, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37'),
(3, 1, 101, 200, 5, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37'),
(4, 4, 10, 50, 15, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37'),
(5, 4, 11, 100, 10, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37'),
(6, 4, 101, 200, 5, '0', '2021-01-11 18:24:37', '2021-01-11 18:24:37');

-- --------------------------------------------------------

--
-- Table structure for table `printery_shop`
--

CREATE TABLE `printery_shop` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=main,1=branch',
  `contact` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shop_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `po_box` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `block` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `building` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `flat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `registeration_date` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `services` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_workers` int(11) NOT NULL DEFAULT '0',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_images` text COLLATE utf8_unicode_ci,
  `price_status` enum('pending','approved','rejected') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=pending,1=approved,2=reject',
  `delete_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `printery_shop`
--

INSERT INTO `printery_shop` (`id`, `user_id`, `branch`, `contact`, `shop_name`, `po_box`, `block`, `building`, `flat`, `latitude`, `longitude`, `start_time`, `end_time`, `registeration_date`, `created_by`, `services`, `updated_by`, `updated_at`, `total_workers`, `logo`, `company_images`, `price_status`, `status`, `delete_status`) VALUES
(1, 4, '0', '9998889898', 'testname', 'abcbox', 'main road', 'abcdd', 'test flat', NULL, NULL, '09:00:00', '11:00:00', '2020-12-13 10:58:00', 0, '2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(2, 5, '0', '8787878781', 'indore book depo', 'ok8787', 'usha nagar', 'abcdd', 'annpurna', NULL, NULL, '12:00:00', '11:00:00', '2020-12-01 11:10:35', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(3, 6, '0', NULL, 'Anuroop Graphics & Printers', '217-B, Bansi Trade Center,', 'Mahatma Gandhi Rd', 'abcdd', 'palasia', NULL, NULL, '12:10:00', '11:00:00', '2020-12-15 11:56:22', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 4, NULL, NULL, 'approved', '1', '0'),
(4, 7, '0', '9898989000', 'Krishna Graphics & Printers', '217-A, Bansi Trade Center,', 'Mahatma Gandhi Rd', 'abcdd', 'palasia', NULL, NULL, '12:00:00', '11:00:00', '2020-12-15 11:56:22', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'approved', '1', '0'),
(5, 28, '0', '9998889898', 'vendorone', 'abcbox', 'main road', 'abcdd', 'test flat', NULL, NULL, '09:00:00', '11:00:00', '2020-12-13 10:58:00', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(6, 8, '0', '8787878782', 'two', 'ok8787', 'usha nagar', 'abcdd', 'annpurna', NULL, NULL, '12:00:00', '11:00:00', '2020-12-01 11:10:35', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(7, 9, '0', '9898989898', 'three', '217-B, Bansi Trade Center,', 'Mahatma Gandhi Rd', 'abcdd', 'palasia', NULL, NULL, '12:00:00', '11:00:00', '2020-12-15 11:56:22', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(8, 10, '0', '9898989000', 'vendor four', '217-A, Bansi Trade Center,', 'Mahatma Gandhi Rd', 'abcdd', 'palasia', NULL, NULL, '12:00:00', '11:00:00', '2020-12-15 11:56:22', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(9, 21, '0', '98798798798', 'abc', 'testvendor-pobox', 'madhumilan', 'Saudi Arabia', '', '23.885942', '45.079162', '10:00:00', '22:00:00', '2020-12-28 01:27:27', 0, '1,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '2', '0'),
(10, 22, '0', '98798798798', 'abc', '', '', 'abcdd', '', NULL, NULL, '00:00:00', NULL, '2020-12-28 01:29:18', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(11, 23, '0', '9999999999', 'abcdef', '', '', 'abcdd', '', NULL, NULL, '00:00:00', NULL, '2020-12-28 01:30:35', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(12, 26, '0', '9898989999', 'omkar stationary', '', '', 'Tilak Nagar, Patrakar Colony, Indore, Madhya Pradesh', '', NULL, NULL, '00:00:00', NULL, '2020-12-29 10:19:58', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '2', '0'),
(14, 4, '0', '9998889898', 'Branch one', 'abcbox', 'main road', 'abcdd', 'test flat', NULL, NULL, '09:00:00', '11:00:00', '2020-12-13 10:58:00', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(15, 4, '0', '9998889898', 'Branch two', 'palasia', 'main road', 'abcdd', 'test flat', NULL, NULL, '09:00:00', '11:00:00', '2020-12-13 10:58:00', 0, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '0', '0'),
(16, 27, '0', NULL, 'omkar stationaryy', 'PO-123123', '', 'Rasoma Square, Scheme 54 PU4, Indore, Madhya Pradesh', '', '22.7488902', '75.8947843', '17:50:00', '21:50:00', '2021-01-06 10:22:21', 27, '1,2', 27, '2021-01-11 09:48:17', 4, '1611060926Penguins.jpg', '1611060926Hydrangeas.jpg,1611060926Tulips.jpg', 'approved', '1', '0'),
(17, 27, '1', '9809898989', 'kanha', '', '', 'Annapurna Road, Prabhu Nagar, Indore, Madhya Pradesh, India', '', '22.6766721', '75.8337998', '00:00:00', NULL, '2021-01-06 10:23:56', 27, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'approved', '1', '1'),
(18, 31, '0', '', 'test shop', '', '', 'Annapurna Road, Prabhu Nagar, Indore, Madhya Pradesh, India', '', '22.6766721', '75.8337998', NULL, NULL, '2021-01-09 08:23:11', NULL, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '2', '0'),
(19, 32, '0', '9998889898', 'sh', 'v-pobox', 'v block', 'MR 9 Road, Anil Nagar, Indore, Madhya Pradesh, India', '', '22.74143', '75.90102519999999', '00:00:00', '22:00:00', '2021-01-09 08:24:16', NULL, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '2', '0'),
(20, 32, '1', '4444555556', 'sh2', 'abc', 'def', 'Regal Square, Indore, Madhya Pradesh, India', '', '22.7201357', '75.8713688', '14:53:00', '22:00:00', '2021-01-09 10:23:21', 32, '1,2,3,4', NULL, '2021-01-11 14:08:32', 0, NULL, NULL, 'pending', '2', '0'),
(22, 35, '0', '', 'suresh stationary', 'po-10', '', 'Bengali Square, Indore, Madhya Pradesh, India', '', '22.7081414', '75.9229596', '10:00:00', '23:00:00', '2021-01-15 09:57:23', NULL, '1,2,3,4', NULL, '2021-01-15 14:27:23', 5, NULL, NULL, 'pending', '2', '0'),
(23, 36, '0', '', 'shiva_company', '', '', 'Tilak Nagar, New Delhi, Delhi, India', '', '28.6389315', '77.08668109999999', NULL, NULL, '2021-01-19 11:03:11', NULL, NULL, NULL, '2021-01-19 15:33:11', 0, NULL, NULL, 'pending', '2', '0'),
(24, 37, '0', '', 'shambhoo stationary', 'PO-123', '', 'Rasoma Square, Scheme 54 PU4, Indore, Madhya Pradesh', '', '22.7488902', '75.8947843', '10:00:00', '22:00:00', '2021-01-19 11:06:21', NULL, '1,2', NULL, '2021-01-19 15:36:21', 5, NULL, NULL, 'pending', '2', '1'),
(33, 35, '1', '9999999991', 'branch one', 'po1', '', 'test add1', '', NULL, NULL, '10:10:00', '23:10:00', '2021-01-20 12:43:50', 35, '', NULL, '2021-01-20 17:13:50', 2, NULL, NULL, 'pending', '2', '1'),
(34, 35, '1', '8888888892', 'branch two', 'po2', '', 'address two', '', NULL, NULL, '11:10:00', '23:30:00', '2021-01-20 12:43:50', 35, '', NULL, '2021-01-20 17:13:50', 3, NULL, NULL, 'pending', '2', '1'),
(35, 35, '1', '7778887878', 'branch three', 'po45', '', 'test address three', '', NULL, NULL, '11:30:00', '16:30:00', '2021-01-20 12:43:50', 35, '', NULL, '2021-01-20 17:13:50', 5, NULL, NULL, 'pending', '2', '1'),
(36, 35, '1', '9999999991', 'branch one', 'po1', '', 'test add1', '', NULL, NULL, '10:10:00', '23:10:00', '2021-01-20 01:08:58', 35, '1,2', NULL, '2021-01-20 17:38:58', 2, NULL, NULL, 'pending', '2', '0'),
(37, 35, '1', '8888888892', 'branch two', 'po2', '', 'address two', '', NULL, NULL, '11:10:00', '23:30:00', '2021-01-20 01:08:58', 35, '3,4', NULL, '2021-01-20 17:38:58', 3, NULL, NULL, 'pending', '2', '0'),
(38, 35, '1', '7778887878', 'branch three', 'po45', '', 'test address three', '', NULL, NULL, '11:30:00', '16:30:00', '2021-01-20 01:08:58', 35, '3', NULL, '2021-01-20 17:38:58', 5, NULL, NULL, 'rejected', '0', '0'),
(53, 42, '0', '', 'caroldata', 'eewserwe', '', 'Palasia, Indore, Madhya Pradesh, India', '', '22.724355', '75.8838944', '10:10:00', '23:00:00', '2021-01-21 10:37:20', NULL, '1,2,3,4', NULL, '2021-01-21 15:07:20', 14, '1611221916downloadd.png', '1611221916imgpsh_fullsize_anim_(1).png,1611221916imgpsh_fullsize_anim.png', 'pending', '2', '0'),
(54, 42, '1', '9999999999999', 'branch one', 'popo444', '', 'palasia square', '', NULL, NULL, '10:00:00', '12:00:00', '2021-01-21 10:42:22', 42, '1,2', NULL, '2021-01-21 15:12:22', 2, NULL, NULL, 'pending', '2', '0'),
(55, 42, '1', '9999999999999', 'branch two', 'olol54654', '', 'geeta bhawan square', '', NULL, NULL, '11:30:00', '16:00:00', '2021-01-21 10:42:22', 42, '3,4', NULL, '2021-01-21 15:12:22', 4, NULL, NULL, 'pending', '2', '0'),
(56, 43, '0', '', 'abc', '', '', 'Abu Dhabi - United Arab Emirates', '', '24.453884', '54.3773438', '14:42:00', '20:42:00', '2021-01-25 10:11:35', NULL, '1,2', NULL, '2021-01-25 14:41:35', 9, NULL, NULL, 'pending', '1', '0'),
(57, 43, '1', '99989898989', 'abc', '', '', 'abc', '', NULL, NULL, '16:00:00', '23:00:00', '2021-01-25 10:30:51', 43, '1,2,3,4', NULL, '2021-01-25 15:00:51', 8, NULL, NULL, 'pending', '2', '1'),
(58, 43, '1', '99989898989', 'abcd', '', '', 'abc', '', NULL, NULL, '16:00:00', '23:00:00', '2021-01-25 10:44:55', 43, '1,2,3,4', NULL, '2021-01-25 15:14:55', 8, NULL, NULL, 'pending', '2', '1'),
(59, 43, '1', '99989898989', 'abcd', '', '', 'abc', '', NULL, NULL, '16:00:00', '23:00:00', '2021-01-25 10:47:16', 43, '1,2,3,4,1,2', NULL, '2021-01-25 15:17:16', 8, NULL, NULL, 'pending', '2', '1'),
(60, 43, '1', '5555555555555', 'newnow', '', '', 'adfc', '', NULL, NULL, '18:16:00', '12:17:00', '2021-01-25 10:47:16', 43, '', NULL, '2021-01-25 15:17:16', 7, NULL, NULL, 'pending', '2', '1'),
(61, 43, '1', '99989898989', 'abcd', '', '', 'abc', '', NULL, NULL, '16:00:00', '23:00:00', '2021-01-25 10:55:30', 43, '1,2,3,4', NULL, '2021-01-25 15:25:30', 8, NULL, NULL, 'pending', '2', '1'),
(62, 43, '1', '5555555555555', 'newnow', '', '', 'adfc', '', NULL, NULL, '18:16:00', '12:17:00', '2021-01-25 10:55:30', 43, '2,3,4', NULL, '2021-01-25 15:25:30', 7, NULL, NULL, 'pending', '2', '1'),
(63, 43, '1', '99989898989', 'abcd', '', '', 'abc', '', NULL, NULL, '16:00:00', '23:00:00', '2021-01-25 11:04:51', 43, '1,2,3,4', NULL, '2021-01-25 15:34:51', 8, NULL, NULL, 'pending', '2', '0'),
(64, 43, '1', '5555555555555', 'newnow', '', '', 'adfc', '', NULL, NULL, '18:16:00', '12:17:00', '2021-01-25 11:04:51', 43, '2,3,4', NULL, '2021-01-25 15:34:51', 7, NULL, NULL, 'pending', '2', '0'),
(65, 43, '1', '9995025222', 'three branch', '', '', 'abc', '', NULL, NULL, '07:32:00', '20:32:00', '2021-01-25 11:04:51', 43, '1,4', NULL, '2021-01-25 15:34:51', 6, NULL, NULL, 'pending', '2', '0'),
(66, 27, '1', '9809898989', 'kanha', '', '', 'Annapurna Road, Prabhu Nagar, Indore, Madhya Pradesh, India', '', NULL, NULL, '19:46:00', '20:47:00', '2021-01-27 03:47:37', 27, '1,2,3,4', NULL, '2021-01-27 15:47:37', 9, NULL, NULL, 'approved', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `print_management`
--

CREATE TABLE `print_management` (
  `id` int(11) NOT NULL,
  `printery_shop_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL COMMENT '1=custom print,2=quickprint,3=translation,4=notes',
  `print_type` int(11) NOT NULL,
  `paper_type_id` int(11) NOT NULL,
  `paper_size_id` int(11) NOT NULL,
  `printing_sides` enum('single','double') COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` enum('colored','black_&_white') COLLATE utf8_unicode_ci DEFAULT NULL,
  `average_price` float NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=active,1=inactive,2=delete',
  `binding` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `print_management`
--

INSERT INTO `print_management` (`id`, `printery_shop_id`, `service_id`, `print_type`, `paper_type_id`, `paper_size_id`, `printing_sides`, `banner_size`, `color`, `average_price`, `created_at`, `updated_at`, `status`, `binding`) VALUES
(1, 16, 1, 1, 4, 4, '', '', '', 10, '2021-01-11 12:45:58', '2021-01-11 17:15:58', '0', ''),
(2, 16, 1, 2, 4, 4, 'single', '', '', 15, '2021-01-11 12:46:48', '2021-01-11 17:16:48', '0', ''),
(3, 16, 1, 2, 4, 4, 'double', '', '', 25, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(4, 17, 1, 1, 4, 4, '', '', '', 11, '2021-01-11 12:45:58', '2021-01-11 17:15:58', '0', ''),
(5, 17, 1, 2, 4, 4, 'single', '', '', 12, '2021-01-11 12:46:48', '2021-01-11 17:16:48', '0', ''),
(6, 17, 1, 2, 4, 4, 'double', '', '', 25, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(7, 17, 1, 2, 5, 4, 'single', '', '', 8, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(8, 17, 1, 2, 5, 4, 'double', '', '', 8, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(9, 17, 1, 3, 4, 5, '', '', '', 18, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '2', ''),
(10, 16, 1, 3, 4, 5, '', '', '', 20, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(11, 16, 1, 4, 4, 0, '', '1', '', 22, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(12, 16, 1, 4, 5, 0, '', '2', '', 25, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(13, 17, 1, 4, 4, 0, '', '1', '', 30, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(14, 17, 1, 4, 5, 0, '', '2', '', 24, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(15, 17, 1, 4, 5, 0, '', '3', '', 24, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(16, 17, 1, 4, 4, 0, '', '2', '', 30, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(17, 17, 1, 3, 5, 4, '', '', '', 22, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(18, 17, 1, 1, 4, 5, '', '', '', 25, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(19, 16, 1, 1, 4, 5, '', '', '', 25, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(20, 17, 1, 3, 4, 4, '', '', '', 18, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(21, 17, 1, 3, 4, 5, '', '', '', 20, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', ''),
(22, 18, 1, 3, 4, 5, '', '', '', 20, '2021-01-11 12:47:30', '2021-01-11 17:17:30', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `print_type`
--

CREATE TABLE `print_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `print_type`
--

INSERT INTO `print_type` (`id`, `name`, `created_at`, `status`, `image`, `url`) VALUES
(1, 'Poster', '2020-12-20 09:09:23', '0', 'poster.png', 'poster'),
(2, 'Flyer', '2020-12-20 09:09:23', '0', 'flyer.png', 'flyer'),
(3, 'Rollup', '2020-12-20 09:09:23', '0', 'rollup.png', 'rollup'),
(4, 'banner', '2020-12-28 14:36:58', '0', 'banner.png', 'banner');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `status` enum('0','1','2') NOT NULL COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `price`, `qty`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Pencil', '16098363471609416045download.png', 100, 1, '0', '2021-01-05 09:45:47', 1, '2021-01-05 09:46:05', 1),
(2, 'Mobile', '1609836432395e24f091c3837a4cd4d1f51d655e33.png', 200, 97, '0', '2021-01-05 09:47:12', 1, '2021-01-05 09:47:55', 1),
(3, 'Bottle', '1609836432395e24f091c3837a4cd4d1f51d655e33.png', 50, -11, '0', '2021-01-05 09:47:12', 1, '2021-01-05 09:47:55', 1),
(4, 'Pen', '1609836432395e24f091c3837a4cd4d1f51d655e33.png', 25, 43, '0', '2021-01-05 09:47:12', 1, '2021-01-05 09:47:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(11) NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1=added,2=ordered,3=updated',
  `product_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `added_date` datetime NOT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `type`, `product_id`, `qty`, `added_date`, `order_id`) VALUES
(1, '1', 1, 10, '2021-01-05 09:45:47', NULL),
(2, '3', 1, 15, '2021-01-05 09:46:05', NULL),
(3, '1', 2, 100, '2021-01-05 09:47:12', NULL),
(4, '3', 2, 100, '2021-01-05 09:47:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('0','1','2') NOT NULL COMMENT '0=active,1=inactive,2=delete',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `status`, `created_at`) VALUES
(1, 'Custom Print', 'custom print description', '0', '2021-01-19 09:36:40'),
(2, 'Quick Print', 'Quick Print description', '0', '2021-01-19 09:36:26'),
(3, 'Translation', 'short description for translation', '0', '2021-01-19 09:36:12'),
(4, 'Notes', 'Notes short description', '0', '2021-01-19 09:35:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3' COMMENT '1=admin,2=vendor,3=consumer',
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=inactive',
  `delete_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=active,1=deleted',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_branches` int(11) NOT NULL DEFAULT '0',
  `reg_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reg_proof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_status` enum('incomplete','pending','approved','disapproved') COLLATE utf8_unicode_ci DEFAULT 'incomplete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `firstname`, `lastname`, `gender`, `email`, `mobile`, `password`, `status`, `delete_status`, `created_at`, `updated_at`, `latitude`, `longitude`, `address`, `total_branches`, `reg_number`, `reg_proof`, `profile_status`) VALUES
(1, '1', 'Admin', NULL, NULL, 'admin@gmail.com', NULL, '0192023a7bbd73250516f069df18b500', '0', '0', '2020-12-12 04:59:54', '2020-12-12 04:59:54', '', '', NULL, 0, NULL, NULL, NULL),
(2, '2', 'Vendor', NULL, NULL, 'krishna@gmail.com', NULL, 'nIdiQgxQtRzzq8DzaugzEg==', '0', '0', '2020-12-12 06:43:01', '2020-12-12 06:43:01', '', '', NULL, 0, NULL, NULL, NULL),
(4, '2', 'student', 'xerox', NULL, 'student@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '1', '2020-12-13 10:58:00', '2020-12-13 10:58:00', '', '', NULL, 0, NULL, NULL, NULL),
(5, '2', 'sanjay', 'malvia', NULL, 'sanjay@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-13 11:10:35', '2020-12-13 11:10:35', '', '', NULL, 0, NULL, NULL, NULL),
(6, '2', 'satyam', 'test', NULL, 'satyam@gmail.com', '9998889898', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-15 11:56:22', '2021-01-28 07:56:20', '', '', NULL, 0, 'reg123', NULL, NULL),
(7, '2', 'v-one', NULL, NULL, 'vone@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-12 06:43:01', '2020-12-12 06:43:01', '', '', NULL, 0, NULL, NULL, NULL),
(8, '2', 'v', 'two', NULL, 'vtwo@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-13 10:58:00', '2020-12-13 10:58:00', '', '', NULL, 0, NULL, NULL, NULL),
(9, '2', 'v', 'threee', NULL, 'vthree@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-13 11:10:35', '2020-12-13 11:10:35', '', '', NULL, 0, NULL, NULL, NULL),
(10, '2', 'v', 'four', NULL, 'vfour@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-15 11:56:22', '2020-12-15 23:56:22', '', '', NULL, 0, NULL, NULL, NULL),
(11, '3', 'test', NULL, NULL, 'test@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-21 11:45:02', '2020-12-21 23:45:02', '', '', NULL, 0, NULL, NULL, NULL),
(12, '3', 'test', NULL, NULL, 'tests@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-21 11:59:21', '2020-12-21 23:59:21', '', '', NULL, 0, NULL, NULL, NULL),
(13, '3', 'soniya', NULL, NULL, 'soniya@gmail.com', NULL, '3a0cca839c97d9f4a352c9e11c22e379', '0', '0', '2020-12-22 07:23:29', '2020-12-22 07:23:29', '22.724355', '75.8838944', 'Palasia, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(14, '3', 'test', NULL, NULL, 'soniyakukreja@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-22 07:47:46', '2020-12-22 07:47:46', '', '', NULL, 0, NULL, NULL, NULL),
(21, '2', 'test', 'vendore', 'male', 'testing@gmail.com', '8889998989', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-28 01:27:27', '2021-01-12 11:56:22', '', '', NULL, 0, NULL, NULL, NULL),
(22, '2', 'sonali', NULL, NULL, 'testing_one@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-28 01:29:18', '2020-12-28 17:59:18', '', '', NULL, 0, NULL, NULL, NULL),
(23, '2', 'last', NULL, NULL, 'last@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-28 01:30:35', '2020-12-28 18:00:35', '', '', NULL, 0, NULL, NULL, NULL),
(25, '3', 'shivani', NULL, NULL, 'shivani@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-29 10:12:58', '2020-12-29 14:42:58', '22.6766721', '75.8337998', 'Annapurna Road, Prabhu Nagar, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(27, '2', 'omkar', 'prasad', 'male', 'omkar@gmail.com', '9668888888888', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2020-12-29 10:21:40', '2021-01-27 03:54:41', '22.7189594', '75.897201', 'Tilak Nagar, Indore, Madhya Pradesh, India', 1, 'REG-9373', '1611060926Chrysanthemum.jpg', NULL),
(30, '3', 'srishti', 'test', 'female', 'srishti@gmail.com', '9898989898', 'e10adc3949ba59abbe56e057f20f883e', '1', '0', '2021-01-06 06:42:45', '2021-01-06 11:12:45', '22.724355', '75.8838944', 'Palasia, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(31, '2', 'vendor test', NULL, NULL, 'vendortest@gmail.com', '9898989999', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-09 08:23:11', '2021-01-09 12:53:11', NULL, NULL, 'Annapurna Road, Prabhu Nagar, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(32, '2', 'v', 'test', 'male', 'v@gmail.com', '8885558585', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-09 08:24:16', '2021-01-09 08:29:31', NULL, NULL, 'MR 9 Road, Anil Nagar, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(33, '3', 'new', 'test', 'male', 'newregister@gmail.com', '9998889898', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-13 06:59:52', '2021-01-13 11:29:52', '22.724355', '75.8838944', 'Palasia, Indore, Madhya Pradesh, India', 0, NULL, NULL, NULL),
(35, '2', 'suresh', 'kumar kukreja', 'male', 'suresh@gmail.com', '+96654545454', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-15 09:57:23', '2021-01-20 12:10:16', NULL, NULL, 'Bengali Square, Indore, Madhya Pradesh, India', 2, 'reg123123', NULL, NULL),
(36, '2', 'shiva', NULL, NULL, 'shiva@gmail.com', '+966878787878', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-19 11:03:11', '2021-01-19 15:33:11', NULL, NULL, 'Tilak Nagar, New Delhi, Delhi, India', 0, NULL, NULL, NULL),
(37, '2', 'shambhoo', 'patidar', NULL, 'shambhoo@gmail.com', '+966588899989', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-19 11:06:21', '2021-01-20 06:42:56', NULL, NULL, 'Rasoma Square, Scheme 54 PU4, Indore, Madhya Pradesh', 0, 'reg-12212', NULL, NULL),
(38, '2', 'test new', NULL, NULL, 'mac@gmail.com', '+966888888888', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-21 05:53:05', '2021-01-21 10:23:05', NULL, NULL, 'Hawally, Kuwait', 0, NULL, NULL, NULL),
(42, '2', 'madhuri', 'Gandhi', NULL, 'madhuri@gmail.com', '+966888999898', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-21 10:37:20', '2021-01-21 10:38:36', NULL, NULL, 'Palasia, Indore, Madhya Pradesh, India', 2, 'resg123', '1611221916dummy.pdf', 'incomplete'),
(43, '2', 'new vendor', 'new', NULL, 'abc@gmailc.om', '+966888666566', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '2021-01-25 10:11:35', '2021-01-25 10:13:14', NULL, NULL, 'Abu Dhabi - United Arab Emirates', 3, '999888reg', NULL, 'incomplete');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_bank_account`
--

CREATE TABLE `vendor_bank_account` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `acc_holder_name` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `acc_number` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=active,2=delete',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `vendor_bank_account`
--

INSERT INTO `vendor_bank_account` (`id`, `vendor_id`, `acc_holder_name`, `bank_name`, `branch_name`, `acc_number`, `ifsc_code`, `status`, `created_at`) VALUES
(1, 35, 'suresh', 'kotak', 'annpurna road', 'khi4566999', '22223eeee', '0', '2021-01-20 01:48:27'),
(3, 42, 'one', 'two', 'three', 'four', 'five', '0', '2021-01-21 11:10:58'),
(4, 43, 'a', 'b', 'c', 'd', 'e', '0', '2021-01-25 10:29:05');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_prices`
--

CREATE TABLE `vendor_prices` (
  `id` int(11) NOT NULL,
  `price_comb_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `added_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_prices`
--

INSERT INTO `vendor_prices` (`id`, `price_comb_id`, `vendor_id`, `price`, `status`, `added_at`, `updated_at`) VALUES
(1, 1, 16, 10, '0', '2021-01-27 02:15:52', '2021-01-27 02:19:49'),
(2, 14, 16, 10, '0', '2021-01-27 02:15:52', '2021-01-27 02:19:49'),
(3, 15, 16, 20.2, '0', '2021-01-27 02:15:52', '2021-01-27 02:19:49'),
(4, 16, 16, 30, '0', '2021-01-27 02:15:52', '2021-01-27 02:19:49'),
(5, 2, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(6, 3, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(7, 4, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(8, 5, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(9, 6, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(10, 12, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(11, 13, 16, 0, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(12, 17, 16, 100, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(13, 18, 16, 200, '0', '2021-01-27 02:44:08', '2021-01-27 02:44:08'),
(14, 7, 16, 0, '0', '2021-01-27 02:51:08', '2021-01-27 02:51:08'),
(15, 8, 16, 0, '0', '2021-01-27 02:51:08', '2021-01-27 02:51:08'),
(16, 19, 16, 10, '0', '2021-01-27 02:51:08', '2021-01-27 02:51:08'),
(17, 20, 16, 25.55, '0', '2021-01-27 02:51:08', '2021-01-27 02:51:08'),
(18, 11, 16, 0, '0', '2021-01-27 03:23:27', '2021-01-27 03:23:27'),
(19, 21, 16, 3, '0', '2021-01-27 03:23:27', '2021-01-27 03:23:27'),
(20, 22, 16, 2.75, '0', '2021-01-27 03:23:27', '2021-01-27 03:23:27'),
(21, 1, 3, 0, '0', '2021-01-28 07:54:27', '2021-01-28 07:54:27'),
(22, 14, 3, 15, '0', '2021-01-28 07:54:27', '2021-01-28 07:54:27'),
(23, 15, 3, 20, '0', '2021-01-28 07:54:27', '2021-01-28 07:54:27'),
(24, 16, 3, 25.5, '0', '2021-01-28 07:54:27', '2021-01-28 07:54:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_size`
--
ALTER TABLE `banner_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_width`
--
ALTER TABLE `banner_width`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extra_copies`
--
ALTER TABLE `extra_copies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incharge_person`
--
ALTER TABLE `incharge_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_address`
--
ALTER TABLE `order_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paper_size`
--
ALTER TABLE `paper_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paper_type`
--
ALTER TABLE `paper_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_combination`
--
ALTER TABLE `price_combination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_range`
--
ALTER TABLE `price_range`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printery_shop`
--
ALTER TABLE `printery_shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `print_management`
--
ALTER TABLE `print_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `print_type`
--
ALTER TABLE `print_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_bank_account`
--
ALTER TABLE `vendor_bank_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_prices`
--
ALTER TABLE `vendor_prices`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_size`
--
ALTER TABLE `banner_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `banner_width`
--
ALTER TABLE `banner_width`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `commission`
--
ALTER TABLE `commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customer_address`
--
ALTER TABLE `customer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `extra_copies`
--
ALTER TABLE `extra_copies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `incharge_person`
--
ALTER TABLE `incharge_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `order_address`
--
ALTER TABLE `order_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `paper_size`
--
ALTER TABLE `paper_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `paper_type`
--
ALTER TABLE `paper_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `price_combination`
--
ALTER TABLE `price_combination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `price_range`
--
ALTER TABLE `price_range`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `printery_shop`
--
ALTER TABLE `printery_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `print_management`
--
ALTER TABLE `print_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `print_type`
--
ALTER TABLE `print_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `vendor_bank_account`
--
ALTER TABLE `vendor_bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vendor_prices`
--
ALTER TABLE `vendor_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
