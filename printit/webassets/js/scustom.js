$('.serviceTypeOptions').on('click',function(e){
	var url = $(this).attr('action');
	$.ajax({
		url: url,
		dataType: "json",
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			$('#service_type_modal').modal('hide');    
			window.location.reload();
		}
	});	
});

$('#copy_number').on('keyup',function(){
	if(parseInt($(this).val())<0 || isNaN($(this).val())){
		$(this).val(0);
	}
});
//$('.modal').modal({backdrop: 'static', keyboard: false})  

$(".img_box").click(function(){
	$("li").removeClass("flow_top_border");
	$(this).parents("li").addClass("flow_top_border");


	$(".process_flow" ).each(function(i,v) {
		var ds = $(this).find('.paper_type');
		var dsid = ds.attr('id');

	  if($(this).hasClass('flow_top_border')){
		ds.addClass(dsid).removeClass(dsid+'1');
	  }else{
		ds.removeClass(dsid).addClass(dsid+'1');
	  }
	});

});

$(document).on('submit','#user_registeration',function(e){
    e.preventDefault();
    $('.error').hide();
    
    if($('#semail').val().trim()=='' || !validateEmail($('#semail').val())){
        $('#semail').focus();
        $('#email_err').show();
        return false;
    }    

    if($('#f_name').val().trim()==''){
        $('#f_name').focus();
        $('#f_name_err').show();
        return false;
    }

    if($('#l_name').val().trim()==''){
        $('#l_name').focus();
        $('#l_name_err').show();
        return false;
    }

    if($('#s_mobile').val().trim()==''){
        $('#s_mobile').focus();
        $('#l_mobile_err').show();
        return false;
    }


    if($('#u_address').val().trim()==''){
        $('#u_address').focus();
        $('#u_address_err').show().html('Please enter your address');
        return false;
    }else if($('#user_lat').val().trim()=='' || $('#user_lng').val().trim()==''){
        $('#u_address_err').show().html('Please select address from auto sugession');
        return false;        
    }

    var pass = $('#password').val().trim();
    var cpass = $('#cpassword').val().trim();
    
    if(pass==''){
        $('#password').focus();
        $('#pass_err').show();
        return false;
    }
    if(cpass==''){
        $('#cpassword').focus();
        $('#cpass_err').show();
        return false;
    }
    
    if(pass !=cpass){
        $('#cpassword').focus();
        $('#cpass_err').show().html("Passwords don't match!");
        return false;
    } 
    
    if($('.s_gender').is(':checked')) {
		var gender = $('.s_gender:checked').val();
     	$('#s_gender_err').hide();
 	}else{
 		$('#s_gender_err').show();
 		return false;
 	}
    
    ds = $(this);
	var url = $(this).attr('action');
	var formData = new FormData(ds[0]);
	$.ajax({
		url: url,
		type: "POST",
		data:formData,
		dataType: "json",
	    processData: false,
	    contentType: false,			
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			
	        $('#common_err').html(res.msg).show();	    
			if(res.success==true){
				ds[0].reset();
			    $('#common_err,#login_msg').removeClass('text-danger').addClass('text-success');
			    $('#login_msg').html('You can login here now').css('display','block');
			    $('.signup-area').css('display','none').css('display','block');
			    $('.login-area').css('display','block');
                //setTimeout(function(){ window.location.reload(); });
			}else{
				$('#common_err').addClass('text-danger').removeClass('text-success');
			}
		}
	});
});
      

function validateEmail(email){
    var reg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;		
	if (reg.test(email) == false) 
	{
		return false;
	}
    return true;
}    


function validateMobile(mobile){
	var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
	if (filter.test(mobile) == false) 
	{
		return false;
	}
    return true;
} 


function alphanumeric_space_only(ds){
	var regex = /^[0-9a-zA-Z ]*$/;
	return regex.test(ds);
}

function validate_pass(ds){
	var dsval = $(ds).val();
	var stre_field = $(ds).parents('.form-group').find('.strongWeak');
	stre_field.show();
	var specialChars = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/gi;
    var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;

	var percent = 0;
	//====check length
	if(dsval.length >=8){
		if(dsval.length >128){
			return { success : false, msg : 'Password Exceed Maximum Allowed Length' };
		}else{
			percent +=20;
		}
		
	}
	
	//====check uppercase
	if(dsval.replace(/[^A-Z]/g, "").length>=1){
		percent +=20;
	}
	//====check lowercase
	if(dsval.replace(/[^a-z]/g, "").length>=1){
		percent +=20;
	}

	//====check numbers
	if(dsval.replace(/[^0-9]/g, "").length>=1){
		percent +=20;
	}
	//====check special charachter
	if(dsval.match(specialChars)){
		percent +=20;
	}
    
    
	if(parseInt(percent)<=25){
		if(parseInt(percent)==0){
			stre_field.find('.stren_line').css({"background": "red","width":"10%"});
		}else{
			stre_field.find('.stren_line').css({"background": "red","width":percent+"%"});
		}
		stre_field.find('.stren_text').html('Very Weak');
		return { success : false, msg : 'Your Password is Weak' };

	}

	if(parseInt(percent)>25 && parseInt(percent)<50){
		stre_field.find('.stren_line').css({"background": "red","width":percent+"%"});
		stre_field.find('.stren_text').html('Weak');
		return { success : false, msg : 'Your Password is Weak' };

	}

	if(parseInt(percent)>50 && parseInt(percent)<100){
		stre_field.find('.stren_line').css({"background": "#8de6498c","width":percent+"%"});
		stre_field.find('.stren_text').html('Medium');
		return { success : false, msg : 'Your Password is Medium' };

	}

	if(parseInt(percent)==100 && regex.test(dsval)){
		stre_field.find('.stren_line').css({"background": "#49e663","width":percent+"%"});
		stre_field.find('.stren_text').html('Strong');

		var pass = $('#password').val().trim();
		var cpass = $('#cpassword').val().trim();

		if(pass !='' && cpass !=''){
			if(pass != cpass) {
			    return { success : false, msg : "Your Password doesn't match" };
			} else {
			    $('#p_err ,#cp_err').hide();
			    return { success : true };
			}
		}else{
			return { success : true };
		}	
	}
	
}

$(document).on('submit','#userlogin',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#login_email').val().trim()=='' || !validateEmail($('#login_email').val())){
        $('#login_email').focus();
        $('#l_email_err').show();
        return false;
    }
    if($('#login_pass').val().trim()==''){
        $('#login_pass').focus();
        $('#l_pass_err').show();
        return false;
    }
   
    
    ds = $(this);
	var url = $(this).attr('action');
	var formData = new FormData(ds[0]);
	$.ajax({
		url: url,
		type: "POST",
		data:formData,
		dataType: "json",
	    processData: false,
	    contentType: false,			
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			if(res.success==true){
				if(res.role==3){
					window.location.reload();
				}else{
					window.location.href = res.redirect;
				}
                //window.location.href = site_url+'select_service_type';
                
			}else{
		        $('#l_pass_err').html(res.msg).show();	    
			}
		}
	});
});


$('#forgotpassForm').on('submit',function(e){
        e.preventDefault();
        $('.error').hide();
        $('#f_send_btn').prop('disabled',false);
        if($('#f_email').val().trim()=='' || !validateEmail($('#f_email').val())){
            $('#f_email').focus();
            $('#f_email_err').show();
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
				    $('#f_email_err').html(res.msg).addClass("text-success").removeClass("text-danger").css("display","block");
				    $('#f_send_btn').prop('disabled',true);
				}else{
			        $('#f_email_err').html(res.msg).addClass("text-danger").removeClass("text-success").css("display","block");	    
				}
			}
		});
  
});


$('#resetPassForm').on('submit',function(e){
        e.preventDefault();
        $('.error').hide();
        var pass  = $('#reset_password').val().trim();
        var cpassword = $('#reset_cpassword').val().trim();

        if(pass==''){
            $('#reset_password').focus();
            $('#reset_pass_err').css('display','block');
            return false;
        }

        if(cpassword==''){
            $('#reset_cpassword').focus();
            $('#reset_cpass_err').css('display','block');
            return false;
        }
        
        if(pass!=cpassword){
            $('#reset_cpassword').focus();
            $('#reset_cpass_err').css('display','block').html("Password don't match with confirm password field");
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
				    $('#reset_cpass_err').html(res.msg).addClass("text-success").removeClass("text-danger").css("display","block");
				    setTimeout(function(){ window.location.href = site_url; }, 2000);
				}else{
			        $('#reset_cpass_err').html(res.msg).addClass("text-danger").removeClass("text-success").css("display","block");	    
				}
			}
		});
});


$(document).on('submit','#vendor_registeration',function (e) {
	e.preventDefault();
    var allIsOk = true;
	$('.invalidText').hide();

	var name  = $('#v_fname').val().trim();
	var v_email  = $('#v_email').val().trim();
	var shop_name  = $('#v_shop_name').val().trim();
	var v_address  = $('#v_address').val().trim();
	var v_password  = $('#v_password').val().trim();
	var v_cpassword  = $('#v_cpassword').val().trim();
	var v_contact_no  = $('#v_contact_no').val().trim();
	
	var vendor_lat  = $('#vendor_lat').val().trim();
	var vendor_lng  = $('#vendor_lng').val().trim();

	if(name == '') {
		$("#v_fname").focus();
		$("#v_name_err").show().html('Please Enter Your First Name');
        allIsOk = false;
    }else
    if(shop_name==''){
    	$("#v_shop_name").focus();
		$("#v_shopname_err").show().html('Please Enter Your Shop Name');
        allIsOk = false;	
    }else
    if(v_email=='' || !validateEmail(v_email)){
    	$("#v_email").focus();
		$("#v_email_err").show().html('Please Enter Valid Email Address');
        allIsOk = false;	
    }else
    if(v_address==''){
    	$("#v_address").focus();
		$("#v_address_err").show().html('Please Enter Your Address');
        allIsOk = false;	
    }else if(vendor_lat=='' || vendor_lng==''){
		$("#v_address_err").show().html('Please select address from auto sugession');
        allIsOk = false;	        
    }
    if(v_contact_no==''){
    	$("#v_contact_no").focus();
		$("#v_contact_err").show().html('Please Enter Your Contact Number');
        allIsOk = false;	
    }else if(v_contact_no.length <10){
    	$("#v_contact_no").focus();
		$("#v_contact_err").show().html('Please Enter Valid Contact Number');
        allIsOk = false;
    }else 
  //   if(!validateMobile(v_contact_no)){
  //   	$("#v_contact_no").focus();
		// $("#v_contact_err").show().html('Please Enter Valid Contact Number');
  //       allIsOk = false;	
  //   }else 
    if(v_password==''){
    	$("#v_password").focus();
		$("#v_pass_err").show().html('Please Enter Your Password');
        allIsOk = false;	
    }else if(v_cpassword ==''){
    	$("#v_cpassword").focus();
		$("#v_cpass_err").show().html('Please Confirm Your Password');
        allIsOk = false;	
    }else if(v_password !=v_cpassword){
    	$("#v_cpassword").focus();
		$("#v_cpass_err").show().html("Passwords don't match");
        allIsOk = false;	
    }
    
    if(allIsOk){
    	ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				$('#v_common_error').show().html(res.msg);
				if(res.success) {
					$('#v_common_error').addClass('text-success').removeClass('text-danger');
					ds[0].reset();
					setTimeout(function(){
					// $('.login-area').fadeIn(1000);
					// $('.vander-area').fadeOut();
					window.location.href = site_url+res.redirect;
					}, 2000);
				}else{
					$('#v_common_error').addClass('text-danger').removeClass('text-success');
					allIsOk = false;
				}

			}
		});
    }
    
    return allIsOk    
});

$('.user_gender').on('click',function(){
    $('.user_gender').parent('label').removeClass('active');
    $(this).parent('label').addClass('active');
});


$(document).on('submit','#updateprofile',function(e){
    e.preventDefault();
    $('.error').hide();
    
    if($('#pemail').val().trim()=='' || !validateEmail($('#pemail').val())){
        $('#pemail').focus();
        $('#pemail_err').show();
        return false;
    }    

    if($('#p_fname').val().trim()==''){
        $('#p_fname').focus();
        $('#p_fname_err').show();
        return false;
    }

    if($('#p_lname').val().trim()==''){
        $('#p_lname').focus();
        $('#p_lname_err').show();
        return false;
    }

    if($('#pmobile').val().trim()==''){
        $('#pmobile').focus();
        $('#p_mobile_err').show();
        return false;
    }


    // if($('#profile_address').val().trim()==''){
    //     $('#profile_address').focus();
    //     $('#p_address_err').show();
    //     return false;
    // }

    
    if($('.user_gender').is(':checked')) {
		var gender = $('.user_gender:checked').val();
     	$('#p_gender_err').hide();
 	}else{
 		$('#p_gender_err').show();
 		return false;
 	}
    
    ds = $(this);
	var url = $(this).attr('action');
	var formData = new FormData(ds[0]);
	$.ajax({
		url: url,
		type: "POST",
		data:formData,
		dataType: "json",
	    processData: false,
	    contentType: false,			
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			
	        $('#profile_update_msg').html(res.msg).css("display","block");	    
			if(res.success==true){
			    $('#profile_update_msg').removeClass('text-danger').addClass('text-success');
			}else{
				$('#profile_update_msg').addClass('text-danger').removeClass('text-success');
			}
			$('html, body').animate({
                scrollTop: $("#profile_update_msg").offset().top
            }, 2000);
            setTimeout(function(){ window.location.href = site_url+'/user'; }, 2000);
		}
	});
});


function gotohome(){
	window.location.href = site_url;
}

$('#open_login_modal').on('click',function(){
	var id= $(this).parents('.poster_popup').attr('id');
	//$('#plz_login_modal').modal('hide');
	$('#'+id).modal('hide');
	$('#form-button-toggle').addClass('open');
	$('#form-panel').addClass('showing');
});

$(document).on('click','.filter_notes',function(){
	var catid = $(this).attr('cat');
	$(document).find('.filter_notes').removeClass('active');
	$(this).addClass('active');
	$.ajax({
		url: site_url+'home/filter_notes/'+catid,
		dataType: "json",
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			if(res.success){
				$('#notes_div').html(res.html);
			}
		}
	});		
});

$('#change_pass').on('submit',function(e){
    e.preventDefault();
    $('.error').hide();
    var old_pass  = $('#old_pass').val().trim();
    var new_pass  = $('#new_pass').val().trim();
    var confirm_pass = $('#confirm_pass').val().trim();
    
    if(old_pass==''){
        $('#old_pass').focus();
        $('#oldpass_err').css('display','block');
        return false;
    }        
    
    if(new_pass==''){
        $('#new_pass').focus();
        $('#newpass_err').css('display','block');
        return false;
    }

    if(confirm_pass==''){
        $('#confirm_pass').focus();
        $('#cpass_err').css('display','block');
        return false;
    }
    
    if(new_pass!=confirm_pass){
        $('#confirm_pass').focus();
        $('#cpass_err').css('display','block').html("New password don't match with confirm password field");
        return false;
    }
    ds = $(this);
	var url = $(this).attr('action');
	var formData = new FormData(ds[0]);
	$.ajax({
		url: url,
		type: "POST",
		data:formData,
		dataType: "json",
	    processData: false,
	    contentType: false,			
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			if(res.success==true){
			    $('#cpass_err').html(res.msg).addClass("text-success").removeClass("text-danger").css("display","block");
			    ds[0].reset();
			    //setTimeout(function(){ window.location.href = site_url; }, 2000);
			}else{
		        $('#cpass_err').html(res.msg).addClass("text-danger").removeClass("text-success").css("display","block");	    
			}
		}
	});
});


$('.edit_useraddress').on('click',function(){
    var addid= $(this).attr('addid');
    $.ajax({
		url: site_url+'user/ajax_editadd_view',
		type: "POST",
		data:{"add_id":addid},
		beforeSend:function(){
			ajaxindicatorstart();
		},
		success: function(res) {
			ajaxindicatorstop();
			$('#addresslist').css('display','none');
			$('#edit_address_form').html(res).css('display','block');
		}
	});
});

$(document).on('submit','#update_address',function(e){
    e.preventDefault();
    $('.error').css('display','none');
    var edit_long = $('#edit_longitude').val().trim();
    var edit_lat = $('#edit_latitude').val().trim();
    var add_type = $('#address_type :selected').val();
    
    if($(this).find('#edit_add').val()==''){
        $('#edit_add').focus();
        $('#o_add_err').css('display','block').html('Please enter your address');
        return false;
    }else if(edit_lat=='' || edit_long==''){
        $('#edit_add').focus();
        $('#o_add_err').css('display','block').html('Please select address from auto sugession');
        return false;        
    }
    // must check lat or long
  
    
    if(add_type==''){
        $('#address_type').focus();
        $('#add_type_err').css('display','block');
        return false;
    }

    if($('#block').val()==''){
        $('#block').focus();
        $('#add_block_err').css('display','block');
        return false;
    }
    
    if($('#street').val()==''){
        $('#street').focus();
        $('#add_street_err').css('display','block');
        return false;
    }
    
    if(add_type=="home" && $('#house').val()==''){
        $('#house').focus();
        $('#add_house_err').css('display','block');
        return false;
    }
    
    if((add_type=="office" || add_type=="apartment") && $('#building').val()==''){
        $('#building').focus();
        $('#add_building_err').css('display','block');
        return false;
    } 
    
    if((add_type=="office" || add_type=="apartment") && $('#floor').val()==''){
        $('#floor').focus();
        $('#add_floor_err').css('display','block');
        return false;
    }    
    if(add_type=="apartment" && $('#apartment').val()==''){
        $('#apartment').focus();
        $('#add_apt_err').css('display','block');
        return false;
    }   

    
    if(add_type=="office" && $('#office').val()==''){
        $('#office').focus();
        $('#add_office_err').css('display','block');
        return false;
    }    
    
    if($('#mobile').val()==''){
        $('#mobile').focus();
        $('#add_mobile_err').css('display','block');
        return false;
    }
    
    
    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#updade_add_error').html(res.msg).show();      
            if(res.success==true){
                $('#updade_add_error').removeClass('text-danger').addClass('text-success');
                //setTimeout(function(){ 
                    //window.location.href = res.href;
                //});
            }else{
                $('#updade_add_error').addClass('text-danger').removeClass('text-success');
            }
            $('html, body').animate({
                scrollTop: $("#updade_add_error").offset().top
            }, 2000);
        }
    }); 
});
