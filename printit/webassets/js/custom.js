

$('.s_gender').on('click',function(){
    $('.s_gender').parent('label').removeClass('active');
    $(this).parent('label').addClass('active');
});

$(document).ready(function () {
  $(".minus").click(function () {
    var $input = $(this).parent().find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".plus").click(function () {
    var $input = $(this).parent().find("input");
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
});


// Right Sidebar Login Form//
$(document).ready(() => {
	$("#form-button-toggle").on("click", () => {
		toggleMenu();
	});
});
const toggleMenu = () => {
	$("#form-button-toggle").toggleClass("open");
	$("#form-panel").toggleClass("showing");
};

$('#singup-btn').click(function(){
$('.signup-area').fadeIn(1000);
 $('.login-area').fadeOut();
});

$('#back-btn').click(function(){
$('.login-area').fadeIn(1000);
 $('.signup-area').fadeOut();
});

$('#forget-btn').click(function(){
$('.forget-pass-area').fadeIn(1000);
 $('.login-area').fadeOut();
});

   $('#vendor-butn').click(function(){
    $('.vander-area').fadeIn(1000);
     $('.login-area').fadeOut();
  });

    $('#v-back-btn').click(function(){
    $('.login-area').fadeIn(1000);
     $('.vander-area').fadeOut();
  });

$('#back1-btn').click(function(){
$('.login-area').fadeIn(1000);
 $('.forget-pass-area').fadeOut();
});

$('#product_box-l').click(function(){
  $(this).fadeOut(1000);
  $('.product-whit-box').fadeIn();
});
$('.product-whit-box').click(function(){
  $(this).fadeOut(1000);
  $('#product_box-l').fadeIn();
});
// Right Sidebar Login Form END//
// Left Menu START//



$(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                 $('#sidebar').toggleClass('fullmenu');
            });
        });
//Left Menu END//
//print_option START//

$(document).ready(function(){
  $( "#print_option li" ).hover(
  function() {
    var a =$(this).attr("id");
    $( this ).parents("#print_option").addClass(a);
  }, function() {
    var a =$(this).attr("id");
   $( this ).parents("#print_option").removeClass(a);
  }
);
});

//print_option END//
//POSTER PAGE ANIMATION START//
 $(document).ready(function(){
          /*$(".img_box").click(function(){
            $("li").removeClass("flow_top_border");
            $(this).parents("li").addClass("flow_top_border");
          });*/
           $(".poster_type_confirm").click(function(){
            
           var a= $(this).attr("data-title");
            $("."+a).show();
          });
        });
//POSTER PAGE ANIMATION END//
//CUSTOM SCROLL START//
(function($){
      $(window).load(function(){
        
       
        
        $("#notes_selection").mCustomScrollbar({
          axis:"x",
          theme:"light-3",
          advanced:{autoExpandHorizontalScroll:true}
        });
        $("#more_product_list").mCustomScrollbar({
          axis:"x",
          theme:"light-3",
          advanced:{autoExpandHorizontalScroll:true}
        });
        
       
        
      });
    })(jQuery);//CUSTOM SCROLL END//

    $(document).ready(function(){
          $(".radio_address").click(function(){
            $(".radio_address").removeClass("active");
            $(this).addClass("active");
          });

  });




$('#login-back-btn').click(function(){
$('#form-panel').removeClass('showing');
$("#form-button-toggle").removeClass("open");
});



$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val(),
            optext: $this.children('option').eq(i).text()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        //$styledSelect.text($(this).text()).removeClass('active');
        $styledSelect.text($(this).attr('optext')).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

//jQuery('.select-area-inner').find('.select-options li').append('<span class="infor-tooltip"><i class="fa fa-info-circle" aria-hidden="true">abc</i></span>');

