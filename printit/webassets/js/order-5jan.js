$('#ordertype').on('click',function(){
    var ordertype =  $('.order_type:checked ').val();
    if($('.order_type').is(':checked')!=true) { 
	    $('#ordertype_err').css('display','block');
	    return false;    
    }else{
        $('#panetry_selection').modal('hide');
        var url = site_url+'set_service_type/'+ordertype;
        $.ajax({
			url: url,
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
                window.location.reload();
			}
		});
    }
});

$('#paperTypeConfirm').on('click',function(){
	$('.error').hide();
    var paper_type = $('#paper_type :selected').val();
    var paper_type_content = $('#paper_type :selected').html();
    if(paper_type=='') { 
    	 $('#type_error').css('display','block').html('Please Select Paper Type');
   	     return false;    
    }else{
        var url = site_url+'user/set_paper_type/'+paper_type;
        $.ajax({
            url: url,
            dataType:"json",
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#ptype').html(paper_type_content).show();
                    $('#papertypemodal').modal('hide');
                    $('.error').hide();
                }else{
                   $('#type_error').css('display','block').html(res.msg); 
                }
            }
        });
    }

});

$('#papersizeconfirm').on('click',function(){
    $('.error').hide();
   
    var paper_size = $('#paper_size :selected').val();
    if(paper_size=='') { 
        $('#size_error').css('display','block').html('Please Select Paper Size');
	    return false;    
    }else{
        var paper_size_content = $('#paper_size :selected').html();
        var url = site_url+'user/set_paper_size/'+paper_size;
        $.ajax({
            url: url,
            dataType:'json',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#psize').html(paper_size_content).show();
                    $('#papersizemodal').modal('hide');
                    $('.error').hide();
                }else{
                   $('#size_error').css('display','block').html(res.msg); 
                }
            }
        });
    }

});


$('#confirmPrintingSide').on('click',function(){
    $('.error').hide();
   
    var printing_side = $('#printing_side :selected').val();
    if(printing_side=='') { 
        $('#printside_err').css('display','block').html('Please Select Printing Side');
        return false;    
    }else{
        var printside_content = $('#printing_side :selected').html();
        var url = site_url+'user/set_printing_side/'+printing_side;
        $.ajax({
            url: url,
            dataType:'json',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#printingsidemodal').modal('hide');
                    $('#psides').html(printside_content).show();
                    $('.error').hide();
                }else{
                   $('#printside_err').css('display','block').html(res.msg); 
                }
            }
        });
    }
});

$('#bannerWidthConfirm').on('click',function(){
    $('.error').hide();
   
    var banner_size = $('#banner_size :selected').val();
    var banner_size_content = $('#banner_size :selected').html();
    if(banner_size=='') { 
         $('#width_err').css('display','block').html('Please Select Banner Width');
         return false;    
    }else{
        var url = site_url+'user/set_banner_width/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'width':banner_size},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#banner_width_modal').modal('hide');
                    $('#pwidth').html(banner_size_content).show();
                    $('.error').hide();
                }else{
                   $('#pwidth').css('display','block').html(res.msg); 
                }
            }
        });
    }  
});

//======quick print===========//
$("#color_confirm").on('click',function(){
    $('.error').hide();
   
    var color = $('#color :selected').val();
    var color_content = $('#color :selected').html();
    color_content = color_content.replace("_", " ");
    if(color=='') { 
         $('#color_err').css('display','block').html('Please Select Color');
         return false;    
    }else{
        var url = site_url+'user/set_color/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'color':color},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#colormodal').modal('hide');
                    $('#pcolor').html(color_content).show();
                }else{
                   $('#pcolor').css('display','block').html(res.msg); 
                }
            }
        });
    }    
});

$("#binding_confirm").on('click',function(){
    $('.error').hide();
   
    var binding = $('#binding :selected').val();

    if(binding=='') { 
         $('#binding_err').css('display','block').html('Please Select Binding');
         return false;    
    }else{
        var url = site_url+'user/set_binding/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'binding':binding},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#bindingmodal').modal('hide');
                    $('#pbinding').html(binding).show();
                }else{
                   $('#pbinding').css('display','block').html(res.msg); 
                }
            }
        });
    }    
});
//======quick print===========//

//======translation=====
$("#transTypeConfirm").on('click',function(){
    $('.error').hide();
   
    var trans_type = $('#trans_type :selected').val();
    var trans_type_content = $('#trans_type :selected').html();

    if(trans_type=='') { 
         $('#trans_type_error').css('display','block').html('Please Select Translation Type');
         return false;    
    }else{
        var url = site_url+'user/set_trans_type/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'trans_type_id':trans_type,'trans_type':trans_type_content},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#trans_typemodal').modal('hide');
                    $('#t_type').html(trans_type_content).show();
                }else{
                   $('#trans_type_error').css('display','block').html(res.msg); 
                }
            }
        });
    }    
});

$("#langfrom_confirm").on('click',function(){
    $('.error').hide();
   
    var lang_from = $('#lang_from :selected').val();
    var langfrom_content = $('#lang_from :selected').html();
    if(lang_from=='') { 
         $('#langFrom_error').css('display','block').html('Please Select Language From');
         return false;    
    }else{
        var url = site_url+'user/set_lang_from/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'lang_from_id':lang_from,'lang_from':langfrom_content},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#langfrom_modal').modal('hide');
                    $('#langfrom').html(langfrom_content).show();
                }else{
                   $('#langFrom_error').css('display','block').html(res.msg); 
                }
            }
        });
    }    
});

$("#langTo_confirm").on('click',function(){
    $('.error').hide();
   
    var lang_to = $('#lang_to :selected').val();
    var langto_content = $('#lang_to :selected').html();
    if(lang_to=='') { 
         $('#langTo_error').css('display','block').html('Please Select Language To');
         return false;    
    }else{
        var url = site_url+'user/set_lang_to/';
        $.ajax({
            url: url,
            dataType:'json',
            method:'post',
            data:{'lang_to_id':lang_to,'lang_to':langto_content},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#langto_modal').modal('hide');
                    $('#langto').html(langto_content).show();
                }else{
                   $('#langTo_error').css('display','block').html(res.msg); 
                }
            }
        });
    }    
});

 
$(document).on("change","#trans_doc",function (){

    $('.error').hide();
    var filetype = this.files[0].type;
    var filename = this.files[0].name;
    var validExtensions = ['docx','xls','xlsx','pdf']; //array of valid extensions
    var fileNameExt = filename.substr(filename.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1){
        $('#doc_err').addClass('text-danger').removeClass('text-success').show().html('Please upload pdf OR docx OR excel file only');    
       $('#uploadimgbtn').css('display','none');
    }else if(filetype.length>0 || filetype=='application/pdf' || filetype=='application/msword' || filetype=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'|| filetype=='application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
        
        console.log('a');
        $('#fileDiv, #msgSpan').hide();
        $('#removeDiv').show();
        $('#filenameSpan').show().html(filename);

        $('#uploadimgbtn').css('display','block');

        //filePreview(this);
    }else{
        $('#doc_err').addClass('text-danger').removeClass('text-success').show().html('Please upload pdf OR docx OR excel file only');    
        $('#uploadimgbtn').css('display','none');
    }

});


$(document).on('click','#uploadTransDocBtn',function(){
//$(document).on('submit','#docuploadform',function(){

    ds = $("#docuploadform");
    var url = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
           //ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#doc_err').show().html(res.msg);    

            if(res.success==true){
                ds[0].reset();
                $('#doc_err').removeClass('text-danger').addClass('text-success');
                setTimeout(function(){
                 $('#imagemodal').modal('hide'); 
                 //$('#pdesign').html($('#filenameSpan').html());
                }, 2000);

            }

        }
    });
});
//======translation=====


function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var timestamp = new Date().getTime();
        reader.onload = function (e) {
           d = new Date();
        $('#uploadImagePreview').hide();
        $('#imagePreview').html('<img src="'+e.target.result+'" />').show();
        var filnme = input.files[0].name;
        $('#fileDiv, #msgSpan').hide();
        $('#removeDiv').show();
        $('#filenameSpan').show().html(filnme);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#removeDiv').on('click',function(){
    $('#uploadImagePreview').show();
    $('#imagePreview').html('<img src="" />').hide();
    $('#fileDiv, #msgSpan').show();
    $('#removeDiv').hide();
    $('#filenameSpan').hide().html('');
    $("#trans_doc, .input_photoupload").val('');

    var url = site_url+'user/unlink_uploaded/';
    // $.ajax({
    //     url: url,
    //     dataType:'json',
    //     beforeSend:function(){
    //         ajaxindicatorstart();
    //     },
    //     success: function(res) {
    //         ajaxindicatorstop();
    //         if(res.success==true){
    //             $('#printingsidemodal').modal('hide');
    //             $('.error').hide();
    //         }else{
    //            $('#printside_err').css('display','block').html(res.msg); 
    //         }
    //     }
    // });

});
 
 
$(document).on("change",".input_photoupload, .input_fileupload",function (){

    $('.error').hide();
    if($(this).attr('id')=='graph_design_one'){
        $('#graph_design_two').val('');
    }else if($(this).attr('id')=='graph_design_two'){
        $('#graph_design_one').val('');
    }
    var filetype = this.files[0].type;

    var filename = this.files[0].name;
    var validExtensions = ['jpg','jpeg','png']; //array of valid extensions
    var fileNameExt = filename.substr(filename.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1){
        $('#design_err').addClass('text-danger').removeClass('text-success').show().html('Please upload pdf OR docx OR excel file only');    
        $('#uploadimgbtn').css('display','none');
    }else if(filetype=='image/jpg' || filetype=='image/jpeg'|| filetype=='image/png'){
        $('#uploadimgbtn').css('display','block');
        filePreview(this);
    }else{
        $('#design_err').addClass('text-danger').removeClass('text-success').show().html('Please upload jpg OR JPEG OR PNG file only');    
        $('#uploadimgbtn').css('display','none');    
    }
});



$('#uploadimgbtn').on('click',function(){
    //var graph_design_one = $('#graph_design_one').val().trim();
    var graph_design_two = $('#graph_design_two').val().trim();
    $('.error').hide();
    //if(graph_design_one=='' && graph_design_two==''){
    if(graph_design_two==''){
        $('#design_err').show();
        return false;
    }else{
        ds = $('#imageuploadform');
        var url = ds.attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
               ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                $('#design_err').show().html(res.msg);    

                if(res.success==true){
                    ds[0].reset();
                    $('#design_err').removeClass('text-danger').addClass('text-success');
                    setTimeout(function(){
                     $('#imagemodal').modal('hide'); 
                     $('#pdesign').html($('#filenameSpan').html());
                    }, 2000);

                }

            }
        });
    }
});


$('#copynumberconfirm').on('click',function(){
    var copy_number = $('#copy_number').val().trim();
    $('.error').hide();
    if(copy_number=='') { 
         $('#copy_err').css('display','block').html('Please Select Number of Copies');
         return false;    
    }else if(isNaN(copy_number) || copy_number<=0){
        $('#copy_err').css('display','block').html('Please Select Valid Number of Copies');
    }else{
        var url = site_url+'user/set_copynumber/'+copy_number;
        $.ajax({
            url: url,
            dataType:'json',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#copy_number_modal').modal('hide');
                    $('#pcopy').html(copy_number).show();
                }else{
                   $('#copy_err').css('display','block').html(res.msg); 
                }
                
            }
        });
    }
});


$('#saveprojectbtn').on('click',function(){
    var project_name = $('#project_name').val().trim();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').css('display','block');
        return false;
    }else{
        var current_url = $("#current_url").val();
        var url = site_url+'save_project';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'current_url':current_url,'continue':true},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#projectname_err').html(res.msg);
                    $('#projectname_modal').modal('hide');  
                    $('#project_name').val('');
                    window.location.href   = site_url+res.href;
                }else{
                    $('#projectname_err').html(res.msg).css('display','block');
                }
            }
        });
    }
});


$('#send_me_proposals').on('click',function(){
    var project_name = $('#project_name').val().trim();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').css('display','block');
        return false;
    }else{
        var current_url = $("#current_url").val();
        var url = site_url+'user/save_translation';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'current_url':current_url,'continue':true},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#projectname_err').html(res.msg);
                    $('#projectname_modal').modal('hide');
                    $('#project_name').val('');
                    $('#trans_success_modal').modal('show'); 
                    setTimeout(function(){
                     window.location.reload();
                    }, 2000);
                }else{
                    $('#projectname_err').html(res.msg).css('display','block');
                }
            }
        });
    }
});


$('#save_translation').on('click',function(){
    var project_name = $('#project_name').val().trim();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').removeClass('text-success').addClass('text-danger').css('display','block');
        return false;
    }else{
        var current_url = $("#current_url").val();       
        var url = site_url+'user/save_translation';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'current_url':current_url,'continue':'false'},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#projectname_err').html(res.msg).removeClass('text-danger').addClass('text-success').css('display','block');
                    setTimeout(function(){ 
                        $('#projectname_modal').modal('hide');  
                        $('#project_name').val('');                       
                        window.location.href   = site_url+'myorders';
                    }, 2000);
                }else{
                    $('#projectname_err').html(res.msg).removeClass('text-success').addClass('text-danger').css('display','block');
                }
            }
        });
    }
});


$('#saveproject').on('click',function(){
    var project_name = $('#project_name').val().trim();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').removeClass('text-success').addClass('text-danger').css('display','block');
        return false;
    }else{
        var current_url = $("#current_url").val();       
        var url = site_url+'save_project';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'current_url':current_url,'continue':'false'},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#projectname_err').html(res.msg).removeClass('text-danger').addClass('text-success').css('display','block');
                    setTimeout(function(){ 
                        $('#projectname_modal').modal('hide');  
                        $('#project_name').val('');                       
                        window.location.href   = site_url+'myorders';
                    }, 2000);
                }else{
                    $('#projectname_err').html(res.msg).removeClass('text-success').addClass('text-danger').css('display','block');
                }
            }
        });
    }
});

$('.savedOrderClick').on('click',function(){
    var oid = $(this).attr('oid');
    $.ajax({
        url: site_url+'user/continue_order_set',
        dataType:'json',
        data:{'order_id':oid},
        dataType: "json",
        method:'POST',
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            window.location.href = res.msg;
            //window.location.href= site_url+'continue_order'
            // if(res.success){
            //     $('#projectname_modal').modal('hide');  
            //     $('#project_name').val('');
            //     window.location.href   = site_url+res.href;
            // }else{
            //     $('#projectname_err').show().html(res.msg);
            // }
        }
    });    
});


$('.no_whitespace').on('keypress',function(e){
 if(e.which === 32){ return false; }
})


$(".pro_product_box").click(function(){
    $('#printry_error').hide();
    if($(this).hasClass('active')){
        $(".pro_product_box").removeClass("active");
        //$('#orderstep1').attr('href','javascript:void(0)');
        var printry = 0;
    }else{
        $(".pro_product_box").removeClass("active");
        $(this).addClass("active");
        $('#right_modal').modal('show');
        //$('#orderstep1').attr('href',site_url+'pickup_delivery');
        var printry = $(this).attr('printryid');
    }
    var vendor_charge = $(this).attr('vendor_charge');

    $('#selectedprintry').val(printry);
    ds = $(this);
    $.ajax({
        url: site_url+'user/select_printry/'+printry,
        dataType: "json",
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#printery_charge').html(vendor_charge);
            //window.location.reload();
        }
    });
});

//$('#orderstep1').on('click',function(){
$('.orderstep1').on('click',function(){
    $('#printry_error').hide();
    var selected = $('#selectedprintry').val();
    console.log('selected=>',selected);
    
    if(selected==0){
        $('#printry_error').css('display','block');
     }else{
        $('#printry_error').css('display','none');
        //var current_url = $('#current_url').val();
        $.ajax({
            url: site_url+'user/update_order_vendor/',
            dataType: "json",
            // method:'POST',
            // data:{'current_url':current_url},
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    window.location.href = res.msg;
                }else{
                   $('#printry_error').html(res.msg).css('display','block'); 
                }
                //$('#printery_charge').html(vendor_charge);
                
            }
        });
     }
     
});

//$('#orderstep2').on('click',function(){
$('.orderstep2').on('click',function(){
    var selected = $('#pick_delivery').val();

    if(selected==''){
        $('#pick_error').css('display','block');
    }else{
        $('#pick_error').hide();
        if(selected=='pickup'){
            var current_url = $('#current_url').val();
            $.ajax({
                url: site_url+'user/set_pickup_delivery/'+selected,
                dataType: "json",
                method:'POST',
                data:{'current_url':current_url},                
                beforeSend:function(){
                    ajaxindicatorstart();
                },
                success: function(res) {
                    ajaxindicatorstop();
                    window.location.href = site_url+'payment'
                }
            });
            
        }else{
            $('#delivery_fee_head').css('display','block');
            // var orderamt = $('#orderamt').val();
            // var delivery_limit = $('#delivery_min_limit').val();
            // if(parseFloat(orderamt)< parseFloat(delivery_limit)){
            //     $('#more_product').modal('show');
            // }else{
            //     $('#address1').modal('show');
            // }
            $('#more_product').modal('show');
        }
    }
});


$('.pickbtn').on('click',function(){
    $('#pick_error').hide();
    $('.pickbtn').removeClass('blue-btn').addClass('dark-btn')
    $(this).removeClass('dark-btn').addClass('blue-btn');
    var ds = $(this).attr('value');
    $('#pick_delivery').val(ds);
    //$('#orderstep2').attr('href','javascript:void(0);');
    $('.orderstep2').attr('href','javascript:void(0);');
    if($(this).attr('value')=='pickup'){
        $('#right_modal').modal('show');
    }else if($(this).attr('value')=='delivery'){
        var orderamt = $('#orderamt').val();
        var delivery_limit = $('#delivery_min_limit').val();
        if(parseFloat(orderamt)< parseFloat(delivery_limit)){
            $('#more_product').modal('show');
        }else{
            $('#address1').modal('show');
        }
    }
});

$('#cancelNewAddress').on('click',function(){
$('#address1').modal('show');
$('#address2').modal('hide');
});

$('#address_option_form').on('submit',function(e){
    e.preventDefault();

    
    if($('.address_option').is(':checked')) {
        var address_option = $('.address_option:checked').val();
        $('#address_option_err').hide();
        
        $.ajax({
            url: site_url+'user/set_address',
            dataType:'json',
            data:{'address_id':address_option},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                
                if(res.success){
                    $('#address_option_err').html(res.msg).css('display','block').addClass('text-success').removeClass('text-danger');
                    setTimeout(function(){ 
                        window.location.href=site_url+'payment';
                    },1000);
                   
                }else{
                    $('#address_option_err').html(res.msg).css('display','block').addClass('text-danger').removeClass('text-success');
                }                
            }
        });

    }else{
        $('#address_option_err').css('display','block');
        return false;
    }
});


$('#save_order_products').on('submit',function(e){
    e.preventDefault();
    var selected_pro = false;
    var prod_total = 0;
    var ds = $(this);

    $(".product" ).each(function(i,v) {
        if($(this).prop('checked')){
            selected_pro = true;
            prod_qty = $(this).parents('.more_product_box').find('.qty').val();
            prod_total += parseFloat($(this).attr('price'))*parseInt(prod_qty);
        }
    });

    var delivery_limit = $('#delivery_min_limit').val();
    var order_total = parseFloat($('#vendorTotal').attr('amt'))+parseFloat(prod_total);

    if(selected_pro){
        if(parseFloat(order_total) < parseFloat(delivery_limit)){
            $('#prod_err').css('display','block').html('Please select more products to reach minimum limit');
        }else{

            var url = ds.attr('action');
            var formData = new FormData(ds[0]);        
            $.ajax({
                url: url,
                method:'POST',
                data:formData,
                dataType: "json",
                processData: false,
                contentType: false,               
                beforeSend:function(){
                    //ajaxindicatorstart();
                },
                success: function(res) {
                    console.log(res);
                    ajaxindicatorstop();
                    if(res.success){
                        $('#address1').modal('show');
                        $('#more_product').modal('hide');
                    }else{
                        $('#prod_err').css('display','block').html(res.msg);
                    }
                }
            });
        }
    }else{
        $('#prod_err').css('display','block').html('Please select product first');
    }

});

function savetocart(){
    var cproject_name = $('#cproject_name').val().trim();
    if(cproject_name ==''){
        $('#cproject_name').focus();
        $('#cprojectname_err').css('display','block');
        return false;
    }else{
        var url = site_url+'user/saveincart/';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':cproject_name},
            method:'POST',
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#cprojectname_err').css('display','block').html('Project saved in cart').addClass('text-success').removeClass('text-danger');
                    setTimeout(function(){ 
                        $('#savetocart').modal('hide'); 
                    }, 1000);
                }else{
                    $('#cprojectname_err').css('display','block').html(res.msg).addClass('text-danger').removeClass('text-success');
                }
            }
        });
    }    
}


$(".printeryBox").click(function(){
    $('#printry_error').hide();
    if($(this).hasClass('active')){
        $(".printeryBox").removeClass("active");
        var printry = 0;
    }else{
        $(".printeryBox").removeClass("active");
        $(this).addClass("active");
        var printry = $(this).attr('printryid');
    }
    $('#selectedprintry').val(printry);
    ds = $(this);
    $.ajax({
        url: site_url+'user/select_printry/'+printry,
        dataType: "json",
        beforeSend:function(){
           ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
        }
    });
});

$('#proceedwithproducts').on('click',function(e){
    var selected = $('#selectedprintry').val();
    if(selected==0){
        $('#printry_error').css('display','block');
    }else{
        $('#printry_error').hide();
        window.location.href= site_url+'pickup_delivery';

    }
});

$('#promo_code_input').on('keyup',function(){
    var user_coupon = $('#promo_code_input').val().trim();
    //console.log($('#promo_code_input').prop('readonly'));
    if(user_coupon !='' && $('#promo_code_input').prop('readonly')==false){
        $.ajax({
        url: site_url+'user/apply_coupon',
        method:'post',
        data:{'user_coupon':user_coupon},
        dataType: "json",
        beforeSend:function(){
           ajaxindicatorstart();
        },
        success: function(res) {
            console.log(res);
            ajaxindicatorstop();
            if(res.success){
                 console.log('success');
                $('#promo_code_input').prop('readonly','readonly');
                $('#promocodeArea').addClass('valid').removeClass('invalid');
                $('#promo_code_msg').show().html(res.msg);
                $('#promo_code_err').hide();

                $('#couponDiv').css('display','block');
                $('#couponVal').html(res.amt+' KD');
                
                var vendorTotal = $('#vendorTotal').attr('amt');
                var totalamt = parseFloat(vendorTotal)-parseFloat(res.amt);

                $('#totalAmtDiv').html(totalamt+' KD');
            }else{
                $('#promocodeArea').addClass('invalid').removeClass('valid');                
                $('#promo_code_err').show().html(res.msg);

                $('#couponDiv').css('display','');
            }
        }
    });
    }
});

$('#localprintery').on('click',function(){
    var url = site_url+'set_service_type/normal';
    $.ajax({
        url: url,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#printery_modal').modal('show');
        }
    });
});


$('#new_order_address').on('submit',function(e){
    e.preventDefault();
    $('#error').css('display','none');

    if($('#order_addrress').val()==''){
        $('#order_addrress').focus();
        $('#o_add_err').css('display','block');
        return false;
    }

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            
            $('#addorder_error').html(res.msg).show();      
            if(res.success==true){
                ds[0].reset();
                $('#addorder_error').removeClass('text-danger').addClass('text-success');
                    setTimeout(function(){ 
                    window.location.href = site_url+'payment'
                });
            }else{
                $('#addorder_error').addClass('text-danger').removeClass('text-success');
            }
        }
    }); 
});

$('.more_product_img, .prodContent').on('click',function(){
    var checkbox = $(this).parents('.more_product_box').find('.prod_checkbox')[0];
    if($(checkbox).prop('checked')){
        $(checkbox).prop('checked',false);
        $(this).parents('.more_product_box').parent('li').removeClass('active');
    }else{
        $(checkbox).prop('checked',true);
        $(this).parents('.more_product_box').parent('li').addClass('active');
    } 
});

$(".prodMinus").click(function () {
    $(this).parents('.more_product_box').find(".qtyerror").css('display','none');
    var checkbox = $(this).parents('.more_product_box').find('.prod_checkbox')[0];

    //console.log('prod_price',prod_price);
    

    var $input = $(this).parent().find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 0 : count;
    $input.val(count);
    $input.change();

    if(count ==0 && $(checkbox).prop('checked')==true){
        $(checkbox).prop('checked',false);
        $(this).parents('.more_product_box').parent('li').removeClass('active');        
        var prod_total = $('#prod_total').attr('val');
        var prod_price = $(checkbox).attr('price');
        var new_prod_total = parseFloat(prod_total)-parseFloat(prod_price);
        $('#prod_total').attr('val',new_prod_total);
        $('#prod_total').html(new_prod_total+' KD');        
    }else if($(checkbox).prop('checked')==true){
        $(checkbox).prop('checked',true);
        $(this).parents('.more_product_box').parent('li').addClass('active');

        var prod_total = $('#prod_total').attr('val');
        var prod_price = $(checkbox).attr('price');
        var new_prod_total = parseFloat(prod_total)-parseFloat(prod_price);
        $('#prod_total').attr('val',new_prod_total);
        $('#prod_total').html(new_prod_total+' KD');   
    }





    return false;
});


$('.plusProd').on('click',function(){

    var ds = $(this);
    var checkbox = $(this).parents('.more_product_box').find('.prod_checkbox')[0];
    

    var pro_qty_input = $(this).parents('.more_product_box').find('.qty');
    var pro_qty = pro_qty_input.val();
    pro_qty++;
    pro_qty_input.val(pro_qty);
    var prod_id = $(this).parents('.more_product_box').find('.prod_checkbox').val();
    var err_div = $(this).parents('.more_product_box').find('.qtyerror');
    $.ajax({
        url: site_url+'user/check_prod_stock',
        type: "POST",
        data:{'prod_id':prod_id,'qty':pro_qty},
        dataType: "json",
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            //console.log('res',res);
            ajaxindicatorstop();
            var prod_total = $('#prod_total').attr('val');
            if(res.success==false){
                pro_qty--;
                pro_qty_input.val(pro_qty);             
                err_div.css('display','block').html(res.msg);
                $(this).parents('.more_product_box').parent('li').removeClass('active');

            }else{
                err_div.css('display','none');
                $(checkbox).prop('checked',true);
                ds.parents('.more_product_box').parent('li').addClass('active');

                var prod_price = $(checkbox).attr('price');
                var new_prod_total = parseFloat(prod_total)+parseFloat(prod_price);
                $('#prod_total').attr('val',new_prod_total);
                $('#prod_total').html(new_prod_total+' KD');

            }
        }
    });
});

$(function() {
    var rotation = 0;
    $("#hori").click(function() {
        rotation = (rotation -180) % 360;
        $("#imagePreview").css({'transform': 'rotate('+rotation+'deg)'});
        if(rotation==0 ||(rotation==-180)){
              $("imagePreview").css({'width': '300px', 'height': '400px'});
              $(".imagebg").css({'width': '300px', 'height': '400px'});
        }
        else{
            $(".imagebg").css({'width': '300px', 'height': '400px'});
        }
        $('#image_rotation').val(rotation);
        
        $.ajax({
            url: site_url+'user/set_image_rotation/',
            dataType: "json",
            method:'POST',
            data:{'image_rotation':rotation},
            beforeSend:function(){
                //ajaxindicatorstart();
            },
            success: function(res) {
            }
        });
    });
    var rotation1=90;
    $("#verti").click(function() {
        rotation1 = (rotation1 + 180) % 360;
        $("#imagePreview").css({'transform': 'rotate('+rotation1+'deg)'});
        
        if(rotation1 ==90 ||(rotation1==270)){
            $(".imagebg").css({'width': '300px', 'height': '420px'})
             $("#imagePreview").css({'width': '300px', 'height': '300px'})
        }else{
            $(".imagebg").css({'width': '300px', 'height': '420px'});
        }
        $('#image_rotation').val(rotation1);

        $.ajax({
            url: site_url+'user/set_image_rotation/',
            dataType: "json",
            method:'POST',
            data:{'image_rotation':rotation1},
            beforeSend:function(){
                //ajaxindicatorstart();
            },
            success: function(res) {
            }
        });
    });
});

$('#continue_note_order').on('click',function(){
    var note_id = $('#note_id').val();
    var project_name = $('#project_name').val().trim();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').css('display','block');
        return false;
    }else{
        var total_pages = $('#total_pages').val();

        var current_url = $("#current_url").val();
        var url = site_url+'user/save_note';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'note_id':note_id,'total_pages':total_pages,'current_url':current_url,'continue':true},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                //ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                console.log('res',res);
                if(res.success){
                    $('#projectname_err').html(res.msg);
                    $('#projectname_modal').modal('hide');  
                    $('#project_name').val('');
                    window.location.href   = site_url+res.href;
                }else{
                    $('#projectname_err').html(res.msg).css('display','block');
                }
            }
        });
    }
});

$('#saveNote').on('click',function(){
    var project_name = $('#project_name').val().trim();
    var note_id = $('#note_id').val();
    if(project_name ==''){
        $('#project_name').focus();
        $('#projectname_err').removeClass('text-success').addClass('text-danger').css('display','block');
        return false;
    }else{
        var current_url = $("#current_url").val();       
        var url = site_url+'user/save_note';
        $.ajax({
            url: url,
            dataType:'json',
            data:{'project_name':project_name,'note_id':note_id,'current_url':current_url,'continue':'false'},
            dataType: "json",
            method:'POST',
            beforeSend:function(){
                //ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success){
                    $('#projectname_err').html(res.msg).removeClass('text-danger').addClass('text-success').css('display','block');
                    setTimeout(function(){ 
                        $('#projectname_modal').modal('hide');  
                        $('#project_name').val('');                       
                        window.location.href   = site_url+'myorders';
                    }, 2000);
                }else{
                    $('#projectname_err').html(res.msg).removeClass('text-success').addClass('text-danger').css('display','block');
                }
            }
        });
    }
});

$('#set_notes_page_range').on('submit',function(e){
    e.preventDefault();
    $('.error').css('display','none');
    var ds = $(this);
    var url = $(this).attr('action');

    var pfrom = $('#page_from').val();
    var pto = $('#page_to').val();
    var option_check = false;
    var validate = true;
    $('.pages_radio').each(function(){
        if($(this).prop('checked')){  option_check = true; }
    });

    if(option_check==false){
        $('#note_page_error').html('Please select option').css('display','block');
        validate = false;
    }else if($('.pages_radio:checked').val()=='range'){
        if(pfrom.length==0  || isNaN(pfrom) || pfrom <=0){
            $('#pagefrom_err').html('Please enter pages from').css('display','block');
            validate =  false;            
        }else if(pto.length =='' || isNaN(pto) || pto <=0){
            $('#pageto_err').html('Please enter pages to').css('display','block');
            validate =  false;            
        }        
    }

    if(validate){
        var formData = new FormData(ds[0]);
        
        $.ajax({
            url: url,
            dataType:'json',
            data:formData,
            dataType: "json",
            method:'POST',
            processData: false,
            contentType: false,             
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                console.log('res',res);
                ajaxindicatorstop();
                if(res.success){
                    $('.error').css('display','none');
                    $('#page_range_modal').modal('hide'); 
                }else{
                    $('#note_page_error').html(res.msg).removeClass('text-success').addClass('text-danger').css('display','block');
                }
            }
        });        
    }

});

$('.pagerange').on('change',function(){
    if($(this).val() <0){
        $(this).val(0);
    }
});

$('.pages_radio').on('click',function(){
    $('#note_page_error').css('display','none');
    if($(this).prop('checked') && $(this).val()=='all'){
        $('#rangeDiv').css('display','none');
    }else if($(this).prop('checked') && $(this).val()=='range'){
        $('#rangeDiv').css('display','block');
    }
});