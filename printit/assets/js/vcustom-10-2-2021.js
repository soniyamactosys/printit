$('#total_branches').on('change',function(){
    var ds = $(this).val();
    $('#current_branches').val(ds);
    $('#branchesDiv').find('.newAppended').remove();

    if(ds>0){
        $('#branchesBtn').css('display','block').addClass('multisteps-form__progress-btn');
        $('#branchesPanel').css('display','block').addClass('multisteps-form__panel');
        var registerd_branches = $('#registerd_branches').val();
        if(parseInt(registerd_branches) <parseInt(ds)){
            var total = parseInt(ds)-parseInt(registerd_branches);
            registerd_branches++;
            $.ajax({
                url: site_url+'vendor/new_branch_form/'+registerd_branches+'/'+total,
                beforeSend:function(){
                    ajaxindicatorstart();
                },
                success: function(res) {
                    ajaxindicatorstop();
                    $('#branchesDiv').append(res);
                    company_address_multiple();
                }
            });
        }
    }else{
        $('#branchesBtn').css('display','none').removeClass('multisteps-form__progress-btn');
        $('#branchesPanel').css('display','none').removeClass('multisteps-form__panel');
    }
    reset_dom_elements();
});

$(document).on('submit','#update_branches',function(e){
    e.preventDefault();
    $('.error').css('display','none');           
    valid = branch_validations();
    console.log(valid);
    if(valid){
        ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            mimeType:"multipart/form-data",        
            processData: false,
            contentType: false,         
            beforeSend:function(){
               ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    swal('', "Details Updated Successfully", "success");
                  setTimeout(function() {
                   setActiveStep(2);   
                   setActivePanel(2);
                   ds[0].reset();
                  },500);
                }else{
                    $('#b_php_err').html(res.msg).css('display','block'); 
                    $('html,body').animate({
                        scrollTop: $("#b_php_err").offset().top
                    }, 1000);    
                }
            }
        });        
    }
});

$(document).on('submit','#update_bankdetails',function(e){
    e.preventDefault();
    $('.error').css('display','none');           
    valid = bank_validations();
    console.log(valid);
    if(valid){
        ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
               ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    swal('', "Details Updated Successfully", "success");
                  // setTimeout(function() {
                  // },500);
                }else{
                    $('#bank_php_err').html(res.msg).css('display','block'); 
                    $('html,body').animate({
                        scrollTop: $("#b_php_err").offset().top
                    }, 1000);    
                }
            }
        });        
    }
});


/*
$(document).on('submit','#add_branch',function(e){
    e.preventDefault();
    $('.error').hide();

    if($('#shop_name').val().trim()==''){
        $('#shop_name').focus();
        $('#shop_name_err').show();
        return false;
    }

    if($('#branch_contact').val().trim()==''){
        $('#branch_contact').focus();
        $('#branch_contact_err').show();
        return false;
    }

    if($('#po_box').val().trim()==''){
        $('#po_box').focus();
        $('#po_box_err').show();
        return false;
    }    
    if($('#block').val().trim()==''){
        $('#block').focus();
        $('#block_err').show();
        return false;
    }

    if($('#branch_address').val().trim()==''){
        $('#branch_address').focus();
        $('#address_err').show();
        return false;
    }
    //if($('#start_time').val()=='00:00:00' || $('#start_time').val()==''){
    if($('#start_time').val()==''){
        $('#start_time').focus();
        $('#start_time_err').show();
        return false;        
    }else{
        $('#start_time_err').hide();
    }


    //if($('#end_time').val()=='00:00:00' || $('#end_time').val()==''){
    if($('#end_time').val()==''){
        $('#end_time').focus();
        $('#end_time_err').show();
        return false;        
    }else{
        $('#end_time_err').hide();
    }    


    var service = false;
    $(".services" ).each(function(i,v) {
      if($(this).is(':checked')){
        service = true;
      }
    });

    if(service) {
        $('#service_err').hide();
    }else{
        $('#service_err').show();
        return false;
    }

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                setTimeout(function() {
                    swal('', "Added Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'vendor/branches';
                }, 2000);

            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});
*/
    
$(document).on('submit','#edit_branch',function(e){
    e.preventDefault();
    $('.error').hide(); 

    if($('#shop_name').val().trim()==''){
        $('#shop_name').focus();
        $('#shop_name_err').show();
        return false;
    }

    if($('#branch_contact').val().trim()==''){
        $('#branch_contact').focus();
        $('#branch_contact_err').show();
        return false;
    }

    if($('#po_box').val().trim()==''){
        $('#po_box').focus();
        $('#po_box_err').show();
        return false;
    }    
    if($('#block').val().trim()==''){
        $('#block').focus();
        $('#block_err').show();
        return false;
    }

    if($('#branch_address').val().trim()==''){
        $('#branch_address').focus();
        $('#address_err').show();
        return false;
    }
    console.log('st',$('#start_time').val());
    console.log('end',$('#end_time').val());
    //if($('#start_time').val()=='00:00:00' || $('#start_time').val()==''){
    if($('#start_time').val()==''){
        $('#start_time').focus();
        $('#start_time_err').show();
        return false;        
    }else{
        $('#start_time_err').hide();
    }


    //if($('#end_time').val()=='00:00:00' || $('#end_time').val()==''){
    if($('#end_time').val()==''){
        $('#end_time').focus();
        $('#end_time_err').show();
        return false;        
    }else{
        $('#end_time_err').hide();
    }    


    var service = false;
    $(".services" ).each(function(i,v) {
      if($(this).is(':checked')){
        service = true;
      }
    });

    console.log('services',service);
    if(service) {
        $('#service_err').hide();
    }else{
        $('#service_err').show();
        return false;
    }

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                setTimeout(function() {
                    swal('', "Updated Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'vendor/branches';
                }, 2000);

            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});


    
$(document).on('submit','#update_myprofile',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#fname').val().trim()==''){
        $('#fname').focus();
        $('#fname_err').show();
        return false;
    }
    if($('#lname').val().trim()==''){
        $('#lname').focus();
        $('#lname_err').show();
        return false;
    }  
    
    if($('#vendor_contact').val().trim()==''){
        $('#vendor_contact').focus();
        $('#vendor_contact_err').show();
        return false;
    }

    if($('.gender').is(':checked')) {
        $('#gender_err').hide();
    }else{
        $('#gender_err').show();
        $('.gender').focus();
        //$('.gander-area').focus();
        return false;
    }    

    if($('#shop_name').val().trim()==''){
        $('#shop_name').focus();
        $('#shop_name_err').show();
        return false;
    }

    if($('#branch_contact').val().trim()==''){
        $('#branch_contact').focus();
        $('#branch_contact_err').show();
        return false;
    }

    /*    if($('#po_box').val().trim()==''){
        $('#po_box').focus();
        $('#po_box_err').show();
        return false;
    }    
    if($('#block').val().trim()==''){
        $('#block').focus();
        $('#block_err').show();
        return false;
    }*/

    if($('#branch_address').val().trim()==''){
        $('#branch_address').focus();
        $('#address_err').show();
        return false;
    }

    //if($('#start_time').val()=='00:00:00' || $('#start_time').val()==''){
    if($('#start_time').val()==''){
        $('#start_time').focus();
        $('#start_time_err').show();
        return false;        
    }else{
        $('#start_time_err').hide();
    }


    //if($('#end_time').val()=='00:00:00' || $('#end_time').val()==''){
    if($('#end_time').val()==''){
        $('#end_time').focus();
        $('#end_time_err').show();
        return false;        
    }else{
        $('#end_time_err').hide();
    }    

    var service = false;
    $(".services" ).each(function(i,v) {
      if($(this).is(':checked')){
        service = true;
      }
    });

    if(service) {
        $('#service_err').hide();
    }else{
        $('#service_err').show();
        return false;
    }
 

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                setTimeout(function() {
                    swal('', "Updated Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'vendor';
                }, 2000);

            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});
//==========old functionality
/*
$('#branch_id').on('change',function(){
    
    $('.error').hide();

    var branch_id = $(this).val();
    if(branch_id==''){
        $('#optionsDiv').css('display','none');
        $('#services').html('<option value="">Please select</option>');
    }else{

        var url = $(this).attr('action');
        $.ajax({
            url: site_url+'vendor/get_branch_services/'+branch_id,

            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#service_err').html('Please select service');  
                    $('#services').html(res.msg);
                }else{
                    $('#service_err').html(res.msg).show();  
                    $('#services').html('<option value="">Please select</option>');     
                }
            }
        });        
    }
});

$('#services').on('change',function(){
    
    $('.error').hide();

    var services = $(this).val();
    if(services==''){
        $('#optionsDiv').css('display','none');
    }else{
        $('#optionsDiv').css('display','block');
        
        if($(this).val()==1){
            $('#printTypeDiv').css('display','flex');
            $('#papersizeDiv').css('display','flex');
            $('#colorDiv').css('display','none');
            $('#bindingDiv').css('display','none');
            $('#sidesDiv').css('display','none');
            $('#widthDiv').css('display','none');        
        }else if($(this).val()==2){
            $('#colorDiv').css('display','flex');
            $('#bindingDiv').css('display','flex');
            $('#papersizeDiv').css('display','flex');
            $('#printTypeDiv').css('display','none'); 
            $('#sidesDiv').css('display','none');
            $('#widthDiv').css('display','none');

        }else if($(this).val()==3 || $(this).val()==4){
            $('#colorDiv').css('display','none');
            $('#bindingDiv').css('display','none');
            $('#papersizeDiv').css('display','none');
            $('#printTypeDiv').css('display','none'); 
            $('#sidesDiv').css('display','none');
            $('#widthDiv').css('display','none');

        }else{
            $('#printTypeDiv').css('display','none');
        }            
    }
});


$('#print_type').on('change',function(){
    var selected = $('#print_type').find(":selected").text().toUpperCase();
    //var areEqual = selected.toUpperCase() === string2.toUpperCase();
    console.log(selected);
    if(selected==='Flyer'.toUpperCase()){
        $('#sidesDiv').css('display','flex');
        $('#papersizeDiv').css('display','flex');
        $('#widthDiv').css('display','none');
    }else if(selected==='Banner'.toUpperCase()){
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','flex');
        $('#papersizeDiv').css('display','none');
    }else{
        $('#colorDiv').css('display','none');
        $('#bindingDiv').css('display','none');
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');
    }
}) 
*/


$('#branch_id').on('change',function(){
    $('.error').hide();
    $('#tableDiv').css('display','none');    
    var branch_id = $(this).val();
    if(branch_id==''){
        $('#print_type').val(null).trigger('change');
        $('#services').val('').trigger('change');
        $('#service_div, #print_type_div').css('display','none');
    }else{
        $.ajax({
            url: site_url+'vendor/get_branch_services/'+branch_id,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                 $('#service_div').css('display','block');
                    $('#services').html(res.msg);
                }else{
                    $('#services').html('<option value="">Please select</option>');     
                }
            }
        });        
    }
});

$('#services').on('change',function(){
    
    $('.error').hide();
    $('#tableDiv').css('display','none');
    var branch = $('#branch_id :selected').val();
    var service = $(this).val();
    var service_type = $('#services :selected').text();
    // console.log('services',service);
    // console.log('branch',branch);
    // console.log('service_type',service_type);
    // console.log('print_type',print_type);
    $('#print_type').val('').trigger('change');

    if(service==''){
        $('#printTypeDiv').css('display','none');
    }else if(service!='' && service==1){
        $('#printTypeDiv').css('display','block');
    }else{
        $('#printTypeDiv').css('display','none');  
        get_price_table();
  
    }    

});

function get_price_table(){
    var branch = $('#branch_id :selected').val();
    var service = $('#services').val();
    var service_type = $('#services :selected').text();
    var print_type= $('#print_type :selected').val();
    $.ajax({
        url: site_url+'vendor/get_prices/',
        dataType: "json",
        type:'POST',
        data:{'branch':branch,'service':service,'print_type':print_type},
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
console.log(res);
// console.log(res.msg);
            if(res.success==true){
                $('#tableDiv').html(res.msg).css('display','block');
            }else{
                $('#tableDiv').css('display','none');
            }
        }
    });      
}

$('#print_type').on('change',function(){
    if($(this).val()==''){
         $('#tableDiv').css('display','none');
    }else{
        get_price_table();
    }
});


$(document).on('submit','#price_form',function(e){
    e.preventDefault();
    $('.error').hide();
    
    var service = $('#services :selected').val();

    if(service==4){
        copy_from = true;
        copy_to = true;
        newprice = true;

        if($('.c_from').length >0){
            $('.c_from').each(function(i,v){

                var dscopy_to = $(this).parents('.newtr').find('.c_to');
                var dsprice = $(this).parents('.newtr').find('.price');
                if($(this).val() =='' || $(this).val() <=0){
                    copy_from = false;
                    $(this).focus();
                    return false;
                }else if(dscopy_to.val() =='' || dscopy_to.val() <=0){
                    copy_to = false;
                    dscopy_to.focus();
                    return false;
                }else if(dsprice.val() =='' || dsprice.val() <=0){
                    newprice = false;
                    dsprice.focus();
                    return false;                    
                }
            });

            if(!copy_from || !copy_to || !newprice){
                return false;
            }
        }
    console.log('copy_from',copy_from);
    console.log('copy_to',copy_to);        
    }



    priceinput = false;
    $('.price').each(function(i,v){
        if($(this).val() !='' && $(this).val() >0){
            priceinput = true;
        }else{
            //$(this).css('border','red').focus();
            $(this).focus();
            return false;
        }
    });

    if(!priceinput){
        return false;
    }else{



        ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
                console.log('before');
                //ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    //$('#add_new').modal('hide');
                    swal('', res.msg, "success");
                    $('#common_err').html(res.msg).css('display','block').addClass('text-success').removeClass('text-danger');                      
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
                }else{
                    $('#common_err').html(res.msg).css('display','block').addClass('text-danger').removeClass('text-success');
                }
            }
        });
    }
});

    ///==print management============
    $(document).on('submit','#add_print_management',function(e){
        e.preventDefault();
        $('.error').hide();

        var branch = $('#branch_id :selected').val();
        var services = $('#services :selected').val();
        var print_type = $('#print_type :selected').val();
        var print_type_content = $('#print_type').find(":selected").text().toUpperCase();
       // console.log(print_type_content);
    //if(selected==='Flyer'.toUpperCase()        
        var paper_type = $('#paper_type :selected').val();
        var paper_size = $('#paper_size :selected').val();
        var color = $('#color :selected').val();
        var binding = $('#binding :selected').val();
        var sides = $('#sides :selected').val();
        var width = $('#width').val().trim();
        var avg_price = $('#avg_price').val().trim();


        if(branch==''){
            console.log('branch');
            $('#branch_id').focus();
            $('#branch_err').show();
            return false;
        }

        if(services==''){
            console.log('1');
            $('#services').focus();
            $('#service_err').show();
            return false;
        }
        if(services==1 && print_type==''){
            console.log('2');
            $('#print_type').focus();
            $('#printtype_err').show();
            return false;
        }

        if(paper_type==''){
            console.log('3');
            $('#paper_type').focus();
            $('#papertype_err').show();
            return false;
        }
        
        if(print_type!='4' && paper_size==''){
            console.log('4');
            $('#paper_size').focus();
            $('#papersize_err').show();
            return false;
        }

        if(services==2 && color==''){
            console.log('5');
            $('#color').focus();
            $('#color_err').show();
            return false;
        }
        if(services==2 && binding==''){
            console.log('6');
            $('#binding').focus();
            $('#binding_err').show();
            return false;
        }
        if(print_type_content==='flyer'.toUpperCase() && sides==''){
            console.log('7');
            $('#sides').focus();
            $('#sides_err').show();
            return false;
        }
        if(print_type=='4' && width==''){
            console.log('8');
            $('#width').focus();
            $('#width_err').show();
            return false;
        }        
        if(avg_price=='' || isNaN(avg_price) || parseFloat(avg_price)<=0){
            console.log('9');
            $('#avg_price').focus();
            $('#avgprice_err').show();
            return false;
        }

        ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
                console.log('before');
                //ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                    $('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }else{
                    $('#common_err').focus();       
                    $('#common_err').html(res.msg).show();      
                }
            }
        });
    });

$('.orderStatusBtn').on('click',function(){
    var status = $(this).attr('status');
    var oid= $(this).parents('.dropdown').attr('oid');
    $('#form_oid').val(oid);
    $('#form_status').val(status);
    $('#status_modal').modal('show');
});



$(document).on('click','#yes_status',function(){
    ds = $('#order_status_form');
    $('.error').css('display','none');
    var url = ds.attr('action');
    var formData = new FormData(ds[0]);

    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                $('#status_modal').modal('hide');
                swal('', "Status Updated Successfully", "success");
                setTimeout(function() {
                    location.reload();
                },1000);

            }else{
                $('#php_error').html(res.msg).css('display','block');       
            }
        }
    });

    //$('#order_status_form').submit();
});
