function profile_validations(){
    var ret = true;
    if($('#c_fname').val().trim()==''){
        $('#c_fname').focus();
        $('#c_fname_err').show();
        ret =  false;
    }
    if($('#c_lname').val().trim()==''){
        $('#c_lname').focus();
        $('#c_lname_err').show();
        ret =  false;
    }  
    
    if($('#c_shop_name').val().trim()==''){
        $('#c_shop_name').focus();
        $('#c_shop_name_err').show();
        ret =  false;
    }

    if($('#total_branches :selected').val().trim()==''){
        $('#total_branches').focus();
        $('#branch_total_err').show();
        ret =  false;
    }    

    if($('#reg_number').val().trim()==''){
        $('#reg_number').focus();
        $('#reg_number_err').show();
        ret =  false;
    } 

    if($('#c_contact').val().trim()==''){
        $('#c_contact').focus();
        $('#c_contact_err').show();
        ret =  false;
    } 

    if($('#c_worker_count :selected').val().trim()=='' || $('#c_worker_count :selected').val().trim()=='0'){
        $('#c_worker_count').focus();
        $('#c_worker_err').show();
        ret =  false;
    }

    if($('#c_address').val().trim()==''){
        $('#c_address').focus();
        $('#c_address_err').show();
        ret =  false;
    } 
    if($('#c_start_time').val().trim()==''){
        $('#c_start_time').focus();
        $('#c_start_time_err').show();
        ret =  false;
    }
    if($('#c_end_time').val().trim()==''){
        $('#c_end_time').focus();
        $('#c_end_time_err').show();
        ret =  false;
    }  

    var service = false;
    $(".c_services" ).each(function(i,v) {
      if($(this).is(':checked')){
        service = true;
      }
    });

    if(service) {
        $('#c_service_err').hide();
    }else{
        $('#c_service_err').show();
        //return false;
        ret =  false;
    }
    
    if($('#c_in_fname').val().trim()==''){
        $('#c_in_fname').focus();
        $('#c_in_fname_err').show();
        ret =  false;
    }
    if($('#c_in_lname').val().trim()==''){
        $('#c_in_lname').focus();
        $('#c_in_lname_err').show();
        ret =  false;
    }
    if($('#c_in_email').val().trim()==''){
        $('#c_in_email').focus();
        $('#c_in_email_err').show();
        ret =  false;
    }
    if($('#c_in_mobile').val().trim()==''){
        $('#c_in_mobile').focus();
        $('#c_in_mobile_err').show();
        ret =  false;
    }            
    return ret;
}


function branch_validations(){
    var ret = true;
    $(".branchSubDiv" ).each(function(i,v) {
      $(this).find('.error').css('display','none');
      var name = $(this).find('.branch_name');
      var contact = $(this).find('.b_contact');
      var b_worker_count = $(this).find('.b_worker_count :selected').val();
      var worker_count = $(this).find('.b_worker_count');
      var b_address = $(this).find('.b_address');
      var b_start_time = $(this).find('.b_start_time');
      var b_end_time = $(this).find('.b_end_time');
      var b_services = $(this).find('.b_services');
      var b_in_fname = $(this).find('.b_in_fname');
      var b_in_lname = $(this).find('.b_in_lname');
      var b_in_email = $(this).find('.b_in_email');
      var b_in_mobile = $(this).find('.b_in_mobile');


      if(name.val().trim()==''){
        $(this).find('.branch_name_err').css('display','block');
        name.focus();
        ret = false;
        return false;
      }

      if(contact.val().trim()==''){
        $(this).find('.b_contact_err').css('display','block');
        contact.focus();
        ret = false;
        return false;
      }

      if(b_worker_count=='' || b_worker_count==0){
        $(this).find('.b_worker_err').css('display','block');
        worker_count.focus();
        ret = false;
        return false;
      }

      if(b_address.val().trim()==''){
        $(this).find('.b_address_err').css('display','block');
        b_address.focus();
        ret = false;
        return false;
      }

      if(b_start_time.val().trim()==''){
        $(this).find('.b_start_time_err').css('display','block');
        b_start_time.focus();
        ret = false;
        return false;
      }

      if(b_end_time.val().trim()==''){
        $(this).find('.b_end_time_err').css('display','block');
        b_end_time.focus();
        ret = false;
        return false;
      }


      var service = false;
        b_services.each(function(i,v) {
        if($(this).is(':checked')){
          service = true;
        }
      });

      if(service){         
        $(this).find('.b_service_err').css('display','none');
      }else{
        $(this).find('.b_service_err').css('display','block');
        ret =  false;
        return false;
      }


      if(b_in_fname.val().trim()==''){
        $(this).find('.b_in_fname_err').css('display','block');
        b_in_fname.focus();
        ret = false;
        return false;
      }


      if(b_in_lname.val().trim()==''){
        $(this).find('.b_in_lname_err').css('display','block');
        b_in_lname.focus();
        ret = false;
        return false;
      }


      if(b_in_email.val().trim()==''){
        $(this).find('.b_in_email_err').css('display','block');
        b_in_email.focus();
        ret = false;
        return false;
      }

      if(b_in_mobile.val().trim()==''){
        $(this).find('.b_in_mobile_err').css('display','block');
        b_in_mobile.focus();
        ret = false;
        return false;
      }      

    });   
    return ret;
}

function bank_validations(){
  var ret = true;
  if($('#acc_holder').val().trim()==''){
      $('#acc_holder').focus();
      $('#acc_holder_err').show();
      ret =  false;
  }
  if($('#bank_name').val().trim()==''){
      $('#bank_name').focus();
      $('#bank_name_err').show();
      ret =  false;
  }  
  
  if($('#branch_name').val().trim()==''){
      $('#branch_name').focus();
      $('#branch_err').show();
      ret =  false;
  }     
  if($('#acc_number').val().trim()==''){
      $('#acc_number').focus();
      $('#acc_no_err').show();
      ret =  false;
  }    
  if($('#ifsc_code').val().trim()==''){
      $('#ifsc_code').focus();
      $('#ifsc_err').show();
      ret =  false;
  }
  return ret;
}
   //DOM elements
DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next'
};





function reset_dom_elements(){
  // reset DOM elements
  DOMstrings = {
    stepsBtnClass: 'multisteps-form__progress-btn',
    stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
    stepsBar: document.querySelector('.multisteps-form__progress'),
    stepsForm: document.querySelector('.multisteps-form__form'),
    stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
    stepFormPanelClass: 'multisteps-form__panel',
    stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
    stepPrevBtnClass: 'js-btn-prev',
    stepNextBtnClass: 'js-btn-next'
  };  
}


//remove class from a set of items
const removeClasses = (elemSet, className) => {
  
  elemSet.forEach(elem => {
    
    elem.classList.remove(className);
    
  });
  
};

//return exect parent node of the element
const findParent = (elem, parentClass) => {
  
  let currentNode = elem;

  while(! (currentNode.classList.contains(parentClass))) {
    currentNode = currentNode.parentNode;
  }
  
  return currentNode;
  
};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = (activeStepNum) => {
  
  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');
  
  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {
    
    if(index <= (activeStepNum) ) {
      elem.classList.add('js-active');
    }
    
  });
};

//get active panel
const getActivePanel = () => {
  
  let activePanel;
  
  DOMstrings.stepFormPanels.forEach(elem => {
    
    if(elem.classList.contains('js-active')) {
      
      activePanel = elem;
      
    }
    
  });
  
  return activePanel;
                                    
};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {
  
  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');
  
  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if(index === (activePanelNum)) {
      
      elem.classList.add('js-active');
   
      setFormHeight(elem);
      
    }
  })
  
};

//set form height equal to current panel height
const formHeight = (activePanel) => {
  
  const activePanelHeight = activePanel.offsetHeight;
  
  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;
  
};

const setFormHeight = () => {
  const activePanel = getActivePanel();

  formHeight(activePanel);
}

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {
  
  //check if click target is a step button
  const eventTarget = e.target;
  
  if(!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }
  
  //get active button step number
  const activeStep = getActiveStep(eventTarget);
  
  //set all steps before clicked (and clicked too) to active

  var validate = false;
  if(activeStep==0){
    validate = true;
  }  
  if(activeStep==1){
    validate = profile_validations();
  }

  if(activeStep==2){
    validate = branch_validations();
  }

  if(validate){
    setActiveStep(activeStep);
    setActivePanel(activeStep);  
  }
  // setActiveStep(activeStep);
  
  //open active panel
  // setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK
DOMstrings.stepsForm.addEventListener('click', e => {
  
  const eventTarget = e.target;
  
  //check if we clicked on `PREV` or NEXT` buttons
  if(! ( (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) || (eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)) ) ) 
  {
    return;
  }
  
  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);
  
  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);
  
  //set active step and active panel onclick
  if(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;
  
  } else {
    
    activePanelNum++;
  
  }
  
  var validate = false;
  if(activePanelNum==0){
    validate = true;
  }    
  if(activePanelNum==1){
    validate = profile_validations();
  }

  if(activePanelNum==2){
    validate = branch_validations();
  }

  if(validate){
    setActiveStep(activePanelNum);
    setActivePanel(activePanelNum);  
  }
 
});


//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);

//changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

const setAnimationType = (newType) => {
  DOMstrings.stepFormPanels.forEach(elem => {
    elem.dataset.animation = newType;
  })
};

//selector onchange - changing animation
// const animationSelect = document.querySelector('.pick-animation__select');

// animationSelect.addEventListener('change', () => {
//   const newAnimationType = animationSelect.value;
  
//   setAnimationType(newAnimationType);
// });

$(document).on('submit','#update_company_profile',function(e){
    e.preventDefault();
    $('.error').hide();
    if(profile_validations()){
        ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            mimeType:"multipart/form-data",        
            processData: false,
            contentType: false,         
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                if(res.success==true){
                  swal('', "Details Updated Successfully", "success");
                  setTimeout(function() {
                   setActiveStep(1);   
                   setActivePanel(1);
                  },500);

                }else{
                    $('#comp_form_error').html(res.msg).show();  
                    $('html, body').animate({
                        scrollTop: $("#comp_form_error").offset().top
                    }, 2000);
                }
            }
        });
    }
});