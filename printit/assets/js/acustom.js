
$(document).on('click','.cs_status',function(){
   
  var cid = $(this).parents('tr').attr('cid');
 
  var status = $(this).attr('st');
  console.log('cid',cid);
  console.log('status',status);
  $('#cs_status_form').find('#cid').val(cid);
  $('#cs_status_form').find('#status').val(status);

  $('#cs_status_modal').modal('show');
});
$(document).on('submit','#cs_status_form',function(e){
    e.preventDefault();

    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            
            ajaxindicatorstop();
            $('#cs_status_modal').modal('hide');
                swal('', "Done", "success");
            setTimeout(function() {
                location.reload();
            }, 2000);
        }
    });
});
$(document).on('click','.ps_status',function(){
   
  var note_id = $(this).parents('tr').attr('note_id');
 
  var status = $(this).attr('st');
  console.log('note_id',note_id);
  console.log('status',status);
  $('#ps_status_form').find('#note_id').val(note_id);
  $('#ps_status_form').find('#status').val(status);

  $('#ps_status_modal').modal('show');
});
$(document).on('submit','#ps_status_form',function(e){
    e.preventDefault();

    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            
            ajaxindicatorstop();
            $('#ps_status_modal').modal('hide');
                swal('', "Done", "success");
            setTimeout(function() {
                location.reload();
            }, 2000);
        }
    });
});
$(document).on('click','.ecs_status_new',function(){
  
   var cid = $(this).parents('tr').attr('cid');
    var status = $(this).attr('st');
  
  $('#ecs_status_form').find('#ecid').val(cid);
  $('#ecs_status_form').find('#estatus').val(status);
 
  $('#ecs_status_modal').modal('show');
});

$(document).on('submit','#ecs_status_form',function(e){
    e.preventDefault();

    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            
            ajaxindicatorstop();
            $('#ecs_status_modal').modal('hide');
                swal('', "Done", "success");
            setTimeout(function() {
                location.reload();
            }, 2000);
        }
    });
});

$(document).on('click','.eps_status',function(){
   
  var note_id = $(this).parents('tr').attr('note_id');
 
  var status = $(this).attr('st');
  console.log('note_id',note_id);
  console.log('status',status);
  $('#eps_status_form').find('#enote_id').val(note_id);
  $('#eps_status_form').find('#epstatus').val(status);

  $('#eps_status_modal').modal('show');
});
$(document).on('submit','#eps_status_form',function(e){
    e.preventDefault();
    $('#eps_status_modal').modal('hide');
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            
            ajaxindicatorstop();
            $('#ecs_status_modal').modal('hide');
                swal('', "Done", "success");
            setTimeout(function() {
                location.reload();
            }, 2000);
        }
    });
});


$(document).on('submit','#update_service_form',function(e){
    e.preventDefault();
    $('.error').hide();
     chargesinput = false;
    $('.charges').each(function(i,v){
        if($(this).val() !='' && $(this).val() >0){
            chargesinput = true;
        }else{
            //$(this).css('border','red').focus();
            $(this).focus();
            return false;
        }
    });
      if(!chargesinput){
        return false;
    }else{
          ds = $(this);
        var url = $(this).attr('action');
        var formData = new FormData(ds[0]);
        $.ajax({
            url: url,
            type: "POST",
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,         
            beforeSend:function(){
                console.log('before');
                //ajaxindicatorstart();
            },
            success: function(res) {
                //console.log(res);
                ajaxindicatorstop();
                if(res.success==true){
                    //$('#add_new').modal('hide');
                    swal('', res.msg, "success");
                    $('#common_err').html(res.msg).css('display','block').addClass('text-success').removeClass('text-danger');                      
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
                }else{
                    $('#common_err').html(res.msg).css('display','block').addClass('text-danger').removeClass('text-success');
                }
            }
        });
    }
});


$(document).on('click',"#allcustomecheck",function () {
    $(".allcustome").prop('checked', $(this).prop('checked'));
});      
$(document).on('click','.csall_status',function(){
    var my_ids = [];
   $(".allcustome").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    if(my_ids.length==0){
         $('#allcid_err').show();
    }else{
        
        my_ids.join();
        var status = $(this).attr('st');
        $('#csall_status_form').find('#allcid').val(my_ids);
        $('#csall_status_form').find('#status').val(status);
        
        $('#csall_status_form').modal('show');
    }
});    
$(document).on('submit','#csall_status_forms',function(e){
    e.preventDefault();
     if($('#allcid').val().trim()==''){
        $('#allcid').focus();
        $('#allcid_err').show();
         $('#csall_status_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        dataType:'json',
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#csall_status_form').modal('hide');
            swal('', "Done", "success");
            if(res.success){
                admin_get_price_table();
            }else{
                $('#allcid_err').show().html(res.msg);                
            }

        }
    });
});
$(document).on('click','.ecsall_status_new',function(){
    var my_ids = [];
   $(".allcustome").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    
    if(my_ids.length==0){
         $('#allcid_err').show();
    }else{
        
        my_ids.join();
        var status = $(this).attr('st');
        $('#csexpall_status_form').find('#allexpcid').val(my_ids);
        $('#csexpall_status_form').find('#status').val(status);
        $('#csexpall_status_form').modal('show');
    }
}); 
$(document).on('submit','#csexpall_status_forms',function(e){
    e.preventDefault();
     if($('#allexpcid').val().trim()==''){
        $('#allexpcid').focus();
        $('#allcid_err').show();
         $('#csexpall_status_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        dataType:'json',
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#csexpall_status_form').modal('hide');
            if(res.success){
                swal('', "Done", "success");
              admin_get_price_table();
            }else{
                $('#allcid_err').show().html(res.msg);
            }
        }
    });
});
$(document).on('click',"#allnotecheck",function () {
    $(".allnote").prop('checked', $(this).prop('checked'));
});  
$(document).on('click','.psall_status',function(){
    var my_ids = [];
   $(".allnote").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    if(my_ids.length==0){
       $('#allpscid_err').show(); 
    }else{
        my_ids.join();
        var status = $(this).attr('st');
        
        $('#psall_status_form').find('#allpscid').val(my_ids);
        $('#psall_status_form').find('#status').val(status);
        
        $('#psall_status_form').modal('show');        
    }
}); 
$(document).on('submit','#psall_status_forms',function(e){
    e.preventDefault();
     if($('#allpscid').val().trim()==''){
        $('#allpscid').focus();
        $('#allpscid_err').show();
         $('#psall_status_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        dataType:'json',
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#psall_status_form').modal('hide');
            swal('', "Done", "success");
            if(res.success){
                admin_get_price_table();
            }else{
                $('#allpscid_err').show().msg(res.html);
            }

        }
    });
});
$(document).on('click','.pssall_status_new',function(){
    var my_ids = [];
    $(".allnote").each(function () {
        if($(this).prop('checked')){
            my_ids.push($(this).val());
        }
    });
    if(my_ids.length==0){
         $('#allpscid_err').show();
    }else{
        $('#allpscid_err').css('display','none');

        my_ids.join();
        var status = $(this).attr('st');
        $('#psall_status_new_form').find('#allexppscid').val(my_ids);
        $('#psall_status_new_form').find('#status').val(status);
        
        $('#psall_status_new_form').modal('show');        
    }

}); 
$(document).on('submit','#psall_status_new_forms',function(e){
    e.preventDefault();
     if($('#allexppscid').val().trim()==''){
        $('#allexppscid').focus();
        $('#allpscid_err').show();
         $('#psall_status_new_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
           // console.log(res);
            ajaxindicatorstop();
            $('#psall_status_new_form').modal('hide');
                swal('', "Done", "success");
                admin_get_price_table();
        }
    });
});

$(document).on('click',"#allquickcheck",function () {
    $(".allquick").prop('checked', $(this).prop('checked'));
    var my_ids = [];
    $(".allquick").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    my_ids.join();
    var status = $(this).attr('st');
    $('#qall_status_form').find('#allqid').val(my_ids);
}); 
$(document).on('click','.qall_status',function(){
    var my_ids = [];
   $(".allquick").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    if(my_ids.length==0){
        $('#allqid_err').show();
    }else{
        $('#allqid_err').css('display','none');

        my_ids.join();
        var status = $(this).attr('st');
        $('#qall_status_form').find('#allqid').val(my_ids);
        $('#qall_status_form').find('#status').val(status);
        $('#qall_status_form').modal('show');
    }
  
});    
$(document).on('submit','#qall_status_forms',function(e){
    e.preventDefault();
     if($('#allqid').val().trim()==''){
        $('#allqid').focus();
        $('#allqid_err').show();
        $('#qall_status_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        dataType:'json',
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#qall_status_form').modal('hide');
            swal('', "Done", "success");
            if(res.success){
                admin_get_price_table();
            }else{
                $('#allqid_err').show().html(res.msg);
            }
        }
    });
});
$(document).on('click','.qall_status_new',function(){
    var my_ids = [];
   $(".allquick").each(function () {
     if($(this).prop('checked')){
         my_ids.push($(this).val());
     }
    });
    
    if(my_ids.length==0){
        $('#allqid_err').show();
    }else{
        $('#allqid_err').css('display','none');
        my_ids.join();
        var status = $(this).attr('st');
        $("#qeall_status_forms").find('#allqid').val(my_ids);
        $("#qeall_status_forms").find('#status').val(status);
        $('#qeall_status_form').modal('show');
    }
});    
$(document).on('submit','#qeall_status_forms',function(e){
    e.preventDefault();
        if($('#allquickcheck').val().trim()==''){
        $('#allquickcheck').focus();
        $('#allqid_err').show();
         $('#qeall_status_form').modal('hide');
        return false;
        
    }
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        dataType:'json',
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#qeall_status_form').modal('hide');
            
            if(res.success){
                swal('', "Done", "success");
                admin_get_price_table();             
            }else{
                $('#allqid_err').show().html(res.msg);
            }

        }
    });
});