/*
$(document).on('submit','#add_paper_typee',function(e){
          console.log('f');
        e.preventDefault();
        alert('fff');
});*/

//============view branch price list===================//
$('#ad_services').on('change',function(){
    
    $('.error').hide();
    $('#colorDiv').css('display','none');
    $('#printTypeDiv').css('display','none');
    $('#ad_tableDiv').css('display','none');
    var branch = $('#branch_id').val();
    var service = $(this).val();
    var service_type = $('#services :selected').text();

    $('#ad_print_type').val('').trigger('change');

    if(service!='' && service==1){
        $('#printTypeDiv').css('display','block');
    }else if(service!='' && service==4){
        $('#colorDiv').css('display','block');
        //admin_get_price_table();
    }else{
        admin_get_price_table();
    }    

});

$('#ad_print_type').on('change',function(){
    if($(this).val()==''){
         $('#ad_tableDiv').css('display','none');
    }else{
        admin_get_price_table();
    }
});
//today notes
$('#ad_color_type').on('change',function(){
    if($(this).val()==''){
         $('#ad_tableDiv').css('display','none');
    }else{
        admin_get_price_table();
    }
});

function admin_get_price_table(){
    var branch = $('#branch_id').val();
    var service = $('#ad_services').val();
    var service_type = $('#ad_services :selected').text();
    var print_type= $('#ad_print_type :selected').val();
    var color_type= $('#ad_color_type :selected').val();
    $.ajax({
        url: site_url+'admin/get_prices/',
        dataType: "json",
        type:'POST',
        data:{'branch':branch,'service':service,'print_type':print_type,'color_type':color_type},
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                $('#ad_tableDiv').html(res.msg).css('display','block');
            }else{
                $('#ad_tableDiv').css('display','none');
            }
        }
    });      
}


//============view branch price list===================//


$(document).on("change",".image",function (){

    $('.error').hide();
    
    var filetype = this.files[0].type;
    if(filetype=='image/jpg' || filetype=='image/jpeg'|| filetype=='image/png'){
        //$('#uploadimgbtn').css('display','block');
    }else{
        $('#img_err').show().html('Please upload jpg OR JPEG OR PNG file only');    
    }
});

$(document).on('submit','#add_paper_type',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
        
    }

    if($('#pt_description').val().trim()==''){
        $('#pt_description').focus();
        $('#desc_err').show();
        return false;
    }

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
    	url: url,
    	type: "POST",
    	data:formData,
    	dataType: "json",
        processData: false,
        contentType: false,			
    	beforeSend:function(){
    		ajaxindicatorstart();
    	},
    	success: function(res) {
    		ajaxindicatorstop();
    		if(res.success==true){
    			$('#add_new_paper').modal('hide');
                swal('', "Added Successfully", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
    		}else{
    	        $('#name_err').html(res.msg).show();	    
    		}
    	}
    });
});

$(document).on('submit','#add_print_type',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
        
    }
    
    if($('#image').val()==''){
        $('#img_err').show().html('Please upload Image');    
        return false;
    }

    if($('#pt_description').val().trim()==''){
        $('#pt_description').focus();
        $('#desc_err').show();
        return false;
    }

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
    	url: url,
    	type: "POST",
    	data:formData,
    	dataType: "json",
        processData: false,
        contentType: false,			
    	beforeSend:function(){
    		ajaxindicatorstart();
    	},
    	success: function(res) {
    		ajaxindicatorstop();
    		if(res.success==true){
    			$('#add_new_print').modal('hide');
                swal('', "Added Successfully", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
    		}else{
    	        $('#name_err').html(res.msg).show();	    
    		}
    	}
    });
});


$('.edit_paper_modal').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: site_url+'admin/edit_paperType_modal/'+id,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#edit_paper_modal').find('.modal-content').html(res);
            $('#edit_paper_modal').modal('show');
        }
    });
});

$('.edit_service_modal').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: site_url+'admin/edit_service_modal/'+id,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#edit_service_modal').find('.modal-content').html(res);
            $('#edit_service_modal').modal('show');
        }
    });
});


$(document).on('submit','#edit_service',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#edit_name').val().trim()==''){
        $('#edit_name').focus();
        $('#edit_name_err').show();
        return false;
    }

    if($('#edit_description').val().trim()==''){
        $('#edit_description').focus();
        $('#edit_desc_err').show();
        return false;
    }
    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success){
                $('#edit_service_modal').modal('hide');
                swal('', "Updated Successfully", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }else{
                $('#edit_name_err').html(res.msg).show();       
            }
        }
    });
});

    $(document).on('submit','#edit_paper_type',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#edit_name').val().trim()==''){
            $('#edit_name').focus();
            $('#edit_name_err').show();
            return false;
            
        }
        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success){
    				$('#edit_paper_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_name_err').html(res.msg).show();	    
				}
			}
		});
     
    });

    $(document).on('submit','#add_paper_size',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#name').val().trim()==''){
            $('#name').focus();
            $('#name_err').show();
            return false;
            
        }
        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new_paper').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#name_err').html(res.msg).show();	    
				}
			}
		});
      });

    $(document).on('submit','#edit_paper_size',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#edit_name').val().trim()==''){
            $('#edit_name').focus();
            $('#edit_name_err').show();
            return false;
            
        }
        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success){
    				$('#edit_paper_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_name_err').html(res.msg).show();	    
				}
			}
		});
     
    });    
    
    
    //$('#amount').on('change',function(){});
    
    //===============ADD COUPON ==========
    $(document).on('submit','#add_coupon',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#code').val().trim()==''){
            $('#code').focus();
            $('#code_err').show();
            return false;
        }
        var users = $('#users').val().trim();
        if(users=='' || parseInt(users)<=0 || isNaN(users) || !isInt_number(users)){
            $('#users').focus();
            $('#user_err').show();
            return false;
        }

        if($('#start_date').val().trim()==''){
            $('#start_date').focus();
            $('#start_date_err').show();
            return false;
        }
        
        if($('#end_date').val().trim()==''){
            $('#end_date').focus();
            $('#end_date_err').show();
            return false;
        }

        var coupon_type = $('#coupon_type :selected').val().trim();
        
        if(coupon_type==''){
            $('#coupon_type').focus();
            $('#coupon_type_err').show();
            return false;
        }else
        if(coupon_type==1){
            $('#amountDiv_percent').show();
            $('#amountDiv_amt').hide();
        }else
        if(coupon_type==0){
            $('#amountDiv_amt').show();
            $('#amountDiv_percent').hide();
        }

        if(coupon_type==1){
            var amount_per = $('#coupon_percent').val().trim();
            if(amount_per=='' || isNaN(amount_per) || parseFloat(amount_per)<=0 || parseFloat(amount_per)>90){
                $('#coupon_percent').focus();
                $('#per_amount_err').show();
                return false;
            }  
        }else if(coupon_type==0){
            var amount = $('#amount').val().trim();
            if(amount=='' || isNaN(amount) || parseFloat(amount)<=0){
                $('#amount').focus();
                $('#amount_err').show();
                return false;
            }  
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#code_err').html(res.msg).show();	    
				}
			}
		});
    });
 
    $('.edit_coupon_modal').on('click',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var code = $(this).parents('tr').find('.code').html();
        var users = $(this).parents('tr').find('.users').html();
        var st_date = $(this).parents('tr').find('.st_date').html();
        var end_date = $(this).parents('tr').find('.end_date').html();
        var amount = $(this).parents('tr').find('.amount').html();
        var type = $(this).data('type');
        
        $('#edit_coupon_modal').find('#edit_coupon_type').val(type).trigger('change');
        $('#edit_coupon_modal').find('#id').val(id);
        $('#edit_coupon_modal').find('#edit_code').val(code);
        $('#edit_coupon_modal').find('#edit_users').val(users);
        $('#edit_coupon_modal').find('#edit_start_date').val(st_date).trigger('change');
        $('#edit_coupon_modal').find('#edit_end_date').val(end_date).trigger('change');

        if(type==1){
            $('#e_amountDiv_amt').hide();
            $('#e_amountDiv_percent').show();
            $('#edit_coupon_modal').find('#e_amount').val('');
            $('#edit_coupon_modal').find('#e_coupon_percent').val(amount);
        }else{
            $('#e_amountDiv_percent').hide();
            $('#e_amountDiv_amt').show();
            $('#edit_coupon_modal').find('#e_amount').val(amount);
            $('#edit_coupon_modal').find('#e_coupon_percent').val('');
        }
        
        $('#edit_coupon_modal').modal('show');
    });

    $(document).on('submit','#edit_coupon',function(e){
        e.preventDefault();
        $('.error').hide();
                        
        if($('#edit_code').val().trim()==''){
            $('#edit_code').focus();
            $('#edit_code_err').show();
            return false;
        }
        
        var users = $('#edit_users').val().trim();
        if(users=='' || parseInt(users)<=0 || isNaN(users) || !isInt_number(users)){
            $('#edit_users').focus();
            $('#edit_user_err').show();
            return false;
        }

        if($('#edit_start_date').val().trim()==''){
            $('#edit_start_date').focus();
            $('#edit_start_date_err').show();
            return false;
        }
        
        if($('#edit_end_date').val().trim()==''){
            $('#edit_end_date').focus();
            $('#edit_end_date_err').show();
            return false;
        }
        var coupon_type = $('#edit_coupon_type :selected').val();
        if(coupon_type==''){
            $('#edit_coupon_type_err').show();
            $('#edit_coupon_type').focus();
            validate = false;
            return false;
        }else
        if(coupon_type==1){
            console.log('1');
            $('#e_amountDiv_percent').show();
            $('#e_amountDiv_amt').hide();
            var amount_per = $('#e_coupon_percent').val().trim();
            if(amount_per=='' || isNaN(amount_per) || parseFloat(amount_per)<=0 || parseFloat(amount_per)>90){
                $('#e_coupon_percent').focus();
                $('#e_per_amount_err').show();
            validate = false;
                return false;
            }          
        }else
        if(coupon_type==0){
            console.log('2');
            $('#e_amountDiv_amt').show();
            $('#e_amountDiv_percent').hide();
            var amount = $('#e_amount').val().trim();
            if(amount=='' || isNaN(amount) || parseFloat(amount)<=0){
                $('#e_amount').focus();
                $('#e_amount_err').show();
            validate = false;
                return false;
            }          
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#edit_coupon_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_code').focus();	    
			        $('#edit_code_err').html(res.msg).show();	    
				}
			}
		});
    });
   


//=====================Add Extra Copy================//

    $(document).on('submit','#add_extracopy',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#copy').val().trim()=='' || isNaN($('#copy').val()) || parseFloat($('#copy').val())<=0 || $('#copy').val() % 1 != 0){
            $('#copy').focus();
            $('#copy_err').show();
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#copy_err').html(res.msg).show();	    
				}
			}
		});
    });
 
    $('.edit_copy_modal').on('click',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var copy = $(this).data('type');
        $('#edit_copy_modal').find('#id').val(id);
        $('#edit_copy_modal').find('#edit_copy').val(copy);
        $('#edit_copy_modal').modal('show');
    });

    $(document).on('submit','#edit_extracopy',function(e){
        e.preventDefault();
        $('.error').hide();
         if($('#edit_copy').val().trim()=='' || isNaN($('#edit_copy').val()) || parseFloat($('#edit_copy').val())<=0 || $('#edit_copy').val() % 1 != 0){
            $('#edit_copy').focus();
            $('#edit_copy_err').show();
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#edit_copy_modal').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_copy').focus();	    
			        $('#edit_copy_err').html(res.msg).show();	    
				}
			}
		});
    });


//=====================Add Extra Copy================//




    
    $(document).on('submit','#add_membership',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#title').val().trim()==''){
            $('#title').focus();
            $('#title_err').show();
            return false;
        }
        if($('#description').val().trim()==''){
            $('#description').focus();
            $('#description_err').show();
            return false;
        }
        if($('#featureseditor').val().trim()==''){
            $('#featureseditor').focus();
            $('#features_err').show();
            return false;
        }
       
        var amount = $('#amount').val().trim();
        if(amount=='' || isNaN(amount) || parseFloat(amount)<=0){
            $('#amount').focus();
            $('#amount_err').html('Please Enter valid Amount').show();
            return false;
        }

        if($('#period :selected').val()==''){
            $('#period').focus();
            $('#period_err').show().html('Please select Period for membership');
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    window.location.href = site_url+'membership';
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
				}else{
			        $('#title').focus();	    
			        $('#title_err').html(res.msg).show();	    
				}
			}
		});
    });

    $(document).on('submit','#edit_membership',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#title').val().trim()==''){
            $('#title').focus();
            $('#title_err').show();
            return false;
        }
        if($('#description').val().trim()==''){
            $('#description').focus();
            $('#description_err').show();
            return false;
        }
        if($('#editfeatureseditor').val().trim()==''){
            $('#editfeatureseditor').focus();
            $('#edit_features_err').show();
            return false;
        }

        var amountt = $('#amountt').val();
        if(amountt=='' || isNaN(amountt) || parseFloat(amountt)<=0){
            $('#amountt').focus();
            $('#amount_err').html('Please Enter valid Amount').show();
            return false;
        }
        
        if($('#period :selected').val()==''){
            $('#period').focus();
            $('#period_err').show().html('Please select Period for membership');
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    window.location.href = site_url+'membership';
                    // setTimeout(function() {
                    //     location.reload();
                    // }, 2000);
				}else{
			        $('#title').focus();	    
			        $('#title_err').html(res.msg).show();	    
				}
			}
		});
    });
    
    $(document).on('submit','#add_commission',function(e){
        e.preventDefault();
        $('.error').hide();
        var commission = $('#commission_percent').val().trim();
        if(commission=='' || isNaN(commission) || parseFloat(commission)<=0){
            $('#commission_percent').focus();
            $('#commission_err').show();
            return false;
        }


        if($('#description').val().trim()==''){
            $('#description').focus();
            $('#des_err').show();
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#code_err').html(res.msg).show();	    
				}
			}
		});
    });

    $('.edit_commission_modal').on('click',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var percent = $(this).parents('tr').find('.abc').attr('vale');
        var des = $(this).parents('tr').find('.des').html();

        $('#edit_commission_modal').find('#id').val(id);
        $('#edit_commission_modal').find('#edit_description').val(des).html(des).trigger('change');
        $('#edit_commission_modal').find('#edit_commission').val(percent);
        $('#edit_commission_modal').modal('show');
    });

    $(document).on('submit','#edit_commission_form',function(e){
        e.preventDefault();
        $('.error').hide();
         var commission = $('#edit_commission').val().trim();
        if(commission=='' || isNaN(commission) || parseFloat(commission)<=0){
            $('#edit_commission').focus();
            $('#edit_commission_err').show();
            return false;
        }

        if($('#edit_description').val().trim()==''){
            $('#edit_description').focus();
            $('#edit_des_err').show();
            return false;
        }

        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#edit_commission_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_commission').focus();	    
			        $('#edit_commission_err').html(res.msg).show();	    
				}
			}
		});
    });
    
    ///==print management============
    $(document).on('submit','#add_print_management',function(e){
        e.preventDefault();
        $('.error').hide();
        var order_type = $('#order_type :selected').val();
        var print_type = $('#print_type :selected').val();
        var print_type_content = $('#print_type').find(":selected").text().toUpperCase();
       // console.log(print_type_content);
    //if(selected==='Flyer'.toUpperCase()        
        var paper_type = $('#paper_type :selected').val();
        var paper_size = $('#paper_size :selected').val();
        var color = $('#color :selected').val();
        var binding = $('#binding :selected').val();
        var sides = $('#sides :selected').val();
        var width = $('#width').val().trim();
        var flyer_copy_from = $('#flyer_copy_from').val().trim();
        var flyer_copy_to = $('#flyer_copy_to').val().trim();
        //var avg_price = $('#avg_price').val().trim();


        if(order_type==''){
            console.log('1');
            $('#order_type').focus();
            $('#ordertype_err').show();
            return false;
        }
        if(order_type==1 && print_type==''){
            console.log('2');
            $('#print_type').focus();
            $('#printtype_err').show();
            return false;
        }

        if(paper_type==''){
            console.log('3');
            $('#paper_type').focus();
            $('#papertype_err').show();
            return false;
        }
        
        if(print_type!='4' && paper_size==''){
            console.log('4');
            $('#paper_size').focus();
            $('#papersize_err').show();
            return false;
        }

        //if((order_type==2 || print_type_content==='flyer'.toUpperCase())&& color==''){
            //console.log(order_type);
            //console.log(color);        
        if(order_type==2 && color==''){
            console.log('5');
            $('#color').focus();
            $('#color_err').show();
            return false;
        }
        if(order_type==2 && binding==''){
            console.log('6');
            $('#binding').focus();
            $('#binding_err').show();
            return false;
        }
        if(print_type_content==='flyer'.toUpperCase() && sides==''){
            console.log('7');
            $('#sides').focus();
            $('#sides_err').show();
            return false;
        }

        if(print_type_content==='flyer'.toUpperCase() && (flyer_copy_from=='' || flyer_copy_from==0)){
            console.log('77');
            $('#flyer_copy_from').focus();
            $('#flyer_copy_from_err').show();
            return false;
        }
        
         if(print_type_content==='flyer'.toUpperCase() && (flyer_copy_to=='' || flyer_copy_to==0)){
            console.log('77');
            $('#flyer_copy_to').focus();
            $('#flyer_copy_to_err').show();
            return false;
        }
        
        if(print_type=='4' && width==''){
            console.log('8');
            $('#width').focus();
            $('#width_err').show();
            return false;
        }        
        // if(avg_price=='' || isNaN(avg_price) || parseFloat(avg_price)<=0){
        //     console.log('9');
        //     $('#avg_price').focus();
        //     $('#avgprice_err').show();
        //     return false;
        // }


        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
                console.log('before');
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success==true){
    				$('#add_new').modal('hide');
                    swal('', "Added Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#common_err').focus();	    
			        $('#common_err').html(res.msg).show();	    
				}
			}
		});
    });
 
    
    
    $('.edit_pm_modal').on('click',function(e){
        e.preventDefault();
         var id = $(this).data('id');
      
        $.ajax({
            url: site_url+'admin/edit_price_modal/'+id,
            beforeSend:function(){
                ajaxindicatorstart();
            },
            success: function(res) {
                ajaxindicatorstop();
                $('#edit_pm_modal').find('.modal-content').html(res);
                $('#edit_pm_modal').modal('show');
            }
        });        
    });
    
    
    $(document).on('submit','#edit_pm',function(e){
        e.preventDefault();
        $('.error').hide();

        // var print_type = $('#e_print_type :selected').val();
        // var paper_type = $('#e_paper_type :selected').val();
        // var paper_size = $('#e_paper_size :selected').val();
        //var avg_price = $('#e_avg_price').val().trim();

        var order_type = $('#e_order_type :selected').val();
        var print_type = $('#e_print_type :selected').val();
        var print_type_content = $('#e_print_type').find(":selected").text().toUpperCase();
      
        var paper_type = $('#e_paper_type :selected').val();
        var paper_size = $('#e_paper_size :selected').val();
        var color = $('#e_color :selected').val();
        var binding = $('#ebindingDiv :selected').val();
        var sides = $('#esidesDiv :selected').val();
        var width = $('#ewidthDiv').val().trim();
        var extra_copies = $('#e_extra_copies').val().trim();
        
        
        if(print_type==''){
            $('#e_print_type').focus();
            $('#e_printtype_err').show();
            return false;
        }

        if(paper_type==''){
            $('#e_paper_type').focus();
            $('#e_papertype_err').show();
            return false;
        }
        
        if(paper_size==''){
            $('#e_paper_size').focus();
            $('#e_papersize_err').show();
            return false;
        }


        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success){
    				$('#edit_pm_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#e_common_err').html(res.msg).show();	    
				}
			}
		});
     
    });

//==product====//
    
$(document).on('submit','#add_product',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
    }
   
    var amount = $('#price').val().trim();
    if(amount=='' || isNaN(amount) || parseFloat(amount)<=0){
        $('#price').focus();
        $('#price_err').html('Please Enter valid Amount').show();
        return false;
    }

    if($('#image').val().trim()==''){
        $('#image').focus();
        $('#image_err').show();
        return false;
    }else{
        if($('#image').attr('is_valid')==0){
            $('#image').parents('.form-group').find('.image_err').show();
            return false;
        }else{
            $('#image').parents('.form-group').find('.image_err').hide();
        }
    }
    var qty = $('#qty').val().trim();
    if(qty=='' || isNaN(qty) || parseInt(qty)<=0 || !isInt_number(qty)){
        $('#qty').focus();
        $('#qty_err').html('Please Enter Valid Quantity').show();
        return false;
    }   

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                setTimeout(function() {
                    swal('', "Added Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'admin/stationary';
                }, 2000);

            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});



$(document).on('submit','#edit_product',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
    }
   
    var amount = $('#price').val().trim();
    if(amount=='' || isNaN(amount) || parseFloat(amount)<=0){
        $('#price').focus();
        $('#price_err').html('Please Enter valid Amount').show();
        return false;
    }

    if($('#image').val().trim()!=''){
        if($('#image').attr('is_valid')==0){
            $('#image').parents('.form-group').find('.image_err').show();
            return false;
        }else{
            $('#image').parents('.form-group').find('.image_err').hide();
        }
    }
    var qty = $('#qty').val().trim();
    if(qty=='' || isNaN(qty) || parseInt(qty)<=0 || !isInt_number(qty)){
        $('#qty').focus();
        $('#qty_err').html('Please Enter Valid Quantity').show();
        return false;
    }   


    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                // swal('', "Updated Successfully", "success");
                // window.location.href = site_url+'admin/stationary';
                setTimeout(function() {
                    swal('', "Updated Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'admin/stationary';
                }, 2000);                
            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});

  
/*
$(document).on('submit','#search_printery',function(e){
        e.preventDefault();
        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			//async: false,
			url: url,
			type: "POST",
			data:formData,
    		//dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
			
				ajaxindicatorstop();
				$('#tableData').html(res);
				$(document).find('#example').DataTable();
				//console.log('res',res);
				if(res.success) {
					//window.location.href = res.redirect;
				}else{
				// 	$("#validation_err").show().html(res.msg);
				// 	allIsOk = false;
				}
			}
		});
    });
*/

$('#order_type, #e_order_type').on('change',function(){
    
    $('#print_type, #paper_type, #paper_size, #color, #binding, #sides, #width').val('').trigger('change');
    $('#print_type').val('').trigger('change');
    if($(this).val()==1){
        $('#printTypeDiv').css('display','flex');
        $('#papersizeDiv').css('display','flex');
        $('#colorDiv').css('display','none');
        $('#bindingDiv').css('display','none');
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');        
    }else if($(this).val()==2){
        $('#colorDiv').css('display','flex');
        $('#bindingDiv').css('display','flex');
        $('#papersizeDiv').css('display','flex');
        $('#printTypeDiv').css('display','none'); 
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');

    }else if($(this).val()==3 || $(this).val()==4){
        $('#colorDiv').css('display','none');
        $('#bindingDiv').css('display','none');
        $('#papersizeDiv').css('display','none');
        $('#printTypeDiv').css('display','none'); 
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');

    }else{
        $('#printTypeDiv').css('display','none');
    }
})

$('.only_whole_number').on('keyup',function(){
    var ds = $(this).val();
    //console.log('ds',ds);
    if(isNaN(ds) || ds % 1 != 0){
        $(this).val('');
    }
});

$('#print_type').on('change',function(){
    var selected = $('#print_type').find(":selected").text().toUpperCase();
    console.log('selected',selected);
    //var areEqual = selected.toUpperCase() === string2.toUpperCase();
    if(selected==='Flyer'.toUpperCase()){
        $('#sidesDiv').css('display','flex');
        $('#papersizeDiv').css('display','flex');
        $('#widthDiv').css('display','none');
        $('#colorDiv').css('display','none');
        $('#extra_copyDiv').css('display','flex');
        $('#extra_copyTo').css('display','flex');
    }else if(selected==='Banner'.toUpperCase()){
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','flex');
        $('#papersizeDiv').css('display','none');
        $('#extra_copyDiv').css('display','none');
        $('#extra_copyTo').css('display','none');
    }else if(selected==='ROLLUP'.toUpperCase()){
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');
        $('#papersizeDiv').css('display','flex');
        $('#extra_copyDiv').css('display','none');
        $('#extra_copyTo').css('display','none');
        $('#colorDiv').css('display','none');
    }else{
        $('#colorDiv').css('display','none');
        $('#bindingDiv').css('display','none');
        $('#sidesDiv').css('display','none');
        $('#widthDiv').css('display','none');
        $('#extra_copyDiv').css('display','none');
        $('#extra_copyTo').css('display','none');
    }
})


function isInt_number(num) {
   return num % 1 === 0;
}

$('.imgInput').on('change', function(){

    var reader = new FileReader();
    var ds = $(this);
    image_validation(this).then(function(){

        setTimeout(function() {

            is_valid = ds.attr('is_valid');
             console.log('is_valid',is_valid);
            if(is_valid==1){
                ds.parents('.form-group').find('.image_err').hide().html('');
            }

        }, 1000);

    });
});
/*$('.docInput').on('change', function(){

    var reader = new FileReader();
    var ds = $(this);
    doc_validation(this).then(function(){

        setTimeout(function() {

            is_valid = ds.attr('is_valid');
             console.log('is_valid',is_valid);
            if(is_valid==1){
                ds.parents('.form-group').find('.doc_err').hide().html('');
            }

        }, 1000);

    });
});


async function doc_validation(ds){
        //console.log('s',ds);

    var msg = '';
    ext_validate =false;
    var inputname = $(ds).attr('name');

    var filename = ds.files[0].name;
    var extension = filename.substr( (filename.lastIndexOf('.') +1) );
    if(inputname=='document' && (extension =='pdf'){
        ext_validate = true;
    }else if(extension =='pdf'){
        ext_validate = true;
    }
    
    console.log('ext_validate',ext_validate);
    if(ext_validate){

        if (window.FileReader && window.Blob) {
                
                var files = $(ds).get(0).files;
                if (files.length > 0) {
                    var file = files[0];
                    var fileReader = new FileReader();
                    fileReader.onloadend = function (e) {
                        var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                        var header = '';
                        for (var i = 0; i < arr.length; i++) {
                            header += arr[i].toString(16);
                        }

                        // Check the file signature against known types
                        var type = 'unknown';
                      

            console.log('filetype',file.type);
            console.log('type',type);


                        if (file.type !== type) {
                            $(ds).attr('is_valid',0);
                            $(ds).parents('.form-group').find('.image_err').show().html('Invalid file selected');
                        } else {
                            $(ds).attr('is_valid',1);
                        }
                    };
                    fileReader.readAsArrayBuffer(file);
                }else{
                    $(ds).attr('is_valid',0);
                    $(ds).parents('.form-group').find('.image_err').show().html('Please select file');              
                }
        }else{
            $(ds).attr('is_valid',0);
            $(ds).parents('.form-group').find('.image_err').show().html('Your browser is not supported. Sorry.');                   
        }

    }else{
        $(ds).attr('is_valid',0);
        $(ds).parents('.form-group').find('.image_err').show().html('invalid file extension');                  
    }
}*/
async function image_validation(ds){
        //console.log('s',ds);

    var msg = '';
    ext_validate =false;
    var inputname = $(ds).attr('name');

    var filename = ds.files[0].name;
    var extension = filename.substr( (filename.lastIndexOf('.') +1) );
    if(inputname=='image' && (extension =='gif' || extension =='jpg' || extension =='jpeg' || extension =='png')){
        ext_validate = true;
    }else if(extension =='jpg' || extension =='jpeg' || extension =='png'){
        ext_validate = true;
    }
    
    console.log('ext_validate',ext_validate);
    if(ext_validate){

        if (window.FileReader && window.Blob) {
                
                var files = $(ds).get(0).files;
                if (files.length > 0) {
                    var file = files[0];
                    var fileReader = new FileReader();
                    fileReader.onloadend = function (e) {
                        var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                        var header = '';
                        for (var i = 0; i < arr.length; i++) {
                            header += arr[i].toString(16);
                        }

                        // Check the file signature against known types
                        var type = 'unknown';
                        switch (header) {
                            case '89504e47':
                                type = 'image/png';
                                break;
                            case '47494638':
                                type = 'image/gif';
                                break;
                            case 'ffd8ffe0':
                            case 'ffd8ffe1':
                            case 'ffd8ffe2':
                                type = 'image/jpeg';
                                break;
                        }

            console.log('filetype',file.type);
            console.log('type',type);


                        if (file.type !== type) {
                            $(ds).attr('is_valid',0);
                            $(ds).parents('.form-group').find('.image_err').show().html('Invalid file selected');
                        } else {
                            $(ds).attr('is_valid',1);
                        }
                    };
                    fileReader.readAsArrayBuffer(file);
                }else{
                    $(ds).attr('is_valid',0);
                    $(ds).parents('.form-group').find('.image_err').show().html('Please select file');              
                }
        }else{
            $(ds).attr('is_valid',0);
            $(ds).parents('.form-group').find('.image_err').show().html('Your browser is not supported. Sorry.');                   
        }

    }else{
        $(ds).attr('is_valid',0);
        $(ds).parents('.form-group').find('.image_err').show().html('invalid file extension');                  
    }
}

$('.datepciker').datepicker({
   autoclose:true,
   format: 'dd-mm-yyyy',
})

$('.coupon_from').datepicker({
    autoclose:true,
    format: 'dd-mm-yyyy',
    //viewMode: "months", 
    //minViewMode: "months"
}).on('changeDate', function () {
    var fromDate = $(this).val();
    var to_date = $(document).find('.coupon_to');
    to_date.val(fromDate);
    to_date.datepicker('destroy');
    to_date.datepicker({
        startDate:fromDate, 
        autoclose:true,
        format: 'dd-mm-yyyy', 
        // viewMode: "months", 
        // minViewMode: "months"      
    });
});

$('.e_coupon_from').datepicker({
    autoclose:true,
    format: 'dd-mm-yyyy',
    //viewMode: "months", 
    //minViewMode: "months"
}).on('changeDate', function () {
    var fromDate = $(this).val();
    var to_date = $(document).find('.e_coupon_to');
    to_date.val(fromDate);
    to_date.datepicker('destroy');
    to_date.datepicker({
        startDate:fromDate, 
        autoclose:true,
        format: 'dd-mm-yyyy', 
        // viewMode: "months", 
        // minViewMode: "months"      
    });
});

$('#coupon_type').on('change',function(){
    if($(this).val()!=''){ 
        $('#coupon_type_err').hide();
        if($(this).val()==1){
            $('#amountDiv_percent').show();
            $('#amountDiv_amt').hide();
            $('#amount').val('');

        }else
        if($(this).val()==0){
            $('#amountDiv_amt').show();
            $('#amountDiv_percent').hide();
            $('#coupon_percent').val('');
        }
    }else{
        $('#amountDiv_percent,#amountDiv_amt').hide();
        $('#coupon_percent, #amount').val('');
    }
});

$('#edit_coupon_type').on('change',function(){
    if($(this).val()!=''){ 
        $('#edit_coupon_type_err').hide();
        if($(this).val()==1){
            $('#e_amountDiv_percent').show();
            $('#e_amountDiv_amt').hide();
            //$('#e_amount').val('');

        }else
        if($(this).val()==0){
            $('#e_amountDiv_amt').show();
            $('#e_amountDiv_percent').hide();
            //$('#e_coupon_percent').val('');
        }
    }else{
        $('#e_amountDiv_percent,#e_amountDiv_amt').hide();
        //$('#e_coupon_percent, #e_amount').val('');
    }    
});

//==banner size===//


    $('.edit_banner_modal').on('click',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        $('#edit_banner_modal').find('#id').val(id);
        $('#edit_banner_modal').find('#edit_name').val(title);
        $('#edit_banner_modal').modal('show');
    });
    
//==banner size===//


$('.vendor_status').on('click',function(){
  var vid = $(this).parents('tr').attr('vid');
  var status = $(this).attr('st');
  $('#vendor_status_form').find('#vendor_id').val(vid);
  $('#vendor_status_form').find('#status').val(status);

  $('#v_status_modal').modal('show');
});

$('.vp_status').on('click',function(){
  var vid = $(this).parents('tr').attr('vid');
  var status = $(this).attr('st');
  console.log('vid',vid);
  console.log('status',status);
  $('#vp_status_form').find('#vendor_id').val(vid);
  $('#vp_status_form').find('#status').val(status);

  $('#vp_status_modal').modal('show');
});

$(document).on('submit','#vendor_status_form',function(e){
    e.preventDefault();
    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        dataType:'JSON',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            
            ajaxindicatorstop();
            if(res.success){
                 $('#v_status_modal').modal('hide');
                    swal('', "Done", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }else{
              $('#name_err').html(res.msg).show();
            }
        }
    });
});


$(document).on('submit','#vp_status_form',function(e){
    e.preventDefault();

    var ds = $(this);
    var url  = ds.attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        data:formData,
        type:'post',
        processData: false,
        contentType: false,
        beforeSend:function(){
            //ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#vp_status_modal').modal('hide');
                swal('', "Done", "success");
            setTimeout(function() {
                location.reload();
            }, 2000);
        }
    });
});

//anusha
$(document).on('submit','#add_notes_category',function(e){
    e.preventDefault();
    $('.error').hide();
    $('#name').val()
     if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
        
    }

   

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
    	url: url,
    	type: "POST",
    	data:formData,
    	dataType: "json",
        processData: false,
        contentType: false,			
    	beforeSend:function(){
    		ajaxindicatorstart();
    	},
    	success: function(res) {
    	    //alert(res);
    	     ajaxindicatorstop();
    		if(res.success==true){
    		    //alert('hi');
    			$('#add_note_categories').modal('hide');
                swal('', "Added Successfully", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
    		}else{
    	        $('#name_err').html(res.msg).show();	    
    		}
    	}
    });
});

$('.edit_notes_category_modal').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: site_url+'admin/edit_notes_cat_modal/'+id,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#edit_notes_category_modal').find('.modal-content').html(res);
            $('#edit_notes_category_modal').modal('show');
        }
    });
});
$(document).on('submit','#edit_notes_category',function(e){
        e.preventDefault();
        $('.error').hide();
        if($('#edit_name').val().trim()==''){
            $('#edit_name').focus();
            $('#edit_name_err').show();
            return false;
            
        }
        ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success){
    				$('#edit_notes_category_modal').modal('hide');
                    swal('', "Updated Successfully", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
				}else{
			        $('#edit_name_erra').html(res.msg).show();	    
				}
			}
		});
     
    });
$(document).on('submit','#add_note',function(e){
    //alert('hi');
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
    }
   if($('#editor1').val().trim()==''){
        $('#editor1').focus();
        $('#edi_err').show();
        return false;
    }
    if($('#doc').val().trim()==''){
        $('#doc').focus();
        $('#doc_err').show();
        return false;
    }else{
        console.log($('#doc').attr('is_valid'));
        if($('#doc').attr('is_valid')==0){
            $('#doc').parents('.form-group').find('#doc_err').show();
            return false;
        }else{
             $('#doc').parents('.form-group').find('#doc_err').hide();
        }
    }

   if($('#image').val().trim()==''){
        $('#image').focus();
        $('#image_err').show();
        return false;
    }else{
        if($('#image').attr('is_valid')==0){
            $('#image').parents('.form-group').find('.image_err').show();
            return false;
        }else{
            $('#image').parents('.form-group').find('.image_err').hide();
        }
    }
    

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            alert(res);
            ajaxindicatorstop();
            if(res.success==true){
                setTimeout(function() {
                    swal('', "Added Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'admin/note';
                }, 2000);

            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});
$(document).on('submit','#edit_note',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#name').val().trim()==''){
        $('#name').focus();
        $('#name_err').show();
        return false;
    }
   if($('#editor1').val().trim()==''){
        $('#editor1').focus();
        $('#edi_err').show();
        return false;
    }
    
 
     if($('#doc').val().trim()!=''){
        if($('#doc').attr('is_valid')==0){
            $('#doc').parents('.form-group').find('.doc_err').show();
            return false;
        }else{
            $('#doc').parents('.form-group').find('.doc_err').hide();
        }
    }
    if($('#image').val().trim()!=''){
        if($('#image').attr('is_valid')==0){
            $('#image').parents('.form-group').find('.image_err').show();
            return false;
        }else{
            $('#image').parents('.form-group').find('.image_err').hide();
        }
    }
     
   

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success==true){
                // swal('', "Updated Successfully", "success");
                // window.location.href = site_url+'admin/stationary';
                setTimeout(function() {
                    swal('', "Updated Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'admin/note';
                }, 2000);                
            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});

$(document).on('submit','#update_profile',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#fname').val().trim()==''){
        $('#fname').focus();
        $('#fname_err').show();
        return false;
    }
    
   

    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            //alert(res);
            ajaxindicatorstop();
            if(res.success==true){
                // swal('', "Updated Successfully", "success");
                // window.location.href = site_url+'admin/stationary';
                setTimeout(function() {
                    swal('', "Updated Successfully", "success");
                }, 1000);
                setTimeout(function() {
                    window.location.href = site_url+'admin/myprofile';
                }, 2000);                
            }else{
                $('#php_error').html(res.msg).show();       
            }
        }
    });
});

$(document).on('submit','#edit_customer',function(e){
    e.preventDefault();
    $('.error').hide();
    if($('#fname').val().trim()==''){
        $('#fname').focus();
        $('#fname_err').show();
        return false;
    }

    if($('#adm_cus_address').val().trim()==''){
        $('#adm_cus_address').focus();
        $('#address_err').show();
        return false;
    }
    
     if($('#adm_cus_lat').val().trim()==''){
        $('#adm_cus_lat').focus();
        $('#address_err').show();
        return false;
    }
    ds = $(this);
    var url = $(this).attr('action');
    var formData = new FormData(ds[0]);
    $.ajax({
        url: url,
        type: "POST",
        data:formData,
        dataType: "json",
        processData: false,
        contentType: false,         
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            if(res.success){
               // $('#edit_service_modal').modal('hide');
                swal('', "Updated Successfully", "success");
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }else{
                $('#edit_name_err').html(res.msg).show();       
            }
        }
    });
});
