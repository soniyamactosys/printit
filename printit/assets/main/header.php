<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Printit - Dashboard</title>
    
	<!-- Vendors Style-->
	<link rel="stylesheet" href="css/vendors_css.css">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/skin_color.css">
     
  </head>
  <body class="hold-transition light-skin sidebar-mini theme-primary">
    
<div class="wrapper">
  <!-- <div id="loader"></div> -->
    
  <header class="main-header">
    <div class="d-flex align-items-center logo-box justify-content-start" style="background-color: #000000;">
        <a href="#" class="waves-effect waves-light nav-link d-none d-md-inline-block mx-10 push-btn bg-transparent" data-toggle="push-menu" role="button">
            <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
        </a>    
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- logo-->
          <div class="logo-lg">
              <span class="light-logo"><img src="../images/logo.png" alt="logo"></span>
              <span class="dark-logo"><img src="../images/logo.png" alt="logo"></span>
          </div>
        </a>    
    </div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav"> 
            <li class="btn-group nav-item d-lg-inline-flex d-none">
                <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link full-screen" title="Full Screen">
                    <i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
                </a>
            </li>
        </ul>
      </div>
    </nav>
  </header>