<aside class="main-sidebar">
  <!-- sidebar-->
	<section class="sidebar" style="height: auto;">	
		
	  <!-- sidebar menu-->
	  <ul class="sidebar-menu tree" data-widget="tree">		
		<li>
	      <a href="index.php">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Dashboard</span>
	      </a>
	    </li>
		<li class="header">Shop Management</li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i></i>
	        <span>Printery Shop</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="displayshop_printery.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop Approval</a></li>
		    <li><a href="printeryshop_list.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop List</a></li>
   		    <li><a href="printeryshoprejectlist.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop Rejected List</a></li>
	      </ul>
	    </li>
		<li class="header">PRINT Settings</li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Master Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="paper_type.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Paper Type</a></li>
			<li><a href="paper_size.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Paper Size</a></li>
			<li><a href="extra_copies.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Extra Copies</a></li>
	      </ul>
	    </li>	
	    <li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Average Cost Settings</span>
	      </a>
	    </li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Express Rate Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="banner.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Banner</a></li>
	        <li><a href="rollup.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Rollup</a></li>
	        <li><a href="poster.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Poster</a></li>
	        <li><a href="flyer.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Flyer</a></li>
	      </ul>
	    </li>
		<li class="header">ORDER MANAGEMENT</li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Express Order</span>
	      </a>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Normal Order</span>
	      </a>
	    </li>
		<li class="header">CUSTOMER MANAGEMENT</li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Active Customer</span>
	      </a>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Disable customer</span>
	      </a>
	    </li>
		<li class="header">DELIVERY MANAGEMENT</li>		
		<li><a href="#"><i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Zone Master</span>
	      </a>
	    </li>		
		<li>
	      <a href="zones.php">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Add Zone</span>
	      </a>
	    </li>
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Add Shipping Methods</span>
	      </a>
	    </li>		
		<li>
	      <a href="delivery-cost.php">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Delivery Cost</span>
	      </a>
	    </li>
		<li class="header">COUPON MANAGEMENT</li>		
		<li><a href="#"><i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Add Coupon</span>
	      </a>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Coupon Usage</span>
	      </a>
	    </li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Products</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="product_list.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Product List</a></li>
        	<li><a href="product_stock.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Product Stock</a></li>
	      </ul>
	    </li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Print Management</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="print-management.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Print Management</a></li>
	      </ul>
	    </li>	
	    <li class="header">PRINTERY MANAGEMENT</li>	
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Printery</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="display_printery.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Approval</a></li>
			<li><a href="printery_list.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery List</a></li>
			<li><a href="printeryrejectlist.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Rejected List</a></li>
	      </ul>
	    </li>	
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Orders</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="order.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Orders List</a></li>
        	<li><a href="box_advanced.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Completed Orders</a></li>
        	<li><a href="box_advanced.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Cancelled Orders</a></li>
	      </ul>
	    </li>	
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Commission</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="vendor_list.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Add Commission</a></li>
        	<li><a href="#"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Add Membership</a></li>
	      </ul>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Reports</span>
	      </a>
	    </li>	
	  </ul>
	</section>
	<div class="sidebar-footer">
		<!-- item-->
		<a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><span class="icon-Settings-2"></span></a>
		<!-- item-->
		<a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
		<!-- item-->
		<a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
	</div>
</aside>