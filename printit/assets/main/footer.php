<footer class="main-footer">
<div class="pull-right d-none d-sm-inline-block">
    <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
	  <li class="nav-item">
		 ©2020 All Rights Reserved. <a target="_blank" href="#" class="nav_link">Printit TECHNOLOGIES</a>
	  </li>
	</ul>
</div>
</footer>