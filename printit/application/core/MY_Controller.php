<?php
ob_start();
//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct(){
		parent ::__construct();
        $language = $this->session->userdata("site_lang");
        //echo $language;die;   
        $this->lang->load("locale", $language);        
       
        // $this->dummy_img = 'dummy_profile_picture.png';
        
        //$this->fmt = numfmt_create('Asia/Kuwait', NumberFormatter::CURRENCY );
        $this->fmt = "";
        $this->data = array('footer_data'=>array());

	}
	
	public function frontview($view,$data=array()){
	    if(!empty($data)){
    	    $this->data = $data;
	    }
		$this->load->view('web/header',$this->data);
		$this->load->view('web/sidebar',$this->data);
		$this->load->view($view,$this->data);
		$this->load->view('web/footer',$this->data['footer_data']);
	}


    public function vendorview($view,$data=array()){
        $this->data = $data;
        $this->load->view('common/header',$this->data);
        $this->load->view('vendor/left_nav',$this->data);
        $this->load->view($view,$this->data);
        $this->load->view('common/footer');
    }
    public function mail_template_view($view,$data=array()){
        $return = $this->load->view('mail/header',"",true);
        $return .= $this->load->view($view,$data,true);
        $return .= $this->load->view('mail/footer',"",true);   
        return $return;
    }
    
    public function order_success_user($oid){

        $admin_detail = admin_detail();
        $order_detail = $this->generalmodel->order_detail($oid);

        //$user_detail = user_detail_byemail("",$order_detail['customer']);
        $from = $admin_detail['email'];
        $to = $order_detail['customer_email'];
        //$to = "soniya@mactosys.com";
        
        $subject = "Your order payment made successfully!";
        $data['name'] = $order_detail['customer_name'];
        $data['data'] = $order_detail;
        $message = $this->mail_template_view('mail/order_success_user',$data);

        //print_r($message); exit;
        
        if($this->sendGridMail("",$to,$subject,$message)){ 
            return true;
        }else{
            return false;
        }       
    }

    
    public function order_success_vendor($oid){
        $order_detail = $this->generalmodel->order_detail($oid);

        $user_detail = vendor_detail("",$order_detail['vendor']);

        $to = $user_detail['email'];
        //$to = "developermactosys@gmail.com";
        $subject = "New order!";
        $data['name'] = $user_detail['shop_name'];
        $data['data'] = $order_detail;
        $message = $this->mail_template_view('mail/order_success_vendor',$data);
       
       
       //print_r($order_detail);
       
        //echo $message; exit;
        if($this->sendGridMail("",$to,$subject,$message)){ 
            return true;
        }else{
            return false;
        }            
    }

    public function order_success_incharge($oid){

        $order_detail = $this->generalmodel->order_detail($oid);

        $user_detail = incharge_detail($order_detail['vendor']);
        //print_r($user_detail); exit;
       
        $to = $user_detail['email'];
        //$to = "anusha.mactocarol@gmail.com";
        $subject = "New order!";
        $data['name'] = $user_detail['fname'].' '.$user_detail['lname'];
        $data['data'] = $order_detail;
        $message = $this->mail_template_view('mail/order_success_incharge',$data);
       
       
       //print_r($order_detail);
       
        //echo $message; exit;
        if($this->sendGridMail("",$to,$subject,$message)){ 
            return true;
        }else{
            return false;
        }            
    }
    
        
    public function order_success_admin($oid){
        $order_detail = $this->generalmodel->order_detail($oid);
        $admin_detail = admin_detail();
        //$user_detail = incharge_detail($order_detail['vendor']);
       // print_r($admin_detail); exit;
        $to = $admin_detail['email'] ;
        //$to = "developermactosys@gmail.com" ;
        $subject = "New order!";
        $data['name'] = $admin_detail['firstname'].' '.$admin_detail['lastname'];
        $data['data'] = $order_detail;
        $message = $this->mail_template_view('mail/order_success_admin',$data);
       //echo  $message; 
        if($this->sendGridMail("",$to,$subject,$message)){ 
            return true;
        }else{
            return false;
        }            
    }
	public function unlinkTempFile(){
        $data = $_POST["filename"];
        $file = './tmp_upload/'.$data;
        if(file_exists($file)){
            unlink($file);
            echo true;
        }
    }

    public function uploadDoc($filename='' , $upload_path='',$allowed_extensions=''){
        $newname = time().$_FILES[$filename]['name'];
        $ext = pathinfo($newname, PATHINFO_EXTENSION);
        if(empty($allowed_extensions)){
            $allowed_extensions = array('docx','doc','pdf');
        }

        if(in_array($ext,$allowed_extensions)){

            $allowed_types = implode('|',$allowed_extensions);

            $config['file_name']            = $newname;
            $config['upload_path']          = $upload_path;
            $config['allowed_types']        = $allowed_types;

            //print_r($config);
            //$config['allowed_types']        = 'docx|doc|pdf';
            // $config['max_size']             = 2000;
            // $config['max_width']            = 20240;
            // $config['max_height']           = 8680;
            //$config['encrypt_name']         = true;

            // echo "<pre>"; print_r($_FILES);
            // echo "<pre>"; print_r($config);
            // exit;

            $this->upload->initialize($config);

            if(!$this->upload->do_upload($filename))
            {
                $data = array('error' => $this->upload->display_errors());
            }
            else
            {
                $data = $this->upload->data();
            }
        }else{
            $data = array('error' =>'File Type is not allowed');
        }
        return $data ;
    }

    public function multiple_upload($filename='' , $upload_path='',$allowed_extensions=''){

        $f = '';
        if(empty($allowed_extensions)){
            $allowed_extensions = array('docx','doc','pdf');
        }        
        $allowed_types = implode('|',$allowed_extensions);
        $config['allowed_types']        = $allowed_types;

        $config['upload_path']          = $upload_path;

        foreach($_FILES[$filename]['name'] as $key=>$value){

            $files = $_FILES[$filename];

            $newname = time().$value;
            $ext = pathinfo($newname, PATHINFO_EXTENSION);

            $config['file_name']      = $newname;
            $_FILES['images']['name'] = $files['name'][$key];
            $_FILES['images']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images']['error'] = $files['error'][$key];
            $_FILES['images']['size'] = $files['size'][$key];
            $_FILES['images']['type'] = $files['type'][$key];


            $this->upload->initialize($config);
            if(!$this->upload->do_upload('images')){
                $errors = $this->upload->display_errors();
                return $errors;
            }else{
                $uploadfiles = $this->upload->data();
                $filenames[] = $uploadfiles['file_name'];
                $f = implode(',',$filenames);
            }
        }
        return $f;        
    }

    public function printery_withprice($oid="",$printery_id=""){
        $current_order = $this->session->userdata('current_order');
        $price_where ="";
        // echo "<pre>"; print_r($_SESSION); exit;
        // echo "<pre>"; print_r($current_order); exit;
        if($current_order['order_type']==1){
            
            $price_where .= " AND `print_type`=".$current_order['print_type']." AND `paper_type_id`=".$current_order['paper_type'];   

            if($current_order['print_type']==1){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==2){
                //$price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."'";
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."' AND `flyer_copy_from`<=".$current_order['no_of_copies']." AND `flyer_copy_to` >=".$current_order['no_of_copies'];

            }elseif($current_order['print_type']==3){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==4){
                $price_where .= " AND `banner_size` =".$current_order['banner_size'];
            }          

            $where = "ps.status='1' AND vp.`price_status`='1' AND vp.price >0 ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }

            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();
        }elseif($current_order['order_type']==2){

            $price_where = " AND `paper_size_id` =".$current_order['paper_size']." AND `paper_type_id`=".$current_order['paper_type']." AND `color`='".$current_order['color']."' AND `binding`='".$current_order['binding']."'";

            $where = "ps.status='1' AND vp.`price_status`='1' AND vp.price >0 ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }

            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();

        }elseif($current_order['order_type']==4){

            $color = $current_order['color'];
            $copy_number = $current_order['no_of_copies'];
            $where = "npr.status = '0' AND npr.price_status='1' AND npr.color='$color'";
            $where .= " AND npr.`copy_from` <=".$copy_number." AND `copy_to` >=".$copy_number;

            if(!empty($printery_id)){ $where .= " AND npr.printery_shop_id=".$printery_id; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,npr.printery_shop_id,npr.price as vendor_charge,npr.copy_from,npr.copy_to,npr.price,npr.id as npr_id')
            ->from('notes_price_range as npr')
            ->join('printery_shop as ps','ps.id = npr.printery_shop_id')
            ->where($where)
            ->get();

        }elseif($current_order['order_type']==3){
            
            $where = "tp.order_id=".$oid;
            
            if(!empty($printery_id)){ $where .= " AND tp.printery_shop_id=".$printery_id; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,tp.printery_shop_id,tp.price as vendor_charge,tp.id as tp_id')
            ->from('trans_proposals as tp')
            ->join('printery_shop as ps','ps.id = tp.printery_shop_id')
            ->where($where)
            ->get();
          
        }     
         //->result_array();
// echo $this->db->last_query();
//         echo $query; exit;
        if(!empty($printery_id)){
            $data = $query->row_array();
            $copy_number = $this->session->userdata('copy_number');
            $data['vendor_charge'] = $data['vendor_charge']*$copy_number;
            
        }else{
            $data = $query->result_array();
            $data = array_map("myfunction",$data);
        }


        //  echo $this->db->last_query();
        //   echo "<pre>"; print_r($data);
        //   exit;        
        return $data;
    }

    public function ex_printery_with_price($oid="",$printery_id="", $lat="",$long=""){
        
        if(empty($printery_id)){
            $query1 = "SELECT ps.id, (3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - ps.latitude) * pi()/180 / 2), 2) +COS( ".$lat." * pi()/180) * COS(ps.latitude * pi()/180) * POWER(SIN(( ".$long." - ps.longitude) * pi()/180 / 2), 2) ))) as distance from printery_shop as ps WHERE status='1' AND `delete_status`='0' having distance <= 10 order by distance";
            $list = $this->db->query($query1)->result_array();
            $ids = implode(",",array_column($list,'id'));            
        }
        //  echo "<pre>"; print_r($list); 
        //  echo "<pre>"; print_r($ids); 
        // echo $this->db->last_query();
        // exit;
        $current_order = $this->session->userdata('current_order');
        $price_where ="";
        // echo "<pre>"; print_r($_SESSION); exit;
        // echo "<pre>"; print_r($current_order); exit;
        if($current_order['order_type']==1){
            
            $price_where .= " AND `print_type`=".$current_order['print_type']." AND `paper_type_id`=".$current_order['paper_type'];   

            if($current_order['print_type']==1){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==2){
                //$price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."'";
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."' AND `flyer_copy_from`<=".$current_order['no_of_copies']." AND `flyer_copy_to` >=".$current_order['no_of_copies'];

            }elseif($current_order['print_type']==3){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==4){
                $price_where .= " AND `banner_size` =".$current_order['banner_size'];
            }          


            $where = "ps.status='1' AND vp.`exp_price_status`='1'  AND vp.exp_price >0  ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND ps.id IN(".$ids.")"; }


            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.exp_price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();
        }elseif($current_order['order_type']==2){

            $price_where = " AND `paper_size_id` =".$current_order['paper_size']." AND `paper_type_id`=".$current_order['paper_type']." AND `color`='".$current_order['color']."' AND `binding`='".$current_order['binding']."'";


            $where = "ps.status='1' AND vp.`exp_price_status`='1' AND vp.exp_price >0 ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND ps.id IN(".$ids.")"; }

            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.exp_price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();

        }elseif($current_order['order_type']==4){

            $color = $current_order['color'];
            $copy_number = $current_order['no_of_copies'];
            $where = "npr.status = '0' AND npr.exp_price_status='1' AND npr.exp_price >0 AND npr.color='$color'";
            $where .= " AND npr.`copy_from` <=".$copy_number." AND `copy_to` >=".$copy_number;
            

            // $page_range = $current_order['note_page_range'];            

            // $note_total_pages = $current_order['note_total_pages'];
            // $where .= " AND npr.`copy_from` <=".$note_total_pages." AND `copy_to` >=".$note_total_pages;
           

            if(!empty($printery_id)){ $where .= " AND npr.printery_shop_id=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND npr.printery_shop_id IN(".$ids.")"; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,npr.printery_shop_id,npr.exp_price as vendor_charge,npr.copy_from,npr.copy_to,npr.exp_price as price,npr.id as npr_id')
            ->from('notes_price_range as npr')
            ->join('printery_shop as ps','ps.id = npr.printery_shop_id')
            ->where($where)
            ->get();


        }elseif($current_order['order_type']==3){
            
            $where = "tp.order_id=".$oid;
            
            if(!empty($printery_id)){ $where .= " AND tp.printery_shop_id=".$printery_id; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,tp.printery_shop_id,tp.price as vendor_charge,tp.id as tp_id')
            ->from('trans_proposals as tp')
            ->join('printery_shop as ps','ps.id = tp.printery_shop_id')
            ->where($where)
            ->get();
          
        }     

        $copy_number = $this->session->userdata('copy_number');
        $data = $query->row_array();

        if(!empty($data)){
            $data['vendor_charge'] = $data['vendor_charge']*$copy_number;
            $data['price'] = $data['vendor_charge']*$copy_number;            
        }

        // echo "<pre>";
        // echo $this->db->last_query();
        // print_r($data); exit;
        //print_r($query);
        
        //echo "<pre>"; print_r($data);  exit;
        return $data;
        
    }
    


	/* Mail Send */
	/*
	public function send_mail($email, $subject, $message)	
	{
		$this->load->library('PHPMailer/PHPMailer');
		$mail = new PHPMailer();
		$mail->IsSMTP();   
		$mail->Host = "smtp.gmail.com";  // specify main and backup server
		$mail->SMTPAuth = true;     // turn on SMTP authentication
		$mail->Username = "ashish.thinkdebug@gmail.com";  // SMTP username
		$mail->Password = "#AB@thinkdebug"; // SMTP password
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		$mail->From = "info@thinkdebug";
		$mail->FromName = "Wardrobe Wizard";
		$mail->AddAddress($email);
		$mail->WordWrap = 50; 
		$mail->IsHTML(true); 
		$mail->Subject = $subject;
		$mail->Body    = $message;
		if(!$mail->Send()){
		  return false;
		}else{
			return true;
		}
	}
	*/
    public function sendGridMail($from="",$to,$subject,$message,$cc="",$attachment="",$attachmentname=""){

        $this->load->library('email');
$config['mailtype'] = 'html';
$config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'utf-8';
$config['wordwrap'] = TRUE;
$this->email->initialize($config);

       // $this->email->clear(TRUE);

        // $this->email->initialize(array(
        //   'protocol' => 'smtp',
        //   'smtp_host' => 'smtp.sendgrid.net',
        //   'smtp_user' => 'norelllply',
        //   'smtp_pass' => 'f6EbJqElllHpA#T%g^mYU',
         //   'smtp_secure'=> 'encryption_starttls',
        //   'smtp_port' => 587,
       //  'smtp_port' => 465,
        //   'crlf' => "\r\n",
        //   'newline' => "\r\n"
        // ));
        
        if(empty($from)){
            $admin_detail = admin_detail();
            $from = $admin_detail['email'];
        }

       
        $this->email->from($from, 'Print It');
        $this->email->to($to);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject($subject);
        $this->email->set_mailtype('html');
        $this->email->message($message);


        $mailsent = $this->email->send();

        if($mailsent ==TRUE ){
            $return  =  "TRUE" ;
        }else{
            $return  =$this->email->print_debugger(array('headers'));
        }
        return $return ;
    
    }


}
?>