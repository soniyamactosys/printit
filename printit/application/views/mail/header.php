<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>

    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 

   
    <style>

	   
/*BUTTON*/
.btn{
	padding: 5px 20px;
	display: inline-block;
}
.btn.btn-primary{
	border-radius: 3px;
	background: #29A9CC;
	color: #ffffff;
}


h1,h2,h3,h4,h5,h6{
	font-family: 'Poppins', sans-serif;
	color: #000000;
	margin-top: 0;
	font-weight: 400;
}

body{
	font-family: 'Muli', sans-serif;
	font-weight: 400;
	font-size: 15px;
	line-height: 1.8;
	color: #808080; 
}

a{
	color: #29A9CC; text-decoration:none;
}

table{width:100%;
}
/*LOGO*/

.logo a {
    color: #fff;
    text-transform: capitalize;
}



/*INTRO*/

.bg_white {
    background: #fff;
}
.bg_primary {
    background: #FB5C17;
}
.intro .text h2{
	color: #000;
	font-size: 34px;
	margin-bottom: 0;
	font-weight: 300;
}
.intro .text h2 span{
	font-weight: 600;
	color: #ffc0d0;
}



ul.social{
	padding: 0; margin:0;
}
ul.social li{
	display: inline-block;
	margin-right: 10px;
}




@media screen and (max-width: 500px) {


}


    </style>


</head>

<body width="100%" style="font-family: 'Muli', sans-serif;
	font-weight: 400;
	font-size: 15px;
	line-height: 1.8;
	color: #808080; margin: 0; padding: 0 !important; background-image:url(<?php echo base_url('webassets/mail_images') ?>/main_bg.jpg); background-size: cover;">
	<center style="width: 100%;">
    
    <div style="max-width: 800px; margin: 30px auto 0;" class="email-container">
    	<!-- BEGIN BODY -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td valign="top" class="" style="padding: 20px; text-align: center;">
          	<h1 class="logo" style="	font-family: 'Poppins', sans-serif;
	color: #000000;
	margin-top: 0;
	font-weight: 400;margin:0 "><a href="#"><img src="<?php echo base_url('webassets/mail_images') ?>/logo.png" width="200px"></a></h1>
          </td>
	      </tr><!-- end tr -->
	      
	     
				<tr>
          <td valign="middle" class="intro bg_white" style="background: #fff;padding: 2em 0 4em 0;">
            <table>
            	<tr>
            		<td>
            			<div class="text" style="padding: 0 2.5em; text-align: center;">