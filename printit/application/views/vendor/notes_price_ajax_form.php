<div class="price-table-box" id="tableDiv">
<input type="hidden" name="maxval" id="maxval" value="<?php if(!empty($notes_price)){ echo max(array_column($notes_price,'copy_to')); }else{ echo 0; }  ?>" />
<button onclick="add_more_notes_price()" type="button" class="btn btn-success float-right mb-10"><i class="fa fa-plus">Add More</i></button>
<br>
<table class="table table-hover">
  <thead>
    <tr>
      <th>Page From</th>
      <th>Page To</th>
      <th>Color</th>
     <th>Normal price printery</th>
        <th>Express price printery</th>
      <th></th>
    </tr>
  </thead>
  <tbody id="notes_price_table">
    <?php 
     if(!empty($notes_price)){ 
      
      foreach($notes_price as $combi){ 
      
      $key=$combi['npr_id'];
      ?>
      <tr>
        <td> <?php echo $combi['copy_from']; ?></td>
        <td> <?php echo $combi['copy_to']; ?> </td>      
        <td> <?php if($combi['color'] == 'Black_and_White'){ echo 'Black & White'; } else{ echo 'Colored'; } ?> </td>      
        <td><input name="price[<?php echo $key; ?>]" value="<?php echo empty($combi['price'])?'0':$combi['price'] ; ?>" type="number" step=".01" class="form-control price"> 
          <input type="hidden" name="copy_from[<?php echo $key; ?>]" value="<?php echo $combi['copy_from']; ?>"> 
          <input type="hidden" name="copy_to[<?php echo $key; ?>]" value="<?php echo $combi['copy_to']; ?>"> 
          <input type="hidden" value="<?php echo $combi['color']; ?>" id="color"> 
          <input type="hidden" name="npr_id[<?php echo $key; ?>]" value="<?php echo $combi['npr_id']; ?>"> 
        </td>      
         <td><input name="exp_price[<?php echo $key; ?>]" value="<?php echo empty($combi['exp_price'])?'0':$combi['exp_price'] ; ?>" type="number" step=".01" class="form-control exp_price"> </td>   
         <td></td>   
      </tr>
      <?php }  

    } ?>
    
  </tbody>
</table>


</div>
<span id="common_err" class="text-danger text-center error" style="display:none;"></span>
<div class="price-update-btn">
   <button type="button"  id="update_printery_price" >update</button>
</div>
<script type="text/javascript">
var e_count = $('.c_to').length;
  function add_more_notes_price(){
         e_count++;
   var c_to_input = $('.c_to').length;
   if(c_to_input>0){
      var copyto =   $('.c_to').last().val();
   }else{
    var copyto = $('#maxval').val();
   }
   
    var colors = $('#color').val();
    if(colors == 'Black_and_White'){
        color = 'Black & White'; 
    }else{
       color = 'Colored';  
    }
   
    copyfrom = parseInt(copyto)+1;
    copy_to = parseInt(copyto)+100;
    
    $('#notes_price_table').append('<tr class="newtr"><td><input type="number" step="1" class="form-control c_from" name="copy_from[]" value="'+copyfrom+'" min="'+copyfrom+'" id="copy_from'+e_count+'" e_count="'+e_count+'"></td><td><input type="number" step="1" class="form-control c_to" name="copy_to[]" value="'+copy_to+'" id="copy_to'+e_count+'"  e_count="'+e_count+'"></td><td>'+color+'</td><td><input name="price[]" step="0.1" value="" type="number"  class="form-control price"></td><td><input name="exp_price[]" step="0.1"  value="" type="number" class="form-control exp_price"></td><td><button class="btn btn-danger removenote_price"><i class="fa fa-times"></i></button></td></tr>');
  }
  
  $(document).on('blur','.c_from',function(){
      var e_count = $(this).attr('e_count');
      var next_copyto = $("#copy_to"+e_count);
       e_count--;
      var dsval= $(this).val();
      if(e_count>0){
        var prevval= $("#copy_to"+e_count).val();
          
      }else{
        var prevval= $('#maxval').val();  
      }
      prevval2 = parseInt(prevval)+1;
    if(parseInt(dsval) <= parseInt(prevval) || parseInt(dsval) > parseInt(prevval2)){
        //$(this).css('border-color','red');
        $(this).val(parseInt(prevval)+1);
        console.log('error');
    }else{
        
        $(next_copyto).val(parseInt(dsval)+100);
        
    }

  });
  
    $(document).on('blur','.c_to',function(){
        var e_count = $(this).attr('e_count');
        var copyfrom = $("#copy_from"+e_count).val();
        var dsval= $(this).val();

    if(parseInt(dsval) <= parseInt(copyfrom)){
        $(this).val(parseInt(copyfrom)+99);
    }
    
    // $('.c_from').each(function(){
    //     var newfrom = 
    //     $(this).val()
    // });

  });

$(document).on('click','.removenote_price',function(){
    $(this).parents('tr').remove();
});
</script>