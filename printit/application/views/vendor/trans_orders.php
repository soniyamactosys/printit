<?php //$orders = $this->session->userdata('trans_order'); 

//print_r($orders); exit;
?>

  <div class="content-wrapper">
	  <div class="container-full">
		<section class="content">
		  <div class="row">
			<div class="col-12">
			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Orders </h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">
						<?php /*
					<div class="search_panel_outer container-fluid">
						<form action="" method="post">
						<div class="row">
							<div class="col-md-3">
								<label>Buyer</label>
								<input type="text" value="<?php if(!empty($_POST['buyer'])){ echo $_POST['buyer']; } ?>" name="buyer" id='buyer' class="form-control filter_class" placeholder='Buyer'>
							</div>
							<div class="col-md-3">
								<label>Vendor</label>
								<input type="text" value="<?php if(!empty($_POST['vendor'])){ echo $_POST['vendor']; } ?>" name="vendor" id='vendor' class="form-control filter_class" placeholder='Vendor'>
							</div>
							<div class="col-md-3">
								<label>Order From</label>
								<input type="date" value="<?php if(!empty($_POST['order_from'])){ echo $_POST['order_from']; } ?>" name="order_from" id='order_from' class="form-control filter_class datepicker" placeholder='Order From'>
							</div>
							<div class="col-md-3">
								<label>Order To</label>
								<input type="date" value="<?php if(!empty($_POST['order_to'])){ echo $_POST['order_to']; } ?>" name="order_to" id='order_to' class="form-control filter_class datepicker" placeholder='order _to'>
							</div>

                          <div class="col-md-3">
                               <input type='submit' class="waves-effect waves-light btn btn-primary mb-5" value="Search">
                                <a href="<?php echo site_url('admin/orders'); ?>"  class="waves-effect waves-light btn btn-secondary mb-5">Clear</a>
							</div>
						</div>
					</form>
					</div>
					*/ ?>

					
<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Order Number</th>
								<th>Project Name</th>
								<th>Status</th>
								<th>Price</th>
								<th>Action</th>
							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($orders)){ foreach($orders as $value){ $i++; ?>
							<tr>

								<td><?php echo $i;?></td>
								<td><?php echo date('d-m-Y',strtotime($value['created_at'])); ?></td>
								<td><?php echo $value['order_id'];?></td>
								<td><?php echo $value['project_name'];?></td>
								<td><?php echo str_replace("_"," ",$value['status']); ?></td>
								<td><?php echo $value['price'];?></td>
								
<td>
	<a href="<?php echo site_url('vendor/trans_order_detail/').encoding($value['order_id']); ?>" class="btn btn-primary btn-sm" style="margin:5px;"><i class="fa fa-eye" style="font-size:12px" aria-hidden="true"></i></a>
<?php /*
<div class="dropdown" oid="<?php echo $value['order_number'];  ?>">
	<a data-toggle="dropdown" href="javascript:void(0)" class="statusbtn" aria-expanded="false">Status</a>
	<div class="dropdown-menu dropdown-menu-right" style="">
		<?php if($value['order_type']=='1'){ ?>
			<a class="dropdown-item orderStatusBtn" href="javascript:void(0)" status="being_printed"><i class="si-check si"></i> Being Printed</a>
		<?php }elseif($value['order_type']=='3'){ ?>
			<a class="dropdown-item orderStatusBtn" href="javascript:void(0)" status="awaiting_translation_proposals"><i class="si-check si"></i> Awaiting Translation Proposals</a>
		<?php } ?>
	  <a class="dropdown-item orderStatusBtn" href="javascript:void(0)" status="ready_for_pickup"><i class="si-clock si"></i> Ready for Pickup</a>
	  <a class="dropdown-item orderStatusBtn" href="javascript:void(0)" status="delivered"><i class="si-close si"></i> Delivered</a>
	  
	</div>
</div>


*/ ?>
								</td> 
								

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Order Number</th>
								<th>Project Name</th>
								<th>Status</th>
								<th>Price</th>								
								<th>Action</th>
							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->
<?php echo form_open('vendor/update_order_status',array('id'=>'order_status_form')); ?>
<input type="hidden" name="oid" id="form_oid">
<input type="hidden" name="status" id="form_status">
<?php echo form_close();
 ?>
		</section>
	  </div>
  </div>
<!-- /.content-wrapper -->