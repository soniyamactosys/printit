<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
		  <div class="row">
			<div class="col-12">
			  <!-- /.box -->
			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Branches</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12"> <a href="<?php echo site_url('vendor/add_branch'); ?>"  class="btn btn-success mt-10 float-right">+ Add New</a></div>
<?php /*
					<div class="search_panel_outer container-fluid">
						
						<form id="search_printery" action="<?php //echo site_url('filter_printeryshop'); ?>" method="post">
							<div class="row">
								<div class="col-md-3">
									 <label>Shop Name</label>

	                              <input type="text" value="<?php if(!empty($_POST['shop_name'])){ echo $_POST['shop_name']; } ?>" name="shop_name" id='shop_name' class="form-control filter_class" placeholder='Shop Name'>
								</div>
								<div class="col-md-3">
									 <?php if(empty($this->uri->segment('3'))){ ?>

	                       

	                               <label>Status</label>

	                              <!--<input type="text" name="status" id='status' class="form-control filter_class" placeholder='Status'>-->

	                              <select  name="status" class="selectpicker filter_class">

	                                  <option value="" selected="">Status</option>

	                                  <option value="0" <?php if(!empty($_POST['status']) && $_POST['status']==0 ){ echo "selected"; } ?>>Active</option>

	                                  <option value="1"  <?php if(!empty($_POST['status']) && $_POST['status']==1 ){ echo "selected"; } ?>>In-Active</option>

	                                  <option value="3"  <?php if(!empty($_POST['status']) && $_POST['status']==3 ){ echo "selected"; } ?>>Pending</option>

	                                  <option value="2"  <?php if(!empty($_POST['status']) && $_POST['status']==2 ){ echo "selected"; } ?>>Blocked</option>

	                              </select>


	                           <?php  } ?>
								</div>
								<div class="col-md-3">
									<label>Registration Date</label>

	                              <input type="date" name="registeration_date" id='registeration_date' class="datepicker form-control filter_class" placeholder='From date' value="<?php if(!empty($_POST['registeration_date'])){ echo $_POST['registeration_date']; } ?>">
	                          </div>
	                          <div class="col-md-3">
	                               <input type='submit' id="" class="waves-effect waves-light btn btn-primary mb-5" value="Search">
	                                <a href="<?php echo site_url('printery_list'); ?>"  class="waves-effect waves-light btn btn-secondary mb-5">Clear</a>
								</div>
							</div>
						</form>
						
					</div>
*/ ?>

					
<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Branch Name</th>

<!-- 								<th>P.O. Box</th>

								<th>Block</th> -->

								<th>Address</th>

								<th>Contact</th>

								<th>Working Hours</th>

								<th>Status</th>

								<th>Registration Date</th>

								<th>Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($branches)){ foreach($branches as $value){ $i++; ?>

						    

							<tr>

								<td><?php echo $i;?></td>

								<td><?php echo $value['shop_name'];?></td>

								<!-- <td><?php echo $value['po_box'];?></td> -->

								<!-- <td><?php echo $value['block'];?></td> -->

								<td><?php echo $value['po_box'].' '.$value['building'];?></td>

								<!-- <td><?php echo $value['flat'];?></td> -->

								<td><?php echo $value['contact'];?></td>

								<td><?php echo date('h:i A',strtotime($value['start_time'])).' '.date('h:i A',strtotime($value['end_time'])) ;?></td>

								<td><?php if($value['shop_status']=='0'){ echo 'Active'; }elseif($value['shop_status']=='1'){ echo 'In-Active'; }elseif($value['shop_status'] =='2'){ echo 'Blocked'; }else{ echo 'Pending';} ?></td>

								<td><?php echo date('d-m-Y',strtotime($value['registeration_date'])); ?></td>

								<td>
									<!-- <a href="<?php echo site_url('vendor_detail/').encoding($value['user_id']); ?>" name="update" class="btn btn-warning btn-sm mr-5" id="upd"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
									<a href="<?php echo site_url('vendor/edit_branch/').encoding($value['id']); ?>" class="btn btn-primary btn-sm"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('vendor/delete_branch/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>

								<!-- <a href="javascript:void(0)" class="btn btn-primary btn-sm shopchangestatus" style="margin:5px;"><i class="fa fa-close" style="font-size:12px" aria-hidden="true"></i></a> -->
							</td>

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Branch Name</th>

<!-- 								<th>P.O. Box</th>

								<th>Block</th> -->

								<th>Address</th>

								

								<th>Contact</th>

								<th>Working Hours</th>

								<th>Status</th>

								<th>Registration Date</th>

								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>

  <!-- /.content-wrapper -->