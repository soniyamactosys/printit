<h1>vendor registeration form</h1>
<form action="<?php echo site_url('vendor_registeration') ?>" method="post" id="vendor_registeration">

<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="firstname" name="firstname" class="form-control pl-15 bg-transparent" placeholder="firstname">
    </div>
    <span id="fname_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your firstname</span>
</div>


<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="lastname" name="lastname" class="form-control pl-15 bg-transparent" placeholder="lastname">
    </div>
    <span id="lname_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your lastname</span>
</div>

<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="contact_no" name="contact_no" class="form-control pl-15 bg-transparent" placeholder="Contact Number">
    </div>
    <span id="contact_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Contact Number</span>
</div>


<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="email" name="email" class="form-control pl-15 bg-transparent" placeholder="Email">
    </div>
    <span id="email_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your Email Address</span>
</div>



<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="shop_name" name="shop_name" class="form-control pl-15 bg-transparent" placeholder="shop name">
    </div>
    <span id="shopname_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your shop name</span>
</div>

<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="po_box" name="po_box" class="form-control pl-15 bg-transparent" placeholder="PO box">
    </div>
    <span id="pobox_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your PO Box</span>
</div>


<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="block" name="block" class="form-control pl-15 bg-transparent" placeholder="Block">
    </div>
    <span id="contact_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Block</span>
</div>






<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="flat" name="flat" class="form-control pl-15 bg-transparent" placeholder="Flat">
    </div>
    <span id="contact_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Flat</span>
</div>



<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="start_time" name="start_time" class="form-control pl-15 bg-transparent" placeholder="start time">
    </div>
    <span id="contact_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter start time</span>
</div>

<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
    </div>
    <input type="text" id="end_time" name="end_time" class="form-control pl-15 bg-transparent" placeholder="end time">
    </div>
    <span id="contact_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter end time</span>
</div>
<div class="form-group">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
    </div>
    <input type="password" id="pass" name="pass" class="form-control pl-15 bg-transparent" placeholder="Password">
    </div>
    <span id="pass_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your Password</span>
    <span id="validation_err" class="text-danger invalidText" style="display:none;font-weight:bold;"></span>
</div>


	<div class="col-12 text-center">
										  <button type="submit" class="btn btn-danger mt-10">SIGN IN</button>
										</div>

</form>	
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>	
	<script src="<?php echo base_url('assets/js/loaderjs.js'); ?>"></script>

<script>
$(document).on('submit','#vendor_registeration',function (e) {
	e.preventDefault();
    var allIsOk = true;
	var email = $("#email").val().trim();
	$('.invalidText').hide();

// 	if (email == '') {
// 		$("#email").focus();
// 		$("#email_err").show().html('Please Enter Your Email Address');
//         allIsOk = false;
//     }else if(validateEmail(email)==false){
// 		$("#email").focus();
// 		$("#email_err").show().html('Please Enter Valid Email Address');
//         allIsOk = false;        
//     }
    
//     var pass = $("#pass").val();
// 	if (pass == '') {
// 		$("#pass").focus();
// 		$("#pass_err").show();
//         allIsOk = false;
//     }
    
    if(allIsOk){
    	ds = $(this);
		var url = $(this).attr('action');
		var formData = new FormData(ds[0]);
		$.ajax({
			url: url,
			type: "POST",
			data:formData,
    		dataType: "json",
		    processData: false,
		    contentType: false,			
			beforeSend:function(){
				ajaxindicatorstart();
			},
			success: function(res) {
				ajaxindicatorstop();
				if(res.success) {
					window.location.href = res.redirect;
						$("#validation_err").show().html(res.msg);
				}else{
					$("#validation_err").show().html(res.msg);
					allIsOk = false;
				}
			}
		});
    }
    return allIsOk    
});

</script>