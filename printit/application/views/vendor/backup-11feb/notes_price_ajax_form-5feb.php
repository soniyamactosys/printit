<div class="price-table-box" id="tableDiv">
<button onclick="add_more_notes_price()" type="button" class="btn btn-success float-right"><i class="fa fa-plus">Add More</i></button>
<br>
<table class="table table-hover">
  <thead>
    <tr>
      <th>Page From</th>
      <th>Page To</th>
      <th>Price</th>
    </tr>
  </thead>
  <tbody id="notes_price_table">
    <?php 
     if(!empty($notes_price)){ 
      
      foreach($notes_price as $combi){ 
      
      $key=$combi['npr_id'];
      ?>
      <tr>
        <td> <?php echo $combi['copy_from']; ?></td>
        <td> <?php echo $combi['copy_to']; ?> </td>      
        <td><input name="price[<?php echo $key; ?>]" value="<?php echo empty($combi['price'])?'0':$combi['price'] ; ?>" type="number" step=".01" class="form-control price"> 
          <input type="hidden" name="copy_from[<?php echo $key; ?>]" value="<?php echo $combi['copy_from']; ?>"> 
          <input type="hidden" name="copy_to[<?php echo $key; ?>]" value="<?php echo $combi['copy_to']; ?>"> 
          <input type="hidden" name="npr_id[<?php echo $key; ?>]" value="<?php echo $combi['npr_id']; ?>"> 
        </td>      
      </tr>
      <?php }  

    } ?>
    
  </tbody>
</table>


</div>
<span id="common_err" class="text-danger text-center error" style="display:none;"></span>
<div class="price-update-btn">
   <button type="submit" >update</button>
</div>
<script type="text/javascript">
  function add_more_notes_price(){
    $('#notes_price_table').append('<tr class="newtr"><td><input type="number" class="form-control c_from" name="copy_from[]" value=""></td><td><input type="number" class="form-control c_to" name="copy_to[]" value=""></td><td><input name="price[]" value="" type="number" step=".01" class="form-control price"><button class="btn btn-danger"><i class="fa fa-times"></i></button></td></tr>');
  }
</script>