<?php $j = $start-1; for($i=1;$i<=$total;$i++){ ?>
<div class="newAppended branchSubDiv" style="padding-bottom: 25px;">
  <h3 class="multisteps-form__title" >Branch <?php echo $start; ?>.</h3>
  <div class="form-row mt-4">
    <div class="col-12 col-sm-6">
      <span>Branch Name:</span>
      <input class="multisteps-form__input form-control branch_name" type="text" placeholder="Branch Name"  name="shop_name[<?php echo $j; ?>]"  value="" />
      <span class="form-text error text-danger branch_name_err" id="c_shop_name_err" style="display:none;">Please Enter Branch name</span>
    </div>
    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
      <span>Branch Contact Number:</span>
      <input class="multisteps-form__input form-control b_contact" type="text" placeholder="Contact Number"  value="" name="mobile[<?php echo $j; ?>]"  maxlength="13" />
      <span class="form-text error text-danger b_contact_err"  style="display:none;">Please Enter Contact Number</span>
    </div>
  </div>    
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Number of Workers:</span>
        <select class="multisteps-form__select form-control b_worker_count" name="worker_count[<?php echo $j; ?>]" >
          <option value="">Please Select</option>
          <?php for($f=1;$f<=50;$f++){ ?>
            <option value="<?= $f; ?>"  ><?= $f; ?></option>
          <?php } ?>
        </select> 
        <span class="form-text error text-danger b_worker_err"  style="display:none;">Please Enter Number of Workers</span>
      </div>
      <div class="col-12 col-sm-6 b_addressDiv" >
        <label class="">Address</label>
        <input class="multisteps-form__input form-control b_address" id="test<?php echo $i; ?>"  type="text" name="address[<?php echo $j; ?>]"  value="" placeholder="Address" >
        <input type="hidden" name="latitude[<?php echo $j; ?>]" class="b_lat" id="testlat<?php echo $i; ?>">
        <input type="hidden" name="longitude[<?php echo $j; ?>]" class="b_lng" id="testlon<?php echo $i; ?>">
        <span class="form-text error text-danger b_address_err"  style="display:none;">Please Enter Address</span>
      </div>                 
    </div>                 


    <div class="form-row mt-4">
      <div class="col-12 col-sm-6">
        <span>P.O. Box :</span>
        <input class="multisteps-form__input form-control po_box" type="text" placeholder="P.O. Box"  value="" name="po_box[<?php echo $j; ?>]"  maxlength="13" />
      </div> 
      <div class="col-6 col-sm-3 mt-4 mt-sm-0">
        <label class="">Start Time</label>
        <input class="multisteps-form__input form-control b_start_time" type="time" name="start_time[<?php echo $j; ?>]"  value="">
        <span class="form-text error text-danger b_start_time_err"  style="display:none;">Please Enter Start Time</span>
      </div>
      <div class="col-6 col-sm-3 mt-4 mt-sm-0">
        <label class="">End Time</label>
        <input class="multisteps-form__input form-control b_end_time" type="time" name="end_time[<?php echo $j; ?>]"  value="">
        <span class="form-text error text-danger b_end_time_err"  style="display:none;">Please Enter End Time</span>
      </div>                 
    </div>
    <div class="form-row mt-4">
      <div class="col">
        <label class="">Services</label>
        <div class="services-area">
          <div class="service-checkbox-area">
          <?php 
          //$myservices = explode(',',$myprofile['services']);
          if(!empty($services)){ foreach($services as $value){ ?>
            <label class="container  tooltip-container"><?php echo $value['name']; ?>
              <input type="checkbox"  name="c_services[<?= $j; ?>][]" class="b_services" value="<?php echo $value['id']; ?>" >
              <span class="checkmark"></span>
              <?php if(!empty($value['description'])){ ?><span class="tooltip"><?php  echo $value['description']; ?> </span><?php } ?>
            </label>
          <?php } } ?>
          </div>
          <span class="form-text error text-danger b_service_err"  style="display:none;">Please Select services</span>
        </div>
      </div>
    </div>
    <br>
    <h3 class="multisteps-form__title">Incharge Person Details</h3>
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6">
        <span>First Name</span>
        <input class="multisteps-form__input form-control b_in_fname" type="text" placeholder="First Name"  name="c_in_fname[<?php echo $j; ?>]"  value="" />
        <span class="form-text error text-danger b_in_fname_err"  style="display:none;">Please Enter First name</span>
      </div>
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Last Name</span>
        <input class="multisteps-form__input form-control b_in_lname" type="text" placeholder="Last Name"  name="c_in_lname[<?php echo $j; ?>]"  value=""/>
        <span class="form-text error text-danger b_in_lname_err"  style="display:none;">Please Enter Last name</span>
      </div>
    </div>
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Email:</span>
        <input class="multisteps-form__input form-control b_in_email" type="email" placeholder="Email"  value="" name="c_in_email[<?php echo $j; ?>]"  />
        <span class="form-text error text-danger b_in_email_err"  style="display:none;">Please Enter Email Address</span>                    
      </div>
      <div class="col-12 col-sm-6">
        <span>Mobile Number:</span>
        <input class="multisteps-form__input form-control b_in_mobile" type="text" placeholder="Contact Number"  value="" name="c_in_mobile[<?php echo $j; ?>]"   maxlength="13" />
        <span class="form-text error text-danger b_in_mobile_err"  style="display:none;">Please Enter Mobile Number</span>                    
      </div>                  
    </div>
</div>
<?php $j++; $start++; } ?>