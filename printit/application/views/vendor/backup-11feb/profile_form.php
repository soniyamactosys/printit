<?php //echo "<pre>"; print_r($myprofile); exit; ?>
<input type="hidden" id="registerd_branches" value="<?php if(!empty($myprofile['total_branches'])){ echo $myprofile['total_branches']; }else{ echo 0; } ?>">
  <div class="content-wrapper">

	  <div class="container-full">

		<!-- Main content -->

		<section class="content">
			

<div class="content">
  <!--content inner-->
  <div class="content__inner">
  
    <div class="container ">
      <!--multisteps-form-->
      <div class="multisteps-form">
        <!--progress bar-->
        <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto mb-4">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Basic Details</button>

              <button class="<?= empty($myprofile['total_branches'])?'':'multisteps-form__progress-btn';  ?>" type="button" title="Address" id="branchesBtn" style="display:<?= empty($myprofile['total_branches'])?'none':'block';  ?>" >Branches Detail</button>
              <button class="multisteps-form__progress-btn" type="button" title="Address"> Bank Account Detail</button>
              <!-- <button class="multisteps-form__progress-btn" type="button" title="Order Info">Password</button>  -->
             
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-12 m-auto">
            <!-- <form class="multisteps-form__form"> -->
            <div class="multisteps-form__form">
              <!--single form panel-->
              <?php  echo form_open('vendor/edit_company_detail',array('id'=>'update_company_profile'));  ?>
                <input type="hidden" name="mainprintery_id" value="<?php if(!empty($myprofile['printery_id'])){ echo $myprofile['printery_id']; } ?>">
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Company Details</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>First Name:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="First Name"  name="fname" id="c_fname" value="<?php echo $myprofile['owner_fname']; ?>" />
                      <span class="form-text error text-danger" id="c_fname_err" style="display:none;">Please Enter Company Owner First name</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Last Name:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Last Name"  name="lname" id="c_lname" value="<?php echo $myprofile['owner_lname']; ?>"/>
                      <span class="form-text error text-danger" id="c_lname_err" style="display:none;">Please Enter Company Owner Last name</span>
                    </div>
                  </div>                  
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>Company Name:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Company Name"  name="shop_name" id="c_shop_name" value="<?php echo $myprofile['shop_name']; ?>" />
                      <span class="form-text error text-danger" id="c_shop_name_err" style="display:none;">Please Enter Company name</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Number Of Branches:</span>
                      <select class="multisteps-form__select form-control" name="total_branches" id="total_branches">
                        <?php for($f=0;$f<=20;$f++){ ?>
                          <option value="<?= $f;  ?>" <?php if($f==$myprofile['total_branches']){ echo 'selected'; } ?> ><?= $f;  ?></option>
                        <?php } ?>
                      </select>
                      <span class="form-text error text-danger" id="branch_total_err" style="display:none;">Please Enter How many branches you have</span>
                    </div>
                  </div>    
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>Registeration Number:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Registeration Number"  name="reg_number" id="reg_number" value="<?php echo $myprofile['reg_number']; ?>" />
                      <span class="form-text error text-danger" id="reg_number_err" style="display:none;">Please Enter Registeration Number</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Registeration Proof:</span>
                      <input type="file" class="multisteps-form__input" name="registery_file" id="registery_file" accept=".jpg,.pdf, .jpeg">
                      <br><small>allowed type pdf or jpg or jpeg</small>
                      <span class="form-text error text-danger" id="registery_file_err" style="display:none;">Please Upload registeration Proof</span>
                    </div>
                  </div>   
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>Company Logo:</span><br>
                      <input type="file" name="company_logo" id="company_logo"  accept=".jpg,.png,.jpeg" />
                      <br><small>allowed type jpg, jpeg, png</small>
                      <span class="form-text error text-danger" id="logo_err" style="display:none;">Please Upload company images</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Company Images:</span>
                      <input type="file" multiple="" name="company_images[]" id="company_images" accept=".jpg,.jpeg,.png" />
                      <br><small>allowed type jpg, jpeg, png</small>
                      <span class="form-text error text-danger" id="company_images_err" style="display:none;">Please Upload company images</span>
                    </div>
                  </div>    
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Company Email:</span>
                      <input class="multisteps-form__input form-control" type="email" placeholder="Email"  value="<?php echo $myprofile['c_email']; ?>" onkeydown="return false;" />
                    </div>
                    <div class="col-12 col-sm-6">
                      <span>Company Contact Number:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Contact Number"  value="<?php echo $myprofile['c_contact']; ?>" name="mobile" id="c_contact" maxlength="13" />
                      <span class="form-text error text-danger" id="c_contact_err" style="display:none;">Please Enter Contact Number</span>
                    </div>                  
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Number of Workers:</span>
                      <select class="multisteps-form__select form-control" name="worker_count" id="c_worker_count">
                        <option value="">Please Select</option>
                        <?php for($f=1;$f<=50;$f++){ ?>
                          <option value="<?= $f;  ?>" <?php if($f==$myprofile['total_workers']){ echo 'selected'; } ?> ><?= $f;  ?></option>
                        <?php } ?>
                      </select> 
                      <span class="form-text error text-danger" id="c_worker_err" style="display:none;">Please Enter Number of Workers</span>
                    </div>
                    <div class="col-12 col-sm-6">
                      <label class="">Address</label>
                      <input class="multisteps-form__input form-control" type="text" name="address" id="c_address" value="<?php echo $myprofile['building']; ?>" placeholder="Address" >
                      <input type="hidden" name="latitude" id="c_lat">
                      <input type="hidden" name="longitude" id="c_lng">
                      <span class="form-text error text-danger" id="c_address_err" style="display:none;">Please Enter Address</span>
                    </div>                 
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>P.O. Box :</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="P.O. Box"  value="<?php echo $myprofile['po_box']; ?>" name="po_box" id="po_box" maxlength="13" />
                    </div> 
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="">Start Time</label>
                      <!-- <input class="multisteps-form__input form-control timepicker" type="text" name="start_time" id="c_start_time" value="<?php echo date('H:i',strtotime($myprofile['start_time'])); ?>">
                       -->
                      <input class="multisteps-form__input form-control" type="time" name="start_time" id="c_start_time" value="<?php echo date('H:i',strtotime($myprofile['start_time'])); ?>">
                      <span class="form-text error text-danger" id="c_start_time_err" style="display:none;">Please Enter Start Time</span>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="">End Time</label>
                      <input class="multisteps-form__input form-control" type="time" name="end_time" id="c_end_time" value="<?php echo date('H:i',strtotime($myprofile['end_time'])); ?>">
                      <span class="form-text error text-danger" id="c_end_time_err" style="display:none;">Please Enter End Time</span>
                    </div>                 
                  </div>
                  <div class="form-row mt-4">
                    <div class="col">
                      <label class="">Services at Main Branch</label>
                      <div class="services-area">
                        <div class="service-checkbox-area">
                        <?php 
                        $myservices = explode(',',$myprofile['services']);
                        if(!empty($services)){ foreach($services as $value){ ?>
                          <label class="container tooltip-container"><?php echo $value['name']; ?>
                            <input type="checkbox"  name="c_services[]" class="c_services" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$myservices)){ echo 'checked'; } ?> >
                            <span class="checkmark"></span>
                            <?php if(!empty($value['description'])){ ?><span class="tooltip"><?php  echo $value['description']; ?> </span><?php } ?>
                          </label>
                        <?php } } ?>
                        </div>
                        <span class="form-text error text-danger" id="c_service_err" style="display:none;">Please Select services</span>
                      </div>
                    </div>
                  </div>
                  <br>
                  <h3 class="multisteps-form__title">Incharge Person Details</h3>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>First Name</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="First Name"  name="c_in_fname" id="c_in_fname" value="<?php if(!empty($myprofile['in_fname'])){ echo $myprofile['in_fname']; } ?>" />
                      <span class="form-text error text-danger" id="c_in_fname_err" style="display:none;">Please Enter First name</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Last Name</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Last Name"  name="c_in_lname" id="c_in_lname" value="<?php if(!empty($myprofile['in_lname'])){ echo $myprofile['in_lname']; } ?>"/>
                      <span class="form-text error text-danger" id="c_in_lname_err" style="display:none;">Please Enter Last name</span>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Email:</span>
                      <input class="multisteps-form__input form-control" type="email" placeholder="Email"  value="<?php if(!empty($myprofile['in_email'])){ echo $myprofile['in_email']; } ?>" name="c_in_email" id="c_in_email" />
                      <span class="form-text error text-danger" id="c_in_email_err" style="display:none;">Please Enter Email Address</span>                    
                    </div>
                    <div class="col-12 col-sm-6">
                      <span>Mobile Number:</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Contact Number"  value="<?php if(!empty($myprofile['in_mobile'])){ echo $myprofile['in_mobile']; } ?>" name="c_in_mobile" id="c_in_mobile"  maxlength="13" />
                      <span class="form-text error text-danger" id="c_in_mobile_err" style="display:none;">Please Enter Mobile Number</span>                    
                    </div>                  
                  </div>
                    <div class="error text-danger" id="comp_form_error"></div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-success ml-auto text-center" type="submit">Update</button>
                    <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                  </div>
                  
                </div>
              </div>
              <?php echo form_close(); 
               echo form_open('vendor/edit_company_branches',array('id'=>'update_branches'));
              ?>
              <!--single form panel-->
              <input type="hidden" name="current_branches" id="current_branches" value="<?= !empty($myprofile['total_branches'])?$myprofile['total_branches']:''; ?>">
              <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('vendordata')['user_id']; ?>">
 
              <div class="<?= empty($myprofile['total_branches'])?'':'multisteps-form__panel'; ?> shadow p-4 rounded bg-white" data-animation="scaleIn" id="branchesPanel" style="display:<?= empty($myprofile['total_branches'])?'none':'block';  ?>">
                
                <div class="multisteps-form__content" id="branchesDiv">
                  <div id="b_php_err" class="text-danger text-center error" style="display: none;"></div>
                  <?php if(!empty($myprofile['branches'])){ $j=0; $no = 0; foreach($myprofile['branches'] as $branch){  $no++; ?>
                      <div class="branchSubDiv" style="padding-bottom: 25px;">
                        <h3 class="multisteps-form__title" >Branch <?php echo $no; ?>.</h3>
                        <div class="form-row mt-4">
                          <div class="col-12 col-sm-6">
                            <span>Branch Name:</span>
                            <input class="multisteps-form__input form-control branch_name" type="text" placeholder="Branch Name"  name="shop_name[]"  value="<?= empty($branch['shop_name'])?'':$branch['shop_name']; ?>" />
                            <span class="form-text error text-danger branch_name_err" id="c_shop_name_err" style="display:none;">Please Enter Branch name</span>
                          </div>
                          <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                            <span>Branch Contact Number:</span>
                            <input class="multisteps-form__input form-control b_contact" type="text" placeholder="Contact Number"  value="<?= empty($branch['contact'])?'':$branch['contact']; ?>" name="mobile[]"  maxlength="13" />
                            <span class="form-text error text-danger b_contact_err"  style="display:none;">Please Enter Contact Number</span>
                          </div>
                        </div>    
                          <div class="form-row mt-4">
                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                              <span>Number of Workers:</span>
                              <select class="multisteps-form__select form-control b_worker_count" name="worker_count[]" >
                                <option value="">Please Select</option>
                                <?php for($f=1;$f<=50;$f++){ ?>
                                  <option value="<?= $f; ?>" <?php if(!empty($branch['total_workers']) && $branch['total_workers']==$f){ echo 'selected'; } ?>  ><?= $f; ?></option>
                                <?php } ?>
                              </select> 
                              <span class="form-text error text-danger b_worker_err"  style="display:none;">Please Enter Number of Workers</span>
                            </div>
                            <div class="col-12 col-sm-6">
                              <label class="">Address</label>
                              <input class="multisteps-form__input form-control b_address" type="text" name="address[]"  value="<?= empty($branch['building'])?'':$branch['building']; ?>" placeholder="Address" >
                              <input type="hidden" name="latitude" class="b_lat">
                              <input type="hidden" name="longitude" class="b_lng">
                              <span class="form-text error text-danger b_address_err"  style="display:none;">Please Enter Address</span>
                            </div>                 
                          </div>                 


                          <div class="form-row mt-4">
                            <div class="col-12 col-sm-6">
                              <span>P.O. Box :</span>
                              <input class="multisteps-form__input form-control po_box" type="text" placeholder="P.O. Box"  value="<?= empty($branch['po_box'])?'':$branch['po_box']; ?>" name="po_box[]"  maxlength="13" />
                            </div> 
                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                              <label class="">Start Time</label>
                              <input class="multisteps-form__input form-control b_start_time" type="time" name="start_time[]"  value="<?= empty($branch['start_time'])?'':date('H:i',strtotime($branch['start_time'])); ?>">
                              <span class="form-text error text-danger b_start_time_err"  style="display:none;">Please Enter Start Time</span>
                            </div>
                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                              <label class="">End Time</label>
                              <input class="multisteps-form__input form-control b_end_time" type="time" name="end_time[]"  value="<?= empty($branch['end_time'])?'':date('H:i',strtotime($branch['end_time'])); ?>">
                              <span class="form-text error text-danger b_end_time_err"  style="display:none;">Please Enter End Time</span>
                            </div>                 
                          </div>
                          <div class="form-row mt-4">
                            <div class="col">
                              <label class="">Services</label>
                              <div class="services-area">
                                <div class="service-checkbox-area">
                                <?php 
                                $myservices = explode(',',$branch['services']);
                                if(!empty($services)){ foreach($services as $value){ ?>
                                  <label class="container tooltip-container"><?php echo $value['name']; ?>
                                    <input type="checkbox"  name="c_services[<?= $j; ?>][]" class="b_services" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$myservices)){ echo 'checked'; } ?> >
                                    <span class="checkmark"></span>
                                    <?php if(!empty($value['description'])){ ?><span class="tooltip"><?php  echo $value['description']; ?> </span><?php } ?>
                                  </label>
                                <?php } } ?>
                                </div>
                                <span class="form-text error text-danger b_service_err"  style="display:none;">Please Select services</span>
                              </div>
                            </div>
                          </div>
                          <br>
                          <h3 class="multisteps-form__title">Incharge Person Details</h3>
                          <div class="form-row mt-4">
                            <div class="col-12 col-sm-6">
                              <span>First Name</span>
                              <input class="multisteps-form__input form-control b_in_fname" type="text" placeholder="First Name"  name="c_in_fname[]"  value="<?= empty($branch['in_fname'])?'':$branch['in_fname']; ?>" />
                              <span class="form-text error text-danger b_in_fname_err"  style="display:none;">Please Enter First name</span>
                            </div>
                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                              <span>Last Name</span>
                              <input class="multisteps-form__input form-control b_in_lname" type="text" placeholder="Last Name"  name="c_in_lname[]"  value="<?= empty($branch['in_lname'])?'':$branch['in_lname']; ?>"/>
                              <span class="form-text error text-danger b_in_lname_err"  style="display:none;">Please Enter Last name</span>
                            </div>
                          </div>
                          <div class="form-row mt-4">
                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                              <span>Email:</span>
                              <input class="multisteps-form__input form-control b_in_email" type="email" placeholder="Email"  value="<?= empty($branch['in_email'])?'':$branch['in_email']; ?>" name="c_in_email[]"  />
                              <span class="form-text error text-danger b_in_email_err"  style="display:none;">Please Enter Email Address</span>                    
                            </div>
                            <div class="col-12 col-sm-6">
                              <span>Mobile Number:</span>
                              <input class="multisteps-form__input form-control b_in_mobile" type="text" placeholder="Contact Number"  value="<?= empty($branch['in_mobile'])?'':$branch['in_mobile']; ?>" name="c_in_mobile[]"   maxlength="13" />
                              <span class="form-text error text-danger b_in_mobile_err"  style="display:none;">Please Enter Mobile Number</span>                    
                            </div>                  
                          </div>
                      </div>
                  <?php $j++; } }elseif(!empty($myprofile['total_branches'])){ 
                   $start = 1;
                    for($s=0;$s<$myprofile['total_branches'];$s++){ ?>
<div class="newAppended branchSubDiv" style="padding-bottom: 25px;">
  <h3 class="multisteps-form__title" >Branch <?php echo $s+1; ?>.</h3>
  <div class="form-row mt-4">
    <div class="col-12 col-sm-6">
      <span>Branch Name:</span>
      <input class="multisteps-form__input form-control branch_name" type="text" placeholder="Branch Name"  name="shop_name[]"  value="" />
      <span class="form-text error text-danger branch_name_err" id="c_shop_name_err" style="display:none;">Please Enter Branch name</span>
    </div>
    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
      <span>Branch Contact Number:</span>
      <input class="multisteps-form__input form-control b_contact" type="text" placeholder="Contact Number"  value="" name="mobile[]"  maxlength="13" />
      <span class="form-text error text-danger b_contact_err"  style="display:none;">Please Enter Contact Number</span>
    </div>
  </div>    
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Number of Workers:</span>
        <select class="multisteps-form__select form-control b_worker_count" name="worker_count[]" >
          <option value="">Please Select</option>
          <?php for($f=1;$f<=50;$f++){ ?>
            <option value="<?= $f; ?>"  ><?= $f; ?></option>
          <?php } ?>
        </select> 
        <span class="form-text error text-danger b_worker_err"  style="display:none;">Please Enter Number of Workers</span>
      </div>
      <div class="col-12 col-sm-6">
        <label class="">Address</label>
        <input class="multisteps-form__input form-control b_address" type="text" name="address[]"  value="" placeholder="Address" >
        <input type="hidden" name="latitude" class="b_lat">
        <input type="hidden" name="longitude" class="b_lng">
        <span class="form-text error text-danger b_address_err"  style="display:none;">Please Enter Address</span>
      </div>                 
    </div>                 


    <div class="form-row mt-4">
      <div class="col-12 col-sm-6">
        <span>P.O. Box :</span>
        <input class="multisteps-form__input form-control po_box" type="text" placeholder="P.O. Box"  value="" name="po_box[]"  maxlength="13" />
      </div> 
      <div class="col-6 col-sm-3 mt-4 mt-sm-0">
        <label class="">Start Time</label>
        <input class="multisteps-form__input form-control b_start_time" type="time" name="start_time[]"  value="">
        <span class="form-text error text-danger b_start_time_err"  style="display:none;">Please Enter Start Time</span>
      </div>
      <div class="col-6 col-sm-3 mt-4 mt-sm-0">
        <label class="">End Time</label>
        <input class="multisteps-form__input form-control b_end_time" type="time" name="end_time[]"  value="">
        <span class="form-text error text-danger b_end_time_err"  style="display:none;">Please Enter End Time</span>
      </div>                 
    </div>
    <div class="form-row mt-4">
      <div class="col">
        <label class="">Services</label>
        <div class="services-area">
          <div class="service-checkbox-area">
          <?php 
          //$myservices = explode(',',$myprofile['services']);
          if(!empty($services)){ foreach($services as $value){ ?>
            <label class="container  tooltip-container"><?php echo $value['name']; ?>
              <input type="checkbox"  name="c_services[<?= $s; ?>][]" class="b_services" value="<?php echo $value['id']; ?>" >
              <span class="checkmark"></span>
              <?php if(!empty($value['description'])){ ?><span class="tooltip"><?php  echo $value['description']; ?> </span><?php } ?>
            </label>
          <?php } } ?>
          </div>
          <span class="form-text error text-danger b_service_err"  style="display:none;">Please Select services</span>
        </div>
      </div>
    </div>
    <br>
    <h3 class="multisteps-form__title">Incharge Person Details</h3>
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6">
        <span>First Name</span>
        <input class="multisteps-form__input form-control b_in_fname" type="text" placeholder="First Name"  name="c_in_fname[]"  value="" />
        <span class="form-text error text-danger b_in_fname_err"  style="display:none;">Please Enter First name</span>
      </div>
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Last Name</span>
        <input class="multisteps-form__input form-control b_in_lname" type="text" placeholder="Last Name"  name="c_in_lname[]"  value=""/>
        <span class="form-text error text-danger b_in_lname_err"  style="display:none;">Please Enter Last name</span>
      </div>
    </div>
    <div class="form-row mt-4">
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        <span>Email:</span>
        <input class="multisteps-form__input form-control b_in_email" type="email" placeholder="Email"  value="" name="c_in_email[]"  />
        <span class="form-text error text-danger b_in_email_err"  style="display:none;">Please Enter Email Address</span>                    
      </div>
      <div class="col-12 col-sm-6">
        <span>Mobile Number:</span>
        <input class="multisteps-form__input form-control b_in_mobile" type="text" placeholder="Contact Number"  value="" name="c_in_mobile[]"   maxlength="13" />
        <span class="form-text error text-danger b_in_mobile_err"  style="display:none;">Please Enter Mobile Number</span>                    
      </div>                  
    </div>
</div>
                    <?php }
                  } ?>
                </div>
                <div class="button-row d-flex mt-4">
                    <button class="btn btn-success ml-auto text-center" type="submit">Update</button>
                    <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                  </div>
              </div>

              <?php echo form_close(); ?>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                <?php echo form_open('vendor/edit_bankdetail',array('id'=>'update_bankdetails')); ?>
                <h3 class="multisteps-form__title">Bank Account Detail</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>Account Holder Name</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Account Holder Name" name="acc_holder" id="acc_holder" value="<?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['acc_holder_name'];  ?>" />
                      <span class="form-text error text-danger" id="acc_holder_err" style="display:none;">Please Enter Account Holder Name</span>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Bank Name</span>
                      <input class="multisteps-form__input form-control" type="text" name="bank_name" id="bank_name" placeholder="Bank Name"  value="<?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['bank_name'];  ?>" />
                      <span class="form-text error text-danger" id="bank_name_err" style="display:none;">Please Enter Bank Name</span>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>Branch Name</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="Branch Name" name="branch_name" id="branch_name" value="<?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['branch_name'];  ?>" />
                      <span class="form-text error text-danger" id="branch_err" style="display:none;">Please Enter Branch Name</span>  
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <span>Account Number</span>
                      <input class="multisteps-form__input form-control" type="text" name="acc_number" id="acc_number" placeholder="Account Number" value="<?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['acc_number'];  ?>" />
                      <span class="form-text error text-danger" id="acc_no_err" style="display:none;">Please Enter Account Number</span>  
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <span>SWIFT Code</span>
                      <input class="multisteps-form__input form-control" type="text" placeholder="SWIFT Code" id="ifsc_code" name="ifsc_code" value="<?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['ifsc_code'];  ?>" />
                      <span class="form-text error text-danger" id="ifsc_err" style="display:none;">Please Enter SWIFT Code</span>   
                    </div>
                  </div>
                </div>
                <div class="button-row d-flex mt-4">
                   <!--  <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button> -->
                    
                    <button class="btn btn-primary ml-auto" type="submit" title="Next">Update</button>
                    <!-- <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button> -->
                  </div>
                   <?php echo form_close(); ?>
              </div>

              <div class="multisteps-form__panel shadow p-5 rounded bg-white" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Update Password</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <input class="multisteps-form__input form-control" type="password" placeholder="Old Password" name="old_password" />
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <input class="multisteps-form__input form-control" type="password" name="new_password" placeholder="New Password"/>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <input class="multisteps-form__input form-control" type="password" placeholder="Confirm Password" name="c_password" />
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                    </div>
                  </div>
                </div>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
		</section>

		<!-- /.content -->

	  </div>

  </div>


