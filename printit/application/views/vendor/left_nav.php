<aside class="main-sidebar">
  <!-- sidebar-->
	<section class="sidebar" style="height: auto;">	
		<div class="vendor-name-area">
		     <h4>Vendor</h4>
		     <h5><?php echo $this->vendordata['firstname'].' '.$this->vendordata['lastname'] ?></h5>
		 </div>
	  <!-- sidebar menu-->
	  <ul class="sidebar-menu tree" data-widget="tree">		
		<li class="<?php echo ($menu=='dashboard')?'active':''; ?>">
	      <a href="<?php echo site_url('vendor'); ?>">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Dashboard</span>
	      </a>
	    </li>
	    <li class="<?php echo ($menu=='trans_orders')?'active':''; ?>">
	      <a href="<?php echo site_url('vendor/trans_orders'); ?>">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Translation Orders</span>
	      </a>
	    </li>
	    <?php /*
		<li class="header">Shop Management</li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i></i>
	        <span>Printery Shop</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='approved_printery')?'active':''; ?>"><a href="<?php echo site_url('printery_list_approved'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop Approval</a></li>
		    <li class="<?php echo ($menu=='all_printery')?'active':''; ?>"><a href="<?php echo site_url('printery_list'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop List</a></li>
   		    <li class="<?php echo ($menu=='rejected_printery')?'active':''; ?>"><a href="<?php echo site_url('printery_list_rejected'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Shop Rejected List</a></li>
	      </ul>
	    </li>
		<li class="header">PRINT Settings</li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Master Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='paper_type')?'active':''; ?>"><a href="<?php echo site_url('paper_type_setting'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Paper Type</a></li>
			<li class="<?php echo ($menu=='paper_size')?'active':''; ?>"><a href="<?php echo site_url('paper_size_setting'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Paper Size</a></li>
			<li class="<?php echo ($menu=='extra_copies')?'active':''; ?>"><a href="<?php echo site_url('extra_copies'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Extra Copies</a></li>
			<li class="<?php echo ($menu=='print_type')?'active':''; ?>"><a href="<?php echo site_url('print_type'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Print Type</a></li>
	      </ul>
	    </li>	
	    <li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Average Cost Settings</span>
	      </a>
	    </li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Express Rate Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="banner.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Banner</a></li>
	        <li><a href="rollup.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Rollup</a></li>
	        <li><a href="poster.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Poster</a></li>
	        <li><a href="flyer.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Flyer</a></li>
	      </ul>
	    </li>
		<li class="header">ORDER MANAGEMENT</li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Express Order</span>
	      </a>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Normal Order</span>
	      </a>
	    </li>
		<li class="header">CUSTOMER MANAGEMENT</li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Active Customer</span>
	      </a>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Disable customer</span>
	      </a>
	    </li>
		<li class="header">DELIVERY MANAGEMENT</li>		
		<li><a href="#"><i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Zone Master</span>
	      </a>
	    </li>		
		<li>
	      <a href="zones.php">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Add Zone</span>
	      </a>
	    </li>
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Add Shipping Methods</span>
	      </a>
	    </li>		
		<li>
	      <a href="delivery-cost.php">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Delivery Cost</span>
	      </a>
	    </li>
		<li class="header">COUPON MANAGEMENT</li>		
		<li><a href="<?php echo site_url('Admin/coupons'); ?>"><i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Coupon</span>
	      </a>
	    </li>		
		<!--<li>-->
	 <!--     <a href="#">-->
	 <!--       <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>-->
		<!--	<span>Coupon Usage</span>-->
	 <!--     </a>-->
	 <!--   </li>-->
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Products</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li <?php echo ($menu=='products')?'active':''; ?>><a href="<?php echo site_url('Admin/products'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Product List</a></li>
        	<li><a href="#"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Product Stock</a></li>
	      </ul>
	    </li>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Print Management</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='print_management')?'active':''; ?>"><a href="<?php echo site_url('print_management'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Print Management</a></li>
	      </ul>
	    </li>	
	    <li class="header">PRINTERY MANAGEMENT</li>	
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Printery</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="display_printery.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Approval</a></li>
			<li><a href="printery_list.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery List</a></li>
			<li><a href="printeryrejectlist.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Printery Rejected List</a></li>
	      </ul>
	    </li>	
	    */ ?>
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Orders</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <!--<li class="<?php echo ($menu=='orders' || $menu=='order_detail')?'active':''; ?>"><a href="<?php echo site_url('vendor/orders'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Orders List</a></li>
        	<li class="<?php echo ($menu=='orders')?'active':''; ?>"><a href="<?php echo site_url('vendor/orders/complete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Completed Orders</a></li>-->
        	<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Normal orders</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='custom')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/customecomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Custom Print</a></li>
        	<li class="<?php echo ($menu=='quick')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/quickcomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Quick Print</a></li>
        	<li class="<?php echo ($menu=='notes')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/notescomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Notes</a></li>
	      </ul>
	    </li>
	    <li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Express orders</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='custom')?'active':''; ?> "><a href="<?php echo site_url('vendor/order/ecustomecomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Custom Print</a></li>
        	<li class="<?php echo ($menu=='quick')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/equickcomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Quick Print</a></li>
        	<li class="<?php echo ($menu=='translate')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/etranslatecomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Translate</a></li>
        	<li class="<?php echo ($menu=='notes')?'active':''; ?>"><a href="<?php echo site_url('vendor/order/notesallcomplete'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Notes</a></li>
	      </ul>
	    </li> </ul>
	    </li>
	    <li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Setting</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li class="<?php echo ($menu=='branches')?'active':''; ?>"><a href="<?php echo site_url('vendor/branches'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Branch List</a></li>
	      	
	        <?php /*<li class="<?php echo ($menu=='price_management')?'active':''; ?>"><a href="<?php echo site_url('vendor/price_management'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Price Management old</a></li>*/ ?>

	        <li class="<?php echo ($menu=='add_price')?'active':''; ?>"><a href="<?php echo site_url('vendor/add_price'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Price Management</a></li>
	      </ul>
	    </li>
	    <?php /*	
		<li class="treeview">
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
	        <span>Commission</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-right pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">			
	        <li><a href="<?php echo site_url('commission'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Commission</a></li>
        	<li><a href="<?php echo site_url('membership'); ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Membership</a></li>
	      </ul>
	    </li>		
		<li>
	      <a href="#">
	        <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
			<span>Reports</span>
	      </a>
	    </li>	
	    */ ?>
	  </ul>
	</section>
	<div class="sidebar-footer">
		<!-- item-->
		<a href="<?php echo site_url('vendor/edit_myprofile'); ?>" class="link" title="" data-original-title="Settings"><span class="icon-Settings-2"></span></a>
		<!-- item-->
		<a href="mailbox.html" class="link"  title="" data-original-title="Email"><span class="icon-Mail"></span></a>
		<!-- item-->
		<a href="<?php echo site_url('vendor_logout'); ?>" class="link" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
	</div>
</aside>