<?php
//echo "<pre>"; print_r($givenprice); exit;
?>
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

<div class="container-full">



<!-- Main content -->

<section class="content">

<div class="row">



<div class="col-12">

<!-- /.box -->



<div class="box">

<div class="box-header with-border">

<h3 class="box-title">Order Detail </h3>

</div>

<!-- /.box-header -->

<div class="box-body">



<div class="table-responsive">
<?php //echo "<pre>"; print_r($detail); ?>

<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">	<tbody>
<!-- <tr>
<th>Service type</th>
<td><?php echo ($detail['service_type']=='1')?'Normal':'Express'; ?></td>
</tr> -->
<tr>
<th>Project Name</th>
<td><?php echo $detail['project_name']; ?></td>
</tr>
<tr>
<th>Order Number</th>
<td><?php echo $detail['order_number']; ?></td>
</tr>	
<tr>
<th>Order Date</th>
<td><?php echo date('d-m-Y',strtotime($detail['order_date'])); ?></td>
</tr>	
<tr>
</tr>	
<tr>
<th>Language From</th>
<td><?php 
if(!empty($detail['from'])){ echo $detail['from']; }
?></td>
</tr>

<tr>
<th>Language to</th>
<td><?php 
if(!empty($detail['to'])){ echo $detail['to']; }
?></td>
</tr>
<tr>
<th>Design</th>
<td>
<a class="btn btn-primary btn-sm" href="<?php echo base_url('tran_doc/').$detail['customer'].'/'.$detail['trans_doc']; ?>" target="_blank">click to view</a>
<a class="btn btn-success btn-sm" href="<?php echo base_url('tran_doc/').$detail['customer'].'/'.$detail['trans_doc']; ?>" download=""><i class="fa fa-download"></i></a>
<!-- <img width="100px" height="100px" src="<?php echo base_url('order_uploads/').$detail['image_uploaded']; ?>" /> -->
</td>
</tr>
<tr>
<th>Delivery Address</th>
<td><?php if(!empty($detail['address'])){echo $detail['address']; } ?></td>
</tr>
	
<tr>
<th>Customer</th>
<td><?php echo $detail['customer_name']; ?></td>
</tr>	
<tr>
<th>Amount & note for customer</th>
<td>
	<?php 
	//echo "<pre>"; print_r($detail); exit;
	echo form_open('vendor/add_proposal_price',array('id'=>'add_proposal_price')); ?>
	<input type="hidden" name="customer" value="<?php echo $detail['customer']; ?>">
	<input type="hidden" name="customer_email" value="<?php echo $detail['customer_email']; ?>">
	<input type="hidden" name="customer_name" value="<?php echo $detail['customer_name']; ?>">
	<input type="hidden" name="order_id" value="<?php echo $detail['id']; ?>">
	<input type="text" name="price" id="price" placeholder="price" class="form-control col-md-4" value="<?php if(!empty($givenprice)){ echo $givenprice['price']; } ?>" <?php if(!empty($givenprice)){ echo 'readonly'; } ?> >
	<span id="price_err" class="error text-danger" style="display: none;"></span>
	<br>
	<textarea name="note" id="note" class="form-control col-md-8" <?php if(!empty($givenprice)){ echo 'readonly'; } ?> placeholder="add note for customer"><?php if(!empty($givenprice)){ echo $givenprice['note']; } ?></textarea>
	<span id="note_err" class="error text-danger" style="display: none;"></span>
	<?php if(empty($givenprice)){ ?>
		<input type="submit" class="btn btn-success" value="Add Price">
	<?php  } ?>
	
	<?php echo form_close(); ?>
</td>
</tr>

</tbody>
</table>


</div>              

</div>

<!-- /.box-body -->

</div>

<!-- /.box -->          

</div>

<!-- /.col -->

</div>

<!-- /.row -->

</section>

<!-- /.content -->



</div>

</div>