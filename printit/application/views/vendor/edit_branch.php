  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Edit Branch</h4>  
				</div>
				<div class="box-body">
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('vendor/edit_branch',array('id'=>'edit_branch')); ?> 
					<input type="hidden" name="printery_id" value="<?php echo $branch_data['printery_id']; ?>">
					<div class="form-group row">
						<label class="col-form-label col-md-2">Shop Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="shop_name" id="shop_name" value="<?php echo $branch_data['shop_name']; ?>">
							<span class="form-text error text-danger" id="shop_name_err" style="display:none;">Please Enter Shop name</span>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-md-2">Contact Number</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="branch_contact" id="branch_contact" value="<?php echo $branch_data['contact']; ?>">
							<span class="form-text error text-danger" id="branch_contact_err" style="display:none;">Please Enter Your Contact Number</span>
						</div>
					</div>	
										
					<div class="form-group row">
						<label class="col-form-label col-md-2">PO Box</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="po_box" id="po_box" value="<?php echo $branch_data['po_box']; ?>">
							<span class="form-text error text-danger" id="po_box_err" style="display:none;">Please Enter PO BOX number</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Block</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="block" id="block" value="<?php echo $branch_data['block']; ?>" >
							<span class="form-text error text-danger" id="block_err" style="display:none;">Please Enter Block</span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-2">Address</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="address" id="branch_address" value="<?php echo $branch_data['building']; ?>" >
							<input type="hidden" name="latitude" id="branch_lat">
							<input type="hidden" name="longitude" id="branch_lng">
							<span class="form-text error text-danger" id="address_err" style="display:none;">Please Enter Address</span>
						</div>
					</div>
					<label>Working Hours</label>		
					<div class="form-group row">
						<label class="col-form-label col-md-2">Start Time</label>
						<div class="col-md-10">
							<input class="form-control" type="time" name="start_time" id="start_time"  value="<?php echo $branch_data['start_time']; ?>">
							<span class="form-text error text-danger" id="start_time_err" style="display:none;">Please Enter Start Time</span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-2">End Time</label>
						<div class="col-md-10">
							<input class="form-control" type="time" name="end_time" id="end_time" value="<?php echo $branch_data['end_time']; ?>">
							<span class="form-text error text-danger" id="end_time_err" style="display:none;">Please Enter End Time</span>
						</div>
					</div>		
					<div class="form-group row">
						<label class="col-form-label col-md-2">Services at Main Branch</label>
						<div class="col-md-10">
							<div class="services-area">
                               <div class="service-checkbox-area">
							<?php 
							$branch_service= explode(',',$branch_data['services']);

							if(!empty($services)){ foreach($services as $value){ ?>
								<label class="container"><?php echo $value['name']; ?>
								  <input type="checkbox"  name="services[]" class="
								services" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$branch_service)){ echo 'checked'; } ?> >
								  <span class="checkmark"></span>
								</label>
							<?php } } ?>
                               </div>
								<span class="form-text error text-danger" id="service_err" style="display:none;">Please Select services</span>
							</div>
						</div>
					</div>	
					
					<div class="form-group row">
						<a href="<?php echo site_url('vendor/branches'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right rightalignbtn">Update</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>