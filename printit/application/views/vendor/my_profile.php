<?php //echo "<pre>"; print_r($myprofile); exit;
//$myprofile
 ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">My Profile</h3>
					<!-- <div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php //echo site_url('dashboard'); ?>"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><a href="<?php //echo site_url('printery_list'); ?>">My Profile</a></li>
								<li class="breadcrumb-item active" aria-current="page">My Profile</li>
							</ol>
						</nav>
					</div> -->
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">

		  <div class="row">
			<div class="col-12 col-lg-7 col-xl-8">
				
			  <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li><a class="active" href="#company_detail" data-toggle="tab">Company Detail</a></li>
				  <li><a href="#branches_detail" data-toggle="tab">Branch Detail</a></li>
				  <li><a href="#bank_detail" data-toggle="tab">Bank Detail</a></li>
				</ul>

				<div class="tab-content">

				 <div class="active tab-pane" id="company_detail">
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<!-- <p>
							<time class="float-right text-fade" datetime="2017-07-14 20:00">2 hours ago</time>
							</p> -->
							<p><?php echo $myprofile['owner_fname'].' '.$myprofile['owner_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Company Name</strong>
							<div class="media-body">
							<p><?php echo $myprofile['shop_name']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Number of Branches</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['total_branches'])?'Branches Not added yet':$myprofile['total_branches']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Registeration Number</strong>
							<div class="media-body">
							<p><?php echo $myprofile['reg_number']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Registeration Proof</strong>
							<div class="media-body">
							<p><button class="btn btn-success"><a style="text-decoration: none;" href="<?php echo base_url('vendor_uploads/').$myprofile['user_id'].'/'.$myprofile['reg_proof']; ?>" download=""><i class="fa fa-download"></i></a></button></p>
							</div>
						</div>
						<div class="media">
							<strong>Company Email</strong>
							<div class="media-body">
							<p><?php echo $myprofile['c_email']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Company Contact Number</strong>
							<div class="media-body">
							<p><?php echo $myprofile['c_contact']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Number of Workers</strong>
							<div class="media-body">
							<p><?php echo $myprofile['total_workers']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>P.O. Box</strong>
							<div class="media-body">
							<p><?php echo $myprofile['po_box']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Address</strong>
							<div class="media-body">
							<p><?php echo $myprofile['building']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Working Hours</strong>
							<div class="media-body">
							<p><?php echo date('h:i A',strtotime($myprofile['start_time']))." - ".date('h:i A',strtotime($myprofile['end_time'])); ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Services</strong>
							<div class="media-body">
							<p><?php echo $myprofile['serviceNames']; ?></p>
							</div>
						</div>
															
					  </div>
					</div>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4>Incharge Person Detail</h4>
						</div>

						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<p><?php echo $myprofile['in_fname'].' '.$myprofile['in_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Email Address</strong>
							<div class="media-body">
							<p><?php echo $myprofile['in_email']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Mobile Number</strong>
							<div class="media-body">
							<p><?php echo $myprofile['in_mobile']; ?></p>
							</div>
						</div>			
						</div>
					</div>
					<div class="box p-15"> 
						<div class="timeline timeline-single-column timeline-single-full-column">
							<?php if(!empty($myprofile['company_images'])){ ?>
							<span class="timeline-label">
								<span class="badge badge-info badge-pill">Images</span>
							</span>

							<div class="timeline-item">
								<div class="timeline-point timeline-point-success">
									<i class="fa fa-image"></i>
								</div>
								<div class="timeline-event">
									<div class="timeline-heading">
										<!-- <h4 class="timeline-title"><a href="#">Rakesh Kumar</a><small> uploaded new photos</small></h4> -->
									</div>
									<div class="timeline-body">
										<?php 
											$c_images = explode(",",$myprofile['company_images']);
											foreach($c_images as $val){ ?>
											<img src="<?php echo base_url('vendor_uploads/').$myprofile['user_id'].'/'.$val; ?>" alt="..." class="m-10">
											<?php 
											}
										?>									
									</div>
									<!-- <div class="timeline-footer">
										<p class="text-right"><i class="fa fa-clock-o"></i> 8 days ago</p>
									</div> -->
								</div>
							</div>
							<?php } ?>



						</div>
					</div>  
				  </div>  

				 <div class="tab-pane" id="branches_detail">
				 	<?php if(!empty($myprofile['branches'])){ foreach($myprofile['branches'] as $branch){ ?>
				 	
					<div class="box">

					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4><?php 
							if($branch['status']==0){ $status = 'Active'; }elseif($branch['status']==1){ $status = 'Inactive'; }elseif($branch['status']==3){ $status = 'Pending'; }
							echo $branch['shop_name'].' - '.$status; ?></h4>
						</div>
						<div class="media">
							<strong>Contact Number</strong>
							<div class="media-body">
							<p><?php echo $branch['contact']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Number of Workers</strong>
							<div class="media-body">
							<p><?php echo $branch['total_workers']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Address</strong>
							<div class="media-body">
							<p><?php echo $branch['po_box'].' '.$branch['building'] ; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Working Hours</strong>
							<div class="media-body">
							<p><?php echo date('h:i A',strtotime($branch['start_time']))." - ".date('h:i A',strtotime($branch['end_time'])); ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Services</strong>
							<div class="media-body">
							<p><?php echo $branch['serviceNames']; ?></p>
							</div>
						</div>
					  </div>
					</div>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4>Incharge Person Detail</h4>
						</div>

						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<p><?php echo $branch['in_fname'].' '.$branch['in_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Email Address</strong>
							<div class="media-body">
							<p><?php echo $branch['in_email']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Mobile Number</strong>
							<div class="media-body">
							<p><?php echo $branch['in_mobile']; ?></p>
							</div>
						</div>			
						</div>
					</div>
					<?php }}else{ ?>
					<div class="box">
						
						<div class="media-list media-list-divided bg-lighter">
						<div class="media">
							
							<div class="media-body">
							<p>Branches are not added yet</p>
							</div>
						</div>
						</div>
					</div>
					<?php } ?>
				  </div> 				    
				  <!-- /.tab-pane -->
				 <div class="tab-pane" id="bank_detail">
				 	<?php if(!empty($myprofile['bank_acc'])){ ?>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<strong>Account Holder Name</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['acc_holder_name']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Bank Name</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['bank_name']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Branch Name</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['branch_name']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Account Number</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['acc_number']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>SWIFT Code</strong>
							<div class="media-body">
							<p><?php echo empty($myprofile['bank_acc'])?'':$myprofile['bank_acc']['ifsc_code']; ?></p>
							</div>
						</div>
						
						</div>
					</div>
					<?php }else{ ?>
					<div class="box">
						
						<div class="media-list media-list-divided bg-lighter">
						<div class="media">
							
							<div class="media-body">
							<p>Bank Details not added yet</p>
							</div>
						</div>
						</div>
					</div>
					<?php }  ?>

				  </div> 
				 
				  <!-- /.tab-pane -->

				  
				  <!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			  </div>
			  <!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->		

			  <div class="col-12 col-lg-5 col-xl-4">
				 <div class="box box-widget widget-user">
					<!-- Add the bg color to the header using any of the bg-* classes -->
					<?php
					
					$profile1 = './vendor_uploads/'.$myprofile['user_id'].'/'.$myprofile['logo'];
					if(!file_exists($profile1)){
						$profileimg = base_url('assets/images/male_profile.png');
					}else{
						$profileimg = base_url('vendor_uploads/').$myprofile['user_id'].'/'.$myprofile['logo'];
					}
					// if(!empty($myprofile['gender']) && $myprofile['gender']=='female'){
					// 	$profileimg = 'female_profile.jpg';
					// }

					if(!empty($myprofile['company_images'])){
						$imgArr = explode(",",$myprofile['company_images']);
						$company_image = base_url('vendor_uploads/').$myprofile['user_id'].'/'.$imgArr[0];
					}else{
						$company_image = base_url('assets/images/gallery/full/10.jpg');
					}
					 ?>					

					<div class="widget-user-header bg-black" style="background: url('<?php echo $company_image; ?>') center center;">
					  <h3 class="widget-user-username"><?php echo $myprofile['owner_fname'].' '. $myprofile['owner_lname']; ?></h3>
					  <h6 class="widget-user-desc">Printery Shop</h6>
					</div>
					<div class="widget-user-image">
					
					  <img class="rounded-circle" src="<?php echo $profileimg; ?>" alt="User Avatar">
					</div>
					<div class="box-footer">
					  <div class="row">
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">12K</h5>
							<span class="description-text">Current Orders</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 br-1 bl-1">
						  <div class="description-block">
							<h5 class="description-header">550</h5>
							<span class="description-text">Completed Orders</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">158</h5>
							<span class="description-text">Canceled Orders</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
					  </div>
					  <!-- /.row -->
					</div>
				  </div>
				  <div class="box">
					<div class="box-body box-profile">            
					  <div class="row">
						<div class="col-12">
							<div>
								<p>Email :<span class="text-gray pl-10"><?php echo  $myprofile['c_email']; ?></span> </p>
								<p>Phone :<span class="text-gray pl-10"><?php echo  $myprofile['c_contact']; ?></span></p>
								<p>Address :<span class="text-gray pl-10"><?php echo $myprofile['po_box'].', '.$myprofile['building']; ?></span></p>
							</div>
						</div>
						<!-- <div class="col-12">
							<div class="pb-15">						
								<p class="mb-10">Social Profile</p>
								<div class="user-social-acount">
									<button class="btn btn-circle btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></button>
									<button class="btn btn-circle btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></button>
									<button class="btn btn-circle btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></button>
								</div>
							</div>
						</div> -->
						<div class="col-12">
							<div>
								<div class="map-box">
									<?php
									echo '<iframe src = "https://maps.google.com/maps?q='.$myprofile['latitude'].','.$myprofile['longitude'].'&hl=es;z=14&amp;output=embed"></iframe>';
									?>
								</div>
							</div>
						</div>
					  </div>
					</div>
					<!-- /.box-body -->
				  </div>
			  </div>
		  </div>
		  <!-- /.row -->

		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->


<?php /*
///google map
<div class="mapContainer">
    <a class="direction-link" target="_blank" href="//maps.google.com/maps?f=d&amp;daddr=22.731211,75.854202&amp;hl=en"> Get Directions</a>
    <div id="map"></div>
</div>
<style type="text/css">
	#map{
    width: 100%;
    height: 400px;
}

.mapContainer{
    width:50%;
    position: relative;
}
.mapContainer a.direction-link {
    position: absolute;
    top: 15px;
    right: 15px;
    z-index: 100010;
    color: #FFF;
    text-decoration: none;
    font-size: 15px;
    font-weight: bold;
    line-height: 25px;
    padding: 8px 20px 8px 50px;
    background: #0094de;
    background-image: url('direction-icon.png');
    background-position: left center;
    background-repeat: no-repeat;
}
.mapContainer a.direction-link:hover {
    text-decoration: none;
    background: #0072ab;
    color: #FFF;
    background-image: url('direction-icon.png');
    background-position: left center;
    background-repeat: no-repeat;
}
</style>
*/ ?>