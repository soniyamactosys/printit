  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Price Management</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Branch Name</th>
								<th>Order Type</th>
								<th>Print Type</th>
								<th>Paper Type</th>
								<th>Paper Size</th>
								<th>Printing Sides</th>
								<th>Width</th>
								<th>Color</th>
								<th>Binding</th>
								<th>Average Price</th>
								<!--<th>Status</th>-->
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableData">
						    <?php $i=0; 
//echo "<pre>"; print_r($management); exit;
						    
						    if(!empty($management)){ foreach($management as $value){ $i++; ?>
						    
							<tr>
								<td><?php echo $i;?></td>
								<td class="printry" id="<?php echo $value['printery_id']; ?>"><?php echo $value['shop_name'];?>
								</td>
								<td class="order_type" id="<?php echo $value['service_id']; ?>"><?php echo empty($value['service_name'])?'-':$value['service_name']; ?>
								</td>
								<td class="print_type" id="<?php echo $value['print_type_id']; ?>"><?php echo empty($value['print_type'])?'-':$value['print_type']; ?></td>
								<td class="paper_type" id="<?php echo $value['paper_type_id']; ?>"><?php echo empty($value['paper_type'])?'-':$value['paper_type'];?></td>
								<td class="size" id="<?php echo $value['paper_size_id']; ?>"><?php if(!empty($value['size'])){ echo $value['size']; }else{ echo "-"; }?></td>
								<td class="printing_sides" id="<?php echo $value['printing_sides']; ?>"><?php if(!empty($value['printing_sides'])){ echo $value['printing_sides']; }else{ echo "-"; } ?></td>

								<td class="width" id="<?php echo $value['width']; ?>"><?php if(!empty($value['width'])){ echo $value['width']; }else{ echo "-"; } ?></td>
								<td class="color" id="<?php echo $value['color']; ?>"><?php if(!empty($value['color'])){ echo $value['color']; }else{ echo "-"; }?></td>
								<td class="binding" id="<?php echo $value['binding']; ?>"><?php if(!empty($value['binding'])){ echo $value['binding']; }else{ echo "-"; }?></td>
								<td class="avg_price"><?php echo empty($value['average_price'])?'0':$value['average_price']; ?></td>
								<td>
									<a href="<?php echo site_url('vendor/add_price_range/').encoding($value['printery_id']).'/'.encoding($value['pm_id']); ?>" data-id="<?php echo encoding($value['pm_id']); ?>" class="btn btn-success btn-sm mr-5"><i class="fa fa-plus" aria-hidden="true"></i></a>

<?php /*
									<a href="<?php echo site_url('vendor/view_price_management/').encoding($value['printery_id']); ?>" data-id="<?php echo encoding($value['printery_id']); ?>" class="btn btn-info btn-sm mr-5"><i class="fa fa-eye" aria-hidden="true"></i></a>


									<a data-id="<?php echo encoding($value['printery_id']); ?>" class="btn btn-primary btn-sm mr-5 edit_pm_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_printmanagement/').encoding($value['pm_id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
								*/ ?>
							</tr>
						    <?php } }   ?>

						</tbody>				  
						<tfoot>
							<tr>
							    <th>#</th>
								<th>Printry</th>						    
							    <th>Order Type</th>
								<th>Print Type</th>
								<th>Paper Type</th>
								<th>Paper Size</th>
								<th>Printing Sides</th>
								<th>Width</th>
								<th>Color</th>
								<th>Binding</th>
								<th>Average Price</th>
								<!--<th>Status</th>-->
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  
  
<!-- Modal -->
<div class="modal center-modal fade" id="add_new" tabindex="-1">
	<div class="modal-dialog adminmodalarea">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Add New Price</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		<form action="<?php echo site_url('vendor/add_price_management'); ?>" id="add_print_management">

		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-4">Branch *</label>
					<div class="col-md-8">
					    <select name="branch_id" id="branch_id" class="form-control">
						    <option value="">Please select</option>
						    <?php if(!empty($branches)){ foreach($branches as $branch){ ?>
						    	<option value="<?php echo $branch['id']; ?>"><?php echo $branch['shop_name']; ?></option>
						    <?php  } } ?>
						</select>
						<span class="text-danger error" id="branch_err" style="display:none;">Please Select Your Branch</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Service*</label>
					<div class="col-md-8">
					    <select name="service" id="services" class="form-control">
						    <option value="">Please select</option>
						</select>
						<span class="text-danger error" id="service_err" style="display:none;">Please Select Service</span>
					</div>
				</div>

<div id="optionsDiv" style="display: none;">				
				<div class="form-group row" id="printTypeDiv" style="display: none;">
					<label class="col-form-label col-md-4">Print Type *</label>
					<div class="col-md-8">
					    <select name="print_type" id="print_type" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($print_type)){ foreach($print_type as $print){ ?>
						    <option value="<?php echo $print['id']; ?>"><?php echo $print['name']; ?></option>
						    <?php } } ?>
						</select>
						<span class="text-danger error" id="printtype_err" style="display:none;">Please Select Print Type</span>
					</div>
				</div>
				<div class="form-group row"  id="paperTypeDiv">
					<label class="col-form-label col-md-4">Paper Type *</label>
					<div class="col-md-8">
		                <select name="paper_type" id="paper_type" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_type)){ foreach($paper_type as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="papertype_err" style="display:none;">Please select Paper Type</span>
					</div>
				</div>
				<div class="form-group row" id="papersizeDiv">
					<label class="col-form-label col-md-4">Paper Size *</label>
					<div class="col-md-8">
						<select name="paper_size" id="paper_size" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_size)){ foreach($paper_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="papersize_err" style="display:none;">Please select Paper Size</span>
					</div>
				</div>
				<div class="form-group row" id="colorDiv" style="display: none;">
					<label class="col-form-label col-md-4">Color *</label>
					<div class="col-md-8">
						<select name="color" id="color" class="form-control">
						    <option value="">Please select</option>
						    <option value="colored">colored</option>
						    <option value="black_white">black & white</option>
						</select>

						<span class="text-danger error" id="color_err" style="display:none;">Please select Color</span>
					</div>
				</div>
				<div class="form-group row" id="bindingDiv" style="display: none;">
					<label class="col-form-label col-md-4">Binding *</label>
					<div class="col-md-8">
						<select name="binding" id="binding" class="form-control">
						    <option value="">Please select</option>
						    <option value="yes">Yes</option>
						    <option value="no">No</option>
						</select>

						<span class="text-danger error" id="binding_err" style="display:none;">Please select Binding</span>
					</div>
				</div>
				<div class="form-group row" id="sidesDiv" style="display: none;">
					<label class="col-form-label col-md-4">Printing Sides *</label>
					<div class="col-md-8">
						<select name="sides" id="sides" class="form-control">
						    <option value="">Please select</option>
						    <option value="single">Single Side</option>
						    <option value="double">Double Side</option>
						</select>

						<span class="text-danger error" id="sides_err" style="display:none;">Please select Printing Sides</span>
					</div>
				</div>
				<div class="form-group row" id="widthDiv" style="display: none;">
					<label class="col-form-label col-md-4">Width *</label>
					<div class="col-md-8">
						<select name="width" id="width" class="form-control">
						    <option value="">Please select</option>
 							<?php if(!empty($banner_width)){ foreach($banner_width as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>
						<!-- <input class="form-control" type="text" name="width" id="width"> -->
						<span class="text-danger error" id="width_err" style="display:none;">Please Enter Banner Width</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Average Prize *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="avg_price" id="avg_price">
						<span class="text-danger error" id="avgprice_err" style="display:none;">Please Enter Valid Average Price</span>
					</div>
				</div>
</div>				
				<div class="form-group row">
					<label class="col-form-label col-md-4"></label>
					<div class="col-md-8">
						<span class="text-danger error" id="common_err" style="display:none;"></span>
					</div>
				</div>
		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
		</form>
		</div>
	</div>
</div>
<!-- /.modal -->
  
  
   
  <!-- Modal -->
  <div class="modal center-modal fade" id="edit_pm_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Edit Print Management</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/edit_print_management'); ?>" id="edit_pm" method="post">
		<input type="hidden" id="id" name="id" />
		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-4">Order Type *</label>
					<div class="col-md-8">
					    <select name="order_type" id="e_order_type" class="form-control">
						    <option value="">Please select</option>
						    <option value="1">Custom Print</option>
						    <option value="2">Quick Print</option>
						    <option value="3">Translation</option>
						    <option value="4">Notes</option>
						</select>
						<span class="text-danger error" id="e_ordertype_err" style="display:none;">Please Select Order Type</span>
					</div>
				</div>    
				<div class="form-group row" id="printTypeDiv" style="display: none;">
					<label class="col-form-label col-md-2">Print Type *</label>
					<div class="col-md-10">
					    <select name="print_type" id="e_print_type" class="form-control">
						    <option value="">Please select</option>
						    <?php if(!empty($print_type)){ foreach($print_type as $print){ ?>
						    <option value="<?php echo $print['id']; ?>"><?php echo $print['name']; ?></option>
						    <?php } } ?>
						</select>
						<span class="text-danger error" id="e_printtype_err" style="display:none;">Please Select Print Type</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-2">Paper Type *</label>
					<div class="col-md-10">
                        <select name="paper_type" id="e_paper_type" class="form-control">
						    <option value="">Please select</option>
						    <?php if(!empty($paper_type)){ foreach($paper_type as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="e_papertype_err" style="display:none;">Please select Paper Type</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-2">Paper Size *</label>
					<div class="col-md-10">
						<select name="paper_size" id="e_paper_size" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_size)){ foreach($paper_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="e_papersize_err" style="display:none;">Please select Paper Size</span>
					</div>
				</div>
				<div class="form-group row" id="ecolorDiv" style="display: none;">
					<label class="col-form-label col-md-4">Color *</label>
					<div class="col-md-8">
						<select name="color" id="e_color" class="form-control">
						    <option value="">Please select</option>
						    <option value="colored">colored</option>
						    <option value="black_white">black & white</option>
						</select>

						<span class="text-danger error" id="e_color_err" style="display:none;">Please select Color</span>
					</div>
				</div>
				<div class="form-group row" id="ebindingDiv" style="display: none;">
					<label class="col-form-label col-md-4">Binding *</label>
					<div class="col-md-8">
						<select name="binding" id="e_binding" class="form-control">
						    <option value="">Please select</option>
						    <option value="yes">Yes</option>
						    <option value="no">No</option>
						</select>

						<span class="text-danger error" id="e_binding_err" style="display:none;">Please select Binding</span>
					</div>
				</div>
				<div class="form-group row" id="esidesDiv" style="display: none;">
					<label class="col-form-label col-md-4">Printing Sides *</label>
					<div class="col-md-8">
						<select name="sides" id="e_sides" class="form-control">
						    <option value="">Please select</option>
						    <option value="single">Single Side</option>
						    <option value="double">Double Side</option>
						</select>

						<span class="text-danger error" id="e_sides_err" style="display:none;">Please select Printing Sides</span>
					</div>
				</div>
				<div class="form-group row" id="ewidthDiv" style="display: none;">
					<label class="col-form-label col-md-4">Width *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="width" id="e_width">
						<span class="text-danger error" id="e_width_err" style="display:none;">Please Enter Banner Width</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-2">Average Prize *</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="avg_price" id="e_avg_price">
						<span class="text-danger error" id="e_avgprice_err" style="display:none;">Please Enter Valid Average Price</span>
					</div>
				</div>
			    <div class="form-group row"><span class="text-danger error" id="e_common_err" style="display:none;text-align:center !important"></span></div>

		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
 