<?php 

//echo "<pre>"; print_r($myprofile); exit; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Edit My Profile</h4>  
				</div>
				<div class="box-body">
					<h3>Personal Detail</h3>
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('vendor/edit_myprofile',array('id'=>'update_myprofile')); ?> 
					<div class="form-group row">
						<label class="col-form-label col-md-2">First Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="fname" id="fname" value="<?php echo $myprofile['firstname']; ?>">
							<span class="form-text error text-danger" id="fname_err" style="display:none;">Please Enter First name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Last Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="lname" id="lname" value="<?php echo $myprofile['lastname']; ?>">
							<span class="form-text error text-danger" id="lname_err" style="display:none;">Please Enter Last name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Email Address</label>
						<div class="col-md-10">
							<input class="form-control" type="text" value="<?php echo $myprofile['email']; ?>" onkeydown="return false;">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Contact Number</label>
						<div class="col-md-10">
							<input class="form-control" type="text" value="<?php echo $myprofile['mobile']; ?>" name="mobile" id="vendor_contact">
							<span class="form-text error text-danger" id="vendor_contact_err" style="display:none;">Please Enter Your Contact Number</span>
						</div>
					</div>	
					<div class="form-group row ichack-input">
						<label class="col-form-label col-md-2">Gender</label>
					

						<div class="col-md-10">
							     <div class="gander-area">
		                              <p>
									    <input type="radio" class="gender" name="gender" id="test1" <?php if($myprofile['gender']=='male'){ echo  'checked'; } ?> value="male" >
									    <label for="test1">Male</label>
									  </p>
									  <p>
									    <input type="radio" class="gender" name="gender" id="test2" <?php if($myprofile['gender']=='female'){ echo  'checked'; } ?> value="female"  >
									    <label for="test2">Female</label>
									  </p>
									</div>
							<span class="form-text error text-danger" id="gender_err" style="display:none;">Please Enter Gender</span>
						</div>
					</div>				
					
					<h3>Shop Detail</h3>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Shop Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="shop_name" id="shop_name"  value="<?php echo $myprofile['shop_name']; ?>">
							<span class="form-text error text-danger" id="shop_name_err" style="display:none;">Please Enter Shop name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Contact Number</label>
						<div class="col-md-10">
							<input class="form-control" type="text" value="<?php echo $myprofile['contact']; ?>" name="branch_contact" id="branch_contact">
							<span class="form-text error text-danger" id="branch_contact_err" style="display:none;">Please Enter Your Contact Number</span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-2">PO Box</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="po_box" id="po_box" value="<?php echo $myprofile['po_box']; ?>">
							<span class="form-text error text-danger" id="po_box_err" style="display:none;">Please Enter PO BOX number</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Block</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="block" id="block" value="<?php echo $myprofile['block']; ?>">
							<span class="form-text error text-danger" id="block_err" style="display:none;">Please Enter Block</span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-2">Address</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="address" id="branch_address" value="<?php echo $myprofile['building']; ?>" >
							<input type="hidden" name="latitude" id="branch_lat">
							<input type="hidden" name="longitude" id="branch_lng">
							<span class="form-text error text-danger" id="address_err" style="display:none;">Please Enter Address</span>
						</div>
					</div>
					<label>Working Hours</label>		
					<div class="form-group row">
						<label class="col-form-label col-md-2">Start Time</label>
						<div class="col-md-10">
							<input class="form-control" type="time" name="start_time" id="start_time" value="<?php echo $myprofile['start_time']; ?>">
							<span class="form-text error text-danger" id="start_time_err" style="display:none;">Please Enter Start Time</span>
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-2">End Time</label>
						<div class="col-md-10">
							<input class="form-control" type="time" name="end_time" id="end_time" value="<?php echo $myprofile['end_time']; ?>">
							<span class="form-text error text-danger" id="end_time_err" style="display:none;">Please Enter End Time</span>
						</div>
					</div>		
					<div class="form-group row">
						<label class="col-form-label col-md-2">Services at Main Branch</label>
						<div class="col-md-10">
							<div class="services-area">
                               <div class="service-checkbox-area">
							<?php 
							$myservices = explode(',',$myprofile['services']);
							if(!empty($services)){ foreach($services as $value){ ?>
								<label class="container"><?php echo $value['name']; ?>
								  <input type="checkbox"  name="services[]" class="
								services" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$myservices)){ echo 'checked'; } ?> >
								  <span class="checkmark"></span>
								</label>
							<?php } } ?>
                               </div>


						<!-- 	<?php 
							$myservices = explode(',',$myprofile['services']);
							if(!empty($services)){ foreach($services as $value){ ?>
								<input type="checkbox" name="services[]" class="
								services" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$myservices)){ echo 'checked'; } ?> style="opacity: 1 !important;position: relative;left: 0px"><?php echo $value['name']; ?>
							<?php } } ?>
								 -->
								<span class="form-text error text-danger" id="service_err" style="display:none;">Please Select services</span>
							</div>
						</div>
					</div>	

					<div class="form-group row">
						<button type="submit" class="btn btn-success float-right">Update</button>
					</div>
			    	<?php echo form_close(); ?>
					<!-- /.box-header -->
					<!-- /.box-body -->
				  </div>

			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>

		<style type="text/css">


		</style>