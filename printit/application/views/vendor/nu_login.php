
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/'); ?>/images/favicon.ico">

    <title>PrintIt - Vendor Log in </title>
  
	<!-- Vendors Style-->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/vendors_css.css'); ?>">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/skin_color.css'); ?>">	

</head>
	
<body class="hold-transition theme-primary bg-img" style="background-image: url(<?php echo base_url('assets/images/auth-bg/bg-1.jpg'); ?>)">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">	
			
			<div class="col-12">
				<div class="row justify-content-center no-gutters">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded30 shadow-lg">
							<div class="content-top-agile p-20 pb-0">
								<h2 class="text-primary">Let's Get Started</h2>
								<p class="mb-0">Sign in to continue to PrintIt.</p>							
							</div>
							<div class="p-40">
								<form action="<?php echo site_url('authorize/vendor_login') ?>" method="post" id="adminlogin">
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
											</div>
											<input type="text" id="email" name="email" class="form-control pl-15 bg-transparent" placeholder="Email">
										</div>
										<span id="email_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your Email Address</span>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" id="pass" name="pass" class="form-control pl-15 bg-transparent" placeholder="Password">
										</div>
										<span id="pass_err" class="text-danger invalidText" style="display:none;font-weight:bold;">Please Enter Your Password</span>
									<span id="validation_err" class="text-danger invalidText" style="display:none;font-weight:bold;"></span>
									</div>
									
									
									  <div class="row">
										<div class="col-6">
										  <div class="checkbox">
											<input type="checkbox" id="basic_checkbox_1" name="remember_me" value="1" >
											<label for="basic_checkbox_1">Remember Me</label>
										  </div>
										</div>
										<!-- /.col -->
										<div class="col-6">
										 <div class="fog-pwd text-right">
											<a href="javascript:void(0)" class="hover-warning"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
										  </div>
										</div>
										<!-- /.col -->
										<div class="col-12 text-center">
										  <button type="submit" class="btn btn-danger mt-10">SIGN IN</button>
										</div>
										<!-- /.col -->
									  </div>
								</form>	
								<div class="text-center">

								</div>	
							</div>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Vendor JS -->
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>	
	<script src="<?php echo base_url('assets/js/loaderjs.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/login.js'); ?>"></script>	
	<script src="<?php echo base_url('assets/js/vendors.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/pages/chat-popup.js'); ?>"></script>
    <script src="<?php echo base_url('assets/assets/icons/feather-icons/feather.min.js'); ?>"></script>	

</body>
</html>
