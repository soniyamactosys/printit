<div class="content-wrapper">

	  <div class="container-full">

		<!-- Main content -->

		<section class="content">
			

   
  <!--content inner-->
  <div class="content__inner">
        <div class="container ">
             <div class="price-table-area">
                <?php echo form_open('vendor/add_price',array('id'=>'price_form')) ?>

                 <h4>ADD PRICES</h4>
                 <div class="price-filter-area">
   
                     <div class="row">
                         <div class="col-lg-4 col-md-4">
                             <div class="price-filter-box">
                                 <div class="form-group">
                              <label for="branch">Select Branch</label>
                              <select class="form-control" id="branch_id" name="branch_id">
                                  <option value="">Please select</option>
                                <?php if(!empty($branches)){ foreach($branches as $value){ ?>
                                  <option value="<?php echo $value['id']; ?>"><?php echo $value['shop_name']; ?></option>
                                <?php }} ?>
                              </select>
                              </div>
                             </div>
                         </div>
                          <div class="col-lg-4 col-md-4" id="service_div" style="display: none;">
                              <div class="price-filter-box">
                                    <div class="form-group">
                              <label for="services">Select Service</label>
                              <select class="form-control" id="services" name="services">
                                  
                              </select>
                              </div>
                             </div>
                         </div>
                         
                          <div class="col-lg-4 col-md-4" id="printTypeDiv" style="display: none;">
                              <div class="price-filter-box">
                                    <div class="form-group">
                              <label for="print_type">Print Type </label>
                              <select class="form-control" id="print_type" name="print_type">
                                <option value="">Please select</option>
                                <?php if(!empty($print_type)){ foreach($print_type as $print){ ?>
                                <option value="<?php echo $print['id']; ?>"><?php echo $print['name']; ?></option>
                                <?php } } ?>

                              </select>
                              </div>
                             </div>
                         </div>
                          <div class="col-lg-4 col-md-4" id="colorDiv" style="display: none;">
                              <div class="price-filter-box">
                                    <div class="form-group">
                              <label for="print_type">Color</label>
                              <select class="form-control" id="color_type" name="color_type">
                                <option value="">Please select</option>
                                <option value="colored">Colored</option>
                                <option value="Black_and_White">Black & White</option>
                              </select>
                              </div>
                             </div>
                         </div>
                     </div>

                 </div>
                  <div id="tableDiv">
                  
                 </div>
                <?php echo form_close(); ?>
             </div>
       </div>
 </div>
    
      </section>
      
      </div>
      </div>
      
<div class="modal center-modal fade" id="price_update_confirm" tabindex="-1">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title">Are Your Sure??</h5>
		<button type="button" class="close" data-dismiss="modal">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
		<p>Do you really want to update prices  of this printery?</p>
	  </div>
	  <div class="modal-footer modal-footer-uniform">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success float-right" id="update_printery_charges" >Yes</button>
	  </div>
	</div>
  </div>
</div>      