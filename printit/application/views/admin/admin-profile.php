  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Admin Profile</h4>  
				</div>
				<div class="box-body">
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('admin/update_profile',array('id'=>'update_profile')); ?> 
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">First Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="fname" id="fname" value="<?= $admin_pro['firstname']; ?>">
							<span class="form-text error text-danger" id="fname_err" style="display:none;">Please Enter First Name</span>
						</div>
					</div>
						<div class="form-group row">
						<label class="col-form-label col-md-2">Last Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="lname" id="lname" value="<?php if($admin_pro['lastname']){echo $admin_pro['lastname']; } ?>">
							<span class="form-text error text-danger" id="lname_err" style="display:none;">Please Enter Last Name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Email</label>
						<div class="col-md-10">
							<input class="form-control" type="email" readonly value="<?= $admin_pro['email']; ?>" name="email" id="email">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Mobile</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="mobile" id="mobile" value="<?php if($admin_pro['mobile']){echo $admin_pro['mobile']; } ?>">
						</div>
					</div>
					<div class="form-group row">
						<a href="<?php echo site_url('dashboard'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right rightalignbtn">Update</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>