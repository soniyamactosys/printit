
		  <div class="modal-header">

			<h5 class="modal-title">Edit Services</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/edit_service'); ?>" id="edit_service" method="post">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-3">Type *</label>

					<div class="col-md-9">

						<input class="form-control" type="text" name="name" id="edit_name" value="<?php echo $detail['name']; ?>">

						<input type="hidden" name="id" id="id" value="<?php echo encoding($detail['id']); ?>">

						<span class="text-danger error" id="edit_name_err" style="display:none;">Please Enter Valid Type</span>

					</div>

				</div>

				<div class="form-group row">

					<label class="col-form-label col-md-3">Description *</label>

					<div class="col-md-9">

						<textarea id="edit_description" name="description" class="form-control"><?php echo $detail['description']; ?></textarea>
						<span class="text-danger error" id="edit_desc_err" style="display:none;">Please Enter Description</span>

					</div>

				</div>


		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Update</button>

		  </div>

        </form>