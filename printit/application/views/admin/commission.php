  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Commission</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					    
                       
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Commission Percent</th>
								<th>Description</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						    <?php $i=0; if(!empty($commission)){ foreach($commission as $value){ $i++; ?>
						    
							<tr>
								<td><?php echo $i;?></td>
								<td class="abc" vale="<?php echo $value['percent']; ?>"><?php echo $value['percent'].'%'; ?></td>
								<td class="des"><?php echo $value['description'];?></td>
								<td class="status"><?php echo ($value['status']=='0')?'Active':'In-active';?></td>
								<td><a data-id="<?php echo encoding($value['id']); ?>" class="btn btn-primary btn-sm mr-5 edit_commission_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_commission/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
							    </td>
							</tr>
						    <?php } }  ?>

						</tbody>				  
						<tfoot>
							<tr>
							    <th>#</th>
								<th>Commission Percent</th>
								<th>Description</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <!-- Modal -->
  <div class="modal center-modal fade" id="add_new" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Add New Commission Percent</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/add_commission'); ?>" id="add_commission">

		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-3">Commission Percent *</label>
					<div class="col-md-9">
						<input class="form-control" type="text" name="commission_percent" id="commission_percent">
						<span class="text-danger error" id="commission_err" style="display:none;">Please Enter Valid Commission Amount</span>
					</div>
				</div>
                <div class="form-group row">
					<label class="col-form-label col-md-3">Description *</label>
					<div class="col-md-9">
						<textarea name="description" id="description" class="form-control"></textarea>
						<span class="text-danger error" id="des_err" style="display:none;">Please Enter Description</span>
					</div>
				</div>
		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
  
   
  <!-- Modal -->
  <div class="modal center-modal fade" id="edit_commission_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Edit Commission</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/edit_commission'); ?>" id="edit_commission_form" method="post">

		  <div class="modal-body">
			<div class="box-body">
			    <input type="hidden" name="id" id="id" />
				<div class="form-group row">
					<label class="col-form-label col-md-3">Commission Percent*</label>
					<div class="col-md-9">
						<input class="form-control" type="text" name="commission_percent" id="edit_commission">
						<span class="text-danger error" id="edit_commission_err" style="display:none;">Please Enter Valid Commission Percent</span>
					</div>
				</div>
				
				
                <div class="form-group row">
					<label class="col-form-label col-md-3">Description *</label>
					<div class="col-md-9">
						<textarea name="description" id="edit_description" class="form-control"></textarea>
						<span class="text-danger error" id="edit_des_err" style="display:none;">Please Enter Description</span>
					</div>
				</div>


            </div>
            

		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
 