
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Master Setting - Services</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

				   <!-- <div class="col-md-12"> <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new_service" class="btn btn-success mt-10 float-right">+ Add New</a></div> -->

					<div class="table-responsive" style="margin-top: 15px;">
                      

					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Service Name</th>
								<th>Description</th>
								<th>Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($services)){ foreach($services as $value){ $i++; ?>

						    

							<tr>

								<td><?php echo $i;?></td>

								<td><?php echo $value['name'];?></td>
								<td><?php echo $value['description'];?></td>

								<td><a data-title="<?php echo $value['name']; ?>" data-id="<?php echo encoding($value['id']); ?>" name="update" class="btn btn-primary btn-sm  edit_service_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>

								<!-- <a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_service/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a> -->

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Service Name</th>
								<th>Description</th>

								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  </div>

  </div>

  <!-- /.content-wrapper -->

  

  

  <!-- Modal -->

  <div class="modal center-modal fade" id="add_new_service" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Add New Paper Type</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/add_paper_type'); ?>" id="add_paper_type">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-3">Type *</label>

					<div class="col-md-9">

						<input class="form-control" type="text" name="name" id="name">

						<span class="text-danger error" id="name_err" style="display:none;">Please Enter Valid Type</span>

					</div>

				</div>
				<div class="form-group row">

					<label class="col-form-label col-md-3">Description *</label>

					<div class="col-md-9">

						<textarea name="description" id="pt_description" class="form-control"></textarea>

						<span class="text-danger error" id="desc_err" style="display:none;">Please Enter Description</span>

					</div>

				</div>

		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Add</button>

		  </div>

        </form>

		</div>

	  </div>

	</div>

  <!-- /.modal -->

  

  

   

  <!-- Modal -->

  <div class="modal center-modal fade" id="edit_service_modal" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">


		</div>

	  </div>

	</div>

  <!-- /.modal -->

 