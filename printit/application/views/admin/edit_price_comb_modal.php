		  <div class="modal-header">
			<h5 class="modal-title">Edit Print Management</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/edit_print_management'); ?>" id="edit_pm" method="post">
<input type="hidden" id="id" name="id" />
		  <div class="modal-body">
		       
	<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-4">Service Type *</label>
					<div class="col-md-8">
					    <select name="order_type" id="e_order_type" class="form-control">
						    <option value="">Please select</option>
						    <option value="1" <?php if($detail['service_id']==1){ echo 'selected';} ?> >Custom Print</option>
						    <option value="2" <?php if($detail['service_id']==2){ echo 'selected';} ?> >Quick Print</option>
						    <option value="3" <?php if($detail['service_id']==3){ echo 'selected';} ?> >Translation</option>
						    <option value="4" <?php if($detail['service_id']==4){ echo 'selected';} ?> >Notes</option>
						</select>
						<span class="text-danger error" id="e_ordertype_err" style="display:none;">Please Select Order Type</span>
					</div>
				</div>    
				<div class="form-group row" id="printTypeDiv" style="<?php if($detail['service_id']!=1){echo 'display:none';} ?>">
					<label class="col-form-label col-md-4">Print Type *</label>
					<div class="col-md-8">
					    <select name="print_type" id="e_print_type" class="form-control">
						    <option value="">Please select</option>
						    <?php if(!empty($print_type)){ foreach($print_type as $print){ ?>
						    <option value="<?php echo $print['id']; ?>"  <?php if($detail['print_type']==$print['id']){ echo 'selected';} ?> ><?php echo $print['name']; ?></option>
						    <?php } } ?>
						</select>
						<span class="text-danger error" id="e_printtype_err" style="display:none;">Please Select Print Type</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Paper Type *</label>
					<div class="col-md-8">
                        <select name="paper_type" id="e_paper_type" class="form-control">
						    <option value="">Please select</option>
						    <?php if(!empty($paper_type)){ foreach($paper_type as $value){ ?>
						    <option value="<?php echo $value['id']; ?>" <?php if($detail['paper_type_id']==$value['id']){ echo 'selected';} ?>><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="e_papertype_err" style="display:none;">Please select Paper Type</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Paper Size *</label>
					<div class="col-md-8">
						<select name="paper_size" id="e_paper_size" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_size)){ foreach($paper_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>" <?php if($detail['paper_size_id']==$value['id']){ echo 'selected';} ?> ><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="e_papersize_err" style="display:none;">Please select Paper Size</span>
					</div>
				</div>
				<div class="form-group row" id="ecolorDiv" style="<?php if(empty($detail['color'])){ echo 'display:none'; } ?>">
					<label class="col-form-label col-md-4">Color *</label>
					<div class="col-md-8">
						<select name="color" id="e_color" class="form-control">
						    <option value="">Please select</option>
						    <option value="colored" <?php if($detail['color']=='colored'){ echo 'selected';} ?>>colored</option>
						    <option value="black_white" <?php if($detail['color']=='black_white'){ echo 'selected';} ?>>black & white</option>
						</select>

						<span class="text-danger error" id="e_color_err" style="display:none;">Please select Color</span>
					</div>
				</div>
				<div class="form-group row" id="ebindingDiv" style="<?php if(empty($detail['binding'])){echo 'display:none'; } ?>">
					<label class="col-form-label col-md-4">Binding *</label>
					<div class="col-md-8">
						<select name="binding" id="e_binding" class="form-control">
						    <option value="">Please select</option>
						    <option value="yes" <?php if($detail['binding']=='yes'){ echo 'selected';} ?>>Yes</option>
						    <option value="no" <?php if($detail['binding']=='no'){ echo 'selected';} ?>>No</option>
						</select>

						<span class="text-danger error" id="e_binding_err" style="display:none;">Please select Binding</span>
					</div>
				</div>
				<div class="form-group row" id="esidesDiv" style="<?php if(empty($detail['printing_sides'])){ echo 'display:none'; } ?>">
					<label class="col-form-label col-md-4">Printing Sides *</label>
					<div class="col-md-8">
						<select name="sides" id="e_sides" class="form-control">
						    <option value="">Please select</option>
						    <option value="single"<?php if($detail['printing_sides']=='single'){ echo 'selected';} ?>>Single Side</option>
						    <option value="double"<?php if($detail['printing_sides']=='double'){ echo 'selected';} ?>>Double Side</option>
						</select>

						<span class="text-danger error" id="e_sides_err" style="display:none;">Please select Printing Sides</span>
					</div>
				</div>
				<div class="form-group row" id="ewidthDiv" style="<?php if(empty($detail['banner_size'])){ echo 'display:none'; } ?>">
					<label class="col-form-label col-md-4">Width *</label>
					<div class="col-md-8">
						<select name="width" id="width" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($banner_size)){ foreach($banner_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>" <?php if($detail['banner_size']==$value['id']){ echo 'selected';} ?>><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>
						<span class="text-danger error" id="e_width_err" style="display:none;">Please Enter Banner Width</span>
					</div>
				</div>
				<div class="form-group row" id="extra_copyDiv" style="<?php if(empty($detail['extra_copies'])){ echo 'display:none';} ?>" >
					<label class="col-form-label col-md-4">Extra Copies *</label>
					<div class="col-md-8">
						<input class="form-control only_whole_number" type="text" name="extra_copies" id="e_extra_copies" value="<?php echo $detail['extra_copies']; ?>" >
						<span class="text-danger error" id="e_extra_copy_err" style="display:none;">Please Select extra Copies</span>
					</div>
				</div>
				
			    <div class="form-group row"><span class="text-danger error" id="e_common_err" style="display:none;text-align:center !important"></span></div>

		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Update</button>
		  </div>
        </form>