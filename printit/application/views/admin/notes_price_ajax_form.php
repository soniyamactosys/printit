<?php //echo "<pre>"; print_r($notes_price); exit; ?>

<table id="example4" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
  <thead>
       <div class="inner-drop" style="display:flex;  align-items: baseline;width: 290px;justify-content: space-around;margin-bottom: 10px;">   
        <div class="dropdown">
        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Update normal prices</a>
        	<div class="dropdown-menu dropdown-menu-right" style="">
        	  <a class="dropdown-item psall_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
        	  <a class="dropdown-item psall_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
        	  <a class="dropdown-item psall_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
        	 </div>
        </div>
        <div class="dropdown margin-bottom">
        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Update express prices</a>
        	<div class="dropdown-menu dropdown-menu-right" style="">
        	  <a class="dropdown-item pssall_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
        	  <a class="dropdown-item pssall_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
        	  <a class="dropdown-item pssall_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
        	 </div>
        </div>
        </div>
        <span class="form-text error text-danger" id="allpscid_err" style="display:none;">Please select note list</span>
		
    <tr>
        <th><input type="checkbox" id="allnotecheck" name="allnotecheck"></th>
	   <th>#</th>
      <th>Page From</th>
      <th>Page To</th>
      <th>Color</th>
      <th>Price</th>
      <th>Express Price</th>
      <th>Price Status</th>
      <th>Express Price Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody id="notes_price_table">
    <?php 
     if(!empty($notes_price)){ 
      $i=0;
      foreach($notes_price as $combi){ 
      $i++;
      $key=$combi['npr_id'];
      ?>
      	<tr note_id="<?php  echo encoding($combi['npr_id']); ?>" >
      	    <td><input type="checkbox" class="allnote" name="allnote" value="<?php echo $combi['npr_id'];?>"></td>

      	<td><?php echo $i;?></td>
        <td> <?php echo $combi['copy_from']; ?></td>
        <td> <?php echo $combi['copy_to']; ?> </td>      
        <td> <?php echo str_replace("_"," ",$combi['color']); ?> </td>      
        <td> <?php echo $combi['price']; ?> </td>      
        <td> <?php echo $combi['exp_price']; ?> </td>      
        <td><?php if($combi['price_status']=='0'){ echo 'Pending'; }elseif($combi['price_status']=='1'){ echo 'Approved'; }elseif($combi['price_status'] =='2'){ echo 'Rejected'; } ?></td>
        <td><?php if($combi['exp_price_status']=='0'){ echo 'Pending'; }elseif($combi['exp_price_status']=='1'){ echo 'Approved'; }elseif($combi['exp_price_status'] =='2'){ echo 'Rejected'; } ?></td>

         <td>
            <div class="dropdown">
            	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
            	<div class="dropdown-menu dropdown-menu-right" style="">
            	  <a class="dropdown-item ps_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
            	  <a class="dropdown-item ps_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
            	  <a class="dropdown-item ps_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
            	 </div>
            </div>
             <div class="dropdown margin-bottom">
            	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
            	<div class="dropdown-menu dropdown-menu-right" style="">
            	  <a class="dropdown-item eps_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
            	  <a class="dropdown-item eps_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
            	  <a class="dropdown-item eps_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
            	 </div>
            </div>
        </td>
        
      </tr>
      <?php }  

    } ?>
    
  </tbody>
  <tfoot>
    <tr>
        <th></th>
     
	  <th>#</th>
	   <th>Page From</th>
      <th>Page To</th>
      <th>Color</th>
      <th>Price</th>
      <th>Express Price</th>
      <th>Price Status</th>
      <th>Express Price Status</th>
      <th>Action</th>
	</tr>
	</tfoot>
</table>
