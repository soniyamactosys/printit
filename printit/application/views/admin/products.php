<?php //echo "<pre>"; print_r($printeryshop_list); exit; ?>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Stationary </h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">
					<div class="col-md-12"> <a href="<?php echo site_url('admin/add_stationary'); ?>"  class="btn btn-success mt-10 float-right">+ Add New</a></div>
					<?php /*
					<div class="search_panel_outer container-fluid">
						<form id="search_printery" action="<?php //echo site_url('filter_printeryshop'); ?>" method="post">
						<div class="row">
							<div class="col-md-3">
								 <label>Shop Name</label>

                              <input type="text" value="<?php if(!empty($_POST['shop_name'])){ echo $_POST['shop_name']; } ?>" name="shop_name" id='shop_name' class="form-control filter_class" placeholder='Shop Name'>
							</div>
							<div class="col-md-3">
								 <?php if(empty($this->uri->segment('3'))){ ?>
                               <label>Status</label>
                              <select  name="status" class="selectpicker filter_class">

                                  <option value="" selected="">Status</option>

                                  <option value="0" <?php if(!empty($_POST['status']) && $_POST['status']==0 ){ echo "selected"; } ?>>Active</option>

                                  <option value="1"  <?php if(!empty($_POST['status']) && $_POST['status']==1 ){ echo "selected"; } ?>>In-Active</option>

                                  <option value="3"  <?php if(!empty($_POST['status']) && $_POST['status']==3 ){ echo "selected"; } ?>>Pending</option>

                                  <option value="2"  <?php if(!empty($_POST['status']) && $_POST['status']==2 ){ echo "selected"; } ?>>Blocked</option>

                              </select>


                           <?php  } ?>
							</div>
							<div class="col-md-3">
								<label>Registration Date</label>

                              <input type="date" name="registeration_date" id='registeration_date' class="datepicker form-control filter_class" placeholder='From date' value="<?php if(!empty($_POST['registeration_date'])){ echo $_POST['registeration_date']; } ?>">
                          </div>
                          <div class="col-md-3">
                               <input type='submit' id="" class="waves-effect waves-light btn btn-primary mb-5" value="Search">
                                <a href="<?php echo site_url('printery_list'); ?>"  class="waves-effect waves-light btn btn-secondary mb-5">Clear</a>
							</div>
						</div>
					</form>
					</div>
					*/ ?>

					
<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Image</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Status</th>
								<th>Action</th>
							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($products)){ foreach($products as $value){ $i++; ?>
							<tr>

								<td><?php echo $i;?></td>
								<td><?php echo $value['name'];?></td>
								<td><img src="<?php echo base_url('products/').$value['image']; ?>" height="100px" width="100px" /></td>
								<td><?php echo $value['price'];?></td>
								<td><?php echo $value['qty'];?></td>
								<td><?php if($value['status']=='0'){ echo 'Active'; }elseif($value['status']=='1'){ echo 'In-Active'; } ?></td>
								<td>
                                 <a href="<?php echo site_url('admin/edit_stationary/').encoding($value['id']); ?>" class="btn btn-primary btn-sm" style="margin:5px;"><i class="fa fa-pencil" style="font-size:12px" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('admin/delete_stationary/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>

								</td>

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Image</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Status</th>
								<th>Action</th>
							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>

  <!-- /.content-wrapper -->

  

  <?php /*

<?php include('footer.php'); ?>

  <!-- Control Sidebar -->

  <aside class="control-sidebar">

	  

	<div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white" data-toggle="control-sidebar"></i></span> </div>  <!-- Create the tabs -->

    <ul class="nav nav-tabs control-sidebar-tabs">

      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" class="active"><i class="mdi mdi-message-text"></i></a></li>

      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="mdi mdi-playlist-check"></i></a></li>

    </ul>

    <!-- Tab panes -->

    <div class="tab-content">

      <!-- Home tab content -->

      <div class="tab-pane active" id="control-sidebar-home-tab">

          <div class="flexbox">

			<a href="javascript:void(0)" class="text-grey">

				<i class="ti-more"></i>

			</a>	

			<p>Users</p>

			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>

		  </div>

		  <div class="lookup lookup-sm lookup-right d-none d-lg-block">

			<input type="text" name="s" placeholder="Search" class="w-p100">

		  </div>

          <div class="media-list media-list-hover mt-20">

			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-success" href="#">

				<img src="../images/avatar/1.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>

				</p>

				<p>Praesent tristique diam...</p>

				  <span>Just now</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-danger" href="#">

				<img src="../images/avatar/2.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Luke</strong></a>

				</p>

				<p>Cras tempor diam ...</p>

				  <span>33 min ago</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-warning" href="#">

				<img src="../images/avatar/3.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Evan</strong></a>

				</p>

				<p>In posuere tortor vel...</p>

				  <span>42 min ago</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-primary" href="#">

				<img src="../images/avatar/4.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Evan</strong></a>

				</p>

				<p>In posuere tortor vel...</p>

				  <span>42 min ago</span>

			  </div>

			</div>			

			

			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-success" href="#">

				<img src="../images/avatar/1.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>

				</p>

				<p>Praesent tristique diam...</p>

				  <span>Just now</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-danger" href="#">

				<img src="../images/avatar/2.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Luke</strong></a>

				</p>

				<p>Cras tempor diam ...</p>

				  <span>33 min ago</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-warning" href="#">

				<img src="../images/avatar/3.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Evan</strong></a>

				</p>

				<p>In posuere tortor vel...</p>

				  <span>42 min ago</span>

			  </div>

			</div>



			<div class="media py-10 px-0">

			  <a class="avatar avatar-lg status-primary" href="#">

				<img src="../images/avatar/4.jpg" alt="...">

			  </a>

			  <div class="media-body">

				<p class="font-size-16">

				  <a class="hover-primary" href="#"><strong>Evan</strong></a>

				</p>

				<p>In posuere tortor vel...</p>

				  <span>42 min ago</span>

			  </div>

			</div>

			  

		  </div>



      </div>

      <!-- /.tab-pane -->

      <!-- Settings tab content -->

      <div class="tab-pane" id="control-sidebar-settings-tab">

          <div class="flexbox">

			<a href="javascript:void(0)" class="text-grey">

				<i class="ti-more"></i>

			</a>	

			<p>Todo List</p>

			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>

		  </div>

        <ul class="todo-list mt-20">

			<li class="py-15 px-5 by-1">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_1" class="filled-in">

			  <label for="basic_checkbox_1" class="mb-0 h-15"></label>

			  <!-- todo text -->

			  <span class="text-line">Nulla vitae purus</span>

			  <!-- Emphasis label -->

			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>

			  <!-- General tools such as edit or delete-->

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_2" class="filled-in">

			  <label for="basic_checkbox_2" class="mb-0 h-15"></label>

			  <span class="text-line">Phasellus interdum</span>

			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5 by-1">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_3" class="filled-in">

			  <label for="basic_checkbox_3" class="mb-0 h-15"></label>

			  <span class="text-line">Quisque sodales</span>

			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_4" class="filled-in">

			  <label for="basic_checkbox_4" class="mb-0 h-15"></label>

			  <span class="text-line">Proin nec mi porta</span>

			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5 by-1">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_5" class="filled-in">

			  <label for="basic_checkbox_5" class="mb-0 h-15"></label>

			  <span class="text-line">Maecenas scelerisque</span>

			  <small class="badge bg-primary"><i class="fa fa-clock-o"></i> 1 week</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_6" class="filled-in">

			  <label for="basic_checkbox_6" class="mb-0 h-15"></label>

			  <span class="text-line">Vivamus nec orci</span>

			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 1 month</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5 by-1">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_7" class="filled-in">

			  <label for="basic_checkbox_7" class="mb-0 h-15"></label>

			  <!-- todo text -->

			  <span class="text-line">Nulla vitae purus</span>

			  <!-- Emphasis label -->

			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>

			  <!-- General tools such as edit or delete-->

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_8" class="filled-in">

			  <label for="basic_checkbox_8" class="mb-0 h-15"></label>

			  <span class="text-line">Phasellus interdum</span>

			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5 by-1">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_9" class="filled-in">

			  <label for="basic_checkbox_9" class="mb-0 h-15"></label>

			  <span class="text-line">Quisque sodales</span>

			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

			<li class="py-15 px-5">

			  <!-- checkbox -->

			  <input type="checkbox" id="basic_checkbox_10" class="filled-in">

			  <label for="basic_checkbox_10" class="mb-0 h-15"></label>

			  <span class="text-line">Proin nec mi porta</span>

			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>

			  <div class="tools">

				<i class="fa fa-edit"></i>

				<i class="fa fa-trash-o"></i>

			  </div>

			</li>

		  </ul>

      </div>

      <!-- /.tab-pane -->

    </div>

  </aside>

  <!-- /.control-sidebar -->

  

  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->

  <div class="control-sidebar-bg"></div>

</div>

<!-- ./wrapper -->

	

	<!-- Sidebar -->

	

		

	<!-- Page Content overlay -->

	

	<!-- Vendor JS -->

	<script src="js/vendors.min.js"></script>

	<script src="js/pages/chat-popup.js"></script>

    <script src="../assets/icons/feather-icons/feather.min.js"></script>	<script src="../assets/vendor_components/datatable/datatables.min.js"></script>

	

	<!-- EduAdmin App -->

	<script src="js/template.js"></script>

	

	<script src="js/pages/data-table.js"></script>



</body>



</html>

*/ ?>






