  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Membership</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="<?php echo site_url('add_membership'); ?>"  class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					    
                       
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Amount</th>
								<!--<th>Description</th>
								<th>Features</th>-->
								<th>Type</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableData">
						    <?php $i=0; if(!empty($membership)){ foreach($membership as $value){ $i++; ?>
						    
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $value['title'];?></td>
								<td><?php echo $value['amount'];?></td>
								<!--<td><?php echo $value['description'];?></td>
								<td><?php echo $value['features'];?></td>-->
								<td><?php echo $value['type'];?></td>
								<td><?php echo ($value['status']=='0')?'Active':'In-active'; ?></td>
								<td>
								    <a href="<?php echo site_url('admin/view_membership/').encoding($value['id']); ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
								    <a href="<?php echo site_url('edit_membership/').encoding($value['id']); ?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_membership/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
								</td>
							</tr>
						    <?php } }  ?>

						</tbody>				  
						<tfoot>
							<tr>
							<th>#</th>
								<th>Title</th>
								<th>Amount</th>
								<!--<th>Description</th>
								<th>Features</th>-->
								<th>Type</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  