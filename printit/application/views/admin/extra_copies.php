  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Extra Copies</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					    
                       
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Copies</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableData">
						    <?php $i=0; if(!empty($copies)){ foreach($copies as $value){ $i++; ?>
						    
							<tr class="mytr">
								<td><?php echo $i;?></td>
								<td><?php echo $value['copy_total'];?></td>
								<td><a data-id="<?php echo encoding($value['id']); ?>" data-type="<?php echo $value['copy_total']; ?>" class="btn btn-primary btn-sm edit_copy_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_extra_copy/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
							</tr>
						    <?php } }  ?>

						</tbody>				  
						<tfoot>
							<tr>
							    <th>#</th>
								<th>Copies</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <!-- Modal -->
  <div class="modal center-modal fade" id="add_new" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Add Extra Copy</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/add_extra_copies'); ?>" id="add_extracopy">

		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-3">Copies *</label>
					<div class="col-md-7">
						<input class="form-control" type="text" name="copy" id="copy">
						<span class="text-danger error" id="copy_err" style="display:none;">Please Enter Valid Extra Copies</span>
					</div>
				</div>
		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
  
   
  <!-- Modal -->
  <div class="modal center-modal fade" id="edit_copy_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Edit Extra Copy</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('Admin/edit_extra_copies'); ?>" id="edit_extracopy" method="post">

		  <div class="modal-body">
			<div class="box-body">
			    <input type="hidden" name="id" id="id" />
				<div class="form-group row">
					<label class="col-form-label col-md-3">Extra Copy *</label>
					<div class="col-md-7">
						<input class="form-control" type="text" name="copy" id="edit_copy">
						<span class="text-danger error" id="edit_copy_err" style="display:none;">Please Enter Valid Extra Copy</span>
					</div>
				</div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
	</div>
  <!-- /.modal -->
 
 
<?php  $this->load->view('common/delete_confirmation'); ?>