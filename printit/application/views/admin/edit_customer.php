  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
         <section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Update Customer detail</h4>  
				</div>
				<div class="box-body">
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('admin/edit_customer',array('id'=>'edit_customer'));  ?> 
					<input type="hidden" name="id" value="<?php echo $customer['id'];  ?>">
					<div class="form-group row">
						<label class="col-form-label col-md-2">First Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="fname" id="fname" value="<?php echo $customer['firstname']; ?>">
							<span class="form-text error text-danger" id="fname_err" style="display:none;">Please Enter First name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Last Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="lname" id="nlame" value="<?php echo $customer['lastname']; ?>">
							<span class="form-text error text-danger" id="lname_err" style="display:none;">Please Enter Last name</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Email</label>
						<div class="col-md-10">
							<input class="form-control" type="text" readonly name="email" id="email" value="<?php echo $customer['email']; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Mobile</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="mobile" id="mobile" value="<?php echo $customer['mobile']; ?>">
							
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Address</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="address" id="adm_cus_address" value="<?php echo $customer['address']; ?>">
							<input class="form-control" type="hidden" name="lat" id="adm_cus_lat" value="<?php echo $customer['latitude']; ?>">
							<input class="form-control" type="hidden" name="lng" id="adm_cus_lng" value="<?php echo $customer['longitude']; ?>">
							<span class="form-text error text-danger" id="address_err" style="display:none;">Please Enter Address</span>
						</div>
					</div>
					 <?php
					    
        				//	if(isset($customer_add)) {
        					//  	foreach($customer_add as $list) { ?>
                					<!--<div class="form-group row">
                					    
                						<label class="col-form-label col-md-2"><?php //echo $list['label']; ?></label>
                						<div class="col-md-10">
                							  	<input class="form-control" type="text" name="all_address" id="alladm_cus_address" value="<?php echo $list['address']; ?>">
                        					  	<input class="form-control" type="hidden" name="all_lat" id="alladm_cus_lat" value="<?php echo $list['latitude']; ?>">
                        					  	<input class="form-control" type="hidden" name="all_lng" id="alladm_cus_lng" value="<?php echo $list['longitude']; ?>">
                						</div>
                						
                					</div>-->
					<?php // } }  ?>
					

					<div class="form-group row">
					    <?php if($customer['status'] == 0){ ?>
						<a href="<?php echo site_url('admin/customers/active'); ?>" class="btn btn-warning float-right">Cancel</a>
						<?php } else{?>
						<a href="<?php echo site_url('admin/customers/in-active'); ?>" class="btn btn-warning float-right">Cancel</a>
						<?php } ?>
						<button type="submit" class="btn btn-success float-right">Update</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>