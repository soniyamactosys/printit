  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-12">														
					<div class="box no-shadow mb-0 bg-transparent">
						<div class="box-header no-border px-0">
							<h4 class="box-title">Admin</h4>	
							<!--<ul class="box-controls pull-right d-md-flex d-none">-->
							<!--  <li>-->
							<!--	<button class="btn btn-primary-light px-10">View All</button>-->
							<!--  </li>-->
							<!--</ul>-->
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6 col-12">
					<div class="box bg-secondary-light pull-up">
						<div class="box-body">	
							<div class="flex-grow-1">	
								<div class="d-flex align-items-center pr-2 justify-content-between">
									<div class="d-flex">									
										<span class="badge badge-primary mr-15">Active Orders</span>
										<!-- <span class="badge badge-primary mr-5"><i class="fa fa-lock"></i></span>
										<span class="badge badge-primary"><i class="fa fa-clock-o"></i></span> -->
									</div>
									<!--<div class="dropdown">-->
									<!--	<a data-toggle="dropdown" href="#" class="px-10 pt-5"><i class="ti-more-alt"></i></a>-->
									<!--	<div class="dropdown-menu dropdown-menu-right">-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-import"></i> Import</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-export"></i> Export</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-printer"></i> Print</a>-->
									<!--	  <div class="dropdown-divider"></div>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>-->
									<!--	</div>-->
									<!--</div>						-->
								</div>
								<h4 class="mt-25 mb-5"><?= $pending_orders;  ?></h4>
								<!--<p class="text-fade mb-0 font-size-12">45 Days Left</p>-->
							</div>	
						</div>					
					</div>
				</div>
				<!--<div class="col-xl-3 col-md-6 col-12">
					<div class="box bg-secondary-light pull-up" >
						<div class="box-body">	
							<div class="flex-grow-1">	
								<div class="d-flex align-items-center pr-2 justify-content-between">
									<div class="d-flex">									
										<span class="badge badge-dark mr-15">Pending Orders</span>
									</div>
									<!--<div class="dropdown">
									<!--	<a data-toggle="dropdown" href="#" class="px-10 pt-5"><i class="ti-more-alt"></i></a>-->
									<!--	<div class="dropdown-menu dropdown-menu-right">-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-import"></i> Import</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-export"></i> Export</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-printer"></i> Print</a>-->
									<!--	  <div class="dropdown-divider"></div>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>-->
									<!--	</div>-->
									<!--</div>					
								</div>
								<h4 class="mt-25 mb-5"></h4>
								<!--<p class="text-fade mb-0 font-size-12">Registration Date</p>
							</div>	
						</div>					
					</div>
				</div>-->
				<div class="col-xl-4 col-md-6 col-12">
					<div class="box bg-secondary-light pull-up">
						<div class="box-body">	
							<div class="flex-grow-1">	
								<div class="d-flex align-items-center pr-2 justify-content-between">
									<div class="d-flex">									
										<span class="badge badge-primary mr-15">Completed Orders</span>
										<!-- <span class="badge badge-primary mr-5"><i class="fa fa-lock"></i></span> -->
									</div>
									<!--<div class="dropdown">-->
									<!--	<a data-toggle="dropdown" href="#" class="px-10 pt-5"><i class="ti-more-alt"></i></a>-->
									<!--	<div class="dropdown-menu dropdown-menu-right">-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-import"></i> Import</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-export"></i> Export</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-printer"></i> Print</a>-->
									<!--	  <div class="dropdown-divider"></div>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>-->
									<!--	</div>-->
									<!--</div>						-->
								</div>
								<h4 class="mt-25 mb-5"><?= $completed_orders; ?></h4>
								<!--<p class="text-fade mb-0 font-size-12">Registration Date</p>-->
							</div>	
						</div>					
					</div>
				</div>
				<div class="col-xl-4 col-md-6 col-12">
					<div class="box bg-secondary-light pull-up" style="">
					    <!--//background-image: url(https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-11/512/shop-icon.png); background-position: 100% 75%; background-repeat: no-repeat; background-size: 100px;-->
						<div class="box-body">	
							<div class="flex-grow-1">	
								<div class="d-flex align-items-center pr-2 justify-content-between">
									<div class="d-flex">									
										<span class="badge badge-warning-light mr-15">Total Orders</span>
										<!--<span class="badge badge-warning-light mr-5"><i class="fa fa-lock"></i></span>-->
									</div>
									<!--<div class="dropdown">-->
									<!--	<a data-toggle="dropdown" href="#" class="px-10 pt-5"><i class="ti-more-alt"></i></a>-->
									<!--	<div class="dropdown-menu dropdown-menu-right">-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-import"></i> Import</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-export"></i> Export</a>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-printer"></i> Print</a>-->
									<!--	  <div class="dropdown-divider"></div>-->
									<!--	  <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>-->
									<!--	</div>-->
									<!--</div>						-->
								</div>
								<h4 class="mt-25 mb-5"><?= $pending_orders+$completed_orders ; ?></h4>
								<!--<p class="text-fade mb-0 font-size-12">Registration Date</p>-->
							</div>	
						</div>					
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-6 col-6">
					<div class="box">
						<div class="box-body">
							<p class="text-fade">Orders</p>
							<!--<h3 class="mt-0 mb-20">19 <small class="text-success"><i class="fa fa-arrow-up ml-15 mr-5"></i> 2 New</small></h3>-->
							<!--<div id="charts_widget_2_chart"></div>-->
							 <!--<canvas id="pieChart"></canvas>-->
							 <!--<ul class="legend">-->
                    
        <!--                  <li class="legend__item">-->
        <!--                    <div style="background-color: #04a08b; height: 10px; width: 10px; margin-top: 3px; margin-right: 10px;"></div>-->
        <!--                    <div style="margin-right: 10px;">Express Orders</div>-->
        <!--                    <input type="hidden" id="one" class="input" value='<?=  $totalExporder ?>'>-->
        <!--                  </li>-->
                    
        <!--                  <li class="legend__item">-->
        <!--                    <div style="background-color: #ec8000; height: 10px; width: 10px; margin-top: 3px; margin-right: 10px;"></div>-->
        <!--                    <div style="margin-right: 10px;">Normal Orders</div>-->
        <!--                    <input type="hidden" id="two" class="input" value='<?=  $totalNororder ?>'>-->
        <!--                  </li>-->
                    
        <!--                </ul>-->
                          <div id="chartContainer" style="height: 370px; width: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-6">
					<div class="box">
					
							<p class="text-fade">Earning Graph</p>
							<!--<h3 class="mt-0 mb-20">21 h 30 min <small class="text-danger"><i class="fa fa-arrow-down ml-25 mr-5"></i> 15%</small></h3>-->
							<!--<div id="charts_widget_1_chart"></div>-->
							<div class="box-body">
                        <!--<h3 class="mt-0 mb-20">21 h 30 min <small class="text-danger"><i class="fa fa-arrow-down ml-25 mr-5"></i> 15%</small></h3>-->
                        <!--<div id="charts_widget_1_chart"></div>-->
                        <figure class="highcharts-figure">
                          <div id="container"></div>
                        </figure>
                    </div>
					
					</div>
				</div>
			<!--	<div class="col-xl-3 col-12">
					<div class="box">
						<div class="box-header with-border">
							<h4 class="box-title">Total Earning</h4>
							<ul class="box-controls pull-right d-md-flex d-none">
							  <li class="dropdown">
								<button class="dropdown-toggle btn btn-warning-light px-10" data-toggle="dropdown" href="#">Today</button>										  
								<div class="dropdown-menu dropdown-menu-right">
								  <a class="dropdown-item active" href="#">Today</a>
								  <a class="dropdown-item" href="#">Yesterday</a>
								  <a class="dropdown-item" href="#">Last week</a>
								  <a class="dropdown-item" href="#">Last month</a>
								</div>
							  </li>
							</ul>
						</div>
						<div class="box-body">
							<div id="revenue5"></div>
							<div class="d-flex justify-content-center">
								<p class="d-flex align-items-center font-weight-600 mx-20"><span class="badge badge-xl badge-dot badge-warning mr-20"></span> commission</p>
								<p class="d-flex align-items-center font-weight-600 mx-20"><span class="badge badge-xl badge-dot badge-primary mr-20"></span> Total Earning</p>
							</div>
						</div>
					</div>
				</div>-->
			</div>
		<!--	<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-xl-8 col-12">
							<div class="row">
								<div class="col-12">
									<div class="box bg-transparent no-shadow mb-0">
										<div class="box-header no-border px-0">
											<h4 class="box-title">Recent Orders</h4>							
											<div class="box-controls pull-right d-md-flex d-none">
											  <a href="#">View All</a>
											</div>
										</div>
									</div>
									<div class="box">
										<div class="box-body py-10">
											<div class="table-responsive">
												<table class="table no-border mb-0">
													<tbody>
														<tr>
															<th>Logo</th>
															<th class="font-weight-600">Order Name</th>
															<th class="font-weight-600">Customer Name</th>
															<th class="font-weight-600">Status</th>
															<th class="font-weight-600">Type</th>
															<th class="font-weight-600">Cost</th>
														</tr>
														<tr>
															<td>
																<div class="bg-danger h-50 w-50 l-h-50 rounded text-center">
																  <p class="mb-0 font-size-20 font-weight-600">A1</p>
																</div>
															</td>
															<td class="font-weight-600">Flex Design</td>
															<td class="text-fade">John Doe</td>
															<td class="font-weight-500 text-success"><span class="badge badge-sm badge-dot badge-success mr-10"></span>Finished</td>
															<td class="text-fade">flex</td>
															<td class="font-weight-600">2500</td>
														</tr>
														<tr>
															<td>
																<div class="bg-info h-50 w-50 l-h-50 rounded text-center">
																  <p class="mb-0 font-size-20 font-weight-600">B1</p>
																</div>
															</td>
															<td class="font-weight-600">Contemporary Art</td>
															<td class="text-fade">John Doe</td>
															<td class="font-weight-500 text-warning"><span class="badge badge-sm badge-dot badge-warning mr-10"></span>Active</td>
															<td class="text-fade">flex</td>
															<td class="font-weight-600">2500</td>
														</tr>
														<tr>
															<td>
																<div class="bg-primary h-50 w-50 l-h-50 rounded text-center">
																  <p class="mb-0 font-size-20 font-weight-600">C1</p>
																</div>
															</td>
															<td class="font-weight-600">Flex Design</td>
															<td class="text-fade">John Doe</td>
															<td class="font-weight-500 text-warning"><span class="badge badge-sm badge-dot badge-warning mr-10"></span>Active</td>
															<td class="text-fade">flex</td>
															<td class="font-weight-600">2500</td>
														</tr>
														<tr>
															<td>
																<div class="bg-success h-50 w-50 l-h-50 rounded text-center">
																  <p class="mb-0 font-size-20 font-weight-600">A2</p>
																</div>
															</td>
															<td class="font-weight-600">Flex Design</td>
															<td class="text-fade">John Doe</td>
															<td class="font-weight-500 text-success"><span class="badge badge-sm badge-dot badge-success mr-10"></span>Finished</td>
															<td class="text-fade">flex</td>
															<td class="font-weight-600">2500</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-12 mt-65">
							<!-- <div class="box">
								<div class="box-body">							
									<div id="calendar" class="dask evt-cal min-h-400"></div>
								</div>
							</div> -->
							<!--<a href="#" class="box bg-danger bg-hover-danger pull-up">
								<div class="box-body">
									<div class="d-flex align-items-center">
										<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
											<span class="text-white icon-Cart2 font-size-40"><span class="path1"></span><span class="path2"></span></span>
										</div>
										<div class="ml-10">
											<h4 class="text-white mb-0">+1 1234 456 789</h4>
											<h5 class="text-white-50 mb-0">Toll Free</h5>
										</div>
									</div>							
								</div>
							</a>
							<a href="#" class="box bg-primary bg-hover-primary pull-up">
								<div class="box-body">
									<div class="d-flex align-items-center">
										<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
											<span class="text-white icon-Mail font-size-40"></span>
										</div>
										<div class="ml-10">
											<h4 class="text-white mb-0">print@print.com</h4>
											<h5 class="text-white-50 mb-0">Free to Fill Us</h5>
										</div>
									</div>							
								</div>
							</a>-->
						</div>
					</div>
				</div>
			</div>-->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.co

  <!-- Footer -->



  <!-- Control Sidebar -->

 <aside class="control-sidebar">
	  
	<div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white" data-toggle="control-sidebar"></i></span> </div>  <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" class="active"><i class="mdi mdi-message-text"></i></a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="mdi mdi-playlist-check"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
          <div class="flexbox">
			<a href="javascript:void(0)" class="text-grey">
				<i class="ti-more"></i>
			</a>	
			<p>Users</p>
			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>
		  </div>
		  <div class="lookup lookup-sm lookup-right d-none d-lg-block">
			<input type="text" name="s" placeholder="Search" class="w-p100">
		  </div>
          <div class="media-list media-list-hover mt-20">
			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-success" href="#">
				<img src="../images/avatar/1.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>
				</p>
				<p>Praesent tristique diam...</p>
				  <span>Just now</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-danger" href="#">
				<img src="../images/avatar/2.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Luke</strong></a>
				</p>
				<p>Cras tempor diam ...</p>
				  <span>33 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-warning" href="#">
				<img src="../images/avatar/3.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-primary" href="#">
				<img src="../images/avatar/4.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>			
			
			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-success" href="#">
				<img src="../images/avatar/1.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>
				</p>
				<p>Praesent tristique diam...</p>
				  <span>Just now</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-danger" href="#">
				<img src="../images/avatar/2.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Luke</strong></a>
				</p>
				<p>Cras tempor diam ...</p>
				  <span>33 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-warning" href="#">
				<img src="../images/avatar/3.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-primary" href="#">
				<img src="../images/avatar/4.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>
			  
		  </div>

      </div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
          <div class="flexbox">
			<a href="javascript:void(0)" class="text-grey">
				<i class="ti-more"></i>
			</a>	
			<p>Todo List</p>
			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>
		  </div>
        <ul class="todo-list mt-20">
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_1" class="filled-in">
			  <label for="basic_checkbox_1" class="mb-0 h-15"></label>
			  <!-- todo text -->
			  <span class="text-line">Nulla vitae purus</span>
			  <!-- Emphasis label -->
			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
			  <!-- General tools such as edit or delete-->
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_2" class="filled-in">
			  <label for="basic_checkbox_2" class="mb-0 h-15"></label>
			  <span class="text-line">Phasellus interdum</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_3" class="filled-in">
			  <label for="basic_checkbox_3" class="mb-0 h-15"></label>
			  <span class="text-line">Quisque sodales</span>
			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_4" class="filled-in">
			  <label for="basic_checkbox_4" class="mb-0 h-15"></label>
			  <span class="text-line">Proin nec mi porta</span>
			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_5" class="filled-in">
			  <label for="basic_checkbox_5" class="mb-0 h-15"></label>
			  <span class="text-line">Maecenas scelerisque</span>
			  <small class="badge bg-primary"><i class="fa fa-clock-o"></i> 1 week</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_6" class="filled-in">
			  <label for="basic_checkbox_6" class="mb-0 h-15"></label>
			  <span class="text-line">Vivamus nec orci</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 1 month</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_7" class="filled-in">
			  <label for="basic_checkbox_7" class="mb-0 h-15"></label>
			  <!-- todo text -->
			  <span class="text-line">Nulla vitae purus</span>
			  <!-- Emphasis label -->
			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
			  <!-- General tools such as edit or delete-->
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_8" class="filled-in">
			  <label for="basic_checkbox_8" class="mb-0 h-15"></label>
			  <span class="text-line">Phasellus interdum</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_9" class="filled-in">
			  <label for="basic_checkbox_9" class="mb-0 h-15"></label>
			  <span class="text-line">Quisque sodales</span>
			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_10" class="filled-in">
			  <label for="basic_checkbox_10" class="mb-0 h-15"></label>
			  <span class="text-line">Proin nec mi porta</span>
			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
		  </ul>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<style>
     .canvasjs-chart-credit{
        display: none !important;
     }
     .highcharts-credits{
         display: none !important;
     }
  </style>
<script>
//window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	exportEnabled: true,
	animationEnabled: true,
	title: {
		text: ""
	},
	data: [{
		type: "pie",
		startAngle: 25,
		toolTipContent: "<b>{label}</b>: {y}",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - {y}",
		dataPoints: [
			{ y: <?=  $totalExporder ?>, label: "Express Orders" },
			{ y: <?=  $totalNororder ?>, label: "Normal Orders" }
		]
	}]
});
chart.render();

/*var chartq = new CanvasJS.Chart("chartContainerVendor", {
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	exportEnabled: true,
	animationEnabled: true,
	title: {
		text: ""
	},
	data: [{
		type: "pie",
		startAngle: 25,
		toolTipContent: "<b>{label}</b>: {y}",
		showInLegend: "true",
		legendText: "{label}",
		indexLabelFontSize: 16,
		indexLabel: "{label} - {y}",
		dataPoints: [
			{ y: , label: "Express Orders" },
			{ y: , label: "Normal Orders" }
		]
	}]
});
chartq.render();*/
//earning chart
 Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  xAxis: {
    categories: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'July',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ],
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Earning'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} BHD</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'Earning',
    data: [<?php echo join($chart_data, ',') ?>]
  }]
});
//}
</script>
<!-- ./wrapper -->

	

	<!-- Page Content overlay -->


	

	<!-- Vendor Files -->

