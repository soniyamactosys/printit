
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title"><?= $title; ?></h3>
				</div>
				<!-- /.box-header -->
				<?php echo form_open('admin/update_service_fees',array('id'=>'update_service_form')) ?>

				<div class="box-body">
					<div class="table-responsive">
					  <table id="" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>ID</th>
								<th>Service Type</th>
								<th>Charge</th>
							</tr>
						</thead>
						<tbody>
						     <?php $i=0; if(!empty($serfeelist)){ foreach($serfeelist as $value){ $i++; 
						     ?>
						     <tr>
						    <td><?= $i;?></td>
						    <td><?php echo ucfirst($value['type']); ?>
						    <input type="hidden" name="types[]" value="<?php echo $value['type'];  ?>">
						    </td>
							<td><input type="number" step=".01" class="form-control charges" id="charges" name="charges[]" value="<?php echo $value['charge'];  ?>"></td>
						     </tr>
						    <?php } } ?>
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<span id="common_err" class="text-danger text-center error" style="display:none;"></span>
                <div class="price-update-btn">
                   <button type="submit" >update</button>
                </div>
                <?php echo form_close(); ?>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->
  

