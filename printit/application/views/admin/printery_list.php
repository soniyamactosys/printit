<?php //echo "<pre>"; print_r($printeryshop_list); exit; ?>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Printery Shop <?php if($this->uri->segment('1')=='printery_list_rejected'){ echo "Rejected"; }elseif($this->uri->segment('1')=='printery_list_approved'){ echo "Approved"; } ?></h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

					<div class="search_panel_outer container-fluid">
						<form id="search_printery" action="<?php //echo site_url('filter_printeryshop'); ?>" method="post">
						<div class="row">
							<div class="col-md-3">
								 <label>Shop Name</label>

                              <input type="text" value="<?php if(!empty($_POST['shop_name'])){ echo $_POST['shop_name']; } ?>" name="shop_name" id='shop_name' class="form-control filter_class" placeholder='Shop Name'>
							</div>
							<div class="col-md-3">
								 <?php if(empty($this->uri->segment('3'))){ ?>

                       

                               <label>Status</label>

                              <!--<input type="text" name="status" id='status' class="form-control filter_class" placeholder='Status'>-->

                              <select  name="status" class="selectpicker filter_class">

                                  <option value="" selected="">Status</option>

                                  <option value="0" <?php if(!empty($_POST['status']) && $_POST['status']==0 ){ echo "selected"; } ?>>Active</option>

                                  <option value="1"  <?php if(!empty($_POST['status']) && $_POST['status']==1 ){ echo "selected"; } ?>>In-Active</option>

                                  <option value="3"  <?php if(!empty($_POST['status']) && $_POST['status']==3 ){ echo "selected"; } ?>>Pending</option>

                                  <option value="2"  <?php if(!empty($_POST['status']) && $_POST['status']==2 ){ echo "selected"; } ?>>Blocked</option>

                              </select>


                           <?php  } ?>
							</div>
							<div class="col-md-3">
								<label>Registration Date</label>

                              <input type="date" name="registeration_date" id='registeration_date' class="datepicker form-control filter_class" placeholder='From date' value="<?php if(!empty($_POST['registeration_date'])){ echo $_POST['registeration_date']; } ?>">
                          </div>
                          <div class="col-md-3">
                               <input type='submit' id="" class="waves-effect waves-light btn btn-primary mb-5" value="Search">
                                <a href="<?php echo site_url('printery_list'); ?>"  class="waves-effect waves-light btn btn-secondary mb-5">Clear</a>
							</div>
						</div>
					</form>
					</div>

					
            <div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Shop Name</th>

								<!-- <th>P.O. Box</th>

								<th>Block</th> -->

								<th>Address</th>

								<!-- <th>Flat/Shop</th> -->

								<th>Contact</th>

								<th>Working Hours</th>

								<th>Detail Status</th>

								<th>Registration Date</th>

								<th>Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($printeryshop_list)){ foreach($printeryshop_list as $value){ $i++; ?>

						    

							<tr vid="<?php echo encoding($value['id']); ?>" >

								<td><?php echo $i;?></td>

								<td><?php echo $value['shop_name'];?></td>

								<td><?php echo $value['po_box'].' '.$value['building'];?></td>

								<td><?php echo $value['mobile'];?></td>

								<td><?php echo date('h:i A',strtotime($value['start_time'])).' '.date('h:i A',strtotime($value['end_time'])); ?></td>

								<td><?php if($value['shop_status']=='0'){ echo 'Pending'; }elseif($value['shop_status']=='1'){ echo 'Approved'; }elseif($value['shop_status'] =='2'){ echo 'Rejected'; } ?></td>


								<td><?php echo date('d-m-Y',strtotime($value['registeration_date'])); ?></td>

								<td>
                        <div class="dropdown">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Detail Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item vendor_status" st="pending" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item vendor_status"  st="approve" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item vendor_status"  st="reject" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	  
                        	</div>
                        </div>

<!-- <div class="dropdown">
	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
	<div class="dropdown-menu dropdown-menu-right" style="">
	  <a class="dropdown-item vp_status" st="pending" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
	  <a class="dropdown-item vp_status"  st="approved" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
	  <a class="dropdown-item vp_status"  st="rejected" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
	  
	</div>
</div> -->

<a href="<?php echo site_url('admin/view_branch_pricelist/').encoding($value['id']); ?>" name="" class="btn btn-warning btn-sm" id="upd">View Price List</a>


									<a href="<?php echo site_url('vendor_detail/').encoding($value['user_id']); ?>" name="update" class="btn btn-warning btn-sm" id="upd"><i class="fa fa-eye" aria-hidden="true"></i></a>

								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_printery/').encoding($value['user_id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>

								<!-- <a href="javascript:void(0)" class="btn btn-primary btn-sm shopchangestatus" style="margin:5px;"><i class="fa fa-close" style="font-size:12px" aria-hidden="true"></i></a> -->

							</td>

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Shop Name</th>
								<th>Address</th>

								<th>Contact</th>

								<th>Working Hours</th>
								<th>Detail Status</th>
								<th>Registration Date</th>

								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>

  <!-- /.content-wrapper -->


<!-- Modal -->
  <div class="modal center-modal fade" id="v_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <form id="vendor_status_form" action="<?php echo site_url('admin/vendor_status_update'); ?>">
                	<input type="hidden" name="vendor_id" id="vendor_id">
                	<input type="hidden" name="status" id="status">
                	<button type="submit" class="btn btn-success float-right" >Yes</button>
                </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  <!-- Modal -->
  <div class="modal center-modal fade" id="vp_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="vp_status_form" action="<?php echo site_url('admin/vp_status_update'); ?>">
            	<input type="hidden" name="vendor_id" id="vendor_id">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->