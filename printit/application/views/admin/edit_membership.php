  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Edit Membership</h4>  
				</div>
				<div class="box-body">
				    <form id="edit_membership" action="<?php echo site_url('Admin/edit_membership'); ?>">
				        <input type="hidden" name="id" value="<?php echo $data['id'];  ?>" />
					<div class="form-group row">
						<label class="col-form-label col-md-2">Title</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="title" id="title" value="<?php echo $data['title']; ?>">
							<span class="form-text error text-danger" id="title_err" style="display:none;">Please Enter Title</span>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Description</label>
						<div class="col-md-10">
							<textarea name="description" id="description" class="form-control"><?php echo $data['description']; ?></textarea>
							<span class="form-text error text-danger" id="description_err" style="display:none;">Please Enter Description</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Features</label>
						<div class="col-md-10">
							<textarea name="editfeatureseditor" id="editfeatureseditor" class="form-control"><?php echo $data['features']; ?></textarea>
							<span class="form-text error text-danger" id="edit_features_err" style="display:none;">Please Enter Features</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Amount</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="amount" id="amountt" value="<?php echo $data['amount']; ?>">
							<span class="form-text error text-danger" id="amount_err" style="display:none;">Please Enter Amount</span>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Period</label>
						<div class="col-md-10">
							<select class="form-control" name="period" id="period" style="width: 100%;" tabindex="-1" aria-hidden="true">
						    <option value="">Please select</option>
						    <option value="Monthly" <?php if($data['type']=='Monthly'){ echo 'selected'; } ?> >Monthly</option>
						    <option value="Quarterly" <?php if($data['type']=='Quarterly'){ echo 'selected'; } ?> >Quarterly</option>
						    <option value="Half Yearly" <?php if($data['type']=='Half Yearly'){ echo 'selected'; } ?> >Half Yearly</option>
						    <option value="Yearly" <?php if($data['type']=='Yearly'){ echo 'selected'; } ?> >Yearly</option>
						    </select>   
							<span class="form-text error text-danger" id="period_err" style="display:none;">Please Enter Period</span>
						</div>
					</div>
					
					<div class="form-group row">
						<a href="<?php echo site_url('membership'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right">Update</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>