  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Update note</h4>  
				</div>
				<div class="box-body">
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('admin/edit_note',array('id'=>'edit_note'));  ?> 
					<input type="hidden" name="id" value="<?php echo $data['id'];  ?>">
					<div class="form-group row">
						<label class="col-form-label col-md-2">Note Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="name" id="name" value="<?php echo $data['name']; ?>">
							<span class="form-text error text-danger" id="name_err" style="display:none;">Please Enter Note name</span>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Notes Category</label>
						<div class="col-md-10">
						    <select  class="form-control" name="note_cat">
    						 <?php
        					if(isset($note_cat)) {
        					  	foreach($note_cat as $list) { ?>
                             <option <?php if($data['category']==$list['id']){ ?> selected="selected" <?php } ?> value="<?= $list['id']; ?>"><?= $list['name']; ?><option>
        					<?php }}  ?>
    					</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Total Pages</label>
						<div class="col-md-10">
							<input class="form-control allownumericwithoutdecimal" type="text" name="total_pages" id="total_pages" value="<?php echo $data['total_pages']; ?>">
							<span class="form-text error text-danger" id="total_pages_err" style="display:none;">Please Enter Total Pages</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Description</label>
						<div class="col-md-10">
						 <textarea type="text" name="description" id="editor1" class="form-control"><?php echo $data['description']; ?></textarea>
 	                    <span class="form-text error text-danger" id="edi_err" style="display:none;">Please Enter Notes Description</span>
						
						</div>
					</div>
						<div class="form-group row">
						<label class="col-form-label col-md-2"></label>
						<div class="col-md-10">
							<a href="<?php echo base_url('uploaded_notes/').$data['document']; ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
						</div>
					</div>
					 <div class="form-group row">
						<label class="col-form-label col-md-2">Document</label>
						<div class="col-md-10">
							<input class="form-control docInput" type="file" name="document" accept=".pdf" id="doc" is_valid="1">
							<span class="form-text error text-danger error" id="doc_err" style="display:none;">Please Upload document</span>
						</div>
					</div>
						<div class="form-group row">
						<label class="col-form-label col-md-2"></label>
						<div class="col-md-10">
							<img src="<?php echo base_url('uploaded_notes/images/').$data['image']; ?>" height="100px" width="100px">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Image</label>
						<div class="col-md-10">
							<input class="form-control" type="file" name="image" accept=".jpg,.png,.jpeg" id="image" is_valid="1">
							<span class="form-text error text-danger image_err" id="image_err" style="display:none;">Please Upload Image</span>
						</div>
					</div>

					

					<div class="form-group row">
						<a href="<?php echo site_url('admin/note'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right">Update</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>