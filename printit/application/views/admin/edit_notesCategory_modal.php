
		  <div class="modal-header">

			<h5 class="modal-title">Edit Notes Category</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/edit_notes_category'); ?>" id="edit_notes_category" method="post">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-3">Name *</label>

					<div class="col-md-9">

						<input class="form-control" type="text" name="name" id="edit_name" value="<?php echo $detail['name']; ?>">

						<input type="hidden" name="id" id="id" value="<?php echo encoding($detail['id']); ?>">

						<span class="text-danger error" id="edit_name_erra" style="display:none;">Please Enter Valid Type</span>

					</div>

				</div>



		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Update</button>

		  </div>

        </form>