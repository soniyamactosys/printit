<?php //echo "<pre>"; print_r($detail); exit; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Vendor Profile</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo site_url('dashboard'); ?>"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><a href="<?php echo site_url('printery_list'); ?>">Printery Shop</a></li>
								<li class="breadcrumb-item active" aria-current="page">Profile</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">

		  <div class="row">
			<div class="col-12 col-lg-7 col-xl-8">
				
			  <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li><a class="active" href="#company_detail" data-toggle="tab">Company Detail</a></li>
				  <li><a href="#branches_detail" data-toggle="tab">Branch Detail</a></li>
				  <li><a href="#bank_detail" data-toggle="tab">Bank Detail</a></li>
				</ul>

				<div class="tab-content">

				 <div class="active tab-pane" id="company_detail">
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<!-- <p>
							<time class="float-right text-fade" datetime="2017-07-14 20:00">2 hours ago</time>
							</p> -->
							<p><?php echo $detail['owner_fname'].' '.$detail['owner_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Company Name</strong>
							<div class="media-body">
							<p><?php echo $detail['shop_name']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Number of Branches</strong>
							<div class="media-body">
							<p><?php echo empty($detail['total_branches'])?'Branches Not added yet':$detail['total_branches']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Registeration Number</strong>
							<div class="media-body">
							<p><?php echo $detail['reg_number']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Registeration Proof</strong>
							<div class="media-body">
							<p><button class="btn btn-success"><a style="text-decoration: none;" href="<?php echo base_url('vendor_uploads/').$detail['user_id'].'/'.$detail['reg_proof']; ?>" download=""><i class="fa fa-download"></i></a></button></p>
							</div>
						</div>
						<div class="media">
							<strong>Company Email</strong>
							<div class="media-body">
							<p><?php echo $detail['c_email']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Company Contact Number</strong>
							<div class="media-body">
							<p><?php echo $detail['c_contact']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Number of Workers</strong>
							<div class="media-body">
							<p><?php echo $detail['total_workers']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>P.O. Box</strong>
							<div class="media-body">
							<p><?php echo $detail['po_box']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Address</strong>
							<div class="media-body">
							<p><?php echo $detail['building']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Working Hours</strong>
							<div class="media-body">
							<p><?php echo date('h:i A',strtotime($detail['start_time']))." - ".date('h:i A',strtotime($detail['end_time'])); ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Services</strong>
							<div class="media-body">
							<p><?php echo $detail['serviceNames']; ?></p>
							</div>
						</div>
															
					  </div>
					</div>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4>Incharge Person Detail</h4>
						</div>

						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<p><?php echo $detail['in_fname'].' '.$detail['in_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Email Address</strong>
							<div class="media-body">
							<p><?php echo $detail['in_email']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Mobile Number</strong>
							<div class="media-body">
							<p><?php echo $detail['in_mobile']; ?></p>
							</div>
						</div>			
						</div>
					</div>
					<div class="box p-15"> 
						<div class="timeline timeline-single-column timeline-single-full-column">
							<?php if(!empty($detail['company_images'])){  ?>
							<span class="timeline-label">
								<span class="badge badge-info badge-pill">Images</span>
							</span>
							
							<div class="timeline-item">
								<div class="timeline-point timeline-point-success">
									<i class="fa fa-image"></i>
								</div>
								<div class="timeline-event">
									<div class="timeline-heading">
										<!-- <h4 class="timeline-title"><a href="#">Rakesh Kumar</a><small> uploaded new photos</small></h4> -->
									</div>
									<div class="timeline-body">
										<?php 
											$c_images = explode(",",$detail['company_images']);
											foreach($c_images as $val){ ?>
											<img src="<?php echo base_url('vendor_uploads/').$detail['user_id'].'/'.$val; ?>" alt="..." class="m-10">
											<?php 
											}
										?>						
									</div>
									<!-- <div class="timeline-footer">
										<p class="text-right"><i class="fa fa-clock-o"></i> 8 days ago</p>
									</div> -->
								</div>
							</div>
							<?php } ?>	


						</div>
					</div>  
				  </div>  

				 <div class="tab-pane" id="branches_detail">
				 	<?php if(!empty($detail['branches'])){ foreach($detail['branches'] as $branch){ ?>
				 	
					<div class="box">

					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4><?php 
							if($branch['status']==0){ $status = 'Active'; }elseif($branch['status']==1){ $status = 'Inactive'; }elseif($branch['status']==2){ $status = 'Pending'; }
							echo $branch['shop_name'].' - '.$status; ?></h4>
						</div>
						<div class="media">
							<strong>Contact Number</strong>
							<div class="media-body">
							<p><?php echo $branch['contact']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Number of Workers</strong>
							<div class="media-body">
							<p><?php echo $branch['total_workers']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Address</strong>
							<div class="media-body">
							<p><?php echo $branch['po_box'].' '.$branch['building'] ; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Working Hours</strong>
							<div class="media-body">
							<p><?php echo date('h:i A',strtotime($branch['start_time']))." - ".date('h:i A',strtotime($branch['end_time'])); ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Services</strong>
							<div class="media-body">
							<p><?php echo $branch['serviceNames']; ?></p>
							</div>
						</div>
					  </div>
					</div>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<h4>Incharge Person Detail</h4>
						</div>

						<div class="media">
							<strong>Full Name</strong>
							<div class="media-body">
							<p><?php echo $branch['in_fname'].' '.$branch['in_lname']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Email Address</strong>
							<div class="media-body">
							<p><?php echo $branch['in_email']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Mobile Number</strong>
							<div class="media-body">
							<p><?php echo $branch['in_mobile']; ?></p>
							</div>
						</div>			
						</div>
					</div>
					<?php }}else{ ?>
					<div class="box">
						
						<div class="media-list media-list-divided bg-lighter">
						<div class="media">
							
							<div class="media-body">
							<p>Branches are not added yet</p>
							</div>
						</div>
						</div>
					</div>
					<?php } ?>
				  </div> 				    
				  <!-- /.tab-pane -->
				 <div class="tab-pane" id="bank_detail">
				 	<?php if(!empty($detail['bank_acc'])){ ?>
					<div class="box">
					  <div class="media-list media-list-divided bg-lighter">
						<div class="media">
							<strong>Account Holder Name</strong>
							<div class="media-body">
							<p><?php echo empty($detail['bank_acc'])?'':$detail['bank_acc']['acc_holder_name']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Bank Name</strong>
							<div class="media-body">
							<p><?php echo empty($detail['bank_acc'])?'':$detail['bank_acc']['bank_name']; ?></p>
							</div>
						</div>						
						<div class="media">
							<strong>Branch Name</strong>
							<div class="media-body">
							<p><?php echo empty($detail['bank_acc'])?'':$detail['bank_acc']['branch_name']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>Account Number</strong>
							<div class="media-body">
							<p><?php echo empty($detail['bank_acc'])?'':$detail['bank_acc']['acc_number']; ?></p>
							</div>
						</div>
						<div class="media">
							<strong>SWIFT Code</strong>
							<div class="media-body">
							<p><?php echo empty($detail['bank_acc'])?'':$detail['bank_acc']['ifsc_code']; ?></p>
							</div>
						</div>
						
						</div>
					</div>
					<?php }else{ ?>
					<div class="box">
						
						<div class="media-list media-list-divided bg-lighter">
						<div class="media">
							
							<div class="media-body">
							<p>Bank Details not added yet</p>
							</div>
						</div>
						</div>
					</div>
					<?php }  ?>

				  </div> 
				 
				  <!-- /.tab-pane -->

				  
				  <!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			  </div>
			  <!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->		

			  <div class="col-12 col-lg-5 col-xl-4">
				 <div class="box box-widget widget-user">
					<?php
					
					$profile1 = './vendor_uploads/'.$detail['user_id'].'/'.$detail['logo'];
					if(!file_exists($profile1)){
						$profileimg = base_url('assets/images/male_profile.png');
					}else{
						$profileimg = base_url('vendor_uploads/').$detail['user_id'].'/'.$detail['logo'];
					}
					if(!empty($detail['company_images'])){
						$imgArr = explode(",",$detail['company_images']);
						$company_image = base_url('vendor_uploads/').$detail['user_id'].'/'.$imgArr[0];
					}else{
						$company_image = base_url('assets/images/gallery/full/10.jpg');
					}
					 ?>

					<!-- Add the bg color to the header using any of the bg-* classes -->
					<div class="widget-user-header bg-black" style="background: url('<?php echo $company_image; ?>') center center;">
					  <h3 class="widget-user-username"><?php echo $detail['owner_fname'].' '. $detail['owner_lname']; ?></h3>
					  <h6 class="widget-user-desc">Printery Shop</h6>
					</div>
					<div class="widget-user-image">
					  <img class="rounded-circle" src="<?php echo base_url('assets/images/user3-128x128.jpg'); ?>" alt="User Avatar">
					</div>
					<div class="box-footer">
					  <div class="row">
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header"><?php echo $current_orders; ?></h5>
							<span class="description-text">Current Orders</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 br-1 bl-1">
						  <div class="description-block">
							<h5 class="description-header"><?php echo $completed_orders; ?></h5>
							<span class="description-text">Completed Orders</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col 
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">158</h5>
							<span class="description-text">Canceled Orders</span>
						  </div>
						</div>
						 /.col -->
					  </div>
					  <!-- /.row -->
					</div>
				  </div>
				  <div class="box">
					<div class="box-body box-profile">            
					  <div class="row">
						<div class="col-12">
							<div>
								<p>Email :<span class="text-gray pl-10"><?php echo  $detail['c_email']; ?></span> </p>
								<p>Phone :<span class="text-gray pl-10"><?php echo  $detail['c_contact']; ?></span></p>
								<p>Address :<span class="text-gray pl-10"><?php echo $detail['po_box'].', '.$detail['building']; ?></span></p>
							</div>
						</div>
						<!-- <div class="col-12">
							<div class="pb-15">						
								<p class="mb-10">Social Profile</p>
								<div class="user-social-acount">
									<button class="btn btn-circle btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></button>
									<button class="btn btn-circle btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></button>
									<button class="btn btn-circle btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></button>
								</div>
							</div>
						</div> -->
						<div class="col-12">
							<div>
								<div class="map-box">
									<?php
									echo '<iframe src = "https://maps.google.com/maps?q='.$detail['latitude'].','.$detail['longitude'].'&hl=es;z=14&amp;output=embed"></iframe>';
									?>
								</div>
							</div>
						</div>
					  </div>
					</div>
					<!-- /.box-body -->
				  </div>
				  <?php /*
				<div class="box">
					<div class="box-body">
						<div class="flexbox align-items-baseline mb-20">
						  <h6 class="text-uppercase ls-2">Friends</h6>
						  <small>20</small>
						</div>
						<div class="gap-items-2 gap-y">
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/1.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/3.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/4.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/5.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/6.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/7.jpg'); ?>" alt="..."></a>
						  <a class="avatar" href="#"><img src="<?php echo base_url('assets/images/avatar/8.jpg'); ?>" alt="..."></a>
						  <a class="avatar avatar-more" href="#">+15</a>
						</div>
					</div>
					<div class="box-footer">
						<a class="text-uppercase d-blockls-1 text-fade" href="#">Invite People</a>
					</div>
				</div>
				<div class="box box-inverse" style="background-color: #3b5998">
				  <div class="box-header no-border">
					<span class="fa fa-facebook font-size-30"></span>
					  <div class="box-tools pull-right">
						<h5 class="box-title box-title-bold">Facebook feed</h5>
					  </div>
				  </div>

				  <blockquote class="blockquote blockquote-inverse no-border m-0 py-15">
					<p>Holisticly benchmark plug imperatives for multifunctional deliverables. Seamlessly incubate cross functional action.</p>
					<div class="flexbox">
					  <time class="text-white" datetime="2017-11-21 20:00">21 November, 2017</time>
					  <span><i class="fa fa-heart"></i> 75</span>
					</div>
				  </blockquote>
				</div>
				*/ ?>

			  </div>

		  </div>
		  <!-- /.row -->

		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
 
<?php /* 
include('footer.php'); ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar">
	  
	<div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white" data-toggle="control-sidebar"></i></span> </div>  <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" class="active"><i class="mdi mdi-message-text"></i></a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="mdi mdi-playlist-check"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
          <div class="flexbox">
			<a href="javascript:void(0)" class="text-grey">
				<i class="ti-more"></i>
			</a>	
			<p>Users</p>
			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>
		  </div>
		  <div class="lookup lookup-sm lookup-right d-none d-lg-block">
			<input type="text" name="s" placeholder="Search" class="w-p100">
		  </div>
          <div class="media-list media-list-hover mt-20">
			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-success" href="#">
				<img src="../images/avatar/1.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>
				</p>
				<p>Praesent tristique diam...</p>
				  <span>Just now</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-danger" href="#">
				<img src="../images/avatar/2.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Luke</strong></a>
				</p>
				<p>Cras tempor diam ...</p>
				  <span>33 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-warning" href="#">
				<img src="../images/avatar/3.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-primary" href="#">
				<img src="../images/avatar/4.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>			
			
			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-success" href="#">
				<img src="../images/avatar/1.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Tyler</strong></a>
				</p>
				<p>Praesent tristique diam...</p>
				  <span>Just now</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-danger" href="#">
				<img src="../images/avatar/2.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Luke</strong></a>
				</p>
				<p>Cras tempor diam ...</p>
				  <span>33 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-warning" href="#">
				<img src="../images/avatar/3.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>

			<div class="media py-10 px-0">
			  <a class="avatar avatar-lg status-primary" href="#">
				<img src="../images/avatar/4.jpg" alt="...">
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong>Evan</strong></a>
				</p>
				<p>In posuere tortor vel...</p>
				  <span>42 min ago</span>
			  </div>
			</div>
			  
		  </div>

      </div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
          <div class="flexbox">
			<a href="javascript:void(0)" class="text-grey">
				<i class="ti-more"></i>
			</a>	
			<p>Todo List</p>
			<a href="javascript:void(0)" class="text-right text-grey"><i class="ti-plus"></i></a>
		  </div>
        <ul class="todo-list mt-20">
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_1" class="filled-in">
			  <label for="basic_checkbox_1" class="mb-0 h-15"></label>
			  <!-- todo text -->
			  <span class="text-line">Nulla vitae purus</span>
			  <!-- Emphasis label -->
			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
			  <!-- General tools such as edit or delete-->
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_2" class="filled-in">
			  <label for="basic_checkbox_2" class="mb-0 h-15"></label>
			  <span class="text-line">Phasellus interdum</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_3" class="filled-in">
			  <label for="basic_checkbox_3" class="mb-0 h-15"></label>
			  <span class="text-line">Quisque sodales</span>
			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_4" class="filled-in">
			  <label for="basic_checkbox_4" class="mb-0 h-15"></label>
			  <span class="text-line">Proin nec mi porta</span>
			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_5" class="filled-in">
			  <label for="basic_checkbox_5" class="mb-0 h-15"></label>
			  <span class="text-line">Maecenas scelerisque</span>
			  <small class="badge bg-primary"><i class="fa fa-clock-o"></i> 1 week</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_6" class="filled-in">
			  <label for="basic_checkbox_6" class="mb-0 h-15"></label>
			  <span class="text-line">Vivamus nec orci</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 1 month</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_7" class="filled-in">
			  <label for="basic_checkbox_7" class="mb-0 h-15"></label>
			  <!-- todo text -->
			  <span class="text-line">Nulla vitae purus</span>
			  <!-- Emphasis label -->
			  <small class="badge bg-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
			  <!-- General tools such as edit or delete-->
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_8" class="filled-in">
			  <label for="basic_checkbox_8" class="mb-0 h-15"></label>
			  <span class="text-line">Phasellus interdum</span>
			  <small class="badge bg-info"><i class="fa fa-clock-o"></i> 4 hours</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5 by-1">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_9" class="filled-in">
			  <label for="basic_checkbox_9" class="mb-0 h-15"></label>
			  <span class="text-line">Quisque sodales</span>
			  <small class="badge bg-warning"><i class="fa fa-clock-o"></i> 1 day</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
			<li class="py-15 px-5">
			  <!-- checkbox -->
			  <input type="checkbox" id="basic_checkbox_10" class="filled-in">
			  <label for="basic_checkbox_10" class="mb-0 h-15"></label>
			  <span class="text-line">Proin nec mi porta</span>
			  <small class="badge bg-success"><i class="fa fa-clock-o"></i> 3 days</small>
			  <div class="tools">
				<i class="fa fa-edit"></i>
				<i class="fa fa-trash-o"></i>
			  </div>
			</li>
		  </ul>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	
	
	<!-- Page Content overlay -->
	
	
	<!-- Vendor JS -->
	<script src="js/vendors.min.js"></script>
    <script src="../assets/icons/feather-icons/feather.min.js"></script>	
	
	<!-- EduAdmin App -->
	<script src="js/template.js"></script>
	
	<script src="js/pages/timeline.js"></script>
	
	
</body>

</html>
*/ ?>