  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Add Stationary</h4>  
				</div>
				<div class="box-body">
					<div id="php_error" class="text-danger"></div>
					<?php echo form_open_multipart('admin/add_stationary',array('id'=>'add_product')); ?> 
					<div class="form-group row">
						<label class="col-form-label col-md-2">Stationary Name</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="name" id="name">
							<span class="form-text error text-danger" id="name_err" style="display:none;">Please Enter Stationary name</span>
						</div>
					</div>
					
					<!-- <div class="form-group row">
						<label class="col-form-label col-md-2">Description</label>
						<div class="col-md-10">
							<textarea name="description" id="description" class="form-control"></textarea>
							<span class="form-text error text-danger" id="description_err" style="display:none;">Please Enter Description</span>
						</div>
					</div> -->
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Price</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="price" id="price">
							<span class="form-text error text-danger" id="price_err" style="display:none;">Please Enter Price</span>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-md-2">Image</label>
						<div class="col-md-10">
							<input class="form-control imgInput" type="file" name="image" accept=".jpg,.png,.jpeg" id="image" is_valid="1">
							<span class="form-text error text-danger image_err" id="image_err" style="display:none;">Please Upload Image</span>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-md-2">Quantity</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="qty" id="qty" value="1" minimum="1">
							<span class="form-text error text-danger" id="qty_err" style="display:none;" >Please Enter Quantity</span>
						</div>
					</div>
					
					<div class="form-group row">
						<a href="<?php echo site_url('admin/stationary'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right rightalignbtn">Add</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>