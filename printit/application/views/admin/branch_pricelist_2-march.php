<?php //echo "<pre>"; print_r($printeryshop_list); exit; ?>

  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  <!-- CUSTOME PRINT -->

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title"><?php echo $branchDetail['shop_name']; ?>-  Price List - Custom Print
				</div>

				<!-- /.box-header -->

				<div class="box-body">

					

					
              <div class="table-responsive">
					 <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                       
						<thead>
						 <div class="inner-drop" style="display:flex;  align-items: baseline;width: 290px;justify-content: space-around;margin-bottom: 10px;">   
                        <div class="dropdown">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item csall_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item csall_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item csall_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        <div class="dropdown margin-bottom">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item ecsall_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item ecsall_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item ecsall_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        </div>
                        <span class="form-text error text-danger" id="allcid_err" style="display:none;">Please select custom print list</span>
							<tr>
                               <th><input type="checkbox" id="allcustomecheck" name="allcustomecheck"></th>
								<th>#</th>
								<th>Print Type</th>
                                <th>Paper Size</th>
                                <th>Paper Type</th>
                                <th>Printing Sides</th>
                                <th>Banner Size</th>
                                <th>Price</th>
                                <th>Express Price</th>
								<th>Update Date</th>
								<th>Price Status</th>
								<th>Express Price Status</th>
                                <th>Action</th>
							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($pricelist)){ foreach($pricelist as $value){ $i++; ?>

						    

							<tr cid="<?php  echo encoding($value['id']); ?>" >
                               <td><input type="checkbox" class="allcustome" name="allcustome" value="<?php echo $value['id'];?>"></td>
 
								<td><?php echo $i;?></td>
								
								<!-- <td><?php echo $value['service'];?></td> -->

								<td><?php echo $value['print']; ?></td>
								<td><?php echo $value['size_name'];  ?></td>
								<td><?php echo $value['paper']; ?></td>
								<td><?php if(strcasecmp($value['print'],'flyer')==0){ echo $value['printing_sides']; }else{ echo "-"; } ?></td>
								<td><?php if(strcasecmp($value['print'],'banner')==0){ echo $value['banner_size_name']; }else{ echo "-"; } ?></td>
								<?php /*
								<td><?php echo $value['color']; ?></td>
								
								<td><?php echo $value['extra_copies']; ?></td>
								
								*/ ?>

								<td><?php echo $value['price'];?></td>
								<td><?php echo $value['exp_price'];?></td>

								<td><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></td>
								<td><?php if($value['price_status']=='0'){ echo 'Pending'; }elseif($value['price_status']=='1'){ echo 'Approved'; }elseif($value['price_status'] =='2'){ echo 'Rejected'; } ?></td>
								<td><?php if($value['exp_price_status']=='0'){ echo 'Pending'; }elseif($value['exp_price_status']=='1'){ echo 'Approved'; }elseif($value['exp_price_status'] =='2'){ echo 'Rejected'; } ?></td>
                
                                 <td>
                                    <div class="dropdown">
                                    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                                    	<div class="dropdown-menu dropdown-menu-right" style="">
                                    	  <a class="dropdown-item cs_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                                    	  <a class="dropdown-item cs_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                                    	  <a class="dropdown-item cs_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                                    	 </div>
                                    </div>
                                     <div class="dropdown margin-bottom">
                                    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                                    	<div class="dropdown-menu dropdown-menu-right" style="">
                                    	  <a class="dropdown-item ecs_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                                    	  <a class="dropdown-item ecs_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                                    	  <a class="dropdown-item ecs_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                                    	 </div>
                                    </div>
                                </td>
							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>
							     <th></th>
								<th>#</th>
								<!-- <th>Service</th> -->
								<th>Print Type</th>
                                <th>Paper Size</th>
                                <th>Paper Type</th>
                                <th>Printing Sides</th>
                                <th>Banner Size</th>                                
                                <th>Price</th>
                                <th>Express Price</th>
								<th>Update Date</th>
								<th>Price Status</th>
								<th>Express Price Status</th>
								 <th>Action</th>
							</tr>

						</tfoot>

					</table>
			

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>
            <!-- QUICK PRINT -->  
            <div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title"><?php echo $branchDetail['shop_name']; ?>-  Price List - Quick Print
				</div>

				<!-- /.box-header -->

				<div class="box-body">

					

					
              <div class="table-responsive">
					 <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>
                           <div class="inner-drop" style="display:flex;  align-items: baseline;width: 290px;justify-content: space-around;margin-bottom: 10px;">   
                        <div class="dropdown">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item qall_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item qall_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item qall_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        <div class="dropdown margin-bottom">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item qall_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item qall_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item qall_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        </div>
                        <span class="form-text error text-danger" id="allqid_err" style="display:none;">Please select custom print list</span>
						
							<tr>
                                <th><input type="checkbox" id="allquickcheck" name="allquickcheck"></th>
								<th>#</th>
                                <th>Paper Size</th>
                                <th>Paper Type</th>
                                <th>Paper Color</th>       
                                <th>Price</th>
                                <th>Express Price</th>
								<th>Update Date</th>
								<th>Price Status</th>
								<th>Express Price Status</th>
                                <th>Action</th>
							</tr>
                         
						</thead>
						

						<tbody id="tableData">

						    <?php $i=0; if(!empty($pricelistquick)){ foreach($pricelistquick as $value){ $i++; ?>

						    

							<tr cid="<?php  echo encoding($value['id']); ?>" >
                                <td><input type="checkbox" class="allquick" name="allquick" value="<?php echo $value['id'];?>"></td>
                                <td><?php echo $i;?></td>
								<td><?php echo $value['size_name'];  ?></td>
								<td><?php echo $value['paper']; ?></td>
								<td><?php echo $value['color']; ?></td>
								<td><?php echo $value['price'];?></td>
								<td><?php echo $value['exp_price'];?></td>

								<td><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></td>
								<td><?php if($value['price_status']=='0'){ echo 'Pending'; }elseif($value['price_status']=='1'){ echo 'Approved'; }elseif($value['price_status'] =='2'){ echo 'Rejected'; } ?></td>
								<td><?php if($value['exp_price_status']=='0'){ echo 'Pending'; }elseif($value['exp_price_status']=='1'){ echo 'Approved'; }elseif($value['exp_price_status'] =='2'){ echo 'Rejected'; } ?></td>
                
                                 <td>
                                    <div class="dropdown">
                                    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                                    	<div class="dropdown-menu dropdown-menu-right" style="">
                                    	  <a class="dropdown-item cs_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                                    	  <a class="dropdown-item cs_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                                    	  <a class="dropdown-item cs_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                                    	 </div>
                                    </div>
                                
                                    <div class="dropdown margin-bottom">
                                    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                                    	<div class="dropdown-menu dropdown-menu-right" style="">
                                    	  <a class="dropdown-item ecs_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                                    	  <a class="dropdown-item ecs_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                                    	  <a class="dropdown-item ecs_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                                    	 </div>
                                    </div>
                                </td>
							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>
								<th></th>
								<th>#</th>
                                <th>Paper Size</th>
                                <th>Paper Type</th>
                                <th>Paper Color</th>    
                                <th>Price</th>
                                <th>Express Price</th>
								<th>Update Date</th>
								<th>Price Status</th>
								<th>Express Price Status</th>
								 <th>Action</th>
							</tr>

						</tfoot>

					</table>
			

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>
            <!-- NOTES -->  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title"><?php echo $branchDetail['shop_name']; ?>-  Price List -Notes
				</div>

				<!-- /.box-header -->

				<div class="box-body">

					

					
              <div class="table-responsive">
					 
                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                  <thead>
                       <div class="inner-drop" style="display:flex;  align-items: baseline;width: 290px;justify-content: space-around;margin-bottom: 10px;">   
                        <div class="dropdown">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item psall_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item psall_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item psall_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        <div class="dropdown margin-bottom">
                        	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                        	<div class="dropdown-menu dropdown-menu-right" style="">
                        	  <a class="dropdown-item pssall_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                        	  <a class="dropdown-item pssall_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                        	  <a class="dropdown-item pssall_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                        	 </div>
                        </div>
                        </div>
                        <span class="form-text error text-danger" id="allpscid_err" style="display:none;">Please select note list</span>
						
                    <tr>
                        <th><input type="checkbox" id="allnotecheck" name="allnotecheck"></th>
					   <th>#</th>
                      <th>Page From</th>
                      <th>Page To</th>
                      <th>Price</th>
                      <th>Express Price</th>
                      <th>Price Status</th>
                      <th>Express Price Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="notes_price_table">
                    <?php 
                     if(!empty($notes_price)){ 
                      $i=0;
                      foreach($notes_price as $combi){ 
                      $i++;
                      $key=$combi['npr_id'];
                      ?>
                      	<tr note_id="<?php  echo encoding($combi['id']); ?>" >
                      	    <td><input type="checkbox" class="allnote" name="allnote" value="<?php echo $combi['id'];?>"></td>
 
                      		<td><?php echo $i;?></td>
                        <td> <?php echo $combi['copy_from']; ?></td>
                        <td> <?php echo $combi['copy_to']; ?> </td>      
                        <td> <?php echo $combi['price']; ?> </td>      
                        <td> <?php echo $combi['exp_price']; ?> </td>      
                        <!--<td><input name="price[<?php echo $key; ?>]" value="<?php echo empty($combi['price'])?'0':$combi['price'] ; ?>" type="number" step=".01" class="form-control price"> -->
                        <!--  <input type="hidden" name="copy_from[<?php echo $key; ?>]" value="<?php echo $combi['copy_from']; ?>"> -->
                        <!--  <input type="hidden" name="copy_to[<?php echo $key; ?>]" value="<?php echo $combi['copy_to']; ?>"> -->
                        <!--  <input type="hidden" name="npr_id[<?php echo $key; ?>]" value="<?php echo $combi['npr_id']; ?>">-->
                        <!--  <input type="hidden" name="price_status[<?php echo $key; ?>]" value="<?php echo $combi['id']; ?>">-->
                          
                        <!-- </td> -->
                         <td><?php if($combi['price_status']=='0'){ echo 'Pending'; }elseif($combi['price_status']=='1'){ echo 'Approved'; }elseif($combi['price_status'] =='2'){ echo 'Rejected'; } ?></td>
                         <td><?php if($combi['exp_price_status']=='0'){ echo 'Pending'; }elseif($combi['exp_price_status']=='1'){ echo 'Approved'; }elseif($combi['exp_price_status'] =='2'){ echo 'Rejected'; } ?></td>
                
                         <td>
                            <div class="dropdown">
                            	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                            	<div class="dropdown-menu dropdown-menu-right" style="">
                            	  <a class="dropdown-item ps_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                            	  <a class="dropdown-item ps_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                            	  <a class="dropdown-item ps_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                            	 </div>
                            </div>
                             <div class="dropdown margin-bottom">
                            	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                            	<div class="dropdown-menu dropdown-menu-right" style="">
                            	  <a class="dropdown-item eps_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                            	  <a class="dropdown-item eps_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                            	  <a class="dropdown-item eps_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                            	 </div>
                            </div>
                        </td>
                        
                      </tr>
                      <?php }  
                
                    } ?>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                        <th></th>
                     
    				  <th>#</th>
    				   <th>Page From</th>
                      <th>Page To</th>
                      <th>Price</th>
                      <th>Express Price</th>
                      <th>Price Status</th>
                      <th>Express Price Status</th>
                      <th>Action</th>
					</tr>
					</tfoot>
                </table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

			<!-- /.col -->
		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>

  <!-- /.content-wrapper -->
<!-- /. custom modal -->
  <div class="modal center-modal fade" id="cs_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="cs_status_form" action="<?php echo site_url('admin/custome_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="cid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
<!-- /. custom modal with express price-->
  <div class="modal center-modal fade" id="ecs_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update Express Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="ecs_status_form" action="<?php echo site_url('admin/custome_express_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="ecid">
            	<input type="hidden" name="status" id="estatus">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->

  <!-- /. notes modal -->
  <div class="modal center-modal fade" id="ps_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="ps_status_form" action="<?php echo site_url('admin/notes_price_status_update'); ?>">
            	<input type="hidden" name="note_id" id="note_id">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
   <!-- /. express notes modal -->
  <div class="modal center-modal fade" id="eps_status_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="eps_status_form" action="<?php echo site_url('admin/notes_express_price_status_update'); ?>">
            	<input type="hidden" name="note_id" id="enote_id">
            	<input type="hidden" name="status" id="epstatus">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
  <!-- /. custom modal -->
  <div class="modal center-modal fade" id="csall_status_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="csall_status_forms" action="<?php echo site_url('admin/custome_all_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allcid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
    <!-- /. custom modal -->
  <div class="modal center-modal fade" id="psall_status_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="psall_status_forms" action="<?php echo site_url('admin/notes_all_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allpscid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
      <!-- /. custom modal -->
  <div class="modal center-modal fade" id="psall_status_new_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="psall_status_new_forms" action="<?php echo site_url('admin/notes_all_exp_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allexppscid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
   <div class="modal center-modal fade" id="qall_status_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="qall_status_forms" action="<?php echo site_url('admin/quick_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allqid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal center-modal fade" id="qeall_status_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="qeall_status_forms" action="<?php echo site_url('admin/quick_exp_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allqid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal center-modal fade" id="csexpall_status_form" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do you really want to update All Price status of this printery?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <form id="csexpall_status_forms" action="<?php echo site_url('admin/custome_all_exp_price_status_update'); ?>">
            	<input type="hidden" name="cid" id="allexpcid">
            	<input type="hidden" name="status" id="status">
            	<button type="submit" class="btn btn-success float-right" >Yes</button>
            </form>
			
		  </div>
		</div>
	  </div>
	</div>