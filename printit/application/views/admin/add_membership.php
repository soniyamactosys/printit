  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Add Membership</h4>  
				</div>
				<div class="box-body">
				    <form id="add_membership" action="<?php echo site_url('Admin/add_membership'); ?>">
					<div class="form-group row">
						<label class="col-form-label col-md-2">Title</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="title" id="title">
							<span class="form-text error text-danger" id="title_err" style="display:none;">Please Enter Title</span>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Description</label>
						<div class="col-md-10">
							<textarea name="description" id="description" class="form-control"></textarea>
							<span class="form-text error text-danger" id="description_err" style="display:none;">Please Enter Description</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Features</label>
						<div class="col-md-10">
							<textarea name="featureseditor" id="featureseditor" class="form-control"></textarea>
							<span class="form-text error text-danger" id="features_err" style="display:none;">Please Enter Features</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Amount</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="amount" id="amount">
							<span class="form-text error text-danger" id="amount_err" style="display:none;">Please Enter Amount</span>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-form-label col-md-2">Period</label>
						<div class="col-md-10">
							<select class="form-control" name="period" id="period" style="width: 100%;" tabindex="-1" aria-hidden="true">
						    <option valu="">Please select</option>
						    <option value="Monthly">Monthly</option>
						    <option value="Quarterly">Quarterly</option>
						    <option value="Half Yearly">Half Yearly</option>
						    <option value="Yearly">Yearly</option>
						    </select>   
							<span class="form-text error text-danger" id="period_err" style="display:none;">Please Enter Period</span>
						</div>
					</div>
					
					<div class="form-group row">
						<a href="<?php echo site_url('membership'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right">Add</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>