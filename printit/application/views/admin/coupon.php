  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Coupon Codes</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					    
                       
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Code</th>
								<th>Users</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Type</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableData">
						    <?php $i=0; if(!empty($coupon)){ foreach($coupon as $value){ $i++; ?>
						    
							<tr class="mytr">
								<td><?php echo $i;?></td>
								<td class="code"><?php echo $value['code'];?></td>
								<td class="users"><?php echo $value['users'];?></td>
								<td class="st_date"><?php echo date('d-m-Y',strtotime($value['start_date'])); ?></td>
								<td class="end_date"><?php echo date('d-m-Y',strtotime($value['end_date']));?></td>
								<td class="amount"><?php echo $value['amount'];?></td>
								<td class="type"><?php echo ($value['type']=='0')?'Fixed':'Percent(%)';?></td>
								<td class="status"><?php echo ($value['status']=='0')?'Active':'In-active';?></td>
								<td><a data-id="<?php echo encoding($value['id']); ?>" data-type="<?php echo $value['type']; ?>" class="btn btn-primary btn-sm  edit_coupon_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_coupon/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
							</tr>
						    <?php } }  ?>

						</tbody>				  
						<tfoot>
							<tr>
							    <th>#</th>
								<th>Code</th>
								<th>Users</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Type</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <!-- Modal -->
  <div class="modal center-modal fade" id="add_new" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Add New Coupon Code</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('admin/add_coupon'); ?>" id="add_coupon">

		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-4">Coupon Code *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="code" id="code">
						<span class="text-danger error" id="code_err" style="display:none;">Please Enter Coupon Code</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Users *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="users" id="users">
						<span class="text-danger error" id="user_err" style="display:none;">Please Enter 
						Number of Coupon Code Users</span>
					</div>
				</div>
				<?php /*
				<div class="form-group row">
					<label class="col-form-label col-md-4">Start Date *</label>
					<div class="col-md-8">
						<input class="form-control" type="date" name="start_date" id="start_date">
						<span class="text-danger error" id="start_date_err" style="display:none;">Please Enter Coupon Start Date</span>
					</div>
				</div>
				*/ ?>


				<div class="form-group row">
					<label class="col-form-label col-md-4">Start Date *</label>
					<div class="col-md-8">
						<input class="form-control coupon_from datepciker" type="text" name="start_date" id="start_date">
						<span class="text-danger error" id="start_date_err" style="display:none;">Please Enter Coupon Start Date</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">End Date *</label>
					<div class="col-md-8">
						<input class="form-control coupon_to datepciker" type="text" name="end_date" id="end_date">
						<span class="text-danger error" id="end_date_err" style="display:none;">Please Enter End Date</span>
					</div>
				</div>
				  
                <div class="form-group row">
					<label class="col-form-label col-md-4">Type *</label>
					<div class="col-md-8">
						<select name="coupon_type" id="coupon_type" class="form-control">
						    <option value="">Please select</option>
						    <option value="1">Percentage(%)</option>
						    <option value="0">Fixed</option>
						</select>
						<span class="text-danger error" id="coupon_type_err" style="display:none;">Please Select Type</span>
					</div>
				</div>

				<div class="form-group row" style="display: none;" id="amountDiv_amt">
					<label class="col-form-label col-md-4">Coupon Amount *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="amount" id="amount">
						<span class="text-danger error" id="amount_err" style="display:none;">Please Enter Valid Coupon Amount</span>
					</div>
				</div>
				<div class="form-group row" style="display: none;" id="amountDiv_percent">
					<label class="col-form-label col-md-4">Coupon Percent *<br>Less than 91%</label>

					<div class="col-md-8">
						<input class="form-control" type="text" name="coupon_percent" id="coupon_percent" placeholder="%" >
						<span class="text-danger error" id="per_amount_err" style="display:none;">Please Enter Valid Coupon Percent</span>
					</div>
				</div>
		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
  
   
  <!-- Modal -->
  <div class="modal center-modal fade" id="edit_coupon_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Edit Coupon Code</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('admin/edit_coupon'); ?>" id="edit_coupon" method="post">

		  <div class="modal-body">
			<div class="box-body">
			    <input type="hidden" name="id" id="id" />
				<div class="form-group row">
					<label class="col-form-label col-md-4">Coupon Code *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="code" id="edit_code">
						<span class="text-danger error" id="edit_code_err" style="display:none;">Please Enter Coupon Code</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Users *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="users" id="edit_users">
						<span class="text-danger error" id="edit_user_err" style="display:none;">Please Enter Number of Coupon Users</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">Start Date *</label>
					<div class="col-md-8">
						<input class="form-control datepicker e_coupon_from" type="text" name="start_date" id="edit_start_date">
						<span class="text-danger error" id="edit_start_date_err" style="display:none;">Please Enter Coupon Start Date</span>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-4">End Date *</label>
					<div class="col-md-8">
						<input class="form-control datepicker e_coupon_to" type="text" name="end_date" id="edit_end_date">
						<span class="text-danger error" id="edit_end_date_err" style="display:none;">Please Enter End Date</span>
					</div>
				</div>

                <div class="form-group row">
					<label class="col-form-label col-md-4">Type *</label>
					<div class="col-md-8">
						<select name="coupon_type" id="edit_coupon_type" class="form-control">
						    <option value="">Please select</option>
						    <option value="1">Percentage(%)</option>
						    <option value="0">Fixed</option>
						</select>
						<span class="text-danger error" id="edit_coupon_type_err" style="display:none;">Please Select Type</span>
					</div>
				</div>

				<!-- <div class="form-group row">
					<label class="col-form-label col-md-4">Coupon Amount *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="amount" id="edit_amount">
						<span class="text-danger error" id="edit_amount_err" style="display:none;">Please Enter Valid Coupon Amount</span>
					</div>
				</div>	 -->


				<div class="form-group row" style="display: none;" id="e_amountDiv_amt">
					<label class="col-form-label col-md-4">Coupon Amount *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="amount" id="e_amount">
						<span class="text-danger error" id="e_amount_err" style="display:none;">Please Enter Valid Coupon Amount</span>
					</div>
				</div>
				<div class="form-group row" style="display: none;" id="e_amountDiv_percent">
					<label class="col-form-label col-md-4">Coupon Percent *<br>Less than 91%</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="coupon_percent" id="e_coupon_percent" placeholder="%" >
						<span class="text-danger error" id="e_per_amount_err" style="display:none;">Please Enter Valid Coupon Percent</span>
					</div>
				</div>

		  </div>

		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
 