
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Master Setting - Paper Type</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

				   <div class="col-md-12"> <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new_paper" class="btn btn-success mt-10 float-right">+ Add New</a></div>

					<div class="table-responsive" style="margin-top: 15px;">

					    <!-- Date Filter -->

                       <!--<table class="ml-25">-->

                           <!--<form id="search_printery" action="<?php echo site_url('filter_printeryshop'); ?>" method="post">-->

                       <!--  <tr class="form-group">-->

                       <!--    <td>-->

                       <!--        <label>Shop Name</label>-->

                       <!--       <input type="text" value="<?php if(!empty($_POST['shop_name'])){ echo $_POST['shop_name']; } ?>" name="shop_name" id='shop_name' class="form-control filter_class" placeholder='Shop Name'>-->

                       <!--    </td>-->

                       <!--    <?php if(empty($this->uri->segment('3'))){ ?>-->

                       <!--    <td>-->

                       <!--        <label>Status</label>-->

                              <!--<input type="text" name="status" id='status' class="form-control filter_class" placeholder='Status'>-->

                       <!--       <select  name="status" class="form-control filter_class">-->

                       <!--           <option value=""></option>-->

                       <!--           <option value="0" <?php if(!empty($_POST['status']) && $_POST['status']==0 ){ echo "selected"; } ?>>Active</option>-->

                       <!--           <option value="1"  <?php if(!empty($_POST['status']) && $_POST['status']==1 ){ echo "selected"; } ?>>In-Active</option>-->

                       <!--           <option value="3"  <?php if(!empty($_POST['status']) && $_POST['status']==3 ){ echo "selected"; } ?>>Pending</option>-->

                       <!--           <option value="2"  <?php if(!empty($_POST['status']) && $_POST['status']==2 ){ echo "selected"; } ?>>Blocked</option>-->

                       <!--       </select>-->

                       <!--    </td>-->

                       <!--    <?php  } ?>-->

                       <!--    <td>-->

                       <!--        <label>Registration Date</label>-->

                       <!--       <input type="date" name="registeration_date" id='registeration_date' class="datepicker form-control filter_class" placeholder='From date' value="<?php if(!empty($_POST['registeration_date'])){ echo $_POST['registeration_date']; } ?>">-->

                       <!--    </td>-->

                          

                       <!--    <td>-->

                       <!--       <input type='submit' id="" class="form-control mt-30" value="Search">-->

                       <!--    </td>-->

                       <!--    <td>-->

                       <!--        <a href="<?php echo site_url('printery_list'); ?>"  class="form-control mt-30">Clear</a>-->

                       <!--    </td>-->

                       <!--  </tr>-->

                       <!--  </form>-->

                       <!--</table>-->

                       

					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Paper Type</th>
								<th>Description</th>
								<th>Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($paper_type)){ foreach($paper_type as $value){ $i++; ?>

						    

							<tr>

								<td><?php echo $i;?></td>

								<td><?php echo $value['name'];?></td>
								<td><?php echo $value['description'];?></td>

								<td><a data-title="<?php echo $value['name']; ?>" data-id="<?php echo encoding($value['id']); ?>" name="update" class="btn btn-primary btn-sm  edit_paper_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>

								<!--<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_papertype/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>-->

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Paper Type</th>
								<th>Description</th>

								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  </div>

  </div>

  <!-- /.content-wrapper -->

  

  

  <!-- Modal -->

  <div class="modal center-modal fade" id="add_new_paper" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Add New Paper Type</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/add_paper_type'); ?>" id="add_paper_type">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-3">Type *</label>

					<div class="col-md-9">

						<input class="form-control" type="text" name="name" id="name">

						<span class="text-danger error" id="name_err" style="display:none;">Please Enter Valid Type</span>

					</div>

				</div>
				<div class="form-group row">

					<label class="col-form-label col-md-3">Description *</label>

					<div class="col-md-9">

						<textarea name="description" id="pt_description" class="form-control"></textarea>

						<span class="text-danger error" id="desc_err" style="display:none;">Please Enter Description</span>

					</div>

				</div>

		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Add</button>

		  </div>

        </form>

		</div>

	  </div>

	</div>

  <!-- /.modal -->

  

  

   

  <!-- Modal -->

  <div class="modal center-modal fade" id="edit_paper_modal" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		</div>

	  </div>

	</div>

  <!-- /.modal -->

 