<?php //echo "<pre>"; print_r($customers); exit; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
		  <div class="row">
			<div class="col-12">
			  <!-- /.box -->
			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title"><?php echo ucfirst($this->uri->segment('3')); ?> Customers </h3>
				</div>

				<!-- /.box-header -->

				<div class="box-body">
					<?php /*
					<div class="search_panel_outer container-fluid">
						<form id="search_printery" action="<?php //echo site_url('filter_printeryshop'); ?>" method="post">
						<div class="row">
							<div class="col-md-3">
								 <label>Shop Name</label>

                              <input type="text" value="<?php if(!empty($_POST['shop_name'])){ echo $_POST['shop_name']; } ?>" name="shop_name" id='shop_name' class="form-control filter_class" placeholder='Shop Name'>
							</div>
							<div class="col-md-3">
								 <?php if(empty($this->uri->segment('3'))){ ?>

                       

                               <label>Status</label>

                              <!--<input type="text" name="status" id='status' class="form-control filter_class" placeholder='Status'>-->

                              <select  name="status" class="selectpicker filter_class">

                                  <option value="" selected="">Status</option>

                                  <option value="0" <?php if(!empty($_POST['status']) && $_POST['status']==0 ){ echo "selected"; } ?>>Active</option>

                                  <option value="1"  <?php if(!empty($_POST['status']) && $_POST['status']==1 ){ echo "selected"; } ?>>In-Active</option>

                                  <option value="3"  <?php if(!empty($_POST['status']) && $_POST['status']==3 ){ echo "selected"; } ?>>Pending</option>

                                  <option value="2"  <?php if(!empty($_POST['status']) && $_POST['status']==2 ){ echo "selected"; } ?>>Blocked</option>

                              </select>


                           <?php  } ?>
							</div>
							<div class="col-md-3">
								<label>Registration Date</label>

                              <input type="date" name="registeration_date" id='registeration_date' class="datepicker form-control filter_class" placeholder='From date' value="<?php if(!empty($_POST['registeration_date'])){ echo $_POST['registeration_date']; } ?>">
                          </div>
                          <div class="col-md-3">
                               <input type='submit' id="" class="waves-effect waves-light btn btn-primary mb-5" value="Search">
                                <a href="<?php echo site_url('printery_list'); ?>"  class="waves-effect waves-light btn btn-secondary mb-5">Clear</a>
							</div>
						</div>
					</form>
					</div>
					*/ ?>

					
                    <div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>
								<th>#</th>
								<th>Customer Name</th>
								<th>Mobile</th>
								<th>Email Address</th>
								<th>Gender</th>
								<th>Address</th>
								<th>Status</th>
								<th>Registration Date</th>
								<th>Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($customers)){ foreach($customers as $value){ $i++; ?>
							<tr>

								<td><?php echo $i;?></td>

								<td><?php echo ucfirst($value['full_name']); ?></td>

								<td><?php echo $value['mobile'];?></td>

								<td><?php echo $value['email'];?></td>

								<td><?php echo ucfirst($value['gender']); ?></td>

								<td><?php echo $value['address'];?></td>
								<td><?php if($value['status']=='0'){ echo 'Active'; }elseif($value['status']=='1'){ echo 'In-Active'; } ?></td>

								<td><?php echo date('d-m-Y',strtotime($value['created_at'])); ?></td>

								<td>
									
		
								<a href="<?php echo site_url('admin/edit_customer/').encoding($value['id']); ?>" class="btn btn-primary btn-sm" action=""> <i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_customer/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>

								
								</td>

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>
								<th>Customer Name</th>
								<th>Mobile</th>
								<th>Email Address</th>
								<th>Gender</th>
								<th>Address</th>
								<th>Status</th>
								<th>Registration Date</th>
								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>

  <!-- /.content-wrapper -->







