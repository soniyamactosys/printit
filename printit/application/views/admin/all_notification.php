
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">All Notifications</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

				   <?php /*<div class="col-md-12"> <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new_paper" class="btn btn-success mt-10 float-right">+ Add New</a></div> */ ?>

					<div class="table-responsive" style="margin-top: 15px;">


					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($data)){ foreach($data as $value){ $i++; ?>
							<tr>
								<td><?php echo $i;?></td>
								<td><a href="<?php if($value['status']==0){ echo $value['redirect']; }else{ echo 'javascript:void(0)';} ?>"><?php echo $value['title'];?></a></td>
							</tr>
						    <?php } }  ?>
						</tbody>				  
						<tfoot>

							<tr>
								<th>#</th>
								<th>Title</th>
							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  </div>

  </div>
  <!-- /.content-wrapper -->