  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Paper Size</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

				     <div class="col-md-12"><a href="javascript:void(0)" data-toggle="modal" data-target="#add_new_paper" class="btn btn-success mt-10 float-right">+ Add New</a></div>

					<div class="table-responsive"style="margin-top: 15px;">

					    

                       

					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Paper Size</th>
								<!--<th>Status</th>-->

								<th align="right">Action</th>

							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($paper_type)){ foreach($paper_type as $value){ $i++; ?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $value['name'];?></td>
								<?php /*<td><?php if($value['status']=='0'){ echo 'Active';}else{ echo "In-active"; } ?></td> */?>

								<td><a data-title="<?php echo $value['name']; ?>" data-id="<?php echo encoding($value['id']); ?>" name="update" class="btn btn-primary btn-sm  edit_paper_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>

								<!--<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_papersize/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>-->
								<!--<div class="dropdown">-->
								<!--	<a data-toggle="dropdown" href="#" class="px-10 pt-5" aria-expanded="false">Status</a>-->
								<!--	<div class="dropdown-menu dropdown-menu-right" style="">-->
								<!--	  <a class="dropdown-item" href="#"><i class="si-check si"></i> active</a>-->
								<!--	  <a class="dropdown-item" href="#"><i class="si-clock si"></i> pending</a>-->
								<!--	  <a class="dropdown-item" href="#"><i class="si-close si"></i> inactive</a>-->
									  
								<!--	</div>-->
								<!--</div>-->
							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Paper Size</th>
							<!--<th>Status</th>-->
								<th>Action</th>

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  </div>

  </div>

  <!-- /.content-wrapper -->

  

  

  <!-- Modal -->

  <div class="modal center-modal fade" id="add_new_paper" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Add New Paper Size</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/add_paper_size'); ?>" id="add_paper_size">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-2">Size *</label>

					<div class="col-md-10">

						<input class="form-control" type="text" name="name" id="name">

						<span class="text-danger error" id="name_err" style="display:none;">Please Enter Valid Size</span>

					</div>

				</div>

		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Add</button>

		  </div>

        </form>

		</div>

	  </div>

	</div>

  <!-- /.modal -->

  

  

   

  <!-- Modal -->

  <div class="modal center-modal fade" id="edit_paper_modal" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Edit Paper Size</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/edit_paper_size'); ?>" id="edit_paper_size" method="post">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-2">Size *</label>

					<div class="col-md-10">

						<input class="form-control" type="text" name="name" id="edit_name">

						<input type="hidden" name="id" id="id">

						<span class="text-danger error" id="edit_name_err" style="display:none;">Please Enter Valid Type</span>

					</div>

				</div>

		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Add</button>

		  </div>

        </form>

		</div>

	  </div>

	</div>

  <!-- /.modal -->

 