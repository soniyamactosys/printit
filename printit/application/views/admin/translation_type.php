
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">translation Type</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">

				   <div class="col-md-12"> <a href="javascript:void(0)" data-toggle="modal" data-target="#add_tran_type" class="btn btn-success mt-10 float-right">+ Add New</a></div>

					<div class="table-responsive" style="margin-top: 15px;">

					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

						<thead>

							<tr>

								<th>#</th>

								<th>Name</th>
								
                                
							</tr>

						</thead>

						<tbody id="tableData">

						    <?php $i=0; if(!empty($translation_type)){ foreach($translation_type as $value){ $i++; ?>

						    

							<tr>

								<td><?php echo $i;?></td>

								<td><?php echo $value['name'];?></td>
								<!--<td><a data-title="<?php echo $value['name']; ?>" data-id="<?php echo encoding($value['id']); ?>" name="update" class="btn btn-primary btn-sm mr-5 edit_notes_category_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>

								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_papertype/').encoding($value['id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>-->

							</tr>

						    <?php } }  ?>



						</tbody>				  

						<tfoot>

							<tr>

								<th>#</th>

								<th>Name</th>
								

							</tr>

						</tfoot>

					</table>

					</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  </div>

  </div>

  <!-- /.content-wrapper -->

  

  

  <!-- Modal -->

  <div class="modal center-modal fade" id="add_tran_type" tabindex="-1">

	  <div class="modal-dialog">

		<div class="modal-content">

		  <div class="modal-header">

			<h5 class="modal-title">Add New Translational Type</h5>

			<button type="button" class="close" data-dismiss="modal">

			  <span aria-hidden="true">&times;</span>

			</button>

		  </div>

          <form action="<?php echo site_url('Admin/add_tran_type'); ?>" id="add_tran_type">



		  <div class="modal-body">

			<div class="box-body">

				<div class="form-group row">

					<label class="col-form-label col-md-3">Name *</label>

					<div class="col-md-9">

						<input class="form-control" type="text" name="name" id="name">

						<span class="text-danger error" id="name_err" style="display:none;">Please Enter Valid Name</span>

					</div>

				</div>
			

		    </div>

		  </div>

		  <div class="modal-footer modal-footer-uniform">

			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

				<button type="submit" class="btn btn-success float-right">Add</button>

		  </div>

        </form>

		</div>

	  </div>

	</div>

  <!-- /.modal -->

 