 <table id="example1" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
   
	<thead>
	 <div class="inner-drop" style="display:flex;  align-items: baseline;width: 290px;justify-content: space-around;margin-bottom: 10px;">   
    <div class="dropdown">
    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
    	<div class="dropdown-menu dropdown-menu-right" style="">
    	  <a class="dropdown-item csall_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
    	  <a class="dropdown-item csall_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
    	  <a class="dropdown-item csall_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
    	 </div>
    </div>
    <div class="dropdown margin-bottom">
    	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
    	<div class="dropdown-menu dropdown-menu-right" style="">
    	  <a class="dropdown-item ecsall_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
    	  <a class="dropdown-item ecsall_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
    	  <a class="dropdown-item ecsall_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
    	 </div>
    </div>
    </div>
    <span class="form-text error text-danger" id="allcid_err" style="display:none;">Please select custom print list</span>
		<tr>
           <th><input type="checkbox" id="allcustomecheck" name="allcustomecheck"></th>
			<th>#</th>
			<th>Print Type</th>
            <th>Paper Size</th>
            <th>Paper Type</th>
            <th>Printing Sides</th>
            <th>Banner Size</th>
            <th>Price</th>
            <th>Express Price</th>
			<th>Update Date</th>
			<th>Price Status</th>
			<th>Express Price Status</th>
            <th>Action</th>
		</tr>

	</thead>

	<tbody id="tableData">

	    <?php $i=0; if(!empty($pricelist)){ foreach($pricelist as $value){ $i++; ?>

	    

		<tr cid="<?php  echo encoding($value['id']); ?>" >
           <td><input type="checkbox" class="allcustome" name="allcustome" value="<?php echo $value['id'];?>"></td>

			<td><?php echo $i;?></td>
			<td><?php echo $value['print']; ?></td>
			<td><?php echo $value['size_name'];  ?></td>
			<td><?php echo $value['paper']; ?></td>
			<td><?php if(strcasecmp($value['print'],'flyer')==0){ echo $value['printing_sides']; }else{ echo "-"; } ?></td>
			<td><?php if(strcasecmp($value['print'],'banner')==0){ echo $value['banner_size_name']; }else{ echo "-"; } ?></td>
			<td><?php echo $value['price'];?></td>
			<td><?php echo $value['exp_price'];?></td>

			<td><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></td>
			<td><?php if($value['price_status']=='0'){ echo 'Pending'; }elseif($value['price_status']=='1'){ echo 'Approved'; }elseif($value['price_status'] =='2'){ echo 'Rejected'; } ?></td>
			<td><?php if($value['exp_price_status']=='0'){ echo 'Pending'; }elseif($value['exp_price_status']=='1'){ echo 'Approved'; }elseif($value['exp_price_status'] =='2'){ echo 'Rejected'; } ?></td>

             <td>
                <div class="dropdown">
                	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Price Status</a>
                	<div class="dropdown-menu dropdown-menu-right" style="">
                	  <a class="dropdown-item cs_status" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                	  <a class="dropdown-item cs_status"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                	  <a class="dropdown-item cs_status"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                	 </div>
                </div>
                 <div class="dropdown margin-bottom">
                	<a data-toggle="dropdown" href="javascript:void(0)" class="btn btn-primary" aria-expanded="false">Express Price Status</a>
                	<div class="dropdown-menu dropdown-menu-right" style="">
                	  <a class="dropdown-item ecs_status_new" st="0" href="javascript:void(0)" ><i class="si-check si"></i> Pending</a>
                	  <a class="dropdown-item ecs_status_new"  st="1" href="javascript:void(0)" ><i class="si-check si"></i> Approve</a>
                	  <a class="dropdown-item ecs_status_new"  st="2" href="javascript:void(0)" ><i class="si-clock si"></i> Reject</a>
                	 </div>
                </div>
            </td>
		</tr>

	    <?php } }  ?>



	</tbody>				  

	<tfoot>

		<tr>
		     <th></th>
			<th>#</th>
			<th>Print Type</th>
            <th>Paper Size</th>
            <th>Paper Type</th>
            <th>Printing Sides</th>
            <th>Banner Size</th>                                
            <th>Price</th>
            <th>Express Price</th>
			<th>Update Date</th>
			<th>Price Status</th>
			<th>Express Price Status</th>
			 <th>Action</th>
		</tr>

	</tfoot>

</table>