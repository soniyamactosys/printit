  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  
			<div class="col-12">
			  <!-- /.box -->

			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Service Management</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
				    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new" class="btn btn-success mt-10 float-right">+ Add New</a>
					<div class="table-responsive">
					    
                       
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
								<th>#</th>
								<th>Service Type</th>
								<th>Print Type</th>
								<th>Paper Type</th>
								<th>Paper Size</th>
								<th>Color</th>
								<th>Printing Sides</th>
								<th>Copy Range</th>							
								<th>Width</th>
								<th>Binding</th>
								<!--<th>Status</th>-->
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableData">
						    <?php $i=0; 
//echo "<pre>"; print_r($management); exit;
						    
						    if(!empty($management)){ foreach($management as $value){ $i++; 
						    	//echo "<pre>"; print_r($value['color']); exit;?>
						    
							<tr>
								<td><?php echo $i;?></td>
								
								<td class="order_type" id="<?php echo $value['service_id']; ?>"><?php echo $value['service_name']; ?></td>
								<td class="print_type" id="<?php echo $value['print_type_id']; ?>"><?php echo $value['print_type'];?></td>
								<td class="paper_type" id="<?php echo $value['paper_type_id']; ?>"><?php echo $value['paper_type'];?></td>
								<td class="size" id="<?php echo $value['paper_size_id']; ?>"><?php if(!empty($value['size'])){ echo $value['size']; }else{ echo "-"; }?></td>
								<td class="color" id="<?php echo $value['color']; ?>"><?php if(!empty($value['color'])){ echo str_replace('_',' ',$value['color']); }else{ echo "-"; } ?></td>
								<td class="printing_sides" id="<?php echo $value['printing_sides']; ?>"><?php if(!empty($value['printing_sides'])){ echo ($value['printing_sides']=='single')?'One side':'Two side'; }else{ echo "-"; } ?></td>
								
								<td class="width" id="<?php echo $value['flyer_copy_from']; ?>"><?php if(!empty($value['flyer_copy_from'])){ echo $value['flyer_copy_from']."-". $value['flyer_copy_to']; }else{ echo "-"; } ?></td>
								<td class="width" id="<?php echo $value['banner_size']; ?>"><?php if(!empty($value['banner_size'])){ echo $value['banner_size_name']; }else{ echo "-"; } ?></td>
								
								<td class="binding" id="<?php echo $value['binding']; ?>"><?php if(!empty($value['binding'])){ echo $value['binding']; }else{ echo "-"; }?></td>
								
								<!--<td class="status"><?php echo ($value['pm_status']=='0')?'Active':'In-active';?></td>-->
								<td>
									<!--<a href="<?php echo site_url('admin/view_price_management/').encoding($value['pm_id']); ?>" data-id="<?php echo encoding($value['pm_id']); ?>" class="btn btn-info btn-sm mr-5"><i class="fa fa-eye" aria-hidden="true"></i></a>-->
									<!--<a data-id="<?php echo encoding($value['pm_id']); ?>" class="btn btn-primary btn-sm mr-5 edit_pm_modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>-->
								<a href="javascript:void(0)" class="btn btn-danger btn-sm delete" action="<?php echo site_url('Admin/delete_printmanagement/').encoding($value['pm_id']); ?>"> <i class="fa fa-trash" aria-hidden="true"></i></a>
							</tr>
						    <?php } }   ?>

						</tbody>				  
						<tfoot>
							<tr>
							    <th>#</th>
								<th>Service Type</th>
								<th>Print Type</th>
								<th>Paper Type</th>
								<th>Paper Size</th>
								<th>Color</th>
								<th>Printing Sides</th>
								<th>Copy Range</th>
								<th>Width</th>
								
								<th>Binding</th>
								<!--<th>Status</th>-->
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <!-- Modal -->
  <div class="modal center-modal fade" id="add_new" tabindex="-1">
	  <div class="modal-dialog adminmodalarea">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Add New Print Management</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
          <form action="<?php echo site_url('admin/add_price_combination'); ?>" id="add_print_management">

		  <div class="modal-body">
			<div class="box-body">
				<div class="form-group row">
					<label class="col-form-label col-md-4">Service Type *</label>
					<div class="col-md-8">
					    <select name="order_type" id="order_type" class="form-control">
						    <option value="">Please select</option>
						    <option value="1">Custom Print</option>
						    <option value="2">Quick Print</option>
						    <?php /*if(!empty($services)){ foreach($services as $value){ ?>
						    	<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } }*/ ?>
						</select>
						<span class="text-danger error" id="ordertype_err" style="display:none;">Please Select Service Type</span>
					</div>
				</div>
				<div class="form-group row" id="printTypeDiv" style="display: none;">
					<label class="col-form-label col-md-4">Print Type *</label>
					<div class="col-md-8">
					    <select name="print_type" id="print_type" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($print_type)){ foreach($print_type as $print){ ?>
						    <option value="<?php echo $print['id']; ?>"><?php echo $print['name']; ?></option>
						    <?php } } ?>
						</select>
						<span class="text-danger error" id="printtype_err" style="display:none;">Please Select Print Type</span>
					</div>
				</div>
				<div class="form-group row"  id="paperTypeDiv">
					<label class="col-form-label col-md-4">Paper Type *</label>
					<div class="col-md-8">
                        <select name="paper_type" id="paper_type" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_type)){ foreach($paper_type as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="papertype_err" style="display:none;">Please select Paper Type</span>
					</div>
				</div>
				<div class="form-group row" id="papersizeDiv">
					<label class="col-form-label col-md-4">Paper Size *</label>
					<div class="col-md-8">
						<select name="paper_size" id="paper_size" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($paper_size)){ foreach($paper_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<span class="text-danger error" id="papersize_err" style="display:none;">Please select Paper Size</span>
					</div>
				</div>
				<div class="form-group row" id="colorDiv" style="display: none;">
					<label class="col-form-label col-md-4">Color *</label>
					<div class="col-md-8">
						<select name="color" id="color" class="form-control">
						    <option value="">Please select</option>
						    <option value="colored">colored</option>
						    <option value="black_&_white">black & white</option>
						</select>

						<span class="text-danger error" id="color_err" style="display:none;">Please select Color</span>
					</div>
				</div>
				<div class="form-group row" id="bindingDiv" style="display: none;">
					<label class="col-form-label col-md-4">Binding *</label>
					<div class="col-md-8">
						<select name="binding" id="binding" class="form-control">
						    <option value="">Please select</option>
						    <option value="yes">Yes</option>
						    <option value="no">No</option>
						</select>

						<span class="text-danger error" id="binding_err" style="display:none;">Please select Binding</span>
					</div>
				</div>
				<div class="form-group row" id="sidesDiv" style="display: none;">
					<label class="col-form-label col-md-4">Printing Sides *</label>
					<div class="col-md-8">
						<select name="sides" id="sides" class="form-control">
						    <option value="">Please select</option>
						    <option value="single">One Side</option>
						    <option value="double">Two Side</option>
						</select>

						<span class="text-danger error" id="sides_err" style="display:none;">Please select Printing Sides</span>
					</div>
				</div>
				<div class="form-group row" id="widthDiv" style="display: none;">
					<label class="col-form-label col-md-4">Width *</label>
					<div class="col-md-8">
						
						<select name="width" id="width" class="form-control">
						    <option value="">Please select</option>
						    
						    <?php if(!empty($banner_size)){ foreach($banner_size as $value){ ?>
						    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
						    <?php } } ?>
						</select>

						<!-- <input class="form-control" type="text" name="width" id="width"> -->
						<span class="text-danger error" id="width_err" style="display:none;">Please Enter Banner Width</span>
					</div>
				</div>
				<div class="form-group row" id="extra_copyDiv" style="display: none;" >
					<label class="col-form-label col-md-4">Copies From *</label>
					<div class="col-md-8">
						<input class="form-control only_whole_number" type="text" name="flyer_copy_from" id="flyer_copy_from" >
						<span class="text-danger error" id="flyer_copy_from_err" style="display:none;">Please Select from Copies</span>
					</div>
				</div>
				
				<div class="form-group row" id="extra_copyTo" style="display: none;" >
					<label class="col-form-label col-md-4">Copies To *</label>
					<div class="col-md-8">
						<input class="form-control only_whole_number" type="text" name="flyer_copy_to" id="flyer_copy_to" >
						<span class="text-danger error" id="flyer_copy_to_err" style="display:none;">Please Select to Copies</span>
					</div>
				</div>
				<!-- <div class="form-group row">
					<label class="col-form-label col-md-4">Average Prize *</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="avg_price" id="avg_price">
						<span class="text-danger error" id="avgprice_err" style="display:none;">Please Enter Valid Average Price</span>
					</div>
				</div> -->
				<div class="form-group row">
					<label class="col-form-label col-md-4"></label>
					<div class="col-md-8">
						<span class="text-danger error" id="common_err" style="display:none;"></span>
					</div>
				</div>
		    </div>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-success float-right">Add</button>
		  </div>
        </form>
		</div>
	  </div>
	</div>
  <!-- /.modal -->
  
  
   
  <!-- Modal -->
  <div class="modal center-modal fade" id="edit_pm_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">

		</div>
	  </div>
	</div>
  <!-- /.modal -->
 