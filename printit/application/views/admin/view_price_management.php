  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
<section class="content">

		  <div class="row">

			<div class="col-12">
			  <div class="box">
				  
				<div class="box-header">
					<h4 class="box-title">Price Management</h4>  
					<?php //echo "<pre>"; print_r($detail); 
					$detail[0]['order_type'] ?>
				</div>
				<div class="box-body">
				    <form id="add_membership" action="<?php echo site_url('Admin/add_membership'); ?>">
					<div class="form-group row">
						<label class="col-form-label col-md-2">Order Type</label>
						<div class="col-md-10">
							<?php if($detail[0]['order_type']=='1'){
								echo "Custom Print";
							}elseif($detail[0]['order_type']=='2'){
								echo "Quick Print";
							}elseif($detail[0]['order_type']=='3'){
								echo "Translation";
							}elseif($detail[0]['order_type']=='4'){
								echo "Notes";
							}
							?>
						</div>
					</div>
					<?php if($detail[0]['order_type']=='1'){ ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Print Type</label>
						<div class="col-md-10">
							<?php echo $detail[0]['print_type'] ;	?>
						</div>
					</div>
					<?php } ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Paper Type</label>
						<div class="col-md-10">
							<?php echo $detail[0]['paper_type'] ;	?>
						</div>
					</div>
					<?php if($detail[0]['order_type']=='1' && !empty($detail[0]['print_type']) && strtolower($detail[0]['print_type'])!='banner'){ ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Paper Size</label>
						<div class="col-md-10">
							<?php echo $detail[0]['size'] ;	?>
						</div>
					</div>
					<?php } 

					if($detail[0]['order_type']=='2'){ ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Color</label>
						<div class="col-md-10">
							<?php echo $detail[0]['color'] ;	?>
						</div>
					</div>
					<?php } ?>
					<?php if($detail[0]['order_type']=='1' && $detail[0]['print_type']=='banner'){ ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Binding</label>
						<div class="col-md-10">
							<?php echo $detail[0]['binding'] ;	?>
						</div>
					</div>
					<?php } 
					if($detail[0]['order_type']=='1' && !empty($detail[0]['print_type']) && $detail[0]['print_type']=='banner'){ ?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Width</label>
						<div class="col-md-10">
							<?php echo $detail[0]['width'] ;	?>
						</div>
					</div>
					<?php } 
					if($detail[0]['order_type']=='1' && !empty($detail[0]['print_type']) && strtolower($detail[0]['print_type'])=='flyer'){
					?>
					<div class="form-group row">
						<label class="col-form-label col-md-2">Printing Sides</label>
						<div class="col-md-10">
							<?php echo $detail[0]['binding'] ;	?>
						</div>
					</div>
					<?php }  ?> 	 	
					<div class="form-group row">
						<label class="col-form-label col-md-2">Average Price</label>
						<div class="col-md-10">
							<?php echo $detail[0]['average_price'] ;	?>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label col-md-2">Average Price</label>					<?php if(!empty($extra_price)){ foreach($extra_price as $pric){ ?>
						
							<?php echo $detail[0]['average_price'] ;	?>
						
					<?php } } ?><div class="col-md-10">
						</div>
					</div>

					
					<!-- <div class="form-group row">
						<label class="col-form-label col-md-2">Amount</label>
						<div class="col-md-10">
							<input class="form-control" type="text" name="amount" id="amount">
							<span class="form-text error text-danger" id="amount_err" style="display:none;">Please Enter Amount</span>
						</div>
					</div>
					 -->
					
					
					<div class="form-group row">
						<a href="<?php echo site_url('membership'); ?>" class="btn btn-warning float-right">Cancel</a>
						<button type="submit" class="btn btn-success float-right">Add</button>
					</div>
			        </form>
				<!-- /.box-header -->
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- ./col -->
		  </div>
		  <!-- /.row -->
		</section>
		</div>
		</div>