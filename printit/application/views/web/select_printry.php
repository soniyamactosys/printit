
            
            <div class="main_content">
            <div id="print_option">
                            <ul>
                             <li id="start"><span><svg xmlns="http://www.w3.org/2000/svg" width="91" height="90" viewBox="0 0 91 90">
    <g id="prefix__Empty_printirie" data-name="Empty printirie" transform="translate(.328 -.425)">
        <ellipse id="prefix__Ellipse_51" cx="45.5" cy="45" data-name="Ellipse 51" rx="45.5" ry="45" transform="translate(-.328 .425)" style="fill:#fff"/>
        <g id="prefix__Printt_Houses_Icon" data-name="Printt Houses Icon" transform="translate(21.013 20.288)">
            <path id="prefix__Rectangle_97" d="M0 0H49.684V49.684H0z" data-name="Rectangle 97" style="opacity:0;fill:#001b29"/>
            <path id="prefix__Path_4013" d="M10.991 5H6.5A1.5 1.5 0 0 0 5 6.5v3A1.5 1.5 0 0 0 6.5 11h3A1.5 1.5 0 0 0 11 9.5zM6.51 13.988V28.92h2.982v-7.44a3 3 0 0 1 3-3h3a3 3 0 0 1 3 3v7.44h9.01V13.988h-3.025a4.478 4.478 0 0 1-3-1.145 4.477 4.477 0 0 1-3 1.145h-3a4.478 4.478 0 0 1-3-1.145 4.477 4.477 0 0 1-3 1.145zm-3-1.13A4.487 4.487 0 0 1 2 9.492v-3A4.5 4.5 0 0 1 6.5 2h20.974a4.5 4.5 0 0 1 4.5 4.5v3a4.482 4.482 0 0 1-1.479 3.334V28.92a3 3 0 0 1-3 3h-11.45a3.043 3.043 0 0 1-.559.052h-3a3.043 3.043 0 0 1-.559-.052H6.512a3 3 0 0 1-3-3V12.858zm11.976 16.063V21.48h-3v7.44h3zM19.981 5h-5.993v4.5a1.5 1.5 0 0 0 1.5 1.5h3a1.5 1.5 0 0 0 1.5-1.5zm3 0v4.5a1.5 1.5 0 0 0 1.5 1.5h3a1.5 1.5 0 0 0 1.5-1.5v-3a1.5 1.5 0 0 0-1.5-1.5zm0 13.486h1.5a1.5 1.5 0 0 1 1.5 1.5v4.5a1.5 1.5 0 0 1-1.5 1.5h-1.5a1.5 1.5 0 0 1-1.5-1.5v-4.5a1.5 1.5 0 0 1 1.497-1.503z" data-name="Path 4013" transform="translate(7.81 7.079)" style="fill:#001b29"/>
        </g>
    </g>
</svg></span></li>
                             <li></li>
                             <li id="custom">
                                <div class="line"></div>
                                <h4><span>Custom <br />Print</span></h4> 
                                <div class="img_box"><img src="<?php echo base_url('webassets/images/2.png'); ?>"> </div>
                                <a href="<?php echo site_url('customprint'); ?>"><h5>Custom <span>Print</span></h5></a>
                             </li>
                             <li></li>
                             <li id="quick">
                                <div class="line"></div>
                                <h4><span>Quick  <br />Print</span></h4> 
                                <div class="img_box"><img src="<?php echo base_url('webassets/images/3.png'); ?>"> </div>
                                 <a href="<?php echo site_url('quickprint'); ?>"><h5>Quick <span>Print</span></h5></a></li>
                             <li></li>
                             <li id="translation"><div class="line"></div><h4><span>Translation</span></h4>
                              <div class="img_box"><img src="<?php echo base_url('webassets/images/4.png'); ?>"> </div>
                              <a href="translation.html"><h5>Translation</h5></a></li>
                             <li></li>
                             <li id="notes"><div class="line"></div><h4><span>Notes</span></h4> 
                             <div class="img_box"><img src="<?php echo base_url('webassets/images/5.png'); ?>"> </div>
                              <a href="notes-selection.html"><h5>Notes</h5></a></li>
                             <li><span>Go</span></li>
                            </ul><!-- ul end here -->
                           </div>
                    </div>
                           
