<!doctype html>
<html lang="en">
   <head>
      <title>Print It</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="<?php echo base_url('webassets/css/all.min.css') ?>">
      <link rel="stylesheet" href="<?php echo base_url('webassets/css/bootstrap.min.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('webassetscss/animation.css/') ?>">
      <link rel="stylesheet" href="<?php echo base_url('webassets/css/style.css') ?>">
      <link rel="stylesheet" href="<?php echo base_url('webassets/css/jquery.mCustomScrollbar.css') ?>" />
   </head>
   <body>
      <div class="wrapper">
         <!-- Sidebar  -->
         <nav id="sidebar" class="active side-menu">
            <button type="button" id="sidebarCollapse" class="btn"> <i class="fas fa-bars"></i><i class="fas fa-times"></i></button>
            <ul>
               <li><a href="#"> Home <span><img src="<?php echo base_url('webassets/images/Home-Icon.png'); ?>"></span></a></li>
               <li><a href="#">Printeries <span><img src="<?php echo base_url('webassets/images/Printt_House_ Icon.png'); ?>"></span></a></li>
               <li><a href="#"data-toggle="modal" data-target="#right_modal">Orders <span><img src="images/order-icon.png"></span></a></li>
               <li><a href="#">Settings <span><img src="<?php echo base_url('webassets/images/Settings-icon.png'); ?>"></span></a></li>
               <li><a href="#">Cart <span><img src="<?php echo base_url('webassets/images/cart-icon.png'); ?>"></span></a></li>
            </ul>
         </nav>
         <!-- Page Content  -->
         <div id="content">
            <header>
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-lg-3 col-md-3">
                        <a href="#" class="logo"><img src="images/logo.png" width="160px"></a>
                     </div>
                     <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="head_search_bar proposal-head-bar">
                           <form>
                              <input type="search" class="form-control" placeholder="Proposals">
                              <button><i class="fa fa-home" aria-hidden="true"></i></button>
                           </form>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4">
                        <div class="account_name text-right proposal-acount">Account Name <img src="images/user.png"></div>
                     </div>
                  </div>
               </div>
            </header>
            <div class="main_content no-padding">
               <div class="proposal_products_area" id="notes_selection" >
                  <ul class="porposal_product_inner">
                     <li>
                        <ul class="pro_product_col1">
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal" >
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                       <p>Working Hours<br>8 am - 5 PM</p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <ul class="pro_product_col1" >
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li class="pro_product_box" data-toggle="modal" data-target="#right_modal">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4>1.5</h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4>Print House <br> Name</h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right bottom-align">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                       <p>Salmiyah <br>
                                          Baghdad St.
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
               <div class="product-next-btn">
                  <a href="#">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
               </div>
            </div>
            <footer>
               <ul>
                  <li><a href="#">© 2020 Printit. </a></li>
                  <li><a href="#">Contact us</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Terms & Conditions</a></li>
                  <li><a href="#">Privacy</a></li>
               </ul>
            </footer>
            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount">Account Name <img src="images/user.png"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>Translation  <span><img src="images/Translation.png"></span></h4>
                                       <p>Project Name<br>
                                          Here
                                       </p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p>Select
                                          <br>Printerie
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="promotion-code-area">
                              <h4>Enter Promotion Code</h4>
                              <input type="text"   class="form-control" placeholder="1234567">
                              <span>10% Discount added</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url('webassets/js/popper.min.js'); ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('webassets/js/bootstrap.min.js'); ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('webassets/js/custom.js'); ?>"></script>
      <script src="<?php echo base_url('webassets/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>

   </body>
</html>