<?php
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
$doc_org_name = $order['doc_org_name'];
}else{
$doc_org_name = ''  ;
}

//echo "<pre>"; print_r($_SESSION); exit;
//print_r($order); exit;
?> 
<div class="pink-modals">
  <div class="modal fade poster_popup" id="plz_login_modal_pink" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">To continue Please Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <button type="button" class="btn btn-primary" id="open_login_modal">Go To Login</button>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!--Step 1 Popup Start -->
<div class="modal fade" id="trans_typemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="staticBackdropLabel">Translation Type</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="select-area-inner">
          <select class="custom-select selectpicker " name="trans_type" id="trans_type">
            <option value="" >Select Translation Type</option>
            <?php if(!empty($trans_types)){ foreach($trans_types as $value){ ?>
            <option value="<?php echo $value['id']; ?>" <?php echo (!empty($order) && $order['trans_type_id']==$value['id'])?'selected':''; ?> ><?php echo $value['name']; ?></option>
            <?php } } ?>
            
          </select>
        </div>
        <span class="error text-danger text-center" id="trans_type_error"></span>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary trans_type_confirm" id="transTypeConfirm" >Confirm</button>
        </div>
      </div>
    </div>
</div>
<!--Step 1 Popup End -->

<!--Step 2 Popup Start -->
<div class="modal fade" id="langfrom_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="staticBackdropLabel">Language From</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="select-area-inner">
          <select class="custom-select" name="lang_from" id="lang_from">
          <option value="">Select Language</option>
          <?php if(!empty($language)){ foreach($language as $value){ ?>
          <option value="<?php echo $value['id']; ?>" <?php echo (!empty($order) && $order['lang_from_id']==$value['id'])?'selected':''; ?> ><?php echo $value['name']; ?></option>
          <?php } } ?>
          </select>
        </div>
        <span class="error text-danger text-center" id="langFrom_error"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="langfrom_confirm">Confirm</button>
      </div>
    </div>
  </div>
</div>
<!--Step 2 Popup End  -->

<!--Step 2 Popup Start -->
<div class="modal fade" id="langto_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="staticBackdropLabel">Language To</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="select-area-inner">
          <select class="custom-select" name="lang_to" id="lang_to">
          <option value="">Select Language</option>
          <?php if(!empty($language)){ foreach($language as $value){ ?>
          <option value="<?php echo $value['id']; ?>" <?php echo (!empty($order) && $order['lang_to_id']==$value['id'])?'selected':''; ?> ><?php echo $value['name']; ?></option>
          <?php } } ?>
          </select>
        </div>
        <span class="error text-danger text-center" id="langTo_error"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="langTo_confirm">Confirm</button>
      </div>
    </div>
  </div>
</div>
<!--Step 2 Popup End  -->

<div class="modal right fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content uploadarea">
      <?php echo form_open_multipart('user/upload_doc',array('id'=>'docuploadform')); ?>
      <!-- <input type="hidden" name="image_rotation" id="image_rotation"> -->
      <div class="modal-header">
       <div class="fileupload_name"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>  Design   
        <span id="uploadTransDocBtn" style="display:<?php echo empty($order)?'block':'none'; ?>" ><i class="fas fa-check"></i></span> 
        </div>
       
      </div>
      <div class="modal-body">
        <span id="doc_err" class="text-danger error" style="display: none;">Please Upload document first</span>
        <div class="upload_box">
          <!-- <div class="photo_upload"><input type="file" id="graph_design_one" name="graph_design_one" class="input_photoupload" accept=".jpg,.jpeg,.png" ><img src="<?php //echo base_url('webassets/images/Camera.png'); ?>"></div> -->

          <div class="file_upload" id="fileDiv" style="display:<?php echo empty($order)?'':'none' ; ?>" >
            <input type="file" id="trans_doc" name="trans_doc" class="" accept=".pdf,.docx,.doc"><img src="<?php echo base_url('webassets/images/Upload.png'); ?>"> Upload</div>
          <span id="msgSpan" style="display:<?php echo empty($order)?'':'none'; ?>">Max File Size <br> 20MB</span>
          <div class="file_remove" id="removeDiv" style="display:<?php echo empty($order)?'none':'block'; ?>"><span class="fa fa-remove"></span> Remove</div>
          <span id="filenameSpan" style="display:<?php echo empty($order)?'none':'block'; ?>"><?php echo empty($order)?'':$doc_org_name; ?></span>
        </div>
        <div class="design_upload" id="uploadImagePreview" style="display:<?php echo empty($order)?'':'none' ; ?>" >
          <input type="file" class="input_designupload">
          <h3>Upload <br />Your Design</h3>
          <img class="plusicon" src="<?php echo base_url('webassets/images/Cover.png'); ?>">
        </div><?php /*
        <div class="design_upload" id="imagePreview" style="display:<?php echo empty($order)?'none':'block'; ?>;" >
          if(!empty($order)){ ?>
            <img src="<?php echo base_url('order_uploads/').$order['image_uploaded']; ?>">
          <?php }
        </div>*/ ?>
        <!-- <div class="upload_setting">
          <ul>
            <li class="active" id="verti"><img src="<?php echo base_url('webassets/images/Ver.png'); ?>"> Vertical</li>
             <li id="hori"><img src="<?php echo base_url('webassets/images/Hor.png'); ?>"> Horizontal </li>
              <li><img src="<?php echo base_url('webassets/images/Move.png'); ?>"> Move</li>
               <li><img src="<?php echo base_url('webassets/images/Scale.png'); ?>"> Scale</li>
          </ul>
        </div> -->

      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<div class="modal fade " id="projectname_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Save this project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="project-name-area">
         <input type="text" name="project_name" id="project_name" class="form-control" placeholder="Project Name" value="<?php echo empty($order)?'':$order['project_name']; ?>" />
         <span class="text-danger error text-center" id="projectname_err" style="display: none;">Please enter project name</span>
         </div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="button" class="btn btn-primary" id="continue_translation">Save & Continue</button>
        <button type="button" class="btn btn-primary" id="save_translation">Save</button>
      </div>
    </div>
  </div>
</div> 



<div class="modal fade poster_popup" id="translation_alert_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Sorry!!</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">This service is available only for express orders </div>
      <!-- <div class="modal-footer">
      </div> -->
    </div>
  </div>
</div>


</div>

<style type="text/css">
.file_remove{  
  display: inline-block;
  border: solid 1px #F44336;
  background: rgba(41, 149, 204, 0.3);
  position: relative;
  border-radius: 60px;
  width: 113px;
  color: #F44336;
  height: 50px;
  text-align: center;
  line-height: 46px;
  cursor: pointer;
  font-size: 14px;
  padding-right: 7px;
  margin: 0 10px;
  float: left;
}
</style>
