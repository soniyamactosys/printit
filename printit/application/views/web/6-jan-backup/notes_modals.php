<div class="white-modals">
<div class="modal fade poster_popup" id="projectname_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Save this project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="project-name-area">
         <input type="text" name="project_name" id="project_name" class="form-control" placeholder="Project Name" value="<?php echo empty($order)?'':$order['project_name']; ?>" />
         <span class="text-danger error text-center" id="projectname_err" style="display: none;">Please enter project name</span>
         </div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="button" class="btn btn-primary" data-title="" id="continue_note_order">Save & Continue</button>
        <button type="button" class="btn btn-primary" id="saveNote">Save</button>
      </div>
    </div>
  </div>
</div> 

<div class="modal fade poster_popup" id="page_range_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Select Pages</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('user/set_notes_page_range',array('id'=>'set_notes_page_range')); ?>
      <div class="modal-body">
        <div class="project-name-area">
            <div class="custom-control custom-radio">
                <input type="radio" name="all_pages" class="pages_radio custom-control-input" id="all_pages" value="all">
        <label class="custom-control-label" for="all_pages">All Pages</label>
      </div>
      <div class="custom-control custom-radio">
                <input type="radio" name="all_pages" class="pages_radio custom-control-input" id="page_range" value="range">
        <label class="custom-control-label" for="page_range"> Page Range</label>
      </div>
            
            
         
          <span id="note_page_error" style="display: none;" class="text-danger error"></span>      

          <div id="rangeDiv" style="display: none;">
            <label> From</label>
            <input type="number" name="page_from" id="page_from" class="form-control pagerange" placeholder="From" value="<?php echo empty($order['page_from'])?'':$order['page_from']; ?>" />
            
            <label> To</label>         
            <input type="number" name="page_to" id="page_to" class="form-control pagerange" placeholder="To" value="<?php echo empty($order['page_to'])?'':$order['page_to']; ?>" />
            <span class="text-danger error text-center" id="pageto_err" style="display: none;">Please select page to</span>
            <span class="text-danger error text-center" id="pagefrom_err" style="display: none;">Please select page from</span>
          </div>

        </div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="submit" class="btn btn-primary">Confirm</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> 

<div class="modal fade poster_popup" id="plz_login_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">To continue Please Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-primary" id="open_login_modal">Go To Login</button>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
</div>