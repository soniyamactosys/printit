<?php
$data['papertype'] = $papertype;
$data['papersize'] = $papersize;
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
//echo "<pre>"; print_r($order); exit;
}
?>
<div class="main_content">
  <div id="flow_option" class="roll_up yellow-print">
    <ul>
      <li>
      <div class="custom_print_inner text-center">

        <img src="<?php echo base_url('webassets/images/roll_upmain.png'); ?>">
        <div class="btn_outer"></div>

      </div>
      </li>   
      <li class="process_flow"><div class="line"></div><h4><span>Paper Type</span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#papertypemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up1.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5>Paper Type</h5></a>
<h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="ptype" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['papertype'])?'':$order['papertype']; ?></h6>
        
      </li>


      <li class="process_flow"><div class="line"></div><h4><span>Paper Size</span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#papersizemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up2.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5>Paper Size</h5></a>
<h6 class="paper_type <?php echo empty($order)?'psize':'psize1';  ?>" id="psize" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order)?'':$order['papersize']; ?></h6>
      </li>

    <li class="process_flow"><div class="line"></div><h4><span>Color</span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#colormodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up3.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5>Color</h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pcolor':'pcolor1';  ?>" id="pcolor" style="display:<?php echo empty($order)?'none':'block';  ?>">
        <?php echo empty($order['color'])?'':ucfirst(str_replace("_"," ",$order['color'])); ?>
      </h6>
    </li>

      
      <li class="process_flow"><div class="line"></div><h4><span>Document</span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#imagemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up4.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5>Document</h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pdesign':'pdesign1';  ?>" id="pdesign" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['img_original_name'])?'':$order['img_original_name']; ?></h6>
      </li>


      <li class="process_flow"><div class="line"></div><h4><span>Binding</span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#bindingmodal'; } ?>">
        <img src="<?php echo base_url('webassets/images/roll_up5.png'); ?>"> 
      </div>
      <a href="javascript:void(0)"><h5>Binding</h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pbinding':'pbinding';  ?> " id="pbinding" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['binding'])?'':$order['binding']; ?></h6>

      </li>


      <li class="process_flow" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#copy_number_modal'; } ?>"><div class="line"></div><h4><span>number of <br />copies</span></h4>
      <div class="img_box"><img src="<?php echo base_url('webassets/images/roll_up6.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5>number of <br />copies</h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pcopy':'pcopy1';  ?> " id="pcopy" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['no_of_copies'])?'':$order['no_of_copies']; ?></h6>

      </li>


      <li data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#projectname_modal'; } ?>"><span>Print <br />It</span></li>
    </ul><!-- ul end here -->
    
  </div>

</div>
<?php $this->load->view('web/quick_print_modals',$data); ?>               
