<div class="main_content">
   <div class="order-page-area">
  <?php 
    // echo "<pre>"; print_r($saved_orders);
    // echo "<pre>"; print_r($confirmed_orders);
    // exit;
    if(!empty($saved_orders)){ ?>
        <div class="save-order-area">
           <h5>Saved Orders</h5>
           <ul>
            <?php 
            foreach($saved_orders as $key=>$value){ 
              $mod = $key%6;
              $colorclass = 'save-order-box';
              if($value['order_type']=='1'){
                $colorclass= 'save-order-box-2';
                $txt = "Custom Print";
                $icon = '<i class="fa fa-print" aria-hidden="true"></i>';
              }elseif($value['order_type']=='2'){
                $colorclass= 'save-order-box-3';
                $txt = "Quick Print";
                $icon = '<img src="'.base_url('webassets/images/clock-icon.png').'">';

              }elseif($value['order_type']=='3'){
                $colorclass= 'save-order-box-4';
                $txt = "Translation";
                $icon = '<img src="'.base_url('webassets/images/order_img.png').'">';

              }elseif($value['order_type']=='4'){
                $colorclass= 'note-save-box';
                $txt = "Notes";
                $icon = '<img src="'.base_url('webassets/images/note-icon.png').'">';
              }
              
              ?>
              <li>

                   <div class="save-order-box <?php echo $colorclass; ?>">
                       <div class="save-order-heading">
                           <h4><?php echo $txt; ?></h4>
                           <?php echo $icon; ?>
                       </div>
                       <div class="save-order-content">
                           <h6><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></h6>
                           <p><?php echo $value['project_name'];  ?></p>
                          <a href="javascript:void(0)" oid="<?php echo $value['order_id']; ?>" class="savedOrderClick"><button><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
                       </div>
                   </div>
                </li>
              
            <?php 
            if($mod==5){ ?></ul><ul><?php  }
          }  ?>
           </ul>
        </div>
<?php }  ?>

         <div class="save-order-area order-history-area note-order-box">
           <h5>Orders History</h5>
           <ul>
            <?php if(!empty($confirmed_orders)){ foreach($confirmed_orders as $key=>$value){ 
            $mod = $key%6;
            ?>
              
              
              <li><a href="<?php echo site_url('order_detail').'/'.encoding($value['order_id']); ?>">
                 <div class="save-order-box">
                     <div class="save-order-heading">
                         <h4>#<?php echo $value['order_id']; ?></h4>
                         
                     </div>
                     <div class="save-order-content">
                         <h6><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></h6>
                         <p><?php echo $value['project_name'];  ?></p>
                        <h5><?php echo $value['amount'].' KD'; ?></h5>
                     </div>
                 </div></a>
              </li>

            <?php  
            if($mod==5){ ?></ul><ul><?php  }
          } } ?>
           </ul>
        </div>
   </div>



</div>
            