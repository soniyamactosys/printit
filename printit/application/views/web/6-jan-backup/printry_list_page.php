<?php
$userdata = $this->session->userdata('userdata');
$currentorder = $this->session->userdata('current_order');
$oid = $this->session->userdata('continue_order');
$selected_printry = $this->session->userdata('selected_printry');

$prev_link = get_order_prev_step($oid,current_url());
//echo "<pre>"; print_r($list);
//exit; ?>
 <input type="hidden" name="selectedprintry" id="selectedprintry" value="<?php if(!empty($selected_printry)){ echo $selected_printry['id']; } ?>">

            <div class="main_content no-padding">
               <div class="proposal_products_area" id="notes_selection" >
                  <ul class="porposal_product_inner">
                     <li>
                        <ul class="pro_product_col1">
                           <?php 
                           if(!empty($list)){ foreach($list as $key=>$value){ ?>
                           <li class="pro_product_box <?php if(!empty($selected_printry) && $selected_printry['id']==$value['branch_id']){ echo 'active'; } ?>" printryid="<?php echo $value['branch_id']; ?>" vendor_charge="<?php echo $value['vendor_charge']; ?>">
                              <div class="row">
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4><?php echo $value['vendor_charge']; ?></h4>
                                       </div>
                                       <div class="right_side_sec">
                                          <h4><?php echo $value['shop_name']; ?></h4>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_right">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                                            <p><?php 
                                       $fullAddress = '';
                                     // if(!empty($value['po_box'])){
                                     //   $fullAddress .= $value['po_box'].'<br>';
                                     // } 

                                     // if(!empty($value['block'])){
                                     //   $fullAddress .= $value['block'].'<br>';
                                     // }

                                     if(!empty($value['address'])){
                                       $fullAddress .= $value['address'].'<br>';
                                     }
                                       echo $fullAddress;

                                        ?></p>
                                       <p>Working Hours<br>
                                       <?php 
                                       $workingHour = '';
                                       if(!empty($value['start_time'])){
                                         $workingHour .= date('h A',strtotime($value['start_time']));
                                       }

                                       if(!empty($value['end_time'])){
                                         $workingHour .= " - ".date('h A',strtotime($value['end_time']));
                                       }
                                       echo $workingHour;?>
                                       </p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <?php 
                           if($key%5==4){ ?></ul><li><ul class="pro_product_col1" >
                              </li><?php }
                        } } ?>
                        </ul>
                     </li>
                  </ul>
               </div>
               <span id="printry_error" class="text-danger text-center error" style="display: none;">Please Select Printry </span>
               <div class="product-next-btn">
                  <?php /*if(!empty($selected_printry)){ 
                     if($order_type==1){
                        $link = site_url('pickup_delivery');
                     }else{
                        $link = site_url('payment');
                     }
                     ?>
                     <a href="javascript:void(0)" action="<?php echo $link; ?>" id="orderstep1">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                  <?php }else{ ?>
                     <a href="javascript:void(0)" id="orderstep1">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                  <?php } */ ?>

                     <?php 
                     if(!empty($order_type)){ 
                        if($order_type==1 || $order_type==4){
                           $link = site_url('pickup_delivery');
                        }else{
                           $link = site_url('payment');
                        } 
                     }else{
                        $link = '';
                     }
                     ?>
                     <a class="print-prev-btn" href="<?php echo $prev_link; ?>"><span><i class="fa fa-arrow-left" aria-hidden="true"></i></span> PREV </a>
                   <a href="javascript:void(0)" class="orderstep1" action="<?php echo $link; ?>">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                  
               </div>
            </div>

            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4><?php if($currentorder['order_type']==1){
                                          echo "Custom Print";
                                          $img = 'Custom-print.png';
                                       }elseif($currentorder['order_type']==2){
                                          echo "Quick Print";
                                          $img = '3.png';
                                       }elseif($currentorder['order_type']==3){
                                          echo "Translation";
                                          $img = '4.png';
                                       }elseif($currentorder['order_type']==4){
                                          echo "Notes";
                                          $img = '5.png';
                                       }else{
                                          $img = 'Translation.png';
                                       }
                                       ?> <span><img src="<?php echo base_url('webassets/images/').$img; ?>"></span></h4>
                                       <p><?php echo $currentorder['project_name']; ?></p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p id="printery_charge"><?php if(!empty($selected_printry)){ echo $selected_printry['vendor_charge']; } ?></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="product-next-btn model-next-btn">
                              <a href="javascript:void(0)" class="orderstep1" action="<?php echo $link; ?>">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                           </div>
                          <!-- <div class="promotion-code-area " id="promocodeArea">
                              <h4>Enter Promotion Code</h4>
                              <input type="text" id="promo_code_input"   class="form-control" placeholder="1234567">
                              <span id="promo_code_msg"></span>
                           </div>
                           <span id="promo_code_err" class="text-center text-danger"></span> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>


<?php if(!empty($selected_printry)){ ?>
<script type="text/javascript">

$(document).ready(function(){
  $("#right_modal").modal('show');
});

</script>
<?php } ?>  