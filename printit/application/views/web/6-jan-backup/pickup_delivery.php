<?php 
$userdata = $this->session->userdata('userdata');
$selected_printry = $this->session->userdata('selected_printry'); 
$currentorder = $this->session->userdata('current_order');
$vendor_charge = $this->session->userdata('vendor_charge');
$coupon_applied = $this->session->userdata('coupon_applied');
$pickup_delivery = $this->session->userdata('pickup_delivery');
$order_products = $this->session->userdata('order_products');

$oid =  $this->session->userdata('continue_order');
$prev_url = get_order_prev_step($oid,current_url());
$prod_total=0;
$total = $vendor_charge;
if(!empty($order_products)){
  $prod_total = get_prodTotal($order_products);
  $total += $prod_total;
}
$min_deliver_charge = 100;

// echo $total;
// exit;
// if(!empty($coupon_applied)){
//   $total
// }

?>
<input type="hidden" name="orderamt" id="orderamt" value="<?php echo $vendor_charge; ?>">
<input type="hidden" name="pick_delivery" id="pick_delivery" value="<?php if(!empty($pickup_delivery)){ echo $pickup_delivery; } ?>">
<input type="hidden" name="delivery_min_limit" id="delivery_min_limit" value="<?php echo $min_deliver_charge;  ?>">

            <div class="main_content no-padding">
               <div class="order_detail_outer">
                  <div class="order_detail_inner">
                     <a href="javascript:void(0)" value="pickup" class="pickbtn btn <?php if(!empty($pickup_delivery) && $pickup_delivery=='pickup'){ echo 'blue-btn'; }else{ echo 'dark-btn'; } ?>">Pick up</a>
                     <a href="javascript:void(0)" value="delivery" class="pickbtn btn <?php if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){ echo 'blue-btn'; }else{ echo 'dark-btn'; } ?>">Delivery <span>KD <?php echo $min_deliver_charge; ?> Minimum order</span></a>
                     <h2><?php echo $selected_printry['shop_name']; ?></h2>
                     <h3><?php 
                      $fullAddress = '';
                    // if(!empty($selected_printry['po_box'])){
                    //   $fullAddress .= $selected_printry['po_box'].'<br>';
                    // } 

                    // if(!empty($selected_printry['block'])){
                    //   $fullAddress .= $selected_printry['block'].'<br>';
                    // }

                    if(!empty($selected_printry['address'])){
                      $fullAddress .= $selected_printry['address'].'<br>';
                    }
                     echo $fullAddress;
                     ?></h3>
                     <?php /*
                     <img src="<?php echo base_url('webassets/images/map.jpg'); ?>">
                     */
 echo '<div class="mapouter"><div class="gmap_canvas"><iframe src = "https://maps.google.com/maps?q='.$selected_printry['latitude'].','.$selected_printry['longitude'].'&hl=es;z=14&amp;output=embed"></iframe></div></div>';
                      ?>

                  </div>

               </div>
                <span id="pick_error" class="text-danger text-center" style="display: none;">Please Select Pick up Or Delivery option </span>
               <div class="product-next-btn">
                <?php 
                $nextlink = "javascript:void(0)";
                if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){

                  if(empty($order_products)){
                    $nextlink = site_url("payment");
                  }else{
                    $nextlink = "javascript:void(0)";
                  }
                  
                }elseif(!empty($pickup_delivery) && $pickup_delivery=='pickup'){
                  $nextlink = site_url("payment");
                }

                 ?>
                  
<a class="print-prev-btn pic-prev-btn" href="<?php echo $prev_url; ?>" id=""><span><i class="fa fa-arrow-left" aria-hidden="true"></i></span> PREV</a>


                  <a href="<?php echo $nextlink; ?>" class="orderstep2">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>

               
  <div class="pro-select-box">
    <?php if(!empty($order_products)){ ?>
  <a href="javascript:void(0);" data-toggle="modal" data-target="#more_product" class="">View selected products </a>
<?php } ?>
  <?php if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){ ?>
  <a href="javascript:void(0);" data-toggle="modal" data-target="#address1"  class="">view selected address </a>            
  <?php } ?>
  </div>
</div>
</div>
            

      <div class="modal right fade model-hide pickup_model" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount"><?php echo $userdata['name']; ?><img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li class="active-tl"></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>
                                      <?php if($currentorder['order_type']==1){
                                          echo "Custom Print";
                                          $img = 'Custom-print.png';
                                       }elseif($currentorder['order_type']==2){
                                          echo "Quick Print";
                                          $img = '3.png';
                                       }elseif($currentorder['order_type']==3){
                                          echo "Translation";
                                          $img = '4.png';
                                       }elseif($currentorder['order_type']==4){
                                          echo "Notes";
                                          $img = '5.png';
                                       }else{
                                          $img = 'Translation.png';
                                       }
                                       ?> 
                                        <span><img src="<?php echo base_url('webassets/images/').$img; ?>"></span></h4>
                                       <p><?php echo $currentorder['project_name']; ?></p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p><?php echo $vendor_charge; ?></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="order_detail_list">
                              <h2>order detail</h2>
                              <table class="table">
                                 <tr><th>
                                   
                                  <?php 
                                    if($currentorder['order_type']==1){
                                    echo "Custom Print";
                                    }elseif($currentorder['order_type']==2){
                                    echo "Quick Print";
                                    }elseif($currentorder['order_type']==3){
                                    echo "Translation";
                                    }elseif($currentorder['order_type']==4){
                                    echo "Notes";
                                    }
                                  ?>
                                 </th>
                                 <td id="vendorTotal" amt="<?php echo $vendor_charge; ?>"><?php echo $vendor_charge; ?> KD</td>
                                  </tr>

                                  <tr id="delivery_fee_head" style="display: none;"><th>Delivery Fees</th><td id="delivery_fee_val"></td></tr>
                                  <?php if(!empty($extra_product)){ ?>
                                  <tr><th>Pin</th><td>1KD</td></tr>
                                  <?php } ?>
                                   <tr id="couponDiv" style="display: none;"><th>Coupon Applied</th><td id="couponVal"></td></tr>
                                 <tfoot>
                                    <tr><th>Total</th><td id="totalAmtDiv"><?php echo $vendor_charge; ?> KD</td></tr>
                                 </tfoot>
                              </table>
                           </div>
                           <div class="promotion-code-area" id="promocodeArea">
                              <h4>Enter Promotion Code</h4>
                              <input type="text" id="promo_code_input"   class="form-control" placeholder="1234567">
                              <span id="promo_code_msg"></span>
                              <span class="promo_apply_btn">Apply</span>
                           </div>
                           <span id="promo_code_err" class="text-center text-danger"></span>
                           <div class="product-next-btn model-next-btn">
                               <a href="<?php echo $nextlink; ?>" class="orderstep2">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

<div class="modal bottom fade animate" id="more_product" tabindex="-1" role="dialog" aria-labelledby="more_product">
  <div class="modal-dialog" role="document">
  <div class="modal-content animate-bottom">
  <div class="modal-header">
  <?php if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){ ?>
    <h5 class="modal-title">Minimum <?php echo $min_deliver_charge; ?> KD for delivery </h5>
  <?php } ?>
  <h2>Order Amount <span id="total_inmodal"><?php echo $vendor_charge; ?> KD</span></h2>
  <h2>Product Total <span id="prod_total" val="<?php echo $prod_total; ?>"><?php echo $prod_total; ?> KD</span></h2>

  </div>
  <div class="modal-body">
    <?php echo form_open('user/save_products',array('id'=>'save_order_products')); ?>
    <h3>Add More Products</h3>
    <div id="more_product_list" class="content">
     <ul class="moreproduct_ul">
      <?php
       if(!empty($products)){ foreach($products as $value){ 
          $key= $value['id'];
          if(!empty($order_products) && array_key_exists($key,$order_products)){
            //$sess_prod = $order_products[$key];
            $active = 'active';
            $qty = $order_products[$key]['qty'];
          }else{
            $active = '';
            $qty = 0;
          }
        ?>
        <li class="<?php echo $active; ?>" >
           <div class="more_product_box">
              <input type="hidden" name="name[<?php echo $key; ?>]" value="<?php echo $value['name']; ?>" >
              <input type="hidden" name="price[<?php echo $key; ?>]" value="<?php echo $value['price']; ?>" >
              <input type="checkbox" class="product prod_checkbox" name="products[<?php echo $key; ?>]" value="<?php echo $value['id'];  ?>" price="<?php echo $value['price'];  ?>" <?php if(!empty($active)){ echo 'checked'; } ?>>

        <!--       <input type="text" name="qty[<?php echo $key; ?>]" value="<?php echo $qty; ?>" > -->


              <div class="more_product_img more-product-area"><img src="<?php echo base_url('products'
                ).'/'.$value['image'];?>"></div>
              <div class="more_product_content more-content-area">
                <div class="quant-btn">
                  <div class="number">
                      <span style="font-size: smaller;" class="qtyerror text-danger"></span>
                      <span class="prodMinus">-</span>
                      <input type="text" class="qty" name="qty[<?php echo $key; ?>]" value="<?php echo $qty; ?>" >
                      <span class="plusProd">+</span>
                  </div>
                </div>
                <div class="prodContent" >
                  <h4><?php echo $value['name']; ?></h4><h6><?php echo $value['price']; ?> KD</h6>
                </div>
            </div>
           </div>
        </li>
      <?php } } ?>
     </ul>

     </div>
      <div class="next-btn text-center">
        <span id="prod_err" class="text-center text-danger" style="display: none; padding-top: 15px;"></span>
      
        <input type="submit" name="submit" value="Next" class="nest-button">
      </div>
      <?php echo form_close(); ?>
  </div>

  </div>
  </div>
</div>


 <div class="modal right fade model-hide" data-backdrop="static" data-keyboard="false" id="address2" tabindex="-1" role="dialog" aria-labelledby="address2">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                     <button type="button" class="close" id="cancelNewAddress" >
                      <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
                    </button> 
                       <h3>Add New Address</h3>
                     </div>
                      <div class="modal-body">
                         <div class="form-group">
                           <span id="addorder_error" class="error text-center text-danger" ></span>
                          </div>
                        <?php echo form_open('user/add_new_address',array('id'=>'new_order_address')); ?>
                       
                           <div class="form-group">
                            <input type="text" name="label" class="form-control" placeholder="Address Label">
                          </div>
                            <div class="form-group">
                            <input type="address" name="address" id="order_addrress" class="form-control" placeholder="Address">
                             <input type="hidden" name="longitude" id="order_lat">
                            <input type="hidden" name="latitude" id="order_long">
                            <span class="text-danger text-center error" id="o_add_err" style="display: none;">Please add your address</span>

                          </div>

                           <div class="form-group">
                            <input type="text" name="area" class="form-control" placeholder="Area"></div>
                           
                           <div class="form-group">
                            <input type="text" name="block" class="form-control" placeholder="block"></div>
                           <div class="form-group">
                            <input type="text" name="street" class="form-control" placeholder="Street"></div>
                           <div class="form-group">
                            <input type="text" name="avenue" class="form-control" placeholder="Avenue"></div>
                           <div class="form-group">
                            <input type="text" name="building" class="form-control" placeholder="Building"></div>
                           <div class="form-group">
                            <input type="text" name="floor" class="form-control" placeholder="Floor"></div>
                           <div class="form-group">
                            <input type="text" name="apartment" class="form-control" placeholder="Apartment No."></div>
                           <div class="form-group">
                            <input type="text" name="add_mobile" class="form-control" placeholder="Mobile Number"></div>
                           <div class="form-group">
                            <textarea  name="extra_instruction" class="form-control" placeholder="Extra Instructions"></textarea></div>

                        <div class="address_action">
                           <button type="submit" class="btn blue-btn">Confirm</button>
                        </div>
                        <?php echo form_close(); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal right fade model-hide"  id="address1" tabindex="-1" role="dialog" aria-labelledby="address1" data-backdrop="static" data-keyboard="false" >
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <?php echo form_open('javascript:void(0)',array('id'=>'address_option_form')); ?>
                     <div class="modal-header">
                      <?php if(!empty($currentorder['delivery_address'])){ ?>  
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
                    </button> 
                       <?php } ?>
                     </div>
                      <div class="modal-body">
                        <h3>Select Delivery Address</h3>
                        <ul>
                            <?php if(!empty($my_address_options)){ foreach($my_address_options as $value){  $active = empty($currentorder['delivery_address'])?'':($value['id']==$currentorder['delivery_address'])?'active':''; ?>
                            <li><div class="radio_address <?php echo $active;  ?>"><label for="<?php echo 'add_radio'.$value['id']; ?>"><?php echo $value['label']; ?> <span>Salmiyah</span></label>
                            <input type="radio" class="address_option" value="<?php echo $value['id']; ?>" name="address_option" id="<?php echo 'add_radio'.$value['id']; ?>" <?php if(!empty($currentorder['delivery_address']) && $value['id']==$currentorder['delivery_address']){ echo 'checked'; } ?> ></div></li>
                            <?php } } ?>
                        </ul>
                        

                        <div class="address_action">
                          <span class="text-danger text-center" style="display: none;" id="address_option_err">Please Select address</span>
                           <a href="javascript:void(0)" class="btn dark-btn"  data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#address2">New Address</a>
                           
                          
                            <input type="submit" class="btn blue-btn" name="submit" value="Done">
                         
                           
                        </div>
                     </div>
                     <?php echo form_close(); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
