<?php $userdata = $this->session->userdata('userdata');
$vendordata =  $this->session->userdata('vendordata');
$headsearchdisplay = 'block';
$placeholder = "";

$oid =  $this->session->userdata('continue_order');
$prev_url = get_order_prev_step($oid,current_url());
if(strcasecmp($prev_url,current_url())==0){
$prev_url = "";
}
?>
 <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" class="active side-menu">
           <button type="button" id="sidebarCollapse" class="btn"> <i class="fas fa-bars"></i><i class="fas fa-times"></i></button>
            <ul>
				<li><a href="<?php echo site_url(); ?>"> <?php echo $this->lang->line('home');  ?> <span><img src="<?php echo base_url('webassets/images/Home-Icon.png'); ?>"></span></a></li>
				<li><a href="javascript:void(0)" data-toggle="modal" data-target="#printeries"><?php echo $this->lang->line('printeries');  ?> <span><img src="<?php echo base_url('webassets/images/Printt_House_ Icon.png'); ?>"></span></a></li>
				<?php if($userdata){ ?>
				<li><a href="<?php echo site_url('myorders'); ?>"><?php echo $this->lang->line('orders'); ?> <span><img src="<?php echo base_url('webassets/images/order-icon.png'); ?>"></span></a></li>
				<li><a href="<?php echo site_url('setting'); ?>" ><?php echo $this->lang->line('settings');  ?><span><img src="<?php echo base_url('webassets/images/Settings-icon.png'); ?>"></span></a></li>
				<li><a href="javascript:void(0)" data-toggle="modal" data-target="#cart" class="cart_side"><?php echo $this->lang->line('cart');  ?><span><img src="<?php echo base_url('webassets/images/cart-icon.png'); ?>"></span><span class="dot" id="dotnotify" style="display:none;"></span></a></li>
				<?php } ?>
				<li>
                           <div class="laung-btn">
                              <a href="<?php echo site_url("home/switchLang/english"); ?>" class="<?php echo ($this->session->userdata('site_lang')=='english')?'':'laung-btn-arbic'; ?>"><button>English</button></a>
                              <a href="<?php echo site_url("home/switchLang/arabic"); ?>"  class="<?php echo ($this->session->userdata('site_lang')=='arabic')?'':'laung-btn-arbic'; ?>"><button>عربي</button></a>
                           </div>
                        </li>
            </ul>

        </nav>



        <!-- Page Content  -->
        <div id="content">
            <header>
                <div class="container-fluid">
                    <?php 
                    $segment1 = $this->uri->segment(1);
                    if($vendordata){ ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-7 col-7">
                                <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"><span><?php echo $this->lang->line('the_easiest_way_to_print_in_kuwait');  ?></span></a>
                        </div>
                        <!--<div class="col-xl-2 col-lg-2 col-md-2"></div>-->
                        <div class="col-md-4 col-sm-5 col-5">
                                <div class="dropdown">
                                  <div class="account_name text-right dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $vendordata['username']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                                  
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('vendor'); ?>"><?php echo $this->lang->line('go_to_panel');  ?></a>
                                    <a class="dropdown-item" href="<?php echo site_url('vendor_logout'); ?>"><?php echo $this->lang->line('logout');  ?></a>
                                  </div>
                                </div>
                        </div>
                    </div>
                    <?php }elseif($userdata){ 
                    
                    if($segment1=='home' || $segment1=='select_service_type' || $segment1=='' || $segment1=='myprofile' ){ ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-7 col-7">
                                <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"><span><?php echo $this->lang->line('the_easiest_way_to_print_in_kuwait');  ?></span></a>
                        </div>
                        <!--<div class="col-xl-2 col-lg-2 col-md-2"></div>-->
                        <div class="col-md-4 col-sm-5 col-5">
                                <div class="dropdown">
                                  <div class="account_name text-right dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                                  
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('user'); ?>"><?php echo $this->lang->line('profile');  ?></a>
                                    <a class="dropdown-item" href="<?php echo site_url('userlogout'); ?>"><?php echo $this->lang->line('logout');  ?></a>
                                  </div>
                                </div>
                        </div>
                    </div>
                    <?php }else{ 
                        if($segment1=='print-type'){
                            //$placeholder = ucfirst($this->uri->segment(2)); 
                            $placeholder = $this->lang->line($this->uri->segment(2)); 
                        }elseif($segment1=='user'){
                          //$placeholder = "Custom Print";
                          $placeholder = $this->lang->line('user_profile'); 
                        }elseif($segment1=='customprint'){
                          //$placeholder = "Custom Print";
                          $placeholder = $this->lang->line('custom_print'); 
                        }elseif($segment1=='quickprint'){
                          //$placeholder = "Quick Print";
                          $placeholder = $this->lang->line('quick_print');
                        }elseif($segment1=='translation'){
                          //$placeholder = "Translation";
                          $placeholder = $this->lang->line('translation');
                        }elseif($segment1=='notes'){
                          //$placeholder = "Notes";
                          $placeholder = $this->lang->line('notes');
                        }elseif($segment1=='order_confirmed'){
                         // $placeholder = "Order Status"; 
                          $placeholder = $this->lang->line('order_status');
                          
                        }elseif($segment1=='payment'){
                          //$placeholder = "Payment"; 
                          $placeholder = $this->lang->line('payment');
                        }elseif($segment1=='pickup_delivery' || $segment1=='delivery' ){
                          //$placeholder = "Order Details"; 
                          $placeholder = $this->lang->line('order_details');
                          
                        }elseif($segment1=='printery_shops'){
                         // $placeholder = "Printery Name"; 
                          $placeholder = $this->lang->line('order_details');
                        }elseif($segment1=='continue_order'){
                          $placeholder = $this->session->userdata('header_placeholder');
                        }elseif($segment1=='order_detail'){
                          //$placeholder = "Order Status";
                          $placeholder = $this->session->userdata('order_status');
                        }elseif($segment1=='myorders'){
                          //$placeholder = "Orders";
                          $placeholder = $this->session->userdata('orders');

                        }elseif($segment1=='note_detail'){
                          //$placeholder = "Notes";
                          $placeholder = $this->lang->line('notes');
                        }else{
                          //$placeholder = "Proposals";
                          $placeholder = $this->lang->line('proposals');
                        }
                        //echo $placeholder; exit; 

                        if($segment1=='select_service_type'){
                          $headsearchdisplay = 'none';
                        }

                    ?>
                        
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"></a>
                        </div>
                        <?php if($segment1=='printery_shops' || $segment1=='pickup_delivery' || $segment1=='payment' || $segment1=='order_confirmed' || $segment1=='print-type' || $segment1=='quickprint' || $segment1=='translation'  || $segment1=='continue_order' || $segment1=='order_detail' || $segment1=='myorders' || $segment1=='note_detail'){
                         ?>
                          <div class="col-xl-5 col-lg-5 col-md-5">
                            <div class="head_search_bar proposal-head-bar">
                               
                              <?php if($segment1 !='myorders' && $segment1 !='order_confirmed' && $segment1 !='order_detail' ){ ?>
                                <button onclick="goBack(this)"  href="<?php echo $prev_url; ?>" class="backbtn"><i class="fas fa-arrow-left"></i></button>
                              <?php } ?>
                                <form method="post" action="javascript:void(0)">
                                <input type="search" class="form-control" placeholder="<?php echo $placeholder; ?>">
                                  
                                  <?php if($segment1=='pickup_delivery'){ ?>
                                  <?php }elseif($segment1=='order_detail'){  ?>
                                  <!--<button onclick="gotohome()">-->
                                  <!--  <i class="fa fa-home" aria-hidden="true"></i>-->
                                  <!--</button> -->
                                  <button type="button" onclick="openWin()" > <img src="<?php echo base_url('webassets/images/Save-Icon.png'); ?>"></button>

                                  <?php }elseif($segment1=='order_confirmed'){ ?>
                                  <button type="button" onclick="openWin()" > <img src="<?php echo base_url('webassets/images/Save-Icon.png'); ?>"></button>
                                  
                                  <?php }elseif($segment1=='myorders'){  ?>
                                    <button id="removeSaved" type="button"><img src="<?php echo base_url('webassets/images/Trash-icon.png'); ?>"></button>
                                  <?php }else{ ?>
                                  <!-- <button type="button" data-toggle="modal" data-target="#savetocart" > <img src="<?php //echo base_url('webassets/images/Save-Icon.png'); ?>"></button>-->
                                  <button type="button" data-toggle="modal" data-target="#projectname_modal" > <img src="<?php echo base_url('webassets/images/Save-Icon.png'); ?>"></button>
                                  <?php } ?>

                                  <!-- <button onclick="gotohome()">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                  </button> -->
                               </form>
                            </div>
                         </div>
                        <?php }elseif($segment1=='myorders'){ ?>
                          <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="head_search_bar proposal-head-bar">
                           <form>
                              <input type="search" class="form-control" placeholder="Orders">
                              <button><img src="<?php echo base_url('webassets/images/Trash-icon.png'); ?>"></button>
                           </form>
                        </div>
                        </div>
                        <?php }elseif($segment1=='setting'){ ?>
                        <div class="col-xl-5 col-lg-5 col-md-5">
                        </div>
                        <?php }else{ ?>
                        
                        <div class="col-xl-5 col-lg-5 col-md-5">
                            <div class="head_search_bar" style="display: <?php echo $headsearchdisplay; ?>">
                              
                                <button onclick="goBack(this)" href="<?php echo $prev_url; ?>" class="backbtn"><i class="fas fa-arrow-left"></i></button>
                                <!--<form action>-->
                                <input type="search" class="form-control" placeholder="<?php echo $placeholder; ?>">
                              <!--</form>-->
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-4 col-md-4">
                                <div class="dropdown account-drop">
                                  <div class="account_name text-right dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                                  
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('user'); ?>"><?php echo $this->lang->line('profile');  ?></a>
                                    <a class="dropdown-item" href="<?php echo site_url('userlogout'); ?>"><?php echo $this->lang->line('logout');  ?></a>
                                  </div>
                                </div>
                        </div>
                        
                    </div>
                    <?php } 
                    }else{ ?>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"><span><?php echo $this->lang->line('the_easiest_way_to_print_in_kuwait');  ?></span></a>
                        </div>
                        <div class="col-md-2 pull-right">
                            <div  id="form-site-header">

                           <div id="form-button-toggle">

                               <div class="hamburger"></div>
                               <div class="hamburger"></div>
                               <div class="hamburger"></div>
                               <!--  <img src="<?php //echo base_url('webassets/images/left-arrow.png'); ?>" class="login-arrow"> -->

                            </div>
                             <div id="form-panel">
                                       <div class="form-section-area">

                                          <div class="login-area">
                                                       <span id="login-back-btn">
                                                          <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                     </span>
                                             <div class="login-logo">
                                                <a href="#"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" class="animatable bounceIn"></a>
                                             </div>
                                             <div class="login-form">
                                                <span class="text-danger text-center text-bold error" id="login_msg" style="display:none;"></span>

                                                <form action="<?php echo site_url('home/userlogin'); ?>" method="post" id="userlogin">
                                                   <input type="email" id="login_email" name="email" placeholder="Email" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="l_email_err" style="display:none;">Please Enter Your Email Address</span>
                                                   <input type="password" placeholder="Password" name="password" id="login_pass" class="form-control animatable bounceInRight"  >
                                                   <span class="text-danger text-center text-bold error" id="l_pass_err" style="display:none;">Please Enter Password</span>
                                                   <div class="login-btn buttonBox">
                                                   
                                                        <button type="submit" class="animatable bounceIn"><?php echo $this->lang->line('login');  ?> </button><div class="border"></div><div class="border"></div><div class="border"></div><div class="border"></div>
                                                    </div>
                                                      <a href="#" class="for-password animatable bounceInLeft" id="forget-btn"><?php echo $this->lang->line('forgot_password');  ?> </a>
                                                </form>
                                               
                                                <div class="account-infor">
                                                   <p class="animatable moveUp"> <?php echo $this->lang->line('Dont_have_an_account?');  ?></p>
                                                   <a href="javascript:void(0)" id="singup-btn" class="animatable bounceIn"> <?php echo $this->lang->line('signup');  ?></a>
                                                    <a href="javascript:void(0)" class="vandor-btn animatable bounceIn" id="vendor-butn"><?php echo $this->lang->line('i_am_vendor');  ?></a>
                                                </div>

                                             </div>
                                          </div>
                                          <div class="signup-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft">  
                                                  <span id="back-btn">
                                                   <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                   </span><?php echo $this->lang->line('signup'); ?>
                                                </h4>
                                             </div>
                                             <span class="text-danger text-center text-bold error" id="common_err" style="display:none;"></span>
                                             <form class="form-signup" action="<?php echo site_url('home/user_register'); ?>" method="post" name="form" id="user_registeration">
                                             <div class="signup-form">
                                                

                                                   <input  type="email" name="email" id="semail" placeholder="Email" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="email_err" style="display:none;">Please Enter Your Email Address</span>
                                                   
                                                   <input  type="text" name="f_name" id="f_name" placeholder="First Name" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="f_name_err" style="display:none;">Please Enter Your First Name</span>

                                                   <input  type="text" name="l_name" id="l_name" placeholder="Last Name" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="l_name_err" style="display:none;">Please Enter Your Last Name</span>

                                                   <input  type="text" name="mobile" id="s_mobile" placeholder="Mobile Number" class="form-control animatable bounceInLeft" value="+966" maxlength="13" minlength="12">
                                                   <span class="text-danger text-center text-bold error" id="l_mobile_err" style="display:none;">Please Enter Your Mobile Number</span>


                                                   <input  type="address" name="address" id="u_address" placeholder="Address" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="u_address_err" style="display:none;">Please Enter Your Address</span>
                                                   <input type="hidden" name="user_lat" id="user_lat">
                                                   <input type="hidden" name="user_lng" id="user_lng">

                                                   <input  type="password" name="password" id="password" placeholder="Password" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="pass_err" style="display:none;">Please Enter Password</span>
                                                   <input  type="password" name="cpassword" id="cpassword" placeholder="Confirm password" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="cpass_err" style="display:none;">Please Enter Confirm Password</span>

                                                   <div class="genderDiv">

                                                    <h4 class="gendar_heading">Gender</h4>

                                                  <label class="man-label">
                                            <input type="radio" name="gender" class="s_gender" class="gender" value="male">
                                            <span class="man-image"></span>
                                            <!-- <img src="<?php echo base_url('webassets/images/male_signup.png') ?>"> -->
                                          </label>

                                      <label class="woman-label">
                                       <input type="radio" name="gender" class="s_gender" class="gender" value="female">
                                        <!-- <img src="<?php echo base_url('webassets/images/female_signup.png') ?>"> -->
                                        <span class="woman-image"></span>
                                      </label>

                                                     
                                        </div>
                                        <span class="text-danger text-center text-bold error" id="s_gender_err" style="display:none;">Please Select Your Gender</span>
                                             </div>
                                             <div class="sign-up-button ">
                                                 <button type="submit" class="animatable bounceIn"> <?php echo $this->lang->line('signup');  ?></button>
                                             </div>
                                             </form>
                                          </div>

                                           <div class="vander-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft">  <span id="v-back-btn">
                                                   <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                   </span><?php echo $this->lang->line('vendor');  ?>
                                                </h4>
                                             </div>
                                             <div class="signup-form">
                                                <form class="form-signup" action="<?php echo site_url('vendor_registeration') ?>" method="post" name="form" id="vendor_registeration" >
                                                  <span class="text-danger error invalidText" id="v_common_error" style="display: 
                                                  none;"></span>
                                                  
                                                   <input  type="text" name="firstname" placeholder="First Name" class="form-control animatable bounceInLeft" id="v_fname">
                                                   <span id="v_name_err" style="display: none;" class="text-danger invalidText"></span>

                                                    <input  type="text" name="shop_name" placeholder="Company Name" class="form-control animatable bounceInLeft" id="v_shop_name">
                                                   <span id="v_shopname_err" style="display: none;" class="text-danger invalidText"></span>

                                                   <input  type="email" name="email" placeholder="Email Address" class="form-control animatable bounceInRight" id="v_email">
                                                   <span id="v_email_err" style="display: none;" class="text-danger invalidText"></span>
                                       
                                                     
                                                    <input  type="text" name="address" placeholder="Address" class="form-control animatable bounceInLeft" id="v_address">
                                                    <span id="v_address_err" style="display: none;" class="text-danger invalidText"></span>
                                                     <input type="hidden" name="vendor_lat" id="vendor_lat">
                                                   <input type="hidden" name="vendor_lng" id="vendor_lng">

                                                     <input type="text" class="form-control animatable bounceInLeft" placeholder="Mobile Number" name="contact_no" id="v_contact_no"  value="+966" maxlength="13" minlength="12" >
                                                     <span id="v_contact_err" style="display: none;" class="text-danger invalidText"></span>
                                                  <input  type="password" name="password" placeholder="Password" class="form-control animatable bounceInLeft" id="v_password">

                                                  <span id="v_pass_err" style="display: none;" class="text-danger invalidText"></span>

                                                  <input  type="password" name="c_password" placeholder="Confirm Password" class="form-control animatable bounceInLeft" id="v_cpassword">

                                                  <span id="v_cpass_err" style="display: none;" class="text-danger invalidText"></span>
                                                  <div class="sign-up-button ">
                                                 <button type="submit" class="animatable bounceIn"><?php echo $this->lang->line('signup');  ?></button>
                                                  </div>

                                                  <!-- <input  type="submit" value="Sign up" class="animatable bounceIn"> -->
                                                </form>
                                             </div>
                                             <!-- <div class="sign-up-button ">
                                                <a href="#" class="animatable bounceIn">sign up</a>
                                             </div> -->
                                          </div>
                                          <div class="forget-pass-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft"> <span id="back1-btn"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><?php echo $this->lang->line('forgot_password'); ?></h4>
                                             </div>
                                             <form class="form-signup" action="<?php echo site_url('home/forgotpass'); ?>" method="post" name="form" id="forgotpassForm">
                                             <div class="signup-form forget-form">
                                                <h4 class="animatable fadeInDown">Type your Email Address</h4>
                                                   <input  type="text" name="email" id="f_email" placeholder="Email Address" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="f_email_err" style="display:none;">Please Enter Your Email Address</span>
                                             </div>
                                             <div class="sign-up-button animatable bounceIn">
                                                <button type="submit" id="f_send_btn"><?php echo $this->lang->line('send');  ?></button>
                                             </div>
                                             </form>
                                          </div>
                                          <?php /*
                                          <div class="forget-pass-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft"> <span id="back1-btn"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>forgot password</h4>
                                             </div>
                                             <div class="signup-form forget-form">
                                                <h4 class="animatable fadeInDown">Type<br>A Password</h4>
                                                <form class="form-signup" action="" method="post" name="form">
                                                   <label class="animatable fadeInUp">Password can have 8 - 16 characters </label>
                                                   <input  type="text" name="password" placeholder="********" class="form-control animatable bounceInLeft">
                                                   <input  type="text" name="confirmpassword" placeholder="Confirm password" class="form-control animatable bounceInRight">
                                                </form>
                                             </div>
                                             <div class="sign-up-button animatable bounceIn">
                                                <a href="#">send</a>
                                             </div>
                                          </div>
                                          */ ?>
                                       </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                    
                    <?php } ?>
                    
                </div>
            </header>
