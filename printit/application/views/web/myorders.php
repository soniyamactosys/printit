<div class="main_content">
   <div class="order-page-area">
  <?php 
    // echo "<pre>"; print_r($saved_orders);
    // echo "<pre>"; print_r($confirmed_orders);
    // exit;
    if(!empty($saved_orders)){ ?>
        <div class="save-order-area">
           <h5><?php echo $this->lang->line('Saved_Orders'); ?></h5>
           <ul>
            <?php 
            foreach($saved_orders as $key=>$value){ 
              $mod = $key%6;
              $colorclass = 'save-order-box';
              if($value['order_type']=='1'){
                $colorclass= 'save-order-box-2';
                $txt = $this->lang->line('custom_print');
                $icon = '<i class="fa fa-print" aria-hidden="true"></i>';
              }elseif($value['order_type']=='2'){
                $colorclass= 'save-order-box-3';
                $txt = $this->lang->line('quick_print');
                $icon = '<img src="'.base_url('webassets/images/clock-icon.png').'">';

              }elseif($value['order_type']=='3'){
                $colorclass= 'save-order-box-4';
                $txt = $this->lang->line('translation');
                $icon = '<img src="'.base_url('webassets/images/order_img.png').'">';

              }elseif($value['order_type']=='4'){
                $colorclass= 'note-save-box';
                $txt = $this->lang->line('notes');
                $icon = '<img src="'.base_url('webassets/images/note-icon.png').'">';
              }
              
              ?>
              <li>
                     <label>
                   <div class="save-order-box <?php echo $colorclass; ?>">
                       <div class="dltorder_checkbox"><input type="checkbox" name="checkbox[]" class="dltsavedorder" value="<?php echo $value['order_id']; ?>" /></div>
                       <div class="save-order-heading">
                           <h4><?php echo $txt; ?></h4>
                           
                           <?php echo $icon; ?>
                       </div>
                       <div class="save-order-content">
                           <h6><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></h6>
                           <p><?php echo $value['project_name'];  ?></p>
                          <a href="javascript:void(0)" oid="<?php echo $value['order_id']; ?>" class="savedOrderClick"><button><i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
                       </div>
                   </div>
                  </label>
                </li>
              
            <?php 
            /*if($mod==5){ ?></ul><ul><?php  }*/
          }  ?>
           </ul>
        </div>
<?php }  ?>

         <div class="save-order-area order-history-area note-order-box">
           <h5><?php echo $this->lang->line('Orders_History'); ?></h5>
           <ul>
            <?php if(!empty($confirmed_orders)){ foreach($confirmed_orders as $key=>$value){ 
            $mod = $key%6;
            ?>
              
              
              <li><a href="<?php echo site_url('order_detail').'/'.encoding($value['order_id']); ?>">
                 <div class="save-order-box">
                     <div class="save-order-heading">
                         <h4>#<?php echo $value['order_id']; ?></h4>
                         
                     </div>
                     <div class="save-order-content">
                         <h6><?php echo date('d-m-Y',strtotime($value['updated_at'])); ?></h6>
                         <p><?php echo $value['project_name'];  ?></p>
                        <h5><?php echo $value['order_total'].' KD'; ?></h5>
                     </div>
                 </div></a>
              </li>

            <?php  
            /*if($mod==5){ ?></ul><ul><?php  }*/
          } } ?>
           </ul>
        </div>
   </div>



</div>

<div class="modal fade poster_popup" id="delete_order_error" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Delete Saved Orders</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="project-name-area">Please Select Orders to delete </div>
      </div>
      
      <!--<div class="modal-footer project-m-footer">-->
      <!--  <button type="button" class="btn btn-primary" id="saveproject">Save</button>-->
      <!--</div>-->
    </div>
  </div>
</div>   

<div class="modal fade poster_popup" id="delete_order_confirm" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Delete Saved Orders</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('user/delete_saved_orders',array('id'=>'confirm_saved_delete')); ?>
      <div class="modal-body">
          <input type="hidden" id="selectedorders" name="selectedorders" />
        <div class="contentDiv">
            Are you sure do you really want to  permenantly delete selected order saved in cart ?
        </div>
        <div id="dlt_msg_div" style="display:none" class="text-center" style="padding-top:5px;font-weight:bold"></div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="submit" class="btn btn-primary">YES</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>   
