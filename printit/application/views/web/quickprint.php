<?php
$data['papertype'] = $papertype;
$data['papersize'] = $papersize;
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
//echo "<pre>"; print_r($order); exit;
}
?>
<div class="main_content quick_print_area">
  <div id="flow_option" class="roll_up yellow-print">
    <ul>
      <li>
      <div class="custom_print_inner text-center">

        <img src="<?php echo base_url('webassets/images/roll_upmain.png'); ?>">
        <div class="btn_outer"></div>

      </div>
      </li>   
      <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('paper_type'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#papertypemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up1.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('paper_type'); ?></h5></a>
<h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="ptype" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['papertype'])?'':$order['papertype']; ?></h6>
        
      </li>


      <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('paper_size'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#papersizemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up2.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('paper_size'); ?></h5></a>
<h6 class="paper_type <?php echo empty($order)?'psize':'psize1';  ?>" id="psize" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order)?'':$order['papersize']; ?></h6>
      </li>

    <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('color'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#colormodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up3.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('color'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pcolor':'pcolor1';  ?>" id="pcolor" style="display:<?php echo empty($order)?'none':'block';  ?>">
        <?php echo empty($order['color'])?'':ucfirst(str_replace("_"," ",$order['color'])); ?>
      </h6>
    </li>

      
      <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('document'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#imagemodal'; } ?>"><img src="<?php echo base_url('webassets/images/roll_up4.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('document'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pdesign':'pdesign1';  ?>" id="pdesign" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['img_original_name'])?'':$order['img_original_name']; ?></h6>
      </li>


      <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('binding'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#bindingmodal'; } ?>">
        <img src="<?php echo base_url('webassets/images/roll_up5.png'); ?>"> 
      </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('binding'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pbinding':'pbinding1';  ?> " id="pbinding" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['binding'])?'':$order['binding']; ?></h6>

      </li>


      <li class="process_flow" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#copy_number_modal'; } ?>"><div class="line"></div><h4><span><?php echo $this->lang->line('number_of_br_copies'); ?></span></h4>
      <div class="img_box"><img src="<?php echo base_url('webassets/images/roll_up6.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('number_of_br_copies'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order)?'pcopy':'pcopy1';  ?> " id="pcopy" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['no_of_copies'])?'':$order['no_of_copies']; ?></h6>

      </li>


      <li data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_yellow'; }else{ echo '#projectname_modal'; } ?>"><span><?php echo $this->lang->line('print_br_it'); ?></span></li>
    </ul><!-- ul end here -->
    
  </div>

</div>
<?php $this->load->view('web/quick_print_modals',$data); ?>               
