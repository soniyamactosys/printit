
<div id="updade_add_error" class="error text-danger text-center" style="display:none;"></div>
<form action="<?php echo site_url('user/update_address'); ?>" id="update_address" method="post">
              <input type="hidden" name="id" value="<?php echo $detail['id']; ?>" />
                        <div class="form-group">
                        <input type="text" name="label" value="<?php echo $detail['label']; ?>" class="form-control" placeholder="Address Nickname">
                        </div>

                        <div class="form-group">
                            <input id="edit_add" name="address" type="text" placeholder="Area*" class="form-control" value="<?php echo $detail['address']; ?>">
                             <input type="hidden" name="longitude" id="edit_longitude" value="<?php echo $detail['longitude']; ?>">
                            <input type="hidden" name="latitude" id="edit_latitude" value="<?php echo $detail['latitude']; ?>">
                            <span class="text-danger text-center error" id="o_add_err" style="display: none;">Please add area</span>
                          </div> 

                        <div class="form-group">
                            <div class="select">
                                <select name="address_type" class="form-control select-hidden" id="address_type">
                                    <option value="" <?php if($detail['address_type'] ==''){ echo 'selected'; } ?> >Address Type*</option>
                                    <option value="home" <?php if($detail['address_type'] =='home'){ echo 'selected'; } ?> >Home</option>
                                    <option value="apartment" <?php if($detail['address_type'] =='apartment'){ echo 'selected'; } ?> >Apartment</option>
                                    <option value="office" <?php if($detail['address_type'] =='office'){ echo 'selected'; } ?> >Office</option>
                                </select>
                                
                            </div>
                            <span id="add_type_err" class="text-danger text-center error" style="display: none;">Please select address type</span>
                        
                        </div>
                        
                           <div class="form-group">
                            <input type="text" name="block" id="block" class="form-control" placeholder="block*" value="<?php echo $detail['block']; ?>">
                            <span id="add_block_err" class="text-danger text-center error" style="display: none;">Please select block</span>
                            </div>
                            
                           <div class="form-group">
                            <input type="text" name="street" id="street" class="form-control" placeholder="Street*"  value="<?php echo $detail['street']; ?>">
                            <span id="add_street_err" class="text-danger text-center error" style="display: none;">Please enter street</span>
                            </div>
                           <div class="form-group">
                            <input type="text" name="avenue" id="avenue" class="form-control" placeholder="Avenue"  value="<?php echo $detail['avenue']; ?>">
                            </div>
                            
                            <div class="form-group extra_options" id="houseDiv" style="display:<?php echo ($detail['address_type']=='home')?'block':'none'; ?>">
                            <input type="text" name="house" id="house" class="form-control" placeholder="House*"  value="<?php echo $detail['house']; ?>">
                            <span id="add_house_err" class="text-danger text-center error" style="display: none;">Please enter house</span>
                            
                            </div>
                            
                           <div class="form-group extra_options" id="buildingDiv" style="display:<?php echo ($detail['address_type']=='apartment' || $detail['address_type']=='office')?'block':'none'; ?>">
                            <input type="text" name="building" id="building" class="form-control" placeholder="Building*"  value="<?php echo $detail['building']; ?>">
                            <span id="add_building_err" class="text-danger text-center error" style="display: none;">Please enter building</span>
                            </div>
                            
                           <div class="form-group extra_options" id="floorDiv" style="display:<?php echo ($detail['address_type']=='apartment')?'block':'none'; ?>">
                            <input type="text" name="floor" id="floor" class="form-control" placeholder="Floor*" value="<?php echo $detail['floor']; ?>">
                            <span id="add_floor_err" class="text-danger text-center error" style="display: none;">Please enter floor</span>
                            </div>
                            
                           <div class="form-group extra_options" id="apartmentDiv" style="display:<?php echo ($detail['address_type']=='apartment')?'block':'none'; ?>">
                            <input type="text" name="apartment" id="apartment" class="form-control" placeholder="Apartment No. *"  value="<?php echo $detail['apartment_no']; ?>">
                            <span id="add_apt_err" class="text-danger text-center error" style="display: none;">Please enter apartment no.</span>
                            </div>
                            
                            <div class="form-group extra_options" id="officeDiv" style="display:<?php echo ($detail['address_type']=='office')?'block':'none'; ?>">
                            <input type="text" name="office" id="office" class="form-control" placeholder="Office*"  value="<?php echo $detail['office']; ?>">
                            <span id="add_office_err" class="text-danger text-center error" style="display: none;">Please enter office</span>
                            </div>
                            
                          <div class="form-group">
                            <textarea name="additional_directions" class="form-control" placeholder="Additional Directions"><?php echo $detail['additional_directions']; ?></textarea></div>
                           <div class="form-group">
                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile Number*" value="<?php echo $detail['mobile']; ?>">
                            <span id="add_mobile_err" class="text-danger text-center error" style="display: none;">Please enter mobile number</span>
                            
                            </div>
                            
                           <div class="form-group extra_options" id="landlineDiv" style="display:<?php echo ($detail['address_type']=='home')?'block':'none'; ?>">
                            <input type="text" name="landline" class="form-control" placeholder="Landline Number" value="<?php echo $detail['landline']; ?>">
                            </div>
 
                        <div class="update_btn_sec">
                           <button type="submit" class="btn blue-btn">Update</button>
                        </div>
                       <?php echo form_close(); ?>
                        
<script>
                           
$("#edit_add").keyup(function(){
 var input = document.getElementById('edit_add');
 var autocomplete = new google.maps.places.Autocomplete(input);
});                     

                       </script>