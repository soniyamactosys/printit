<div class="main_content">
		    <div class="profile-details-area">
         <div class="personal-information">
             <div class="user_heading">
              <h4 class="profile-heading">Personal Information </h4>
                <button class="edit_btn" data-toggle="modal" data-target="#account" type="button"><i class="fas fa-user-edit"></i></button>
              </div>
             <div class="row">
                 <div class="col-md-4 col-4">
                      <div class="personal-img">
                          <img src="<?php echo base_url('webassets/images/dummy.png'); ?>">
                      </div>
                       <div class="name-details">
                            
                             <h4><span><?php echo ucfirst($user_data['firstname'])." ".ucfirst($user_data['lastname']); ?></span></h4>
                         </div>
                 </div>
                 <div class="col-md-7 col-7">
                     <div class="personal-details">
                         
                     
                         <div class="all-personal-details">
                             <ul>
                                 
                                  <li>
                                     <span class="first-id">Email :</span>
                                     <span class="secound-id"><?php echo $user_data['email']; ?></span>
                                 </li>
                                 <li>
                                     <span class="first-id"> First Name : </span>
                                     <span class="secound-id"><?php echo ucfirst($user_data['firstname']); ?></span>
                                 </li>
                                 <li>
                                     <span class="first-id">Last Name : </span>
                                     <span class="secound-id"> <?php echo ucfirst($user_data['lastname']); ?></span>
                                 </li>
                                 
                                 <li>
                                     <span class="first-id">Mobile Number :</span>
                                     <span class="secound-id"><?php echo $user_data['mobile']; ?></span>
                                 </li>
                                 
                                  <li>
                                     <span class="first-id">address :</span>
                                     <span class="secound-id"> <?php echo $user_data['address']; ?></span>
                                 </li>
                                  <li>
                                     <span class="first-id">Gender :</span>
                                     <span class="secound-id"> <?php echo ucfirst($user_data['gender']); ?></span>
                                 </li>
                                 
                             </ul>
                         </div>
                         
                     </div>
                 </div>
                 
             </div>
             
         </div>
         
        
         
       
          
      </div>
	
</div>