<div class="main_content no-padding">
<?php
    require('./application/controllers/PaymentController.php'); 
    $paymentController = new PaymentController();
    $paymentController->formSubmit();
    $date = new DateTime();
?>
<h1 class="text-center"> Printit Pay here</h1> 

<form action="" class="form" method="post" id="paymentForm">
            <div class="form-group">
                <label for="merchant_code"> <strong>Merchant Code</strong> </label> <br>
                <input type="text" name="merchantCode" value="<?php echo Constantspay::MERCHANT_CODE;?>">
                 <p class="error-display" id="merchantCode_validate"></p>
                
            </div>
            <div class="form-group">
                <label for="amount"> <strong>Amount</strong></label> <br>
                <input type="text" name="amount" value="<?php echo mt_rand(10, 999);?>">
                <p class="error-display" id="amount_validate"></p>
            </div>
            <div class="form-group">
                <label for="currency"> <strong>Currency</strong></label> <br>
                <select class="form-control" id="currency" name="currency">
                    <option name="currency" value=""> Select currency</option>
                    <option name="currency" value="KWD"> Kuwaiti Dinar</option>
                    <option name="currency" value="BHD"> Bahraini Dinar</option>
                    <option name="currency" value="AED"> Emirati Dirham</option>
                    <option name="currency" value="OMR"> Omani Rial</option>
                    <option name="currency" value="QAR"> Qatari Rial</option>
                    <option name="currency" value="SAR"> Saudi Rial</option>
                    <option name="currency" value="USD"> US Dollar</option>
                    <option name="currency" value="GBP"> British Pound</option>
                    <option name="currency" value="EUR"> Euro</option>
                </select>
                <p class="error-display" id="currency_validate"></p>
            </div>
            <div class="form-group">
                <label for="paymentType"> <strong>Payment Type</strong> </label> <br>
                <input type="radio" name="paymentType" value="0" checked> Indirect
                <input type="radio" name="paymentType" value="1"> KNET
                <input type="radio" name="paymentType" value="2"> MPGS
                <!-- <input type="radio" name="paymentType" value="5" checked> Tokenization -->
                <p class="error-display" id="paymentType_validate"></p>
            </div>
            <div class="form-group">
                <label for="responseUrl"> <strong>Response URL</strong> </label> <br>
                <input type="text" name="responseUrl" value="<?php echo Constantspay::RESPONSE_URL;?>">
                <p class="error-display" id="responseUrl_validate"></p>
            </div>
            <div class="form-group">
                <label for="failureUrl"> <strong>Failure URL</strong> </label> <br>
                <input type="text" name="failureUrl" value="<?php echo Constantspay::FAILURE_URL;?>">
                <p class="error-display" id="failureUrl_validate"></p>
            </div>
            <div class="form-group">
                <label for="orderReferenceNumber"> <strong>Order Reference Number</strong> </label> <br>
                <input type="text" name="orderReferenceNumber" value="<?php echo $date->getTimestamp(); ?>">
                <p class="error-display" id="orderReferenceNumber_validate"></p>
            </div>
            <!--<div class="form-group">
                <label for="variable1"> <strong>Variable 1</strong> </label> <br>
                <input type="text" name="variable1" value="">
            </div>
            <div class="form-group">
                <label for="variable2"> <strong>Variable 2</strong> </label> <br>
                <input type="text" name="variable2" value="">
            </div>
            <div class="form-group">
                <label for="variable3"> <strong>Variable 3</strong> </label> <br>
                <input type="text" name="variable3" value="">
            </div>
            <div class="form-group">
                <label for="variable4"> <strong>Variable 4</strong> </label> <br>
                <input type="text" name="variable4" value="">
            </div>
            <div class="form-group">
                <label for="variable5"> <strong>Variable 5</strong> </label> <br>
                <input type="text" name="variable5" value="">
            </div>-->
            <div class="form-group">
                <label for="version"> <strong>Version</strong></label> <br>
                <input type="text" name="version" value="<?php echo Constantspay::VERSION ?>">
                <p class="error-display" id="version_validate"></p>
            </div>            
            <div class="form-group">
                <button class="btn" type="submit" value="submit" name="submit">Submit</button>
            </div>
        </form>
</div>