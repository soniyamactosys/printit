<?php 
$userdata = $this->session->userdata('userdata');
$service_fee = $this->session->userdata('service_fee');
$vendor_charge = $order_data['amount'];
$total = floatval($vendor_charge)+floatval($service_fee);
//$total = $vendor_charge;
$products = $order_data['products'];
if(!empty($products)){
  $total += get_prodTotal($products);
}

if($order_data['delivery_charge']){
  $delivery_fee = $order_data['delivery_charge'];
}else{
  $delivery_fee = 0;
}

?>

         <!-- Page Content  -->
    <input type="hidden" id="inv_no" value="<?php echo $order_data['order_number']; ?>"  />
<div class="main_content">
  <div class="invoice-area">
       <div class="row">
          <div class="col-md-6">
             <div class="invoice-box-left">
               <h6>ORDER NUMBER</h6>
               <h5>#<?php echo $order_data['order_number']; ?></h5>
               <h4>Order Confirmed</h4>
               <p>Thank you for trusting <br> Print it </p>

             </div>
          </div>
           <div class="col-md-6">
            <div class="invoice-box-right">
               <h4>Order Details</h4>
               <div class="order-detail-area">
                  <div class="order-del-content">
                     <ul>
                        <li><?php if($order_data['order_type']==1){
                              echo "Custom Print";
                             // $img = 'Translation.png';
                           }elseif($order_data['order_type']==2){
                              echo "Quick Print";
                              //$img = 'Translation.png';
                           }elseif($order_data['order_type']==3){
                              echo "Translation";
                              //$img = 'Translation.png';
                           }elseif($order_data['order_type']==4){
                              echo "Notes";
                              //$img = 'Translation.png';
                           }
                           ?></li>

                        <li>Service Fee</li>
                        <?php if(!empty($products)){ foreach($products as $value){ ?><li><?php echo $value['name']; ?></li>
                        <?php } } ?>
                        
                        <?php if($order_data['pickup_delivery'] =='2'){ ?>
                        <li>Delivery Fees</li>
                        <?php } ?>                        

                        <?php if(!empty($order_data['applied_coupon_amt'])){
                         ?>
                        <li>Coupon Applied </li>
                        <?php } ?>
                     </ul>
                      <ul>
                        <li><?php echo $order_data['amount']; ?>  KD</li>
                        <li><?php echo $service_fee; ?>  KD</li>

                        <?php if(!empty($products)){ foreach($products as $value){ ?>
                        <li><?php echo floatval($value['prod_price'])*floatval($value['prod_qty']); ?> KD</li>
                        <?php } } ?>
                        
                        <?php if($order_data['pickup_delivery'] =='2'){ ?>
                        <li><?php echo $delivery_fee; ?> KD</li>
                        <?php 
                        $total += $delivery_fee;
                        } ?>
                        
                        <?php  if(!empty($order_data['applied_coupon_amt'])){ 
                          $coupon_amt = $order_data['applied_coupon_amt'];
                          $total = floatval($total) -floatval($coupon_amt);
                        ?>
                        <li><?php echo $coupon_amt; ?> KD</li>
                        <?php } ?>


                     </ul>
                  </div>
                  <div class="order-del-footer">

                      <ul>
                        <li>Total</li>
                     </ul>
                      <ul>
                        <li><?php echo $total; ?>  KD</li>
                     </ul>
                  </div>
               </div>

               <div class="print-shop-area">
                  <h4>Print Shop</h4>
                   <p><?php echo $order_data['vendor_name'];   ?></p>
                  <p><?php 
                  $shop_building = $order_data['p_address'];
                  $shop_lat = $order_data['p_lat'];
                  $shop_long = $order_data['p_lng'];
                  
                  echo $shop_building;
                   ?></p>
               </div>
               <div class="invoice-map-area">
                  <?php /*
                  <div class="mapouter"><div class="gmap_canvas"><iframe width="220" height="81" id="gmap_canvas" src="https://maps.google.com/maps?q=kuwait%20city&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net">embed google map</a></div><style>.mapouter{position:relative;text-align:right;height:81px;width:220px;}.gmap_canvas {overflow:hidden;background:none!important;height:81px;width:220px;}</style></div>
                  */ ?>

                  <?php
                  echo '<div class="mapouter"><div class="gmap_canvas"><iframe src = "https://maps.google.com/maps?q='.$shop_lat.','.$shop_long.'&hl=es;z=14&amp;output=embed"></iframe></div></div>';
                  ?>
                   <?php /*
                  <div class="mapouter"><div class="gmap_canvas"><iframe width="220" height="81" id="gmap_canvas" src="https://maps.google.com/maps?q=$shop_building%20city&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net">embed google map</a></div><style>.mapouter{position:relative;text-align:right;height:81px;width:220px;}.gmap_canvas {overflow:hidden;background:none!important;height:81px;width:220px;}</style></div>*/ ?>
               </div>
             </div>
          </div>
       </div>
  </div>
</div>
           
           <?php /*
            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount">Account Name <img src="images/user.png"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>Translation  <span><img src="images/Translation.png"></span></h4>
                                       <p>Project Name<br>
                                          Here
                                       </p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p>Select
                                          <br>Printerie
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="promotion-code-area">
                              <h4>Enter Promotion Code</h4>
                              <input type="text"   class="form-control" placeholder="1234567">
                              <span>10% Discount added</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        */ ?>
<style>.mapouter{position:relative;text-align:right;height:81px;width:220px;}.gmap_canvas {overflow:hidden;background:none!important;height:81px;width:220px;}</style>        