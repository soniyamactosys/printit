<?php
   //echo "<pre>"; print_r($data); exit;
   ?>
<section class="invoice printableArea" style="width: 800px;
   margin: auto;
   padding: 15px;
   box-shadow: 0 0 10px #ebf3fb;">
   <div class="invoice_print_inner">
      <div class="page-header" style="    justify-content: space-between;
         align-items: center;
         border-bottom: 1px solid #e6e6e6;
         display: -webkit-box;
         display: -moz-box;
         display: -ms-flexbox;
         display: -webkit-flex;
         display: flex;
         padding: 15px 10px;">
         <h2  style="margin: 0;
            font-family: poppins,sans-serif;
            font-size: 30px;">Invoice</h2>
         <h3 style="margin: 0;
            font-family: poppins,sans-serif;
            font-size: 20px;
            color: #4c4c4c;
            "><?php echo date('d/m/Y',strtotime($data['created_at'])); ?></h3>
      </div>
   </div>
   <!-- /.col -->
   <div class="invoice-info" style="
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid #e6e6e6;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      padding: 15px 10px;">
      <div class=" invoice-col">
         <strong style="margin: 0;
            font-family: poppins,sans-serif;
            font-size: 15px;">From</strong> 
         <address>
            <h4 style="
               margin: 0;
               font-family: poppins,sans-serif;
               font-size: 25px;
               padding: 6px 0;
               color: #212529;
               font-style: normal;">Print It</h4>
            <p style="font-weight: 600;
               margin: 0;
               font-style: normal;
               font-family: poppins,sans-serif;
               font-size: 14px;
               color: #212529;
               padding-bottom: 7px;">124 Lorem Ipsum, Suite 478,  Dummuy, USA 123456</p>
            <p style="font-weight: 600;
               margin: 0;
               font-style: normal;
               font-family: poppins,sans-serif;
               font-size: 14px;
               color: #212529;
               padding-bottom: 7px;
               width: 100%;">Phone: (00) 123-456-7890 <span style="float: right;">Email: info@example.com</span></p>
         </address>
      </div>
      <!-- /.col -->
      <div class="invoice-col text-right">
         <strong style="margin: 0;
            font-family: poppins,sans-serif;
            font-size: 15px;">To</strong>
         <address>
            <h4 style="margin: 0;
               font-family: poppins,sans-serif;
               font-size: 25px;
               padding: 6px 0;
               color: #212529;
               font-style: normal;"
               ><?php echo $data['customer_name']; ?></h4>
            <p style="font-weight: 600;
               margin: 0;
               font-style: normal;
               font-family: poppins,sans-serif;
               font-size: 14px;
               color: #3c4248;
               padding-bottom: 7px;"><?php echo $data['d_address']; ?></p>
            <p style="font-weight: 600;
               margin: 0;
               font-style: normal;
               font-family: poppins,sans-serif;
               font-size: 14px;
               color: #3c4248;
               padding-bottom: 7px;
               width: 100%;">Phone: <?php echo $data['customer_mobile']; ?> <span style="float: right;">  Email: <?php echo $data['customer_email']; ?></span></p>
         </address>
      </div>
   </div>
   <?php //print_r($data); ?>
   <!-- /.col -->
   <div class="invoice-details" style="padding: 20px 14px;">
      
      <div style="color: #212529;
         font-size: 16px;
         font-weight: 600;
         font-family: poppins,sans-serif;"><span style=" font-weight: 600;
         margin: 0;
         font-style: normal;
         font-family: poppins,sans-serif;
         font-size:20px;
         color: #78bbff;">Order ID:</span> <?php echo $data['id']; ?></div>

   </div>
   <!-- /.col -->
   <div>
      <table style="width: 100%;
         border-collapse: collapse;
         border: 1px solid #e8edf3;
         margin-bottom: 20px;">
         <tbody>
            <tr>
               <th style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  background: #78bbff;
                  border: none;
                  color: white;
                  font-family: poppins,sans-serif;
                  font-size: 16px;">#</th>
               <th style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  background: #78bbff;
                  border: none;
                  color: white;
                  font-family: poppins,sans-serif;
                  font-size: 16px;">Description</th>
               <th style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  background: #78bbff;
                  border: none;
                  color: white;
                  font-family: poppins,sans-serif;
                  font-size: 16px;">Quantity</th>
               <th style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  background: #78bbff;
                  border: none;
                  color: white;
                  font-family: poppins,sans-serif;
                  font-size: 16px;">Subtotal</th>
            </tr>
            <tr>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;">1</td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php echo $data['project_name']; ?></td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;">1</td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php echo $data['amount']; ?> KD</td>
            </tr>
            <?php $i = 2; if(!empty($data['products'])){ foreach($data['products'] as $prod){ ?>
            <tr>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php echo $i; ?></td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php echo $prod['name']; ?></td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php echo $prod['prod_qty']; ?></td>
               <td style="border-collapse: collapse;
                  padding: 10px 10px;
                  text-align: center;
                  font-family: poppins,sans-serif;
                  font-size: 15px;
                  border: 1px solid #e8edf3;"><?php 
                  echo floatval($prod['prod_qty'])*floatval($prod['prod_price']); 
                  ?> KD</td>
            </tr>
            <?php $i++; } } ?>
         </tbody>
      </table>
   </div>
   <!-- /.col -->
   <div style="text-align: right;">
      <div>
         <p style="font-weight: 700;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 12px;
            color: #212529;
            padding-bottom: 5px;">Sub - Total amount  :  <?php echo $data['amount']; ?> KD</p>
         <p style="font-weight: 700;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 12px;
            color: #212529;
            padding-bottom: 5px;">Service Fee  : <?php echo $data['admin_charge']; ?> KD</p>
         <?php if(!empty($data['product_total'])){ ?>
         <p style="font-weight: 700;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 12px;
            color: #212529;
            padding-bottom: 5px;">Product Total :  <?php echo $data['product_total']; ?> KD </p>
         <?php } ?>
         <?php if($data['pickup_delivery']==2){ ?>
         <p style="font-weight: 700;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 12px;
            color: #212529;
            padding-bottom: 5px;">Delivery Fee  :  <?php echo $data['delivery_charge']; ?> KD</p
            >
         <?php } ?>
         <?php if(!empty($data['applied_coupon_amt'])){ ?>
         <p style="font-weight: 700;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 12px;
            color: #212529;
            padding-bottom: 5px;">Coupon Applied  :  <?php echo $data['applied_coupon_amt']; ?> KD</p
            >
         <?php } ?>
      </div>
      <div class="total-payment">
         <h3 style="font-weight: 600;
            margin: 0;
            font-style: normal;
            font-family: poppins,sans-serif;
            font-size: 20px;
            color: #78bbff;
            padding: 7px 0;"><b>Total :</b> <?php echo $data['order_total']; ?> KD</h3>
      </div>
   </div>
</section>
</div>
</div>
</div>
</body>
</html>