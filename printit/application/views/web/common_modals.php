<?php
//echo "<pre>"; print_r($userdata['gender']); exit;

$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
}

if(!empty($this->data['user_data'])){
$userdata = $this->data['user_data'];

?>
<style>
.pac-container {
        z-index: 10000 !important;
    }
    </style>
    
<div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade right_sidebar" id="account" tabindex="-1" role="dialog" aria-labelledby="account">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h2><button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button> Account</h2>
       
      </div>
      <div class="modal-body">
       <h3>Hello, <?php echo $userdata['firstname'];  ?></h3>
       <span id="profile_update_msg" class="text-success" style="display:none;"></span>
       <form action="<?php echo site_url('user/update_profile'); ?>" id="updateprofile">
        <div class="form-group">
        <input type="email" name="email" id="pemail" value="<?php echo $userdata['email'];  ?>" placeholder="Enter Email Id" class="form-control" >
        <span class="text-danger text-center text-bold error" id="pemail_err" style="display:none;">Please Enter Valid Email Address</span>
        </div>
        <div class="form-group">
        <input type="text" name="f_name" id="p_fname" value="<?php echo $userdata['firstname'];  ?>" placeholder="First name" class="form-control">
        <span class="text-danger text-center text-bold error" id="p_fname_err" style="display:none;">Please Enter Your First Name</span>
        </div>
           <div class="form-group">
           <input type="text" name="l_name" id="p_lname" value="<?php echo $userdata['lastname'];  ?>" placeholder="Last name" class="form-control">
           <span class="text-danger text-center text-bold error" id="p_lname_err" style="display:none;">Please Enter Your Last Name</span>
           </div>
          <div class="form-group">
          <input type="text" name="mobile" id="pmobile" value="<?php echo $userdata['mobile'];  ?>" placeholder="Mobile Number" class="form-control">
          <span class="text-danger text-center text-bold error" id="p_mobile_err" style="display:none;">Please Enter Your Mobile Number</span>
          </div>
          <?php /*
            <div class="form-group">
            <input type="address" name="address"  id="profile_address" placeholder="Address" class="form-control">
            
            <input type="hidden" id="prof_lat" name="user_lat"   value="<?php echo $userdata['latitude']; ?>" >
            <input type="hidden" id="prof_long" name="user_lng" value="<?php echo $userdata['longitude']; ?>" >
            <span class="text-danger text-center text-bold error" id="p_address_err" style="display:none;">Please Enter Your Address</span>
            </div>
            */ ?>
        
            <div class="genderDiv">
            
            <h4 class="gendar_heading">Gender</h4>
            
            <label class="man-label <?php echo (strcasecmp($userdata['gender'],"male")==0)?"active":""; ?>">
            <input type="radio" name="gender" class="user_gender" value="male" <?php echo (strcasecmp($userdata['gender'],"male")==0)?"checked":""; ?> >
            <span class="man-image"></span>
            </label>

              <label class="woman-label <?php echo (strcasecmp($userdata['gender'],"female")==0)?"active":""; ?>">
               <input type="radio" name="gender" class="user_gender" value="female" <?php echo (strcasecmp($userdata['gender'],"female")==0)?"checked":""; ?> >
                <span class="woman-image"></span>
              </label>

            <span class="text-danger text-center text-bold error" id="p_gender_err" style="display:none;">Please Select Your Gender</span>             
            </div>
          <div class="submit_button"><button class="btn primary-btn">Update</button></div>
       </form>
      </div>
      
    </div>
  </div>
</div>

<?php
}
/*
<div class="modal fade" id="cart" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="cartLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content cart-scrollbar" id="cart-style">
      <span class="carticonbox"><img src="<?php echo base_url('webassets/images/cart.png'); ?>"></span>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
        <?php
 //echo "<pre>"; print_r($in_progress_orders);
 
         if(!empty($in_progress_orders)){ 
           $img = 'webassets/images/Custom-print1.png';
          foreach($in_progress_orders as $value){ 
            if($value['order_type']==1){
              $img = 'Custom-print.png';
              $order_status = "Being Printed";
              $link = site_url('order_detail/').encoding($value['order_id']);
              //$link = 'order_detail/';

              // if($value['progress_status']=='being_printed'){
              //   $order_status = "Being Printed";
              // }else{

              // }

              $order_status = ucfirst(str_replace('_',' ',$value['progress_status']));


            }elseif($value['order_type']==2){
              $img = 'Custom-print1.png';

            }elseif($value['order_type']==3){
              $img = 'Custom-print1.png';

            }elseif($value['order_type']==4){ 
              $img = 'Custom-print.png';
            }
          ?>
          <div class="cart_product_box">
            <div class="cart_icon"><img src="<?php echo base_url('webassets/images/'.$img); ?>"></div>
            <div class="cart_content"><h3>#<?php echo $value['order_id']; ?></h3>
            <h2><?php echo $order_status; ?></h2></div>
            <a href="<?php echo $link; ?>"><i class="fas fa-arrow-right"></i></a>
          </div>
        <?php  } } ?>
        </div>
  </div>
</div></div>
*/ ?>


<div class="modal fade" id="cart" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="cartLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <span class="carticonbox"><img src="<?php echo base_url('webassets/images/cart.png'); ?>"></span>
      <div class="cart-scrollbar" id="cart-style">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
        <?php
 //echo "<pre>"; print_r($in_progress_orders); exit;
 
         if(!empty($in_progress_orders)){ 
          $img = 'webassets/images/Custom-print1.png';
          foreach($in_progress_orders as $value){ 

            $order_status = ucfirst(str_replace('_',' ',$value['progress_status']));
            $link = site_url('order_detail/').encoding($value['order_id']);

            if($value['order_type']==1){
              $img = 'Custom-print.png';
              $order_status = "Being Printed";
            }elseif($value['order_type']==2){
              $img = 'Custom-print1.png';

            }elseif($value['order_type']==3){
              $img = 'Custom-print1.png';

              if($value['progress_status']=='proposal_received'){
                $link = site_url("show_translation_proposals/").encoding($value['order_id']);
              }elseif($value['progress_status']=='proposal_accepted' && $value['payment_status']=='0'){
                $link = site_url("user/trans_payment/").encoding($value['order_id']);
              }

            }elseif($value['order_type']==4){ 
              $img = 'Custom-print.png';
            }
          ?>
          <div class="cart_product_box">
            <div class="cart_icon"><img src="<?php echo base_url('webassets/images/'.$img); ?>"></div>
            <div class="cart_content"><h3>#<?php echo $value['order_id']; ?></h3>
            <h2>
              <?php if($value['order_type']==3 && $value['progress_status']=='proposal_received'){ 
                echo $value['trans_proposal_count']." Proposals received"; 
                }else{ echo $order_status; } ?>
                
              </h2></div>
            <a href="<?php echo $link; ?>"><i class="fas fa-arrow-right"></i></a>
          </div>
        <?php  } } ?>
        </div>
        </div>
  </div>
</div></div>


<div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade printeries right_sidebar" id="printeries" tabindex="-1" role="dialog" aria-labelledby="printeries">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <div class="fileupload_name"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>     <span><i class="fas fa-check"></i></span> </div>
       
      </div>
      <div class="modal-body">
        <div class="product_box">
                      <div class="row">
                        <div class="col-md-4"><div class="product_img"><img src="<?php echo base_url('webassets/images/g4652.png'); ?>" class="mCS_img_loaded"></div></div>
                        <div class="col-md-5"><div class="product_content">
                          <h4>Print House Name</h4>
                          <h2>Salmiyah Baghdad St.</h2>
                          <h4>09 AM - 5 PM</h4>
                        </div></div>
                        <div class="col-md-3"><span class="open">open</span></div>
                      </div>
                    </div>
                     <div class="product_box">
                      <div class="row">
                        <div class="col-md-4"><div class="product_img"><img src="<?php echo base_url('webassets/images/g4652.png'); ?>" class="mCS_img_loaded"></div></div>
                        <div class="col-md-5"><div class="product_content">
                          <h4>Print House Name</h4>
                          <h2>Salmiyah Baghdad St.</h2>
                          <h4>09 AM - 5 PM</h4>
                        </div></div>
                        <div class="col-md-3"><span class="closed">closed</span></div>
                      </div>
                    </div>
                    
      </div>
      
    </div>
  </div>
</div>
 

<!-- <div class="modal fade poster_popup" id="plz_login_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">To continue Please Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-primary" id="open_login_modal">Go To Login</button>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div> -->

<div class="modal fade service_type_modal" id="service_type_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area" style="height: 375px;background:rgba(0,0,0,0.3) ">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel" style="color:white !important;"><?php echo $this->lang->line('select_service_type');  ?></h5>
      </div>
      <div class="modal-body">
    <div class="select_services_inner">
        <a class="serviceTypeOptions" href="javascript:void(0);" action="<?php echo site_url('set_service_type/express'); ?>">
        <div class="product_box">
          <div class="row">
            <div class="col-md-5 col-sm-5 col-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printit-logo.png'); ?>" class="mCS_img_loaded"></div></div>
            <div class="col-md-7 col-sm-7 col-7"><div class="product_content">
              <h4><?php echo $this->lang->line('print-it_express');  ?></h4>
              <h2><?php echo $this->lang->line('service_provided_by_print-it_app_with_delivery_option_only');  ?></h2>
            </div></div>
          </div>
        </div>
        </a>
        <a class="serviceTypeOptions" href="javascript:void(0);" action="<?php echo site_url('set_service_type/normal'); ?>">
        <div class="product_box">
          <div class="row">
            <div class="col-md-5 col-sm-5 col-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printt-house.png'); ?>" class="mCS_img_loaded"></div></div>
            <div class="col-md-7 col-sm-7 col-7"><div class="product_content">
              <h4><?php echo $this->lang->line('local_printeries');  ?></h4>
              <h2><?php echo $this->lang->line('service_by_your_favorite_printery_That_is_available_for_delivery_or_pickup');  ?></h2>
            </div></div>
          </div>
        </div>
        </a>
</div>

      </div>
    </div>
  </div>
</div> 