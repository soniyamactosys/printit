<?php 
$userdata = $this->session->userdata('userdata');
$currentorder = $this->session->userdata('current_order');
$selected_printry = $this->session->userdata('selected_printry');
$oid =  $this->session->userdata('continue_order');
$prev_url = get_order_prev_step($oid,current_url());
$service_fee = $this->session->userdata('service_fee');
$delivery_fee = $this->session->userdata('current_order')['delivery_charge'];

$vendor_charge = $this->session->userdata('vendor_charge');
if(empty($vendor_charge)){
  $vendor_charge = $this->session->userdata('current_order')['vendor_charge'];
}
$order_total = floatval($vendor_charge)+floatval($service_fee);


$coupon_amt = 0;
if(!empty($this->session->userdata('coupon_applied'))){ 
  $coupon_end = $this->session->userdata('coupon_applied')['end_date'];
  $coupon_amt = $this->session->userdata['coupon_applied']['applied_amount'];
  if(strtotime($coupon_end) >= strtotime(date('Y-m-d'))){
    $order_total -= $coupon_amt;
  }
}

$order_products = $this->session->userdata('order_products');
if(!empty($order_products)){
  $order_total += get_prodTotal($order_products);
}

$order_total += $delivery_fee;

$order_total = $this->session->userdata('current_order')['order_total'];
?>

            <div class="main_content no-padding">
              <div class="product_order_outer">
               <div class="product-timeline row">
                   <ul class="timeline exp_pay">
                      <li class="active-tl"></li>
                      <!--<li class="active-tl"></li>-->
                      <li class="active-tl"></li>
                   </ul>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="order_detail_list">
                              <h2>order detail</h2>
                              <table class="table">
                                 <tbody><tr>
                                  <th><?php if($currentorder['order_type']==1){
                                          echo "Custom Print";
                                          //$img = 'Custom-print.png';
                                       }elseif($currentorder['order_type']==2){
                                          echo "Quick Print";
                                          //$img = '3.png';
                                       }elseif($currentorder['order_type']==3){
                                          echo "Translation";
                                          //$img = '4.png';
                                       }elseif($currentorder['order_type']==4){
                                          echo "Notes";
                                          //$img = '5.png';
                                       }
                                       //else{
                                          //$img = 'Translation.png';
                                       //}
                                       ?></th>
                                  <td><?php echo $vendor_charge; ?> KD</td></tr>
                                  <tr><th>Service Fee</th><td><?php echo $service_fee; ?> KD</td></tr>

                                  <?php if(!empty($order_products)){ 
                                    foreach($order_products as $prod){ ?>
                                      <tr><th><?php echo $prod['name']; ?></th><td><?php echo floatval($prod['price'])*floatval($prod['qty']); ?> KD</td></tr>
                                    <?php }
                                   } ?>
                                  <?php if(!empty($coupon_amt)){ ?>
                                   <tr><th>Coupon Applied</th><td><?php echo $coupon_amt; ?> KD</td></tr>
                                  <?php } ?>

                                  <?php if($this->session->userdata('pickup_delivery') =='delivery'){ ?>
                                  <tr><th>Delivery Fees</th><td><?php echo $delivery_fee; ?> KD</td></tr>
                                  <?php } ?>
                                   </tbody><tfoot>
                                      <tr><th>Total</th><td><?php echo $order_total; ?> KD</td></tr>
                                   </tfoot>
                              </table></div>
                        
                  </div>   
                  
                  <div class="col-md-4">
                  <?php /* 
                  <div class="order_address">
                      <ul>
                        <?php if($this->session->userdata('pickup_delivery') =='delivery'){ ?>
                          <li><h3>Delivery Address</h3><h5><?php echo $this->session->userdata('delivery_address'); ?></h5></li>
                        <?php } ?>
                        <li><h3>Printery Shop</h3><h5>
                          <?php echo $selected_printry['shop_name']; ?>
                        </h5></li>
                        <li><h3>Address</h3><h5><?php
                $fullAddress = '';
               

                if(!empty($selected_printry['building'])){
                  $fullAddress .= $selected_printry['building'].'<br>';
                }
                 echo $fullAddress;
                         ?></h5></li>
                      </ul>
                   
                  <?php
                  echo '<iframe src = "https://maps.google.com/maps?q='.$selected_printry['latitude'].','.$selected_printry['longitude'].'&hl=es;z=14&amp;output=embed"></iframe>';
                  ?>                      
                    </div>*/ ?>
                  </div>
                  
                  <div class="col-md-4">
                    <h3>Payment Terms</h3>
                    <h5>By making the payment you automatically 
Agree on Print it TREMS & CONDITIONS</h5>
                    <div class="order_address_action">

<?php  //echo form_open('user/payment_gateway'); ?>
                    
<?php

    require('./application/controllers/PaymentController.php'); 
    $paymentController = new PaymentController();
    $paymentController->formSubmit();
    $date = new DateTime();
?>
                      <form action="" class="form" method="post" id="paymentForm">
                             
                <input type="hidden" name="merchantCode" value="<?php echo Constantspay::MERCHANT_CODE;?>">
                <input type="hidden" name="amount" value="<?php echo $order_total; ?>">
                <input type="hidden" name="currency" value="KWD">
                <input type="hidden" name="paymentType" value="0" checked> 
                <input type="hidden" name="responseUrl" value="<?php echo Constantspay::RESPONSE_URL;?>">
                <input type="hidden" name="failureUrl" value="<?php echo Constantspay::FAILURE_URL;?>">
                <input type="hidden" name="orderReferenceNumber" value="<?php echo $date->getTimestamp(); ?>">
                <input type="hidden" name="version" value="<?php echo Constantspay::VERSION ?>">
                
                      <input type="submit" name="submit" class="btn blue-btn checkout" value="Checkout" >
                      <?php echo form_close(); ?>
                        <a href="<?php echo $prev_url; ?>" class="btn dark-btn continueshop">Back</a>
                      </div> 
                  </div>
                </div>
           
          </div>
            <?php /*
            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount">Account Name <img src="images/user.png"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li class="active-tl"></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>Translation  <span><img src="images/Translation.png"></span></h4>
                                       <p>Project Name<br>
                                          Here
                                       </p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p>Select
                                          <br>Printerie
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="order_detail_list">
                              <h2>order detail</h2>
                              <table class="table">
                                 <tr><th>Translation</th><td>2.5KD</td></tr>
                                  <tr><th>Delivery Fees</th><td>0.75KD</td></tr>
                                   <tr><th>Pin</th><td>1KD</td></tr>
                                   <tfoot>
                                      <tr><th>Total</th><td>3.75KD</td></tr>
                                   </tfoot>
                              </table>
                           </div>
                           <div class="promotion-code-area">
                              <h4>Enter Promotion Code</h4>
                              <input type="text"   class="form-control" placeholder="1234567">
                              <span>10% Discount added</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            */ ?>
         </div>
      </div>
      <?php /*
<div class="modal bottom fade animate" id="more_product" tabindex="-1" role="dialog" aria-labelledby="more_product">
  <div class="modal-dialog" role="document">
  <div class="modal-content animate-bottom">
  <div class="modal-header">
  <h5 class="modal-title">Minimum KD2 for delivery </h5>
  <h2>Total <span>1.5 KD</span></h2>

  </div>
  <div class="modal-body">
  <h3>Add More Products</h3>
  <div id="more_product_list" class="content">
   <ul>
      <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
       <li>
         <div class="more_product_box">
            <div class="more_product_img"><img src="images/Pencil.png"></div>
            <div class="more_product_content"><h4>Something Pen</h4><h6>0.5 KD</h6></div>
         </div>
      </li>
   </ul>

   </div>
    <div class="next-btn text-center"><a href="#"class="close" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#address1">Next</a></div>
  </div>

  </div>
  </div>
</div>

 <div class="modal right fade model-hide" id="address2" tabindex="-1" role="dialog" aria-labelledby="address2">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
              </button> 
                 <h3>Add New Address</h3>
               </div>
                <div class="modal-body">
                     <div class="form-group"><input type="text" class="form-control" placeholder="Address Name"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Area"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Address type"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="block"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Street"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Avenue"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Building"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Floor"></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Apartment No."></div>
                     <div class="form-group"><input type="text" class="form-control" placeholder="Mobile Number"></div>
                     <div class="form-group"><textarea class="form-control" placeholder="Extra Instructions"></textarea></div>

                  <div class="address_action">
                     <button class="btn blue-btn">Confirm</button>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      */ ?>
   </div>
</div>
<?php /*
<div class="modal right fade model-hide" id="address1" tabindex="-1" role="dialog" aria-labelledby="address1">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
              </button> 
                 <h3>Select Delivery Address</h3>
               </div>
                <div class="modal-body">
                  <ul>
                     <li><div class="radio_address"><label>Home <span>Salmiyah</span></label><input type="radio" name="address"></div></li>
                      <li><div class="radio_address"><label>Office <span>Kuwait City</span></label><input type="radio" name="address"></div></li>
                       <li><div class="radio_address"><label>Home2 <span>Salwa</span></label><input type="radio" name="address"></div></li>
                  </ul>
                  

                  <div class="address_action">
                     <a href="#" class="btn dark-btn"  data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#address2">New Address</a>
                     <a href="#" class="btn blue-btn">Done</a>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      */ ?>

   </div>
</div>
     