<?php //echo "<pre>"; print_r($detail);  exit;
//$order_total = 100;

?>
<!-- Page Content  -->
           <input type="hidden" id="inv_no" value="<?php echo $detail['order_number']; ?>"  />
<div class="main_content">
  <div class="container">
      <div class="order-details-area">
       <div class="row">
          <div class="col-md-6">
             <div class="invoice-box-left">
               <h6><?php echo $this->lang->line('ORDER_NUMBER'); ?></h6>
               <h5>#<?php echo $detail['order_number']; ?></h5>
               <h4><?php echo ucfirst(str_replace('_',' ',$detail['progress_status'])); ?></h4>
               <p><?php echo $this->lang->line('Thank_you_for_trusting_br_Print_it'); ?></p>

             </div>
              <div class="shop-address">
                  
                <div class="print-shop-area">
                  <h4><?php echo $this->lang->line('Print_Shop'); ?></h4>
                  <p><?php if(!empty($detail['vendor_name'])){ echo $detail['vendor_name'];
                  if($detail['service_type']==1){ echo 
                  "<br>".$detail['p_address']; } } ?></p>
                </div>
               <?php  if($detail['service_type']==1){ ?>
               <div class="invoice-map-area">
                 
                  <div class="mapouter"><div class="gmap_canvas">
                    <?php /*
                    <iframe width="220" height="81" id="gmap_canvas" src="https://maps.google.com/maps?q=kuwait%20city&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <a href="https://www.embedgooglemap.net">embed google map</a>
                    */ ?>
                  <?php
                  if(!empty($detail['p_lat']) && !empty($detail['p_lng'])){
                  //echo "<pre>"; print_r($detail); exit;
                  echo '<iframe src = "https://maps.google.com/maps?q='.$detail['p_lat'].','.$detail['p_lng'].'&hl=es;z=14&amp;output=embed"></iframe>';
                  }
                  ?>  
                  </div></div>
               </div>
               <?php } ?>
                  
              </div>
          </div>
           <div class="col-md-6">
              <?php if($detail['pickup_delivery']=='2'){ ?>
               <div class="shipping-area">
                   <h4>shipping detail</h4>
                   <?php if($detail['order_type']==3){ ?>
                   <ul>
                       <li><i class="fa fa-user" aria-hidden="true"></i> <span><?php if(!empty($detail['address'])){ echo $detail['address']; } ?></span></li>
                   </ul>
                   <?php }else{ ?>
                   <ul>
                       <li><i class="fa fa-user" aria-hidden="true"></i> <span><?php if(!empty($detail['customer_name'])){ echo $detail['customer_name']; } ?></span></li>
                       <li><i class="fa fa-map-marker" aria-hidden="true"></i> <span><?php if(!empty($detail['d_address'])){ echo $detail['d_address']; } ?></span></li>
                       <li><i class="fa fa-phone" aria-hidden="true"></i> <span><?php if(!empty($detail['customer_mobile'])){ echo $detail['customer_mobile']; } ?></span></li>
                   </ul>
                   <?php } ?>
                   
               </div>
            <?php } ?>
            <div class="invoice-box-right">
               <h4><?php echo $this->lang->line('order_details'); ?></h4>
               <div class="order-detail-area">
                  <div class="order-del-content">
                     <ul>
                        <li><?php 
                        if($detail['order_type']==1){
                         echo $this->lang->line('custom_print');
                        }elseif($detail['order_type']==2){
                           echo $this->lang->line('quick_print');
                        }elseif($detail['order_type']==3){
                            echo $this->lang->line('translation');
                        }elseif($detail['order_type']==4){ 
                          echo $this->lang->line('notes');
                        }
                        ?></li>
                          <li><?php echo $this->lang->line('service_fees'); ?></li>          
                      <?php if($detail['pickup_delivery']=='2'){ ?>
                        <li>Delivery Fees</li>
                      <?php } ?>

                      <?php if(!empty($detail['products'])){ foreach($detail['products'] as $pro){ ?>
                        <li><?php echo $pro['name']; ?></li>
                      <?php } } ?>
                      
                      <?php if(!empty($detail['applied_coupon_amt'])){ ?>
                      <li>Coupon Applied </li>
                      <?php } ?>
                     </ul>
                      <ul>
                        <li><?php echo $detail['amount']; ?>  KD</li>
                        <li><?php echo $detail['admin_charge']; ?>  KD</li>
                        <?php if($detail['pickup_delivery']=='2'){ ?>
                        <li><?php echo $detail['delivery_charge']; ?> KD</li>
                        <?php } ?>
                        <?php if(!empty($detail['products'])){ foreach($detail['products'] as $pro){ ?>
                          <li><?php echo floatval($pro['prod_price'])*floatval($pro['prod_qty']); ?> KD</li>
                        <?php } } ?>

                        <?php if(!empty($detail['applied_coupon_amt'])){ ?>
                          <li><?php echo $detail['applied_coupon_amt']; ?> KD</li>
                        <?php } ?>
                      </ul>
                  </div>
                  <div class="order-del-footer">

                      <ul>
                        <li><?php echo $this->lang->line('total'); ?></li>
                        
                     </ul>
                      <ul>
                        <li><?php echo $detail['order_total']; ?> KD</li>
                        
                     </ul>

                  </div>

                
                   
               </div>

               
             </div>
          </div>
         
       </div>
       </div>
  </div>
</div>
<style>.mapouter{position:relative;text-align:right;height:81px;width:220px;}.gmap_canvas {overflow:hidden;background:none!important;height:81px;width:220px;}</style>