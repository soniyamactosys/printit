<?php
// $userdata = $this->session->userdata('userdata');
// $currentorder = $this->session->userdata('current_order');
// $oid = $this->session->userdata('continue_order');
 $selected_printry = $this->session->userdata('selected_printry');

// $prev_link = get_order_prev_step($oid,current_url());
// echo "<pre>"; print_r($list);
// exit;

$project_name = $list[0]['project_name'];
?>
 <input type="hidden" name="order_id" id="oid" value="<?php echo $list[0]['order_id']; ?>" >
 <input type="hidden" name="selectedprintry" id="selectedprintry" value="<?php if(!empty($selected_printry)){ echo $selected_printry['id']; } ?>">

            <div class="main_content no-padding">
               <div class="proposal_products_area" id="notes_selection" >
                  <ul class="porposal_product_inner">
                     <li>
                        <ul class="pro_product_col1">
                           <?php 
                           if(!empty($list)){ foreach($list as $key=>$value){ ?>
                           <li class="trans_pro_product_box <?php if(!empty($selected_printry) && $selected_printry['id']==$value['branch_id']){ echo 'active'; } ?>" printryid="<?php echo $value['branch_id']; ?>" vendor_charge="<?php echo $value['vendor_charge']; ?>" >
                              <div class="row">
                                 <div class="col-sm-4 col-4">
                                    <div class="product_box_left">
                                       <div class="left_side_sec">
                                          <p>KD</p>
                                          <h4><?php echo $value['vendor_charge']; ?></h4>
                                       </div>
                                       <!--<div class="right_side_sec">-->
                                          <h4><?php //echo $value['shop_name']; ?></h4>
                                       <!--</div>-->
                                    </div>
                                 </div>
                                 <div class="col-sm-8 col-8">
                                    <div class="product_box_right">
                                       <div class="selected"><i class="fas fa-check"></i> Selected</div>
                                    <p><?php echo $value['note']; ?></p>
                                    </div>
                                    <div class=" select-box-inner">
                                       <i class="fa fa-check" aria-hidden="true"></i>
                                       <p>Selected</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <?php 
                           if($key%5==4){ ?></ul><li><ul class="pro_product_col1" >
                              </li><?php }
                        } } ?>
                        </ul>
                     </li>
                  </ul>
               </div>
               <span id="printry_error" class="text-danger text-center error" style="display: none;">Please Select Printry </span>
               <div class="product-next-btn">
                  
                     <?php 
                     $link = site_url('delivery');
                    //  if(!empty($order_type)){ 
                    //     if($order_type==1 || $order_type==4){
                    //       $link = site_url('pickup_delivery');
                    //     }else{
                    //       $link = site_url('payment');
                    //     } 
                    //  }else{
                    //     $link = '';
                    //  }
                     ?>
                    <?php /* <a class="print-prev-btn" href="<?php //echo $prev_link; ?>"><span><i class="fa fa-arrow-left" aria-hidden="true"></i></span> PREV </a> 
                   <a href="javascript:void(0)" class="orderstep1" action="<?php echo $link; ?>">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>*/ ?>
                  
               </div>
            </div>

            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>Translation <span><img src="<?php echo base_url('webassets/images/4.png'); ?>"></span></h4>
                                       <p><?php echo $project_name; ?></p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p id="printery_charge"><?php if(!empty($selected_printry)){ echo $selected_printry['vendor_charge']; } ?></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="product-next-btn model-next-btn">
                              <a href="javascript:void(0)" class="trans_orderstep1" >NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>


<?php if(!empty($selected_printry)){ ?>
<script type="text/javascript">

$(document).ready(function(){
  $("#right_modal").modal('show');
});

</script>
<?php } ?>  