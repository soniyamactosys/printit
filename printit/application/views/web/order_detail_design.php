<!-- Page Content  -->
           
<div class="main_content">
  <div class="container">
      <div class="order-details-area">
       <div class="row">
          <div class="col-md-6">
             <div class="invoice-box-left">
               <h6>ORDER NUMBER</h6>
               <h5>#8888</h5>
               <h4>Order Confirmed</h4>
               <p>Thank you for trusting <br> Print it </p>

             </div>
              <div class="shop-address">
                  
                  <div class="print-shop-area">
                  <h4>Print Shop</h4>
                  <p>test addresss</p>
               </div>
               <div class="invoice-map-area">
                 
                  <div class="mapouter"><div class="gmap_canvas"><iframe width="220" height="81" id="gmap_canvas" src="https://maps.google.com/maps?q=kuwait%20city&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net">embed google map</a></div><style>.mapouter{position:relative;text-align:right;height:81px;width:220px;}.gmap_canvas {overflow:hidden;background:none!important;height:81px;width:220px;}</style></div>
                  
               </div>
                  
              </div>
          </div>
           <div class="col-md-6">
               
               <div class="shipping-area">
                   <h4>shipping detail</h4>
                   
                   <ul>
                       <li><i class="fa fa-user" aria-hidden="true"></i> <span>Grant G Reese</span></li>
                       <li><i class="fa fa-map-marker" aria-hidden="true"></i> <span>788  Diamond Cove Lighthouse Point Florida</span></li>
                       <li><i class="fa fa-phone" aria-hidden="true"></i> <span>401-368-7494</span></li>
                   </ul>
                   
               </div>
            <div class="invoice-box-right">
               <h4>Order Details</h4>
               <div class="order-detail-area">
                  <div class="order-del-content">
                     <ul>
                        <li>Custom Print</li>
                        
                        <li>Delivery Fees</li>

                        <li>Pin</li>

                        <li>Coupon Applied </li>
                     </ul>
                      <ul>
                        <li>100  KD</li>
                        
                        <li>0.75 KD</li>
                        <li>1 KD</li>

                        <li>10 KD</li>


                     </ul>
                  </div>
                  <div class="order-del-footer">

                      <ul>
                        <li>Total</li>
                        
                     </ul>
                      <ul>
                        <li>200  KD</li>
                        
                     </ul>

                  </div>

                
                   
               </div>

               
             </div>
          </div>
         
       </div>
       </div>
  </div>
</div>
           
           <?php /*
            <div class="modal right fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount">Account Name <img src="images/user.png"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>Translation  <span><img src="images/Translation.png"></span></h4>
                                       <p>Project Name<br>
                                          Here
                                       </p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4>Total</h4>
                                       <p>Select
                                          <br>Printerie
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="promotion-code-area">
                              <h4>Enter Promotion Code</h4>
                              <input type="text"   class="form-control" placeholder="1234567">
                              <span>10% Discount added</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
        */ ?>