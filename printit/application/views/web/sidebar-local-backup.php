 <?php $userdata = $this->session->userdata('userdata');
 $headsearchdisplay = 'block';
 $placeholder = "";
 ?>
 <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" class="active side-menu">
           <button type="button" id="sidebarCollapse" class="btn"> <i class="fas fa-bars"></i><i class="fas fa-times"></i></button>
            <ul>
				<li><a href="<?php echo site_url(); ?>"> Home <span><img src="<?php echo base_url('webassets/images/Home-Icon.png'); ?>"></span></a></li>
				<li><a href="#" data-toggle="modal" data-target="#printeries">Printeries <span><img src="<?php echo base_url('webassets/images/Printt_House_ Icon.png'); ?>"></span></a></li>
				<?php if($userdata){ ?>
				<li><a href="<?php echo site_url('myorders'); ?>">Orders <span><img src="<?php echo base_url('webassets/images/order-icon.png'); ?>"></span></a></li>
				<li><a href="#" data-toggle="modal" data-target="#account" > Settings <span><img src="<?php echo base_url('webassets/images/Settings-icon.png'); ?>"></span></a></li>
				<li><a href="#" data-toggle="modal" data-target="#cart">Cart <span><img src="<?php echo base_url('webassets/images/cart-icon.png'); ?>"></span></a></li>
				<?php } ?>
				<li>
                           <div class="laung-btn">
                              <a href="#"><button>English</button></a>
                              <a href="#" class="laung-btn-arbic"><button>عربي</button></a>
                           </div>
                        </li>
            </ul>

        </nav>



        <!-- Page Content  -->
        <div id="content">
            <header>
                <div class="container-fluid">
                    <?php 
                    $segment1 = $this->uri->segment(1);
                    if($userdata){ 
                    
                    if($segment1=='home' || $segment1=='select_service_type' || $segment1==''){ ?>
                    <div class="row">
                        <div class="col-md-6">
                                <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"><span>The easiest way to print in Kuwait !</span></a>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2"></div>
                        <div class="col-lg-4 col-md-4">
                                <div class="dropdown">
                                  <div class="account_name text-right dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                                  
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('user'); ?>">Profile</a>
                                    <a class="dropdown-item" href="<?php echo site_url('userlogout'); ?>">Logout</a>
                                  </div>
                                </div>
                        </div>
                    </div>
                    <?php }else{ 
                        if($segment1=='print-type'){
                          $placeholder = ucfirst($this->uri->segment(2)); 
                          //$placeholder = "Poster";
                        }elseif($segment1=='customprint'){
                          $placeholder = "Custom Print";
                        }elseif($segment1=='quickprint'){
                          $placeholder = "Quick Print";
                        }elseif($segment1=='translation'){
                          $placeholder = "";
                        }elseif($segment1=='order_confirmed'){
                          $placeholder = "Invoice"; 
                        }elseif($segment1=='payment'){
                          $placeholder = "Payment"; 
                        }elseif($segment1=='pickup_delivery'){
                          $placeholder = "Order Details"; 
                        }elseif($segment1=='printry_shops'){
                          $placeholder = "Printery Name"; 
                        }else{
                          $placeholder = "Proposals";
                        }
                        //echo $placeholder; exit; 

                        if($segment1=='select_service_type'){
                          $headsearchdisplay = 'none';
                        }

                    ?>
                        
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"></a>
                        </div>
                        <?php if($segment1=='printry_shops' || $segment1=='pickup_delivery' || $segment1=='payment' || $segment1=='order_confirmed' || $segment1=='print-type' || $segment1=='print-type'){

                          // if($segment1=='order_confirmed'){$placeholder = "Invoice"; }else{$placeholder = "Proposals"; }

                         ?>
                          <div class="col-xl-5 col-lg-5 col-md-5">
                            <div class="head_search_bar proposal-head-bar">
                               
                                <button onclick="goBack()" class="backbtn"><i class="fas fa-arrow-left"></i></button>
                                <form>
                                <input type="search" class="form-control" placeholder="<?php echo $placeholder; ?>">
                                  
                                  <?php if($segment1=='pickup_delivery'){ ?>
                                    
                                  <?php }else{ ?>
                                  <button data-toggle="modal" data-target="#savetocart" > <img src="<?php echo base_url('webassets/images/Save-Icon.png'); ?>"></button>
                                  <?php } ?>

                                  <!-- <button onclick="gotohome()">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                  </button> -->
                               </form>
                            </div>
                         </div>
                        <?php }elseif($segment1=='myorders'){ ?>
                        <div class="head_search_bar proposal-head-bar">
                           <form>
                              <input type="search" class="form-control" placeholder="Orders">
                              <button><img src="<?php echo base_url('webassets/images/Trash-icon.png'); ?>"></button>
                           </form>
                        </div>
                        <?php }else{ ?>
                        
                        <div class="col-xl-5 col-lg-5 col-md-5">
                            <div class="head_search_bar" style="display: <?php echo $headsearchdisplay; ?>">
                              
                                <button onclick="goBack()" class="backbtn"><i class="fas fa-arrow-left"></i></button>
                                <form>
                                <input type="search" class="form-control" placeholder="<?php echo $placeholder; ?>">
                              </form>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-4 col-md-4">
                                <div class="dropdown">
                                  <div class="account_name text-right dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $userdata['name']; ?> <img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                                  
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('user'); ?>">Profile</a>
                                    <a class="dropdown-item" href="<?php echo site_url('userlogout'); ?>">Logout</a>
                                  </div>
                                </div>
                        </div>
                        
                    </div>
                    <?php } ?>
                    <?php }else{ ?>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" width="160px"><span>The easiest way to print in Kuwait !</span></a>
                        </div>
                        <div class="col-md-2 pull-right">
                            <div  id="form-site-header">
                           <div id="form-button-toggle">
                               <div class="hamburger"></div>
                               <div class="hamburger"></div>
                               <div class="hamburger"></div>
                            </div>
                             <div id="form-panel">
                                       <div class="form-section-area">
                                          <div class="login-area">
                                             <div class="login-logo">
                                                <a href="#"><img src="<?php echo base_url('webassets/images/logo.png'); ?>" class="animatable bounceIn"></a>
                                             </div>
                                             <div class="login-form">
                                                <form action="<?php echo site_url('home/userlogin'); ?>" method="post" id="userlogin">
                                                   <input type="email" id="login_email" name="email" placeholder="Email" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="l_email_err" style="display:none;">Please Enter Your Email Address</span>
                                                   <input type="password" placeholder="Password" name="password" id="login_pass" class="form-control animatable bounceInRight"  >
                                                   <span class="text-danger text-center text-bold error" id="l_pass_err" style="display:none;">Please Enter Password</span>
                                                   <div class="login-btn buttonBox">
                                                   
                                                        <button type="submit" class="animatable bounceIn">Login </button><div class="border"></div><div class="border"></div><div class="border"></div><div class="border"></div>
                                                    </div>
                                                      <a href="#" class="for-password animatable bounceInLeft" id="forget-btn">FORGOT PASSWORD </a>
                                                </form>
                                               
                                                <div class="account-infor">
                                                   <p class="animatable moveUp">Don’t have an account?</p>
                                                   <a href="javascript:void(0)" id="singup-btn" class="animatable bounceIn"> SIGNUP</a>
                                                    <a href="javascript:void(0)" class="vandor-btn animatable bounceIn" id="vendor-butn">I AM VENDOR</a>
                                                </div>

                                             </div>
                                          </div>
                                          <div class="signup-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft">  
                                                  <span id="back-btn">
                                                   <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                   </span>sign up
                                                </h4>
                                             </div>
                                             <span class="text-danger text-center text-bold error" id="common_err" style="display:none;"></span>
                                             <form class="form-signup" action="<?php echo site_url('home/user_register'); ?>" method="post" name="form" id="user_registeration">
                                             <div class="signup-form">
                                                

                                                   <input  type="email" name="email" id="semail" placeholder="Email" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="email_err" style="display:none;">Please Enter Your Email Address</span>
                                                   
                                                   <input  type="text" name="f_name" id="f_name" placeholder="First Name" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="f_name_err" style="display:none;">Please Enter Your First Name</span>

                                                   <input  type="text" name="l_name" id="l_name" placeholder="Last Name" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="l_name_err" style="display:none;">Please Enter Your Last Name</span>

                                                   <input  type="text" name="mobile" id="s_mobile" placeholder="Mobile Number" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="l_mobile_err" style="display:none;">Please Enter Your Mobile Number</span>


                                                   <input  type="address" name="address" id="u_address" placeholder="Address" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="u_address_err" style="display:none;">Please Enter Your Address</span>
                                                   <input type="hidden" name="user_lat" id="user_lat">
                                                   <input type="hidden" name="user_lng" id="user_lng">

                                                   <input  type="password" name="password" id="password" placeholder="Password" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="pass_err" style="display:none;">Please Enter Password</span>
                                                   <input  type="password" name="cpassword" id="cpassword" placeholder="Confirm password" class="form-control animatable bounceInRight">
                                                   <span class="text-danger text-center text-bold error" id="cpass_err" style="display:none;">Please Enter Confirm Password</span>

                                                   <div class="genderDiv">

                                                    <h4 class="gendar_heading">Gender</h4>

                                                  <label class="man-label">
                                            <input type="radio" name="gender" class="s_gender" class="gender" value="male">
                                            <span class="man-image"></span>
                                            <!-- <img src="<?php echo base_url('webassets/images/male_signup.png') ?>"> -->
                                          </label>

                                      <label class="woman-label">
                                       <input type="radio" name="gender" class="s_gender" class="gender" value="female">
                                        <!-- <img src="<?php echo base_url('webassets/images/female_signup.png') ?>"> -->
                                        <span class="woman-image"></span>
                                      </label>

                                                     
                                        </div>
                                                
                                                  
                                                     
                                                   
                                                    <span class="text-danger text-center text-bold error" id="s_gender_err" style="display:none;">Please Select Your Gender</span>
                                             </div>
                                             <div class="sign-up-button ">
                                                 <button type="submit" class="animatable bounceIn">Sign up</button>
                                             </div>
                                             </form>
                                          </div>

                                           <div class="vander-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft">  <span id="v-back-btn">
                                                   <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                   </span>VENDOR
                                                </h4>
                                             </div>
                                             <div class="signup-form">
                                                <form class="form-signup" action="<?php echo site_url('vendor_registeration') ?>" method="post" name="form" id="vendor_registeration" >
                                                  <span class="text-danger error invalidText" id="v_common_error" style="display: 
                                                  none;"></span>
                                                  
                                                   <input  type="text" name="firstname" placeholder="First Name" class="form-control animatable bounceInLeft" id="v_fname">
                                                   <span id="v_name_err" style="display: none;" class="text-danger invalidText"></span>

                                                    <input  type="text" name="shop_name" placeholder="Shop Name" class="form-control animatable bounceInLeft" id="v_shop_name">
                                                   <span id="v_shopname_err" style="display: none;" class="text-danger invalidText"></span>

                                                   <input  type="email" name="email" placeholder="Email Address" class="form-control animatable bounceInRight" id="v_email">
                                                   <span id="v_email_err" style="display: none;" class="text-danger invalidText"></span>
                                       
                                                     
                                                    <input  type="text" name="address" placeholder="Address" class="form-control animatable bounceInLeft" id="v_address">
                                                    <span id="v_address_err" style="display: none;" class="text-danger invalidText"></span>
                                                     <input type="hidden" name="vendor_lat" id="vendor_lat">
                                                   <input type="hidden" name="vendor_lng" id="vendor_lng">

                                                     <input type="text" class="form-control animatable bounceInLeft" placeholder="Mobile Number" name="contact_no" id="v_contact_no">
                                                     <span id="v_contact_err" style="display: none;" class="text-danger invalidText"></span>
                                                  <input  type="password" name="password" placeholder="Password" class="form-control animatable bounceInLeft" id="v_password">

                                                  <span id="v_pass_err" style="display: none;" class="text-danger invalidText"></span>

                                                  <input  type="password" name="c_password" placeholder="Confirm Password" class="form-control animatable bounceInLeft" id="v_cpassword">

                                                  <span id="v_cpass_err" style="display: none;" class="text-danger invalidText"></span>
                                                  <div class="sign-up-button ">
                                                 <button type="submit" class="animatable bounceIn">Sign up</button>
                                                  </div>

                                                  <!-- <input  type="submit" value="Sign up" class="animatable bounceIn"> -->
                                                </form>
                                             </div>
                                             <!-- <div class="sign-up-button ">
                                                <a href="#" class="animatable bounceIn">sign up</a>
                                             </div> -->
                                          </div>
                                          <div class="forget-pass-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft"> <span id="back1-btn"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>forgot password</h4>
                                             </div>
                                             <form class="form-signup" action="<?php echo site_url('home/forgotpass'); ?>" method="post" name="form" id="forgotpassForm">
                                             <div class="signup-form forget-form">
                                                <h4 class="animatable fadeInDown">Type your Email Address</h4>
                                                   <input  type="text" name="email" id="f_email" placeholder="Email Address" class="form-control animatable bounceInLeft">
                                                   <span class="text-danger text-center text-bold error" id="f_email_err" style="display:none;">Please Enter Your Email Address</span>
                                             </div>
                                             <div class="sign-up-button animatable bounceIn">
                                                <button type="submit">Send</button>
                                             </div>
                                             </form>
                                          </div>
                                          <?php /*
                                          <div class="forget-pass-area">
                                             <div class="signup-header">
                                                <h4 class="animatable bounceInLeft"> <span id="back1-btn"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>forgot password</h4>
                                             </div>
                                             <div class="signup-form forget-form">
                                                <h4 class="animatable fadeInDown">Type<br>A Password</h4>
                                                <form class="form-signup" action="" method="post" name="form">
                                                   <label class="animatable fadeInUp">Password can have 8 - 16 characters </label>
                                                   <input  type="text" name="password" placeholder="********" class="form-control animatable bounceInLeft">
                                                   <input  type="text" name="confirmpassword" placeholder="Confirm password" class="form-control animatable bounceInRight">
                                                </form>
                                             </div>
                                             <div class="sign-up-button animatable bounceIn">
                                                <a href="#">send</a>
                                             </div>
                                          </div>
                                          */ ?>
                                       </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                    
                    <?php } ?>
                    
                </div>
            </header>
