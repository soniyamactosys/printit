<?php
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
//$img_original_name = $this->session->userdata('current_order')['img_original_name'];
}

?> 

<div class="white-modals">
<div class="modal fade poster_popup" id="projectname_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('save_this_project'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="project-name-area">
         <input type="text" name="project_name" id="project_name" class="form-control" placeholder="Project Name" value="<?php echo empty($order)?'':$order['project_name']; ?>" />
         <span class="text-danger error text-center" id="projectname_err" style="display: none;">Please enter project name</span>
         </div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="button" class="btn btn-primary" data-title="" id="continue_note_order"><?php echo $this->lang->line('save_&_continue'); ?></button>
        <button type="button" class="btn btn-primary" id="saveNote"><?php echo $this->lang->line('save'); ?></button>
      </div>
    </div>
  </div>
</div> 

<div class="modal fade " id="copy_number_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('number_of_copies'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="number-area">
        <span class="minus">-</span>
        <input type="text" name="copy_number" id="copy_number" class="form-input" value="<?php echo empty($order)?'1':$order['no_of_copies']; ?>"/>
        <span class="plus">+</span>
        </div>
        <span class="text-danger error text-center" id="copy_err" style="display: none;">Please Select Number of Copies</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  id="copynumberconfirm"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<?php /*
<div class="modal fade poster_popup" id="page_range_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Select Pages</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('user/set_notes_page_range',array('id'=>'set_notes_page_range')); ?>
      <div class="modal-body">
        <div class="project-name-area">
            <div class="custom-control custom-radio">
                <input type="radio" name="all_pages" class="pages_radio custom-control-input" id="all_pages" value="all"  <?php echo (!empty($order) && $order['note_page_range']=="all")?'checked':''; ?>>
        <label class="custom-control-label" for="all_pages">All Pages</label>
      </div>
      <div class="custom-control custom-radio">
                <input type="radio" name="all_pages" class="pages_radio custom-control-input" id="page_range" value="range"  <?php echo (!empty($order) && $order['note_page_range']=="range")?'checked':''; ?>>
        <label class="custom-control-label" for="page_range"> Page Range</label>
      </div>
          <span id="note_page_error" style="display: none;" class="text-danger error"></span>      
          <div id="rangeDiv" style="display: <?php echo (!empty($order) && $order['note_page_range']=="range")?'block':'none'; ?>;">
            <label> From</label>
            <input type="number" name="page_from" id="page_from" class="form-control pagerange" placeholder="From" value="<?php echo empty($order['note_from'])?'1':$order['note_from']; ?>" />
            <label> To</label>         
            <input type="number" name="page_to" id="page_to" class="form-control pagerange" placeholder="To" value="<?php echo empty($order['note_to'])?'':$order['note_to']; ?>" />
            <span class="text-danger error text-center" id="pageto_err" style="display: none;">Please select page to</span>
            <span class="text-danger error text-center" id="pagefrom_err" style="display: none;">Please select page from</span>
          </div>
        </div>
      </div>
      <div class="modal-footer project-m-footer">
        <button type="submit" class="btn btn-primary">Confirm</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> 
*/ ?>

<div class="modal fade poster_popup" id="plz_login_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">To continue Please Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-primary" id="open_login_modal">Go To Login</button>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="colormodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
  <div class="modal-header">
    <h6 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('color'); ?></h6>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="select-area-inner">

      <select class="custom-select" id="color" >
        <option value=""><?php echo $this->lang->line('select_color'); ?></option>
        <option value="colored" <?php if(!empty($order['color']) && $order['color']=="Yes"){ echo "selected"; } ?>>Colored</option>
        <option value="Black_and_White" <?php if(!empty($order['color']) && $order['color']=="Yes"){ echo "selected"; } ?>>Black & White</option>
      </select>
    </div>
    <span class="text-danger error text-center" id="color_err" style="display: none;">Please Select Color</span>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal" id="color_confirm"><?php echo $this->lang->line('confirm'); ?></button>
  </div>
</div>
</div>
</div>
</div>

                 

<!--Step 3 Popup End  -->