<?php
$data['trans_types'] = $trans_types;
$data['language'] = $language;
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
}
//echo "<pre>"; print_r($order); exit;
?>

<div class="main_content">
    <div id="flow_option" class="translation pink_print">
      <ul>
        <li>
        <div class="custom_print_inner text-center">
        <img src="<?php echo base_url('webassets/images/Icon16.png'); ?>">
        <div class="btn_outer"></div>
        </div>
        </li>

        <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('translation_type'); ?></span></h4>
        <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#trans_typemodal'; } ?>"><img src="<?php echo base_url('webassets/images/t1.png'); ?>"> </div>
        <a href="javascript:void(0)"><h5><?php echo $this->lang->line('translation_type'); ?></h5></a>
        <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="t_type" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['trans_type'])?'':$order['trans_type']; ?></h6>
        </li>

        <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('from'); ?></span></h4>
          <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#langfrom_modal'; } ?>"><img src="<?php echo base_url('webassets/images/t2.png'); ?>"> </div>
          <a href="javascript:void(0)"><h5><?php echo $this->lang->line('from'); ?></h5></a>
          <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="langfrom" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['lang_from'])?'':$order['lang_from']; ?></h6>
        </li>
        <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('to'); ?></span></h4>
          <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#langto_modal'; } ?>"><img src="<?php echo base_url('webassets/images/t2.png'); ?>"> </div>
          <a href="javascript:void(0)"><h5><?php echo $this->lang->line('to'); ?></h5></a>
          <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="langto" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['lang_to'])?'':$order['lang_to']; ?></h6>
        </li>
        <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('document'); ?></span></h4>
          <div class="img_box"  data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#imagemodal'; } ?>"><img src="<?php echo base_url('webassets/images/t3.png'); ?>"> </div>
          <a href="javascript:void(0)"><h5><?php echo $this->lang->line('document'); ?></h5></a>
          <h6 class="paper_type <?php echo empty($order)?'pdesign':'pdesign1';  ?>" id="pdesign" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['doc_org_name'])?'':$order['doc_org_name']; ?></h6>
        </li>
        <li class="process_flow"><div class="line"></div><h4><span>Print Type</span></h4>
          <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#trans_print_copy_modal'; } ?>"><img src="<?php echo base_url('webassets/images/t1.png'); ?>"> </div>
          <a href="javascript:void(0)"><h5>Print Type</h5></a>
          <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="copyType" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['print_copy_type'])?'':$order['print_copy_type']; ?></h6>
        </li>
        <li data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal_pink'; }else{ echo '#projectname_modal'; } ?>" ><span><?php echo $this->lang->line('print_br_it'); ?></span></li>
        
        
        </ul><!-- ul end here -->

                            




</div>

</div>
                          
<?php $this->load->view('web/translation_modals',$data); ?>               