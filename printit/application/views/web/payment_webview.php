<?php 
require('./application/controllers/PaymentController.php'); 
$paymentController = new PaymentController();
$paymentController->formSubmit();
$date = new DateTime();
?>
<form action="" class="form" method="post" id="paymentForm">
<input type="hidden" name="merchantCode" value="<?php echo Constantspay::MERCHANT_CODE;?>">
<input type="hidden" name="amount" value="<?php echo $order_total; ?>">
<input type="hidden" name="currency" value="KWD">
<input type="hidden" name="paymentType" value="0" checked> 
<input type="hidden" name="responseUrl" value="<?php echo Constantspay::RESPONSE_URL_APP;?>">
<input type="hidden" name="failureUrl" value="<?php echo Constantspay::FAILURE_URL_APP;?>">
<input type="hidden" name="orderReferenceNumber" value="<?php echo $date->getTimestamp(); ?>">
<input type="hidden" name="version" value="<?php echo Constantspay::VERSION ?>">
<input type="hidden" name="variable1" value="<?php echo $id; ?>">

<input type="submit" name="submit" id="checkout" class="btn blue-btn checkout" value="checkout" style="display:none" >
<?php echo form_close();  ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
window.onload = function(){
$('input[type="submit"]').trigger('click');
}
</script> 