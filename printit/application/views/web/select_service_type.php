<div class="main_content">
<div class="services_type">
<h1>Select Service Type  </h1>
<div class="select_services_inner">
        <a href="<?php echo site_url('set_service_type/express'); ?>">
        <div class="product_box">
          <div class="row">
            <div class="col-md-5 col-sm-5 col-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printit-logo.png'); ?>" class="mCS_img_loaded"></div></div>
            <div class="col-md-7 col-sm-7 col-7"><div class="product_content">
              <h4>Print-it Express </h4>
              <h2>Service Provided by Print-it App with delivery option only</h2>
            </div></div>
          </div>
        </div>
        </a>
        <a href="<?php echo site_url('set_service_type/normal'); ?>">
        <div class="product_box">
          <div class="row">
            <div class="col-md-5 col-sm-5 col-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printt-house.png'); ?>" class="mCS_img_loaded"></div></div>
            <div class="col-md-7 col-sm-7 col-7"><div class="product_content">
              <h4>Local Printeries</h4>
              <h2>Service by your favorite printery That is available for delivery or Pickup </h2>
            </div></div>
          </div>
        </div>
        </a>
</div>
</div>

