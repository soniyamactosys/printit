<?php //echo "<pre>"; print_r($footer_data);
 ?>
<footer>
<ul>
    <li><a href="#"><?php echo $this->lang->line('©_2021_printit');  ?> </a></li>
    <li><a href="#"><?php echo $this->lang->line('contact_us');  ?></a></li>
    <li><a href="#"><?php echo $this->lang->line('about');  ?></a></li>
    <li><a href="#"><?php echo $this->lang->line('terms_&_conditions');  ?></a></li>
    <li><a href="#"><?php echo $this->lang->line('privacy');  ?></a></li>
<li class="footer-insta-icon"><a href="#"><img src="<?php echo base_url('webassets/images/insta-icon.png'); ?>"></a></li>    
</ul>
</footer>
</div>
</div>
<?php
$data = array();
if(!empty($footer_data['in_progress_orders'])){
  $this->data['in_progress_orders'] = $footer_data['in_progress_orders'];
}
$this->load->view('web/common_modals',$this->data); ?>
<script type="text/javascript" src="<?php echo base_url('webassets/js/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('webassets/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('webassets/js/custom.js'); ?>"></script>

<script src="<?php echo base_url('webassets/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/loaderjs.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('webassets/js/scustom.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('webassets/js/order.js'); ?>"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
<script>  
var site_url = '<?php echo site_url(); ?>';
function goBack(ds){
    if($(ds).attr('href')==""){
        window.history.back();
    }else{
        window.location.href = $(ds).attr('href');
    }
}

    function notification() {
        $.ajax({
            url: site_url+'user/notification',
            dataType:'json',
            beforeSend:function(){
            },
            success: function(res) {
                if(res.success){
                    $('#dotnotify').css('display','block');
                }else{
                    $('#dotnotify').css('display','none');
                }
            }
        });        
    }

<?php 
if(is_null($this->session->userdata('service_type'))){ ?>
$('#service_type_modal').modal('show');
<?php }
if(!empty($this->session->userdata['userdata'])){ ?>

notification();

setInterval(function(){
notification();
},30000);
<?php } ?>
</script>

  <!--*************** option jquery  ***************     -->

 <script>
 var myel = jQuery('#custom_select_icon').find('.select-options li');
 var k = 0;
$(myel).each(function(i,v){
    //
    if(i>0){
    $(this).append('<span class="infor-tooltip" id="pt_op'+k+'"></span>');
    k++;
    }
});
    </script>
 <?php 
 //echo "<pre>"; print_r($papertype);
 if(!empty($papertype)){ foreach($papertype as $key=>$type){ ?>
        <script>
        
        $(document).find('<?php echo '#pt_op'.$key; ?>').append('<i class="fa fa-info-circle" aria-hidden="true" ></i><span class="infore_hover"><?php echo $type['description']; ?></span>');
          
        </script>
         <?php } }  ?>
  <!--*************** option jquery  ***************     -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc3cw_q_7kjC_CKRhqV1t2KFQxuM1R-gU&libraries=places"></script>
    
<script type="text/javascript">
	

function user_register() {
  var input = document.getElementById('u_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('user_lat').value = place.geometry.location.lat();
        document.getElementById('user_lng').value = place.geometry.location.lng();
    });
}

if($('#u_address').length >0){
    google.maps.event.addDomListener(window, 'load', user_register);
}

function vendor_register() {
  var input = document.getElementById('v_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('vendor_lat').value = place.geometry.location.lat();
        document.getElementById('vendor_lng').value = place.geometry.location.lng();
    });
}
if($('#v_address').length >0){
    google.maps.event.addDomListener(window, 'load', vendor_register);
}

function profileAddress() {
  var input = document.getElementById('profile_address');
   console.log('input',input);
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        console.log('ss',input);
        var place = autocomplete.getPlace();
         console.log('a',place.name);
        //document.getElementById('user_address').value = place.name;
        document.getElementById('prof_lat').value = place.geometry.location.lat();
        document.getElementById('prof_long').value = place.geometry.location.lng();
    });
}
if($('#profile_address').length >0){
    google.maps.event.addDomListener(window, 'load', profileAddress);
}

function order_address_f() {
  var input = document.getElementById('pac-input');
   console.log('input',input);
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('order_longitude_f').value = place.geometry.location.lat();
        document.getElementById('order_latitude_f').value = place.geometry.location.lng();
    });
}
if($('#pac-input').length >0){
    google.maps.event.addDomListener(window, 'load', order_address_f);
}
/*
function edit_address(){
     console.log('h');
  var input = document.getElementById('edit_add');
  var autocomplete = new google.maps.places.Autocomplete(input);
  console.log('ab');
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
  console.log('lat',place.geometry.location.lat());
  console.log('lng',place.geometry.location.lng());
        document.getElementById('edit_latitude').value = place.geometry.location.lat();
        document.getElementById('edit_longitude').value = place.geometry.location.lng();
    });    
}
if($('#edit_add').length >0){
    console.log('s');
    google.maps.event.addDomListener(window, 'load', edit_address);
}*/

</script>    


<style>  
.rotate90 {  
  -webkit-transform:rotate(90deg);  
  -moz-transform: rotate(90deg);  
  -ms-transform: rotate(90deg);  
  -o-transform: rotate(90deg);  
  transform: rotate(90deg);  
}  

.rotate270 {  
  -webkit-transform:rotate(270deg);  
  -moz-transform: rotate(270deg);  
  -ms-transform: rotate(270deg);  
  -o-transform: rotate(270deg);  
  transform: rotate(270deg);  
} 
.rotate-180 {  
  -webkit-transform:rotate(-180deg);  
  -moz-transform: rotate(-180deg);  
  -ms-transform: rotate(-180deg);  
  -o-transform: rotate(-180deg);  
  transform: rotate(-180deg);  
}  
 
</style>  
</body>
</html>