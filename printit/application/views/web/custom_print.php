
<div class="main_content">

   <div class="custom_print text-center all_custom_print">
    <div class="row">
      <?php if(!empty($print_types)){ foreach($print_types as $type){ 
      ?>
      <div class="col-md-6">
        <div class="custom_print_inner">
          <a href="<?php echo site_url('print-type/').$type['url']; ?>">
            <img src="<?php echo base_url('print_type_images/').$type['image']; ?>">
            <div class="btn_outer"><span class="btn"><i class="fas fa-arrow-right"></i></span></div>
          </a>
          <h2><?php echo ucfirst($type['name']); ?></h2>
        </div>
      </div>
      <?php } } ?>
    </div>
   </div>
           
</div>
                           