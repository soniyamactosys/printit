<!doctype html>
<html lang="en" dir="<?php echo ($this->session->userdata('site_lang')=='arabic')?'rtl':'ltl'; ?>" >
        
<head>
<title>Print It</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<?php echo base_url('webassets/css/all.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('webassets/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php //echo base_url('webassets/css/animation.css'); ?>" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('webassets/css/style.css'); ?>">
<?php if($this->session->userdata('site_lang')=='arabic'){ ?>
<link rel="stylesheet" href="<?php echo base_url('webassets/css/arabic_style.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('webassets/css/arabic_responsive.css'); ?>">
<?php }else{ ?>

<?php } ?>
<link rel="stylesheet" href="<?php echo base_url('webassets/css/responsive.css'); ?>">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url('webassets/css/jquery.mCustomScrollbar.css') ?>" /><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
</head>
<body class="<?php echo ($this->session->userdata('site_lang')=='arabic')?'arabic_active':''; ?>">
<input type="hidden" name="current_url" id="current_url" value="<?php echo current_url(); ?>">	
