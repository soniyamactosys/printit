<?php
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
$img_original_name = $this->session->userdata('current_order')['img_original_name'];
}else{
$img_original_name = ''  ;
}

//echo "<pre>"; print_r($_SESSION); exit;
//print_r($order); exit;
?> 
<div class="modal fade poster_popup" id="papertypemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('paper_type'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="select-area-inner" id="custom_select_icon">
        <select class="custom-select selectpicker " name="paper_type" id="paper_type" onmousemove="showIETooltip();" onmouseout="hideIETooltip();">
        <option value="" ><?php echo $this->lang->line('select_paper_type'); ?></option>
        <?php if(!empty($papertype)){ foreach($papertype as $type){ ?>
        <option title="Option #1" value="<?php echo $type['id']; ?>" <?php echo (!empty($order) && $order['paper_type']==$type['id'])?'selected':''; ?> ><?php echo $type['name']; ?></option>

        <?php } } ?>
       
      </select>
     
    </div>
      <span class="error text-danger text-center" id="type_error"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary poster_type_confirm" data-title="paper_type" id="paperTypeConfirm"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="papersizemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('paper_size'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="select-area-inner">
        <select class="custom-select" name="paper_size" id="paper_size">
        <option value=""><?php echo $this->lang->line('select_paper_size'); ?></option>
        <?php if(!empty($papersize)){ foreach($papersize as $type){ ?>
        <option value="<?php echo $type['id']; ?>" <?php echo (!empty($order) && $order['paper_size']==$type['id'])?'selected':''; ?> ><?php echo $type['name']; ?></option>
        <?php } } ?>
      </select>
    </div>
      <span class="error text-danger text-center" id="size_error"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-title="paper_type" id="papersizeconfirm"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content uploadarea">
      <?php echo form_open_multipart('user/upload_design',array('id'=>'imageuploadform')); ?>
      <input type="hidden" name="image_rotation" id="image_rotation">
      <div class="modal-header">
       <div class="fileupload_name"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>  <?php echo $this->lang->line('design'); ?>   
        <span id="uploadimgbtn" style="display:<?php echo empty($order)?'block':'none'; ?>" ><i class="fas fa-check"></i></span> </span>
        </div>
       
      </div>
      <div class="modal-body">
        <span id="design_err" class="text-danger error" style="display: none;">Please Upload image first</span>
        <div class="upload_box">
          <!-- <div class="photo_upload"><input type="file" id="graph_design_one" name="graph_design_one" class="input_photoupload" accept=".jpg,.jpeg,.png" ><img src="<?php //echo base_url('webassets/images/Camera.png'); ?>"></div> -->

          <div class="file_upload" id="fileDiv" style="display:<?php echo empty($order)?'':'none' ; ?>" ><input type="file" id="graph_design_two" name="graph_design_two" class="input_fileupload" accept=".jpg,.jpeg,.png"><img src="<?php echo base_url('webassets/images/Upload.png'); ?>"> <?php echo $this->lang->line('upload'); ?></div>
          <span id="msgSpan" style="display:<?php echo empty($order)?'':'none'; ?>">Max File Size <br> 20MB</span>
          <div class="file_remove" id="removeDiv" style="display:<?php echo empty($order)?'none':'block'; ?>"><span class="fa fa-remove"></span> <?php echo $this->lang->line('remove'); ?></div>
          <span id="filenameSpan" style="display:<?php echo empty($order)?'none':'block'; ?>"><?php echo empty($order)?'':$img_original_name; ?></span>
        </div>
        <div class="design_upload" id="uploadImagePreview" style="display:<?php echo empty($order)?'':'none' ; ?>" >
          <input type="file" class="input_designupload">
          <h3><?php echo $this->lang->line('upload_your_br_design'); ?></h3>
          <img class="plusicon" src="<?php echo base_url('webassets/images/Cover.png'); ?>">
        </div>
        <div class="design_upload" id="imagePreview" style="display:<?php echo empty($order)?'none':'block'; ?>;" >
          <?php if(!empty($order)){ ?>
            <img src="<?php echo base_url('order_uploads/').$order['image_uploaded']; ?>">
          <?php } ?>
        </div>
        <div class="upload_setting">
          <ul>
            <li class="active" id="verti"><img src="<?php echo base_url('webassets/images/Ver.png'); ?>"> <?php echo $this->lang->line('vertical'); ?></li>
             <li id="hori"><img src="<?php echo base_url('webassets/images/Hor.png'); ?>"> <?php echo $this->lang->line('horizontal'); ?> </li>
             <!--  <li><img src="<?php echo base_url('webassets/images/Move.png'); ?>"> Move</li>
               <li><img src="<?php echo base_url('webassets/images/Scale.png'); ?>"> Scale</li> -->
          </ul>
        </div>

      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="copy_number_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><?php echo $this->lang->line('number_of_copies'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                   <div class="number-area">
                      <span class="minus">-</span>
                      <input type="text" name="copy_number" id="copy_number" class="form-input" value="<?php echo empty($order)?'1':$order['no_of_copies']; ?>"/>
                      <span class="plus">+</span>
                    </div>
                     <span class="text-danger error text-center" id="copy_err" style="display: none;">Please Select Number of Copies</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-title="" id="copynumberconfirm"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="projectname_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content input-number-area input-project-area">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Save this project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="project-name-area">
         <input type="text" name="project_name" id="project_name" class="form-control" placeholder="Project Name" value="<?php echo empty($order)?'':$order['project_name']; ?>" />
         <span class="text-danger error text-center" id="projectname_err" style="display: none;">Please enter project name</span>
         </div>
      </div>
      
      <div class="modal-footer project-m-footer">
        <button type="button" class="btn btn-primary" data-title="" id="saveprojectbtn">Save & Continue</button>
        <button type="button" class="btn btn-primary" id="saveproject">Save</button>
      </div>
    </div>
  </div>
</div> 

<div class="modal fade poster_popup" id="printingsidemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step3Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Print Side</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="select-area-inner">
          <select class="custom-select" name="printing_side" id="printing_side">
            <option selected value="">Select Paper Side</option>
            <option value="single">Single Side</option>
            <option value="double">Double Side</option>
          </select>
        </div>
        <span class="text-danger error text-center" id="printside_err" style="display: none;">Please Select Printing Side</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirmPrintingSide"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="banner_width_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Banner Width</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="select-area-inner">
            <select class="custom-select" name="banner_size" id="banner_size">
              <option selected value="">Select Banner Size</option>
              <?php if(!empty($banner_size)){ foreach($banner_size as $type){ ?>
              <option value="<?php echo $type['id']; ?>"><?php echo $type['name']; ?></option>
              <?php } } ?>
            </select>
          </div>
          <span class="error text-danger text-center" id="width_err"></span>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="bannerWidthConfirm"><?php echo $this->lang->line('confirm'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="plz_login_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">To continue Please Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-primary" id="open_login_modal">Go To Login</button>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<style type="text/css">
.file_remove{  
  display: inline-block;
  border: solid 1px #F44336;
  background: rgba(41, 149, 204, 0.3);
  position: relative;
  border-radius: 60px;
  width: 113px;
  color: #F44336;
  height: 50px;
  text-align: center;
  line-height: 46px;
  cursor: pointer;
  font-size: 14px;
  padding-right: 7px;
  margin: 0 10px;
  float: left;
}
</style>