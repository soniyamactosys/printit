<?php 
$userdata = $this->session->userdata('userdata');
$selected_printry = $this->session->userdata('selected_printry'); 
$currentorder = $this->session->userdata('current_order');
$vendor_charge = $this->session->userdata('vendor_charge');
$coupon_applied = $this->session->userdata('coupon_applied');
$pickup_delivery = $this->session->userdata('pickup_delivery');
$order_products = $this->session->userdata('order_products');

$oid =  $this->session->userdata('continue_order');
$service_fee = $this->session->userdata('service_fee');
$prev_url = get_order_prev_step($oid,current_url());
$order_total = floatval($vendor_charge)+floatval($service_fee);
$prod_total=0;
//$total = $vendor_charge;
if(!empty($order_products)){
  $prod_total = get_prodTotal($order_products);
  $order_total += $prod_total;
}
$delivery_fee = get_delivery_charges(2);
$min_deliver_charge = $this->session->userdata('min_delivery_amount');
// echo $service_type ."<br>".$service_fee;
// echo $this->db->last_query();
// exit;
// echo $total;
// exit;
// if(!empty($coupon_applied)){
//   $total
// }

?>
 <style>
    .pac-container {
    z-index: 10000 !important;
    }
  </style>
<input type="hidden" id="service_type" value="<?php echo $currentorder['service_type']; ?>">
<input type="hidden" name="orderamt" id="orderamt" value="<?php echo $order_total; ?>">
<input type="hidden" name="vendorTotal" id="vendorTotal" value="<?php echo $vendor_charge; ?>">
<input type="hidden" name="service_fee" id="service_fee" value="<?php echo $service_fee; ?>">
<input type="hidden" name="pick_delivery" id="pick_delivery" value="<?php if(!empty($pickup_delivery)){ echo $pickup_delivery; } ?>">
<input type="hidden" name="delivery_min_limit" id="delivery_min_limit" value="<?php echo $min_deliver_charge;  ?>">
<!--<input type="text" name="address" id="order_addrress_anusss" class="form-control" placeholder="Address">-->
                            
            <div class="main_content no-padding">
               <div class="order_detail_outer">
                  <div class="order_detail_inner">
                     <a href="javascript:void(0)" value="pickup" class="pickbtn btn <?php if(!empty($pickup_delivery) && $pickup_delivery=='pickup'){ echo 'blue-btn'; }else{ echo 'dark-btn'; } ?>">Pick up</a>
                     <a href="javascript:void(0)" value="delivery" class="pickbtn btn <?php if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){ echo 'blue-btn'; }else{ echo 'dark-btn'; } ?>">Delivery <span>KD <?php echo $min_deliver_charge; ?> <?php echo $this->lang->line('Minimum_order'); ?></span></a>
                     <h2><?php echo $selected_printry['shop_name']; ?></h2>
                     <h3><?php 
                      $fullAddress = '';
                    // if(!empty($selected_printry['po_box'])){
                    //   $fullAddress .= $selected_printry['po_box'].'<br>';
                    // } 

                    // if(!empty($selected_printry['block'])){
                    //   $fullAddress .= $selected_printry['block'].'<br>';
                    // }

                    // if(!empty($selected_printry['address'])){
                    //   $fullAddress .= $selected_printry['address'].'<br>';
                    // }
                    if(!empty($selected_printry['building'])){
                      $fullAddress .= $selected_printry['building'].'<br>';
                    }
                     echo $fullAddress;
                     ?></h3>
                     <?php /*
                     <img src="<?php echo base_url('webassets/images/map.jpg'); ?>">
                     */
 echo '<div class="mapouter"><div class="gmap_canvas"><iframe src = "https://maps.google.com/maps?q='.$selected_printry['latitude'].','.$selected_printry['longitude'].'&hl=es;z=14&amp;output=embed"></iframe></div></div>';
                      ?>

                  </div>

               </div>
                <span id="pick_error" class="text-danger text-center" style="display: none;">Please Select Pick up Or Delivery option </span>
               <div class="product-next-btn">
                <?php 
                $nextlink = "javascript:void(0)";
                if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){

                  if(empty($order_products)){
                    $nextlink = site_url("payment");
                  }else{
                    $nextlink = "javascript:void(0)";
                  }
                  
                }elseif(!empty($pickup_delivery) && $pickup_delivery=='pickup'){
                  $nextlink = site_url("payment");
                }

                 ?>

<?php /*                  
<a  class="print-prev-btn pic-prev-btn" href="<?php echo $prev_url; ?>" id=""> <span><i class="fa fa-arrow-left" aria-hidden="true"></i></span>PREV</a>
<a href="<?php echo $nextlink; ?>" class="orderstep2">NEXT <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
*/ ?>
               
   <div class="pro-select-box">
    <?php /*
    if(!empty($order_products)){ ?>
  <a href="javascript:void(0);" data-toggle="modal" data-target="#more_product" class="">View selected products </a>
<?php } 
if(!empty($pickup_delivery) && $pickup_delivery=='delivery'){ ?>
  <a href="javascript:void(0);" data-toggle="modal" data-target="#address1"  class="">view selected address </a>            
  <?php }*/ ?>
  </div>
</div>
</div>
            

      <div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade model-hide" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header account-pro">
                        <div class="account_name text-right proposal-acount"><?php echo $userdata['name']; ?><img src="<?php echo base_url('webassets/images/user.png'); ?>"></div>
                     </div>
                     <div class="modal-body">
                        <div class="product-timeline">
                           <ul class="timeline">
                              <li class="active-tl"></li>
                              <li class="active-tl"></li>
                              <li></li>
                           </ul>
                        </div>
                        <div class="main-trasletion-box">
                           <div class="transletion-area">
                              <div class="row">
                                 <div class="col-sm-6 right-padding">
                                    <div class="trans-box-left">
                                       <h4>
                                      <?php if($currentorder['order_type']==1){
                                         echo $this->lang->line('custom_print');
                                          $img = 'Custom-print.png';
                                       }elseif($currentorder['order_type']==2){
                                           echo $this->lang->line('quick_print');
                                          $img = '3.png';
                                       }elseif($currentorder['order_type']==3){
                                           echo $this->lang->line('translation');
                                          $img = '4.png';
                                       }elseif($currentorder['order_type']==4){
                                          echo $this->lang->line('notes');
                                          $img = '5.png';
                                       }else{
                                         echo $this->lang->line('notes');
                                       }
                                       ?> 
                                       
                                        <span><img src="<?php echo base_url('webassets/images/').$img; ?>"></span></h4>
                                       <p><?php echo $currentorder['project_name']; ?></p>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 left-padding">
                                    <div class="trans-box-right">
                                       <h4><?php echo $this->lang->line('total'); ?></h4>
                                       <p  class="totalAmt"><?php echo format_currency($order_total); ?></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="order_detail_list">
                              <h2><?php echo $this->lang->line('order_details'); ?></h2>
                              <table class="table" id="o_detailDiv">
                                 <tr>
                                   <th>
                                  <?php 
                                    if($currentorder['order_type']==1){
                                    echo $this->lang->line('custom_print');
                                    }elseif($currentorder['order_type']==2){
                                    echo $this->lang->line('quick_print');
                                    }elseif($currentorder['order_type']==3){
                                    echo $this->lang->line('translation');
                                    }elseif($currentorder['order_type']==4){
                                    echo $this->lang->line('notes');
                                    }
                                  ?>
                                 </th>
                                 <td><?php echo format_currency($vendor_charge); ?></td>
                                  </tr>

                                  <tr id="service_fee_head"><th><?php echo $this->lang->line('service_fees'); ?></th><td><?php echo format_currency($service_fee); ?></td></tr>
                                  <tr id="delivery_fee_head" style="display: none;"><th><?php echo $this->lang->line('Delivery_Fees'); ?></th><td id="delivery_fee_val"></td></tr>
                                  <?php if(!empty($extra_product)){ foreach($extra_product as $prod){ ?>
                                  <tr><th>Pin</th><td>1KD</td></tr>
                                   
                                  
                                  <?php } }  ?>
                                   
                                 <tfoot>
                                    <tr><th><?php echo $this->lang->line('total'); ?></th><td id="totalAmtDiv" class="totalAmt"><?php if(!empty($order_total)){ echo format_currency($order_total); }else{ echo '0 KD'; } ?> </td></tr>
                                 </tfoot>
                              </table>
                           </div>
                           <div class="promotion-code-area" id="promocodeArea">
                              <h4><?php echo $this->lang->line('Enter_Promotion_Code'); ?></h4>
                              <input type="text" id="promo_code_input"   class="form-control" placeholder="1234567">
                             
                              <span class="promo_apply_btn"><?php echo $this->lang->line('Apply'); ?></span>
                           </div>
                           <span id="promo_code_msg"></span>
                           <span id="promo_code_err" class="text-center text-danger"></span>
                           <div class="product-next-btn model-next-btn">
                               <a href="<?php echo $nextlink; ?>" class="orderstep2"><?php echo $this->lang->line('NEXT'); ?> <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

<div class="modal bottom fade animate" id="more_product" tabindex="-1" role="dialog" aria-labelledby="more_product">
  <div class="modal-dialog" role="document">
  <div class="modal-content animate-bottom">
  <div class="modal-header">
      <?php if(floatval($currentorder['vendor_charge'])<floatval($min_deliver_charge)){ ?>

  <h5 class="modal-title">
Your  total Order Amount is less than <?php echo $min_deliver_charge; ?> KD. So Please select products from below stationary to reach or exceed this Total Order Amount
</h5>
<?php } ?>
  <h2>Order Amount <span id="total_inmodal"><?php echo $vendor_charge; ?> KD</span></h2>
  <h2>Product Total <span id="prod_total" val="<?php echo $prod_total; ?>"><?php echo $prod_total; ?> KD</span></h2>

  </div>
  <div class="modal-body">
    <?php echo form_open('user/save_products',array('id'=>'save_order_products')); ?>
    <h3><?php echo $this->lang->line('Add_More_Products'); ?></h3>
    <div id="more_product_list" class="content">
     <ul class="moreproduct_ul">
      <?php


$op = array();
if(!empty($order_products)){ foreach($order_products as $k=>$v){
  $j = array_search($k, array_column($products, 'id'));
  if($j===false){ 

    $op[$k] = $order_products[$k];
  }
}}
// echo "<pre>"; print_r($op);
// exit; 
      if(!empty($op)){ foreach($op as $key=>$value){ ?>
        <li class="active" >
           <div class="more_product_box">
              <input type="hidden" name="name[<?php echo $key; ?>]" value="<?php echo $value['name']; ?>" >
              <input type="hidden" name="price[<?php echo $key; ?>]" value="<?php echo $value['price']; ?>" >
              <input type="checkbox" class="product prod_checkbox" name="products[<?php echo $key; ?>]" value="<?php echo $key;  ?>" price="<?php echo $value['price'];  ?>" checked already="<?php echo $value['qty'];  ?>">

              <div class="more_product_img more-product-area"><img src="<?php echo base_url('products'
                ).'/'.$value['image'];?>"></div>
              <div class="more_product_content more-content-area">
                <div class="quant-btn">
                  <div class="number">
                      <span style="font-size: smaller;" class="qtyerror text-danger"></span>
                      <span class="prodMinus">-</span>
                      <input type="text" class="qty" name="qty[<?php echo $key; ?>]" value="<?php echo $value['qty']; ?>" >
                      <span class="plusProd">+</span>
                  </div>
                </div>
                <div class="prodContent" >
                  <h4><?php echo $value['name']; ?></h4><h6><?php echo $value['price']; ?> KD</h6>
                </div>
            </div>
           </div>
        </li>
      <?php } }
        
       if(!empty($products)){ foreach($products as $value){ 
          $key= $value['id'];
          if(!empty($order_products) && array_key_exists($key,$order_products)){
            //$sess_prod = $order_products[$key];
            $active = 'active';
            $qty = $order_products[$key]['qty'];
          }else{
            $active = '';
            $qty = 0;
          }
        ?>
        <li class="<?php echo $active; ?>" >
           <div class="more_product_box">
              <input type="hidden" name="name[<?php echo $key; ?>]" value="<?php echo $value['name']; ?>" >
              <input type="hidden" name="price[<?php echo $key; ?>]" value="<?php echo $value['price']; ?>" >
              <input type="checkbox" class="product prod_checkbox" name="products[<?php echo $key; ?>]" value="<?php echo $value['id'];  ?>" price="<?php echo $value['price'];  ?>" <?php if(!empty($active)){ echo 'checked'; } ?> already="<?php echo $qty;  ?>" >

        <!--       <input type="text" name="qty[<?php echo $key; ?>]" value="<?php echo $qty; ?>" > -->


              <div class="more_product_img more-product-area"><img src="<?php echo base_url('products'
                ).'/'.$value['image'];?>"></div>
              <div class="more_product_content more-content-area">
                <div class="quant-btn">
                  <div class="number">
                      <span style="font-size: smaller;" class="qtyerror text-danger"></span>
                      <span class="prodMinus">-</span>
                      <input type="text" class="qty" name="qty[<?php echo $key; ?>]" value="<?php echo $qty; ?>" >
                      <span class="plusProd">+</span>
                  </div>
                </div>
                <div class="prodContent" >
                  <h4><?php echo $value['name']; ?></h4><h6><?php echo $value['price']; ?> KD</h6>
                </div>
            </div>
           </div>
        </li>
      <?php } } ?>
     </ul>

     </div>
      <div class="next-btn text-center">
        <span id="prod_err" class="text-center text-danger" style="display: none; padding-top: 15px;"></span>
      
        <input type="submit" name="submit" value="<?php echo $this->lang->line('NEXT'); ?>" class="nest-button">
      </div>
      <?php echo form_close(); ?>
  </div>

  </div>
  </div>
</div>


 <div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade model-hide" data-backdrop="static" data-keyboard="false" id="address2" tabindex="-1" role="dialog" aria-labelledby="address2">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                     <button type="button" class="close" id="cancelNewAddress" >
                      <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
                    </button> 
                       <h3><?php echo $this->lang->line('Add_New_Address'); ?></h3>
                     </div>
                      <div class="modal-body">
                         
                           <span  class="address_field_error" >Fields are mandatory *</span>
                         
                        <?php echo form_open('user/add_new_address',array('id'=>'new_order_address')); ?>
                       
                        <div class="form-group">
                        <input type="text" name="label" class="form-control" placeholder="Address Nickname">
                        </div>

                        <div class="form-group">
                            <input id="pac-input" name="address" type="text" placeholder="Area*" class="form-control new_add_input" autocomplete="off">
                             <input type="hidden" name="longitude" id="order_longitude_f">
                            <input type="hidden" name="latitude" id="order_latitude_f">
                            <span class="text-danger text-center error" id="o_add_err" style="display: none;">Please add area</span>

                          </div> 

                        <div class="form-group">
                            <!--<label>address_type*</label>-->
                            <select name="address_type" class="form-control" id="address_type">
                                <option value="">Address Type*</option>
                                <option value="home">Home</option>
                                <option value="appartment">Apartment</option>
                                <option value="office">Office</option>
                            </select>
                            <span id="add_type_err" class="text-danger text-center error" style="display: none;">Please select address type</span>
                        
                        </div>
                        
                           <div class="form-group">
                            <input type="text" name="block" id="block" class="form-control" placeholder="block*">
                            <span id="add_block_err" class="text-danger text-center error" style="display: none;">Please select block</span>
                            </div>
                            
                           <div class="form-group">
                            <input type="text" name="street" id="street" class="form-control" placeholder="Street*">
                            <span id="add_street_err" class="text-danger text-center error" style="display: none;">Please enter street</span>
                            </div>
                           <div class="form-group">
                            <input type="text" name="avenue" id="avenue" class="form-control" placeholder="Avenue">
                            <!--<span id="add_avenue_err" class="text-danger text-center error"></span>-->
                            
                            </div>
                            
                            <div class="form-group extra_options" id="houseDiv" style="display:none;">
                            <input type="text" name="house" id="house" class="form-control" placeholder="House*">
                            <span id="add_house_err" class="text-danger text-center error" style="display: none;">Please enter house</span>
                            
                            </div>
                            
                           <div class="form-group extra_options" id="buildingDiv" style="display:none;">
                            <input type="text" name="building" id="building" class="form-control" placeholder="Building*">
                            <span id="add_building_err" class="text-danger text-center error" style="display: none;">Please enter building</span>
                            </div>
                            
                           <div class="form-group extra_options" id="floorDiv" style="display:none;">
                            <input type="text" name="floor" id="floor" class="form-control" placeholder="Floor*">
                            <span id="add_floor_err" class="text-danger text-center error" style="display: none;">Please enter floor</span>
                            </div>
                            
                           <div class="form-group extra_options" id="apartmentDiv" style="display:none;">
                            <input type="text" name="apartment" id="apartment" class="form-control" placeholder="Apartment No. *">
                            <span id="add_apt_err" class="text-danger text-center error" style="display: none;">Please enter apartment no.</span>
                            </div>
                            
                            <div class="form-group extra_options" id="officeDiv" style="display:none;">
                            <input type="text" name="office" id="office" class="form-control" placeholder="Office*">
                            <span id="add_office_err" class="text-danger text-center error" style="display: none;">Please enter office</span>
                            </div>
                            
                          <div class="form-group">
                            <textarea  name="additional_directions" class="form-control" placeholder="Additional Directions"></textarea></div>
                           <div class="form-group">
                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile Number*">
                            <span id="add_mobile_err" class="text-danger text-center error" style="display: none;">Please enter mobile number</span>
                            
                            </div>
                            
                           <div class="form-group extra_options" id="landlineDiv" style="display:none">
                            <input type="text" name="landline" class="form-control" placeholder="Landline Number"></div>
 
                        <div class="address_action">
                           <button type="submit" class="btn blue-btn"><?php echo $this->lang->line('confirm'); ?></button>
                        </div>
                        <?php echo form_close(); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal <?php echo ($this->session->userdata('site_lang')=='arabic')?'left':'right'; ?> fade model-hide"  id="address1" tabindex="-1" role="dialog" aria-labelledby="address1" data-backdrop="static" data-keyboard="false" >
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <?php echo form_open('user/set_address',array('id'=>'address_option_form')); ?>
                     <div class="modal-header">
                      <?php //if(!empty($currentorder['delivery_address'])){ ?>  
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true"> <i class="fas fa-times"></i> </span>
                    </button> 
                       <?php //} ?>
                     </div>
                      <div class="modal-body">
                        <h3><?php echo $this->lang->line('Select_Delivery_Address'); ?></h3>
                        <ul>
                            <?php if(!empty($my_address_options)){ foreach($my_address_options as $value){  $active = empty($currentorder['delivery_address'])?'':($value['id']==$currentorder['delivery_address'])?'active':''; ?>
                            <li><div class="radio_address <?php echo $active;  ?>"><label for="<?php echo 'add_radio'.$value['id']; ?>"><?php if(!empty($value['label'])){ echo $value['label']; }else{ echo ucfirst($value['address_type']); } ?> </label>
                            <input type="radio" class="address_option" value="<?php echo $value['id']; ?>" name="address_option" id="<?php echo 'add_radio'.$value['id']; ?>" <?php if(!empty($currentorder['delivery_address']) && $value['id']==$currentorder['delivery_address']){ echo 'checked'; } ?> ></div></li>
                            <?php } } ?>
                        </ul>
                        

                        <div class="address_action">
                          <span class="text-danger text-center" style="display: none;" id="address_option_err">Please Select address</span>
                           <a href="javascript:void(0)" class="btn dark-btn"  data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#address2"><?php echo $this->lang->line('New_Address'); ?></a>
                           <input type="submit" class="btn blue-btn" name="submit" value="<?php echo $this->lang->line('Done'); ?>">
                         </div>
                     </div>
                     <?php echo form_close(); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
     