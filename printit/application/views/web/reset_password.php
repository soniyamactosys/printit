 <div class="main_content">
           
               <div class="custom_print text-center reset_password_page">
                <div class="row">
<form id="resetPassForm" action="<?php echo site_url('Home/reset_new_password') ?>">
    <input type="hidden" name="email" value="<?php echo $this->input->get('email'); ?>" />
    <input type="hidden" name="token" value="<?php echo $this->input->get('token'); ?>" />
<input type="password" name="password" id="reset_password" class="form-control" placeholder="New Password" >
<span id="reset_pass_err" class="error text-danger" style="display:none;">Please Enter New Password</span>
<input type="password" name="cpassword" id="reset_cpassword" class="form-control" placeholder="Confirm New Password" >
<span id="reset_cpass_err" class="error text-danger" style="display:none;">Please Confirm Your Password</span>

<input type="submit" value="Reset" />
</form>

</div>
</div>
</div>