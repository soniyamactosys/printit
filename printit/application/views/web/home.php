<?php $userdata = $this->session->userdata('userdata');
$service_type = $this->session->userdata('service_type');

//print_r($service_type); exit;
 ?>
            
            <div class="main_content">
            <div id="print_option">
                    <div class="select_panetry" data-toggle="modal" data-target="#panetry_selection"><i class="fa fa-arrow-right" aria-hidden="true"></i> <?php echo $this->lang->line('select_a_printery');  ?></div>
                            <ul>
                            
                            <?php 
                            if($service_type=='express'){ ?>

 
      <li id="start"><span class="svg-img <?php echo ($userdata)?'':'loggedin'; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="91" height="90" viewBox="0 0 91 90">
    <g id="prefix__Empty_printirie" data-name="Empty printirie" transform="translate(.328 -.425)" class="span_c_icon">
        <ellipse id="prefix__Ellipse_51" cx="45.5" cy="45" data-name="Ellipse 51" rx="45.5" ry="45" transform="translate(-.328 .425)" style="fill:#fff"/>
        <img src="<?php echo base_url('webassets/images/Printit-logo.png'); ?>" class="svg-c-img">
    </g>
</svg>


 </span>

</li>
                            <?php }else{ ?>
                              
<li id="start"><span class="svg-img  <?php echo ($userdata)?'':'loggedin'; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="91" height="90" viewBox="0 0 91 90">
    <g id="prefix__Empty_printirie" data-name="Empty printirie" transform="translate(.328 -.425)" class="span_c_icon">
        <ellipse id="prefix__Ellipse_51" cx="45.5" cy="45" data-name="Ellipse 51" rx="45.5" ry="45" transform="translate(-.328 .425)" style="fill:#fff"/>
        <g id="prefix__Printt_Houses_Icon" data-name="Printt Houses Icon" transform="translate(21.013 20.288)" class="svg_inner_icon" >
            <path id="prefix__Rectangle_97" d="M0 0H49.684V49.684H0z" data-name="Rectangle 97" style="opacity:0;fill:#001b29"/>
            <path id="prefix__Path_4013" d="M10.991 5H6.5A1.5 1.5 0 0 0 5 6.5v3A1.5 1.5 0 0 0 6.5 11h3A1.5 1.5 0 0 0 11 9.5zM6.51 13.988V28.92h2.982v-7.44a3 3 0 0 1 3-3h3a3 3 0 0 1 3 3v7.44h9.01V13.988h-3.025a4.478 4.478 0 0 1-3-1.145 4.477 4.477 0 0 1-3 1.145h-3a4.478 4.478 0 0 1-3-1.145 4.477 4.477 0 0 1-3 1.145zm-3-1.13A4.487 4.487 0 0 1 2 9.492v-3A4.5 4.5 0 0 1 6.5 2h20.974a4.5 4.5 0 0 1 4.5 4.5v3a4.482 4.482 0 0 1-1.479 3.334V28.92a3 3 0 0 1-3 3h-11.45a3.043 3.043 0 0 1-.559.052h-3a3.043 3.043 0 0 1-.559-.052H6.512a3 3 0 0 1-3-3V12.858zm11.976 16.063V21.48h-3v7.44h3zM19.981 5h-5.993v4.5a1.5 1.5 0 0 0 1.5 1.5h3a1.5 1.5 0 0 0 1.5-1.5zm3 0v4.5a1.5 1.5 0 0 0 1.5 1.5h3a1.5 1.5 0 0 0 1.5-1.5v-3a1.5 1.5 0 0 0-1.5-1.5zm0 13.486h1.5a1.5 1.5 0 0 1 1.5 1.5v4.5a1.5 1.5 0 0 1-1.5 1.5h-1.5a1.5 1.5 0 0 1-1.5-1.5v-4.5a1.5 1.5 0 0 1 1.497-1.503z" data-name="Path 4013" transform="translate(7.81 7.079)" style="fill:#001b29"/>
        </g>
    </g>
</svg>
</span>
</li>

                            <?php } ?>


                             <li></li>
                             <li id="custom">
                                <div class="line"></div>
                                <h4><span><?php echo $this->lang->line('custom_print');  ?></span></h4> 
                                <div class="img_box"><a href="<?php echo site_url('customprint'); ?>"><img src="<?php echo base_url('webassets/images/2.png'); ?>"> </a></div>
                                <?php if(!empty($userdata) && $service_type==''){ ?>
                                <a href="<?php echo site_url('customprint'); ?>" 
                                  class="selectprintry_modal"><h5>Custom <span>Print</span></h5></a>
                                <?php }else{ ?>
                                <a href="<?php echo site_url('customprint'); ?>"><h5><?php echo $this->lang->line('custom_print');  ?></h5></a>  
                                <?php } ?>
                                
                             </li>
                             <li></li>
                             <li id="quick">
                                <div class="line"></div>
                                <h4><span><?php echo $this->lang->line('quick_print');  ?></span></h4> 
                                <div class="img_box"><a href="<?php echo site_url('quickprint'); ?>"><img src="<?php echo base_url('webassets/images/3.png'); ?>"></a> </div>
                                 <a href="<?php echo site_url('quickprint'); ?>"><h5><?php echo $this->lang->line('quick_print');  ?></span></h5></a></li>
                             <li></li>
                             <?php if(empty($service_type) || $service_type=='express'){ ?>
                             <li id="translation"><div class="line"></div><h4><span><?php echo $this->lang->line('translation');  ?></span></h4>
                              <div class="img_box"><a href="<?php echo site_url('translation'); ?>"><img src="<?php echo base_url('webassets/images/4.png'); ?>"> </a></div>
                              <a href="<?php echo site_url('translation'); ?>"><h5><?php echo $this->lang->line('translation');  ?></h5></a></li>
                             <li></li>
                             <?php } ?>
                             <li id="notes"><div class="line"></div><h4><span><?php echo $this->lang->line('notes');  ?></span></h4> 
                             <div class="img_box"><a href="<?php echo site_url('notes'); ?>"><img src="<?php echo base_url('webassets/images/5.png'); ?>"></a> </div>
                              <a href="<?php echo site_url('notes'); ?>"><h5><?php echo $this->lang->line('notes');  ?></h5></a></li>
                             <li><span><?php echo $this->lang->line('go');  ?></span></li>
                            </ul><!-- ul end here -->
                           </div>
                    </div>
                           


                           <div class="modal fade" id="panetry_selection" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                      
                      <div class="modal-body">
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="form-group select_panetry_mode">
                      <p><input type="radio" id="express" value="express" class="order_type" name="radio-group" <?php if($service_type=='express'){ echo 'checked'; } ?> ><label for="express"> <img src="<?php echo base_url('webassets/images/launchingIcon.png'); ?>" class="home_print_logo"> <?php echo $this->lang->line('express_printery_order'); ?></label><span class="select_tooltip" data-toggle="tooltip" data-placement="top" title="Service by your favorite printery That is available for delivery or Pickup"><i class="fa fa-info-circle" aria-hidden="true"></i></span></p>
                                  <p><input type="radio" id="normal" value="normal" class="order_type" name="radio-group" <?php if($service_type=='normal'){ echo 'checked'; } ?>><label for="normal"><img src="<?php echo base_url('webassets/images/Printt-house.png'); ?>" class="home_print_logo1"><?php echo $this->lang->line('normal_printery_order'); ?> </label> <span class="select_tooltip" data-toggle="tooltip" data-placement="top" title="Service Provided by Print-it App with delivery option only"><i class="fa fa-info-circle" aria-hidden="true"></i></span></p>
                    <span class="error text-danger" id="ordertype_err" style="display:none;">Please select order type</span>
                                
                                </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-title="paper_type" id="ordertype"><?php echo $this->lang->line('confirm'); ?></button>
                      </div>
                    </div>
                  </div>
                </div>
                
                <style>


/*.svg_inner_icon{
    display:none !important;}
}*/

                </style>
                
                <script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>