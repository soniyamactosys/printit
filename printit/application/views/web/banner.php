<?php 
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
//echo "<pre>"; print_r($order); exit;
} 

?>
       
<div class="main_content">

    <div id="flow_option" class="custom_print blue_print">
                <ul>
                 <li>
                   <div class="custom_print_inner text-center">
                   
                      <img src="<?php echo base_url('webassets/images/banner.png'); ?>">
                      <div class="btn_outer"></div>
                   
                  </div>
                 </li>
                
                <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('paper_type'); ?></span></h4>
                  <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#papertypemodal'; } ?>"><img src="<?php echo base_url('webassets/images/poster1.png'); ?>"> </div>
                  <a href="javascript:void(0);"><h5><?php echo $this->lang->line('paper_type'); ?></h5></a>
                  <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="ptype" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['papertype'])?'':$order['papertype']; ?></h6>
                </li>
                <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('width'); ?></span></h4>
                  <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#banner_width_modal'; } ?>"><img src="<?php echo base_url('webassets/images/poster5.png'); ?>"> </div>
                  <a href="javascript:void(0);"><h5><?php echo $this->lang->line('width'); ?></h5></a>
                  <h6 class="paper_type <?php echo empty($order)?'pwidth':'pwidth1'; ?>" id="pwidth" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['bannersize'])?'':$order['bannersize']; ?></h6>
                </li>
                  <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('design'); ?></span></h4>
                  <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#imagemodal'; } ?>"><img src="<?php echo base_url('webassets/images/poster3.png'); ?>"> </div>
                  <a href="javascript:void(0);"><h5><?php echo $this->lang->line('design'); ?></h5></a>
                  <h6 class="paper_type <?php echo empty($order)?'pdesign':'pdesign1';  ?>" id="pdesign" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['img_original_name'])?'':$order['img_original_name']; ?></h6>                  
                  </li>

                  <li class="process_flow" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#copy_number_modal'; } ?>"><div class="line"></div><h4><span><?php echo $this->lang->line('number_of_br_copies'); ?></span></h4>
                  <div class="img_box"><img src="<?php echo base_url('webassets/images/poster4.png'); ?>"> </div>
                  <a href="javascript:void(0);"><h5><?php echo $this->lang->line('number_of_br_copies'); ?></h5></a>
                  <h6 class="paper_type <?php echo empty($order)?'pcopy':'pcopy1';  ?>" id="pcopy" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['no_of_copies'])?'':$order['no_of_copies']; ?></h6>
                  </li>
                 
                 <li data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#projectname_modal'; } ?>"><span><?php echo $this->lang->line('print_br_it'); ?></span></li>
                </ul><!-- ul end here -->
               </div>

</div>
                           
<?php
$data['papertype'] = $papertype;
$data['papersize'] = $papersize;

$this->load->view('web/custom_print_modals',$data); ?>
<?php /*
        </div>

        
    </div>

<div class="modal fade poster_popup" id="step1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Paper Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="custom-select">
        <option selected>Select Paper Type</option>
        <option value="1">Paper Type 1</option>
        <option value="2">Paper Type 2</option>
        <option value="3">Paper Type 3</option>
      </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary poster_type_confirm" data-title="paper_type" data-dismiss="modal">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade poster_popup" id="step2" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="step1Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Paper Width</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="custom-select">
        <option selected>Select Paper Width</option>
        <option value="1">Paper Width 1</option>
        <option value="2">Paper Width 2</option>
        <option value="3">Paper Width 3</option>
      </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-title="paper_type" data-dismiss="modal">Confirm</button>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="right_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <div class="fileupload_name"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>  Design   <span><i class="fas fa-check"></i></span> 
        </div>
       
      </div>
      <div class="modal-body">
        <div class="upload_box">
          <div class="photo_upload"><input type="file" class="input_photoupload"><img src="images/Camera.png"></div>
          <div class="file_upload"><input type="file" class="input_fileupload"><img src="images/Upload.png"> Upload</div>
          <span>Max File Size <br> 20MB</span>
        </div>
        <div class="design_upload">
          <input type="file" class="input_designupload">
          <h3>Upload <br />Your Design</h3>
          <img src="images/Cover.png">
        </div>
        <div class="upload_setting">
          <ul>
            <li class="active"><img src="images/Ver.png"> Vertical</li>
             <li><img src="images/Hor.png"> Horizontal </li>
              <li><img src="images/Move.png"> Move</li>
               <li><img src="images/Scale.png"> Scale</li>
          </ul>
        </div>

      </div>
      
    </div>
  </div>
</div>

<div class="modal right fade printeries right_sidebar" id="printeries" tabindex="-1" role="dialog" aria-labelledby="right_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <div class="fileupload_name">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>  Design   <span><i class="fas fa-check"></i></span> 
      </div>
       
      </div>
      <div class="modal-body">
        <div class="product_box">
            <div class="row">
              <div class="col-md-4"><div class="product_img"><img src="images/g4652.png" class="mCS_img_loaded"></div></div>
              <div class="col-md-5"><div class="product_content">
                <h4>Print House Name</h4>
                <h2>Salmiyah Baghdad St.</h2>
                <h4>09 AM - 5 PM</h4>
              </div></div>
              <div class="col-md-3"><span class="open">open</span></div>
            </div>
        </div>
        <div class="product_box">
          <div class="row">
            <div class="col-md-4"><div class="product_img"><img src="images/g4652.png" class="mCS_img_loaded"></div></div>
            <div class="col-md-5"><div class="product_content">
              <h4>Print House Name</h4>
              <h2>Salmiyah Baghdad St.</h2>
              <h4>09 AM - 5 PM</h4>
            </div></div>
            <div class="col-md-3"><span class="closed">closed</span></div>
          </div>
        </div>
        <div class="product_box">
          <div class="row">
            <div class="col-md-4"><div class="product_img"><img src="images/g4652.png" class="mCS_img_loaded"></div></div>
            <div class="col-md-5"><div class="product_content">
              <h4>Print House Name</h4>
              <h2>Salmiyah Baghdad St.</h2>
              <h4>09 AM - 5 PM</h4>
            </div></div>
            <div class="col-md-3"><span class="busy">busy</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<div class="modal fade" id="cart" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="cartLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <span class="carticonbox"><img src="images/cart.png"></span>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
          <div class="cart_product_box">
            <div class="cart_icon"><img src="images/Custom-print1.png"></div>
            <div class="cart_content"><h3>#129383</h3>
            <h2>being printed</h2></div>
            <a href="#"><i class="fas fa-arrow-right"></i></a>
          </div>
          <div class="cart_product_box">
              <div class="cart_icon"><img src="images/Custom-print.png"></div>
              <div class="cart_content"><h3>#129383</h3>
              <h2>Awaiting Translation proposals</h2></div>
              <a href="#"><i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="cart_product_box">
                <div class="cart_icon"><img src="images/Custom-print1.png"></div>
                <div class="cart_content"><h3>#129383</h3>
                <h2>Ready for pickup</h2></div>
                <a href="#"><i class="fas fa-arrow-right"></i></a>
              </div>
             <div class="cart_product_box">
                <div class="cart_icon"><img src="images/Custom-print.png"></div>
                <div class="cart_content"><h3>#129383</h3>
                <h2>3 proposals recieved</h2></div>
                <a href="#"><i class="fas fa-arrow-right"></i></a>
              </div>
        </div>
  </div>
  </div>
</div>
<div class="modal right fade right_sidebar" id="account" tabindex="-1" role="dialog" aria-labelledby="account">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h2><button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button> Account</h2>
       
      </div>
      <div class="modal-body">
        
       <h3>Hello <br />Fadi Ameer</h3>
       <form>
         <div class="form-group"><input type="text" value="Hey@fadiameer.com" placeholder="Enter Email Id" class="form-control"></div>
          <div class="form-group"><input type="text" value="" placeholder="Name" class="form-control"></div>
          <div class="form-group"><input type="text" value="" placeholder="Mobile Number" class="form-control"></div>
          <div class="form-group"><input type="text" value="" placeholder="Current Password" class="form-control"></div>
          <div class="form-group"><input type="text" value="" placeholder="New Password" class="form-control"></div>
          <div class="submit_button"><button class="btn primary-btn">Update</button></div>
       </form>
      </div>
      
    </div>
  </div>
</div>
*/ ?>