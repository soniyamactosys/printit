            <div class="main_content">
           <div class="services_type">
            <h1>Select Service Type  </h1>
            <div class="select_services_inner">
                    <a href="<?php echo site_url('set_service_type/express'); ?>">
                    <div class="product_box">
                      <div class="row">
                        <div class="col-md-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printit-logo.png'); ?>" class="mCS_img_loaded"></div></div>
                        <div class="col-md-7"><div class="product_content">
                          <h4>Print-it Express </h4>
                          <h2>Service Provided by Print-it App with delivery option only</h2>
                        </div></div>
                      </div>
                    </div>
                    </a>
                    <?php if(!empty($this->session->userdata('cart_data'))){ ?>
                      <a href="javascript:void(0)" id="localprintery">
                      <div class="product_box">
                        <div class="row">
                          <div class="col-md-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printt-house.png'); ?>" class="mCS_img_loaded"></div></div>
                          <div class="col-md-7"><div class="product_content">
                            <h4>Local Printeries</h4>
                            <h2>Service by your favorite printery That is available for delivery or Pickup </h2>
                          </div></div>
                        </div>
                      </div>
                      </a>
                    <?php }else{ ?>

                      <a href="<?php echo site_url('set_service_type/normal'); ?>">
                      <div class="product_box">
                        <div class="row">
                          <div class="col-md-5"><div class="product_img"><img src="<?php echo base_url('webassets/images/Printt-house.png'); ?>" class="mCS_img_loaded"></div></div>
                          <div class="col-md-7"><div class="product_content">
                            <h4>Local Printeries</h4>
                            <h2>Service by your favorite printery That is available for delivery or Pickup </h2>
                          </div></div>
                        </div>
                      </div>
                      </a>
                    <?php } ?>
            </div>
           </div>

<div class="modal right fade printeries right_sidebar" id="printery_modal" tabindex="-1" role="dialog" aria-labelledby="printery_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="fileupload_name">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"> <i class="fas fa-arrow-left"></i> </span>
        </button>
        <span><i class="fas fa-search"></i></span> 
        </div>
       
      </div>
      <div class="modal-body">
        <?php if(!empty($local_printery)){ foreach($local_printery as $value){ ?>
          <div class="product_box printeryBox" printryid="<?php echo $value['id']; ?>">
            <div class="row">
              <div class="col-md-4"><div class="product_img"><img src="<?php echo base_url('webassets/images/g4652.png'); ?>" class="mCS_img_loaded"></div></div>
              <div class="col-md-5"><div class="product_content">
                <h4><?php echo $value['shop_name']; ?></h4>
                <h2><?php

                $fullAddress = '';
                if(!empty($value['po_box'])){
                  $fullAddress .= $value['po_box'].'<br>';
                } 

                if(!empty($value['block'])){
                  $fullAddress .= $value['block'].'<br>';
                }

                if(!empty($value['address'])){
                  $fullAddress .= $value['address'].'<br>';
                }
                 echo $fullAddress; ?>
                   
                </h2>
                
                <h4><?php $workingHour = '';
                  if(!empty($value['start_time'])){
                    $workingHour .= date('h A',strtotime($value['start_time']));
                  }

                  if(!empty($value['end_time'])){
                    $workingHour .= " - ".date('h A',strtotime($value['end_time']));
                  }
                  echo $workingHour;
                 ?></h4>
                
              </div></div>
              <div class="col-md-3"><span class="open">
              <?php if(!empty($workingHour)){
                $start_time = strtotime($value['start_time']);
                $end_time = strtotime($value['end_time']);
                $current_time = time();
                if($start_time < $current_time && $end_time >$current_time){
                  echo "Open";
                }else{
                  echo "Closed";
                }
              } ?>
              </span></div>
            </div>
          </div>
        <?php } } ?>
        <input type="hidden" name="selectedprintry" id="selectedprintry">

        <span id="printry_error" class="text-danger text-center" style="display: none;">Please Select Printry </span>
        <div class="sign-up-button">
        <button type="button" class="animatable bounceIn" id="proceedwithproducts">PROCEED WITH PRODUCTS</button>
        </div>
      </div>
    </div>
  </div>
</div>
