
            <div class="main_content">
           
                <div id="flow_option" class="custom_print blue_print flyer_print-area">
                            <ul>
                             <li>
                               <div class="custom_print_inner text-center">
                               
                                  <img src="<?php echo base_url('webassets/images/flyer.png'); ?>">
                                  <div class="btn_outer"></div>
                               
                              </div>
                             </li>
                            
                             <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('paper_type'); ?></span></h4>
                              <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#papertypemodal'; } ?>"><img src="<?php echo base_url('webassets/images/poster1.png'); ?>"> </div>
                              <a href="javascript:void(0)"><h5><?php echo $this->lang->line('paper_type'); ?></h5></a>
                              <h6 class="paper_type <?php echo empty($order)?'ptype':'ptype1';  ?>" id="ptype" style="display:<?php echo empty($order)?'none':'block';  ?>" ><?php echo empty($order['papertype'])?'':$order['papertype']; ?></h6>
                              </li>
                              
                              
                              <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('paper_size'); ?></span></h4>
                              <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#papersizemodal'; } ?>"><img src="<?php echo base_url('webassets/images/poster2.png'); ?>"> </div>
                              <a href="javascript:void(0)"><h5><?php echo $this->lang->line('paper_size'); ?></h5></a>
                              <h6 class="paper_type <?php echo empty($order)?'psize':'psize1';  ?>" id="psize" style="display:<?php echo empty($order)?'none':'block';  ?>"><?php echo empty($order['papersize'])?'':$order['papersize']; ?></h6>
                              </li>
                              <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('printing_br_sides'); ?></span></h4>
                              <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#printingsidemodal'; } ?> "><img src="<?php echo base_url('webassets/images/poster4.png'); ?>"> </div>
                              <a href="javascript:void(0)"><h5><?php echo $this->lang->line('printing_br_sides'); ?></h5></a>
                              <h6 class="paper_type <?php echo empty($order)?'psides':'psides1';  ?>" id="psides" style="display:<?php echo empty($order['printing_side'])?'none':'block';  ?>"><?php echo empty($order['printing_side'])?'':ucfirst($order['printing_side']).' Side'; ?></h6>
                              </li>
                              
                              <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('design'); ?></span></h4>
                              <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#imagemodal'; } ?>"><img src="<?php echo base_url('webassets/images/poster3.png'); ?>"> </div>
                              <a href="javascript:void(0)"><h5><?php echo $this->lang->line('design'); ?></h5></a></li>
                              <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('number_of_br_copies'); ?></span></h4>
                              <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#copy_number_modal'; } ?>"><img src="<?php echo base_url('webassets/images/poster4.png'); ?>"> </div>
                              <a href="javascript:void(0)"><h5><?php echo $this->lang->line('number_of_br_copies'); ?></h5></a>
                              <h6 class="paper_type <?php echo empty($order['no_of_copies'])?'pcopy':'pcopy1';  ?>" id="pcopy" style="display:<?php echo empty($order['no_of_copies'])?'none':'block';  ?>"><?php echo empty($order['no_of_copies'])?'':$order['no_of_copies']; ?></h6>
                              </li>
                             
                             <li data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#projectname_modal'; } ?>"><span><?php echo $this->lang->line('print_br_it'); ?></span></li>
                            </ul><!-- ul end here -->
                           </div>
           
                    </div>

<?php
$data['papertype'] = $papertype;
$data['papersize'] = $papersize;

$this->load->view('web/custom_print_modals',$data); ?>