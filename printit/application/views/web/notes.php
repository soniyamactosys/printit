<div class="main_content">
   <div id="notes_selection" class="content notes_product">
    <div class="notes_filter">
      <ul>
        <li><span><img src="<?php echo base_url('webassets/images/refine.png'); ?>"></span></li>
        <li class="active filter_notes"><a href="<?php echo site_url('notes'); ?>">All Class</a></li>
        <?php if(!empty($notes_cat)){ foreach($notes_cat as $cat){ ?>
        <li class="filter_notes"  cat="<?php echo $cat['id']; ?>"><a href="javascript:void(0)" ><?php echo $cat['name']; ?></a></li>
        <?php } } ?>
      </ul>
    </div>
    
    <ul class="product_list" id="notes_div">
      <?php if(!empty($notes)){ foreach($notes as $key=>$value){ 
        $mod = $key%4;
        if($mod==0){ ?>
        <li class="product_col row">
        <?php } ?>
      
        <div class="product_box">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-6"><div class="product_img"><img src="<?php echo base_url('uploaded_notes/images/').$value['image']; ?>"></div></div>
            <div class="col-md-6 col-sm-6 col-6"><div class="product_content">
              <h4><?php echo $value['cat']; ?></h4>
              <h2><a href="<?php echo site_url('note_detail/').encoding($value['id']); ?>"><?php echo $value['name']; ?></a></h2>
              <!-- <h4>Grade 10</h4> -->
            </div></div>
          </div>
        </div>
    <?php if($mod==3){ ?>
        </li>
    <?php } } } ?>     


</ul>
</div>
</div>