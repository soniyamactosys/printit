<?php 
$order = array();
if(!empty($this->session->userdata('current_order'))){
$order = $this->session->userdata('current_order');
}
?>
<div class="main_content">
<input type="hidden" name="note_id" id="note_id" value="<?php echo $detail['id']; ?>">
<input type="hidden" name="total_pages" id="total_pages" value="<?php echo $detail['total_pages']; ?>">
<div class="notes_detail">
<div class="notes_detail_box">
<div class="notes_top">
<div class="product_box">
  <div class="row">
    <div class="col-md-6"><div class="product_img"><img src="<?php echo base_url('uploaded_notes/images/').$detail['image']; ?>" class="mCS_img_loaded"></div></div>
    <div class="col-md-6"><div class="product_content">
      <h4><?php echo $detail['cat']; ?></h4>
      <h2><?php echo $detail['name']; ?></h2>
      <!-- <h4>Grade 10</h4> -->
    </div></div>
  </div>
</div>
</div>
<div class="notes_bottom">
	<?php echo $detail['description']; ?>
</div>
</div>

<div id="flow_option" class="note_print_area">
    
    <ul>
       <li class="process_flow"><h4><span></span></h4>
    
      <a href="javascript:void(0)"><h5>&&&&&&</h5></a>

      </li>
    
    <li class="process_flow"><div class="line"></div><h4><span><?php echo $this->lang->line('color'); ?></span></h4>
      <div class="img_box" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#colormodal'; } ?>"><img src="<?php echo base_url('webassets/images/note_color.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('color'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order['color'])?'pcolor':'pcolor1';  ?>" id="pcolor" style="display:<?php echo empty($order['color'])?'none':'block';  ?>">
        <?php echo empty($order['color'])?'':ucfirst(str_replace("_"," ",$order['color'])); ?>
        </h6>
    </li>


      <li class="process_flow" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#copy_number_modal'; } ?>"><div class="line"></div><h4><span><?php echo $this->lang->line('number_of_br_copies'); ?></span></h4>
      <div class="img_box"><img src="<?php echo base_url('webassets/images/note_nu.png'); ?>"> </div>
      <a href="javascript:void(0)"><h5><?php echo $this->lang->line('number_of_br_copies'); ?></h5></a>
      <h6 class="paper_type <?php echo empty($order['no_of_copies'])?'pcopy':'pcopy1';  ?> " id="pcopy" style="display:<?php echo empty($order['no_of_copies'])?'none':'block';  ?>"><?php echo empty($order['no_of_copies'])?'':$order['no_of_copies']; ?></h6>

      </li>

    <li>
        <div class="order_btn" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#projectname_modal'; } ?>" ><?php echo $this->lang->line('order_it');  ?></div>
    </li>
    </ul><!-- ul end here -->
    
  </div>


<!--<div class="from-to" data-toggle="modal" data-target="<?php if(empty($this->session->userdata['userdata'])){ echo '#plz_login_modal'; }else{ echo '#page_range_modal'; } ?>" > from - to</div>-->


</div>
</div>

<?php $this->load->view('web/notes_modals'); ?>