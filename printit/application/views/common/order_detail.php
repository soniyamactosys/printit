<?php 		
$ptypeArr = array('poster,flyer,rollup'); 
$printtypeDetail = get_print_type_data("",$detail['print_type']);
//echo "<pre>"; print_r($detail);
?>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

	  <div class="container-full">



		<!-- Main content -->

		<section class="content">

		  <div class="row">

			  

			<div class="col-12">

			  <!-- /.box -->



			  <div class="box">

				<div class="box-header with-border">

				  <h3 class="box-title">Order Detail </h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body">
					
					
					
<div class="table-responsive">
<?php //echo "<pre>"; print_r($detail); ?>

 <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">	<tbody>
		<tr>
			<th>Service type</th>
			<td><?php echo ($detail['service_type']=='1')?'Normal':'Express'; ?></td>
		</tr>
		<tr>
		<th>Project Name</th>
			<td><?php echo $detail['project_name']; ?></td>
		</tr>
		<tr>
			<th>Order Number</th>
			<td><?php echo $detail['order_number']; ?></td>
		</tr>	
		<tr>
			<th>Order Date</th>
			<td><?php echo date('d-m-Y',strtotime($detail['order_date'])); ?></td>
		</tr>	
		<tr>
			<th>Order type</th>
			<td><?php
			switch ($detail['order_type']) {
  case '1':
    echo 'Custom Print';
    break;
  case '2':
    echo 'Quick Print';
    break;
  case '3':
    echo 'Translation';
    break;
  case '4':
    echo 'Notes';
    break;
  default:
    echo 'Custom Print';
} 
			?></td>
		</tr>	
		<?php if($detail['order_type']==1){ ?>

		<tr>
			<th>Print type</th>
			<td>
			<?php 
				if(!empty($printtypeDetail)){ echo $printtypeDetail['name']; }
			?></td>
		</tr>

		<tr>
			<th>Paper type</th>
			<td><?php echo $detail['papertype']; ?></td>
		</tr>
		<?php 
		if(in_array($detail['print_type'],$ptypeArr)){ ?>
		<tr>
			<th>Paper size</th>
			<td><?php echo $detail['papersize']; ?></td>
		</tr>
		<?php }
		} ?>
		<tr>
			<th>Design</th>
			<td>
				<a class="btn btn-primary btn-sm" href="<?php echo base_url('order_uploads/').$detail['image_uploaded']; ?>" target="_blank">click to view</a>
				<a class="btn btn-success btn-sm" href="<?php echo base_url('order_uploads/').$detail['image_uploaded']; ?>" download=""><i class="fa fa-download"></i></a>
				<!-- <img width="100px" height="100px" src="<?php echo base_url('order_uploads/').$detail['image_uploaded']; ?>" /> -->
			</td>
		</tr>
		<tr>
			<th>Number of copies</th>
			<td><?php echo $detail['no_of_copies']; ?></td>
		</tr>	
		<?php 
		if($printtypeDetail['name']=='banner'){ ?>
		<tr>
			<th>Width</th>
			<td><?php echo $detail['width']; ?></td>
		</tr>	
		<?php } 
		if($printtypeDetail['name']=='flyer'){ ?>
		<tr>
			<th>Printing Sides</th>
			<td><?php echo $detail['printing_side']; ?></td>
		</tr>	
		<?php } ?>
		<tr>
			<th>Pickup/Delivery</th>
			<td><?php echo ($detail['pickup_delivery']=='1')?'Pickup':'Delivery'; ?></td>
		</tr>
		<tr>
			<th>Address</th>
			<td><?php echo $detail['address']; ?></td>
		</tr>
		<tr>
			<th>Payment Status</th>
			<td><?php echo ($detail['payment_status']=='0')?'Pending':'Complete'; ?></td>
		</tr>	
		<tr>
			<th>Amount</th>
			<td><?php echo $detail['amount']; ?></td>
		</tr>	
		<tr>
			<th>Customer</th>
			<td><?php echo $detail['customer_name']; ?></td>
		</tr>	
		<tr>
			<th>Vendor</th>
			<td><?php echo $detail['vendor_name']; ?></td>
		</tr>

	</tbody>
</table>


				</div>              

				</div>

				<!-- /.box-body -->

			  </div>

			  <!-- /.box -->          

			</div>

			<!-- /.col -->

		  </div>

		  <!-- /.row -->

		</section>

		<!-- /.content -->

	  

	  </div>

  </div>