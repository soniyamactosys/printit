<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Printit - <?php echo ($title)?$title:''; ?></title>
    
	<!-- Vendors Style-->
 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">


  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/css/vendors_css.css'); ?>">
	  
	<!-- Style-->  
	<!-- <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/skin_color.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datepicker.min.css'); ?>">
<?php /*
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css'); ?>">
 */ ?>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/css/bootstrap-timepicker.min.css" rel="stylesheet" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
  <body class="hold-transition light-skin sidebar-mini theme-primary">
    
<div class="wrapper">
  <!-- <div id="loader"></div> -->
    
  <header class="main-header">
    <div class="d-flex align-items-center logo-box justify-content-start" style="background-color: #000000;">
        <a href="#" class="waves-effect waves-light nav-link d-none d-md-inline-block mx-10 push-btn bg-transparent" data-toggle="push-menu" role="button">
            <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
        </a>    
        <!-- Logo -->
        <a href="<?php echo site_url(); ?>" class="logo">
          <!-- logo-->
          <div class="logo-lg">
              <span class="light-logo"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo"></span>
              <span class="dark-logo"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo"></span>
          </div>
        </a>    
    </div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav"> 
            <li class="btn-group nav-item d-lg-inline-flex d-none">
                <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link full-screen" title="Full Screen">
                    <i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
                </a>
            </li>
        </ul>
      </div>
      
        <div class="dropdown admin_drop_box notification_box">
    <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown">
<i class="fa fa-bell" aria-hidden="true"></i><span class="caret" id="notify_count"></span></button>
    <ul class="dropdown-menu" id="noti_ul">
        <li>
            <div class="notification_panel">
  <div class="notify_hrad"><h4>notification <a href="#"><span class="icon-Settings-2"></span></a></h4>
  </div>
  <div id="notify_body" ><div class="notify_footer">No new notification</div></div>
</div>   
        </li>
       
    </ul>
  </div>
  
  <div class="dropdown admin_drop_box">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
<?php if(!empty($this->vendordata)){ echo $this->vendordata['username']; }elseif(!empty($this->admindata)){ echo "Admin"; } ?>    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li> <i class="fa fa-user-o" aria-hidden="true"></i> <a href="<?php if(!empty($this->vendordata)){ echo site_url('vendor/myprofile'); }elseif(!empty($this->admindata)){ echo  site_url('admin/myprofile'); } ?>">Profile</a></li>
      <li><i class="fa fa-cog" aria-hidden="true"></i> <a href="<?php if(!empty($this->vendordata)){ echo site_url('vendor/edit_myprofile'); }elseif(!empty($this->admindata)){ echo  site_url('admin/edit_myprofile'); } ?>">Setting</a></li>
      <!-- <li><i class="fa fa-key" aria-hidden="true"></i> <a href="#">Change Password</a></li> -->
      <li><i class="fa fa-sign-out" aria-hidden="true"></i> <a href="<?php if(!empty($this->vendordata)){ echo site_url('vendor_logout'); }elseif(!empty($this->admindata)){ echo  site_url('logout'); } ?>">Logout</a></li>
    </ul>
  </div>
    </nav>
  </header>