
<footer class="main-footer">
<div class="pull-right d-none d-sm-inline-block">

    <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">

    <li class="nav-item">

     ©2020 All Rights Reserved. <a target="_blank" href="#" class="nav_link">Printit TECHNOLOGIES</a>

    </li>

  </ul>

</div>

</footer>
<?php $this->load->view('common/delete_confirmation');
$this->load->view('common/status_confirmation');
?>
<script>
var notif_url = '<?php if(!empty($this->session->userdata['vendordata'])){ echo site_url("vendor/notification"); }elseif(!empty($this->session->userdata['admindata'])){ echo site_url("admin/notification");} ?>';
var update_notif_url = '<?php if(!empty($this->session->userdata['vendordata'])){ echo site_url("vendor/mark_read_notif"); }elseif(!empty($this->session->userdata['admindata'])){ echo site_url("admin/mark_read_notif");} ?>';
    function notification() {
        $.ajax({
            url: notif_url,
            dataType:'json',
            beforeSend:function(){
            },
            success: function(res) {
                if(res.success){
                    $('#notify_count').html(res.count);
                }else{
                    $('#notify_count').html('');
                }
                $('#notify_body').html(res.content);
            }
        });        
    }

notification();

setInterval(function(){
notification();
},30000);

$(document).on('click','.notif_li',function(){
    var id = $(this).attr('noti_id');
    var link = $(this).attr('link');
    $.ajax({
        url: update_notif_url,
        dataType:'json',
        data:{"id":id},
        method:'post',
        beforeSend:function(){
        },
        success: function(res) {
            if(res.success){
                window.location.href= link;
            }
        }
    });      
});
</script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
 

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDc3cw_q_7kjC_CKRhqV1t2KFQxuM1R-gU&libraries=places"></script>

<script src="<?php echo base_url('assets/js/vendors.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/bootstrap-select.js'); ?>"></script>

<script src="<?php echo base_url('assets/assets/icons/feather-icons/feather.min.js'); ?>"></script>    



<script src="<?php echo base_url('assets/assets/vendor_components/moment/min/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/assets/vendor_components/fullcalendar/fullcalendar.js'); ?>"></script>



<script src="<?php echo base_url('assets/js/template.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/pages/dashboard3.js'); ?>"></script>

<script src="<?php //echo base_url('assets/js/pages/calendar.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/pages/data-table.js'); ?>"></script>

<script src="<?php echo base_url('assets/assets/vendor_components/datatable/datatables.min.js'); ?>"></script>



<script src="<?php echo base_url('assets/js/loaderjs.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js'); ?>"></script>
<!-- <script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/bootstrap.timepicker/0.2.6/js/bootstrap-timepicker.min.js"></script>
<?php /*
<script src="<?php echo base_url('assets/assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js'); ?>"></script>
*/ ?>
<script src="<?php echo base_url('assets/js/pages/chat-popup.js'); ?>"></script>


<script src="<?php echo base_url('assets/assets/vendor_components/sweetalert/sweetalert.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js'); ?>"></script>

<script>
var site_url = '<?php echo site_url(); ?>';
var base_url = '<?php echo base_url(); ?>';

$('.timepicker').timepicker({
    showInputs: false
  });
</script>


<?php if(!empty($this->session->userdata['vendordata'])){ ?>
<script src="<?php echo base_url('assets/js/vcustom.js'); ?>"></script>


<script>



function branch_register() {
  var input = document.getElementById('branch_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('branch_lat').value = place.geometry.location.lat();
        document.getElementById('branch_lng').value = place.geometry.location.lng();
    });
}

google.maps.event.addDomListener(window, 'load', branch_register);

function company_address() {
  var input = document.getElementById('c_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('c_lat').value = place.geometry.location.lat();
        document.getElementById('c_lng').value = place.geometry.location.lng();
    });
}

google.maps.event.addDomListener(window, 'load', company_address);



/*
var myCenter = new google.maps.LatLng(22.731211,75.854202);
function initialize(){
    var mapProp = {
        center:myCenter,
        zoom:12,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"),mapProp);

    var marker = new google.maps.Marker({
        position:myCenter,
    });

    marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
*/
</script>
<?php }else{ ?>
<script src="<?php echo base_url('assets/js/scustom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/acustom.js'); ?>"></script>

<?php } ?>
<?php /*

<script src="<?php echo base_url('assets/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js'); ?>"></script>

    */ ?>
    <script type="text/javascript" src="<?php echo base_url('assets/js/custom.js'); ?>"></script>


<script>
    $('.delete').on('click',function(e){

        event.preventDefault();

        var ac = $(this).attr('action');

        $('#delete_modal').find('#yes_delete').attr('link',ac);

        $('#delete_modal').modal('show');

    });

    

    $(document).on('click','#yes_delete',function(){

        var url  = $(this).attr('link');

        $.ajax({

      url: url,
      dataType:'json',
      beforeSend:function(){

        ajaxindicatorstart();

      },

      success: function(res) {

        ajaxindicatorstop();
        if(res.success){

          $('#delete_modal').modal('hide');

                    swal('', "Done", "success");

                                    setTimeout(function() {

                    location.reload();

                }, 2000);
        }

                    

               

      }

    });

    });

</script>

<!-- Modal -->
  <div class="modal center-modal fade" id="complete_detail_alert_modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Welcome</h5>
          </div>
          <div class="modal-body">
            <p>Please complete your profile detail first to continue</p>
          </div>
          <div class="modal-footer modal-footer-uniform">
            <a href="<?php echo site_url('vendor/edit_myprofile') ?>" class="btn btn-success float-right">Continue</a>
            <!-- <button type="button" class="btn btn-success float-right">Continue</button> -->
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
  <?php if(!empty($this->session->userdata('vendordata'))){
    if(!check_branch_detail() && $this->uri->segment('2') !='edit_myprofile'){ ?>
        $('#complete_detail_alert_modal').modal({backdrop: 'static', keyboard: false},'show');
    <?php 
    }
  } ?>
  
function company_address_multiple() {
   $('.b_address').each(function(i,v){
    var input = document.getElementById($(this).attr('id'));
    var latinput = $(this).parents('.b_addressDiv').find('.b_lat');
    var longinput = $(this).parents('.b_addressDiv').find('.b_lng');
    var autocomplete = new google.maps.places.Autocomplete(input);        
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        //console.log(place.geometry.location.lat());
        //console.log(place.geometry.location.lng());
        $(latinput).val(place.geometry.location.lat());
        $(longinput).val(place.geometry.location.lng());
   
    });
   })
}

google.maps.event.addDomListener(window, 'load', company_address_multiple);
 
</script>
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
   CKEDITOR.replace( 'editor1' );
   CKEDITOR.replace( 'featureseditor' );
   CKEDITOR.replace( 'editfeatureseditor' );
    $('.view_notes_modal').on('click',function(e){
    //alert('hi');
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: site_url+'admin/view_notes_modal/'+id,
        beforeSend:function(){
            ajaxindicatorstart();
        },
        success: function(res) {
            ajaxindicatorstop();
            $('#view_notes_modal').find('.modal-content').html(res);
            $('#view_notes_modal').modal('show');
        }
    });
}); 

//19 feb
function adm_cus_address() {
  var input = document.getElementById('adm_cus_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('adm_cus_lat').value = place.geometry.location.lat();
        document.getElementById('adm_cus_lng').value = place.geometry.location.lng();
    });
}

google.maps.event.addDomListener(window, 'load', adm_cus_address);

function alladm_cus_address() {
  var input = document.getElementById('alladm_cus_address');
  var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        //document.getElementById('user_address').value = place.name;
        document.getElementById('alladm_cus_lat').value = place.geometry.location.lat();
        document.getElementById('alladm_cus_lng').value = place.geometry.location.lng();
    });
}

google.maps.event.addDomListener(window, 'load', alladm_cus_address);
</script>
  <!-- /.modal -->

<script>
const btn = document.querySelector('.btn');
let inputs = document.querySelectorAll('.input'),
    totalValue = 0;

//

const canvas = document.querySelector('#pieChart');
canvas.height = 410;
canvas.width = 350;
let ctx = canvas.getContext('2d');

// Data object containing pie chart sizes
const data = {
  one : undefined,
  two : undefined
}

// Setting data object values to user / or default input values
function updateValue() {
  inputs.forEach(input => {
    data[input.id] = parseInt(input.value);
  });
  return data;
}

updateValue();

// Update total value (sum of all of the user inputs or default data values)
function updateTotal() {
  for (let value in data) {
    totalValue += data[value];
  }
  return totalValue;
}

updateTotal();

const colors = [
  '#04a08b',
  '#ec8000'
]

// Class for creating a pie chart
class Chart {
  constructor(data, totalValue) {
    this.x = canvas.height / 2;
    this.y = canvas.height / 2;
    this.start = 0;
    this.totalValue = totalValue;
    
    let index = 0;

    for (let value in data) {
      totalValue += data[value];
      ctx.beginPath();
      ctx.moveTo(this.x, this.y);
      ctx.arc(this.x, this.y, 100, this.start, this.start + (Math.PI * 2 * (data[value] / this.totalValue)), false);
      ctx.fillStyle = colors[index];
      ctx.fill();

      this.start += Math.PI * 2 * (data[value] / this.totalValue);
      index += 1;
    }
  }
}

// Default pie chart instance
const test = new Chart(data, totalValue);

// Draw a new chart using updated values
function redrawChart(totalValue) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let chart = new Chart(data, totalValue);
}

// Event handler
function handler(e) {
  let input = e.target.closest('.input');

  if (input) {
    input.value = "";
    return;
  }

  let btn = e.target.closest('.btn');

  if (!btn) return;

  inputs.forEach(input => {
    if (input.value === "") {
      input.value = 0;
    }
  });

  totalValue = 0;
  updateValue();
  updateTotal();
  redrawChart(totalValue);
}

window.addEventListener('click', handler);   




</script>
</body>

</html>