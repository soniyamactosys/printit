<!-- Modal -->
  <div class="modal center-modal fade" id="delete_modal" tabindex="-1">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Are Your Sure??</h5>
			<button type="button" class="close" data-dismiss="modal">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<p>Do You Really Want to Delete this Entry?</p>
		  </div>
		  <div class="modal-footer modal-footer-uniform">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-success float-right" id="yes_delete">Yes</button>
		  </div>
		</div>
	  </div>
	</div>
  <!-- /.modal -->