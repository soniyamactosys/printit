<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->userdata = $this->session->userdata('userdata');
		if($this->userdata){
			$this->data['footer_data']['in_progress_orders'] = $this->generalmodel->orders_status();
		}     
		//echo "<pre>"  ; print_r($this->data['footer_data']['in_progress_orders']); exit;
    }
    
	public function index()
	{   
	    if(empty($this->session->userdata('site_lang'))){
		    $this->session->set_userdata('site_lang',  "english");
	    }
		
		destroy_order_in_session();
		$this->frontview('web/home',$this->data);
	}
    
	public function select_service_type(){
        $where = "`p.status` = '0'";
        $this->data['local_printery']  = $this->generalmodel->printeryshop_list($where);
	    $this->frontview('web/select_service_type',$this->data);
	}

	public function set_service_type($type=""){
	    if(!empty($type) && $this->input->is_ajax_request()){
	        $this->session->set_userdata('service_type',$type);
	        admin_service_fee($type);
	        $this->session->set_userdata('min_delivery_amount',1.500);
	        $return = array('success'=>true);
	    }else{
	    	$return = array('success'=>false);
	    }
	    echo json_encode($return);
	}	
	//=======ORDER TYPES =============
	public function custom_print(){
		destroy_order_in_session();
		if($this->userdata){ $this->session->set_userdata('order_type','1'); }
		$this->data['print_types'] = $this->generalmodel->getparticulardata('*','print_type',array('status'=>'0'),'result_array');
		$this->frontview('web/custom_print',$this->data);
	}
	
	public function quickprint(){
		if($this->userdata){ $this->session->set_userdata('order_type','2'); }
		
		$this->data['papertype'] = $this->generalmodel->web_paper_types("2");
		$this->data['papersize'] = $this->generalmodel->web_paper_size("2");
		
	   // $this->data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    
	   // $this->data['papertype'] = $this->generalmodel->getparticularData('id,name,description','paper_type',array('status'=>'0'),'result_array');		
		$this->frontview('web/quickprint',$this->data);
	    
	}

	public function translation(){
		if($this->userdata){ $this->session->set_userdata('order_type','3'); }
		$this->data['trans_types'] = $this->generalmodel->getparticularData('id,name','translation_types',array('status'=>'0'),'result_array');
		$this->data['language'] = $this->generalmodel->getparticularData('id,name','translation_lang',array('status'=>'0'),'result_array');
		$this->frontview('web/translation',$this->data);
	}

	
	public function notes(){
		//if($this->userdata){ $this->session->set_userdata('order_type','4'); }
		$this->data['notes_cat'] = $this->generalmodel->notes_categories('0');
	    $this->data['notes'] = $this->generalmodel->admin_notes('0');
	    // echo "<pre>"; print_r($this->data);
	    // exit;

	    $this->frontview('web/notes',$this->data);
	}

	public function note_detail($note_id){
		if($this->userdata){ $this->session->set_userdata('order_type','4'); }
		$note_id = decoding($note_id);
		$this->data['detail'] = $this->generalmodel->note_detail($note_id);
	    // echo "<pre>"; print_r($this->data);
	    // exit;

	    $this->frontview('web/note_detail',$this->data);
	}
	//=======ORDER TYPES =============


	public function printery_shops(){
	    
		$current_order = $this->session->userdata('current_order');
		$oid = $this->session->userdata('continue_order');
		$service_type = $this->session->userdata('service_type');
		if(empty($this->userdata) || empty($oid)){ redirect('/'); }
		
		if($service_type=="normal"){
		    
    		$this->data['list'] = $this->printery_withprice();
    		update_order_steps($oid,current_url());
    		$this->frontview('web/printry_list_page',$this->data);
		}else{
		    
		    $user_id = $this->userdata['user_id'];
		    $add_detail = $this->generalmodel->getparticularData("id,latitude,longitude","customer_address","`status`='0' AND `user_id`=$user_id  AND `latitude` IS Not NULL AND `longitude` IS NOT NULL","row_array");
    		
    		$lat = $add_detail['latitude'];
    		$long = $add_detail['longitude'];
    		
    		$this->session->set_userdata('selected_add',$add_detail['id']);
    		
    		$this->data['list'] = $this->ex_printery_with_price($oid,"",$lat,$long);

// echo $this->db->last_query();
// echo "<pre>"; print_r($this->data['list']); exit;
    
    		update_order_steps($oid,current_url());
    		$this->frontview('web/express_printry_list_page',$this->data);		    
		}
	}

	public function print_type($type){

		if($this->userdata){ 
		
			$this->session->set_userdata('order_type','1');
			$this->session->set_userdata('print_type',$type); 
		}
	   // $this->data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	   // $this->data['papertype'] = $this->generalmodel->getparticularData('id,name,description','paper_type',array('status'=>'0'),'result_array');

        $data = get_print_type_data($type);
    
		$this->data['papertype'] = $this->generalmodel->web_paper_types("1",$data['id']);
		$this->data['papersize'] = $this->generalmodel->web_paper_size("1",$data['id']);
		
	    $this->data['banner_size'] = $this->generalmodel->getparticularData('id,name','banner_size',array('status'=>'0'),'result_array');
		$this->frontview('web/'.$type,$this->data);

	}

	

		/*
	public function poster(){
		if($this->userdata){ $this->session->set_userdata('custom_print_option','poster'); }

	    $data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');
		$this->frontview('web/poster',$data);
	}

	public function flyer(){
		$this->session->set_userdata('custom_print_option','flyer');
	    $data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');		
		$this->frontview('web/flyer',$data);
	}
	
	public function rollup(){
		$this->session->set_userdata('custom_print_option','rollup');
		$this->frontview('web/rollup',$data);
	}
	
	public function banner(){
		$this->session->set_userdata('custom_print_option','banner');	
		$this->frontview('web/banner',$data);
	}
	*/


	
	public function user_register(){
	    	    
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
				//echo "<pre>"; print_r($_POST); exit;
	        if($this->form_validation->run('user_registeration')){
				
	            $password = $this->input->post('password');
                $cpassword = $this->input->post('cpassword');
                $address = $this->input->post('address');
                $user_lat = $this->input->post('user_lat');
                $user_lng = $this->input->post('user_lng');
	            $email = $this->input->post('email');
	            $gender = $this->input->post('gender');

				$check_exist = $this->generalmodel->getparticularData('id','users',array('email'=>$email,'delete_status'=>'0'),'row_array');

                if(!empty($check_exist)){
                    $return = array('success'=>false,'msg'=>"Email address is already registered with us!");
                    echo json_encode($return); exit;
                }

                if($password !== $cpassword){
                    $return = array('success'=>false,'msg'=>"Password don't match!");
                    echo json_encode($return); exit;
                }
                // elseif(!pass_strength($password)){
                //     $return = array('success'=>false,'msg'=>'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.');
                //     echo json_encode($return); exit;
                // }
                // else{
                //     $enc_pass = encrypt_pass($password);
                // }
                
	            $fname = $this->input->post('f_name');
	            $lname = $this->input->post('l_name');
	            $mobile = $this->input->post('mobile');
		        $userdata['role'] = '3';
		        $userdata['email'] = $email;
		        $userdata['firstname'] = $fname;
		        $userdata['lastname'] = $lname;
		        $userdata['mobile'] = $mobile;
		        $userdata['gender'] = $gender;
		        $userdata['password'] = md5($this->input->post('password'));
		        $userdata['created_at'] = date('Y-m-d h:i:s');
		        $userdata['latitude'] = $user_lat;
		        $userdata['longitude'] = $user_lng;
		        $userdata['address'] = $address;
	        
                $customer_address['label'] = "Main address";
                $customer_address['address'] = $address;
                $customer_address['latitude'] = $user_lat;
                $customer_address['longitude'] = $user_lng;
	        
	            //if($this->generalmodel->add('users',$userdata)){
	            if($this->generalmodel->register_user($userdata,$customer_address)){
	                //$this->session->set_userdata('userdata',array('email'=>$email,'role'=>'3','name'=>$name));
	                $return = array('success'=>true,'msg'=>'registered successfully');
	            }else{
	                $return = array('success'=>'false','msg'=>'internal error');        
	            }
	        }else{
	            $return = array('success'=>'false','msg'=>validation_errors());
	        }
	        
	        
	        echo json_encode($return);
	    }else{
    	    redirect('/');
	    }

	}
	
	public function userlogin(){
	    
	     if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('user_login')){
	            
	            $email = $this->input->post('email');
	            $password = $this->input->post('password');
	            
	            $userdata = $this->generalmodel->getparticularData('id,email,role,firstname,lastname,  CONCAT_WS(" ",firstname,lastname) AS username,latitude,longitude','users',array('email'=>$email,'password'=>md5($password),'status'=>'0','delete_status'=>'0'),'row_array');

	            if(!empty($userdata)){

	                if($userdata['role']==3){
	                	$this->session->set_userdata('userdata',array('user_id'=>$userdata['id'],'email'=>$email,'role'=>$userdata['role'],'name'=>$userdata['firstname'],'latitude'=>$userdata['latitude'],'longitude'=>$userdata['longitude']));

	                	$return = array('success'=>true,'role'=>'3','redirect'=>site_url('/'));
	                }elseif($userdata['role']==2){


						$branch_ids = $this->generalmodel->getparticularData('GROUP_CONCAT(`id`) as branch_ids','printery_shop',"user_id='".$userdata['id']."' AND delete_status='0' ",'row_array');

//echo $this->db->last_query();

						if(!empty($branch_ids)){
							$sessData['branch_ids']= $branch_ids['branch_ids'];
    						$trans_order = $this->generalmodel->get_trans_proposals($branch_ids['branch_ids']);
    						$this->session->set_userdata('trans_order',$trans_order);					
						}
						//	print_r($branch_ids); exit;


//=======translation proposals
                        


						$sessData['user_id']=$userdata['id'];
						$sessData['email']=$userdata['email'];
						$sessData['role']=$userdata['role'];
						$sessData['username']=$userdata['username'];
						$sessData['firstname']=$userdata['firstname'];
						$sessData['lastname']=$userdata['lastname'];
						$sessData['latitude']=$userdata['latitude'];
						$sessData['longitude']=$userdata['longitude'];
						$this->session->set_userdata('vendordata',$sessData);	                	
	                	$return = array('success'=>true,'role'=>'2','redirect'=>site_url('/vendor'));
	                }

					/*
	                // set cart data
	                $cartdata = $this->generalmodel->getparticularData('*','cart',array('customer'=>$userdata['id']),'result_array');

	                $customArr = $quickArr = $translationArr = $notesArr = array();
	                if(!empty($cartdata)){ foreach($cartdata as $value){
	                	if($value['order_type']==1){
							$customArr[] = $value;
	                	}elseif($value['order_type']==2){
	                		$quickArr[] = $value;
	                	}elseif($value['order_type']==3){
	                		$translationArr[] = $value;
	                	}elseif($value['order_type']==4){
	                		$notesArr[] = $value;
	                	}

	                }}

	                $this->session->set_userdata('cartdata',array('custom'=>$customArr,'quick'=>$quickArr,'translation'=>$translationArr,'notes'=>$notesArr));

	                $this->set_cartDate_session();

	                // echo "<pre>"; print_r($cartdata); 
	                // echo "session <pre>"; print_r($_SESSION); 
	                //  exit;
					*/

	            }else{
	                $return = array('success'=>false,'msg'=>'Invalid email or password');
	            }
	            
	            
	        }else{
	            $return = array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	     }
	}


	
	public function forgotpass(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
            $email = $this->input->post('email');
            $token = md5(uniqid(rand(), true));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            if($this->form_validation->run())
            {
                $userdata = $this->generalmodel->getparticularData('*','users',array('email'=>$email,'status'=>'0','delete_status'=>'0'),'row_array');
                if(empty($userdata)){
                     $return  = array('success'=>false,'msg'=>'Email Address is not registered with us');  
                }else{
                $insertdata['user_email'] = $email;
                $insertdata['token'] = $token;
                $insertdata['expiry'] = date('Y-m-d',strtotime("+1 day"));
                
                if($this->generalmodel->add('password_reset',$insertdata)){
                    
                    // $admin_detail = admin_detail();
                    // $from = $admin_detail['email'];
                    $to = $email;
                    $subject = "Password Reset Request";
                    $data['name'] = $userdata['firstname'].' '.$userdata['lastname'];
                    
                    $data['link'] = site_url('reset_password').'?email='.$email.'&&token='.$token;
                    $message = $this->mail_template_view('mail/forgot_pass',$data);

                    if($this->sendGridMail("",$to,$subject,$message)){
                        $return  = array('success'=>true,'msg'=>'Reset password link has been send to you email address!');    
                    }else{
                        $return  = array('success'=>false,'msg'=>'mail failed');                    
                    }
                }else{
                    $return  = array('success'=>false,'msg'=>'internal error');
                }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}
	
	public function reset_password(){
	    $this->frontview('web/reset_password');
	}
	
	public function reset_new_password(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
            $email = $this->input->post('email');
            $token = $this->input->post('token');
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required');
            $this->form_validation->set_rules('token', 'Token', 'required');
            if($this->form_validation->run())
            {
                $resetData = $this->generalmodel->getparticularData('*','password_reset',array('user_email'=>$email,'token'=>$token,'status'=>'0','expiry >'=>date('Y-m-d')),'row_array');
                if(empty($resetData)){
                     $return  = array('success'=>false,'msg'=>'Link is expired');  
                }else{
                    
                    if($password !== $cpassword){
                        $return = array('success'=>false,'msg'=>"Password don't match!");
                        echo json_encode($return); exit;
                    }else{
                        $this->generalmodel->updaterecord('password_reset',array('status'=>'1'),array('user_email'=>$email,'token'=>$token));
                        $this->generalmodel->updaterecord('users',array('password'=>md5($password)),array('email'=>$email));
                        
                        $user_detail = user_detail_byemail($email);
                        $admin_detail = admin_detail();
                        $from = $admin_detail['email'];

                        $to = $email;
                        $subject = "Password Reset Success";
                        $data['name'] = $user_detail['firstname']." ".$user_detail['lastname'];
                        $message = $this->mail_template_view('mail/pass_reset_success',$data);

                        if($this->sendGridMail($from,$to,$subject,$message)){
        
                        $return  = array('success'=>true,'msg'=>'New Password has been set successfully!');    
                        }else{
                            $return  = array('success'=>false,'msg'=>'mail failed');                    
                        }
                    }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}
	

	public function filter_notes($catid){
	    $data['notes'] = $this->generalmodel->admin_notes('0',$catid);
	    $html = $this->load->view('web/ajax_notes',$data,true);		
		echo json_encode(array('success'=>true,'html'=>$html));
	}

    public function switchLang($language = "") {
	    $this->session->set_userdata('site_lang', $language);
	    redirect($_SERVER['HTTP_REFERER']);
	 }


	public function show_session(){
        //$this->set_cartDate_session();
        echo "<pre>"; print_r($_SESSION); exit;
    }

	public function session_destroy(){
       $this->session->sess_destroy();
        echo "<pre>"; print_r($_SESSION); exit;
    }


	 
	 
	 
	public function test(){

	   //$this->load->view('test2',$this->data);
        $data['link'] = site_url('reset_password').'?email=abc@gmail.com&&token=';
        $message = $this->mail_template_view('mail/forgot_pass',$data);
        
        echo $message;

	}


	public function test1(){
	    $vdetail = $this->generalmodel->test();
	    exit;
        // $this->order_success_admin("0059");
          $vdetail = $this->generalmodel->vendor_of_branch('3');
        print_r($vdetail);
        echo $this->db->last_query();
        exit;
	}
	
	
// 	public function test2(){
//         $this->frontview('web/customer_profile',$this->data);

// 	}



    
    
    
}
