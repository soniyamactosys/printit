<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    public function __construct(){
        
        parent::__construct();

        if(!empty($this->session->userdata['admindata'])){
	        $this->admindata = $this->session->userdata['admindata'];
        }else{
        	redirect('adminlogin');
        }
		$this->data['title'] = '';
	    $this->data['menu'] = '';   
	    $this->load->model('vendormodel');     
    }
    
	public function index()
	{
	    //echo encrypt_pass("admin123"); exit;
	    $this->data['title'] = 'Admin Login';
		$this->load->view('admin/admin_login',$this->data);
	}

	public function printery_list(){
	    
	    $where = "(1=1)";
	    
	    if(!empty($type)){
	         $where .= " AND `p.status` = '0'";
	    }
	    $shop_name = $this->input->post('shop_name');
	    if(!empty($shop_name)){
	        $where .= " AND `shop_name` LIKE'%$shop_name%'";
	    }

	    $registeration_date = $this->input->post('registeration_date');
	    if(!empty($registeration_date)){
	        $where .= " AND `registeration_date` like '%".date('Y-m-d',strtotime($registeration_date))."%'";
	    }
	    
	    $status = $this->input->post('status');
	    if(!empty($status)){
	        $where .= " AND `p.status` = '$status'";
	    }

	    $this->data['title'] = 'Printery Shop';
	    $this->data['menu'] = 'all_printery';
	    $this->data['printeryshop_list'] = $this->generalmodel->printeryshop_list($where);

// echo $this->db->last_query();
// 	    echo "<pre>"; print_r($this->data['printeryshop_list']);
// 	    exit;
        $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/printery_list',$this->data);
        $this->load->view('common/footer');
	}
	
	public function printery_list_rejected(){
	    $where = " `p.status` = '2'";

	    $shop_name = $this->input->post('shop_name');
	    if(!empty($shop_name)){
	        $where .= " AND `shop_name` LIKE'%$shop_name%'";
	    }

	    $registeration_date = $this->input->post('registeration_date');
	    if(!empty($registeration_date)){
	        $where .= " AND `registeration_date` like '%".date('Y-m-d',strtotime($registeration_date))."%'";
	    }



	    $this->data['title'] = 'Printery Shop';
	    $this->data['printeryshop_list'] = $this->generalmodel->printeryshop_list($where);
	    $this->data['menu'] = 'rejected_printery';

        $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/printery_list',$this->data);
        $this->load->view('common/footer');
	
	}
	
	public function printery_list_approved(){
	    
	    $where = " `p.status` = '1'";
	    $shop_name = $this->input->post('shop_name');
	    if(!empty($shop_name)){
	        $where .= " AND `shop_name` LIKE'%$shop_name%'";
	    }

	    $registeration_date = $this->input->post('registeration_date');
	    if(!empty($registeration_date)){
	        $where .= " AND `registeration_date` like '%".date('Y-m-d',strtotime($registeration_date))."%'";
	    }

	    $this->data['title'] = 'Printery Shop';
	    $this->data['printeryshop_list'] = $this->generalmodel->printeryshop_list($where);
	    $this->data['menu'] = 'approved_printery';

        $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/printery_list',$this->data);
        $this->load->view('common/footer');
	
	
	}
	
	public function vendor_detail($id){
	    
	    //$id= decoding($id);
	    $this->data['title'] = 'Vendor Detail';
	    //$this->data['vendor_detail'] = $this->generalmodel->vendor_detail($id);
	    $this->data['detail'] = $this->vendormodel->myprofile($id);

        $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/vendor_detail',$this->data);
        $this->load->view('common/footer');
	}
	
	public function delete_printery($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->deleterecord('users',array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}
/*	
		
	public function change_status_printery($id,$status){
	     $id= decoding($id);
	     $this->generalmodel->updaterecord('printery_shop',array('status'=>$status),array('user_id'=>$id));
	}*/
	
//==========Paper Type=================	
	public function paper_type_setting(){
	    $this->data['title'] = 'Printery Management';
	    $this->data['menu'] = 'paper_type';

	    $this->data['paper_type'] = $this->generalmodel->getparticularData("*","paper_type",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/paper_type_setting',$this->data);
        $this->load->view('common/footer');
	}
	public function add_paper_type(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_paper_type')){
	            
	            $data['name'] = $this->input->post('name');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","paper_type",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('paper_type', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}


	public function edit_paper_type(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_paper_type')){
	            $id = decoding($this->input->post('id'));
	            $data['name'] = $this->input->post('name');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","paper_type",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('paper_type', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_paperType_modal($id)
	{
		$id = decoding($id);
		$data['detail'] =  $this->generalmodel->getparticularData("*","paper_type",array("id"=>$id,'status !='=>'2'),"row_array");

		echo $this->load->view('admin/edit_paperType_modal',$data,true);

	}

    public function delete_papertype($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('paper_type',array('status'=>'1'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}



//==========Paper Size=================	

	public function paper_size_setting(){
	    $this->data['title'] = 'Paper Size';
	    $this->data['menu'] = 'paper_size';
	    $this->data['paper_type'] = $this->generalmodel->getparticularData("*","paper_size",array('status !='=>'2'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/paper_size_setting',$this->data);
        $this->load->view('common/footer');
	}

	public function add_paper_size(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_paper_size')){
	            
	            $data['name'] = $this->input->post('name');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","paper_size",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('paper_size', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_paper_size(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_paper_type')){
	            $id = decoding($this->input->post('id'));
	            $data['name'] = $this->input->post('name');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","paper_size",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('paper_size', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

    public function delete_papersize($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('paper_size',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);
	}




//==========banner widht=================	

	public function banner_size(){
	    $this->data['title'] = 'Banner Size';
	    $this->data['menu'] = 'banner_size';
	    $this->data['banner_size'] = $this->generalmodel->getparticularData("*","banner_size",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/banner_size',$this->data);
        $this->load->view('common/footer');
	}

	public function add_banner_size(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_banner_size')){
	            
	            $data['name'] = $this->input->post('name');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","banner_size",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('banner_size', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_banner_size(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_banner_size')){
	            $id = decoding($this->input->post('id'));
	            $data['name'] = $this->input->post('name');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","banner_size",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('banner_size', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

    public function delete_bannersize($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('banner_size',array('status'=>'1'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}


//==========banner widht=================	
//==========Coupon=================	

	public function coupons(){
	    $this->data['title'] = 'Coupon Codes';
	    $this->data['coupon'] = $this->generalmodel->getparticularData("*","coupon",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/coupon',$this->data);
        $this->load->view('common/footer');
	}

	public function add_coupon(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        //echo "<pre>"; print_r($_POST); 
	        if($this->form_validation->run('add_coupon')){
	            
	            $coupon_type = $this->input->post('coupon_type');
	            // $coupon_percent = $this->input->post('coupon_percent');
	            // $amount = $this->input->post('amount');


	            if($coupon_type==1){
	            	$this->form_validation->set_rules('coupon_percent', 'Coupon Percent', 'required|trim|xss_clean');
			        if ($this->form_validation->run()==FALSE){
			        	$return= array('success'=>false,'msg'=>validation_errors());
	        			echo json_encode($return); exit;
			        }else{
		            	$data['amount'] = $this->input->post('coupon_percent');
			        }
	            }else{
					$this->form_validation->set_rules('amount', 'Amount', 'required|trim|xss_clean');
			        if ($this->form_validation->run()==FALSE){
			        	$return= array('success'=>false,'msg'=>validation_errors());
	        			echo json_encode($return); exit;
			        }else{
			        	$data['amount'] = $this->input->post('amount');
			        }
	            }

	            $data['code'] = $this->input->post('code');
	            $data['users'] = $this->input->post('users');
	            
	            $data['type'] = $this->input->post('coupon_type');
	            $data['start_date'] = date('Y-m-d',strtotime($this->input->post('start_date')));
	            $data['end_date'] = date('Y-m-d',strtotime($this->input->post('end_date')));
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            	      //  echo "<pre>"; print_r($data); exit;


	           $already =  $this->generalmodel->getparticularData("*","coupon",array("code"=>$data['code'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('coupon', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_coupon(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_coupon')){
	            $id = decoding($this->input->post('id'));

				$coupon_type = $this->input->post('coupon_type');
	            if($coupon_type==1){
	            	$this->form_validation->set_rules('coupon_percent', 'Coupon Percent', 'required|trim|xss_clean');
			        if ($this->form_validation->run()==FALSE){
			        	$return= array('success'=>false,'msg'=>validation_errors());
	        			echo json_encode($return); exit;
			        }else{
		            	$data['amount'] = $this->input->post('coupon_percent');
			        }
	            }else{
					$this->form_validation->set_rules('amount', 'Amount', 'required|trim|xss_clean');
			        if ($this->form_validation->run()==FALSE){
			        	$return= array('success'=>false,'msg'=>validation_errors());
	        			echo json_encode($return); exit;
			        }else{
			        	$data['amount'] = $this->input->post('amount');
			        }
	            }


                $data['code'] = $this->input->post('code');
	            $data['users'] = $this->input->post('users');               
	            //$data['amount'] = $this->input->post('amount');
	            $data['type'] = $this->input->post('coupon_type');
	            $data['start_date'] = date('Y-m-d',strtotime($this->input->post('start_date')));
	            $data['end_date'] = date('Y-m-d',strtotime($this->input->post('end_date')));

	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","coupon",array("code"=>$data['code'],'status!='=>'2','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('coupon', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

    public function delete_coupon($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('coupon',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);
	}

//==========Membership=================	

	public function membership(){
	    $this->data['title'] = 'Membership';
	    $this->data['menu'] = 'membership';
	    $this->data['membership'] = $this->generalmodel->getparticularData("*","membership",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/membership_list',$this->data);
        $this->load->view('common/footer');
	}

	public function add_membership(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_membership')){
	            
	            $data['title'] = $this->input->post('title');
	            $data['amount'] = $this->input->post('amount');
	            $data['type'] = $this->input->post('period');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","membership",array("title"=>$data['title'],'status !='=>'2'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('membership', $data)){
        	    
        	    //echo $this->db->last_query();
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
    	    $this->data['title'] = 'Add Membership';
    	    $this->data['menu'] = 'membership';
    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/add_membership');
            $this->load->view('common/footer');
	        
	    }
	}

	public function edit_membership($id=""){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_membership')){
	            
	            $id = $this->input->post('id');
                $data['title'] = $this->input->post('title');
	            $data['amount'] = $this->input->post('amount');
	            $data['type'] = $this->input->post('period');
	            $data['description'] = $this->input->post('description');

	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","membership",array("title"=>$data['title'],'status!='=>'2','id !='=>$id),"row_array");
	          
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('membership', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
	        $id= decoding($id);
	        $this->data['data'] =  $this->generalmodel->getparticularData("*","membership",array("id"=>$id,'status !='=>'2'),"row_array");
	        $this->data['title'] = 'Edit Membership';
    	    $this->data['menu'] = 'membership';

    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/edit_membership');
            $this->load->view('common/footer'); 
	    }
	}

    public function delete_membership($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('membership',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}



//==========commission=================	

	public function commission(){
	    $this->data['title'] = 'Commission';
	    $this->data['commission'] = $this->generalmodel->getparticularData("*","commission",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/commission',$this->data);
        $this->load->view('common/footer');
	}

	public function add_commission(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_commission')){
	            
	            $data['percent'] = $this->input->post('commission_percent');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","commission",array("percent"=>$data['percent'],'status !='=>'2'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('commission', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_commission(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_commission')){
	            $id = decoding($this->input->post('id'));
	            $data['percent'] = $this->input->post('commission_percent');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","commission",array("percent"=>$data['percent'],'status!='=>'2','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('commission', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

    public function delete_commission($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('commission',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}

	
//==========Extra Copies=================	
	public function extra_copies(){
	    $this->data['title'] = 'Extra Copies';
	    $this->data['menu'] = 'extra_copies';
	    $this->data['copies'] = $this->generalmodel->getparticularData("*","extra_copies",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/extra_copies',$this->data);
        $this->load->view('common/footer');
	}

	public function add_extra_copies(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        $this->form_validation->set_rules('copy', 'Copies', 'required');
	        if ($this->form_validation->run()){

	            $data['copy_total'] = $this->input->post('copy');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","extra_copies",array("copy_total"=>$data['copy_total'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('extra_copies', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_extra_copies(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        $this->form_validation->set_rules('copy', 'Copies', 'required');
	        if ($this->form_validation->run()){
	            $id = decoding($this->input->post('id'));
                $data['copy_total'] = $this->input->post('copy');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","extra_copies",array("copy_total"=>$data['copy_total'],'status!='=>'2','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('extra_copies', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

    public function delete_extra_copy($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('extra_copies',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}

//==========Extra Copies=================	

    
    public function print_type(){
        
	    $this->data['title'] = 'Print Type';
	    $this->data['menu'] = 'print_type';
	    $this->data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/print_type',$this->data);
        $this->load->view('common/footer');
    }
	public function add_print_type(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_print_type')){
	            
				if(!empty($_FILES['image']['name'])){
		            $upload_data = $this->uploadDoc('image','print_type_images',array('jpg','jpeg','png'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;
		            }else{
		                $data['image'] = $upload_data['file_name'];
		            }
		        }else{
		             $return= array('success'=>false,'msg'=>'Please Upload Image');
		             echo json_encode($return);
		             exit;
		        }
		        
	            $data['name'] = $this->input->post('name');
	            $data['description'] = $this->input->post('description');
	            $data['url'] = str_replace(' ','-',$this->input->post('name'));
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","print_type",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('print_type', $data)){
        	        $return= array('success'=>true,'msg'=>'Added Successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}
    
    /*
	public function print_management(){
	    
	    $this->data['title'] = 'Print management';
	    $this->data['menu'] = 'print_management';
	    $this->data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
	    $this->data['paper_type'] = $this->generalmodel->getparticularData("*","paper_type",array('status'=>'0'),"result_array");
	    $this->data['paper_size'] = $this->generalmodel->getparticularData("*","paper_size",array('status'=>'0'),"result_array");
	    $this->data['management'] = $this->generalmodel->print_management_records();
	    
	   // echo $this->db->last_query();
	   // echo "<pre>"; print_r($this->data['management']); exit;
	    
	    
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/print_management',$this->data);
        $this->load->view('common/footer');
	}
	public function add_print_management(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_print_mangement')){
	            
				$where = array('status !='=>'2');
	            $data['order_type'] = $where['order_type'] = $this->input->post('order_type');
	            if($data['order_type']=='1'){
	            	$this->form_validation->set_rules('print_type', 'Print Type', 'required|trim|xss_clean');
	            	if($this->form_validation->run() == FALSE){
	            		$return= array('success'=>false,'msg'=>validation_errors());
	            		echo json_encode($return); exit;
	            	}
	            }
				
	            $data['paper_type_id'] = $where['paper_type_id'] = $this->input->post('paper_type');
	            $data['paper_size_id'] = $where['paper_size_id'] = $this->input->post('paper_size');
	            $data['print_type'] =  $this->input->post('print_type');
	            $data['printing_sides'] =  $this->input->post('sides');
	            $data['width'] =  $this->input->post('width');
	            $data['color'] =  $this->input->post('color');
	            $data['binding'] =  $this->input->post('binding');
	            $data['average_price'] = $this->input->post('avg_price');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            

	            if($data['order_type']=='1'){
	            	$where['print_type'] =$data['print_type'];
	            	if($data['print_type']=='2'){
	            		$where['printing_sides'] =$data['printing_sides'];
	            	}
	            }else{
					$where['width'] =$data['width'];
					$where['color'] =$data['color'];
					$where['binding'] =$data['binding'];
					$where['printing_sides'] =$data['printing_sides'];
	            }

	            $already =  $this->generalmodel->getparticularData("*","print_management",$where,"row_array");

	            //echo "<pre>"; print_r($_POST);
	            //echo $this->db->last_query(); 
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('print_management', $data)){
        	        $return= array('success'=>true,'msg'=>'added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	*/

	public function price_combination(){
	    
	    $this->data['title'] = 'Price Combination';
	    $this->data['menu'] = 'price_combination';
	    $this->data['services'] = $this->generalmodel->getparticularData("*","services",array('status'=>'0'),"result_array");
	    $this->data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
	    $this->data['paper_type'] = $this->generalmodel->getparticularData("*","paper_type",array('status'=>'0'),"result_array");
	    $this->data['paper_size'] = $this->generalmodel->getparticularData("*","paper_size",array('status'=>'0'),"result_array");
	    $this->data['management'] = $this->generalmodel->price_combination_records();
		
		$this->data['banner_size'] = $this->generalmodel->getparticularData("*","banner_size",array('status'=>'0'),"result_array");	    
	   // echo $this->db->last_query();
	   // echo "<pre>"; print_r($this->data['management']); exit;
	    
	    
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/price_combination_list',$this->data);
        $this->load->view('common/footer');
	}
	public function add_price_combination(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_print_mangement')){
	            
				$where = array('status !='=>'2');
	            $data['service_id'] = $where['service_id'] = $this->input->post('order_type');
	            if($data['service_id']=='1'){
	            	$this->form_validation->set_rules('print_type', 'Print Type', 'required|trim|xss_clean');
	            	if($this->form_validation->run() == FALSE){
	            		$return= array('success'=>false,'msg'=>validation_errors());
	            		echo json_encode($return); exit;
	            	}
	            }
				
	            $data['paper_type_id'] = $where['paper_type_id'] = $this->input->post('paper_type');
	            $data['paper_size_id'] = $where['paper_size_id'] = $this->input->post('paper_size');
	            $data['print_type'] =  $this->input->post('print_type');
	            $data['printing_sides'] =  $this->input->post('sides');
	            $data['banner_size'] =  $this->input->post('width');
	            $data['color'] =  $this->input->post('color');
	            $data['binding'] =  $this->input->post('binding');
	            $data['extra_copies'] = $this->input->post('extra_copies');
	            //$data['average_price'] = $this->input->post('avg_price');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            

	            if($data['service_id']=='1'){
	            	$where['print_type'] =$data['print_type'];
	            	if($data['print_type']=='2'){
	            		$where['printing_sides'] =$data['printing_sides'];
	            		$where['extra_copies'] =$data['extra_copies'];
	            	}
	            	if($data['print_type']=='4'){
	            		unset($where['paper_size_id']);
	            		$where['banner_size'] =$data['banner_size'];
	            	}
	            }else{
					$where['banner_size'] =$data['banner_size'];
					$where['color'] =$data['color'];
					$where['binding'] =$data['binding'];
					$where['printing_sides'] =$data['printing_sides'];
	            }


	            $already =  $this->generalmodel->getparticularData("*","price_combination",$where,"row_array");

	            // echo "<pre>"; print_r($_POST);
	            // echo $this->db->last_query(); 
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	$query = $this->generalmodel->add('price_combination', $data);
	            	// echo $this->db->last_query();
	            	// print_r($query);

		            if($query){
	        	        $return= array('success'=>true,'msg'=>'Added successfully');
		            }else{
	        	        $return= array('success'=>false,'msg'=>'Something went wrong');
		            }
	        	}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function edit_print_management(){

	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        $id = decoding($this->input->post('id'));
	        $where = array('status !='=>'2','id !='=>$id);
	        if($this->form_validation->run('add_print_mangement')){

	            $data['order_type'] = $where['order_type'] = $this->input->post('order_type');
	            
	            if($data['order_type']=='1'){
	            	$this->form_validation->set_rules('print_type', 'Print Type', 'required|trim|xss_clean');
	            	if($this->form_validation->run() == FALSE){
	            		$return= array('success'=>false,'msg'=>validation_errors());
	            		echo json_encode($return); exit;
	            	}
	            }

	            $data['paper_type_id'] = $where['paper_type_id'] = $this->input->post('paper_type');
	            $data['paper_size_id'] = $where['paper_size_id'] = $this->input->post('paper_size');
	            $data['print_type'] = $where['print_type'] = $this->input->post('print_type');
	            $data['printing_sides'] =  $this->input->post('sides');
	            $data['width'] =  $this->input->post('width');
	            $data['color'] =  $this->input->post('color');
	            $data['binding'] =  $this->input->post('binding');	            
	            $data['average_price'] = $this->input->post('avg_price');
	            $data['updated_at'] = date('Y-m-d h:i:s');
	            
	           // $where =array('paper_type_id'=>$data['paper_type_id'],'paper_size_id'=>$data['paper_size_id'],'print_type'=>$data['print_type'],'status!='=> '2', 'id !='=>$id);

	            if($data['order_type']=='1'){
	            	$where['print_type'] =$data['print_type'];
	            	if($data['print_type']=='2'){
	            		$where['printing_sides'] =$data['printing_sides'];
	            	}
	            }else{
					$where['width'] =$data['width'];
					$where['color'] =$data['color'];
					$where['binding'] =$data['binding'];
					$where['printing_sides'] =$data['printing_sides'];
	            }
	            $already =  $this->generalmodel->getparticularData("*","print_management",$where,"row_array");

	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            if($this->generalmodel->updaterecord('print_management', $data,array('id'=>$id))){
        	        $return= array('success'=>true,'msg'=>'Updated successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}
	
	public function delete_printmanagement($id){
		$id= decoding($id);
		//$query = $this->generalmodel->updaterecord('print_management',array('status'=>'2'),array('id'=>$id));
		$query = $this->generalmodel->updaterecord('price_combination',array('status'=>'2'),array('id'=>$id));
		//echo $this->db->last_query();
		if(!empty($query)){
			$return = array('success'=>true,'msg'=>'Updated Successfully');
		}else{
			$return = array('success'=>false,'msg'=>'delete failed');
		}
		echo json_encode($return);
	}

	public function view_price_management($id){
		$id= decoding($id);
	    $this->data['title'] = 'Print management';
	    $this->data['menu'] = 'print_management';
		$this->data['detail'] =  $this->generalmodel->print_management_records(array('pm.status !='=>'2','pm.id'=>$id));
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav',$this->data);
        $this->load->view('admin/view_price_management',$this->data);
        $this->load->view('common/footer');
	}

	public function stationary(){
		$this->data['title'] = 'Stationary';
	    $this->data['menu'] = 'stationary';
	    $this->data['products'] = $this->generalmodel->getparticularData("*","products",array('status !='=>'2'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav',$this->data);
        $this->load->view('admin/products',$this->data);
        $this->load->view('common/footer');
	}


	public function add_stationary(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
//echo "<pre>"; print_r($_POST);
	        
	        if($this->form_validation->run('add_product')){
				
				if(!empty($_FILES['image']['name'])){
		            $upload_data = $this->uploadDoc('image','products',array('jpg','jpeg','png'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;
		            }else{
		                $image = $upload_data['file_name'];
		            }
		        }

				$created_by = $this->admindata['user_id'];
		        $price = $this->input->post('price');
		        $qty = $this->input->post('qty');
	            $data['name'] = $this->input->post('name');
	            $data['price'] = $price;
	            $data['qty'] = $qty;
	            $data['image'] = $image;
	            $data['created_by'] = $created_by;
	            $data['created_at'] = date('Y-m-d h:i:s');
	            $data['updated_by'] = $created_by;
	            $data['updated_at'] = date('Y-m-d h:i:s');

	           	$already =  $this->generalmodel->getparticularData("*","products",array("name"=>$data['name'],'status !='=>'2'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	$stockdata['type'] = '1';
	            	$stockdata['qty'] = $qty;
	            	$stockdata['added_date'] = date('Y-m-d h:i:s');
		            $product_id = $this->generalmodel->add_product($data,$stockdata);
		            if(!empty($product_id)){
	        	        $return= array('success'=>true,'msg'=>'Added successfully');
		            }else{
	        	        $return= array('success'=>false,'msg'=>'Something went wrong');
		            }
	        	}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
    	    $this->data['title'] = 'Add Stationary';
    	    $this->data['menu'] = 'stationary';
    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/add_product');
            $this->load->view('common/footer');
	        
	    }
	}

	public function edit_stationary($id=""){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_product')){

				if(!empty($_FILES['image']['name'])){
		            $upload_data = $this->uploadDoc('image','products',array('jpg','jpeg','png'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;	                
		            }else{
		                $data['image'] =$upload_data['file_name'];
		            }
		        }

	            $id = $this->input->post('id');
				$created_by = $this->admindata['user_id'];
		        $price = $this->input->post('price');
	            $qty = $this->input->post('qty');
	            $data['name'] = $this->input->post('name');
	            $data['price'] = $price;
	            $data['qty'] = $qty;
	            $data['updated_by'] = $created_by;
	            $data['updated_at'] = date('Y-m-d h:i:s');

            	$stockdata['type'] = '3';
            	$stockdata['qty'] = $qty;
            	$stockdata['product_id'] = $id;
            	$stockdata['added_date'] = date('Y-m-d h:i:s');

            
	            $already =  $this->generalmodel->getparticularData("*","products",array("name"=>$data['name'],'status!='=>'2','id !='=>$id),"row_array");
	          
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{

    	            if(!empty($this->generalmodel->update_product($data,$stockdata,array('id'=>$id)))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
	        $id= decoding($id);
	        $this->data['data'] =  $this->generalmodel->getparticularData("*","products",array("id"=>$id,'status !='=>'2'),"row_array");
	        $this->data['title'] = 'Edit Stationary';
    	    $this->data['menu'] = 'stationary';
	        
    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/edit_product');
            $this->load->view('common/footer'); 
	    }
	}

    public function delete_stationary($id){
	     $id= decoding($id);
	     $query = $this->generalmodel->updaterecord('products',array('status'=>'2'),array('id'=>$id));
	     if(!empty($query)){
	         $return= array('success'=>true,'msg'=>'Deleted Successfully');
	     }else{
	         $return= array('success'=>false,'msg'=>'Something went wrong');
	     }
	     echo json_encode($return);	     
	}

	public function orders($type=""){
		$this->data['title'] = 'Orders';
	    $this->data['menu'] = 'orders';
	    if(!empty($type)){
	    	$where = "`orders`.`status`='1' AND `orders`.`payment_status`='1'";
	    }else{
			$where = "`orders.status`='1'";
	    }

	    /*$buyer = $this->input->post('buyer');
	    if(!empty($buyer)){
	        $where .= " AND `orders.customer` = '$buyer' ";
	    }

	    $vendor = $this->input->post('vendor');
	    if(!empty($vendor)){
	        $where .= " AND `orders.vendor` = '$vendor'";
	    }	*/   

	    $buyer = $this->input->post('buyer');
	    if(!empty($buyer)){
	        $where .= " AND (`u.firstname` LIKE '%$buyer%' OR `u.lastname` LIKE '%$buyer%')";
	    }

	    $vendor = $this->input->post('vendor');
	    if(!empty($vendor)){
	        $where .= " AND (`uv.firstname` LIKE '%$vendor%' OR `uv.lastname` LIKE '%$vendor%')";
	    }	


	    $order_from = $this->input->post('order_from');
	    if(!empty($order_from)){
	        $where .= " AND `orders.created_at` >= '$order_from'";
	    }

	    $order_to = $this->input->post('order_to');
	    if(!empty($order_to)){
	        $where .= " AND `orders.created_at` <= '$order_to'";
	    }	     

// echo "<pre>"; print_r($_POST);
// echo $where;
// exit;
	    // $registeration_date = $this->input->post('registeration_date');
	    // if(!empty($registeration_date)){
	    //     $where .= " AND `registeration_date` like '%".date('Y-m-d',strtotime($registeration_date))."%'";
	    // }
	    


	   // $this->data['orders'] = $this->generalmodel->getparticularData("*","orders",$where,"result_array");

	    $tables[0]['table'] = 'users as u';
	    $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

	    $tables[1]['table'] = 'users as uv';
	    $tables[1]['on'] = 'uv.id = orders.vendor AND uv.role="2" AND uv.delete_status="0"';

	    $this->data['orders'] = $this->generalmodel->getfrommultipletables("*,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,CONCAT_WS(' ',uv.firstname,uv.lastname) AS vendor_name,orders.created_at as order_date,orders.id as order_number ",'orders',$tables,$where,"","orders.id","","","result_array");


	    // echo $this->db->last_query();
	    // echo "<pre>"; print_r($this->data['orders']);
	    // exit;

	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav',$this->data);
        $this->load->view('admin/orders',$this->data);
        $this->load->view('common/footer');
	}

	public function order_detail($oid){
		
		$id= decoding($oid);
		$where = array('orders.status'=>'1','orders.id'=>$id);

	    $tables[0]['table'] = 'users as u';
	    $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

	    $tables[1]['table'] = 'users as uv';
	    $tables[1]['on'] = 'uv.id = orders.vendor AND uv.role="2" AND uv.delete_status="0"';

	    $tables[2]['table'] = 'paper_type as ptype';
	    $tables[2]['on'] = 'orders.paper_type = ptype.id AND ptype.status!="2"';

	    $tables[3]['table'] = 'paper_size as psize';
	    $tables[3]['on'] = 'orders.paper_size = psize.id AND psize.status!="2"';

	    $this->data['detail'] = $this->generalmodel->getfrommultipletables("*,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,CONCAT_WS(' ',uv.firstname,uv.lastname) AS vendor_name,orders.created_at as order_date,orders.id as order_number,psize.name AS papersize,ptype.name as papertype ",'orders',$tables,$where,"","orders.id","","","row_array");		


//echo $this->db->last
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav',$this->data);
        $this->load->view('admin/order_detail',$this->data);
        $this->load->view('common/footer');
	}

	public function customers($status=""){
	    $this->data['title'] = 'Customers';
	    $this->data['menu'] = 'customers';

	   // echo $status; exit;
	    if($status=='active'){
	    	$statuss = '0';
	    }else{
			$statuss = '1';
	    }
	    $this->data['customers'] = $this->generalmodel->getparticularData("*,CONCAT_WS(' ',firstname,lastname) AS full_name","users",array('status'=>$statuss,'role'=>'3','delete_status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/customers',$this->data);
        $this->load->view('common/footer');

	}

	public function services(){
	    $this->data['title'] = 'Services Setting';
	    $this->data['menu'] = 'services';

	    $this->data['services'] = $this->generalmodel->getparticularData("*","services",array('status !='=>'2'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/service_setting',$this->data);
        $this->load->view('common/footer');		
	}
	public function edit_service_modal($id)
	{
		$id = decoding($id);
		$data['detail'] =  $this->generalmodel->getparticularData("*","services",array("id"=>$id,'status !='=>'2'),"row_array");

		echo $this->load->view('admin/edit_service_modal',$data,true);

	}

	public function edit_service(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_service')){
	            $id = decoding($this->input->post('id'));
	            $data['name'] = $this->input->post('name');
	            $data['description'] = $this->input->post('description');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","services",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('services', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }
	}

	public function vendor_status_update(){
		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			$vid = decoding($this->input->post('vendor_id'));
			$status = $this->input->post('status');
			if($status=='pending'){ $st = '0'; }
			elseif($status=='approve'){ $st = '1'; }
			}elseif($status=='reject'){ $st = '2'; }
 			
 			$query = $this->generalmodel->updaterecord('printery_shop',array('status'=>$st),array('id'=>$vid));
 			if(!empty($query)){
 				$return = array('success'=>true,'msg'=>'done');
 			}else{
 				$return = array('success'=>false,'msg'=>'internal error');
 			}
 			echo json_encode($return);
	}
	


	public function vp_status_update(){
		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			$vid = decoding($this->input->post('vendor_id'));
			$status = $this->input->post('status');
 			$query = $this->generalmodel->updaterecord('printery_shop',array('price_status'=>$status),array('id'=>$vid));
 			if(!empty($query)){
 				$return = array('success'=>true,'msg'=>'done');
 			}else{
 				$return = array('success'=>false,'msg'=>'internal error');
 			}
 			echo json_encode($return);
		}
	}
	
	public function edit_price_modal($id){
		$id = decoding($id);
        $data['services'] = $this->generalmodel->getparticularData("*","services",array('status'=>'0'),"result_array");
	    $data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
	    $data['paper_type'] = $this->generalmodel->getparticularData("*","paper_type",array('status'=>'0'),"result_array");
	    $data['paper_size'] = $this->generalmodel->getparticularData("*","paper_size",array('status'=>'0'),"result_array");
	    //$data['management'] = $this->generalmodel->price_combination_records();
		
		$data['banner_size'] = $this->generalmodel->getparticularData("*","banner_size",array('status'=>'0'),"result_array");	    
		
		$data['detail'] =  $this->generalmodel->getparticularData("*","price_combination",array("id"=>$id,'status !='=>'2'),"row_array");

//echo  "<pre>"; print_r($data['detail']); exit;
		echo $this->load->view('admin/edit_price_comb_modal',$data,true);	    
	}

	public function view_branch_pricelist($branchid){
	    $this->data['title'] = 'Services Setting';
	    $this->data['menu'] = 'services';

	    $id = decoding($branchid);
	    //$id = $branchid;


		$where = "pc.status = '0' AND vp.vendor_id = $id AND pc.service_id = '1'";
		$this->data['pricelist'] = $this->db->select('serv.name as service,size.name as size_name,print.name as print,paper.name as paper,vp.price_comb_id,vp.price,pc.id as combi_id,bsize.name as banner_size_name,pc.*,vp.*')
		->from('price_combination as pc')
		->join('services as serv',"serv.id = pc.service_id",'left')
		->join('print_type as print',"print.id = pc.print_type",'left')
		->join('paper_type as paper',"paper.id = pc.paper_type_id",'left')
		->join('paper_size as size',"size.id = pc.paper_size_id",'left')
		->join('banner_size as bsize',"bsize.id = pc.banner_size",'left')
		->join('vendor_prices as vp',"vp.price_comb_id = pc.id AND vp.status='0'",'left')
		->where($where)
		->get()
		->result_array();
		
		$whereqk = "pc.status = '0' AND vp.vendor_id = $id AND pc.service_id = '2'";
		$this->data['pricelistquick'] = $this->db->select('serv.name as service,size.name as size_name,print.name as print,paper.name as paper,vp.price_comb_id,vp.price,pc.id as combi_id,bsize.name as banner_size_name,pc.*,vp.*')
		->from('price_combination as pc')
		->join('services as serv',"serv.id = pc.service_id",'left')
		->join('print_type as print',"print.id = pc.print_type",'left')
		->join('paper_type as paper',"paper.id = pc.paper_type_id",'left')
		->join('paper_size as size',"size.id = pc.paper_size_id",'left')
		->join('banner_size as bsize',"bsize.id = pc.banner_size",'left')
		->join('vendor_prices as vp',"vp.price_comb_id = pc.id AND vp.status='0'",'left')
		->where($whereqk)
		->get()
		->result_array();
		// echo $this->db->last_query();
		// echo "<pre>"; print_r($this->data['pricelist']); 		
		// exit;
        $this->data['branchDetail'] = $this->generalmodel->getparticularData("*","printery_shop",array('id'=>$id),"row_array");
        $where = "npr.status = '0' AND npr.printery_shop_id = ".$id;
			$this->data['notes_price'] = $this->db->select('*')
			->from('notes_price_range as npr')
			->where($where)
			->get()
			->result_array();
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/branch_pricelist',$this->data);
        $this->load->view('common/footer');		}

	public function test(){
	    $this->load->view('common/header');
	    //$this->load->view('testview');
	    $this->load->view('test2');
	    $this->load->view('common/footer');
	}

  //==========anusha Notes category and notes=================	
	public function note_category(){
	    $this->data['title'] = 'Notes Categories';
	    $this->data['menu'] = 'note_category';

	    $this->data['note_category'] = $this->generalmodel->getparticularData("*","notes_categories",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/note_category',$this->data);
        $this->load->view('common/footer');
	}
	public function add_notes_category(){
	    
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	           
	            $data['name'] = $this->input->post('name'); 
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","notes_categories",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('notes_categories', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	      
	        echo json_encode($return);
	    }
	}
	public function edit_notes_cat_modal($id)
	{
		$id = decoding($id);
		$data['detail'] =  $this->generalmodel->getparticularData("*","notes_categories",array("id"=>$id),"row_array");

		echo $this->load->view('admin/edit_notesCategory_modal',$data,true);

	}
	public function edit_notes_category(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	       
	            $id = decoding($this->input->post('id'));
	            $data['name'] = $this->input->post('name');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	            $already =  $this->generalmodel->getparticularData("*","notes_categories",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
    	            if($this->generalmodel->updaterecord('notes_categories', $data,array('id'=>$id))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	        
	        echo json_encode($return);
	    }
	}
	public function note(){
	    $this->data['title'] = 'Notes';
	    $this->data['menu'] = 'note';

	    $this->data['note'] = $this->generalmodel->getparticularData("*","notes",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/note',$this->data);
        $this->load->view('common/footer');
	}
	public function add_notes(){
	     $this->data['title'] = 'Add Notes';
    	    $this->data['menu'] = 'note';
    	   $this->data['note_cat'] = $this->generalmodel->getparticularData("*","notes_categories",array('status'=>'0'),"result_array");
	   
    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/add_notes',$this->data);
            $this->load->view('common/footer');
	}

	public function add_noteaction(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){

	        if(!empty($_FILES['image']['name'])){
		            $upload_data = $this->uploadDoc('image','uploaded_notes/images',array('jpg','jpeg','png'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;
		            }else{
		                $image = $upload_data['file_name'];
		            }
		        }
                if(!empty($_FILES['document']['name'])){
		            $upload_data = $this->uploadDoc('document','uploaded_notes',array('pdf'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;
		            }else{
		                $document = $upload_data['file_name'];
		            }
		        }
				$created_by = $this->admindata['user_id'];
		        $name = $this->input->post('name');
		        $note_cat = $this->input->post('note_cat');
		        $description = $this->input->post('description');
	            $data['category'] = $note_cat;
	            $data['name'] = $name;
	            $data['description'] = $description;
	            $data['total_pages'] = $this->input->post('total_pages');
	            $data['image'] = $image;
	            $data['document'] = $document;
	            $data['created_at'] = date('Y-m-d h:i:s');
	            $already =  $this->generalmodel->getparticularData("*","notes",array("name"=>$data['name']),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	$note_id = $this->generalmodel->add('notes',$data);
		            if(!empty($note_id)){
	        	        $return= array('success'=>true,'msg'=>'Added successfully');
		            }else{
	        	        $return= array('success'=>false,'msg'=>'Something went wrong');
		            }
	        	}
	        
	        echo json_encode($return);
	    }
	}
	public function edit_note($id=""){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	       		if(!empty($_FILES['image']['name'])){
		            $upload_data = $this->uploadDoc('image','uploaded_notes/images',array('jpg','jpeg','png'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;	                
		            }else{
		                $data['image'] =$upload_data['file_name'];
		            }
		        }
		        if(!empty($_FILES['document']['name'])){
		            $upload_data = $this->uploadDoc('document','uploaded_notes',array('pdf'));
		            if(!empty($upload_data['error'])){
		                $return = array('success'=>false,'msg'=>$upload_data['error']);
		                echo json_encode($return); exit;	                
		            }else{
		                $data['document'] =$upload_data['file_name'];
		            }
		        }

	            $id = $this->input->post('id');
				$created_by = $this->admindata['user_id'];
		        $name = $this->input->post('name');
		        $note_cat = $this->input->post('note_cat');
		        $description = $this->input->post('description');
	            $data['category'] = $note_cat;
	            $data['name'] = $name;
	            $data['total_pages'] = $this->input->post('total_pages');
	            $data['description'] = $description;
	            $data['created_at'] = date('Y-m-d h:i:s');

            
	            $already =  $this->generalmodel->getparticularData("*","notes",array("name"=>$data['name'],'status'=>'0','id !='=>$id),"row_array");
	          
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{

    	            if(!empty($this->generalmodel->updaterecord('notes',$data,array('id'=>$id)))){
            	        $return= array('success'=>true,'msg'=>'Updated successfully');
    	            }else{
            	        $return= array('success'=>false,'msg'=>'Something went wrong');
    	            }
	            }
	       
	        echo json_encode($return);
	    }else{
	        $id= decoding($id);
	        $this->data['data'] =  $this->generalmodel->getparticularData("*","notes",array("id"=>$id),"row_array");
	        $this->data['title'] = 'Edit Note';
    	    $this->data['menu'] = 'note';
	        $this->data['note_cat'] = $this->generalmodel->getparticularData("*","notes_categories",array('status'=>'0'),"result_array");
	   
    	    $this->load->view('common/header',$this->data);
            $this->load->view('common/left_nav');
            $this->load->view('admin/edit_note');
            $this->load->view('common/footer'); 
	    }
	}
		public function view_notes_modal($id)
	{
		$id = decoding($id);
		$data['detail'] =  $this->generalmodel->getparticularData("*","notes",array("id"=>$id),"row_array");

		echo $this->load->view('admin/view_notes_modal',$data,true);

	}
	public function translation_lng(){
	    $this->data['title'] = 'translation Language';
	    $this->data['menu'] = 'translation_lng';

	    $this->data['translation_lng'] = $this->generalmodel->getparticularData("*","translation_lang",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/translation_lng',$this->data);
        $this->load->view('common/footer');
	}
		public function add_tran_lng(){
	    
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	           
	            $data['name'] = $this->input->post('name'); 
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","translation_lang",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('translation_lang', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	      
	        echo json_encode($return);
	    }
	}
	public function translation_type(){
	    $this->data['title'] = 'translation Type';
	    $this->data['menu'] = 'translation_type';

	    $this->data['translation_type'] = $this->generalmodel->getparticularData("*","translation_types",array('status'=>'0'),"result_array");
	    $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav');
        $this->load->view('admin/translation_type',$this->data);
        $this->load->view('common/footer');
	}
		public function add_tran_type(){
	    
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	           
	            $data['name'] = $this->input->post('name'); 
	            $data['created_at'] = date('Y-m-d h:i:s');
	            
	           $already =  $this->generalmodel->getparticularData("*","translation_types",array("name"=>$data['name'],'status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            
	            if($this->generalmodel->add('translation_types', $data)){
        	        $return= array('success'=>true,'msg'=>'Added successfully');
	            }else{
        	        $return= array('success'=>false,'msg'=>'Something went wrong');
	            }}
	      
	        echo json_encode($return);
	    }
	}
		public function notes_price_status_update(){
		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			$vid = decoding($this->input->post('note_id'));
			$status = $this->input->post('status');
 			$query = $this->generalmodel->updaterecord('notes_price_range',array('price_status'=>$status),array('id'=>$vid));
 			if(!empty($query)){
 				$return = array('success'=>true,'msg'=>'done');
 			}else{
 				$return = array('success'=>false,'msg'=>'internal error');
 			}
 			echo json_encode($return);
		}
	}
    	public function custome_price_status_update(){
		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			$vid = decoding($this->input->post('cid'));
			$status = $this->input->post('status');
 			$query = $this->generalmodel->updaterecord('vendor_prices',array('price_status'=>$status),array('id'=>$vid));
 			if(!empty($query)){
 				$return = array('success'=>true,'msg'=>'done');
 			}else{
 				$return = array('success'=>false,'msg'=>'internal error');
 			}
 			echo json_encode($return);
		}
	}
}
