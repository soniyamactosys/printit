<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }
    
    public function set_ordertype(){
        if(!empty($this->input->post('order_type'))){
            $this->session->set_userdata('order_detail',array('order_type'=>$this->input->post('order_type')));
        }
    }
    
    public function set_papertype(){
        if(!empty($this->input->post('paper_type'))){
            $this->session->set_userdata('order_detail',array('paper_type'=>$this->input->post('paper_type')));
        }
    }

    public function set_papersize(){
        if(!empty($this->input->post('paper_size'))){
            $this->session->set_userdata('order_detail',array('paper_size'=>$this->input->post('paper_size')));
        }
    }
    
    public function set_copy_number(){
        if(!empty($this->input->post('copy_number'))){
            $this->session->set_userdata('order_detail',array('copy_number'=>$this->input->post('copy_number')));
        }
    }
    
        
    public function set_imageupload(){
        print_r($_FILES); exit;
        
        if(!empty($this->input->post('copy_number'))){
            $this->session->set_userdata('order_detail',array('copy_number'=>$this->input->post('copy_number')));
        }
    }
    
}