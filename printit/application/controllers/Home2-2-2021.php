<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->userdata = $this->session->userdata('userdata');
		if($this->userdata){
			$this->data['footer_data']['in_progress_orders'] = $this->generalmodel->orders_status();
		}       
    }
    
	public function index()
	{
		destroy_order_in_session();
		/*	
			if($this->session->userdata('order_confirmed')){
			$this->session->unset_userdata(array('order_type','print_type','paper_type','paper_size','copy_number','printing_side','uploaded_image','selected_printry','current_order','pickup_delivery','coupon_applied','print_type'));
		}*/
		// if($this->userdata){
		// 	$this->data['footer_data']['in_progress_orders'] = $this->generalmodel->orders_status();
		// }
		$this->frontview('web/home',$this->data);
	}
    
	public function select_service_type(){
        $where = "`p.status` = '0'";
        $this->data['local_printery']  = $this->generalmodel->printeryshop_list($where);
	    $this->frontview('web/select_service_type',$this->data);
	}

	/*public function set_service_type($type=""){
	    if(!empty($type)){
	        $this->session->set_userdata('service_type',$type);
            redirect('/');
	    }
	}*/
	

	public function set_service_type($type=""){
	    if(!empty($type) && $this->input->is_ajax_request()){
	        $this->session->set_userdata('service_type',$type);
	        $return = array('success'=>true);
	    }else{
	    	$return = array('success'=>false);
	    }
	    echo json_encode($return);
	}	
	//=======ORDER TYPES =============
	public function custom_print(){
		destroy_order_in_session();
		if($this->userdata){ $this->session->set_userdata('order_type','1'); }
		$this->data['print_types'] = $this->generalmodel->getparticulardata('*','print_type',array('status'=>'0'),'result_array');
		$this->frontview('web/custom_print',$this->data);
	}
	
	public function quickprint(){
		if($this->userdata){ $this->session->set_userdata('order_type','2'); }
	    $this->data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $this->data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');		
		$this->frontview('web/quickprint',$this->data);
	    
	}

	public function translation(){
		if($this->userdata){ $this->session->set_userdata('order_type','3'); }
		$this->data['trans_types'] = $this->generalmodel->getparticularData('id,name','translation_types',array('status'=>'0'),'result_array');
		$this->data['language'] = $this->generalmodel->getparticularData('id,name','translation_lang',array('status'=>'0'),'result_array');
		$this->frontview('web/translation',$this->data);
	}

	
	public function notes(){
		if($this->userdata){ $this->session->set_userdata('order_type','4'); }
	    $this->data['notes'] = $this->generalmodel->getparticulardata('*','notes',array('status'=>'0','is_delete'=>'0'),'result_array');
	    $this->frontview('web/notes',$this->data);
	}
	//=======ORDER TYPES =============


	public function printry_shops(){
	    
		if(empty($this->userdata)){ redirect('/'); }
		$current_order = $this->session->userdata('current_order');
		$oid = $this->session->userdata('continue_order');
		$this->data['list'] = $this->printery_withprice();

		update_order_steps($oid,current_url());
		$this->frontview('web/printry_list_page',$this->data);
	}

/*	public function get_printery_charge($oid,$printery_id){
		$vendor_charge = 0;
	    $order_detail = $this->generalmodel->order_detail($oid,"0");
        $print_type = $order_detail['print_type'];
        $paper_type = $order_detail['paper_type'];
        $paper_size = $order_detail['paper_size'];
        $copy_number = $order_detail['no_of_copies'];
        $printing_side = $order_detail['printing_side'];
        $width = $order_detail['width'];

    	$price_where = "`status`='0' AND `printery_shop_id`=".$printery_id." AND `service_id`=".$order_detail['order_type'];

    	if($order_detail['order_type']==1){
    		
			$price_where .= " AND `print_type`=".$print_type." AND `paper_type_id`=".$paper_type;	

			if($order_detail['print_type']==1){
				$price_where .= " AND `paper_size_id` =".$paper_size;

    		}elseif($order_detail['print_type']==2){
				$price_where .= " AND `paper_size_id` =".$paper_size." AND 	`printing_sides`='".$printing_side."'";

    		}elseif($order_detail['print_type']==3){
				$price_where .= " AND `paper_size_id` =".$paper_size;

    		}elseif($order_detail['print_type']==4){
				$price_where .= " AND `width` =".$width;
    		}
    	}

    	$avg_priceArr  = $this->generalmodel->get_avg_price($price_where);

			// echo "<pre>";
			// 	if($printery_id=='16' || $printery_id=='17'){
			// 	echo $this->db->last_query();
			// 	print_r($avg_priceArr);
			// 	echo "<br><br>";
			// 	}

    	if(!empty($avg_priceArr)){ 
    		$avg_id = $avg_priceArr['id']; 

    		$price_range_where = "`print_management_id`='".$avg_id."' AND `copy_from` <=".$copy_number." AND `copy_to`>='".$copy_number."' AND `status`='0'";
    		$copy_priceArr  = $this->generalmodel->get_price_range($price_range_where);

				// if($printery_id=='17' || $printery_id=='16'){
				// print_r($shop['user_id']);
				// echo $this->db->last_query();
				// print_r($copy_priceArr);
				// echo "<br><br>";
				// }
    		if(!empty($copy_priceArr)){
    			$vendor_charge = $copy_priceArr['price'];
    		}else{
				$vendor_charge = $avg_priceArr['average_price'];
    		}

    	}
    	return 	$vendor_charge;	
	}*/



	public function print_type($type){

		if($this->userdata){ 
			// $sess_print_type = $this->session->userdata('print_type');
			// if(!empty($sess_print_type) && $sess_print_type!=$type){
			// 	$this->session->unset_userdata(array('paper_type','paper_size','printing_side','copy_number','uploaded_image')); 
			// }
			$this->session->set_userdata('order_type','1');
			$this->session->set_userdata('print_type',$type); 
		}
	    $this->data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $this->data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');
	    $this->data['banner_size'] = $this->generalmodel->getparticularData('id,name','banner_size',array('status'=>'0'),'result_array');
		$this->frontview('web/'.$type,$this->data);

	}

	

		/*
	public function poster(){
		if($this->userdata){ $this->session->set_userdata('custom_print_option','poster'); }

	    $data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');
		$this->frontview('web/poster',$data);
	}

	public function flyer(){
		$this->session->set_userdata('custom_print_option','flyer');
	    $data['papersize'] = $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0'),'result_array');
	    $data['papertype'] = $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0'),'result_array');		
		$this->frontview('web/flyer',$data);
	}
	
	public function rollup(){
		$this->session->set_userdata('custom_print_option','rollup');
		$this->frontview('web/rollup',$data);
	}
	
	public function banner(){
		$this->session->set_userdata('custom_print_option','banner');	
		$this->frontview('web/banner',$data);
	}
	*/


	
	public function user_register(){
	    	    
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
				//echo "<pre>"; print_r($_POST); exit;
	        if($this->form_validation->run('user_registeration')){
				
	            $password = $this->input->post('password');
                $cpassword = $this->input->post('cpassword');
                $address = $this->input->post('address');
                $user_lat = $this->input->post('user_lat');
                $user_lng = $this->input->post('user_lng');
	            $email = $this->input->post('email');
	            $gender = $this->input->post('gender');

				$check_exist = $this->generalmodel->getparticularData('id','users',array('email'=>$email,'delete_status'=>'0'),'row_array');

                if(!empty($check_exist)){
                    $return = array('success'=>false,'msg'=>"Email address is already registered with us!");
                    echo json_encode($return); exit;
                }

                if($password !== $cpassword){
                    $return = array('success'=>false,'msg'=>"Password don't match!");
                    echo json_encode($return); exit;
                }
                // elseif(!pass_strength($password)){
                //     $return = array('success'=>false,'msg'=>'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.');
                //     echo json_encode($return); exit;
                // }
                // else{
                //     $enc_pass = encrypt_pass($password);
                // }
                
	            $fname = $this->input->post('f_name');
	            $lname = $this->input->post('l_name');
	            $mobile = $this->input->post('mobile');
		        $userdata['role'] = '3';
		        $userdata['email'] = $email;
		        $userdata['firstname'] = $fname;
		        $userdata['lastname'] = $lname;
		        $userdata['mobile'] = $mobile;
		        $userdata['gender'] = $gender;
		        $userdata['password'] = md5($this->input->post('password'));
		        $userdata['created_at'] = date('Y-m-d h:i:s');
		        $userdata['latitude'] = $user_lat;
		        $userdata['longitude'] = $user_lng;
		        $userdata['address'] = $address;
	        

	        
	            if($this->generalmodel->add('users',$userdata)){
	                
	                //$this->session->set_userdata('userdata',array('email'=>$email,'role'=>'3','name'=>$name));
	                $return = array('success'=>true,'msg'=>'registered successfully');
	            }else{
	                $return = array('success'=>'false','msg'=>'internal error');        
	            }
	        }else{
	            $return = array('success'=>'false','msg'=>validation_errors());
	        }
	        
	        
	        echo json_encode($return);
	    }else{
    	    redirect('/');
	    }

	}
	
	public function userlogin(){
	    
	     if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('user_login')){
	            
	            $email = $this->input->post('email');
	            $password = $this->input->post('password');
	            
	            $userdata = $this->generalmodel->getparticularData('id,email,role,firstname,lastname,  CONCAT_WS(" ",firstname,lastname) AS username','users',array('email'=>$email,'password'=>md5($password),'status'=>'0','delete_status'=>'0'),'row_array');

	            if(!empty($userdata)){

	                if($userdata['role']==3){
	                	$this->session->set_userdata('userdata',array('user_id'=>$userdata['id'],'email'=>$email,'role'=>$userdata['role'],'name'=>$userdata['firstname']));

	                	$return = array('success'=>true,'redirect'=>site_url('/'));
	                }elseif($userdata['role']==2){


						$branch_ids = $this->generalmodel->getparticularData('GROUP_CONCAT(`id`) as branch_ids','printery_shop',"user_id='".$userdata['id']."' AND delete_status='0' ",'row_array');

						if(!empty($branch_ids)){
							$sessData['branch_ids']= $branch_ids['branch_ids'];
						}
							//print_r($branch_ids); exit;

						$sessData['user_id']=$userdata['id'];
						$sessData['email']=$userdata['email'];
						$sessData['role']=$userdata['role'];
						$sessData['username']=$userdata['username'];
						$sessData['firstname']=$userdata['firstname'];
						$sessData['lastname']=$userdata['lastname'];
						$this->session->set_userdata('vendordata',$sessData);	                	
	                	$return = array('success'=>true,'redirect'=>site_url('/vendor'));
	                }

/*
	                // set cart data
	                $cartdata = $this->generalmodel->getparticularData('*','cart',array('customer'=>$userdata['id']),'result_array');

	                $customArr = $quickArr = $translationArr = $notesArr = array();
	                if(!empty($cartdata)){ foreach($cartdata as $value){
	                	if($value['order_type']==1){
							$customArr[] = $value;
	                	}elseif($value['order_type']==2){
	                		$quickArr[] = $value;
	                	}elseif($value['order_type']==3){
	                		$translationArr[] = $value;
	                	}elseif($value['order_type']==4){
	                		$notesArr[] = $value;
	                	}

	                }}

	                $this->session->set_userdata('cartdata',array('custom'=>$customArr,'quick'=>$quickArr,'translation'=>$translationArr,'notes'=>$notesArr));

	                $this->set_cartDate_session();

	                // echo "<pre>"; print_r($cartdata); 
	                // echo "session <pre>"; print_r($_SESSION); 
	                //  exit;
*/

	            }else{
	                $return = array('success'=>false,'msg'=>'Invalid email or password');
	            }
	            
	            
	        }else{
	            $return = array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	     }
	}


	
	public function forgotpass(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
            $email = $this->input->post('email');
            $token = md5(uniqid(rand(), true));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            if($this->form_validation->run())
            {
                $userdata = $this->generalmodel->getparticularData('*','users',array('email'=>$email,'status'=>'0','delete_status'=>'0'),'row_array');
                if(empty($userdata)){
                     $return  = array('success'=>false,'msg'=>'Email Address is not registered with us');  
                }else{
                $insertdata['user_email'] = $email;
                $insertdata['token'] = $token;
                $insertdata['expiry'] = date('Y-m-d',strtotime("+1 day"));
                
                if($this->generalmodel->add('password_reset',$insertdata)){
                    
                    $link = '<a href="'.site_url('reset_password').'?email='.$email.'&&token='.$token.'"></a>';
                    $from = "test@gmail.com";
                    //$to = "soniyakukreja091@gmail.com";
                    $to = $email;
                    $subject = "Password Reset Request";
                    $message = "Click Here to reset your password <br> ".$link;
                    
                    // echo $token;
                    // echo $link;
                    //echo 'msg'.$message;
                    
                    
                    
                    if($this->sendGridMail($from,$to,$subject,$link)){
    
                    $return  = array('success'=>true,'msg'=>'mail sent');    
                    }else{
                        $return  = array('success'=>false,'msg'=>'mail failed');                    
                    }
                    
                }else{
                    $return  = array('success'=>false,'msg'=>'internal error');
                }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}
	
	public function reset_password(){
	    $this->frontview('web/reset_password');
	}
	
	public function reset_new_password(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
            $email = $this->input->post('email');
            $token = $this->input->post('token');
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required');
            $this->form_validation->set_rules('token', 'Token', 'required');
            if($this->form_validation->run())
            {
                $resetData = $this->generalmodel->getparticularData('*','password_reset',array('user_email'=>$email,'token'=>$token,'status'=>'0','expiry >'=>date('Y-m-d')),'row_array');
                if(empty($resetData)){
                     $return  = array('success'=>false,'msg'=>'Link is expired');  
                }else{
                    
                    if($password !== $cpassword){
                        $return = array('success'=>false,'msg'=>"Password don't match!");
                        echo json_encode($return); exit;
                    }else{
                        $this->generalmodel->updaterecord('password_reset',array('status'=>'1'),array('user_email'=>$email,'token'=>$token));
                        $this->generalmodel->updaterecord('users',array('password'=>md5($password)),array('email'=>$email));

                        $from = "test@gmail.com";
                        $to = $email;
                        $subject = "Password Reset Success";
                        $message = "Password reseted successfully";
                        
                       //                  echo 'msg'.$message;
                        
                        
                        
                        if($this->sendGridMail($from,$to,$subject,$link)){
        
                        $return  = array('success'=>true,'msg'=>'password reset');    
                        }else{
                            $return  = array('success'=>false,'msg'=>'mail failed');                    
                        }
                    }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}
	

	public function test(){
		$where = "p.status='0' AND p.delete_status='0'";
		$this->data['list'] = $this->generalmodel->printeryshop_list($where);
		//echo "<pre>"; print_r(expression)
	   $this->frontview('web/printry_list_page',$this->data);

	}


	public function test1(){
        $this->data['title'] = 'Dashboard';
        $this->data['menu'] = 'dashboard';		
		$this->vendorview('vendor/price_page',$this->data);	   

	}
	
	
	public function test2(){
        $this->frontview('web/customer_profile',$this->data);

	}



	public function show_session(){
        //$this->set_cartDate_session();
        echo "<pre>"; print_r($_SESSION); exit;
    }

	public function session_destroy(){
		 //$this->session->unset_userdata(array('selected_printry'));
       $this->session->sess_destroy();
        echo "<pre>"; print_r($_SESSION); exit;
    }


    
    
    
}
