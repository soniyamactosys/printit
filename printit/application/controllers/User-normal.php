<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    public function __construct(){
        
        parent::__construct();
        
        if(!empty($this->session->userdata['userdata'])){
	        $this->userdata = $this->session->userdata['userdata'];
            $this->data['footer_data']['in_progress_orders'] = $this->generalmodel->orders_status();
            if(!empty($this->session->userdata['continue_order'])){ $this->continue_order = $this->session->userdata['continue_order'];  }
        }else{
            redirect('/');
        }   
    }
    
    public function index(){
        //redirect('/');
        $this->frontview('web/customer_profile',$this->data);
    }

    public function myprofile(){
        $this->frontview('web/customer_profile',$this->data);
    }

    //===================quick & custom print
    public function set_paper_type($type=""){
        $userdata = $this->session->userdata('userdata');
        if(!empty($userdata)){
            $this->session->set_userdata('paper_type',$type);
            $return = array('success'=>true,'msg'=>'');
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function set_paper_size($size){
        $userdata = $this->session->userdata('userdata');
        if(!empty($userdata)){
            $this->session->set_userdata('paper_size',$size);
            $return = array('success'=>true,'msg'=>'');
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function set_copynumber($number=""){
        if(!empty($this->userdata)){
            if(!empty($number) && is_numeric($number)){
            $this->session->set_userdata('copy_number',$number);
            $return = array('success'=>true,'msg'=>'set');                
            }else{
                $return = array('success'=>false,'msg'=>'Please enter valid number of copies');
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function set_printing_side($side){
        if(!empty($this->userdata)){
            if(!empty($side)){
                $this->session->set_userdata('printing_side',$side);
                $return = array('success'=>true,'msg'=>'set');                
            }else{
                $return = array('success'=>false,'msg'=>'Please Select Printing Side');
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function set_banner_width(){
        if(!empty($this->userdata)){
            $width = $this->input->post('width');
            if(!empty($width)){
                $this->form_validation->set_rules('width', 'Banner Width', 'required|xss_clean');
                if($this->form_validation->run())
                {
                    $this->session->set_userdata('banner_width',$width);
                    $return = array('success'=>true,'msg'=>'set');
                }                
            }else{
                $return = array('success'=>false,'msg'=>'Please Enter Banner Width');
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);        
    }

    public function upload_design(){
        if(!empty($this->userdata)){
            if(!empty($_FILES)){
                if(!empty($_FILES['graph_design_one']['name'])){ 
                    $fname ='graph_design_one';
                    $img_original_name = $_FILES['graph_design_one']['name'];
                }elseif(!empty($_FILES['graph_design_two']['name'])){
                    $fname ='graph_design_two';
                    $img_original_name = $_FILES['graph_design_two']['name'];
                }
                $upload_data = $this->uploadDoc($fname,'order_uploads',array('jpg','jpeg','png'));
                if(!empty($upload_data['error'])){
                    $return = array('success'=>false,'msg'=>$upload_data['error']);
                }else{
                    $this->session->set_userdata('uploaded_image',$upload_data['file_name']);
                    $this->session->set_userdata('image_rotation',$this->input->post('image_rotation'));

                    if(!empty($this->session->userdata('current_order'))){
                        $cdata = $this->session->userdata('current_order');
                        $cdata['img_original_name'] = $img_original_name;
                        $this->session->set_userdata('current_order',$cdata);
                    }

                    //echo "<pre>"; print_r($_SESSION); exit;
                    $this->session->set_userdata('img_original_name',$img_original_name);
                    $return = array('success'=>true,'msg'=>'Uploaded successfully');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please Login first');
        }
        echo json_encode($return);
    }

    public function save_project(){
        if(!empty($this->userdata)){
            $project_name = $this->input->post('project_name');
            if(empty($project_name)){
                echo json_encode(array('success'=>false,'msg'=>'Please enter project name')); exit;
            }else{  
                $service_type = ($this->session->userdata('service_type')=='normal' || $this->session->userdata('service_type')==1 )?'1':'2';
                $order_type = $this->session->userdata('order_type');
                $ptype = $this->session->userdata('print_type');
                if(!empty($ptype)){
                    $printtype = get_print_type_data($ptype);
                    $print_type = $printtype['id'];
                }else{
                    $print_type = 0;
                }

                $paper_type = $this->session->userdata('paper_type');
                $paper_size = $this->session->userdata('paper_size');
                $uploaded_image = $this->session->userdata('uploaded_image');
                $img_original_name = $this->session->userdata('img_original_name');
                $image_rotation = $this->session->userdata('image_rotation');
                $copy_number = $this->session->userdata('copy_number');
                $printing_side = $this->session->userdata('printing_side');
                $width = $this->session->userdata('banner_width');
                $color = $this->session->userdata('color');
                $binding = $this->session->userdata('binding');
                $lang_from_id = $this->session->userdata('lang_from_id');
                $lang_to_id = $this->session->userdata('lang_to_id');
                $trans_type_id = $this->session->userdata('trans_type_id');
                $trans_doc = $this->session->userdata('trans_doc');
                $doc_original_name = $this->session->userdata('doc_original_name');

                if(empty($service_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Service')); exit;
                }elseif(empty($order_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Order')); exit;
                }elseif($order_type=='1' && empty($ptype)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Custom Print Option')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && empty($paper_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Paper type')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && $ptype!='banner' && empty($paper_size)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Paper Size')); exit;
                }elseif($order_type=='2' && empty($color)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Color')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && $ptype=='banner' && empty($width)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Banner Width')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && $ptype=='flyer' && empty($printing_side)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Printing Side')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && empty($uploaded_image)){
                    echo json_encode(array('success'=>false,'msg'=>'Please Upload Image')); exit;
                }elseif($order_type=='2' && empty($binding)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Binding')); exit;
                }elseif($order_type=='3' && empty($trans_type_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select translate type')); exit;
                }elseif($order_type=='3' && empty($lang_from_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select language from to translate')); exit;
                }elseif($order_type=='3' && empty($lang_to_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select language to translate')); exit;
                }elseif($order_type=='3' && empty($trans_doc)){
                    echo json_encode(array('success'=>false,'msg'=>'Please Upload Document')); exit;
                }elseif(($order_type=='1' || $order_type=='2') && empty($copy_number)){
                    echo json_encode(array('success'=>false,'msg'=>'Please Enter number of Copies')); exit;
                }

                $odata['customer'] = $this->userdata['user_id'];
                $odata['service_type'] = $service_type;
                $odata['order_type'] = $order_type;
                $odata['project_name'] = $project_name;
                $odata['print_type'] = $print_type;
                $odata['paper_type'] = $paper_type;
                $odata['paper_size'] = $paper_size;
                $odata['no_of_copies'] = $copy_number;
                $odata['image_uploaded'] = $uploaded_image;
                $odata['img_original_name'] = $img_original_name;
                $odata['image_rotation'] = $image_rotation;
                $odata['width'] = $width;
                $odata['printing_side'] = $printing_side;
                $odata['color'] = $color;
                $odata['binding'] = $binding;
                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['status'] = '0';
                $odata['step_url'] = $this->input->post('current_url');

                $all_steps[0] = $this->input->post('current_url');
                $odata['all_steps'] = serialize($all_steps);

                $oid = $this->session->userdata('continue_order');
                if(empty($oid)){
                    $last_id = $this->generalmodel->add('orders',$odata);
                    $current_order['order_id'] = $last_id;
                }else{
                    $updateid = $this->generalmodel->updaterecord('orders',$odata,array('id'=>$oid));
                    $current_order['order_id'] = $oid;
                }
                
                //echo $this->db->last_query();
                $current_order['service_type'] = $odata['service_type'];
                $current_order['order_type'] = $odata['order_type'];
                $current_order['project_name'] = $odata['project_name'];
                $current_order['print_type'] = $odata['print_type'];
                $current_order['paper_type'] = $odata['paper_type'];
                $current_order['paper_size'] = $odata['paper_size'];
                $current_order['no_of_copies'] = $odata['no_of_copies'];
                $current_order['image_uploaded'] = $odata['image_uploaded'];
                $current_order['printing_side'] =  $odata['printing_side'];
                $current_order['banner_size'] =  $odata['width'];
                $current_order['color'] =  $odata['color'];
                $current_order['binding'] =  $odata['binding'];
                //$current_order['order_total'] =  $odata['binding'];
                

                //===
                $paper_type_data =  $this->generalmodel->getparticularData('id,name','paper_type',array('status'=>'0','id'=>$odata['paper_type']),'row_array');
                //print_r($paper_type_data)
               
                $paper_size_data =  $this->generalmodel->getparticularData('id,name','paper_size',array('status'=>'0','id'=>$odata['paper_size']),'row_array');

                if(!empty($paper_type_data)){ $current_order['papertype'] =  $paper_type_data['name']; }
                if(!empty($paper_size_data)){ $current_order['papersize'] =  $paper_size_data['name']; }
                $current_order['img_original_name'] = $img_original_name;
                $current_order['image_rotation'] = $image_rotation;

                //echo "<pre>"; print_r($current_order);exit;

                if($this->input->post('continue')=='true'){
                    $this->session->set_userdata('continue_order',$current_order['order_id']);
                    $this->session->set_userdata('current_order',$current_order);
                }else{
                    $this->session->unset_userdata('current_order');
                }

                if(!empty($last_id) || !empty($updateid)){

                    //$this->session->unset_userdata(array('custom_print_option','order_type','paper_type','copy_number','paper_size','printing_side','uploaded_image','print_type','selected_printry','pickup_delivery','banner_width'));

                        //$this->session->set_userdata('continue_order',$last_id); 

                        if($service_type=='1'){
                            $return = array('success'=>true,'msg'=>'order saved','href'=>'printery_shops');
                        }else{
                            $return = array('success'=>true,'msg'=>'order saved','href'=>'pickup_delivery');
                        }
                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    //=============quick & custom print

    //==================translation

    public function set_trans_type(){
        if(!empty($this->userdata) &&  $this->input->is_ajax_request()){
            $trans_type_id = $this->input->post('trans_type_id');
            $trans_type = $this->input->post('trans_type');

            $this->session->set_userdata('trans_type_id',$trans_type_id);
            $this->session->set_userdata('trans_type',$trans_type);
            $return = array('success'=>true,'msg'=>'set'); 
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }  
        echo json_encode($return);        
    }

    public function set_lang_from(){
        if(!empty($this->userdata) &&  $this->input->is_ajax_request()){
            $lang_from_id = $this->input->post('lang_from_id');
            $lang_from = $this->input->post('lang_from');

            $this->session->set_userdata('lang_from_id',$lang_from_id);
            $this->session->set_userdata('lang_from',$lang_from);
            $return = array('success'=>true,'msg'=>'set'); 
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }  
        echo json_encode($return);        
    }

    public function set_lang_to(){
        if(!empty($this->userdata) &&  $this->input->is_ajax_request()){
            $lang_to_id = $this->input->post('lang_to_id');
            $lang_to = $this->input->post('lang_to');

            $this->session->set_userdata('lang_to_id',$lang_to_id);
            $this->session->set_userdata('lang_to',$lang_to);
            $return = array('success'=>true,'msg'=>'set'); 
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }  
        echo json_encode($return);        
    }   
    
    public function upload_doc(){
        if(!empty($this->userdata)){
            //echo "<pre>"; print_r($_FILES); exit;
            if(!empty($_FILES)){

                $doc_original_name = $_FILES['trans_doc']['name'];

                $user_id = $this->userdata['user_id'];
                $path = 'tran_doc/'.$user_id;
                if(!file_exists($path)) {
                    mkdir($path, 0777, true);
                }           

                $upload_data = $this->uploadDoc('trans_doc',$path,array('pdf','docx','doc','xls','xlsx'));
                if(!empty($upload_data['error'])){
                    $return = array('success'=>false,'msg'=>$upload_data['error']);
                }else{
                    $this->session->set_userdata('trans_doc',$upload_data['file_name']);

                    if(!empty($this->session->userdata('current_order'))){
                        $cdata = $this->session->userdata('current_order');
                        $cdata['doc_original_name'] = $doc_original_name;
                        $this->session->set_userdata('current_order',$cdata);
                    }

                    //echo "<pre>"; print_r($_SESSION); exit;
                    $this->session->set_userdata('doc_original_name',$doc_original_name);
                    $return = array('success'=>true,'msg'=>'Uploaded successfully');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please Login first');
        }
        echo json_encode($return);
    }

    public function save_translation(){

        if(!empty($this->userdata)){
            $project_name = $this->input->post('project_name');
            if(empty($project_name)){
                echo json_encode(array('success'=>false,'msg'=>'Please enter project name')); exit;
            }else{
                
                $service_type = ($this->session->userdata('service_type')=='normal')?'1':'2';
                $order_type = $this->session->userdata('order_type');
               
                $trans_type_id = $this->session->userdata('trans_type_id');
                $lang_from_id = $this->session->userdata('lang_from_id');
                $lang_to_id = $this->session->userdata('lang_to_id');
                $trans_doc = $this->session->userdata('trans_doc');
                $doc_original_name = $this->session->userdata('doc_original_name');

                if(empty($service_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Service')); exit;
                }elseif($service_type==1){
                    echo json_encode(array('success'=>false,'msg'=>'This service is only available for express orders')); exit;
                }elseif(empty($order_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Order')); exit;
                }elseif($order_type=='3' && empty($trans_type_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select translate type')); exit;
                }elseif($order_type=='3' && empty($lang_from_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select language from to translate')); exit;
                }elseif($order_type=='3' && empty($lang_to_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select language to translate')); exit;
                }elseif($order_type=='3' && empty($trans_doc)){
                    echo json_encode(array('success'=>false,'msg'=>'Please Upload Document')); exit;
                }

                $odata['customer'] = $this->userdata['user_id'];
                $odata['service_type'] = $service_type;
                $odata['order_type'] = $order_type;
                $odata['project_name'] = $project_name;

                $odata['trans_type_id'] = $trans_type_id;
                $odata['lang_from'] = $lang_from_id;
                $odata['lang_to'] = $lang_to_id;
                $odata['trans_doc'] = $trans_doc;


                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['status'] = '0';
                $odata['step_url'] = $this->input->post('current_url');

                $all_steps[0] = $this->input->post('current_url');
                $odata['all_steps'] = serialize($all_steps);

                if(!empty($doc_original_name)){
                    $odata['doc_org_name'] = $doc_original_name;
                }
               
                $odata['progress_status'] = "saved";

                // if($this->input->post('continue')=='true'){
                //     $odata['progress_status'] = "awaiting_for_translation_proposals";
                // }else{
                //     $odata['progress_status'] = "saved";
                // }

                $oid = $this->session->userdata('continue_order');
                if(empty($oid)){
                    $last_id = $this->generalmodel->add('orders',$odata);
                    $current_order['order_id'] = $last_id;
                }else{
                    $updateid = $this->generalmodel->updaterecord('orders',$odata,array('id'=>$oid));
                    $current_order['order_id'] = $oid;
                }
                
                $current_order['service_type'] = $odata['service_type'];
                $current_order['order_type'] = $odata['order_type'];
                $current_order['project_name'] = $odata['project_name'];

                $current_order['trans_type_id'] =  $odata['trans_type_id'];
                $current_order['lang_from_id'] =  $odata['lang_from'];
                $current_order['lang_to_id'] =  $odata['lang_to'];
                $current_order['trans_doc'] =  $odata['trans_doc'];
                $current_order['doc_org_name'] =  $odata['doc_org_name'];
                $current_order['trans_type'] =  $this->session->userdata('trans_type');
                $current_order['lang_from'] =  $this->session->userdata('lang_from');
                $current_order['lang_to'] =  $this->session->userdata('lang_to');
              

                
                if(!empty($last_id) || !empty($updateid)){

                    if($this->input->post('continue')=='true'){
                        $this->session->set_userdata('continue_order',$current_order['order_id']);
                        $this->session->set_userdata('current_order',$current_order);
                        $href="delivery";
                    }else{
                        $this->session->unset_userdata('current_order','continue_order');
                    }
                  
                    //$this->session->unset_userdata(array('order_type','trans_type_id','trans_type','lang_from_id','lang_from','lang_to_id','lang_to','trans_doc','doc_original_name'));
                    //print_r($oid)
                    if($this->input->post('continue')=='true'){
                        //$this->session->unset_userdata(array('current_order','continue_order'));
                        
                        $return = array('success'=>true,'href'=>$href,'msg'=>'Request sent! You will get proposals soon');
                    }else{
                        $return = array('success'=>true,'msg'=>'Order Saved');
                    }

                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);

    }


    public function set_tran_address(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){   
            $post_id = $this->input->post('address_id');
            $add_detail = $this->generalmodel->getparticularData('id,address,latitude,longitude','customer_address',array('id'=>$post_id),'row_array');

            $latitude = $add_detail['latitude'];
            $longitude = $add_detail['longitude'];

            if(!empty($add_detail)){
                $order_address['order_id'] = $this->session->userdata['continue_order'];
                $order_address['c_add_id'] = $add_detail['id'];
                $order_address['address'] = $add_detail['address'];

                //$delivery_charge = get_delivery_charges();
                $addQuery = $this->generalmodel->update_tran_delivery_address($add_detail['id'],$order_address,$latitude,$longitude);                    

                if(!empty($addQuery)){

                    $this->session->unset_userdata(array('current_order','continue_order','doc_original_name','trans_doc','lang_to','lang_to_id','lang_from','lang_from_id','trans_type','trans_type_id','order_type'));

                    $return = array('success'=>true,'href'=>'trans_offer_sent','msg'=>'Address Selected');
                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }
            }else{
                $return = array('success'=>false,'msg'=>'something went wrong');
            }
            // get address from customer_address table where given id from post
            //     
           // echo "<pre>"; print_r($_POST); exit;
        }else{
            $return = array('success'=>false,'msg'=>'login first');
        }
        echo json_encode($return);
    }



    public function add_trans_new_address(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){  

            $customer_address['user_id'] = $this->userdata['user_id'];
            $customer_address['label'] = $this->input->post('label');
            $customer_address['address'] = $this->input->post('address');
            $customer_address['latitude'] = $this->input->post('latitude');
            $customer_address['longitude'] = $this->input->post('longitude');

            $order_address['order_id'] = $this->session->userdata['continue_order'];
            $order_address['address'] = $customer_address['address'];

            $lastid = $this->generalmodel->add_new_order_address($customer_address,$order_address);

            if(!empty($lastid)){
               
                $this->session->unset_userdata(array('current_order','continue_order','doc_original_name','trans_doc','lang_to','lang_to_id','lang_from','lang_from_id','trans_type','trans_type_id','order_type'));

                $return = array('success'=>true,'href'=>site_url('trans_offer_sent'),'msg'=>'Added Successfully');
            }else{
                $return = array('success'=>false,'msg'=>'');
            }
            echo json_encode($return);
        }
    }    

    public function trans_offer_sent(){
        $this->frontview('web/trans_offer_sent',$this->data);
    }

    //===============translation=======

    public function select_printry($printry){
        //print_r($printry);exit;
        if(!empty($this->userdata)){
			if(empty($printry)){
				$this->session->unset_userdata(array('selected_printry','vendor_charge'));
                $return = array('success'=>true,'msg'=>'unset');
			}else{

                $printry_detail = $this->printery_withprice("",$printry);
              
                if(!empty($printry_detail)){
                    $this->session->set_userdata('selected_printry',$printry_detail);
                  
                    $this->session->set_userdata('vendor_charge',$printry_detail['vendor_charge']);

                    $return = array('success'=>true,'msg'=>'set');     
                    
                }else{
                    $return = array('success'=>false,'msg'=>'internal error');                     
                }
			}
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function pickup_delivery(){
        if(!empty($this->userdata) && !empty($this->continue_order)){
           	if($this->session->userdata('service_type')=='normal'){
                if(empty($this->session->userdata('selected_printry'))){ 
                    redirect('printery_shops');
                }

                $oid = $this->session->userdata('continue_order');
                update_order_steps($oid,current_url());
    			$this->data['products'] = $this->generalmodel->getparticularData('*','products',array('status'=>'0','qty >='=>1),'result_array',10,0,'price');

                $this->data['my_address_options'] = $this->generalmodel->getparticularData('*',' customer_address',array('user_id'=>$this->userdata['user_id'],'status'=>'0'),'result_array');

    			$this->frontview('web/pickup_delivery',$this->data);
        	}elseif($this->session->userdata('service_type')=='express'){
        		$this->frontview('web/only_delivery',$this->data);
        	}
        }else{
            redirect('/');
        }
    }

    public function delivery(){
        if(!empty($this->userdata) && !empty($this->continue_order)){
            $oid = $this->session->userdata('continue_order');
            update_order_steps($oid,current_url());
            $this->data['products'] = $this->generalmodel->getparticularData('*','products',array('status'=>'0','qty >='=>1),'result_array',10,0,'price');

            $this->data['my_address_options'] = $this->generalmodel->getparticularData('*',' customer_address',array('user_id'=>$this->userdata['user_id']),'result_array');

            $this->frontview('web/only_delivery',$this->data);
        }else{
            redirect('/');
        }
    }

    public function set_pickup_delivery($type){
        if(!empty($this->userdata) && !empty($this->continue_order)){
            $this->session->set_userdata('pickup_delivery',$type);
            $value = ($type=='pickup')?'1':'2';
            $current_order_id = $this->session->userdata('continue_order');

            if($type=='pickup'){
                $delivery_charge = 0;
            }else{
                //====get delivery charges
                $delivery_charge = get_delivery_charges();
            }

            $query = "UPDATE orders SET pickup_delivery=$value,order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$current_order_id;

            $update_id = $this->db->query($query);

            if($update_id){
                $this->set_order_in_session($current_order_id);
                $return = array('success'=>true,'msg'=>'');
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }
    public function payment(){
    	if(!empty($this->userdata) && !empty($this->continue_order)){
            
            $oid = $this->session->userdata('continue_order');
            update_order_steps($oid,current_url());
            $this->frontview('web/payment',$this->data);
    	}else{
            redirect('/');
        }
    }

    public function payment_gateway(){
        if(!empty($this->userdata) && !empty($this->continue_order)){

            //$current_order_id = $this->session->userdata('continue_order');
            //$updatequery = $this->generalmodel->updaterecord('orders',array('step_counter'=>'7'),array('id'=>$current_order_id));      
            //$this->frontview('web/payment_gateway');

    	   redirect('order_confirmed');
        }else{
            redirect('/');
        }
    }

    public function order_confirmed(){
            
        if(!empty($this->userdata) && !empty($this->continue_order)){
            
            $pick_delivery = $this->session->userdata('pickup_delivery');
            $coupon_applied = $this->session->userdata('coupon_applied');

            $current_order = $this->session->userdata('current_order');

            //echo "<pre>"; print_r($current_order); exit;

            if(!empty($current_order)){
                $pick_delivery = $current_order['pickup_delivery'];
                //$delivery_charge = $current_order['delivery_charge'];

            }
            
            $data['updated_at']=date('Y-m-d h:i:s');
            $data['updated_by']=$this->userdata['user_id'];
            $data['vendor']=$this->session->userdata['selected_printry']['id'];
            $data['status']='1';
            $data['payment_status']='1';
            $data['progress_status']='being_printed';

            //echo $pick_delivery; exit;
            if($pick_delivery=='pickup'||$pick_delivery=='1'){
                $data['pickup_delivery']='1';

                if(!empty($coupon_applied)){
                    $data['coupon_code']=$coupon_applied['code'];
                    $data['coupon_amt']=$coupon_applied['coupon_amount'];
                    $data['applied_coupon_amt']=$coupon_applied['applied_amount'];
                    $data['coupon_start']=$coupon_applied['start_date'];
                    $data['coupon_end']=$coupon_applied['end_date'];
                }


            }else{
                $data['pickup_delivery']='2';
                //$data['delivery_charge']= $delivery_charge;
                $data['delivery_address']= $this->session->userdata('delivery_address');
            }

                $current_order_id = $this->session->userdata('continue_order');
                
                $this->db->trans_start();
                    $updatequery = $this->generalmodel->updaterecord('orders',$data,array('id'=>$current_order_id));
                    
                    $query = "UPDATE orders SET order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$current_order_id;
    
                    $update_query = $this->generalmodel->customquery($query,"row_array");
                
                    if(!empty($coupon_applied)){
                        $update_coupon_users = $this->db->query("UPDATE coupon SET used_by = used_by+1 WHERE `status`='0' AND code='".$coupon_applied['code']."'");
                    }
                $this->db->trans_complete();
               
                if(empty($updatequery)){
                    echo json_encode(array('success'=>false,'msg'=>'query failed'));
                    exit;
                }else{
                    $this->session->set_userdata('order_confirmed');
                    
                    $this->session->unset_userdata(array('current_order','selected_printry','pickup_delivery','coupon_applied','vendor_charge','continue_order','delivery_address','order_type','order_products'));
                    $this->data['order_data'] = $this->generalmodel->order_detail($current_order_id);
                    $this->frontview('web/order_confirmed',$this->data);     
                }
        }else{
            redirect('/');
        }
    }

    public function save_products(){
        if(!empty($this->userdata) && !empty($this->continue_order) && $this->input->is_ajax_request()){
            $post_products['products'] = $products = $this->input->post('products');
            if(!empty($products)){

                $post_products['price'] = $this->input->post('price');
                $post_products['qty'] = $this->input->post('qty');
                $post_products['name'] = $this->input->post('name');
                $this->session->set_userdata('post_products',$post_products);
                $return = array('success'=>true,'msg'=>'set');
            }else{
                $return = array('success'=>false,'msg'=>'something went wrong');
            }
        }
        echo json_encode($return);
    }
    /*
    public function save_products(){
        if(!empty($this->userdata) && !empty($this->continue_order) && $this->input->is_ajax_request()){
            //echo "<pre>"; print_r($_POST); 
            $oid = $this->session->userdata('continue_order');
            $products = $this->input->post('products');
            $price = $this->input->post('price');
            $quantity = $this->input->post('qty');
            $name = $this->input->post('name');
            $sess_pro = array();
    

            if(!empty($products)){
                $i=0;
                foreach($products as $key=>$val){
                    $pro[$i]['order_id'] = $oid;
                    $pro[$i]['prod_id'] = $val;
                    $pro[$i]['prod_price'] = $price[$key];
                    $pro[$i]['prod_qty'] = $quantity[$key];
                    $pro[$i]['created_at'] = date('Y-m-d h:i:s');


                    $stockData[$i]['qty'] = $quantity[$key];
                    $stockData[$i]['id'] = $val;

                    $sess_pro[$val]['name'] = $name[$key];
                    $sess_pro[$val]['qty'] = $quantity[$key];
                    $sess_pro[$val]['price'] = $price[$key];

                    $i++;
                } 

                // print_r($pro); 
                // print_r($stockData); 
                // print_r($sess_pro); 
                // exit;

                $this->generalmodel->add_order_products($oid,$pro,$stockData);
                $this->session->set_userdata('order_products',$sess_pro);
                $return = array('success'=>true,'msg'=>'set');

            }else{
                $return = array('success'=>false,'msg'=>'something went wrong');
            }
        }
        echo json_encode($return);
    }
    */
    public function saveincart(){
        
        if(!empty($this->userdata)){
            $project_name = $this->input->post('project_name');
            if(empty($project_name)){
                echo json_encode(array('success'=>false,'msg'=>'Please enter project name')); exit;
            }else{

                $service_type = ($this->session->userdata('service_type')=='normal')?'1':'2';
                //$service_type = $this->session->userdata('service_type');
                //if($service_type=='normal'){}
                $order_type = $this->session->userdata('order_type');
                //$custom_print_option = $this->session->userdata('custom_print_option');
                $ptype = $this->session->userdata('print_type');
                if(!empty($ptype)){
                    $printtype = get_print_type_data($ptype);
                    $print_type = $printtype['id'];
                }

                // print_r($printtype);
                // print_r($print_type);
                // exit;
                $paper_type = $this->session->userdata('paper_type');
                $paper_size = $this->session->userdata('paper_size');
                $uploaded_image = $this->session->userdata('uploaded_image');
                $copy_number = $this->session->userdata('copy_number');
                $printing_side = $this->session->userdata('printing_side');
                $width = $this->session->userdata('width');
                // echo "<pre>"; print_r($_SESSION);
                // echo $print_type; exit;

              

               /* $average_costArr = $this->generalmodel->getparticularData('average_price','print_management',array('status'=>'0','paper_type_id'=>$paper_type,'paper_size_id'=>$paper_size,'print_type'=>$print_type),'row_array');

                if(!empty($average_costArr)){
                    $average_cost = $average_costArr['average_price'];
                }else{
                    $average_cost = 10;
                }
                $amount = floatval($average_cost)*floatval($copy_number);
                */

                //$cartdata = $this->session->userdata('cartdata');
                $odata['customer'] = $this->userdata['user_id'];
                $odata['service_type'] = $service_type;
                $odata['order_type'] = $order_type;
                $odata['project_name'] = $project_name;
                $odata['print_type'] = $print_type;
                $odata['paper_type'] = $paper_type;
                $odata['paper_size'] = $paper_size;
                $odata['no_of_copies'] = $copy_number;
                $odata['image_uploaded'] = $uploaded_image;
                $odata['printing_side'] = $printing_side;

                //$odata['amount'] = $amount;
                if($print_type=='4'){ $odata['width'] = $width; }
                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['status'] = '0';

                $last_id = $this->generalmodel->add('cart',$odata);

                //echo $this->db->last_query();
                if(!empty($last_id)){
                    $return = array('success'=>true,'msg'=>'Project saved in cart');
                    $this->set_cartDate_session();
                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }

            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    
        /*
        if(!empty($this->userdata)){
            $project_name = $this->input->post('project_name');
            if(empty($project_name)){
                echo json_encode(array('success'=>false,'msg'=>'Please enter project name'));
            }else{

            }
            $service_type = $this->session->userdata('service_type');
            //if($service_type=='normal'){}
            $order_type = $this->session->userdata('order_type');
            $custom_print_option = $this->session->userdata('custom_print_option');
            $print_type = $this->session->userdata('print_type');
            $paper_type = $this->session->userdata('paper_type');
            $paper_size = $this->session->userdata('paper_size');
            $uploaded_image = $this->session->userdata('uploaded_image');
            $copy_number = $this->session->userdata('copy_number');

            $average_costArr = $this->generalmodel->getparticularData('average_price','print_management',array('status'=>'0','paper_type_id'=>$paper_type,'paper_size_id'=>$paper_size,'print_type'=>$print_type),'row_array');


            //$service_type = $this->session->userdata('service_type');
            //if($service_type=='normal'){}
            $order_type = $this->session->userdata('order_type');
            $custom_print_option = $this->session->userdata('custom_print_option');
            $print_type = $this->session->userdata('print_type');
            $paper_type = $this->session->userdata('paper_type');
            $paper_size = $this->session->userdata('paper_size');
            $uploaded_image = $this->session->userdata('uploaded_image');
            $copy_number = $this->session->userdata('copy_number');

            $average_costArr = $this->generalmodel->getparticularData('average_price','print_management',array('status'=>'0','paper_type_id'=>$paper_type,'paper_size_id'=>$paper_size,'print_type'=>$print_type),'row_array');

            if(!empty($average_costArr)){
                $average_cost = $average_costArr['average_price'];
            }else{
                $average_cost = 10;
            }
            

            $cartdata = $this->session->userdata('cartdata');
            $cartdata['user_id'] = $userdata['user_id'];
            $cartdata['order_type'] = $order_type;
            $cartdata['paper_type'] = $type;
            $last_id = $this->generalmodel->add('cart',$cartdata);

            
            $lastrecord = $this->generalmodel->getparticularData('*','cart',array('id'=>$last_id),'row_array');

            echo "<pre>"; print_r($lastrecord);

            if(!empty($cartdata)){
                // check which order is set
                //$this->generalmodel->update_cart
                                    // set cart data
                    
                if($order_type==1){
                    $customArr = $this->session->userdata['cartdata']['custom'];


                }elseif($order_type==2){
                    $quickArr[] = $this->session->userdata['cartdata']['quick'];
                }elseif($order_type==3){
                    $translationArr[] = $this->session->userdata['cartdata']['translation'];
                }elseif($order_type==4){
                    $notesArr[] = $this->session->userdata['cartdata']['notes'];
                }

                    

                $this->session->set_userdata('cartdata',array('custom'=>$customArr,'quick'=>$quickArr,'translation'=>$translationArr,'notes'=>$notesArr));

            }else{
                if($this->session->userdata('service_type')=='normal'){
                     $cartdata['service_type'] = '0';
                }else{  $cartdata['service_type'] = '1'; }

                $cartdataArr[0] = $cartdata;
                $this->session->set_userdata('cartdata',$cartdataArr);
            }
            echo "<pre>"; print_r($_SESSION); exit;
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
        */
     
    }


    public function myorders(){
        if(!empty($this->userdata)){

            $this->data['saved_orders'] = $this->generalmodel->orders('0');

// echo $this->db->last_query();
// echo "<pre>"; print_r($this->data['saved_orders']);
// exit;


            $this->data['confirmed_orders'] = $this->generalmodel->orders('1');
            $this->frontview('web/myorders',$this->data);
        //$this->load->view('web/myorders');
        }
    }

    public function apply_coupon(){
        if(!empty($this->userdata) && $this->input->is_ajax_request() && !empty($this->input->post())){        
            $this->form_validation->set_rules('user_coupon', 'User Coupon', 'required|trim|xss_clean');
            if ($this->form_validation->run()==FALSE){
                $return= array('success'=>false,'msg'=>validation_errors());                        
            }else{
                $user_coupon = $this->input->post('user_coupon');
                $order_amount = $this->session->userdata('vendor_charge');
                $where = array('code'=>$user_coupon,'status'=>'0');
                $coupon_detail = $this->generalmodel->getparticularData('*','coupon',$where,'row_array');
                //echo "<pre>"; print_r($get_coupon_detail); exit;

                if(!empty($coupon_detail)){
                    $today = date('Y-m-d');
                    if($coupon_detail['users'] == $coupon_detail['used_by'] ){
                        $return= array('success'=>false,'msg'=>'Sorry maximum users limit has been reached!');
                    }elseif(strtotime($coupon_detail['start_date']) <=strtotime($today) && strtotime($coupon_detail['end_date']) >=strtotime($today)){

                        
                        if($coupon_detail['type']=='0'){

                            $coupon_amt = $coupon_detail['amount'];
                            $return= array('success'=>true,'msg'=>$coupon_detail['amount'].' Discount Added','amt'=>$coupon_amt);
                        }elseif($coupon_detail['type']=='1'){

                            $coupon_amt = (floatval($order_amount)*floatval($coupon_detail['amount']))/100;
                            $return= array('success'=>true,'msg'=>$coupon_detail['amount'].'% Discount Added','amt'=>$coupon_amt);
                        }

                        $coupon_applied['code'] = $coupon_detail['code'];
                        $coupon_applied['coupon_amount'] = $coupon_detail['amount'];
                        $coupon_applied['applied_amount'] = $coupon_amt;
                        $coupon_applied['type'] = $coupon_detail['type'];
                        $coupon_applied['start_date'] =  $coupon_detail['start_date'];
                        $coupon_applied['end_date'] =  $coupon_detail['end_date'];

                        $this->session->set_userdata('coupon_applied',$coupon_applied);
                    }else{
                         $return= array('success'=>false,'msg'=>'Coupon Code Expired');
                    }
                }else{
                    $return= array('success'=>false,'msg'=>'Invalid Coupon Code');     
                }
            }
        }else{
            $return= array('success'=>false,'msg'=>'internal error');            
        }
        echo json_encode($return);
    }

    public function continue_order_set(){
        if(!empty($this->userdata) && $this->input->is_ajax_request() && !empty($this->input->post())){    
            $oid= $this->input->post('order_id');
            $this->session->set_userdata('continue_order',$oid); 

            if(!empty($oid)){

                $odata = $this->generalmodel->order_detail($oid,'0');
                //$this->session->unset_userdata('order_type'); 
                //echo "<pre>"; print_r($odata); 
                /*
                $current_order['order_id'] = $oid;
                $current_order['service_type'] = $odata['service_type'];
                $current_order['order_type'] = $odata['order_type'];
                $current_order['project_name'] = $odata['project_name'];
                if($odata['order_type']==1 || $odata['order_type']==2){
                    $current_order['print_type'] = $odata['print_type'];
                    $current_order['paper_type'] = $odata['paper_type'];
                    $current_order['paper_size'] = $odata['paper_size'];
                    $current_order['no_of_copies'] = $odata['no_of_copies'];
                    $current_order['image_uploaded'] = $odata['image_uploaded'];
                    $current_order['printing_side'] =  $odata['printing_side'];
                    $current_order['banner_size'] =  $odata['width'];
                    $current_order['bannersize'] =  $odata['bannersize'];
                    $current_order['papertype']   =  $odata['papertype'];
                    $current_order['papersize']   =  $odata['papersize'];
                    $current_order['img_original_name'] = $odata['img_original_name'];
                    $current_order['image_rotation'] = $odata['image_rotation'];
                    $current_order['color'] = $odata['color'];             
                    $current_order['binding'] = $odata['binding'];                  
                    $current_order['products'] = $odata['products'];             
                
                }
                    $current_order['vendor_charge'] = $odata['amount'];
                    $current_order['delivery_charge'] = $odata['delivery_charge'];
                    $current_order['product_total'] = $odata['product_total'];
                    $current_order['order_total'] = $odata['order_total'];
                    $current_order['coupon_start'] = $odata['coupon_start'];
                    $current_order['coupon_end'] = $odata['coupon_end'];
                    $current_order['applied_coupon_amt'] = $odata['applied_coupon_amt'];
                    $current_order['all_steps'] = $odata['all_steps'];
                    $current_order['step_url'] = $odata['step_url'];             
                    $current_order['delivery_address'] = $odata['delivery_address'];             
                    $current_order['pickup_delivery'] = $odata['pickup_delivery'];            
    

                if($odata['order_type']==3){
                    $current_order['trans_type_id'] =  $odata['trans_type_id'];
                    $current_order['lang_from_id'] =  $odata['lang_from'];
                    $current_order['lang_to_id'] =  $odata['lang_to'];
                    $current_order['trans_doc'] =  $odata['trans_doc'];
                    $current_order['doc_org_name'] =  $odata['doc_org_name'];
                    $current_order['lang_from'] =  $odata['from'];
                    $current_order['lang_to'] =  $odata['to'];
                    $current_order['trans_type'] =  $odata['trans_type'];

                    $this->session->set_userdata(array('order_type'=>$odata['order_type'],'lang_from_id'=>$odata['lang_from'],'lang_to_id'=>$odata['lang_to'],'trans_type_id'=>$odata['trans_type_id'],'trans_doc'=>$odata['trans_doc'])); 

                }

                $this->session->set_userdata('current_order',$current_order);

                if($odata['order_type']==1 || $odata['order_type']==2){
                    if(!empty($odata['products'])){
                        foreach($odata['products'] as $p){
                            $key = $p['prod_id'];
                            $order_products[$key]['name'] = $p['name'];
                            $order_products[$key]['qty'] = $p['qty'];   
                            $order_products[$key]['price'] =  $p['price']; 
                        }
                                      
                        $this->session->set_userdata(array('order_products'=>$order_products));
                    }

                    $this->session->set_userdata(array('order_type'=>$odata['order_type'],'service_type'=>$odata['service_type'],'print_type'=>$odata['print_type'],'paper_type'=>$odata['paper_type'],'paper_size'=>$odata['paper_size'],'uploaded_image'=>$odata['image_uploaded'],'image_rotation'=>$odata['image_rotation'],'img_original_name'=>$odata['img_original_name'],'copy_number'=>$odata['no_of_copies']));   

                    if(!empty($odata['vendor'])){

                        $printry_detail = $this->printery_withprice("",$odata['vendor']);
                        $this->session->set_userdata('selected_printry',$printry_detail);
                        $this->session->set_userdata('vendor_charge',$printry_detail['vendor_charge']);                    
                    }   

                    $sessArr['pickup_delivery'] = ($odata['pickup_delivery']=='1') ? 'pickup' : (($odata['pickup_delivery']=='2') ? 'delivery' : '');   

                    if(!empty($odata['width'])){
                        $sessArr['banner_width'] =  $odata['width'];
                    }
                    $this->session->set_userdata($sessArr);                                     
                }


        //echo "<pre>"; print_r($_SESSION); exit;

        //$sessArr['pickup_delivery'] = ($odata['pickup_delivery']=='1')?'pickup':'delivery';

             
                    //$current_order = $this->generalmodel->order_detail($oid,'0');
                    //echo "<pre>"; print_r($current_order); exit;
                */

                $this->set_order_in_session($oid);

                if($odata['order_type']=='1'){

                    $print_type = get_print_type_data("",$odata['print_type']);
                    if(!empty($print_type)){ $placeholder = ucfirst($print_type['name']); }
                    //$this->data['page'] = strtolower($print_type['name']);

                }elseif($odata['order_type']=='2'){
                    $placeholder = 'Quick Print'; 
                    //$this->data['page'] = "quickprint";
                }elseif($odata['order_type']=='3'){
                    $placeholder = "Translation"; 
                    //$this->data['page'] = 'translation';

                }elseif($odata['order_type']=='4'){
                    $placeholder = "Notes"; 
                    //$this->data['page'] = 'notes';
                }
                $this->session->set_userdata('header_placeholder',$placeholder);                

            }

            $return = array('success'=>true,'msg'=>$odata['step_url']);
        }else{
            $return = array('success'=>false,'msg'=>'something went wrong');
        }
        echo json_encode($return);        
    }

    public function update_order_vendor(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){        
        
            //get selected vendor  from session and get amount then update order
            //echo "<pre>"; print_r($_SESSION);
            $oid = $this->session->userdata['continue_order'];
            $selected_printry = $this->session->userdata['selected_printry']['branch_id'];
            $vendor_charge = $this->session->userdata['vendor_charge'];
            $service_type = $this->session->userdata['service_type'];

            $current_order = $this->session->userdata('current_order');
            if(!empty($current_order)){
                $order_type = $current_order['order_type'];
            }

            if($service_type=='normal'){
                $link = site_url('pickup_delivery');
            }else{
                $link = site_url('delivery');
            }

            $data['vendor'] = $selected_printry;
            
            $data['step_url'] = $link;
            $data['amount'] = $vendor_charge;
            $data['order_total'] =$vendor_charge;

            
            $updatequery = $this->generalmodel->updaterecord('orders',$data,array('id'=>$oid));
            if($updatequery){

                update_order_steps($oid,site_url('printery_shops'));
                $return = array('success'=>true,'msg'=>$link);
            }else{
                $return = array('success'=>false,'msg'=>'internal error');
            }
            echo json_encode($return);
        }
    }
    
    public function update_trans_order_vendor(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){   
            
            $printery = $this->input->post('printery');
            $oid = $this->input->post('oid');

            $this->session->set_userdata('continue_order',$oid); 
            $q = $this->generalmodel->accept_trans_proposal($oid,$printery);

            if(!empty($oid)){
                $odata = $this->generalmodel->order_detail($oid,'0');
                $this->set_order_in_session($oid);
                $this->session->set_userdata('header_placeholder',"Translation");                

            
                update_order_steps($oid,site_url('payment'));
                //update_order_steps($oid,site_url('printery_shops'));
                $return = array('success'=>true,'msg'=>site_url('payment'));
            }else{
                $return = array('success'=>false,'msg'=>'internal error');
            }
            echo json_encode($return);
        }
    }


    public function set_address(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){   
            $post_id = $this->input->post('address_id');
            $add_detail = $this->generalmodel->getparticularData('id,address','customer_address',array('id'=>$post_id),'row_array');

            if(!empty($add_detail)){
                $order_address['order_id'] = $oid =  $this->session->userdata['continue_order'];
                $order_address['c_add_id'] = $add_detail['id'];
                $order_address['address'] = $add_detail['address'];
                //$addQuery = $this->generalmodel->add('order_address',$order_address);

                //===order products==========
            $post_prod = $this->session->userdata('post_products');
            if(!empty($post_prod)){
                $products = $post_prod['products'];
                $price = $post_prod['price'];
                $quantity = $post_prod['qty'];
                $name = $post_prod['name'];
                $sess_pro = array();   
                if(!empty($products)){
                    $i=0;
                    foreach($products as $key=>$val){
                        $pro[$i]['order_id'] = $oid;
                        $pro[$i]['prod_id'] = $val;
                        $pro[$i]['prod_price'] = $price[$key];
                        $pro[$i]['prod_qty'] = $quantity[$key];
                        $pro[$i]['created_at'] = date('Y-m-d h:i:s');

                        $stockData[$i]['qty'] = $quantity[$key];
                        $stockData[$i]['id'] = $val;

                        $sess_pro[$val]['name'] = $name[$key];
                        $sess_pro[$val]['qty'] = $quantity[$key];
                        $sess_pro[$val]['price'] = $price[$key];
                        $i++;
                    } 
                }                
            }             
            //===order products==========

                $delivery_charge = get_delivery_charges();
                $addQuery = $this->generalmodel->update_order_delivery_address($add_detail['id'],$delivery_charge,$order_address);                    

                if(!empty($addQuery)){
                    
                    if(!empty($products)){
                        $this->generalmodel->add_order_products($oid,$pro,$stockData);
                        //$this->session->set_userdata('order_products',$sess_pro);
                    }

                    // $this->session->set_userdata('pickup_delivery','delivery');
                    // $this->session->set_userdata('delivery_address',$add_detail['address']);

                    // $current_order = $this->session->userdata('current_order');
                    // $current_order['pickup_delivery'] = '2';
                    // $current_order['delivery_charge'] = $delivery_charge;
                    // $current_order['delivery_address'] = $add_detail['id'];

                    // $this->session->set_userdata('current_order',$current_order);  

                    $this->set_order_in_session($oid);


                    $return = array('success'=>true,'href'=>site_url('payment'),'msg'=>'address selected');
                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }
            }else{
                $return = array('success'=>false,'msg'=>'something went wrong');
            }
            // get address from customer_address table where given id from post
            //     
           // echo "<pre>"; print_r($_POST); exit;
        }else{
            $return = array('success'=>false,'msg'=>'login first');
        }
        echo json_encode($return);
    }

    public function add_new_address(){
		if(!empty($this->userdata) && $this->input->is_ajax_request()){  

			$customer_address['user_id'] = $this->userdata['user_id'];
			$customer_address['label'] = $this->input->post('label');
			$customer_address['address'] = $this->input->post('address');
			$customer_address['latitude'] = $this->input->post('latitude');
			$customer_address['longitude'] = $this->input->post('longitude');

			$order_address['order_id'] = $current_order_id = $this->session->userdata['continue_order'];
			$order_address['address'] = $customer_address['address'];

            $delivery_charge = get_delivery_charges();
              


            //===order products==========
            $post_prod = $this->session->userdata('post_products');
            if(!empty($post_prod)){
                $products = $post_prod['products'];
                $price = $post_prod['price'];
                $quantity = $post_prod['qty'];
                $name = $post_prod['name'];
                $sess_pro = array();   
                if(!empty($products)){
                    $i=0;
                    foreach($products as $key=>$val){
                        $pro[$i]['order_id'] = $current_order_id;
                        $pro[$i]['prod_id'] = $val;
                        $pro[$i]['prod_price'] = $price[$key];
                        $pro[$i]['prod_qty'] = $quantity[$key];
                        $pro[$i]['created_at'] = date('Y-m-d h:i:s');

                        $stockData[$i]['qty'] = $quantity[$key];
                        $stockData[$i]['id'] = $val;

                        $sess_pro[$val]['name'] = $name[$key];
                        $sess_pro[$val]['qty'] = $quantity[$key];
                        $sess_pro[$val]['price'] = $price[$key];
                        $i++;
                    } 
                }                
            }             
            //===order products==========


            $this->db->trans_start();

		    $this->generalmodel->add_new_order_address($customer_address,$order_address,$delivery_charge);

            if(!empty($products)){
                $this->generalmodel->add_order_products($current_order_id,$pro,$stockData);
            }
            
            $this->db->trans_complete();

            $this->set_order_in_session($current_order_id);
			$return = array('success'=>true,'href'=>site_url('payment'),'msg'=>'added successfully');
			echo json_encode($return);
		}
    }

    public function orderDetail(){
		$this->frontview('web/order_detail_design',$this->data); 
    }


    public function order_detail($oid){
        $oid = decoding($oid);
        $this->data['detail'] = $this->generalmodel->order_detail($oid);

        //echo "<pre>"; print_r($this->data['detail']); exit;
        $this->frontview('web/order_detail',$this->data); 
    }

    public function check_prod_stock(){
        if(!empty($this->userdata) && !empty($this->continue_order) && $this->input->is_ajax_request()){

            $prod_id = $this->input->post('prod_id');
            $qty =  $this->input->post('qty');
            $already_having =  $this->input->post('already_having');

            $check_qty = $qty-$already_having;
            //$where = "`id`=".$prod_id." AND `status`='0' AND `qty`>=".$qty;
            $where = "`id`=".$prod_id." AND `status`='0' AND `qty`>=".$check_qty;
            $prodQtyCheck = $this->generalmodel->getparticularData('id,price,qty','products',$where,'row_array');

// echo "<pre>"; print_r($_POST); 
// echo $this->db->last_query();
// print_r($prodQtyCheck);
            if(!empty($prodQtyCheck)){
                $return = array('success'=>true,'msg','price'=>$prodQtyCheck['price']);
            }else{
                --$qty;
                if($qty==0){
                    $return = array('success'=>false,'msg'=>' Not available in stock');
                }else{
                    $return = array('success'=>false,'msg'=>'only '.$qty.' in stock');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'login first');
        }
        echo json_encode($return);
    }

    public function set_image_rotation(){
        if(!empty($this->userdata) && !empty($this->continue_order) && $this->input->is_ajax_request()){
            $image_rotation = $this->input->post('image_rotation');
            $this->session->set_userdata('image_rotation',$image_rotation);  
        }
    }

    public function set_color(){
        if(!empty($this->userdata) &&  $this->input->is_ajax_request()){
            $color = $this->input->post('color');
            $this->session->set_userdata('color',$color);
            $return = array('success'=>true,'msg'=>'set'); 
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }  
        echo json_encode($return);
    }

    public function set_binding(){
        if(!empty($this->userdata) &&  $this->input->is_ajax_request()){
            $binding = $this->input->post('binding');
            $this->session->set_userdata('binding',$binding);
            $return = array('success'=>true,'msg'=>'set'); 
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }  
        echo json_encode($return);        
    }

    //===========notes

    public function save_note(){
        if(!empty($this->userdata)){

            $project_name = $this->input->post('project_name');
            $note_id = $this->input->post('note_id');
            $total_pages = $this->input->post('total_pages');

            $page_range = $this->session->userdata('page_range');
            $notes_page_from = $this->session->userdata('notes_page_from');
            $notes_page_to = $this->session->userdata('notes_page_to');

            if(empty($project_name)){
                echo json_encode(array('success'=>false,'msg'=>'Please enter project name')); exit;
            }else{
                
                $service_type = ($this->session->userdata('service_type')=='normal')?'1':'2';
                $order_type = $this->session->userdata('order_type');
                
                if(empty($service_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Service')); exit;
                }elseif(empty($order_type)){
                    echo json_encode(array('success'=>false,'msg'=>'Please select Order')); exit;
                }elseif(empty($note_id)){
                    echo json_encode(array('success'=>false,'msg'=>'Please Select Note Document')); exit;
                }elseif((empty($page_range) || $page_range == 'range' ) && (empty($notes_page_from) || empty($notes_page_to))){
                    echo json_encode(array('success'=>false,'msg'=>'Please Select Pages')); exit;
                }

                //exit;

                $odata['customer'] = $this->userdata['user_id'];
                $odata['service_type'] = $service_type;
                $odata['order_type'] = $order_type;
                $odata['project_name'] = $project_name;
                $odata['note_id'] = $note_id;

                $odata['note_from'] = $notes_page_from;
                $odata['note_to'] = $notes_page_to;
                $odata['note_total_pages'] = $total_pages;
                $odata['note_page_range'] = $page_range;

                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['status'] = '0';
                $odata['step_url'] = $this->input->post('current_url');

                $all_steps[0] = $this->input->post('current_url');
                $odata['all_steps'] = serialize($all_steps);

                
                if($this->input->post('continue')=='true'){
                    $odata['progress_status'] = "being_printed";
                }else{
                    $odata['progress_status'] = "saved";
                }

                $oid = $this->session->userdata('continue_order');
                if(empty($oid)){
                    $last_id = $this->generalmodel->add('orders',$odata);
                    $current_order['order_id'] = $order_id = $last_id;
                }else{
                    $updateid = $this->generalmodel->updaterecord('orders',$odata,array('id'=>$oid));
                    $current_order['order_id'] = $order_id = $oid;
                }

                if($this->input->post('continue')=='true'){
                    $this->set_order_in_session($order_id);
                }
                
                if(!empty($last_id) || !empty($updateid)){

//echo "<pre>"; print_r($_POST);
                    
                    if($this->input->post('continue')=='true'){
                        if($service_type==1){
                            $return = array('success'=>true,'msg'=>'Order Saved','href'=>'printery_shops');
                        }else{
                            $return = array('success'=>true,'msg'=>'Order Saved','href'=>'delivery');
                        }
                    }else{

                        $this->session->unset_userdata(array('notes_page_from','notes_page_to','page_range','order_type','current_order','continue_order'));

// print_r($_SESSION);
// exit;

                        $return = array('success'=>true,'msg'=>'Order Saved');
                    }

                }else{
                    $return = array('success'=>false,'msg'=>'internal error');
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    public function set_notes_page_range(){
        if(!empty($this->userdata) && $this->input->is_ajax_request()){

            $page_from = $this->input->post('page_from'); 
            $page_to = $this->input->post('page_to');
            $page_range = $this->input->post('all_pages');

            if($page_range =='range'){

           
                if(empty($page_from) || !is_numeric($page_from)){
                    $return = array('success'=>false,'msg'=>'Please enter valid number');
                    echo json_encode($return); exit;
                }elseif(empty($page_to) || !is_numeric($page_to)){
                    $return = array('success'=>false,'msg'=>'Please enter valid number');
                    echo json_encode($return); exit;                
                }else{
                    $this->session->set_userdata(array('page_range'=>$page_range,'notes_page_from'=>$page_from,'notes_page_to'=>$page_to)); 
                }  
            }else{
                $this->session->unset_userdata(array('notes_page_from','notes_page_to'));  
                $this->session->set_userdata(array('page_range'=>$page_range));  
            }
            $return = array('success'=>true,'msg'=>'set');
        
        }else{
            $return = array('success'=>false,'msg'=>'Please login first');
        }
        echo json_encode($return);
    }

    private function set_order_in_session($oid){

        $this->session->set_userdata('continue_order',$oid);
        $odata = $this->generalmodel->order_detail($oid,'0');
        //$this->session->unset_userdata('order_type'); 
        //echo "<pre>"; print_r($odata); 

        $current_order['order_id'] = $oid;
        $current_order['service_type'] = $odata['service_type'];
        $current_order['order_type'] = $odata['order_type'];
        $current_order['project_name'] = $odata['project_name'];
        if($odata['order_type']==1 || $odata['order_type']==2){
            $current_order['print_type'] = $odata['print_type'];
            $current_order['paper_type'] = $odata['paper_type'];
            $current_order['paper_size'] = $odata['paper_size'];
            $current_order['no_of_copies'] = $odata['no_of_copies'];
            $current_order['image_uploaded'] = $odata['image_uploaded'];
            $current_order['printing_side'] =  $odata['printing_side'];
            $current_order['banner_size'] =  $odata['width'];
            $current_order['bannersize'] =  $odata['bannersize'];
            $current_order['papertype']   =  $odata['papertype'];
            $current_order['papersize']   =  $odata['papersize'];
            $current_order['img_original_name'] = $odata['img_original_name'];
            $current_order['image_rotation'] = $odata['image_rotation'];
            $current_order['color'] = $odata['color'];             
            $current_order['binding'] = $odata['binding'];                  
            $current_order['products'] = $odata['products'];             
        
        }elseif($odata['order_type']==4){
            $current_order['note_id'] =  $odata['note_id'];
            $current_order['note_from'] = $odata['note_from'];
            $current_order['note_to'] = $odata['note_to'];
            $current_order['note_total_pages'] = $odata['note_total_pages'];
            $current_order['note_page_range'] = $odata['note_page_range'];
            
            $this->session->set_userdata(array('page_range'=>$odata['note_page_range'],'notes_page_from'=>$odata['note_from'],'notes_page_to'=>$odata['note_to']));
        }
        $current_order['vendor_charge'] = $odata['amount'];
        $current_order['delivery_charge'] = $odata['delivery_charge'];
        $current_order['product_total'] = $odata['product_total'];
        $current_order['order_total'] = $odata['order_total'];
        $current_order['coupon_start'] = $odata['coupon_start'];
        $current_order['coupon_end'] = $odata['coupon_end'];
        $current_order['applied_coupon_amt'] = $odata['applied_coupon_amt'];
        $current_order['all_steps'] = $odata['all_steps'];
        $current_order['step_url'] = $odata['step_url'];             
        $current_order['delivery_address'] = $odata['delivery_address'];             
        $current_order['pickup_delivery'] = $odata['pickup_delivery'];            


        if($odata['order_type']==3){
            $current_order['trans_type_id'] =  $odata['trans_type_id'];
            $current_order['lang_from_id'] =  $odata['lang_from'];
            $current_order['lang_to_id'] =  $odata['lang_to'];
            $current_order['trans_doc'] =  $odata['trans_doc'];
            $current_order['doc_org_name'] =  $odata['doc_org_name'];
            $current_order['lang_from'] =  $odata['from'];
            $current_order['lang_to'] =  $odata['to'];
            $current_order['trans_type'] =  $odata['trans_type'];


            $this->session->set_userdata(array('order_type'=>$odata['order_type'],'lang_from_id'=>$odata['lang_from'],'lang_to_id'=>$odata['lang_to'],'trans_type_id'=>$odata['trans_type_id'],'trans_doc'=>$odata['trans_doc'])); 

        }

        $this->session->set_userdata('current_order',$current_order);
        if(!empty($odata['products'])){
            foreach($odata['products'] as $p){
                $key = $p['prod_id'];
                $order_products[$key]['name'] = $p['name'];
                $order_products[$key]['qty'] = $p['qty'];   
                $order_products[$key]['price'] =  $p['price']; 
                $order_products[$key]['image'] =  $p['image']; 
            }
                          
            $this->session->set_userdata(array('order_products'=>$order_products));
        }

        if(!empty($odata['vendor'])){

            $printry_detail = $this->printery_withprice($oid,$odata['vendor']);
            $this->session->set_userdata('selected_printry',$printry_detail);
            $this->session->set_userdata('vendor_charge',$printry_detail['vendor_charge']);            
        } 

        $sessArr['pickup_delivery'] = ($odata['pickup_delivery']=='1') ? 'pickup' : (($odata['pickup_delivery']=='2') ? 'delivery' : '');   

        if($odata['order_type']==1 || $odata['order_type']==2){

            $servtype = ($odata['service_type']=='1' || $odata['service_type']=='normal')?'normal':'express';
            $this->session->set_userdata(array('order_type'=>$odata['order_type'],'service_type'=>$servtype,'print_type'=>$odata['print_type'],'paper_type'=>$odata['paper_type'],'paper_size'=>$odata['paper_size'],'uploaded_image'=>$odata['image_uploaded'],'image_rotation'=>$odata['image_rotation'],'img_original_name'=>$odata['img_original_name'],'copy_number'=>$odata['no_of_copies']));   

            if(!empty($odata['width'])){
                $sessArr['banner_width'] =  $odata['width'];
            }
        }
        $this->session->set_userdata($sessArr);                                     
          
    }

    public function show_translation_proposals($oid){
        $oid = decoding($oid);
        update_order_steps($oid,current_url());

		$this->data['list'] = $this->generalmodel->trans_proposal($oid);
		$this->data['userdata'] = $this->userdata;

        $this->frontview('web/trans_proposal_page',$this->data);
    }
    


	public function userlogout(){
        $this->session->unset_userdata(array('userdata','service_type','custom_print_option','order_type','cartdata','paper_type','copy_number','paper_size','printing_side','uploaded_image','print_type','selected_printry','current_order','pickup_delivery','banner_width','cart_data','coupon_applied','order_products','continue_order','header_placeholder','image_rotation','img_original_name','vendor_charge','order_confirmed','delivery_address','color','binding','trans_type_id','trans_type','lang_from_id','lang_from','lang_to_id','lang_to','trans_doc','doc_original_name','page_range','notes_page_from','notes_page_to','post_products','trans_order'));
        redirect('/');
	}

 }