<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct(){
        
        parent::__construct();
        if(!empty($this->session->userdata['admindata'])){
            $this->admindata = $this->session->userdata['admindata'];
        }else{
            redirect('adminlogin');
        }
    }
    
	public function index()
	{
        $this->data['active_vendor'] = $this->generalmodel->printeryshop_count(array('p.status'=>'0'));
        $this->data['in_active_vendor'] = $this->generalmodel->printeryshop_count(array('p.status'=>'1'));
        $this->data['blocked_vendor'] = $this->generalmodel->printeryshop_count(array('p.status'=>'2'));
        $this->data['pending_vendor'] = $this->generalmodel->printeryshop_count(array('p.status'=>'3'));
       
        $this->data['title'] = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('service_type ', '1');
        $query = $this->db->get();
        $totalNororder = $query->num_rows();
        
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('service_type ', '2');
        $query = $this->db->get();
        $totalExporder = $query->num_rows();
         
        // $this->data['active_orders'] = $this->generalmodel->get_active_order();
	     $this->data['pending_orders'] = $this->generalmodel->get_pending_order();
	     $this->data['completed_orders'] = $this->generalmodel->get_completeds_order();
	     $this->data['total_orders'] = $this->generalmodel->get_total_order();
         // Earning Chart Details
        $this->db->select('extract(MONTH from created_at) as month,sum(admin_charge) as total_value');
        $this->db->from('orders');
        $this->db->where('payment_status','1');
        $this->db->group_by('month'); 
        $query = $this->db->get();
       // echo $this->db->last_query(); die;
        $chart_data1 = $query->result_array();
        $chart_data = array();
        foreach($chart_data1 as $chart_val){
            $chart_data[$chart_val['month']] = $chart_val['total_value'];
        } 
        $this->load->view('common/header',$this->data);
        $this->load->view('common/left_nav',$this->data);
        $this->load->view('admin/dashboard',['totalNororder' => $totalNororder,'totalExporder' => $totalExporder,'chart_data' => $chart_data]);
        $this->load->view('common/footer');
		
	}
	
	public function logout(){
	    //$this->session->unset_userdata('admindata');
	    $this->session->sess_destroy();
		redirect('/');
	    
	}
}