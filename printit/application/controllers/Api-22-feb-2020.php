<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('vendormodel');
    }
    
    public function user_resgister(){
        	    	    
	    if(!empty($this->input->post())){
	        
	        if($this->form_validation->run('app_user_registeration')){
				
	            $password = $this->input->post('password');
                //$cpassword = $this->input->post('cpassword');
                $address = $this->input->post('address');
                $user_lat = $this->input->post('user_lat');
                $user_lng = $this->input->post('user_lng');
	            $email = $this->input->post('email');
	            $gender = $this->input->post('gender');

				$check_exist = $this->generalmodel->getparticularData('id','users',array('email'=>$email,'delete_status'=>'0'),'row_array');

                if(!empty($check_exist)){
                    $return = array('success'=>false,'msg'=>"Email address is already registered with us!");
                    echo json_encode($return); exit;
                }

                // if($password !== $cpassword){
                //     $return = array('success'=>false,'msg'=>"Password don't match!");
                //     echo json_encode($return); exit;
                // }

	            $fname = $this->input->post('f_name');
	            $lname = $this->input->post('l_name');
	            $mobile = $this->input->post('mobile');
		        $userdata['role'] = '3';
		        $userdata['email'] = $email;
		        $userdata['firstname'] = $fname;
		        $userdata['lastname'] = $lname;
		        $userdata['mobile'] = $mobile;
		        if(!empty($gender)){
		            $userdata['gender'] = $gender;
		        }
		        $userdata['password'] = md5($this->input->post('password'));
		        $userdata['created_at'] = date('Y-m-d h:i:s');
		        if(!empty($user_lat)){
    		        $userdata['latitude'] = $user_lat;
                    $customer_address['latitude'] = $user_lat;
		        }
                if(!empty($user_lng)){    		        
    		        $userdata['longitude'] = $user_lng;
                    $customer_address['longitude'] = $user_lng;
		        }
                if(!empty($address)){    		        
		            $userdata['address'] = $address;
                    $customer_address['address'] = $address;
                }
                
                $customer_address['label'] = "Main address";
                
	            //if($this->generalmodel->add('users',$userdata)){
                if($this->generalmodel->register_user($userdata,$customer_address)){	                
	                $return = array('success'=>true,'msg'=>'registered successfully');
	            }else{
	                $return = array('success'=>'false','msg'=>'internal error');        
	            }
	        }else{
	            $return = array('success'=>'false','msg'=>validation_errors());
	        }
        }else{
           $return = array('success'=>'false','msg'=>'post empty');
        }
        echo json_encode($return);
    }
    
    public function login(){
        if(!empty($this->input->post())){
	        if($this->form_validation->run('user_login')){
	            $email = $this->input->post('email');
	            $password = $this->input->post('password');
	            
	            $userdata = $this->generalmodel->getparticularData('id,email,role,firstname,lastname,  CONCAT_WS(" ",firstname,lastname) AS username,latitude,longitude','users',array('email'=>$email,'password'=>md5($password),'status'=>'0','delete_status'=>'0'),'row_array');

	            if(!empty($userdata)){
	                $return = array('success'=>true,'role'=>$userdata['role'],'user_id'=>$userdata['id']);
	            }else{
	                $return = array('success'=>false,'msg'=>'Invalid email or password');
	            }
	        }else{
	            $return = array('success'=>false,'msg'=>validation_errors());
	        }
        }else{
            $return = array('success'=>'false','msg'=>'post empty');
        }
        echo json_encode($return);
    }
    
    public function printery_registeration(){

	    if(!empty($this->input->post())){
	        if($this->form_validation->run('vendor_registeration')){
	            
	            $email = $this->input->post('email');
	            $password = $this->input->post('password');
                $cpassword = $this->input->post('c_password');
                $address = $this->input->post('address');
                $user_lat = $this->input->post('vendor_lat');
                $user_lng = $this->input->post('vendor_lng');

                if($password !== $cpassword){
                    $return = array('success'=>false,'msg'=>"Password don't match!");
                    echo json_encode($return); exit;
                }

                $check_exist = $this->generalmodel->getparticularData('id','users',array('email'=>$email,'delete_status'=>'0'),'row_array');

                if(!empty($check_exist)){
                    $return = array('success'=>false,'msg'=>"Email address is already registered with us!");
                    echo json_encode($return); exit;
                }

		        $userdata['role'] = '2';
		        //$userdata['firstname'] = $this->input->post('firstname');
		        $userdata['email'] = $this->input->post('email');
		        $userdata['mobile'] = $this->input->post('contact_no');
		        $userdata['password'] = md5($password);
		        $userdata['created_at'] = date('Y-m-d h:i:s');
		        $userdata['address'] = $address;		        
		        
		        
		        $printryData['shop_name'] = $this->input->post('shop_name');
		        $printryData['building'] = $address;
		        $printryData['latitude'] = $user_lat;
		        $printryData['longitude'] = $user_lng;
		        $printryData['registeration_date'] =date('Y-m-d h:i:s');
		        $printryData['status'] = '0';
	        
		        $userid = $this->vendormodel->add_vendor($userdata,$printryData);

		        if(!empty($userid)){
		        	$return = array('success'=>true,'msg'=>'Registered successfully');
		        }else{
		        	$return = array('success'=>false,'msg'=>'Internal Error');
		        }
		    }else{
            	$return = array('success'=>false,'msg'=>validation_errors());
            }
	    }else{
            $return = array('success'=>false,'msg'=>'post empty');
	    }
        echo json_encode($return);
    }

	public function forgotpass(){
	    if(!empty($this->input->post())){
	        
            $email = $this->input->post('email');
            $token = md5(uniqid(rand(), true));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            if($this->form_validation->run())
            {
                $userdata = $this->generalmodel->getparticularData('*','users',array('email'=>$email,'status'=>'0','delete_status'=>'0'),'row_array');
                if(empty($userdata)){
                     $return  = array('success'=>false,'msg'=>'Email Address is not registered with us');  
                }else{
                $insertdata['user_email'] = $email;
                $insertdata['token'] = $token;
                $insertdata['expiry'] = date('Y-m-d',strtotime("+1 day"));
                
                if($this->generalmodel->add('password_reset',$insertdata)){
                    
                    $link = '<a href="'.site_url('reset_password').'?email='.$email.'&&token='.$token.'"></a>';
                    $from = "soniyakukreja091@gmail.com";
                    $to = $email;
                    $subject = "Password Reset Request";
                    $message = "Click Here to reset your password <br> ".$link;
                    
                    if($this->sendGridMail($from,$to,$subject,$link)){
    
                    $return  = array('success'=>true,'msg'=>'mail sent','link'=>$link);    
                    }else{
                        $return  = array('success'=>false,'msg'=>'mail failed');                    
                    }
                    
                }else{
                    $return  = array('success'=>false,'msg'=>'internal error');
                }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}

	public function reset_new_password(){
	    if(!empty($this->input->post())){
	        
            $email = $this->input->post('email');
            $token = $this->input->post('token');
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required');
            $this->form_validation->set_rules('token', 'Token', 'required');
            if($this->form_validation->run())
            {
                $resetData = $this->generalmodel->getparticularData('*','password_reset',array('user_email'=>$email,'token'=>$token,'status'=>'0','expiry >'=>date('Y-m-d')),'row_array');
                if(empty($resetData)){
                     $return  = array('success'=>false,'msg'=>'Link is expired');  
                }else{
                    
                    if($password !== $cpassword){
                        $return = array('success'=>false,'msg'=>"Password don't match!");
                        echo json_encode($return); exit;
                    }else{
                        $this->generalmodel->updaterecord('password_reset',array('status'=>'1'),array('user_email'=>$email,'token'=>$token));
                        $this->generalmodel->updaterecord('users',array('password'=>md5($password)),array('email'=>$email));

                        $from = "test@gmail.com";
                        $to = $email;
                        $subject = "Password Reset Success";
                        $message = "Password reseted successfully";

                        if($this->sendGridMail($from,$to,$subject,$link)){
        
                        $return  = array('success'=>true,'msg'=>'password reset');    
                        }else{
                            $return  = array('success'=>false,'msg'=>'mail failed');                    
                        }
                    }
                }
            }else{
                $return  = array('success'=>false,'msg'=>validation_errors());  
            }
	    }
	    echo json_encode($return);
	}
	
	public function services(){
	    $this->check_header();
        $data = $this->generalmodel->getparticularData("id,name,description","services",array('status'=>'0'),"result_array");
        if(!empty($data)){
            $return  = array('success'=>true,'data'=>$data); 
        }else{
            $return  = array('success'=>false,'data'=>"data not available"); 
        }
        echo json_encode($return);
	}
	
	public function custom_print_options(){
	    $this->check_header();
        $data = $this->generalmodel->getparticularData("id,name,image,url","print_type",array('status'=>'0'),"result_array");
        if(!empty($data)){
            foreach($data as $key=>$value){
                $data[$key]['image'] = base_url('print_type_images/'.$value['image']);
            }
            $return  = array('success'=>true,'data'=>$data); 
        }else{
            $return  = array('success'=>false,'data'=>"data not available"); 
        }
        echo json_encode($return);
	}	
	
	public function dropdown_options(){
	    $this->check_header();
        $data['paper_type'] = $this->generalmodel->getparticularData("id,name,description","paper_type",array('status'=>'0'),"result_array");
        $data['paper_size'] = $this->generalmodel->getparticularData("id,name","paper_size",array('status'=>'0'),"result_array");
        $data['translation_types'] = $this->generalmodel->getparticularData("id,name","translation_types",array('status'=>'0'),"result_array");
        $data['translation_lang'] = $this->generalmodel->getparticularData("id,name","translation_lang",array('status'=>'0'),"result_array");
        $data['banner_size'] = $this->generalmodel->getparticularData("id,name","banner_size",array('status'=>'0'),"result_array");
        $data['printing_side'] = array(array('id'=>"single",'name'=>'Single'),array('id'=>"double",'name'=>'Double'));
        $data['color'] = array(array('id'=>"colored","name"=>'Colored'),array("id"=>"Black_and_White","name"=>'Black and White'));
        $data['binding'] = array(array('id'=>"Yes","name"=>'Yes'),array('id'=>"No","name"=>'No'));
        $data['notes_pages'] = array(array('id'=>"all","name"=>"All"),array('id'=>"range","name"=>'Range'));
        $data['translation_print_option'] = array(array('id'=>"hard_copy","name"=>"Hard Copy"),array('id'=>"soft_copy","name"=>'Soft Copy'));

        echo json_encode($data);	    
	}
	
	public function notes_data(){
        $data['notes_cat'] =  $this->generalmodel->getparticularData("id,name","notes_categories",array('status'=>'0'),"result_array");
       
        $notes = $this->generalmodel->getparticularData("id,category,name,description,document,image,total_pages,","notes",array('status'=>'0','is_delete'=>'0'),"result_array");
        if(!empty($notes)){ foreach($notes as $key=>$value){
            $notes[$key]['document']     = base_url('uploaded_notes/'.$value['document']);
            $notes[$key]['image']        = base_url('uploaded_notes/images/'.$value['image']);
            $notes[$key]['description']  = strip_tags($value['description']);
            $text =  strip_tags($value['description']);
            $text =  preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
            $notes[$key]['description']  =  $text;
        }}
        $data['notes'] = $notes;
        echo json_encode($data);
	}
	
	public function save_custom_print_order(){
	    
	    if(!empty($this->input->post())){
	        
	        if($this->form_validation->run('api_custom_print_step1')){
	        
    	        $order_type = 1;
    	        $service_type = $this->input->post('order_type');
                $user_id = $this->input->post('user_id');
                $project_name = $this->input->post('project_name');

                $print_type = $this->input->post('print_type');
                $paper_type = $this->input->post('paper_type');
                $paper_size = $this->input->post('paper_size');
                $image = $this->input->post('image');
                $copy_number = $this->input->post('copy_number');
                
                //===flyer
                if($print_type==2){
                    $printing_side = $this->input->post('printing_side');
                    $this->form_validation->set_rules('printing_side', 'Printing Sides', 'required');
                    
                    if($this->form_validation->run()){
                        $odata['printing_side'] = $printing_side;
                    }else{
                        $return = array('success'=>false,'msg'=>"Please select printing side option");
                        echo json_encode($return); exit;                        
                    }
                }
                
                //===rollup
                if($print_type==1 || $print_type==2 || $print_type==3){
                    $paper_size = $this->input->post('paper_size');
                    $this->form_validation->set_rules('paper_size', 'Paper Size', 'required');
                    
                    if($this->form_validation->run()){
                        $odata['paper_size'] = $paper_size;
                    }else{
                        $return = array('success'=>false,'msg'=>"Please select paper size option");
                        echo json_encode($return); exit;                        
                    }
                }
                
                //===banner
                if($print_type==4){
                    $width = $this->input->post('width');
                    $this->form_validation->set_rules('width', 'Banner Width', 'required');
                    
                    if($this->form_validation->run()){
                        $odata['width'] = $width;
                    }else{
                        $return = array('success'=>false,'msg'=>"Please select banner width option");
                        echo json_encode($return); exit;                        
                    }                    
                }
                
                //echo "<pre>"; print_r($_FILES); exit;
                if(!empty($_FILES)){
                    $fname ='image';
                    $img_original_name = $_FILES['image']['name'];
                    
                    $upload_data = $this->uploadDoc($fname,'order_uploads',array('jpg','jpeg','png'));
                    if(!empty($upload_data['error'])){
                        $return = array('success'=>false,'msg'=>$upload_data['error']);
                        echo json_encode($return); exit;
                    }else{
                        $image_uploaded = $upload_data['file_name'];
                    }
                }else{
                    $return  = array('success'=>false,'data'=>"Please upload file");
                    echo json_encode($return); exit;
                } 
                
                $odata['no_of_copies'] = $copy_number;
                $odata['paper_type'] = $paper_type;
                $odata['print_type'] = $print_type;
                $odata['project_name'] = $project_name;
                $odata['order_type'] = $order_type;
                $odata['service_type'] = $service_type;
                $odata['customer'] = $user_id;                	
                $odata['status'] = '0';
                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['image_rotation'] = '';
                $odata['img_original_name'] = $img_original_name;
                $odata['image_uploaded'] = $image_uploaded;
                $odata['insert_by'] = 'app';
                
                
                $print_type = get_print_type_data("",$print_type);
                if(!empty($print_type)){ 
                    $odata['step_url'] = $current_url[0] =  site_url('print-type/').strtolower($print_type['name']);
                    $odata['all_steps'] = serialize($current_url);
                }else{
                    $odata['all_steps'] = '';
                    $odata['step_url'] = '';                    
                }
                
                $order_id = $this->generalmodel->add('orders',$odata);
                if(!empty($order_id)){
                    $return  = array('success'=>true,'data'=>"order saved",'order_id'=>$order_id);
                }else{
                    $return  = array('success'=>false,'data'=>"internal error");
                    echo json_encode($return); exit;
                }
	        }else{
	            $return  = array('success'=>false,'data'=>validation_errors());
	        }
	    }else{
	        $return  = array('success'=>false,'data'=>"post empty");
	    }
	    echo json_encode($return);
	}
	
	public function save_quick_print_order(){
	    
	    if(!empty($this->input->post())){
	        
	        if($this->form_validation->run('api_quick_print_step1')){
	        
    	        $service_type = $this->input->post('order_type');
    	        $order_type = 2;
                $user_id = $this->input->post('user_id');
                $project_name = $this->input->post('project_name');

                $paper_type = $this->input->post('paper_type');
                $paper_size = $this->input->post('paper_size');
                $image = $this->input->post('image');
                $copy_number = $this->input->post('copy_number');
                $color = $this->input->post('color');
                $binding = $this->input->post('binding');
                
                if(!empty($_FILES)){
                    $fname ='image';
                    $img_original_name = $_FILES['image']['name'];
                    
                    $upload_data = $this->uploadDoc($fname,'order_uploads',array('jpg','jpeg','png'));
                    if(!empty($upload_data['error'])){
                        $return = array('success'=>false,'msg'=>$upload_data['error']);
                        echo json_encode($return); exit;
                    }else{
                        $image_uploaded = $upload_data['file_name'];
                    }
                }else{
                    $return  = array('success'=>false,'data'=>"Please upload file");
                    echo json_encode($return); exit;
                } 
                
                $odata['no_of_copies'] = $copy_number;
                $odata['paper_size'] = $paper_size;
                $odata['paper_type'] = $paper_type;
                $odata['print_type'] = 0;
                $odata['project_name'] = $project_name;
                $odata['order_type'] = $order_type;
                $odata['service_type'] = $service_type;
                $odata['customer'] = $user_id;                	
                $odata['color'] = $color;                	
                $odata['binding'] = $binding;                	
                $odata['status'] = '0';
                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['image_rotation'] = '';
                $odata['img_original_name'] = $img_original_name;
                $odata['image_uploaded'] = $image_uploaded;
                $odata['insert_by'] = 'app';
                // $odata['all_steps'] = '';
                // $odata['step_url'] = '';

                $odata['step_url'] = $current_url[0] =  site_url('quickprint');
                $odata['all_steps'] = serialize($current_url);
                


            
                $order_id = $this->generalmodel->add('orders',$odata);
                if(!empty($order_id)){
                    $return  = array('success'=>true,'data'=>"order saved",'order_id'=>$order_id);
                }else{
                    $return  = array('success'=>false,'data'=>"internal error");
                    echo json_encode($return); exit;
                }
	        }else{
	            $return  = array('success'=>false,'data'=>validation_errors());
	        }
	    }else{
	        $return  = array('success'=>false,'data'=>"post empty");
	    }
	    echo json_encode($return);	    
	}

	public function save_translation_order(){
	    
	    if(!empty($this->input->post())){
	        
	        if($this->form_validation->run('api_translation')){
	        
    	        $service_type = 2;
    	        $order_type = 3;
                $user_id = $this->input->post('user_id');
                $project_name = $this->input->post('project_name');
                $trans_type = $this->input->post('trans_type');
                $lang_from = $this->input->post('lang_from');
                $lang_to = $this->input->post('lang_to');
                $print_copy_type = $this->input->post('print_copy_type');

                // echo "<pre>"; print_r($_FILES);
                // print_r($_POST);
                // exit;
                if(!empty($_FILES['trans_doc'])){
                    $fname ='trans_doc';
                    $doc_org_name = $_FILES['trans_doc']['name'];
                    
                    $path = 'tran_doc/'.$user_id;
                    if(!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }  
                
                    $upload_data = $this->uploadDoc($fname,$path,array('pdf','docx','doc','JPG','jpeg','jpg','JPEG','png','PNG'));
                    if(!empty($upload_data['error'])){
                        $return = array('success'=>false,'msg'=>$upload_data['error']);
                        echo json_encode($return); exit;
                    }else{
                        $trans_doc = $upload_data['file_name'];
                    }
                }else{
                    $return  = array('success'=>false,'data'=>"Please upload file");
                    echo json_encode($return); exit;
                } 
                
                $odata['progress_status'] = "saved";
                $odata['customer'] = $user_id;                	
                $odata['service_type'] = $service_type;
                $odata['order_type'] = $order_type;
                $odata['project_name'] = $project_name;
                
                $odata['trans_type_id'] = $trans_type;
                $odata['lang_from'] = $lang_from;
                $odata['lang_to'] = $lang_to;
                $odata['trans_doc'] = $trans_doc;
                $odata['doc_org_name'] = $doc_org_name;
                $odata['print_copy_type'] = $print_copy_type;
                $odata['status'] = '0';
                $odata['created_at'] = date('Y-m-d h:i:s');
                $odata['insert_by'] = 'app';


$trans_current_url = site_url('translation');
$odata['step_url'] = $trans_current_url;

$all_steps[0] = $trans_current_url;
$odata['all_steps'] = serialize($all_steps);
                
                $order_id = $this->generalmodel->add('orders',$odata);
                if(!empty($order_id)){
                    $return  = array('success'=>true,'data'=>"order saved",'order_id'=>$order_id);
                }else{
                    $return  = array('success'=>false,'data'=>"internal error");
                    echo json_encode($return); exit;
                }
	        }else{
	            $return  = array('success'=>false,'data'=>validation_errors());
	        }
	    }else{
	        $return  = array('success'=>false,'data'=>"post empty");
	    }
	    echo json_encode($return);	    
	}
	
	public function save_notes_order(){
        $project_name = $this->input->post('project_name');
        $note_id = $this->input->post('note_id');
        $total_pages = $this->input->post('total_pages');

        //$page_range = $this->input->post('page_range');
        // $notes_page_from = $this->input->post('notes_page_from');
        // $notes_page_to = $this->input->post('notes_page_to');
        $service_type = $this->input->post('service_type');
        $user_id = $this->input->post('user_id');
        $order_type=4;

        if(empty($project_name)){
            echo json_encode(array('success'=>false,'msg'=>'Please enter project name')); exit;
        }else{
            if(empty($service_type)){
                echo json_encode(array('success'=>false,'msg'=>'Please select Service Type')); exit;
            }elseif(empty($note_id)){
                echo json_encode(array('success'=>false,'msg'=>'Please Select Note Document')); exit;
            }
            // elseif((empty($page_range) || $page_range == 'range' ) && (empty($notes_page_from) || empty($notes_page_to))){
            //     echo json_encode(array('success'=>false,'msg'=>'Please Select Pages')); exit;
            // }

            $odata['customer'] = $user_id;
            $odata['service_type'] = $service_type;
            $odata['order_type'] = $order_type;
            $odata['project_name'] = $project_name;
            $odata['note_id'] = $note_id;

            // $odata['note_from'] = $notes_page_from;
            // $odata['note_to'] = $notes_page_to;
            $odata['note_total_pages'] = $total_pages;
            $odata['note_page_range'] = "all";

            $odata['created_at'] = date('Y-m-d h:i:s');
            $odata['status'] = '0';

            $odata['progress_status'] = "saved";
            $odata['insert_by'] = "app";
            $odata['step_url'] = $current_url[0] =  site_url('notes');
            $odata['all_steps'] = serialize($current_url);

            $last_id = $this->generalmodel->add('orders',$odata);

            if(!empty($last_id) ){
                $return = array('success'=>true,'msg'=>'Order Saved','order_id'=>$last_id);
            }else{
                $return = array('success'=>false,'msg'=>'internal error');
            }
        }

        echo json_encode($return);
	}
	
	public function complete_order(){
	    
	    $oid = $this->input->post('order_id');
	    $selected_printry = $this->input->post('printry_id');
	    $vendor_charge = $this->input->post('vendor_charge');
	    $pickup_delivery = $this->input->post('pickup_delivery');
        $post_id = $this->input->post('address_id');
        $user_id = $this->input->post('user_id');


        // $data['payment_status'] = $payment_status;
        // $data['status'] = $status;
        // $data['progress_status'] = $progress_status;
        // $data['delivery_address'] = $delivery_address;
        
        if(empty($selected_printry)){
            echo json_encode(array('success'=>false,'msg'=>"Please select printery")); exit;
        }elseif(empty($vendor_charge)){
            echo json_encode(array('success'=>false,'msg'=>"Printery_charges are blank")); exit;
        }elseif(empty($pickup_delivery)){
            echo json_encode(array('success'=>false,'msg'=>"Please select pickup or delivery")); exit;
        }
        
        $data['vendor'] = $selected_printry;
        $data['amount'] = $vendor_charge;
        $data['pickup_delivery'] = $pickup_delivery;
        $data['order_total'] =$vendor_charge;
        
        //$updatequery = $this->generalmodel->updaterecord('orders',$data,array('id'=>$oid));  
        
	    if($pickup_delivery==2){

	        $delivery_charge =  get_delivery_charges();
	        
	        if(!empty($post_id)){
                
                $add_detail = $this->generalmodel->getparticularData('id,address,latitude,longitude','customer_address',array('id'=>$post_id),'row_array');

                if(!empty($add_detail)){
                    $order_address['order_id'] = $oid ;
                    $order_address['c_add_id'] = $add_detail['id'];
                    $order_address['address'] = $add_detail['address'];
                }
                
                //$addQuery = $this->generalmodel->update_order_delivery_address($add_detail['id'],$delivery_charge,$order_address);                    

	        }else{
                $customer_address['user_id'] = $user_id;
                $customer_address['label'] = $this->input->post('label');
                $customer_address['address'] = $this->input->post('address');
                $customer_address['latitude'] = $this->input->post('latitude');
                $customer_address['longitude'] = $this->input->post('longitude');
                
                $order_address['order_id'] = $oid;
                $order_address['address'] = $customer_address['address'];
                
                //$addQuery = $this->generalmodel->add_new_order_address($customer_address,$order_address,$delivery_charge);
	        }
	    }
	    

            
            //===order products==========
            $post_prod = $this->input->post('products_ids');
            $post_qty = $this->input->post('products_qty');
            if(!empty($post_prod)){
                $products = $post_prod['products'];
                $price = $post_prod['price'];
                $quantity = $post_prod['qty'];
                $name = $post_prod['name'];
                $sess_pro = array();   
                if(!empty($products)){
                    $i=0;
                    foreach($products as $key=>$val){
                        $pro[$i]['order_id'] = $oid;
                        $pro[$i]['prod_id'] = $val;
                        $pro[$i]['prod_price'] = $price[$key];
                        $pro[$i]['prod_qty'] = $quantity[$key];
                        $pro[$i]['created_at'] = date('Y-m-d h:i:s');

                        $stockData[$i]['qty'] = $quantity[$key];
                        $stockData[$i]['id'] = $val;

                        $sess_pro[$val]['name'] = $name[$key];
                        $sess_pro[$val]['qty'] = $quantity[$key];
                        $sess_pro[$val]['price'] = $price[$key];
                        $i++;
                    } 
                }                
            }             

    $this->db->trans_start();
        $q1 = $this->generalmodel->updaterecord('orders',$data,array('id'=>$oid)); 
        if($pickup_delivery==2){
            if(!empty($post_id)){
                $q2 = $this->generalmodel->update_order_delivery_address($add_detail['id'],$delivery_charge,$order_address);                    
            }else{
                $q2 = $this->generalmodel->add_new_order_address($customer_address,$order_address,$delivery_charge);
            }
        }
        if(!empty($products)){
            $q3 = $this->generalmodel->add_order_products($oid,$pro,$stockData);
        }
    $this->db->trans_complete();
    
    
        if(empty($q1)){
            $return = array('success'=>false,'data'=>"q1 failed","msg"=>"select Printery");
        }elseif($pickup_delivery==2 && empty($q2)){
             $return = array('success'=>false,'data'=>"q2 failed","msg"=>"select Printery");
        }elseif(!empty($products) && empty($q3)){
             $return = array('success'=>false,'data'=>"q3 failed","msg"=>"select Printery");
        }else{
             $return = array('success'=>true,'data'=>"order complete");
        }
	    echo json_encode($return);  
	}
	
	public function complete_trans_order(){
        
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    
	    $oid = $request->order_id;
	    $post_id = $request->address_id;
	    $user_id = $request->user_id;
	    
	   // $oid = $this->input->post('order_id');
    //     $post_id = $this->input->post('address_id');
    //     $user_id = $this->input->post('user_id');

        if(empty($oid)){
            echo json_encode(array('success'=>false,'msg'=>"Please enter order Id")); exit;
        }
        if(!empty($post_id)){
            
            $add_detail = $this->generalmodel->getparticularData('id,address,latitude,longitude','customer_address',array('id'=>$post_id),'row_array');

            if(!empty($add_detail)){
                $order_address['order_id'] = $oid;
                $order_address['c_add_id'] = $add_detail['id'];
                $order_address['address'] = $add_detail['address'];
            }
        }else{
            $customer_address['user_id'] = $user_id;
            $customer_address['label'] = $this->input->post('label');
            $customer_address['address'] = $this->input->post('address');
            $customer_address['latitude'] = $this->input->post('latitude');
            $customer_address['longitude'] = $this->input->post('longitude');
            
            $order_address['order_id'] = $oid;
            $order_address['address'] = $customer_address['address'];
            
        }


            /*
            //===order products==========

            $post_prod = $this->input->post('products_ids');
            $post_qty = $this->input->post('products_qty');
            if(!empty($post_prod)){
                $products = $post_prod['products'];
                $price = $post_prod['price'];
                $quantity = $post_prod['qty'];
                $name = $post_prod['name'];
                $sess_pro = array();   
                if(!empty($products)){
                    $i=0;
                    foreach($products as $key=>$val){
                        $pro[$i]['order_id'] = $oid;
                        $pro[$i]['prod_id'] = $val;
                        $pro[$i]['prod_price'] = $price[$key];
                        $pro[$i]['prod_qty'] = $quantity[$key];
                        $pro[$i]['created_at'] = date('Y-m-d h:i:s');

                        $stockData[$i]['qty'] = $quantity[$key];
                        $stockData[$i]['id'] = $val;

                        $sess_pro[$val]['name'] = $name[$key];
                        $sess_pro[$val]['qty'] = $quantity[$key];
                        $sess_pro[$val]['price'] = $price[$key];
                        $i++;
                    } 
                }                
            }    

            //===order products==========

            if(!empty($addQuery)){
                
                if(!empty($products)){
                    $this->generalmodel->add_order_products($oid,$pro,$stockData);
                }
            }
            */

    $this->db->trans_start();
        //$q1 = $this->generalmodel->updaterecord('orders',$data,array('id'=>$oid)); 

        if(!empty($post_id)){

            $latitude = $add_detail['latitude'];
            $longitude = $add_detail['longitude'];
            $q2 = $this->generalmodel->update_tran_delivery_address($add_detail['id'],$order_address,$latitude,$longitude);                    
        }else{
            $q2 = $this->generalmodel->add_new_trans_address($customer_address,$order_address);
        }                
            
        // if(!empty($products)){
        //     $q3 = $this->generalmodel->add_order_products($oid,$pro,$stockData);
        // }
    $this->db->trans_complete();
        if(empty($q2)){
            $return = array('success'=>false,'data'=>"q2 failed","msg"=>"internal error");
        }else{
             $return = array('success'=>true,'data'=>"order complete");
        }
	    echo json_encode($return);  
	}

	
    public function my_address_list(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    
	    $user_id = $request->user_id;
	    
	    $data = $this->generalmodel->getparticularData('id,address_type,label,address,latitude,longitude,area,block,street,avenue,house,additional_directions,mobile,landline,building,floor,apartment_no,office',' customer_address',array('user_id'=>$user_id,'status!='=>'2'),'result_array');
	    
	    if(!empty($data)){
	        $return = array('success'=>true,'data'=>$data);
	    }else{
	        $return = array('success'=>false,'data'=>$data);
	    }

	    echo json_encode($return);        
    }
    
    public function myorders(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    
	    $user_id = $request->user_id;

        $saved_orders = $this->generalmodel->orders('0',$user_id);

        $confirmed_orders = $this->generalmodel->orders('1',$user_id);
        $return = array('success'=>true,'saved_orders'=>$saved_orders,'completed_orders'=>$confirmed_orders);

	    echo json_encode($return);        
    }
    
    public function printery_list(){
        	$this->data['list'] = $this->printery_withprice();
    }
    public function order_printery_list(){

        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    if(empty($request)){ 	        
	        echo json_encode(array('success'=>true,"data"=>array(),"msg"=>"post empty"));
	        exit; 
	    }
	    $oid = $request->order_id;
	    
	    $current_order = $this->generalmodel->order_detail($oid,'0');
	    if(empty($current_order)){
	        echo json_encode(array('success'=>true,"data"=>array(),"msg"=>"order don't exist"));
	        exit;
	    }

        $price_where ="";
        if($current_order['service_type']==1){

            if($current_order['order_type']==1){
                
                $price_where .= " AND `print_type`=".$current_order['print_type']." AND `paper_type_id`=".$current_order['paper_type'];   
    
                if($current_order['print_type']==1){
                    $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];
    
                }elseif($current_order['print_type']==2){
                    $price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."'";
    
                }elseif($current_order['print_type']==3){
                    $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];
    
                }elseif($current_order['print_type']==4){
                    $price_where .= " AND `banner_size` =".$current_order['banner_size'];
                }          
    
    
                $where = "ps.status='1' AND vp.`price_status`='1' AND vp.price >0 ";
                if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
    
                $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.price as vendor_charge,vp.price_comb_id")
                ->from('printery_shop as ps')
                ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
                ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
                ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
                ->where($where)
                ->order_by("vp.price","ASC")    
                ->get();
            }elseif($current_order['order_type']==2){
                $price_where = " AND `paper_size_id` =".$current_order['paper_size']." AND `paper_type_id`=".$current_order['paper_type']." AND `color`='".$current_order['color']."' AND `binding`='".$current_order['binding']."'";

                $where = "ps.status='1' AND vp.`price_status`='1' AND vp.price >0 ";
                if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
    
                $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.price as vendor_charge,vp.price_comb_id")
                ->from('printery_shop as ps')
                ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
                ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
                ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
                ->where($where)
                ->order_by("vp.price","ASC")    
                ->get();
    
            }elseif($current_order['order_type']==4){
    
                $where = "npr.status = '0' AND npr.price_status='1'";
                $note_total_pages = $current_order['note_total_pages'];
                $where .= " AND npr.`copy_from` <=".$note_total_pages." AND `copy_to` >=".$note_total_pages;
               
                if(!empty($printery_id)){ $where .= " AND npr.printery_shop_id=".$printery_id; }
    
                $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,npr.printery_shop_id,npr.price as vendor_charge,npr.copy_from,npr.copy_to,npr.price,npr.id as npr_id')
                ->from('notes_price_range as npr')
                ->join('printery_shop as ps','ps.id = npr.printery_shop_id')
                ->where($where)
                ->get();

            }elseif($current_order['order_type']==3){
                $where = "tp.order_id=".$oid;
                
                if(!empty($printery_id)){ $where .= " AND tp.printery_shop_id=".$printery_id; }
    
                $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,tp.printery_shop_id,tp.price as vendor_charge,tp.id as tp_id')
                ->from('trans_proposals as tp')
                ->join('printery_shop as ps','ps.id = tp.printery_shop_id')
                ->where($where)
                ->get();
            }     
            
            if(!empty($printery_id)){
                $data = $query->row_array();
            }else{
                $data = $query->result_array();
            }
        }else{
          
            $user_id = $current_order['customer'];
		    $add_detail = $this->generalmodel->getparticularData("id,latitude,longitude","customer_address","`status`='0' AND `user_id`=$user_id","row_array");
    		
    		$lat = $add_detail['latitude'];
    		$long = $add_detail['longitude'];
    		
            //if(empty($printery_id)){
                $query = "SELECT ps.id, (3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - ps.latitude) * pi()/180 / 2), 2) +COS( ".$lat." * pi()/180) * COS(ps.latitude * pi()/180) * POWER(SIN(( ".$long." - ps.longitude) * pi()/180 / 2), 2) ))) as distance from printery_shop as ps WHERE status='1' AND `delete_status`='0' having distance <= 10 order by distance";
                $list = $this->db->query($query)->result_array();
                if(!empty($list)){ $ids = implode(",",array_column($list,'id'));  }          
            //}



         //echo "<pre>"; print_r($list); 
        // echo "<pre>"; print_r($ids); 
        // echo $this->db->last_query();
        // exit;
        $price_where ="";
        // echo "<pre>"; print_r($_SESSION); exit;
        // echo "<pre>"; print_r($current_order); exit;
        if($current_order['order_type']==1){
            
            $price_where .= " AND `print_type`=".$current_order['print_type']." AND `paper_type_id`=".$current_order['paper_type'];   

            if($current_order['print_type']==1){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==2){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size']." AND  `printing_sides`='".$current_order['printing_side']."'";

            }elseif($current_order['print_type']==3){
                $price_where .= " AND `paper_size_id` =".$current_order['paper_size'];

            }elseif($current_order['print_type']==4){
                $price_where .= " AND `banner_size` =".$current_order['banner_size'];
            }          


            $where = "ps.status='1' AND vp.`price_status`='1'  AND vp.exp_price >0  ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND ps.id IN(".$ids.")"; }


            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.exp_price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();
        }elseif($current_order['order_type']==2){

            $price_where = " AND `paper_size_id` =".$current_order['paper_size']." AND `paper_type_id`=".$current_order['paper_type']." AND `color`='".$current_order['color']."' AND `binding`='".$current_order['binding']."'";


            $where = "ps.status='1' AND vp.`price_status`='1' AND vp.exp_price >0 ";
            if(!empty($printery_id)){ $where .= " AND `ps`.`id`=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND ps.id IN(".$ids.")"; }

            $query = $this->db->select("DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,u.id as user_id,u.firstname,u.lastname,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,vp.exp_price as vendor_charge,vp.price_comb_id")
            ->from('printery_shop as ps')
            ->join('users as u',"u.id=ps.user_id AND u.role='2' AND u.delete_status='0' AND `u`.`status`='0'",'LEFT')
            ->join('vendor_prices as vp',"ps.id = vp.vendor_id AND vp.status='0'",'left')
            ->join('price_combination as pc',"pc.id = vp.price_comb_id AND pc.status='0' AND pc.service_id=".$current_order['order_type'].$price_where)
            ->where($where)
            ->order_by("vp.price","ASC")    
            ->get();

        }elseif($current_order['order_type']==4){

            $where = "npr.status = '0' AND npr.exp_price_status='1' AND npr.exp_price >0";
            
            $page_range = $current_order['note_page_range'];            

            $note_total_pages = $current_order['note_total_pages'];
            $where .= " AND npr.`copy_from` <=".$note_total_pages." AND `copy_to` >=".$note_total_pages;


            if(!empty($printery_id)){ $where .= " AND npr.printery_shop_id=".$printery_id; }
            elseif(!empty($ids)){ $where .= " AND npr.printery_shop_id IN(".$ids.")"; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,npr.printery_shop_id,npr.exp_price as vendor_charge,npr.copy_from,npr.copy_to,npr.price,npr.id as npr_id')
            ->from('notes_price_range as npr')
            ->join('printery_shop as ps','ps.id = npr.printery_shop_id')
            ->where($where)
            ->get();


        }elseif($current_order['order_type']==3){
            
            $where = "tp.order_id=".$oid;
            
            if(!empty($printery_id)){ $where .= " AND tp.printery_shop_id=".$printery_id; }

            $query = $this->db->select('DISTINCT (`ps`.`id`) as branch_id,`ps`.`id`,ps.latitude,ps.longitude,ps.shop_name,ps.po_box,ps.building,ps.start_time,ps.end_time,ps.logo,ps.status as shop_status,tp.printery_shop_id,tp.price as vendor_charge,tp.id as tp_id')
            ->from('trans_proposals as tp')
            ->join('printery_shop as ps','ps.id = tp.printery_shop_id')
            ->where($where)
            ->get();
          
        }     
         //->result_array();
//echo $this->db->last_query();
//         echo $query; exit;
        $data[] = $query->row_array();
        //if(!empty($query->row_array())){
        //$data = array_map("myfunction",$data);
            
        //}
        
    //echo "<pre>"; print_r($data);  exit;
        //return $data[0];
            //$data =  $data[0];
      
        }
        
        if(empty($data)){
            echo json_encode(array('success'=>true,"data"=>array(),"msg"=>"Printeries not available"));
            
        }else{
            echo json_encode(array('success'=>true,"data"=>$data));
            
        }
        // $query->result_array();


        
//   echo "<pre>"; print_r($current_order);
//  echo $this->db->last_query(); 
//  echo "<pre>"; print_r($data);
//           exit;
       
    }
    

    public function order_detail(){
        $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $oid = $request->order_id ;
         $data = $this->generalmodel->order_detail($oid);
         echo json_encode(array('success'=>true,"data"=>$data));
    }

    public function add_address(){
        $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
        //echo "<pre>"; print_r($request); 
        $_POST = (array)$request ;
        
        //echo "<pre>"; print_r($_POST); exit;
        $user_id = $request->user_id;

        $customer_address['user_id'] = $request->user_id;
        $customer_address['address'] = $request->address;
        $customer_address['latitude'] = $request->latitude;
        $customer_address['longitude'] = $request->longitude;
        $customer_address['address_type'] = $request->address_type;
        $customer_address['block'] = $request->block;
        $customer_address['street'] = $request->street;	
        $customer_address['building'] = $request->building;
        $customer_address['house'] = $request->house;
        $customer_address['mobile'] = $request->mobile;   
        $customer_address['apartment_no'] = $request->apartment_no;
        $customer_address['floor'] = $request->floor;    
        $customer_address['office'] = $request->office;
        $customer_address['via'] = "app";
        
        if(!empty($request->label)){
		    $customer_address['label'] = $request->label;
		}        
        if(!empty($request->avenue)){
		    $customer_address['avenue'] = $request->avenue;
		}         
        if(!empty($request->additional_directions)){
		    $customer_address['additional_directions'] = $request->additional_directions;
		}
        if(!empty($request->landline)){
            $customer_address['landline'] = $request->landline;
        }

        $this->form_validation->set_rules('user_id', 'User Id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address_type', 'Address Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'Area', 'required|trim|xss_clean');
        $this->form_validation->set_rules('block', 'block', 'required|trim|xss_clean');
        $this->form_validation->set_rules('street', 'street', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobile', 'mobile', 'required|trim|xss_clean');

		
		if($this->input->post('address_type')=='home'){
            $this->form_validation->set_rules('house', 'House', 'required|trim|xss_clean');
		}elseif($this->input->post('address_type')=='apartment'){
		    $this->form_validation->set_rules('building', 'Building', 'required|trim|xss_clean');
		    $this->form_validation->set_rules('floor', 'Floor', 'required|trim|xss_clean');
		    $this->form_validation->set_rules('apartment_no', 'Apartment Number', 'required|trim|xss_clean');
		}elseif($this->input->post('address_type')=='office'){
            $this->form_validation->set_rules('building', 'Building', 'required|trim|xss_clean');
            $this->form_validation->set_rules('floor', 'Floor', 'required|trim|xss_clean');		    
            $this->form_validation->set_rules('office', 'Office', 'required|trim|xss_clean');
		}           
		
		
		if($this->form_validation->run()==FALSE){
            $return= array('success'=>false,'msg'=>validation_errors());  
            echo json_encode($return); exit;
        }
                
            
        $data = $this->generalmodel->add('customer_address',$customer_address);            
        if(!empty($data)){
           $return =  array('success'=>true,"data"=>"added successfully");
        }else{
           $return =  array('success'=>false,"data"=>"internal error");
        }
        echo json_encode($return);        
    }
    
    public function note_detail(){
        $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    
	    $id = $request->id;
        $data = $this->generalmodel->note_detail($id);
        
        if(!empty($data)){
            $text = strip_tags($data['description']);
            $text =  str_replace("\r"," ",$text);
            $text =  str_replace("\n"," ",$text);
            $text =  preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);            
            $data['description']  = trim($text);
            $data['image']  =  base_url('uploaded_notes/images/'.$data['image']);
            
           $return =  array('success'=>true,"data"=>$data);
        }else{
           $return =  array('success'=>false,"data"=>"internal error");
        }
        echo json_encode($return);       
    }

	public function product_list(){
        $data = $this->generalmodel->getparticularData('*','products',array('status'=>'0','qty >='=>1),'result_array',"","",'price');
        if(!empty($data)){
            foreach($data as $key=>$val){
                $data[$key]['image'] = base_url('products/').$val['image'];
            }
             echo json_encode(array('success'=>true,'data'=>$data,'msg'=>""));
        }else{
             echo json_encode(array('success'=>false,'data'=>array(),'msg'=>"Products not available"));
        }
	}
	
	public function add_product(){
	    $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $_POST = (array)$request;
	    $error =  $pro =$stockData = array();

        $this->form_validation->set_rules('order_id', 'Order Id', 'required|trim|xss_clean');		    
        $this->form_validation->set_rules('prod_id', 'Products', 'required|trim|xss_clean');
        $this->form_validation->set_rules('qty', 'Quantity', 'required|trim|xss_clean');
	    if($this->form_validation->run()==FALSE){
            $return= array('success'=>false,'msg'=>validation_errors());  
            echo json_encode($return); exit;
        }
        
        $oid = $_POST['order_id'];
        $products = explode(",",$_POST['prod_id']);
        $quantity = explode(",",$_POST['qty']);

        if(!empty($products)){
            $i=0;
            foreach($products as $key=>$val){
                $postqty = $actualqty = $quantity[$key];
                 
                $prodQtyCheck = $this->db->select("`pro`.id,`pro`.price,`pro`.name,`pro`.qty,(pro.qty+op.prod_qty) as available_qty")
                ->from('products as pro')
                ->join('order_products as op',"op.prod_id=pro.id AND `op`.`status`='0' AND `op`.`order_id`=$oid",'left')
                ->where("`pro`.`id`=".$val." AND `pro`.`status`='0'")
                ->get()->row_array();
                
                if(!empty($prodQtyCheck) && ($quantity[$key]<= $prodQtyCheck['qty'] || $quantity[$key]<= $prodQtyCheck['available_qty'])){
                    
                    if($quantity[$key]>0){
                        $pro[$i]['order_id'] = $oid;
                        $pro[$i]['prod_id'] = $val;
                        $pro[$i]['prod_price'] = $prodQtyCheck['price'];
                        $pro[$i]['prod_qty'] = $quantity[$key];
                        $pro[$i]['created_at'] = date('Y-m-d h:i:s');
                    }
                    $stockData[$i]['id'] = $val;
                    $stockData[$i]['qty'] = $quantity[$key];
                    $i++;
                }else{
                    if(!empty($prodQtyCheck['available_qty'])){
                        $error[] = "Available Quantity of ".$prodQtyCheck['name']." is ".$prodQtyCheck['available_qty'];
                    }else{
                        $error[] = "Available Quantity of ".$prodQtyCheck['name']." is ".$prodQtyCheck['qty'];
                    }
                }
            } 
            if(empty($error)){
                $query = $this->generalmodel->add_order_products($oid,$pro,$stockData);
                if($query){
                    $return = array('success'=>true,'msg'=>"added");
                }else{
                    $return = array('success'=>false,'msg'=>"internal error");
                }
            }
        }else{
            $return = array('success'=>false,'msg'=>"Please select products");
        } 
        
        if(!empty($error)){ 
            $err_str = implode($error,"<br>"); 
            $return = array('success'=>false,'msg'=>$err_str);
        }
        echo json_encode($return);
	}
	
    public function my_active_orders(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $customer_id = $request->user_id;
	    
	    if(!empty($customer_id)){
            $data = $this->generalmodel->customer_current_order($customer_id);
            //echo "<pre>"; print_r($data); exit;
            if(!empty($data)){
                $return = array('success'=>true,"data"=>$data);
            }else{
                $return = array('success'=>false,"data"=>array());
            } 
	    }else{
	        $return = array('success'=>false,"data"=>array(),"msg"=>"Please send customer id");
	    }
        echo json_encode($return);
    }
    
    public function translation_proposals(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $oid = $request->order_id;

	    if(!empty($oid)){
	        $data = $this->generalmodel->trans_proposal($oid);
            if(!empty($data)){
                $return = array('success'=>true,"data"=>$data);
            }else{
                $return = array('success'=>false,"data"=>array(),"msg"=>"Proposals not received");
            } 
	    }else{
	        $return = array('success'=>false,"data"=>array(),"msg"=>"Please send order id");
	    }
        echo json_encode($return);
    }
    
    public function accept_proposal(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $oid = $request->order_id;
        $printery = $request->printery;

        $q = $this->generalmodel->accept_trans_proposal($oid,$printery);
        if(!empty($oid)){
            update_order_steps($oid,site_url('payment'));
            $return = array('success'=>true,'msg'=>"Proposal accepted");
        }else{
            $return = array('success'=>false,'msg'=>'Please enter order Id');
        }
        echo json_encode($return);
    }
    
    public function apply_coupon(){
        $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $user_coupon = $request->coupon_code;
	    $oid = $request->order_id;
	    
	    
        if(empty($user_coupon)){       
            $return= array('success'=>false,'msg'=>"Please enter coupon code"); 
        }elseif(empty($oid)){       
            $return= array('success'=>false,'msg'=>"Please enter order id"); 
        }else{
            
            $orderData = $this->generalmodel->getparticularData('amount,admin_charge,product_total,delivery_charge,order_total','orders',array("id"=>$oid),'row_array');
            if(empty($orderData['amount'])){
                $return = array('success'=>false,'msg'=>'Printery is not selected yet');
                echo json_encode($return);
                exit;
            }else{
                $order_amount = $orderData['amount'];
            }
            
            $where = array('code'=>$user_coupon,'status'=>'0');
            $coupon_detail = $this->generalmodel->getparticularData('*','coupon',$where,'row_array');
            //echo "<pre>"; print_r($get_coupon_detail); exit;

            if(!empty($coupon_detail)){
                $today = date('Y-m-d');
                if($coupon_detail['users'] == $coupon_detail['used_by'] ){
                    $return= array('success'=>false,'msg'=>'Sorry maximum users limit has been reached!');
                }elseif(strtotime($coupon_detail['start_date']) <=strtotime($today) && strtotime($coupon_detail['end_date']) >=strtotime($today)){
                    
                    if($coupon_detail['type']=='0'){
                        $coupon_amt = $coupon_detail['amount'];
                        $return['coupon_data']= array('type'=>"fixed",'amt'=>$coupon_detail['amount']);
                    }elseif($coupon_detail['type']=='1'){
                        $coupon_amt = (floatval($orderData['amount'])*floatval($coupon_detail['amount']))/100;
                        $return['coupon_data']= array('type'=>"percentage",'amt'=>$coupon_detail['amount']);
                    }
                    
                    $new_order_total = round((floatval($orderData['order_total']) - floatval($coupon_amt)),2);
                    $return['success']= true;
                    $return['vendor_charge'] = $orderData['amount'];
                    $return['service_fee'] = $orderData['admin_charge'];
                    $return['delivery_fee'] = $orderData['delivery_charge'];                    
                    $return['msg']= 'coupon can apply';
                    $return['applied_coupon_amt'] = $coupon_amt;
                    $return['order_total'] = $new_order_total;                       
                    //$return= array('success'=>true,'msg'=>'coupon can apply','type'=>$type,'amt'=>$coupon_detail['amount']);
                }else{
                    $return= array('success'=>false,'msg'=>'Coupon Code Expired');
                }
            }else{
                $return= array('success'=>false,'msg'=>'Invalid Coupon Code');     
            }
        }
        echo json_encode($return);
    }
	
	public function edit_address(){
        $this->check_header();
        $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
        //echo "<pre>"; print_r($request); 
        $_POST = (array)$request ;
        
        //echo "<pre>"; print_r($_POST); exit;
        $user_id = $request->user_id;
        $address_id = $request->address_id;

        //$customer_address['user_id'] = $request->user_id;
        $customer_address['address'] = $request->address;
        $customer_address['latitude'] = $request->latitude;
        $customer_address['longitude'] = $request->longitude;
        $customer_address['address_type'] = $request->address_type;
        $customer_address['block'] = $request->block;
        $customer_address['street'] = $request->street;	
        $customer_address['building'] = $request->building;
        $customer_address['house'] = $request->house;
        $customer_address['mobile'] = $request->mobile;   
        $customer_address['apartment_no'] = $request->apartment_no;
        $customer_address['floor'] = $request->floor;    
        $customer_address['office'] = $request->office;
        $customer_address['via'] = "app";
        
        if(!empty($request->label)){
		    $customer_address['label'] = $request->label;
		}        
        if(!empty($request->avenue)){
		    $customer_address['avenue'] = $request->avenue;
		}         
        if(!empty($request->additional_directions)){
		    $customer_address['additional_directions'] = $request->additional_directions;
		}
        if(!empty($request->landline)){
            $customer_address['landline'] = $request->landline;
        }

        $this->form_validation->set_rules('user_id', 'User Id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address_id', 'Address Id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address_type', 'Address Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required|trim|xss_clean');
        $this->form_validation->set_rules('address', 'Area', 'required|trim|xss_clean');
        $this->form_validation->set_rules('block', 'block', 'required|trim|xss_clean');
        $this->form_validation->set_rules('street', 'street', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mobile', 'mobile', 'required|trim|xss_clean');

		
		if($this->input->post('address_type')=='home'){
            $this->form_validation->set_rules('house', 'House', 'required|trim|xss_clean');
		}elseif($this->input->post('address_type')=='apartment'){
		    $this->form_validation->set_rules('building', 'Building', 'required|trim|xss_clean');
		    $this->form_validation->set_rules('floor', 'Floor', 'required|trim|xss_clean');
		    $this->form_validation->set_rules('apartment_no', 'Apartment Number', 'required|trim|xss_clean');
		}elseif($this->input->post('address_type')=='office'){
            $this->form_validation->set_rules('building', 'Building', 'required|trim|xss_clean');
            $this->form_validation->set_rules('floor', 'Floor', 'required|trim|xss_clean');		    
            $this->form_validation->set_rules('office', 'Office', 'required|trim|xss_clean');
		}           
		
		
		if($this->form_validation->run()==FALSE){
            $return= array('success'=>false,'msg'=>validation_errors());  
            echo json_encode($return); exit;
        }
                
            
        $data = $this->generalmodel->updaterecord('customer_address',$customer_address,array('id'=>$address_id,'user_id'=>$user_id));            
        if(!empty($data)){
           $return =  array('success'=>true,"data"=>"updated successfully");
        }else{
           $return =  array('success'=>false,"data"=>"internal error");
        }
        echo json_encode($return);        
    	    
	    
	    
	}
	
	public function test_with_header(){
	    $this->check_header();
	    $request = json_decode(rtrim(file_get_contents('php://input'), "\0"));
	    $email = $request->email ;
	    echo json_encode(array('success'=>false,'msg'=>"abc"));
	}	
	private function check_header(){
        $header = $this->input->request_headers();  
        // if($header['Content-Type'] !='application/json'){
        //     echo json_encode($return  = array('success'=>false,'msg'=>"header not sent")); exit;
        // }
	}
}