<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorize extends MY_Controller {

    public function __construct(){
        
        parent::__construct();

        $this->load->model('vendormodel');
    }
    
	public function index()
	{
		$this->load->view('admin/admin_login');
	}
	
	public function admin_login(){
        if($this->session->userdata('admindata') !=''){
        	redirect('dashboard');
        }
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			if($this->form_validation->run('login_validation')){
				$email = $this->input->post('email');
				$user_pass = md5($this->input->post('pass'));
			//	$user_pass = encrypt_pass(trim($this->input->post('pass'))); 

				$data = $this->generalmodel->getparticularData('id,role,firstname,lastname,email, CONCAT_WS(" ",firstname,lastname) AS username','users',"email='$email' AND password='$user_pass' AND role='1' AND status='0' AND delete_status='0' ",'row_array');


				if(!empty($data)){
					$sessData['user_id']=$data['id'];
					$sessData['email']=$data['email'];
					$sessData['role']=$data['role'];
					$sessData['username']=$data['username'];
					$sessData['firstname']=$data['firstname'];
					$sessData['lastname']=$data['lastname'];
					$this->session->set_userdata('admindata',$sessData);
					
					$return = array('success'=>true,'redirect'=>'dashboard','msg'=>'You are logged in successfully!');
					
				}else{
    				$return = array('success'=>false,'msg'=>'Email Address or Password is not correct!');
    			}
			}else{
				$return = array('success'=>false,'msg'=>validation_errors());
			}
			
			echo json_encode($return);
		}else{
		    $this->load->view('admin/admin_login');
		}
	}

	public function vendor_login(){
        if($this->session->userdata('vendordata') !=''){ redirect('vendor'); }

		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			if($this->form_validation->run('login_validation')){
				$email = $this->input->post('email');
				$user_pass = md5($this->input->post('pass'));
				$remember_me = $this->input->post('remember_me');

				$data = $this->generalmodel->getparticularData('id,role,firstname,lastname,email, CONCAT_WS(" ",firstname,lastname) AS username','users',"email='$email' AND password='$user_pass' AND role='2' AND status='0' AND delete_status='0' ",'row_array');


				if(!empty($data)){

					$branch_ids = $this->generalmodel->getparticularData('GROUP_CONCAT(`id`) as branch_ids','printery_shop',"user_id='".$data['id']."' AND delete_status='0' ",'row_array');

					if(!empty($branch_ids)){
						$sessData['branch_ids']= $branch_ids['branch_ids'];
					}
						//print_r($branch_ids); exit;

					$sessData['user_id']=$data['id'];
					$sessData['email']=$data['email'];
					$sessData['role']=$data['role'];
					$sessData['username']=$data['username'];
					$sessData['firstname']=$data['firstname'];
					$sessData['lastname']=$data['lastname'];
					$this->session->set_userdata('vendordata',$sessData);

				if(!empty($remember_me)){
					$this->load->helper('cookie');
					$cookie = array(
					    'name'   => 'remember_me_token',
					    'value'  => 'Random string',
					    'expire' => '1209600',  // Two weeks
					    //'domain' => '.your_domain.com',
					    'domain' => site_url(),
					    'path'   => '/'
					);

					set_cookie($cookie);
				}
					


					$return = array('success'=>true,'redirect'=>'vendor','msg'=>'You are logged in successfully!');
					
				}else{
    				$return = array('success'=>false,'msg'=>'Email Address or Password is not correct!');
    			}
			}else{
				$return = array('success'=>false,'msg'=>validation_errors());
			}
			
			echo json_encode($return);
		}else{
		    redirect('/');
		}
	}


	public function registeration(){
        
        if($this->session->userdata('vendordata') !=''){ redirect('vendor');  } 

	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        if($this->form_validation->run('vendor_registeration')){
	            
	            $email = $this->input->post('email');
	            $password = $this->input->post('password');
                $cpassword = $this->input->post('c_password');
                $address = $this->input->post('address');
                $user_lat = $this->input->post('vendor_lat');
                $user_lng = $this->input->post('vendor_lng');

                if($password !== $cpassword){
                    $return = array('success'=>false,'msg'=>"Password don't match!");
                    echo json_encode($return); exit;
                }

                $check_exist = $this->generalmodel->getparticularData('id','users',array('email'=>$email,'delete_status'=>'0'),'row_array');

                if(!empty($check_exist)){
                    $return = array('success'=>false,'msg'=>"Email address is already registered with us!");
                    echo json_encode($return); exit;
                }

		        $userdata['role'] = '2';
		        $userdata['firstname'] = $this->input->post('firstname');
		        $userdata['email'] = $this->input->post('email');
		        $userdata['mobile'] = $this->input->post('contact_no');
		        $userdata['password'] = md5($password);
		        $userdata['created_at'] = date('Y-m-d h:i:s');
		        $userdata['address'] = $address;		        
		        
		        
		        $printryData['shop_name'] = $this->input->post('shop_name');
		        $printryData['building'] = $address;
		        $printryData['latitude'] = $user_lat;
		        $printryData['longitude'] = $user_lng;
		        $printryData['registeration_date'] =date('Y-m-d h:i:s');
		        $printryData['status'] = '0';
	        
		        $userid = $this->vendormodel->add_vendor($userdata,$printryData);

		        // echo $this->db->last_query();
		        // echo $userid; exit;

		        if(!empty($userid)){

                    $mainbranch_id = $this->generalmodel->getparticularData('id','printery_shop',array('user_id'=>$userid,'delete_status'=>'0'),'row_array');
                    
                    
					$sessData['branch_ids']=implode(',',$mainbranch_id);
					$sessData['user_id']=$userid;
					$sessData['email']=$userdata['email'];
					$sessData['role']='2';
					$sessData['username']=$userdata['firstname'];
					$sessData['firstname']=$userdata['firstname'];
					$sessData['lastname']='';
					$sessData['profile_status']='incomplete';
					
					$this->session->set_userdata('vendordata',$sessData);

		        	$return = array('success'=>true,'msg'=>'Registered successfully','redirect'=>'vendor');
		        }else{
		        	$return = array('success'=>false,'msg'=>'Internal Error');
		        }
		    }else{
            	$return = array('success'=>false,'msg'=>validation_errors());
            }
	        echo json_encode($return);
	    }else{
	    	redirect('/');
    	    //$this->load->view('vendor/registeration_form');
	    }
	}
}