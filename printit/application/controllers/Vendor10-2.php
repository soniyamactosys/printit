<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MY_Controller {

    public function __construct(){
        
        parent::__construct();
        if(!empty($this->session->userdata['vendordata'])){
            $this->vendordata = $this->session->userdata['vendordata'];
        }else{
            //redirect('vendorlogin');
            redirect('');
        }
        $this->load->model('vendormodel');
    }
    
	public function index()
	{
        $this->data['title'] = 'Dashboard';
        $this->data['menu'] = 'dashboard';		
		$this->vendorview('vendor/dashboard',$this->data);
	}

	public function orders($type=""){
		if(check_branch_detail()){
			$this->data['title'] = 'Orders';
		    $this->data['menu'] = 'orders';

		    $where = "orders.status='1' AND orders.vendor IN(".$this->vendordata['branch_ids'].")";
		    if(!empty($type)){
		    	$where .= " AND orders.payment_status = '1'";
		    	//$where = array('orders.vendor'=>$this->vendordata['user_id'],'orders.status'=>'1','orders.payment_status'=>'1');
		    }else{
				//$where = array('orders.vendor'=>$this->vendordata['user_id'],'orders.status'=>'1','orders.payment_status'=>'1');
		    }
		   // $this->data['orders'] = $this->generalmodel->getparticularData("*","orders",$where,"result_array");

		    $tables[0]['table'] = 'users as u';
		    $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

		    $tables[1]['table'] = 'users as uv';
		    $tables[1]['on'] = 'uv.id = orders.vendor AND uv.role="2" AND uv.delete_status="0"';

		    $this->data['orders'] = $this->generalmodel->getfrommultipletables("*,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,CONCAT_WS(' ',uv.firstname,uv.lastname) AS vendor_name,orders.created_at as order_date,orders.id as order_number ",'orders',$tables,$where,"","orders.id","","","result_array");

		    // echo $this->db->last_query();
		    // echo "<pre>"; print_r($this->data['orders']);
		    // exit;

		    $this->vendorview('vendor/orders',$this->data);
		}else{
			redirect('/vendor');
		}
	}

	public function update_order_status(){
		if(!empty($this->vendordata) && !empty($this->input->post()) && $this->input->is_ajax_request()){
			$oid = $this->input->post('oid');
			$status = $this->input->post('status');
			$updateQuery = $this->generalmodel->updaterecord("orders",array('progress_status'=>$status),array('id'=>$oid));	

			if($updateQuery){
				$return = array('success'=>true,'msg'=>'status updated');
			}else{
				$return = array('success'=>false,'msg'=>'query failed');
			}		
		}else{
			$return = array('success'=>false,'msg'=>'internal error');
		}
		echo json_encode($return);
	}

	public function branches(){
		$this->data['title'] = 'Branches';
	    $this->data['menu'] = 'branches';
	  	$parent_id = $this->vendordata['user_id'];

	    $this->data['branches'] = $this->generalmodel->getparticularData("*,status as shop_status","printery_shop",array('delete_status'=>'0','user_id'=>$parent_id,'branch !='=>'0'),"result_array");
	    $this->vendorview('vendor/branches',$this->data);

	}

	public function add_branch(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_branch')){
				
				$created_by = $this->vendordata['user_id'];
				$contact = $this->input->post('branch_contact');
				$branch_lat = $this->input->post('branch_lat');
				$branch_lng = $this->input->post('branch_lng');
		        $printryData['user_id'] = $created_by;
		        $printryData['registeration_date'] = date('Y-m-d h:i:s');
		        $printryData['created_by'] = $created_by;
				$printryData['contact'] = $contact;
		        $printryData['shop_name'] = $this->input->post('shop_name');
		        $printryData['po_box'] = $this->input->post('po_box');
		        $printryData['block'] = $this->input->post('block');
		        $printryData['building'] = $this->input->post('address');
		        $printryData['start_time'] = $this->input->post('start_time');
		        $printryData['end_time'] = $this->input->post('end_time');
		        $printryData['status'] = '3';
		        $printryData['branch'] = '1';

		        if(!empty($this->input->post('latitude'))){
		        	$printryData['latitude'] = $this->input->post('latitude');
		    	}
		    	if(!empty($this->input->post('longitude'))){
		        	$printryData['longitude'] = $this->input->post('longitude');
		    	}
				$services = '';
				if(!empty($this->input->post('services'))){ $services = implode(',',$this->input->post('services')) ; }		    	
		        $printryData['services'] = $services;

	           	$already =  $this->generalmodel->getparticularData("*","printery_shop",array("contact"=>$contact,'delete_status'=>'0'),"row_array");
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	
		            $branch_id = $this->generalmodel->add("printery_shop",$printryData);
		            $i=0;
		            $post_service = $this->input->post('services');
		            foreach($post_service as $val){
		            	$priceData[$i]['printery_shop_id'] = $branch_id;
		            	$priceData[$i]['service_id'] = $val;
		            	$i++;
		            }
		            $this->generalmodel->insert_batchdata("print_management",$priceData);

		            if(!empty($branch_id)){
	        	        $return= array('success'=>true,'msg'=>'Added successfully');
		            }else{
	        	        $return= array('success'=>true,'msg'=>'Something went wrong');
		            }
	        	}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
    	    $this->data['title'] = 'Add Branch';
    	    $this->data['menu'] = 'branch';
	        $this->data['services'] = $this->generalmodel->get_services('0');
    	    
			$this->vendorview('vendor/add_branch',$this->data);
	    }
	}

	public function edit_branch($id=""){
		$id = decoding($id);
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_branch')){
					
				//	echo "<pre>"; print_r($_POST); exit;

				$created_by = $this->vendordata['user_id'];
				$printery_id = $this->input->post('printery_id');
				$contact = $this->input->post('branch_contact');
				$branch_lat = $this->input->post('branch_lat');
				$branch_lng = $this->input->post('branch_lng');

		        $printryData['updated_by'] = $created_by;
		        $printryData['updated_at'] = date('Y-m-d h:i:s');
		        //$printryData['created_by'] = $created_by;
				$printryData['contact'] = $contact;
		        $printryData['shop_name'] = $this->input->post('shop_name');
		        $printryData['po_box'] = $this->input->post('po_box');
		        $printryData['block'] = $this->input->post('block');
		        $printryData['building'] = $this->input->post('address');
		        $printryData['start_time'] = $this->input->post('start_time');
		        $printryData['end_time'] = $this->input->post('end_time');
		        $printryData['status'] = '3';


		        if(!empty($this->input->post('latitude'))){
		        	$printryData['latitude'] = $this->input->post('latitude');
		    	}
		    	if(!empty($this->input->post('longitude'))){
		        	$printryData['longitude'] = $this->input->post('longitude');
		    	}
				$services = '';
				if(!empty($this->input->post('services'))){ $services = implode(',',$this->input->post('services')) ; }		    	
		        $printryData['services'] = $services;

	           	$already =  $this->generalmodel->getparticularData("*","printery_shop",array("contact"=>$contact,'delete_status'=>'0','id !='=>$printery_id),"row_array");

	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	
		            //$branch_id = $this->generalmodel->add("printery_shop",$printryData);
		            $branch_id = $this->generalmodel->updaterecord("printery_shop",$printryData,array('id'=>$printery_id,'delete_status'=>'0'));


		            if(!empty($branch_id)){
	        	        $return= array('success'=>true,'msg'=>'Updated successfully');
		            }else{
	        	        $return= array('success'=>true,'msg'=>'Something went wrong');
		            }
	        	}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }else{
    	    $this->data['title'] = 'Edit Branch';
    	    $this->data['menu'] = 'branch';
	        $this->data['services'] = $this->generalmodel->get_services('0');
	        $this->data['branch_data'] = $this->generalmodel->branch_data($id);
    	    //echo "<pre>"; print_r($this->data['branch_data']); exit;
			$this->vendorview('vendor/edit_branch',$this->data);
	    }		
	}

	public function delete_branch($id){
		$id= decoding($id);
		$this->generalmodel->updaterecord('printery_shop',array('delete_status'=>'1'),array('id'=>$id));

	}

	public function myprofile(){

		if(check_branch_detail()){
	        $this->data['title'] = 'My profile';
	        $this->data['menu'] = 'myprofile';		
	        $this->data['services'] = $this->generalmodel->get_services('active');
	        $this->data['myprofile'] = $this->vendormodel->myprofile();
	        // echo $this->db->last_query(); 
	        // echo "<pre>"; print_r($this->data); exit;
	        $this->vendorview('vendor/my_profile',$this->data);	
        }else{
        	redirect('/vendor');
        }	
	}


	public function edit_myprofile(){
	    $this->data['title'] = 'my profile';
        $this->data['menu'] = 'myprofile';
        $this->data['myprofile'] = $this->vendormodel->myprofile();
        //echo $this->db->last_query(); die;
        $this->data['services'] = $this->generalmodel->get_services('0');     
         //echo "<pre>"; print_r($this->data['myprofile']);   		 exit;
		$this->vendorview('vendor/profile_form',$this->data);			
	}

	public function new_branch_form($start,$total){
		$data['start'] = $start;
		$data['total'] = $total;
		$data['services'] = $this->generalmodel->get_services('0'); 		
		echo  $this->load->view('vendor/new_branch_form',$data,true);
	}


	public function price_management(){
		$this->data['title'] = 'Price Management';
	    $this->data['menu'] = 'price_management';
	  	$vendor_id = $this->vendordata['user_id'];

	    $this->data['branches'] = $this->generalmodel->getparticularData("*","printery_shop",array('user_id'=>$vendor_id,'delete_status'=>'0'),"result_array");

	    $this->data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
	    $this->data['paper_type'] = $this->generalmodel->getparticularData("*","paper_type",array('status'=>'0'),"result_array");
	    $this->data['paper_size'] = $this->generalmodel->getparticularData("*","paper_size",array('status'=>'0'),"result_array");

	    $this->data['banner_width'] = $this->generalmodel->getparticularData("*","banner_width",array('status'=>'0'),"result_array");
	    $this->data['management'] = $this->generalmodel->price_management_records($vendor_id);

	    $this->vendorview('vendor/price_management',$this->data);		
	}

	public function get_branch_services($branchid){
	    $services = $this->generalmodel->getparticularData("services","printery_shop",array('id'=>$branchid,'delete_status'=>'0'),"row_array");		
		$all_services = array();

	    if(!empty($services['services'])){ 
	    	$all_services = $this->generalmodel->getparticularData("id,name","services","`id` IN(".$services['services'].")","result_array");

	    	if(!empty($all_services)){ 
				$html = '<option value="">Please select</option>';
	    		foreach($all_services as $service){
	    			$html .= '<option value="'.$service['id'].'">'.$service['name'].'</option>';
	    		} 
				$return = array('success'=>true,'msg'=>$html);
	    	}else{
	    		$return = array('success'=>false,'msg'=>'No Services are added by admin');
	    	}
	    }else{
	    	$return = array('success'=>false,'msg'=>'Services are not selected for branch yet');
	    }
	    echo json_encode($return);
	}

	public function add_price_management(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
	        if($this->form_validation->run('add_price_mangement')){
	            
				$where = array('status !='=>'2');
	            $data['printery_shop_id'] = $where['printery_shop_id'] = $this->input->post('branch_id');
	            $data['service_id'] = $where['service'] = $this->input->post('service');

	            if($data['service_id']=='1'){
	            	$this->form_validation->set_rules('print_type', 'Print Type', 'required|trim|xss_clean');
	            	if($this->form_validation->run() == FALSE){
	            		$return= array('success'=>false,'msg'=>validation_errors());
	            		echo json_encode($return); exit;
	            	}
	            }
				
	            $data['paper_type_id'] = $where['paper_type_id'] = $this->input->post('paper_type');
	            $data['paper_size_id'] = $where['paper_size_id'] = $this->input->post('paper_size');
	            $data['print_type'] =  $this->input->post('print_type');
	            $data['printing_sides'] =  $this->input->post('sides');
	            $data['width'] =  $this->input->post('width');
	            $data['color'] =  $this->input->post('color');
	            $data['binding'] =  $this->input->post('binding');
	            $data['average_price'] = $this->input->post('avg_price');
	            $data['created_at'] = date('Y-m-d h:i:s');
	            

	            if($data['service_id']=='1'){
	            	$where['print_type'] =$data['print_type'];
	            	if($data['print_type']=='2'){
	            		$where['printing_sides'] =$data['printing_sides'];
	            	}
	            }else{
					$where['width'] =$data['width'];
					$where['color'] =$data['color'];
					$where['binding'] =$data['binding'];
					$where['printing_sides'] =$data['printing_sides'];
	            }

	            $already =  $this->generalmodel->getparticularData("*","print_management",$where,"row_array");

	            // echo "<pre>"; print_r($_POST);
	            // print_r($data);
	            // echo $this->db->last_query(); 
	            if(!empty($already)){
	                $return= array('success'=>false,'msg'=>'Already Exist!');
	            }else{
	            	$addquery = $this->generalmodel->add('print_management', $data);

					// echo $this->db->last_query(); 
					// print_r($addquery);
					// exit;
		            if($addquery){
	        	        $return= array('success'=>true,'msg'=>'Added successfully');
		            }else{
	        	        $return= array('success'=>true,'msg'=>'Something went wrong');
		            }
	        	}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        echo json_encode($return);
	    }		
	}

	public function add_price_range($branchid,$pm_id){
		$branchid = decoding($branchid);
		$pm_id = decoding($pm_id);
		$this->data['title'] = 'Add Extra copy Price';
	    $this->data['menu'] = 'price_management';
	  	$vendor_id = $this->vendordata['user_id'];
	    $this->data['detail'] = $this->generalmodel->price_management_records($vendor_id,$pm_id,"row");

		// echo $this->db->last_query();
		// 	    echo "<pre>"; print_r($this->data['detail']); exit;

	    $this->vendorview('vendor/add_price_range',$this->data);	
	}

	public function add_price(){
		$this->data['title'] = 'Price Management';
	    $this->data['menu'] = 'add_price';
	    if(!empty($this->vendordata)){
			if(!empty($this->input->post()) && $this->input->is_ajax_request()){

				$service =$this->input->post('services');
					//echo "<pre>"; print_r($_POST); exit;
				if($service==4){
					$npr_id =$this->input->post('npr_id');
					$prices =$this->input->post('price');
					$branch = $this->input->post('branch_id');
					$copy_from = $this->input->post('copy_from');
					$copy_to = $this->input->post('copy_to');
					$i=0;
					foreach($prices as $key=>$price){

						if(!empty($npr_id) && array_key_exists($key,$npr_id)){
							$where['id'] = $npr_id[$key];
						}
						$where['printery_shop_id'] = $branch;
						
						$where['copy_from'] = $copy_from[$key];
						$where['copy_to'] = $copy_to[$key];

						$check_already = $this->generalmodel->getparticularData("id","notes_price_range",$where,"result_array");
						 // echo $this->db->last_query();
						 // echo "<br>";

						$currentdate = date('Y-m-d h:i:s');
						if(!empty($check_already)){
							$data['price'] =$price;				
							$data['updated_at'] = $currentdate;

							$up_query = $this->generalmodel->updaterecord("notes_price_range",$data,$where);
							if(empty($up_query)){
								echo json_encode(array('success'=>false,'msg'=>'Update internal error')); exit;
							}
							// echo $this->db->last_query();
						 // echo "<br>";

						}else{
							$in_data[$i]['printery_shop_id'] = $branch;
							$in_data[$i]['copy_from'] = $copy_from[$key];
							$in_data[$i]['copy_to'] = $copy_to[$key];
							$in_data[$i]['status'] = '0';
							$in_data[$i]['created_at'] = $currentdate;
							$in_data[$i]['updated_at']	= $currentdate;
							$in_data[$i]['price']	= $price;
							$i++;
						}
					}
						//echo "<pre>"; print_r($in_data); exit;
					if(!empty($in_data)){
						$in_query = $this->generalmodel->insert_batchdata("notes_price_range",$in_data);
						//  echo "<pre>"; print_r($in_query);
						// echo $this->db->last_query(); exit;
						if(empty($in_query)){
							echo json_encode(array('success'=>false,'msg'=>'Internal error')); exit;
						}
					}
				}else{

					$combi_id =$this->input->post('combi_id');
					$prices =$this->input->post('price');
					$branch = $this->input->post('branch_id');
					$i=0;
					foreach($prices as $key=>$price){
						
						$where['price_comb_id'] = $combi_id[$key];
						$where['vendor_id'] = $branch;

						$check_already = $this->generalmodel->getparticularData("id","vendor_prices",$where,"result_array");
						 // echo $this->db->last_query();
						 // echo "<br>";

						$currentdate = date('Y-m-d h:i:s');
						if(!empty($check_already)){
							$data['price'] =$price;				
							$data['updated_at'] = $currentdate;

							$up_query = $this->generalmodel->updaterecord("vendor_prices",$data,$where);
							if(empty($up_query)){
								echo json_encode(array('success'=>false,'msg'=>'Update internal error')); exit;
							}
						}else{
							$in_data[$i]['price_comb_id'] = $combi_id[$key];
							$in_data[$i]['vendor_id'] = $branch;
							$in_data[$i]['price'] = $price;
							$in_data[$i]['status'] = '0';
							$in_data[$i]['added_at'] = $currentdate;
							$in_data[$i]['updated_at']	= $currentdate;
							$i++;
						}
					}
						//echo "<pre>"; print_r($in_data); exit;
					if(!empty($in_data)){
						$in_query = $this->generalmodel->insert_batchdata("vendor_prices",$in_data);
						 //echo "<pre>"; print_r($in_query); exit;
						// echo $this->db->last_query();
						if(empty($in_query)){
							echo json_encode(array('success'=>false,'msg'=>'Internal error')); exit;
						}
					}
				
				}

				echo json_encode(array('success'=>true,'msg'=>'Record Updated')); exit;
		    }else{

			  	$vendor_id = $this->vendordata['user_id'];
				
				$this->data['print_type'] = $this->generalmodel->getparticularData("*","print_type",array('status'=>'0'),"result_array");
			    $this->data['branches'] = $this->generalmodel->getparticularData("id,shop_name","printery_shop",array('user_id'=>$vendor_id,'delete_status'=>'0'),"result_array");
			    //echo "<pre>"; print_r($this->data['branches']); exit;

				$this->vendorview('vendor/add_price_form',$this->data);		
		    }
		}	
	}

	public function get_prices(){
		//echo "<pre>"; print_r($_POST); exit();
		if(!empty($this->vendordata) && !empty($this->input->post()) && $this->input->is_ajax_request()){
			
		$branch = $this->input->post('branch');
		$service = $this->input->post('service');	
		$print_type = empty($this->input->post('print_type'))?'0':$this->input->post('print_type');		
		
		if($service==1 || $service==2){

			$where = "pc.status = '0' AND pc.`service_id`= $service AND pc.`print_type` =$print_type ";
			$data['price_combination'] = $this->db->select('serv.name as service,size.name as size_name,print.name as print,paper.name as paper,vp.price_comb_id,vp.price,pc.id as combi_id,bsize.name as banner_size_name,pc.*')
			->from('price_combination as pc')
			->join('services as serv',"serv.id = pc.service_id",'left')
			->join('print_type as print',"print.id = pc.print_type",'left')
			->join('paper_type as paper',"paper.id = pc.paper_type_id",'left')
			->join('paper_size as size',"size.id = pc.paper_size_id",'left')
			->join('banner_size as bsize',"bsize.id = pc.banner_size",'left')
			->join('vendor_prices as vp',"vp.price_comb_id = pc.id AND vp.status='0' AND vp.vendor_id = ".$branch,'left')
			->where($where)
			->get()
			->result_array();
			// echo $this->db->last_query();
			// echo "<pre>"; print_r($data['price_combination']); 		
			// exit;

			$data['post']['service_name'] = $data['price_combination'][0]['service'];
			$data['post']['service'] = $data['price_combination'][0]['service_id'];
			$data['post']['printtypeName'] = $data['price_combination'][0]['print'];

			// echo $this->db->last_query();
			// echo "<pre>"; print_r($data); 		
			// exit;

			$html = $this->load->view('vendor/add_price_ajax_form',$data,true);			

		}elseif($service==4){
			

			$where = "npr.status = '0' AND npr.printery_shop_id = ".$branch;
			$data['notes_price'] = $this->db->select('npr.printery_shop_id,npr.price,npr.copy_from,npr.copy_to,npr.price,npr.id as npr_id')
			->from('notes_price_range as npr')
			->where($where)
			->get()
			->result_array();

			// echo $this->db->last_query();
			// echo "<pre>"; print_r($data['notes_price']); 		
			// exit;
			$html = $this->load->view('vendor/notes_price_ajax_form',$data,true);	
			// $data['post']['service_name'] = "Notes";
			// $data['post']['service'] = 4;
			// $data['post']['printtypeName'] = '';
		}else{
			$data['post']['service_name'] = "";
			$data['post']['service'] = "";
			$data['post']['printtypeName'] = "";

			$html = $this->load->view('vendor/add_price_ajax_form',$data,true);
		}

			$return = array('success'=>true,'msg'=>$html);
		}else{
			$return = array('success'=>false,'msg'=>'login first');
		}
		echo json_encode($return);
	}

	public function test(){
		$data = $this->generalmodel->getparticularData("*"," print_management","","result_array");

		$res3 = $this->search3($data,array('printery_shop_id'=>'18','print_type'=>'3')); 
		echo "<pre>RES2:::"; print_r($res3); 
		echo "<pre>"; print_r($data); 
		//echo "<pre>"; print_r($data);
		 exit;
	}


	function search3($array, $search_list) { 
    	// Create the result array 
	    $result = array(); 
	  
	    // Iterate over each array element 
	    foreach ($array as $key => $value) { 
	  
	        // Iterate over each search condition 
	        foreach ($search_list as $k => $v) { 
	      
	            // If the array element does not meet 
	            // the search condition then continue 
	            // to the next element 
	            if (!isset($value[$k]) || $value[$k] != $v) 
	            { 
	                // Skip two loops 
	                continue 2; 
	            } 
	        } 
	        // Append array element's key to the 
	        //result array 
	        $result[] = $value; 
	    } 
  
	    // Return result  
	    return $result; 
	} 	

	
	public function order_detail($oid){
		//if(!empty($this->session->userdata('admindata')) || !empty($this->session->userdata('vendordata'))){
			$id= decoding($oid);
			$where = array('orders.status'=>'1','orders.id'=>$id);

		    $tables[0]['table'] = 'users as u';
		    $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

		    $tables[1]['table'] = 'users as uv';
		    $tables[1]['on'] = 'uv.id = orders.vendor AND uv.role="2" AND uv.delete_status="0"';

		    $tables[2]['table'] = 'paper_type as ptype';
		    $tables[2]['on'] = 'orders.paper_type = ptype.id AND ptype.status!="2"';

		    $tables[3]['table'] = 'paper_size as psize';
		    $tables[3]['on'] = 'orders.paper_size = psize.id AND psize.status!="2"';

		    $this->data['detail'] = $this->generalmodel->getfrommultipletables("*,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,CONCAT_WS(' ',uv.firstname,uv.lastname) AS vendor_name,orders.created_at as order_date,orders.id as order_number,psize.name AS papersize,ptype.name as papertype ",'orders',$tables,$where,"","orders.id","","","row_array");

		    $this->data['title'] = 'order detail';
		    $this->data['menu'] = 'order_detail';
	        $this->vendorview('common/order_detail',$this->data);
    	//}
	}

	public function edit_company_detail(){
		if(!empty($this->vendordata) && !empty($this->input->post()) && $this->input->is_ajax_request()){

			$vendor_id = $this->vendordata['user_id'];
			$mainprintery_id = $this->input->post('mainprintery_id');
			$path = 'vendor_uploads/'.$vendor_id;
			if(!file_exists($path)) {
			    mkdir($path, 0777, true);
			}			
			//====upload company logo images====//
			if(!empty($_FILES['company_logo']['name'])){
				$logo = $this->uploadDoc('company_logo',$path,array('jpg','png','jpeg'));
				//echo "<pre>"; print_r($logo); exit;
				if(!empty($logo['error'])){
					$return = array('success'=>false,'msg'=>$logo['error']);
					echo json_encode($return); exit;				   
				}else{
				     $printryData['logo'] = $logo['file_name'];
				}
				
			}

			//====upload registeration proof====//
			if(!empty($_FILES['registery_file']['name'])){
				$reg_proof = $this->uploadDoc('registery_file',$path,array('jpg','png','jpeg','pdf'));
				if(!empty($reg_proof['error'])){
					$return = array('success'=>false,'msg'=>$reg_proof['error']);
					echo json_encode($return); exit;
				}else{
					$userdata['reg_proof'] = $reg_proof['file_name'];
				}
			}

			//====upload company multiple images====//
			if(!empty($_FILES['company_images']['name'][0])){
				$company_images = $this->multiple_upload('company_images',$path,array('jpg','png','jpeg'));
				if(!empty($company_images['error'])){
					$return = array('success'=>false,'msg'=>$company_images['error']);
					echo json_encode($return); exit;
				}else{
					$printryData['company_images'] = $company_images;
				}				
			}
			
			$services = '';
			if(!empty($this->input->post('c_services'))){ $services = implode(',',$this->input->post('c_services')) ; }
		        $userdata['firstname'] = $this->input->post('fname');
		        $userdata['lastname'] = $this->input->post('lname');
		        $userdata['total_branches'] = $this->input->post('total_branches');
		        $userdata['reg_number'] = $this->input->post('reg_number');
		        $userdata['mobile'] = $this->input->post('mobile');
		        $userdata['updated_at'] = date('Y-m-d h:i:s');


		        $printryData['total_workers'] = $this->input->post('worker_count');
		        $printryData['contact'] = $this->input->post('branch_contact');
		        $printryData['shop_name'] = $this->input->post('shop_name');
		        $printryData['po_box'] = $this->input->post('po_box');
		        $printryData['building'] = $this->input->post('address');
		        $printryData['start_time'] = $this->input->post('start_time');
		        $printryData['end_time'] = $this->input->post('end_time');
		        $printryData['status'] = '0';
	        if(!empty($this->input->post('latitude'))){
	        	$printryData['latitude'] = $this->input->post('latitude');
	    	}
	    	if(!empty($this->input->post('longitude'))){
	        	$printryData['longitude'] = $this->input->post('longitude');
	    	}
	        $printryData['services'] = $services;


			$incharge_person['printery_id'] = $mainprintery_id;
			$incharge_person['user_id'] = $vendor_id;
			$incharge_person['fname'] = $this->input->post('c_in_fname');
			$incharge_person['lname'] = $this->input->post('c_in_lname');
			$incharge_person['email'] = $this->input->post('c_in_email');
			$incharge_person['mobile'] = $this->input->post('c_in_mobile');
			$incharge_person['status'] = '0';
	
			$update = $this->vendormodel->update_vendor_profile($userdata,$printryData,$incharge_person,$mainprintery_id);

	        if(!empty($update)){
	        	$return = array('success'=>true,'msg'=>'Updated successfully');
	        }else{
	        	$return = array('success'=>false,'msg'=>'Internal Error');
	        }			
		}else{
			$return = array('success'=>false,'msg'=>'login first');
		}	
		echo json_encode($return);
	}
	
	public function  edit_company_branches(){
	    if(!empty($this->input->post()) && $this->input->is_ajax_request()){
	        
			$user_id = $this->input->post('user_id');
			//$current_branches = $this->input->post('current_branches');
			$current_branches = count($this->input->post('shop_name'));
			$contact = $this->input->post('mobile');
			$contact_str = "'".implode("','",$contact)."'";

			$serArr = $this->input->post('c_services');
			$po_box = $this->input->post('po_box');
			$shop_name = $this->input->post('shop_name');
			$building = $this->input->post('address');
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			$start_time = $this->input->post('start_time');
			$end_time = $this->input->post('end_time');
			$total_workers = $this->input->post('worker_count');
			$c_in_fname = $this->input->post('c_in_fname');
			$c_in_lname = $this->input->post('c_in_lname');
			$c_in_email = $this->input->post('c_in_email');
			// foreach(!empty()){

			// }
			$c_in_email_str = "'".implode("','",$c_in_email)."'";

			$c_in_mobile = $this->input->post('c_in_mobile');
			$c_in_mobile_str = "'".implode("','",$c_in_mobile)."'";

			$created_by = $user_id;
	        if($this->form_validation->run('add_branch')){

				$b_already =  $this->generalmodel->getparticularData("id","printery_shop"," `user_id` !=".$user_id." AND `contact` IN($contact_str) AND `delete_status`='0'","row_array");
				
				$incharg_already =  $this->generalmodel->getparticularData("id","incharge_person"," `user_id` !=".$user_id." AND (`mobile` IN($c_in_mobile_str) OR `email` IN($c_in_email_str)) AND `status`='0'","row_array");				
                 //echo $this->db->last_query(); die;
	            if(!empty($b_already)){
	                $return= array('success'=>false,'msg'=>'Branch with this contact number already exist!');
	                echo json_encode($return); exit;
	            }

	            if(!empty($incharg_already)){
	                $return= array('success'=>false,'msg'=>'Incharge person with this contact details already exist!');
	                echo json_encode($return); exit;
	            }

	        	for($i=0;$i<$current_branches;$i++){

	            	$printryData[$i]['user_id'] = $user_id;
	            	$printryData[$i]['branch'] = '1';
	            	if(!empty($contact)){
	            		$printryData[$i]['contact'] = $contact[$i];
	            	}
	            	if(!empty($shop_name)){
	            		$printryData[$i]['shop_name'] = $shop_name[$i];
	            	}
	            	if(!empty($po_box)){
	            		$printryData[$i]['po_box'] = $po_box[$i];
	            	}
	            	if(!empty($building)){
		            	$printryData[$i]['building'] = $building[$i];
		            }
		            if(!empty($latitude)){
		            	$printryData[$i]['latitude'] = $latitude[$i];
		            }
		            if(!empty($longitude)){
		            	$printryData[$i]['longitude'] = $longitude[$i];
		            }
		            if(!empty($start_time)){
		            	$printryData[$i]['start_time'] = $start_time[$i];
		            }
		            if(!empty($end_time)){
		            	$printryData[$i]['end_time'] = $end_time[$i];
		            }
		            if(!empty($total_workers)){
			        	$printryData[$i]['total_workers'] = $total_workers[$i];
			    	}
	            	$printryData[$i]['status'] = '0';
	            	$printryData[$i]['registeration_date'] =  date('Y-m-d h:i:s');
	            	$printryData[$i]['created_by'] = $created_by;
					
					$services = '';
					if(!empty($serArr[$i])){ $services = implode(',',$serArr[$i]); }		    

			        $printryData[$i]['services'] = $services;

					//branch incharge data
					$incharge_person[$i]['user_id'] = $user_id;
					$incharge_person[$i]['fname'] = $c_in_fname[$i];
					$incharge_person[$i]['lname'] = $c_in_lname[$i];
					$incharge_person[$i]['email'] = $c_in_email[$i];
					$incharge_person[$i]['mobile'] = $c_in_mobile[$i];
					$incharge_person[$i]['status'] = '0';
	        	}


// echo "<pre>"; print_r($_POST);
// print_r($printryData);
// exit;

				$return  = $this->vendormodel->insert_branches($printryData,$incharge_person,$user_id);

	        	$return= array('success'=>true,'msg'=>'added successfully');
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	        
	    }else{
            $return= array('success'=>false,'msg'=>'post empty');
	    }
	    echo json_encode($return);
	}

	public function edit_bankdetail(){

		if(!empty($this->input->post()) && $this->input->is_ajax_request()){
			//echo "<pre>"; print_r($_POST); exit;
			if($this->form_validation->run('add_bank_acc')){
	        
				$user_id = $this->input->post('user_id');
				$data['vendor_id'] = $this->vendordata['user_id'];
				$data['acc_holder_name'] = $this->input->post('acc_holder');
				$data['bank_name'] = $this->input->post('bank_name');
				$data['branch_name'] = $this->input->post('branch_name');
				$data['acc_number'] = $this->input->post('acc_number');
				$data['ifsc_code'] = $this->input->post('ifsc_code');
				$data['status'] = '0';
				$data['created_at'] = date('Y-m-d h:i:s');

				$insert_id = $this->generalmodel->add("vendor_bank_account",$data);
				if(!empty($insert_id)){
		        	$return= array('success'=>true,'msg'=>'added successfully');
				}else{
	            	$return= array('success'=>false,'msg'=>'internal error');
				}
	        }else{
	            $return= array('success'=>false,'msg'=>validation_errors());
	        }
	    }else{
            $return= array('success'=>false,'msg'=>'post empty');
	    }
	    echo json_encode($return);		
	}

	public function vendor_logout(){
		$this->session->unset_userdata('vendordata');
		redirect('/');
	}
	

}
