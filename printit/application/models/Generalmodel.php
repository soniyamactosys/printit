<?php
if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Generalmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->userdata = $this->session->userdata('userdata');
    }

//=============curd operation=============

    public function add($tableName, $data) {

        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    public function add_product($productData,$stockData){
        $this->db->trans_start();
        $this->db->insert('products', $productData);
        $product_id = $this->db->insert_id();

        $stockData['product_id'] = $product_id;
        $this->db->insert('product_stock', $stockData);

        $this->db->trans_complete();
        return  $product_id ;
    }

    public function update_product($productData,$stockData,$pwhere){
        $this->db->trans_start();
        $update = $this->db->update('products',$productData,$pwhere);
         $this->db->insert('product_stock', $stockData);
        $this->db->trans_complete();
        return  $update;        
    }

    public function updaterecord($table,$data,$where){
    	return $this->db->update($table,$data,$where);
    }

    public function add_order_products($oid,$prodData,$stockData){

        $this->db->trans_start();

        $oldprod = $this->getparticularData("prod_id,prod_qty",'order_products',array('order_id'=>$oid,'status'=>'0'),'result_array');
        
        if(!empty($oldprod)){
            foreach($oldprod as $val){
                $oldqty = $val['prod_qty'];
                $pid = $val['prod_id'];
                $this->db->query("UPDATE `products` SET `qty` = `qty`+$oldqty WHERE `id`=".$pid);
                
               // echo $this->db->last_query()."<br>";
            }
        }
        
        $this->db->update('order_products',array('status'=>'1'),array('order_id'=>$oid));
        if(!empty($prodData)){
            $insertquery = $this->db->insert_batch('order_products',$prodData);
            $product_total = get_prod_total($prodData);
        }else{
            $product_total = 0;
        }
        if(!empty($stockData)){
            foreach($stockData as $val){
                $qty = $val['qty'];
                $pid = $val['id'];
                $this->db->query("UPDATE `products` SET `qty` = `qty`-".$qty." WHERE `id`=".$pid);
                
                //echo $this->db->last_query()."<br>";            
            }
        }
        $query = "UPDATE orders SET product_total=$product_total, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$oid;

        $this->db->query($query);        
//echo $this->db->last_query()."<br>";

        $this->db->trans_complete();
        return $query;
    }

    public function update_order_delivery_address($address_id,$delivery_charge,$order_address){
        
        $this->db->trans_start();
        $this->db->update('order_address',array('status'=>'1'),array('order_id'=>$order_address['order_id']));
        $addQuery = $this->generalmodel->add('order_address',$order_address);

        //$query = "UPDATE orders SET pickup_delivery='2',delivery_address=$address_id,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$order_address['order_id'];
        $query = "UPDATE orders SET pickup_delivery='2',delivery_address=$addQuery,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$order_address['order_id'];

        $lastid = $this->db->query($query);
        $this->db->trans_complete();        
        return $addQuery;

    }


    public function update_tran_delivery_address($address_id,$order_address,$lat,$long){
        
        $this->db->trans_start();
        
        if(!empty($lat) && !empty($long)){
            $nearbyvendor = $this->get_nearby_vendor($lat,$long);
        }
        if(empty($nearbyvendor)){
            return false;
        }
            
        $q1 = $this->db->update('order_address',array('status'=>'1'),array('order_id'=>$order_address['order_id']));

// echo $this->db->last_query();
// echo "<br>".$q1."<br>";
        $addQuery = $this->generalmodel->add('order_address',$order_address);

// echo $this->db->last_query();
// echo "<br>".$addQuery."<br>";

        //$q3 = $this->db->update("orders",array('pickup_delivery'=>2,'delivery_address'=>$address_id,'progress_status'=>'awaiting_for_translation_proposals'),array('id'=>$order_address['order_id']));
        $q3 = $this->db->update("orders",array('pickup_delivery'=>2,'delivery_address'=>$addQuery,'progress_status'=>'awaiting_for_translation_proposals'),array('id'=>$order_address['order_id']));

// echo $this->db->last_query();
// echo "<br>".$q3."<br>";
        //============

        if(!empty($nearbyvendor)){
            
            foreach($nearbyvendor as $i=>$val){
                $trans_proposals[$i]['order_id'] = $order_address['order_id'];
                $trans_proposals[$i]['printery_shop_id'] = $val['id'];
                $trans_proposals[$i]['status'] = 'waiting_for_proposal';
                $trans_proposals[$i]['price'] = '0';
            }
            $this->db->update("trans_proposals",array('delete_status'=>'1'),array('order_id'=>$order_address['order_id']));
            $q4 = $this->insert_batchdata("trans_proposals",$trans_proposals);
        }
        //============

// echo $this->db->last_query();
// echo "<br>".$q4."<br>";
// exit;

        $this->db->trans_complete();    
        if(!empty($nearbyvendor)){ proposal_mail_to_vendor($nearbyvendor,$order_address['order_id']); }
        return $addQuery;

    }
    public function add_new_trans_address($customerAdd,$orderAdd){
        $this->db->trans_start();
        
        $address_id= $this->db->insert('customer_address',$customerAdd);

        $this->db->update('order_address',array('status'=>'1'),array('order_id'=>$orderAdd['order_id']));

        $orderAdd['c_add_id'] = $address_id;
        $addQuery = $this->db->insert('order_address',$orderAdd);        


        //$query = "UPDATE orders SET pickup_delivery='2',status='1',delivery_address=$address_id,progress_status='awaiting_for_translation_proposals' WHERE `id`=".$orderAdd['order_id'];
        $query = "UPDATE orders SET pickup_delivery='2',status='1',delivery_address=$addQuery,progress_status='awaiting_for_translation_proposals' WHERE `id`=".$orderAdd['order_id'];

        $lastid = $this->db->query($query);



        //============
        if(!empty($customerAdd['latitude']) && !empty($customerAdd['longitude'])){
            
            $nearbyvendor = $this->get_nearby_vendor($customerAdd['latitude'],$customerAdd['longitude']);
            if(!empty($nearbyvendor)){
                foreach($nearbyvendor as $i=>$val){
                    $nearbyvendor[$i]['order_id'] = $orderAdd['order_id'];
                    $nearbyvendor[$i]['printery_shop_id'] = $val['id'];
                    $nearbyvendor[$i]['status'] = 'waiting_for_proposal';
                    $nearbyvendor[$i]['price'] = '0';
                }
                $this->insert_batchdata("trans_proposals",$nearbyvendor);
            }
        }
        //============

        $this->db->trans_complete();
        if(!empty($nearbyvendor)){ proposal_mail_to_vendor($nearbyvendor,$orderAdd['order_id']); }

        return $address_id;
    }



    public function add_new_order_address($customerAdd,$orderAdd,$delivery_charge){
        $this->db->trans_start();
        
        $this->db->insert('customer_address',$customerAdd);
        $address_id= $this->db->insert_id();

        $this->db->update('order_address',array('status'=>'1'),array('order_id'=>$orderAdd['order_id']));

        $orderAdd['c_add_id'] = $address_id;
        $addQuery = $this->db->insert('order_address',$orderAdd);        


        //$query = "UPDATE orders SET pickup_delivery='2',delivery_address=$address_id,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$orderAdd['order_id'];
        $query = "UPDATE orders SET pickup_delivery='2',delivery_address=$addQuery,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$orderAdd['order_id'];

        $lastid = $this->db->query($query);

        $this->db->trans_complete();
        return $address_id;
    }
    
    public function register_user($userdata,$userAdd){
        $this->db->trans_start();
        
        $this->generalmodel->add('users',$userdata);
        
        $user_id = $this->db->insert_id();
        
        $userAdd['user_id'] = $user_id;
        
        $this->db->insert('customer_address',$userAdd);
        
        $this->db->trans_complete();
        return $user_id;
    }
    
    
    
    /*public function print_management_records($where=""){
        
        if(empty($where)){
            $where = array('pm.status !='=>'2');
        }
        $query =  $this->db->select('pm.id as pm_id,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.average_price,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,pm.banner_size,pm.service_id,pm.color,pm.binding')
        ->from('price_management as pm')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->where($where)
        ->get()
        ->result_array();
        // echo "<pre>";
        // echo $this->db->last_query();
        // print_r($query); exit;

        return $query;
        
    }*/
    public function price_combination_records($where=""){
        
        if(empty($where)){
            $where = array('pm.status !='=>'2');
        }
        $query =  $this->db->select('pm.id as pm_id,serv.name AS service_name,extra_copies,flyer_copy_from,flyer_copy_to,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,pm.banner_size,banner_size.name as banner_size_name,pm.service_id,pm.color,pm.binding')
        ->from('price_combination as pm')
        ->join('services as serv','serv.id = pm.service_id','left')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->join('banner_size','banner_size.id = pm.banner_size','left')
        ->where($where)
        ->get()
        ->result_array();
        return $query;
        
    }
    public function price_management_records($vendor_id="",$branchid="",$pm_id="",$return_type=""){

        $where['ps.delete_status'] ='0';
        $where['ps.user_id']=$vendor_id;  
        if(!empty($branchid)){
            $where['ps.id']=$branchid;      
        }      
        if(!empty($pm_id)){
            $where['pm.id']=$pm_id;      
        }

        $data =  $this->db->select('ps.user_id,ps.id as printery_id,ps.branch,ps.shop_name,ps.services,pm.id as pm_id,pm.service_id,services.name as service_name,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.average_price,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,bs.name as width,pm.color,pm.binding')
        ->from('printery_shop as ps')
        ->join('print_management as pm','ps.id = pm.printery_shop_id','left')
        ->join('services','services.id = pm.service_id','left')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->join('banner_size as bs','bs.id = pm.banner_size','left')
        ->where($where)
        ->get();

        /*if(!empty($branchid)){
            return $data->row_array();
        }else{
            return $data->result_array();
        }*/

        if(!empty($return_type)){
            if($return_type=='row'){
                return $data->row_array();
            }elseif($return_type=='result'){
             return $data->result_array();
            }
        }elseif(!empty($branchid)){
            return $data->row_array();
        }else{
             return $data->result_array();
        }
    }

    public function saved_orders(){
        $userdata = $this->session->userdata['userdata'];
        $userid = $userdata['user_id'];

        if(empty($where)){
            $where = array('o.customer'=>$userid,'status'=>'0');
        }
        return $this->db->select('o.id as order_id,o.project_name,o.updated_at,o.amount')
        ->from('orders as o')
        ->where($where)
        ->get()->result_array(); 


    }
    public function orders($status,$userid=""){
        if(empty($userid)){
            $userdata = $this->session->userdata['userdata'];
            $userid = $userdata['user_id'];
        }
        if($status !=''){
            //$where = array('o.customer'=>$userid,'o.status'=>$status,'o.payment_status'=>$status);
            $where = array('o.customer'=>$userid,'o.status'=>$status,'o.payment_status'=>$status);
            //$where = array('o.customer'=>$userid,'o.payment_status'=>$status,'o.order_type !='=>'3');
        }else{
            $where = array('o.customer'=>$userid);
        }
        return $this->db->select('o.id as order_id,o.vendor as branch_id,o.order_type,o.project_name,o.updated_at,o.amount as price,o.amount,o.order_total,pt.name as print_type')
        ->from('orders as o')
        ->join('print_type as pt',"pt.id = o.print_type AND pt.status='0'",'left')
        ->where($where)
        ->order_by("o.id","desc")
        ->get()
        ->result_array();         
    }

    public function orders_status($status="",$progress_status=""){
        $userdata = $this->session->userdata['userdata'];
        $userid = $userdata['user_id'];

        $where = "o.customer = ".$userid." AND o.order_type !='3' AND o.payment_status='1' AND pt.status='0' AND (o.progress_status !='delivered' OR o.progress_status IS NULL)";
        // $where['o.customer'] = $userid;
        // $where['pt.status']='0';
        // $where['o.progress_status !='] = 'delivered';
        // $where['o.progress_status OR IS'] = 'NULL';

        // if(!empty($progress_status)){
        //     $where['o.progress_status'] = $progress_status;
        // }

        $custom_quick =  $this->db->select('o.id as order_id,o.order_type,o.project_name,o.updated_at,o.amount,pt.name as print_type,o.status,o.progress_status')
        ->from('orders as o')
        ->join('print_type as pt','pt.id = o.print_type','left')
        ->where($where)
        ->order_by('order_id','DESC')
        ->get()
        ->result_array(); 

// echo $this->db->last_query();
// echo "<pre>"; print_r($custom_quick); 

        $trans_where = "o.customer = ".$userid." AND o.order_type='3' AND o.progress_status NOT IN('delivered','saved') AND o.progress_status IS NOT NULL ";

        $translation =  $this->db->select('o.id as order_id,o.order_type,o.project_name,o.payment_status,o.updated_at,o.amount,o.status,o.progress_status,o.trans_proposal_count')
        ->from('orders as o')
        ->where($trans_where)
        ->order_by('order_id','DESC')
        ->get()
        ->result_array();     

        $merge = array_merge($custom_quick,$translation);

// echo $this->db->last_query();
// echo "<pre>"; print_r($translation); exit;

        usort($merge, function($a, $b) {
            return $a['order_id'] < $b['order_id'];
        });

        return  $merge;          
    }
    
    public function trans_proposal($oid){
        
        $trans_where = "tp.order_id = $oid AND (tp.status = 'proposal_sent' OR tp.status = 'proposal_accepted' ) AND `tp`.price >=0 AND ps.status='1' AND ps.delete_status='0' AND `tp`.delete_status='0'";
        //$trans_where = "tp.order_id = $oid AND (tp.status = 'proposal_sent' OR tp.status = 'proposal_accepted' ) AND `tp`.price >=0  AND ps.status='1' AND ps.delete_status='0'";
        
        $query =  $this->db->select('o.project_name,tp.printery_shop_id as printery_id,COALESCE(tp.note,"NA") as note,tp.order_id,tp.price as vendor_charge,tp.updated_at,ps.id as branch_id,ps.shop_name,ps.building as address,ps.logo,ps.start_time,ps.end_time,tp.status')
        ->from('trans_proposals as tp')
        ->join('printery_shop as ps','ps.id = tp.printery_shop_id')
        ->join('orders as o','o.id = tp.order_id')
        ->where($trans_where)
        ->order_by('order_id')
        ->get()
        ->result_array(); 
        return $query;
        
    }

    public function order_detail($oid,$status=""){

        $where['orders.id'] = $oid;

        $otype_query = $this->db->select('id,service_type,order_type')
        ->from('orders')->where($where)->get()->row_array();

        // echo $this->db->last_query();
        // print_r($otype_query);
        // exit;
        // $order_type = $otype_query['order_type'];

        if(!empty($status)){
            $where['orders.status'] = "$status";
        }

        if($otype_query['order_type']==3){

            $tables[0]['table'] = 'users as u';
            $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

            $tables[1]['table'] = 'translation_lang as frmlang';
            $tables[1]['on'] = 'orders.lang_from = frmlang.id';

            $tables[2]['table'] = 'translation_lang as tolang';
            $tables[2]['on'] = 'orders.lang_to = tolang.id';

            $tables[3]['table'] = 'translation_types as trans_type';
            $tables[3]['on'] = 'orders.trans_type_id = trans_type.id';

            $tables[4]['table'] = 'order_address as address';
            $tables[4]['on'] = 'orders.id = address.order_id AND `address`.`status`="0"';
            
            $tables[5]['table'] = 'printery_shop as ps';
            $tables[5]['on'] = 'ps.id = orders.vendor AND ps.status="1" AND ps.delete_status="0"';

            $tables[6]['table'] = 'services';
            $tables[6]['on'] = 'orders.order_type = services.id AND services.status!="2"';     
            
            $query =   $this->getfrommultipletables("orders.*,services.name as ser_name,trans_type.name as trans_type,frmlang.name as from,tolang.name as to,u.email as customer_email,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,orders.created_at as order_date,orders.id as order_number,address.address as d_address,ps.latitude,ps.longitude,ps.building",'orders',$tables,$where,"","orders.id","","","row_array");             

        }else{        

            $tables[0]['table'] = 'users as u';
            $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

            $tables[1]['table'] = 'printery_shop as ps';
            $tables[1]['on'] = 'ps.id = orders.vendor AND ps.status="1" AND ps.delete_status="0"';

            $tables[2]['table'] = 'paper_type as ptype';
            $tables[2]['on'] = 'orders.paper_type = ptype.id AND ptype.status!="2"';

            $tables[3]['table'] = 'paper_size as psize';
            $tables[3]['on'] = 'orders.paper_size = psize.id AND psize.status!="2"';

            $tables[4]['table'] = 'order_address as o_add';
            $tables[4]['on'] = "o_add.order_id = orders.id AND orders.pickup_delivery='2' AND o_add.status='0'";
            
            $tables[5]['table'] = ' banner_size as bn_size';
            $tables[5]['on'] = 'orders.width = bn_size.id AND bn_size.status!="2"';

            $tables[6]['table'] = 'services';
            $tables[6]['on'] = 'orders.order_type = services.id AND services.status!="2"';            

            $query =   $this->getfrommultipletables("orders.*,services.name as ser_name,bn_size.name as bannersize,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,u.email as customer_email, u.email as customer_email,o_add.address as d_address,u.mobile as customer_mobile,orders.created_at as order_date,orders.id as order_number,psize.name AS papersize,ptype.name as papertype,ps.user_id as user_id,ps.branch,ps.id as branch_id,ps.contact as p_contact,ps.shop_name AS vendor_name,ps.start_time,ps.end_time,ps.latitude as p_lat,ps.longitude as p_lng,ps.building as p_address,o_add.area as d_area,o_add.address as d_address,o_add.mobile as d_mobile,orders.delivery_address,orders.pickup_delivery  ",'orders',$tables,$where,"","orders.id","","","row_array");  
            if(!empty($query)){
                $query['products'] = $this->db->select('op.prod_id,op.prod_price as price,op.prod_qty as qty,op.prod_price,op.prod_qty,p.image,p.name')
                ->from('order_products as op')
                ->join('products as p','op.prod_id = p.id AND op.status="0"','LEFT')
                ->where("op.order_id=".$oid." AND op.status='0'")
                ->get()
                ->result_array();
                    
            }
        }

//  echo $this->db->last_query();
//  echo "<pre>";
//  print_r($query); 
//  print_r($order_products); exit;
        return $query;
    }

    public function insert_batchdata($table,$data){
        $str = $this->db->insert_batch($table, $data);
        if(!empty($str)){
            return true;
        }else{
            return false;
        }
    }
    public function deleterecord($table,$where){
        //return $this->db->delete($table,$where);
        return $this->db->update($table,array('delete_status'=>'1'),$where);
    }

    public function customquery($queryy,$returnType=""){
        $query = $this->db->query("$queryy");
        //print_r($query); exit;
        //echo $query->num_rows(); exit;
        if(!is_bool($query)){
            if(empty($returnType) || $returnType=="result_array"){
                $return =  $query->result_array();
            }elseif($returnType=="result"){
                $return =  $query->result();
            }elseif($returnType=="row_array"){
                $return =  $query->row_array();
            }elseif($returnType=="row"){
                $return =  $query->row();
            }      
        }else{
            $return  = $query;
        }
        return $return;
    }

    public function getparticularData($select="*",$table,$where="",$returnType="",$limit="",$start="",$orderby=""){
        
        $this->db->select($select);
        $this->db->from($table);
        if($where!=""){
            $this->db->where($where);
        }

        if($limit!="" && $start!="")
        {
            $this->db->limit($limit,$start);
        }
        else if($limit!="")
        {
            $this->db->limit($limit);
        }

        if($orderby!="")
        {
            $this->db->order_by($orderby);
        }

        $query = $this->db->get();

        if($query !== FALSE && $query->num_rows() > 0){

        if(empty($returnType) || $returnType=="result_array"){
            $return =  $query->result_array();
        }elseif($returnType=="result"){
            $return =  $query->result();
        }elseif($returnType=="row_array"){
            $return =  $query->row_array();
        }elseif($returnType=="row"){
            $return =  $query->row();
        }      
        }else{
            $return =array();
        }
        return $return;
    }

    public function getfrommultipletables($data="*",$maintable, $tables,$where="",$groupby="",$orderby="",$limit="",$start="",$returnType="",$having=""){
        $return = array();
        $this->db->select($data);
        $this->db->from($maintable);

        foreach($tables as $value){
            $table = $value['table'];
            $on = $value['on'];
            $this->db->join($table,$on,'LEFT');
        }
      
        if($where !=""){
            $this->db->where($where);
        }

        if($limit!="" && $start!="")
        {
            $this->db->limit($limit,$start);
        }
        else if($limit!="")
        {
            $this->db->limit($limit);
        }
      
        if($groupby!="")
        {
            $this->db->group_by($groupby); 
        }
        
        if($orderby!="")
        {
            $this->db->order_by($orderby); 
        }
        if(!empty($having)){
             $this->db->having($having); 
        }
        
      $query = $this->db->get();


// echo "<pre>";
// print_r($query);
// print_r($data);
// print_r($maintable);
// print_r($tables);print_r($where);
// print_r($returnType);
// exit;

      //echo $this->db->last_query();
      if($query->num_rows() > 0){

        if(empty($returnType) || $returnType=="result_array"){
            $return =  $query->result_array();

        }elseif($returnType=="result"){
            $return =  $query->result();
        }elseif($returnType=="row_array"){
            $return =  $query->row_array();
        }elseif($returnType=="row"){
            $return =  $query->row();
        }       
      } 
      return $return; 
    }

    public function modify($tableName, $colName, $id, $data) {

        $this->db->where($colName, $id);

        $this->db->update($tableName, $data);
        return $this->db->affected_rows();
    }    

    public function modifyMulti($tableName, $data1, $data) {

        $this->db->where($data1);

        $this->db->update($tableName, $data);
        return $this->db->affected_rows();
    }
    public function delete($tableName, $colName, $id) {

        $this->db->where($colName, $id);

        $this->db->delete($tableName);

        return TRUE;
    }

    public function deleteMulti($tableName, $wh) {

        $this->db->where($wh);

        $this->db->delete($tableName);

        return TRUE;
    }
    //=============curd operation=============


    public function mail_template($select,$type){
        return $this->getparticulardata($select,'email_template',array('type'=>$type,'status'=>'1'),'row_array');
    }

    // To get single row of table by a id 

public function getSingleRowById($tableName, $colName, $id, $returnType = '') {

    $this->db->where($colName, $id);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0) {

        if ($returnType == 'array')

            return $result->row_array();

        else

            return $result->row();
    }

    else

        return FALSE;

}



    // To get all row of matching criteria

public function getRowAllById($tableName, $colName, $id, $orderby = '', $orderformat = 'asc') {

    if ($colName != '' && $id != '')

        $this->db->where($colName, $id);

    if ($orderby != '')

        $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0)

        return $result->result();

    else

        return FALSE;

}



    // To get data by multiple where 	

public function getRowByWhere($tableName, $filters = '', $select = '', $noRowReturn = '', $returnType = '', $orderby = '', $orderformat = 'asc') {

    if ($select != '')

        $this->db->select($select);

    if (count($filters) > 0) {

        foreach ($filters as $field => $value)

            $this->db->where($field, $value);

    }
    if ($orderby != '')

        $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0) {

        if ($noRowReturn == 'single') {

            if ($returnType == 'array')

                return $result->row_array();

            else

                return $result->row();
        }

        else {
            if ($returnType == 'array')
                return $result->result_array();
            else
                return $result->result();
        }

    }
    else
        return FALSE;

}
 
    // Pagination function 

public function getPaginationData($tableName, $filters = '', $perPage, $start, $orderby, $orderformat,$selectData=''){
 
    //Set default order
    if($selectData!='') 
    {
    $this->db->select($selectData);
    }
    if ($orderformat == '')

    $orderformat = 'asc';

    //add where clause

    if ($filters != '' && count($filters) > 0)

    $this->db->where($filters);

    $this->db->limit($perPage, $start);

    $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0)

    return $result->result();

    else

    return FALSE;
}

/*---get--pagination---*/

public function get_pagination_result($table_name='',$id_array='',$limit='',$offset='',$orderid,$order='')
{     
       
    if(!empty($id_array)):

    foreach ($id_array as $key => $value){
    $this->db->where($key, $value);

    }
    endif;

    if($order!=''){
    $this->db->order_by($orderid,$order); 
    }
    else{
    $this->db->order_by('id','desc'); 
    } 

    if($limit > 0 && $offset>=0){ 

    //$this->db->where($filters);
    $this->db->limit($limit, $offset);

    $query=$this->db->get($table_name);

    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;

    }else{
    $query=$this->db->get($table_name);
    return $query->num_rows();
    }
}


    //Function to return total number of rows

public function getTotalRows($tableName, $filters = NULL) {

    if ($filters != NULL) {

    $this->db->where($filters);

    $count = $this->db->count_all_results($tableName);

    }

    else

    $count = $this->db->count_all($tableName);

    return $count;
}
 
public function get_result($table_name='', $id_array='',$id_array2=''){
        
        if(!empty($id_array)):      
            foreach ($id_array as $key => $value){
                $this->db->where($key, $value);
             
            }
       endif;
        if(!empty($id_array2)):     
            foreach ($id_array2 as $key => $value){
                $this->db->or_where($key, $value);
            }
        endif;
        $query=$this->db->get($table_name);
        if($query->num_rows()>0)
            return $query->result();
        else
            return FALSE;
}
 
public function get_row($table_name='', $id_array='', $id_array2=''){
	if(!empty($id_array)):      
		foreach ($id_array as $key => $value){
			$this->db->where($key, $value);
		}
	endif;
	if(!empty($id_array2)):     
		foreach ($id_array2 as $key => $value){
			$this->db->or_where($key, $value);
		}
	endif;
	$query=$this->db->get($table_name);
	if($query->num_rows()>0) 
		return $query->row();
	else
		return FALSE;
}

public function get_resultbylimit($table_name='',$limit="",$id_array='',$id_array2=''){
    $this->db->limit($limit);
    if(!empty($id_array)):      
    foreach ($id_array as $key => $value){
    $this->db->where($key, $value);

    }
    endif;
    if(!empty($id_array2)):     
    foreach ($id_array2 as $key => $value){
    $this->db->or_where($key, $value);
    }
    endif;
    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Or operator =================------

public function getOrResult($table_name='',$id_array=''){

    if(!empty($id_array)):     

    $this->db->or_where($id_array);
    endif;
    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Using Clause =================------

public function getInClauseData($select='',$table_name='',$colName='',$arr='',$where=''){

    if ($select != '')
    $this->db->select($select);

    if(count($where)>0)
    $this->db->where($where);

    $this->db->where_in($colName, $arr);

    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Using Join=================------

public function getJoinData($seldata,$table1='',$table2='',$join_condition='',$wh='',$orderby='id',$orderformat='asc',$resultType="",$limit="",$start=""){

    $this->db->select($seldata);

    $this->db->from($table1);

    $this->db->join($table2,$join_condition);
	if(!empty($wh))
    $this->db->where($wh);

    $this->db->order_by($orderby, $orderformat);



    if($limit!="" && $start!="")
    {
        $this->db->limit($limit,$start);
    }
    else if($limit!="")
    {
        $this->db->limit($limit);
    }
        
    $query = $this->db->get();
    
    if(empty($resultType)){
        $query->result();
    }else{
        if($resultType=='result_array'){
           return $query->result_array();
        }elseif($resultType=='row_array'){
            return $query->row_array();
        }elseif($resultType=='row'){
            return $query->row();
        }
    }
}

public function customjoin($seldata,$table1='',$table2='',$table3='',$table4='',$join_condition='',$wh=''){

    $this->db->select($seldata);

    $this->db->from($table1,$table2,$table3,$table4);
    
    if(!empty($join_condition))
    $this->db->where($join_condition);

    $query = $this->db->get();
    

    return $query->row_array();

}

public function getsingleJoinData($seldata,$table1='',$table2='',$join_condition='',$wh=''){

    $this->db->select($seldata);

    $this->db->from($table1);

    $this->db->join($table2,$join_condition);
    if(!empty($wh))
    $this->db->where($wh);

  
        
    $query = $this->db->get();
    

    return $query->row_array();

}

/* End of file generalmodel.php */
public function get_all_record($table)
{
    $query = $this->db->get($table);
    if ($query->num_rows() > 0) 
    {
        return $query->result_array();
    } 
    else 
    {
        return array();
    }
}

public function get_data_by_condition($data,$table,$condition){
    $this->db->select($data);
    $query=$this->db->from($table);
    $query=$this->db->where($condition);
    $query=$this->db->get();
    //echo $this->db->last_query(); die();
    return $query->result_array();
    //$this->db->close();
} 

public function threetables($data,$table1,$table2,$table3,$on,$on2,$condition,$where)
{
    $this->db->select($data);
    $this->db->from($table1);
    $this->db->join($table2,$on);
    $this->db->join($table3,$on2);
    $this->db->where($condition);
    if($where!="")
    {
        $this->db->where($where);
    }
    $query=$this->db->get();
    //return $this->db->last_query();
    return $query->result_array();
}

public function threetablesall($data,$table1,$table2,$table3,$on,$on2,$where)
{
    $this->db->select($data);
    $this->db->from($table1);
    $this->db->join($table2,$on);
    $this->db->join($table3,$on2);
   if($where!="")
    {
        $this->db->where($where);
    }
    $query=$this->db->get();
    //return $this->db->last_query();
    return $query->result_array();
}

//==============project functions===========




    public function check_user_exist($email,$id=''){
        $where = "`email`='$email'";
        if(!empty($id)){ $where .= " AND `user_id` !=$id"; }
        return $this->db->select('email')->from('user')->where($where)->get()->row_array();
    }

   
    public function consumer_products($cid){

        $where = array('orders.createdby'=>$cid);
        $this->db->select('GROUP_CONCAT(DISTINCT ord_prod_id SEPARATOR ",") AS pro_id'); 
        $this->db->from('orders');
        $this->db->join('orders_product','orders.orders_id = orders_product.orders_id','LEFT');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array(); 
    }

  
    
    //==================printeryshop_list=======================
    function printeryshop_list($where=""){
        
        if(empty($where)){
            $where = "p.status='0' AND p.delete_status='0'   ";
        } 
        $query = $this->db->select('p.id as id,users.id as user_id,users.*,p.*,p.status as shop_status')
        ->from('printery_shop as p')
        ->join('users',"users.id=p.user_id AND users.role='2' AND users.delete_status='0' AND `users`.`status`='0'",'LEFT')
        ->where($where)
        ->order_by("p.shop_name","ASC")
        ->get()->result_array();
        return $query;
    }

    public function get_avg_price($where){
        $query = $this->db->select('*')->from('print_management')->where($where)->get()->row_array();
       return  $query;
    }

    public function get_price_range($where){
        $query = $this->db->select('*')->from('price_range')->where($where)->get()->row_array();
        return  $query;
    }
    
    //==================printeryshop_list=======================
    function printeryshop_count($where=""){
        $query= $this->db->select('count(`users`.`id`) AS total')
        ->from('printery_shop as p')
        ->join('users','users.id=p.user_id','LEFT')
        ->where("users.role='2' AND users.delete_status='0' AND p.delete_status='0'");
        
        if(!empty($where)){
            $query->where($where);
        }
        return $query->get()->row_array();
    }
    
    
    function vendor_detail($id){
        $query= $this->db->select('users.*,p.*,p.status as shop_status')
        ->from('printery_shop as p')
        ->join('users','users.id=p.user_id','LEFT')
        ->where("users.role='2' AND users.delete_status='0' AND `users.id`=$id");
        
        if(!empty($where)){
            $query->where($where);
        }
        return $query->get()->row_array();
    }
    
    function user_detail($id){
        $query= $this->db->select('users.firstname,users.lastname,users.gender,users.email,users.firstname,users.mobile,users.password,users.address,users.latitude,users.longitude,users.created_at,users.status')
        ->from('users')
        ->where("users.role='3' AND users.delete_status='0' AND `users.id`=$id AND `id`=$id");
        return $query->get()->row_array();        
    }

    function get_services($status){
        return $this->db->select('*')->from('services')->where(array('status'=>$status))->get()->result_array();
    }

    function branch_data($id){
        return $this->db->select('p.id as printery_id,p.*,pm.*')
        ->from('printery_shop as p')
        ->join('print_management as pm',"p.id = pm.printery_shop_id AND pm.status='0'",'LEFT')
        ->where("`p`.`id`=$id AND `p`.delete_status='0'")
        ->get()->row_array();        
    }

    function notes_categories($status){

        $where = "`status` !='2'";
        $query = $this->db->select('*')->from('notes_categories as nc');
        if($status!=''){
            $where .=" AND `status`='$status'";
        }
        $query->where($where);

        return $query->get()->result_array();
    }

    function admin_notes($status,$catid=""){
        $query = $this->db->select('notes.*,nc.name as cat')
        ->from('notes')
        ->join('notes_categories as nc',"notes.category = nc.id",'LEFT')
        ->where("`notes`.`is_delete`='0'");

        if($status !=''){
            $query->where("`notes`.`status`='$status' AND nc.status='0'") ;
        }
        if($catid !='' && $catid >0){
            $query->where("`nc`.`id`='$catid'") ;
        }        
        return $query->get()->result_array(); 
    }

    function note_detail($id){
        $query = $this->db->select('notes.*,nc.name as cat')
        ->from('notes')
        ->join('notes_categories as nc',"notes.category = nc.id",'LEFT')
        ->where("`notes`.`is_delete`='0' AND `notes`.`id`=$id");
        return $query->get()->row_array();        
    }

    public function get_nearby_vendor($lat,$long){
        
        if(!empty($lat) && !empty($long)){
        
            $query = "SELECT printery_shop.id , (3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - printery_shop.latitude) * pi()/180 / 2), 2) +COS( ".$lat." * pi()/180) * COS(printery_shop.latitude * pi()/180) * POWER(SIN(( ".$long." - printery_shop.longitude) * pi()/180 / 2), 2) ))) as distance from printery_shop WHERE status='1' AND `delete_status`='0' having distance <= 2 order by distance";
            return $this->db->query($query)->result_array();
        }else{
            return array();
        }
    }


    public function get_near_by_vendor($lat,$long){

        $query = "SELECT printery_shop.id , (3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - printery_shop.latitude) * pi()/180 / 2), 2) +COS( ".$lat." * pi()/180) * COS(printery_shop.latitude * pi()/180) * POWER(SIN(( ".$long." - printery_shop.longitude) * pi()/180 / 2), 2) ))) as distance from printery_shop WHERE status='1' AND `delete_status`='0' having distance <= 10 order by distance";

//print_r($query); 
//echo $this->db->last_query();
//exit;
        return $this->db->query($query)->result_array();
    }
    public function get_trans_proposals($branch_ids,$all=""){
        
        $where = "`printery_shop_id` IN(".$branch_ids.") ";
        if(empty($all)){
            $where .= " AND `tp`.`status`='waiting_for_proposal'";
        }
        $query = $this->db->select('o.id as order_id,o.project_name,o.created_at,tp.status,tp.price')
        ->from('trans_proposals as tp')
        ->join("orders as o","tp.order_id=o.id",'LEFT')
        ->where($where)
        ->order_by("o.created_at","desc")->get();

        // echo $query;
        // echo $this->db->last_query();exit;
        return $query->result_array();        

    }

    public function send_proposal($order_id,$printery_ids,$price,$note){

        $this->db->trans_start();
        $q1 = $this->db->update('trans_proposals',array('status'=>"proposal_sent",'price'=>$price,'note'=>$note),"`order_id`=$order_id AND `printery_shop_id` IN(".$printery_ids.")");

        $q2 = $this->db->query("UPDATE orders SET `progress_status`='proposal_received', `trans_proposal_count`=`trans_proposal_count`+1 WHERE id=$order_id");

        $this->db->trans_complete();
        
        if(!empty($q1) && !empty($q2)){
            return true;
        }else{
            return false;
        }
        //return $q2;
    }
    

    public function get_new_trans_proposals($branch_ids){

        $query = $this->db->select('o.id as order_id,o.project_name,o.created_at,tp.status')
        ->from('trans_proposals as tp')
        ->join("orders as o","tp.order_id=o.id",'LEFT')
        ->join("orders as o","tp.order_id=o.id",'LEFT')
        ->where("`printery_shop_id` IN(".$branch_ids.") AND `tp`.`status`='waiting_for_proposal'")->get();
        return $query->result_array();        

    }
    
    public function accept_trans_proposal($oid,$printery){
        
        $this->db->trans_start();
        
        //==get delivery charges
        $delivery_charge =  get_delivery_charges();
        $data = $this->db->select('price')->from('trans_proposals')->where("`order_id=$oid AND `printery_shop_id=$printery")->get()->row_array();
        
        $this->db->update('trans_proposals',array("status"=>'proposal_accepted'),"`order_id=$oid AND `printery_shop_id=$printery");

        $query = "UPDATE orders SET vendor=$printery,status='0',delivery_charge=$delivery_charge,progress_status='proposal_accepted',step_url='".site_url('payment')."',amount=".$data['price'].", order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$oid;
        $this->db->query($query);
       
        $this->db->trans_complete();
        return $data['price'] ;
    
    }
    
    public function customer_current_order($customer_id){
        $query = $this->db->select('*')
        ->from("orders")
        ->where("progress_status NOT IN('saved','delivered') AND customer =$customer_id")
        ->get()
        ->result_array();
        return $query ;
    }
    
    public function vendor_of_branch($pid){
        $query = $this->db->select('u.*')
        ->from("users as u")
        ->join("printery_shop as ps","ps.user_id = u.id")
        ->where("ps.id=$pid")
        ->get()
        ->row_array();
        return $query ;        
    }

    /*public function get_active_order(){
        $query = "SELECT * FROM `orders` WHERE progress_status ='being_printed'";
        return $this->db->query($query)->num_rows();
    }*/
     public function get_pending_order(){
        $query = "SELECT * FROM `orders` WHERE progress_status !='delivered' AND progress_status !='saved'";
        return $this->db->query($query)->num_rows();
    }
      public function get_completeds_order(){
        
          $query = "SELECT * FROM `orders` WHERE payment_status ='1'";
        return $this->db->query($query)->num_rows();
    }
     public function get_total_order(){
        
          $query = "SELECT * FROM `orders` WHERE progress_status !='saved'";
        return $this->db->query($query)->num_rows();
    }
    
    public function web_paper_types($service_type,$print_type=""){
        
        $where = "pt.status='0' AND pc.status='0' AND pc.service_id=$service_type";
        if(!empty($print_type)){
            $where .= " AND pc.print_type=$print_type";
        }
        $data = $this->db
        ->select('pt.id,pt.name,pt.description')
        ->from('paper_type as pt')
        ->join('price_combination as pc','pc.paper_type_id = pt.id','left')
        ->where($where)
        ->group_by("pt.id")
        ->get()
        ->result_array();
        return $data;
    }
    
    public function web_paper_size($service_type,$print_type=""){
        $where = "pt.status='0' AND pc.status='0' AND pc.service_id=$service_type";
        if(!empty($print_type)){
            $where .= " AND pc.print_type=$print_type";
        }
        $data = $this->db
        ->select('pt.id,pt.name')
        ->from('paper_size as pt')
        ->join('price_combination as pc','pc.paper_size_id = pt.id','left')
        ->where($where)
        ->group_by("pt.id")
        ->get()
        ->result_array();
        return $data;
    }
    
    public function notification($userid,$read_status="",$start="",$limit="10"){
        
        $where = "user_id=".$userid;
        
        if($read_status!=""){
            $where .= " AND read_status='$read_status'";
        }
        $data = $this->db
        ->select('*')
        ->from('notification as n')
        ->where($where);
        
        if(!empty($start)){
            $data->limit($limit,$start);        
        }else{
            $data->limit($limit,0);        
        }
        
        return $data->order_by('created_at','DESC')->get()->result_array();        
    }
    
    public function unread_notif_count($userid){
        $where = "user_id=".$userid." AND read_status='0'";
        $data =  $this->db
        ->select('count(`id`) as totalcount')
        ->from('notification as n')
        ->where($where)->get()->row_array();
        
        return $data['totalcount'];
    }
    
    public function insert_notification($data){
        return $this->db->insert_batch('notification',$data);
    }
    
    public function customer_address($uid,$address_id=""){
        $where = "`user_id`=".$uid." AND `status`='0' AND `latitude` IS Not NULL AND `longitude` IS NOT NULL";
        if(!empty($address_id)){
            $where .= " AND `id`=$address_id";
        }
        return $this->generalmodel->getparticularData('*','customer_address',$where,'result_array');
    }
    


}


/* Location: ./application/models/generalmodel.php */