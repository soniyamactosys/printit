<?php
if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Vendormodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    function UpdateRecordIn($status,$vid){
         $query = " UPDATE `vendor_prices` SET  `price_status` = '$status' WHERE `id` IN ($vid)";
	     return $this->db->query($query); 
	}
	 function UpdateRecordInExp($status,$vid){
         $query = " UPDATE `vendor_prices` SET  `exp_price_status` = '$status' WHERE `id` IN ($vid)";
	     return $this->db->query($query); 
	}
	 function UpdateRecordInNotes($status,$vid){
         $query = " UPDATE `notes_price_range` SET  `price_status` = '$status' WHERE `id` IN ($vid)";
	     return $this->db->query($query); 
	}
	 function UpdateRecordInNotesExp($status,$vid){
         $query = " UPDATE `notes_price_range` SET  `exp_price_status` = '$status' WHERE `id` IN ($vid)";
	     return $this->db->query($query); 
	}
    public function add_vendor($userData,$printryData){
        $this->db->trans_start();
        
        $this->db->insert('users', $userData);

        $user_id = $this->db->insert_id();        

        $printryData['user_id'] = $user_id;
        $this->db->insert('printery_shop', $printryData);

        $this->db->insert_id();

        $this->db->trans_complete();
        return $user_id;
        
    }

    public function update_vendor_profile($userData,$printryData,$incharge_person,$mainprintery_id){
        $this->db->trans_start();
        
        $uid = $this->session->userdata['vendordata']['user_id'];
        $update_u = $this->db->update('users',$userData,array('id'=>$uid));

// echo $this->db->last_query();
// echo "<br>";
        $update_v = $this->db->update('printery_shop',$printryData,array('user_id'=>$uid,'branch'=>'0','delete_status'=>'0'));

// echo "<pre>"; print_r($printryData);
// echo $this->db->last_query();
// echo "<br>";
        $get_inc = $this->db->get_where('incharge_person',array('printery_id'=>$mainprintery_id,'status'=>'0'))->row_array();


// echo $this->db->last_query();
// echo "<br>";        
        if(empty($get_inc)){
            $incharge_person['printery_id']=$mainprintery_id;
            $incharge_person['created_at'] = date('Y-m-d h:i:s');
            
            $this->db->insert('incharge_person', $incharge_person);
        }else{
            $update_in = $this->db->update('incharge_person',$incharge_person,array('printery_id'=>$mainprintery_id,'status'=>'0'));
        }

        $branches = $userData['total_branches'];

        // $updateQuery = $this->generalmodel->updaterecord("printery_shop",array('delete_status'=>'1'),array('user_id'=>$uid,'branch'=>'1'));   

        // if(!empty($branches)){
        //     for($i=0;$i<$branches;$i++){
        //         $branch[$i]['user_id'] = $uid;
        //         $branch[$i]['branch'] = '1';
        //     }
        // $insert1 = $this->db->insert_batch('printery_shop', $branch);

        // }
//insert_batch
// echo $this->db->last_query();
// echo "<br>";
        $this->db->trans_complete();
        //return 1;
        return $update_v;
    }

    public function insert_branches($printryData,$incharge_person,$user_id){
        
        $this->db->trans_start();
        $updateQuery = $this->generalmodel->updaterecord("printery_shop",array('delete_status'=>'1'),array('user_id'=>$user_id,'branch !='=>'0'));   
        $insert1 = $this->db->insert_batch('printery_shop', $printryData);

        $first_id = $this->db->insert_id();

        $count = count($printryData);
        $last_id = $first_id + ($count-1);

        if(!empty($incharge_person)){
            foreach($incharge_person as $key=>$value){
                $incharge_person[$key]['printery_id'] = $first_id;

                $first_id++;
            }
        }
        $insert2 = $this->db->insert_batch('incharge_person', $incharge_person);

        $this->generalmodel->updaterecord("users",array('total_branches'=>$count),array('id'=>$user_id));   

        $this->db->trans_complete();
        return $insert2;        
    }

    public function myprofile($id=""){
        if(!empty($id)){
            $vendor_id = decoding($id);
        }else{
            $vendordata = $this->session->userdata['vendordata'];
            $vendor_id = $vendordata['user_id'];
        }
          
        $query = $this->db->select('u.id as user_id,u.reg_proof,logo,company_images,p.latitude,p.longitude,u.firstname as owner_fname,u.lastname as owner_lname,u.email as c_email,u.mobile as c_contact,u.total_branches,u.reg_number ,inc.fname as in_fname,inc.lname as in_lname,inc.email as in_email,inc.mobile as in_mobile,p.id as printery_id,p.shop_name,p.total_workers,p.building,p.po_box,p.start_time,p.end_time,p.services')
        ->from('users as u')
        ->join('printery_shop as p','u.id = p.user_id','left')
        ->join('incharge_person as inc','p.id = inc.printery_id','left')
        ->where("u.`id`=".$vendor_id." AND u.`delete_status`='0' AND p.`delete_status`='0' AND `branch`='0'")
        ->get()
        ->row_array();

        if(!empty($query)){
            $serviceNames = array();
            if(!empty($query['services'])){
                $serArr = explode(",",$query['services']);
                foreach ($serArr as $key => $value) {
                    
                    $serv_data = get_services_data($value);
                    if(!empty($serv_data)){
                        $serviceNames[]= $serv_data['name'];
                    }
                   // print_r($serv_data); exit;
                }
            }
            $query['serviceNames'] = implode(',',$serviceNames);

            $query['branches'] = $this->db->select('inc.fname as in_fname,inc.lname as in_lname,inc.email as in_email,inc.mobile as in_mobile,p.id as printery_id,p.shop_name,p.total_workers,p.building,p.po_box,p.start_time,p.end_time,p.services,p.contact,p.status')
            ->from('printery_shop as p')
            //->join('printery_shop as p','u.id = p.user_id','left')
            ->join('incharge_person as inc','p.id = inc.printery_id','left')
            ->where("p.user_id=".$vendor_id." AND p.`delete_status`='0' AND `branch`!='0'")
            ->get()
            ->result_array(); 

            if(!empty($query['branches'])){
                foreach($query['branches'] as $key=>$branch){
                    
                    $serviceNames = array();
                    if(!empty($branch['services'])){
                        $serArr = explode(",",$branch['services']);
                        foreach ($serArr as $value) {
                            
                            $serv_data = get_services_data($value);
                            if(!empty($serv_data)){
                                $serviceNames[]= $serv_data['name'];
                            }
                        }
                    }
                    $query['branches'][$key]['serviceNames'] = implode(',',$serviceNames);
                }
            }

            $query['bank_acc'] = $this->db->select('*')
            ->from('vendor_bank_account')
            ->where("vendor_id=".$vendor_id." AND `status`='0'")
            ->get()
            ->row_array(); 
        }
        return $query;
    }
    
    public function getorder($vendor_id){
        
          $query = "SELECT group_concat(`id`) as ids from printery_shop WHERE user_id= $vendor_id";
        return $this->db->query($query)->row_array();
    }
     public function get_completed_order($idven){
        
          $query = " SELECT * FROM `orders` WHERE payment_status ='1' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
     public function get_current_order($idven){
        $query = " SELECT * FROM `orders` WHERE progress_status !='delivered' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
     public function get_express_order($idven){
        
          $query = " SELECT * FROM `orders` WHERE service_type='2' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
     public function get_normal_order($idven){
        
          $query = " SELECT * FROM `orders` WHERE service_type='1' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
    public function get_pending_order_v($idven){
        
        $query = " SELECT * FROM `orders` WHERE progress_status !='delivered' AND progress_status !='saved' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
     public function get_completeds_order_v($idven){
        $query = " SELECT * FROM `orders` WHERE  payment_status ='1' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
    public function get_total_order_v($idven){
        $query = " SELECT * FROM `orders` WHERE progress_status !='saved' AND vendor IN ($idven)";
        return $this->db->query($query)->num_rows();
    }
}