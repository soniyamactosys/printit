<?php
if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Generalmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->userdata = $this->session->userdata('userdata');
    }

//=============curd operation=============

    public function add($tableName, $data) {

        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    public function add_product($productData,$stockData){
        $this->db->trans_start();
        $this->db->insert('products', $productData);
        $product_id = $this->db->insert_id();

        $stockData['product_id'] = $product_id;
        $this->db->insert('product_stock', $stockData);

        $this->db->trans_complete();
        return  $product_id ;
    }

    public function update_product($productData,$stockData,$pwhere){
        $this->db->trans_start();
        $update = $this->db->update('products',$productData,$pwhere);
         $this->db->insert('product_stock', $stockData);
        $this->db->trans_complete();
        return  $update;        
    }

    public function updaterecord($table,$data,$where){
    	return $this->db->update($table,$data,$where);
    }

    public function add_order_products($oid,$prodData,$stockData){
        $this->db->trans_start();

        $this->db->update('order_products',array('status'=>'1'),array('order_id'=>$oid));
        $insertquery = $this->db->insert_batch('order_products',$prodData);

        $product_total = get_prod_total($prodData);
        // echo "<pre>"; print_r($prodData); 
        // echo $product_total;
        // exit;

        foreach($stockData as $val){
            $qty = $val['qty'];
            $pid = $val['id'];
            $this->db->query("UPDATE `products` SET `qty` = `qty`-".$qty." WHERE `id`=".$pid);
        }

        $query = "UPDATE orders SET product_total=$product_total, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$oid;

        $this->db->query($query);        

        $this->db->trans_complete();
        return $insertquery;
    }

    public function update_order_delivery_address($address_id,$delivery_charge,$order_address){
        
        $this->db->trans_start();
        $addQuery = $this->generalmodel->add('order_address',$order_address);

        $query = "UPDATE orders SET pickup_delivery='2',delivery_address=$address_id,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$order_address['order_id'];

        $lastid = $this->db->query($query);
        $this->db->trans_complete();        
        return $addQuery;

    }
    public function add_new_order_address($customerAdd,$orderAdd,$delivery_charge){
        $this->db->trans_start();
        
        $address_id= $this->db->insert('customer_address',$customerAdd);
        
        $orderAdd['c_add_id'] = $address_id;
        $this->db->insert('order_address',$orderAdd);        


        $query = "UPDATE orders SET pickup_delivery='2',delivery_address=$address_id,delivery_charge=$delivery_charge, order_total = (`amount`+`delivery_charge`+`product_total`+admin_charge)-`applied_coupon_amt` WHERE `id`=".$orderAdd['order_id'];

        $lastid = $this->db->query($query);

        $this->db->trans_complete();
        return $address_id;
    }
    
    /*public function print_management_records($where=""){
        
        if(empty($where)){
            $where = array('pm.status !='=>'2');
        }
        $query =  $this->db->select('pm.id as pm_id,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.average_price,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,pm.banner_size,pm.service_id,pm.color,pm.binding')
        ->from('price_management as pm')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->where($where)
        ->get()
        ->result_array();
        // echo "<pre>";
        // echo $this->db->last_query();
        // print_r($query); exit;

        return $query;
        
    }*/
    public function price_combination_records($where=""){
        
        if(empty($where)){
            $where = array('pm.status !='=>'2');
        }
        $query =  $this->db->select('pm.id as pm_id,serv.name AS service_name,extra_copies,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,pm.banner_size,banner_size.name as banner_size_name,pm.service_id,pm.color,pm.binding')
        ->from('price_combination as pm')
        ->join('services as serv','serv.id = pm.service_id','left')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->join('banner_size','banner_size.id = pm.banner_size','left')
        ->where($where)
        ->get()
        ->result_array();
        return $query;
        
    }
    public function price_management_records($vendor_id="",$branchid="",$pm_id="",$return_type=""){
        
        // if(empty($where)){
        //     $where = array('pm.status !='=>'2');
        // }
        $where['ps.delete_status'] ='0';
        $where['ps.user_id']=$vendor_id;  
        if(!empty($branchid)){
            $where['ps.id']=$branchid;      
        }      
        if(!empty($pm_id)){
            $where['pm.id']=$pm_id;      
        }

        /*if(!empty($branchid)){
            $where = array('ps.delete_status'=>'0','ps.user_id'=>$vendor_id,'pm.id'=>$branchid);
        }else{
            $where = array('ps.delete_status'=>'0','ps.user_id'=>$vendor_id);
        }*/



        $data =  $this->db->select('ps.user_id,ps.id as printery_id,ps.branch,ps.shop_name,ps.services,pm.id as pm_id,pm.service_id,services.name as service_name,pm.paper_type_id,pm.paper_size_id,pm.print_type as print_type_id,pm.average_price,pm.status as pm_status,s.name as size,t.name as paper_type,pt.name as  print_type,pm.printing_sides,bs.name as width,pm.color,pm.binding')
        ->from('printery_shop as ps')
        ->join('print_management as pm','ps.id = pm.printery_shop_id','left')
        ->join('services','services.id = pm.service_id','left')
        ->join('paper_size as s','s.id = pm.paper_size_id','left')
        ->join('paper_type as t','t.id = pm.paper_type_id','left')
        ->join('print_type as pt','pt.id = pm.print_type','left')
        ->join('banner_size as bs','bs.id = pm.banner_size','left')
        ->where($where)
        ->get();

        /*if(!empty($branchid)){
            return $data->row_array();
        }else{
            return $data->result_array();
        }*/

        if(!empty($return_type)){
            if($return_type=='row'){
                return $data->row_array();
            }elseif($return_type=='result'){
             return $data->result_array();
            }
        }elseif(!empty($branchid)){
            return $data->row_array();
        }else{
             return $data->result_array();
        }
    }

    public function saved_orders(){
        $userdata = $this->session->userdata['userdata'];
        $userid = $userdata['user_id'];

        if(empty($where)){
            $where = array('o.customer'=>$userid,'status'=>'0');
        }
        return $this->db->select('o.id as order_id,o.project_name,o.updated_at,o.amount')
        ->from('orders as o')
        ->where($where)
        ->get()->result_array(); 


    }
    public function orders($status){
        $userdata = $this->session->userdata['userdata'];
        $userid = $userdata['user_id'];

        if(empty($where)){
            $where = array('o.customer'=>$userid,'o.status'=>$status);
        }
        return $this->db->select('o.id as order_id,o.order_type,o.project_name,o.updated_at,o.amount,pt.name as print_type')
        ->from('orders as o')
        ->join('print_type as pt',"pt.id = o.print_type AND pt.status='0'",'left')
        ->where($where)
        ->get()->result_array();         
    }

    public function orders_status($status="",$progress_status=""){
        $userdata = $this->session->userdata['userdata'];
        $userid = $userdata['user_id'];

        $where = "o.customer = ".$userid." AND o.order_type !='3' AND o.payment_status='1' AND pt.status='0' AND (o.progress_status !='delivered' OR o.progress_status IS NULL)";
        // $where['o.customer'] = $userid;
        // $where['pt.status']='0';
        // $where['o.progress_status !='] = 'delivered';
        // $where['o.progress_status OR IS'] = 'NULL';

        // if(!empty($progress_status)){
        //     $where['o.progress_status'] = $progress_status;
        // }

        $custom_quick =  $this->db->select('o.id as order_id,o.order_type,o.project_name,o.updated_at,o.amount,pt.name as print_type,o.status,o.progress_status')
        ->from('orders as o')
        ->join('print_type as pt','pt.id = o.print_type','left')
        ->where($where)
        ->order_by('order_id')
        ->get()
        ->result_array(); 

        $trans_where = "o.customer = ".$userid." AND o.order_type='3' AND o.progress_status NOT IN('delivered','saved') AND o.progress_status IS NOT NULL ";

        $translation =  $this->db->select('o.id as order_id,o.order_type,o.project_name,o.updated_at,o.amount,o.status,o.progress_status')
        ->from('orders as o')
        ->where($trans_where)
        ->order_by('order_id')
        ->get()
        ->result_array();     

        $merge = array_merge($custom_quick,$translation);

// echo $this->db->last_query();
// echo "<pre>"; print_r($translation); exit;

        usort($merge, function($a, $b) {
            return $a['order_id'] - $b['order_id'];
        });

        return  $merge;          
    }

    public function order_detail($oid,$status=""){

        $where['orders.id'] = $oid;

        $otype_query = $this->db->select('id,service_type,order_type')
        ->from('orders')->where($where)->get()->row_array();

        // echo $this->db->last_query();
        // print_r($otype_query);
        // exit;
        // $order_type = $otype_query['order_type'];

        if(!empty($status)){
            $where['orders.status'] = "$status";
        }

        if($otype_query['order_type']==3){

            $tables[0]['table'] = 'users as u';
            $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

            $tables[1]['table'] = 'translation_lang as frmlang';
            $tables[1]['on'] = 'orders.lang_from = frmlang.id';

            $tables[2]['table'] = 'translation_lang as tolang';
            $tables[2]['on'] = 'orders.lang_to = tolang.id';

            $tables[3]['table'] = 'translation_types as trans_type';
            $tables[3]['on'] = 'orders.trans_type_id = trans_type.id';


            $query =   $this->getfrommultipletables("orders.*,trans_type.name as trans_type,frmlang.name as from,tolang.name as to,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,orders.created_at as order_date,orders.id as order_number",'orders',$tables,$where,"","orders.id","","","row_array");             

        }else{        

            $tables[0]['table'] = 'users as u';
            $tables[0]['on'] = 'u.id = orders.customer AND u.role="3" AND u.delete_status="0"';

            $tables[1]['table'] = 'printery_shop as ps';
            $tables[1]['on'] = 'ps.id = orders.vendor AND ps.status="1" AND ps.delete_status="0"';

            $tables[2]['table'] = 'paper_type as ptype';
            $tables[2]['on'] = 'orders.paper_type = ptype.id AND ptype.status!="2"';

            $tables[3]['table'] = 'paper_size as psize';
            $tables[3]['on'] = 'orders.paper_size = psize.id AND psize.status!="2"';

            $tables[4]['table'] = 'order_address as o_add';
            $tables[4]['on'] = 'o_add.order_id = orders.id AND orders.pickup_delivery="2"';
            
            $tables[5]['table'] = ' banner_size as bn_size';
            $tables[5]['on'] = 'orders.width = bn_size.id AND bn_size.status!="2"';

            $query =   $this->getfrommultipletables("*,bn_size.name as bannersize,CONCAT_WS(' ',u.firstname,u.lastname) AS customer_name,orders.created_at as order_date,orders.id as order_number,psize.name AS papersize,ptype.name as papertype,ps.user_id as user_id,ps.branch,ps.id as branch_id,ps.contact as p_contact,ps.shop_name AS vendor_name,ps.start_time,ps.end_time,ps.latitude as p_lat,ps.longitude as p_lng,ps.building as p_address,o_add.area as d_area,o_add.address as d_address,o_add.mobile as d_mobile,orders.delivery_address,orders.pickup_delivery  ",'orders',$tables,$where,"","orders.id","","","row_array");  

            $query['products'] = $this->db->select('op.prod_id,p.name,op.prod_price as price,op.prod_qty as qty,op.prod_price,op.prod_qty')
            ->from('order_products as op')
            ->join('products as p','op.order_id = p.id AND op.status="0"','LEFT')
            ->where("op.order_id=".$oid." AND op.status='0'")
            ->get()
            ->result_array();
        }

// echo $this->db->last_query();
// echo "<pre>";
// print_r($query); 
// print_r($order_products); exit;
        return $query;
    }

    public function insert_batchdata($table,$data){
        $str = $this->db->insert_batch($table, $data);
        if(!empty($str)){
            return true;
        }else{
            return false;
        }
    }
    public function deleterecord($table,$where){
        //return $this->db->delete($table,$where);
        return $this->db->update($table,array('delete_status'=>'1'),$where);
    }

    public function customquery($queryy,$returnType=""){
        $query = $this->db->query("$queryy");
        //print_r($query); exit;
        //echo $query->num_rows(); exit;
        if(!is_bool($query)){
            if(empty($returnType) || $returnType=="result_array"){
                $return =  $query->result_array();
            }elseif($returnType=="result"){
                $return =  $query->result();
            }elseif($returnType=="row_array"){
                $return =  $query->row_array();
            }elseif($returnType=="row"){
                $return =  $query->row();
            }      
        }else{
            $return  = $query;
        }
        return $return;
    }

    public function getparticularData($select="*",$table,$where="",$returnType="",$limit="",$start="",$orderby=""){
        
        $this->db->select($select);
        $this->db->from($table);
        if($where!=""){
            $this->db->where($where);
        }

        if($limit!="" && $start!="")
        {
            $this->db->limit($limit,$start);
        }
        else if($limit!="")
        {
            $this->db->limit($limit);
        }

        if($orderby!="")
        {
            $this->db->order_by($orderby);
        }

        $query = $this->db->get();

        if($query !== FALSE && $query->num_rows() > 0){

        if(empty($returnType) || $returnType=="result_array"){
            $return =  $query->result_array();
        }elseif($returnType=="result"){
            $return =  $query->result();
        }elseif($returnType=="row_array"){
            $return =  $query->row_array();
        }elseif($returnType=="row"){
            $return =  $query->row();
        }      
        }else{
            $return =array();
        }
        return $return;
    }

    public function getfrommultipletables($data="*",$maintable, $tables,$where="",$groupby="",$orderby="",$limit="",$start="",$returnType="",$having=""){
        $return = array();
        $this->db->select($data);
        $this->db->from($maintable);

        foreach($tables as $value){
            $table = $value['table'];
            $on = $value['on'];
            $this->db->join($table,$on,'LEFT');
        }
      
        if($where !=""){
            $this->db->where($where);
        }

        if($limit!="" && $start!="")
        {
            $this->db->limit($limit,$start);
        }
        else if($limit!="")
        {
            $this->db->limit($limit);
        }
      
        if($groupby!="")
        {
            $this->db->group_by($groupby); 
        }
        
        if($orderby!="")
        {
            $this->db->order_by($orderby); 
        }
        if(!empty($having)){
             $this->db->having($having); 
        }
        
      $query = $this->db->get();


// echo "<pre>";
// print_r($query);
// print_r($data);
// print_r($maintable);
// print_r($tables);print_r($where);
// print_r($returnType);
// exit;

      //echo $this->db->last_query();
      if($query->num_rows() > 0){

        if(empty($returnType) || $returnType=="result_array"){
            $return =  $query->result_array();

        }elseif($returnType=="result"){
            $return =  $query->result();
        }elseif($returnType=="row_array"){
            $return =  $query->row_array();
        }elseif($returnType=="row"){
            $return =  $query->row();
        }       
      } 
      return $return; 
    }

    public function modify($tableName, $colName, $id, $data) {

        $this->db->where($colName, $id);

        $this->db->update($tableName, $data);
        return $this->db->affected_rows();
    }    

    public function modifyMulti($tableName, $data1, $data) {

        $this->db->where($data1);

        $this->db->update($tableName, $data);
        return $this->db->affected_rows();
    }
    public function delete($tableName, $colName, $id) {

        $this->db->where($colName, $id);

        $this->db->delete($tableName);

        return TRUE;
    }

    public function deleteMulti($tableName, $wh) {

        $this->db->where($wh);

        $this->db->delete($tableName);

        return TRUE;
    }
    //=============curd operation=============


    public function mail_template($select,$type){
        return $this->getparticulardata($select,'email_template',array('type'=>$type,'status'=>'1'),'row_array');
    }

    // To get single row of table by a id 

public function getSingleRowById($tableName, $colName, $id, $returnType = '') {

    $this->db->where($colName, $id);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0) {

        if ($returnType == 'array')

            return $result->row_array();

        else

            return $result->row();
    }

    else

        return FALSE;

}



    // To get all row of matching criteria

public function getRowAllById($tableName, $colName, $id, $orderby = '', $orderformat = 'asc') {

    if ($colName != '' && $id != '')

        $this->db->where($colName, $id);

    if ($orderby != '')

        $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0)

        return $result->result();

    else

        return FALSE;

}



    // To get data by multiple where 	

public function getRowByWhere($tableName, $filters = '', $select = '', $noRowReturn = '', $returnType = '', $orderby = '', $orderformat = 'asc') {

    if ($select != '')

        $this->db->select($select);

    if (count($filters) > 0) {

        foreach ($filters as $field => $value)

            $this->db->where($field, $value);

    }
    if ($orderby != '')

        $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0) {

        if ($noRowReturn == 'single') {

            if ($returnType == 'array')

                return $result->row_array();

            else

                return $result->row();
        }

        else {
            if ($returnType == 'array')
                return $result->result_array();
            else
                return $result->result();
        }

    }
    else
        return FALSE;

}
 
    // Pagination function 

public function getPaginationData($tableName, $filters = '', $perPage, $start, $orderby, $orderformat,$selectData=''){
 
    //Set default order
    if($selectData!='') 
    {
    $this->db->select($selectData);
    }
    if ($orderformat == '')

    $orderformat = 'asc';

    //add where clause

    if ($filters != '' && count($filters) > 0)

    $this->db->where($filters);

    $this->db->limit($perPage, $start);

    $this->db->order_by($orderby, $orderformat);

    $result = $this->db->get($tableName);

    if ($result->num_rows() > 0)

    return $result->result();

    else

    return FALSE;
}

/*---get--pagination---*/

public function get_pagination_result($table_name='',$id_array='',$limit='',$offset='',$orderid,$order='')
{     
       
    if(!empty($id_array)):

    foreach ($id_array as $key => $value){
    $this->db->where($key, $value);

    }
    endif;

    if($order!=''){
    $this->db->order_by($orderid,$order); 
    }
    else{
    $this->db->order_by('id','desc'); 
    } 

    if($limit > 0 && $offset>=0){ 

    //$this->db->where($filters);
    $this->db->limit($limit, $offset);

    $query=$this->db->get($table_name);

    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;

    }else{
    $query=$this->db->get($table_name);
    return $query->num_rows();
    }
}


    //Function to return total number of rows

public function getTotalRows($tableName, $filters = NULL) {

    if ($filters != NULL) {

    $this->db->where($filters);

    $count = $this->db->count_all_results($tableName);

    }

    else

    $count = $this->db->count_all($tableName);

    return $count;
}
 
public function get_result($table_name='', $id_array='',$id_array2=''){
        
        if(!empty($id_array)):      
            foreach ($id_array as $key => $value){
                $this->db->where($key, $value);
             
            }
       endif;
        if(!empty($id_array2)):     
            foreach ($id_array2 as $key => $value){
                $this->db->or_where($key, $value);
            }
        endif;
        $query=$this->db->get($table_name);
        if($query->num_rows()>0)
            return $query->result();
        else
            return FALSE;
}
 
public function get_row($table_name='', $id_array='', $id_array2=''){
	if(!empty($id_array)):      
		foreach ($id_array as $key => $value){
			$this->db->where($key, $value);
		}
	endif;
	if(!empty($id_array2)):     
		foreach ($id_array2 as $key => $value){
			$this->db->or_where($key, $value);
		}
	endif;
	$query=$this->db->get($table_name);
	if($query->num_rows()>0) 
		return $query->row();
	else
		return FALSE;
}

public function get_resultbylimit($table_name='',$limit="",$id_array='',$id_array2=''){
    $this->db->limit($limit);
    if(!empty($id_array)):      
    foreach ($id_array as $key => $value){
    $this->db->where($key, $value);

    }
    endif;
    if(!empty($id_array2)):     
    foreach ($id_array2 as $key => $value){
    $this->db->or_where($key, $value);
    }
    endif;
    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Or operator =================------

public function getOrResult($table_name='',$id_array=''){

    if(!empty($id_array)):     

    $this->db->or_where($id_array);
    endif;
    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Using Clause =================------

public function getInClauseData($select='',$table_name='',$colName='',$arr='',$where=''){

    if ($select != '')
    $this->db->select($select);

    if(count($where)>0)
    $this->db->where($where);

    $this->db->where_in($colName, $arr);

    $query=$this->db->get($table_name);
    if($query->num_rows()>0)
    return $query->result();
    else
    return FALSE;
}

//---------========== Get Data Using Join=================------

public function getJoinData($seldata,$table1='',$table2='',$join_condition='',$wh='',$orderby='id',$orderformat='asc',$resultType="",$limit="",$start=""){

    $this->db->select($seldata);

    $this->db->from($table1);

    $this->db->join($table2,$join_condition);
	if(!empty($wh))
    $this->db->where($wh);

    $this->db->order_by($orderby, $orderformat);



    if($limit!="" && $start!="")
    {
        $this->db->limit($limit,$start);
    }
    else if($limit!="")
    {
        $this->db->limit($limit);
    }
        
    $query = $this->db->get();
    
    if(empty($resultType)){
        $query->result();
    }else{
        if($resultType=='result_array'){
           return $query->result_array();
        }elseif($resultType=='row_array'){
            return $query->row_array();
        }elseif($resultType=='row'){
            return $query->row();
        }
    }
}

public function customjoin($seldata,$table1='',$table2='',$table3='',$table4='',$join_condition='',$wh=''){

    $this->db->select($seldata);

    $this->db->from($table1,$table2,$table3,$table4);
    
    if(!empty($join_condition))
    $this->db->where($join_condition);

    $query = $this->db->get();
    

    return $query->row_array();

}

public function getsingleJoinData($seldata,$table1='',$table2='',$join_condition='',$wh=''){

    $this->db->select($seldata);

    $this->db->from($table1);

    $this->db->join($table2,$join_condition);
    if(!empty($wh))
    $this->db->where($wh);

  
        
    $query = $this->db->get();
    

    return $query->row_array();

}

/* End of file generalmodel.php */
public function get_all_record($table)
{
    $query = $this->db->get($table);
    if ($query->num_rows() > 0) 
    {
        return $query->result_array();
    } 
    else 
    {
        return array();
    }
}

public function get_data_by_condition($data,$table,$condition){
    $this->db->select($data);
    $query=$this->db->from($table);
    $query=$this->db->where($condition);
    $query=$this->db->get();
    //echo $this->db->last_query(); die();
    return $query->result_array();
    //$this->db->close();
} 

public function threetables($data,$table1,$table2,$table3,$on,$on2,$condition,$where)
{
    $this->db->select($data);
    $this->db->from($table1);
    $this->db->join($table2,$on);
    $this->db->join($table3,$on2);
    $this->db->where($condition);
    if($where!="")
    {
        $this->db->where($where);
    }
    $query=$this->db->get();
    //return $this->db->last_query();
    return $query->result_array();
}

public function threetablesall($data,$table1,$table2,$table3,$on,$on2,$where)
{
    $this->db->select($data);
    $this->db->from($table1);
    $this->db->join($table2,$on);
    $this->db->join($table3,$on2);
   if($where!="")
    {
        $this->db->where($where);
    }
    $query=$this->db->get();
    //return $this->db->last_query();
    return $query->result_array();
}

//==============project functions===========




    public function check_user_exist($email,$id=''){
        $where = "`email`='$email'";
        if(!empty($id)){ $where .= " AND `user_id` !=$id"; }
        return $this->db->select('email')->from('user')->where($where)->get()->row_array();
    }

   
    public function consumer_products($cid){

        $where = array('orders.createdby'=>$cid);
        $this->db->select('GROUP_CONCAT(DISTINCT ord_prod_id SEPARATOR ",") AS pro_id'); 
        $this->db->from('orders');
        $this->db->join('orders_product','orders.orders_id = orders_product.orders_id','LEFT');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array(); 
    }

  
    
    //==================printeryshop_list=======================
    function printeryshop_list($where=""){
        
        if(empty($where)){
            $where = "p.status='0' AND p.delete_status='0'   ";
        } 
        $query = $this->db->select('p.id as id,users.id as user_id,users.*,p.*,p.status as shop_status')
        ->from('printery_shop as p')
        ->join('users',"users.id=p.user_id AND users.role='2' AND users.delete_status='0' AND `users`.`status`='0'",'LEFT')
        ->where($where)
        ->order_by("p.shop_name","ASC")
        ->get()->result_array();
        return $query;
        

  /*      if(empty($where)){
            $where = "pm.status='0'";
        } 
        $query = $this->db->select('pm.average_price
            ,users.*,p.*,p.status as shop_status,p.id as id,users.id as user_id')
        ->from('print_management as pm')
        ->join('printery_shop as p',"pm.printery_shop_id=p.id AND p.status=0 AND p.delete_status='0'",'LEFT')
        ->join('users',"users.id=p.user_id AND users.role='2' AND users.delete_status='0' AND `users`.`status`='0'",'LEFT')
        ->where($where)
        ->order_by("p.shop_name","ASC")
        ->get()->result_array();
        return $query;*/

        
    }

    public function get_avg_price($where){
       // echo $where; 
        $query = $this->db->select('*')->from('print_management')->where($where)->get()->row_array();
       // print_r($query);exit;
               //print_r($this->db->last_query());
       // echo "<pre>"; print_r($order_detail); exit;
      //echo "<pre>"; print_r($copy_priceArr); 
       return  $query;
    }

    public function get_price_range($where){
        //echo $where;exit;
        $query = $this->db->select('*')->from('price_range')->where($where)->get()->row_array();

       //         print_r($this->db->last_query());
       // echo "<pre>"; print_r($order_detail); exit;
        return  $query;
    }
    
    //==================printeryshop_list=======================
    function printeryshop_count($where=""){
        $query= $this->db->select('count(`users`.`id`) AS total')
        ->from('printery_shop as p')
        ->join('users','users.id=p.user_id','LEFT')
        ->where("users.role='2' AND users.delete_status='0' AND p.delete_status='0'");
        
        if(!empty($where)){
            $query->where($where);
        }
        return $query->get()->row_array();
    }
    
    
    function vendor_detail($id){
        $query= $this->db->select('users.*,p.*,p.status as shop_status')
        ->from('printery_shop as p')
        ->join('users','users.id=p.user_id','LEFT')
        ->where("users.role='2' AND users.delete_status='0' AND `users.id`=$id");
        
        if(!empty($where)){
            $query->where($where);
        }
        return $query->get()->row_array();
    }

    function get_services($status){
        return $this->db->select('*')->from('services')->where(array('status'=>$status))->get()->result_array();
    }

    function branch_data($id){
        return $this->db->select('p.id as printery_id,p.*,pm.*')
        ->from('printery_shop as p')
        ->join('print_management as pm',"p.id = pm.printery_shop_id AND pm.status='0'",'LEFT')
        ->where("`p`.`id`=$id AND `p`.delete_status='0'")
        ->get()->row_array();        
    }

    function notes_categories($status){

        $where = "`status` !='2'";
        $query = $this->db->select('*')->from('notes_categories as nc');
        if($status!=''){
            $where .=" AND `status`='$status'";
        }
        $query->where($where);

        return $query->get()->result_array();
    }

    function admin_notes($status,$catid=""){
        $query = $this->db->select('notes.*,nc.name as cat')
        ->from('notes')
        ->join('notes_categories as nc',"notes.category = nc.id",'LEFT')
        ->where("`notes`.`is_delete`='0'");

        if($status !=''){
            $query->where("`notes`.`status`='$status' AND nc.status='0'") ;
        }
        if($catid !='' && $catid >0){
            $query->where("`nc`.`id`='$catid'") ;
        }        
        return $query->get()->result_array(); 
    }

    function note_detail($id){
        $query = $this->db->select('notes.*,nc.name as cat')
        ->from('notes')
        ->join('notes_categories as nc',"notes.category = nc.id",'LEFT')
        ->where("`notes`.`is_delete`='0' AND `notes`.`id`=$id");
        return $query->get()->row_array();        
    }

public function get_nearby_vendor($lat,$long){
        
         $query = "SELECT printery_shop.id , (3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - printery_shop.latitude) * pi()/180 / 2), 2) +COS( ".$lat." * pi()/180) * COS(printery_shop.latitude * pi()/180) * POWER(SIN(( ".$long." - printery_shop.longitude) * pi()/180 / 2), 2) ))) as distance from users inner join printery_shop on users.id= printery_shop.user_id having distance <= 2 order by distance";

        return $this->db->query($query)->result_array();
       //$query = $this->db->get();
       
       
       
    }
    

}


/* Location: ./application/models/generalmodel.php */