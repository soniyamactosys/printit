<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'Home';
$route['vendor_registeration'] = 'Authorize/registeration';
$route['vendorlogin'] = 'Authorize/vendor_login';
$route['vendor_logout'] = 'Vendor/vendor_logout';




$route['adminlogin'] = 'Authorize/admin_login';
$route['printery_list'] = 'Admin/printery_list';
$route['printery_list_approved'] = 'Admin/printery_list_approved';
$route['printery_list_rejected'] = 'Admin/printery_list_rejected';
$route['add_membership'] = 'Admin/add_membership';
$route['membership'] = 'Admin/membership';
$route['edit_membership/(.+)'] = 'Admin/edit_membership/$1';
//$route['vendor_detail/:num'] = 'Admin/vendor_detail/$1';
$route['vendor_detail/(.+)']= 'Admin/vendor_detail/$1';

//$route['printery_list/(.+)'] = 'Admin/printery_list/$1';
$route['filter_printeryshop'] = 'Admin/filter_printeryshop';
$route['paper_type_setting'] = 'Admin/paper_type_setting';
$route['print_type'] = 'Admin/print_type';
$route['paper_type_setting'] = 'Admin/paper_type_setting';
$route['paper_size_setting'] = 'Admin/paper_size_setting';
$route['extra_copies'] = 'Admin/extra_copies';
$route['print_management'] = 'Admin/print_management';


//=========web====
$route['logout'] = 'Dashboard/logout';
$route['userlogout'] = 'User/userlogout';
$route['customprint'] = 'Home/custom_print';
$route['poster'] = 'Home/poster';
$route['translation'] = 'Home/translation';
$route['notes'] = 'Home/notes';
$route['note_detail/(.+)'] = 'Home/note_detail/$1';
$route['banner'] = 'Home/banner';
$route['flyer'] = 'Home/flyer';
$route['rollup'] = 'Home/rollup';
$route['quickprint'] = 'Home/quickprint';
$route['reset_password'] = 'Home/reset_password';
$route['select_service_type'] = 'Home/select_service_type';
$route['printery_shops'] = 'Home/printery_shops';
$route['set_service_type/(.+)'] = 'Home/set_service_type/$1';
$route['save_project'] = 'User/save_project';
$route['pickup_delivery'] = 'User/pickup_delivery';
$route['delivery'] = 'User/delivery';
$route['print-type/(.+)'] = 'Home/print_type/$1';
$route['payment'] = 'User/payment';
$route['order_confirmed'] = 'User/order_confirmed';
$route['checkout_failed'] = 'User/checkout_failed';
$route['myorders'] = 'User/myorders';
$route['continue_order'] = 'User/continue_order';
$route['order_detail/(.+)'] = 'User/order_detail/$1';
//$route['order-detail/(.+)'] = 'home/order_detail/$1';
$route['myprofile'] = 'User/myprofile';
$route['trans_offer_sent'] = 'User/trans_offer_sent';
$route['show_translation_proposals/(.+)'] = 'User/show_translation_proposals/$1';
$route['setting'] = 'User/setting';



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
