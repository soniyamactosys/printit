 <?php 
$config = array(
        'login_validation' => array(
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email'
                ),
                array(
                        'field' => 'pass',
                        'label' => 'Password',
                        'rules' => 'required|trim|xss_clean'
                ),

        ),'add_paper_type' => array(
                array(
                        'field' => 'name',
                        'label' => 'Paper Type',
                        'rules' => 'required|trim|xss_clean'
                ),
        ),'add_print_type' => array(
                array(
                        'field' => 'name',
                        'label' => 'Paper Type',
                        'rules' => 'required|trim|xss_clean'
                ),
        ),'add_paper_size' => array(
                array(
                        'field' => 'name',
                        'label' => 'Paper Size',
                        'rules' => 'required|trim|xss_clean'
                ),
        ),'add_banner_size' => array(
                array(
                        'field' => 'name',
                        'label' => 'Banner Size',
                        'rules' => 'required|trim|xss_clean'
                ),
        ),'add_coupon' => array(
                array(
                        'field' => 'code',
                        'label' => 'Coupon Code',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'users',
                        'label' => 'Users',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'coupon_type',
                        'label' => 'Coupon Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'start_date',
                        'label' => 'Start Date',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'end_date',
                        'label' => 'End Date',
                        'rules' => 'required|trim|xss_clean'
                ),
        )
        ,'add_membership' => array(
                array(
                        'field' => 'amount',
                        'label' => 'Membership Amount',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'title',
                        'label' => 'Title',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'period',
                        'label' => 'Period',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'description',
                        'label' => 'Description',
                        'rules' => 'required|trim|xss_clean'
                )
                //,array(
                //         'field' => 'features',
                //         'label' => 'Features',
                //         'rules' => 'required|trim|xss_clean'
                // ),
        ),'add_commission' => array(
                array(
                        'field' => 'commission_percent',
                        'label' => 'commission Percent',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'description',
                        'label' => 'Description',
                        'rules' => 'required|trim|xss_clean'
                )
        ),
        'add_print_mangement' => array(
                array(
                        'field' => 'order_type',
                        'label' => 'Order Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'paper_type',
                        'label' => 'Paper Type',
                        'rules' => 'required|trim|xss_clean'
                ),
                // array(
                //         'field' => 'avg_price',
                //         'label' => 'Average price',
                //         'rules' => 'required|trim|xss_clean'
                // )
                // ,array(
                //         'field' => 'paper_size',
                //         'label' => 'Print Size',
                //         'rules' => 'required|trim|xss_clean'
                // )
        ),'add_price_mangement' => array(
                array(
                        'field' => 'branch_id',
                        'label' => 'Branch',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'service',
                        'label' => 'Service',
                        'rules' => 'required|trim|xss_clean'
                ),
                // array(
                //         'field' => 'paper_type',
                //         'label' => 'Paper Type',
                //         'rules' => 'required|trim|xss_clean'
                // ),
                array(
                        'field' => 'avg_price',
                        'label' => 'Average price',
                        'rules' => 'required|trim|xss_clean'
                )
        ),
        
        'user_registeration' => array(
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email',
                ),array(
                        'field' => 'f_name',
                        'label' => 'First Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'l_name',
                        'label' => 'Last Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'mobile',
                        'label' => 'Mobile Number',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'address',
                        'label' => 'Address',
                        'rules' => 'required|trim|xss_clean'
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required|trim|xss_clean|min_length[6]|max_length[128]'
                ),array(
                        'field' => 'cpassword',
                        'label' => 'Confirm Password',
                        'rules' => 'required|trim|xss_clean|min_length[6]|max_length[128]'
                ),array(
                        'field' => 'gender',
                        'label' => 'Gender',
                        'rules' => 'required|trim|xss_clean'
                )

        ),
        'app_user_registeration' => array(
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email',
                ),array(
                        'field' => 'f_name',
                        'label' => 'First Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'l_name',
                        'label' => 'Last Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'mobile',
                        'label' => 'Mobile Number',
                        'rules' => 'required|trim|xss_clean'
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required|trim|xss_clean|min_length[6]|max_length[128]'
                )

        ),

        'user_login' => array(
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email',
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required|trim|xss_clean'
                )
        ),
        'vendor_registeration' => array(
                // array(
                //         'field' => 'firstname',
                //         'label' => 'Name',
                //         'rules' => 'required|trim|xss_clean'
                // ),
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email',
                ),array(
                        'field' => 'address',
                        'label' => 'Address',
                        'rules' => 'required|trim|xss_clean',
                ),array(
                        'field' => 'contact_no',
                        'label' => 'Contact Number',
                        'rules' => 'required|trim|xss_clean',
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required|trim|xss_clean|min_length[6]|max_length[128]'
                ),array(
                        'field' => 'c_password',
                        'label' => 'Confirm Password',
                        'rules' => 'required|trim|xss_clean|min_length[6]|max_length[128]'
                ),
        ),
        'add_branch'=> array(
                array(
                        'field' => 'shop_name[]',
                        'label' => 'Shop Name',
                        'rules' => 'required|trim|xss_clean'
                ),
                array(
                        'field' => 'address[]',
                        'label' => 'Address',
                        'rules' => 'required|trim|xss_clean',
                ),array(
                        'field' => 'mobile[]',
                        'label' => 'Contact Number',
                        'rules' => 'required|trim|xss_clean',
                ),array(
                        'field' => 'worker_count[]',
                        'label' => 'Total Workers',
                        'rules' => 'required|trim|xss_clean',
                ),array(
                        'field' => 'start_time[]',
                        'label' => 'Start Time',
                        'rules' => 'required|trim|xss_clean',
                ),array(
                        'field' => 'end_time[]',
                        'label' => 'End Time',
                        'rules' => 'required|trim|xss_clean',
                )
        ),
        'add_product' => array(
                array(
                        'field' => 'name',
                        'label' => 'Product Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'price',
                        'label' => 'Price',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'qty',
                        'label' => 'Quantity',
                        'rules' => 'required|trim|xss_clean|is_natural_no_zero'
                )
        ),'add_service' => array(
                array(
                        'field' => 'name',
                        'label' => 'Service',
                        'rules' => 'required|trim|xss_clean'
                ), array(
                        'field' => 'description',
                        'label' => 'Description',
                        'rules' => 'required|trim|xss_clean'
                ),
        ),'add_bank_acc' => array(
                array(
                        'field' => 'acc_holder',
                        'label' => 'Account Holder',
                        'rules' => 'required|trim|xss_clean'
                ), array(
                        'field' => 'bank_name',
                        'label' => 'Bank Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'branch_name',
                        'label' => 'Branch Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'acc_number',
                        'label' => 'Account Number',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'ifsc_code',
                        'label' => 'IFSC Number',
                        'rules' => 'required|trim|xss_clean'
                )
        ),'api_custom_print_step1' => array(
                array(
                        'field' => 'order_type',
                        'label' => 'Order Type',
                        'rules' => 'required|trim|xss_clean'
                ), array(
                        'field' => 'user_id',
                        'label' => 'User',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'project_name',
                        'label' => 'Project Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'print_type',
                        'label' => 'Print Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'paper_type',
                        'label' => 'Paper Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'copy_number',
                        'label' => 'Number of copies',
                        'rules' => 'required|trim|xss_clean'
                )
                
        ),'api_quick_print_step1' => array(
                array(
                        'field' => 'order_type',
                        'label' => 'Order Type',
                        'rules' => 'required|trim|xss_clean'
                ), array(
                        'field' => 'user_id',
                        'label' => 'User',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'project_name',
                        'label' => 'Project Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'paper_type',
                        'label' => 'Paper Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'paper_size',
                        'label' => 'Paper Size',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'copy_number',
                        'label' => 'Number of copies',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'color',
                        'label' => 'Color',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'binding',
                        'label' => 'Binding',
                        'rules' => 'required|trim|xss_clean'
                )
        ),'api_translation' => array(
                array(
                        'field' => 'user_id',
                        'label' => 'User',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'project_name',
                        'label' => 'Project Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'trans_type',
                        'label' => 'Translation Type',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'lang_from',
                        'label' => 'Language From',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'lang_to',
                        'label' => 'Language To',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'print_copy_type',
                        'label' => 'Print Type',
                        'rules' => 'required|trim|xss_clean'
                )
        ),
        'profile_validate'=>array(
                array(
                        'field' => 'email',
                        'label' => 'Email address',
                        'rules' => 'required|trim|xss_clean|valid_email',
                ),
                array(
                        'field' => 'f_name',
                        'label' => 'First Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'l_name',
                        'label' => 'Last Name',
                        'rules' => 'required|trim|xss_clean'
                ),array(
                        'field' => 'mobile',
                        'label' => 'Mobile Number',
                        'rules' => 'required|trim|xss_clean'
                ),
                array(
                        'field' => 'gender',
                        'label' => 'Gender',
                        'rules' => 'required|trim|xss_clean'
                )
                // ,array(
                //         'field' => 'address',
                //         'label' => 'Address',
                //         'rules' => 'required|trim|xss_clean'
                // )

        )
        
)
?>        