<?php
function encrypt_pass($inputpassword){

	$cryptKey =  '!@#$&*qJB0r7354000021xG03efyCp!@#$&*85';
	$method = "AES-256-CBC";
	$iv = md5(md5($cryptKey));
	$password = openssl_encrypt($inputpassword, $method, md5($cryptKey), 0, hex2bin($iv));
	return $password;
       
}


function destroy_order_in_session(){
    $ci = get_instance();
    $sessarr[]='image_rotation';
    $sessarr[]='img_original_name';
    $sessarr[]='vendor_charge';
    $ci->session->unset_userdata($sessarr);
    $ci->session->unset_userdata(array('color','delivery_fee','custom_print_option','order_type','cartdata','paper_type','copy_number','paper_size','printing_side','uploaded_image','print_type','selected_printry','current_order','pickup_delivery','banner_width','cart_data','coupon_applied','order_products','continue_order','header_placeholder','page_range','notes_page_from','notes_page_to','order_confirmed','post_products','selected_add','print_copy_type','lang_from_id','lang_to_id','trans_type_id','trans_doc'));
}

function format_currency($amt){
    $fmt = numfmt_create('Asia/Kuwait', NumberFormatter::CURRENCY );
   // return str_replace("KWD","",numfmt_format_currency($fmt,$amt, "KWD"))." KD";
   return number_format($amt, 2)." KD";
}

function admin_service_fee($type){
    $ci = get_instance();
    $data = $ci->db->select('charge')->from('admin_service_fee')->where("`status`='0' AND `type`='$type'")->get()->row_array();
    if(!empty($data)){
        $ci->session->set_userdata('service_fee',$data['charge']);
        $sf=$data['charge'];
    }else{
        $ci->session->set_userdata('service_fee',0);
        $sf=0;
    }
    return $sf;
}


function get_delivery_charges($distance=""){
    $ci = get_instance();
    $fee = 30;    
    $ci->session->set_userdata('delivery_fee',$fee);
    return $fee;
}

function admin_detail(){
    $ci = get_instance();
    return $ci->db->select('id,firstname,lastname,email,mobile')->from('users')->where("`role`='1' AND `status`='0' AND `delete_status`='0'")->get()->row_array();
}


function user_detail_byemail($email="",$id=""){
    $ci = get_instance();
    $where = "`role`='3' AND `status`='0' AND `delete_status`='0'";
    if(!empty($id)){
        $where .= " AND `id`='".$id."' ";
    }
    if(!empty($email)){
        $where .= " AND `email`='".$email."' ";
    }
    return $ci->db->select('firstname,lastname,email,mobile')->from('users')->where($where)->get()->row_array();
}


function vendor_detail($id){
    $ci = get_instance();
    $where = "u.`role`='2' AND u.`status`='0' AND u.`delete_status`='0'";
    if(!empty($id)){
        $where .= " AND `ps.id`='".$id."' ";
    }
    
    return $ci->db->select('ps.shop_name,ps.building,u.email,u.mobile')
    ->from('printery_shop as ps')
    ->join('users as u','u.id=ps.user_id','left')
    ->where($where)->get()
    ->row_array();
}


function incharge_detail($id){
    $ci = get_instance();
    $where = "ip.`status`='0' AND `ps.id`='".$id."' ";

    
    return $ci->db->select('ip.fname,ip.lname,ip.mobile,ip.email,ps.shop_name,ps.building')
    ->from('printery_shop as ps')
    ->join('incharge_person as ip','ip.printery_id=ps.id','left')
    ->where($where)->get()
    ->row_array();
}
/*
function delivery_fee($distance){
    $ci = get_instance();
    $ci->session->set_userdata('delivery_fee',100);
}
*/

function update_order_steps($oid,$url){
    $ci = get_instance();
    $data = $ci->db->select('all_steps')->from('orders')->where("`id`=$oid")->get()->row_array();
    if(!empty($data['all_steps'])){
      $all_steps = unserialize($data['all_steps']);
      if(!in_array($url,$all_steps)){
        array_push($all_steps,$url);
      }
      $update = $ci->db->update('orders',array('all_steps'=>serialize($all_steps),'step_url'=>$url),"`id`=$oid");
      return $update;
    }
}

function get_order_prev_step($oid,$url){
    $ci = get_instance();
    $data = $ci->db->select('all_steps')->from('orders')->where("`id`=$oid")->get()->row_array();
    if(!empty($data['all_steps'])){
      $all_steps = unserialize($data['all_steps']);
      
      $key= array_search($url,$all_steps);
      if(!empty($key)){
        $k = $key-1;

        // print_r($all_steps[$k]);
        // exit;
        return $all_steps[$k];
      }else{
        //print_r($url); exit;
        return $url;
      }

      //$update = $ci->db->update('orders',array('all_steps'=>serialize($all_steps),'step_url'=>$url),"`id`=$oid");
      //return $update;
    }  
}

// function get_ymd_format($date){
//   if(!empty($date)){
//     $dateArra = explode('/',$date);
//     return $dateArra[2].'-'.$dateArra[0].'-'.$dateArra[1];
//   }
// }

// function get_ym_start($date){
//   if(!empty($date)){
//     $dateArra = explode('/',$date);
//     return $dateArra[1].'-'.$dateArra[0].'-01';
//   }    
// }

// function get_ym_end($date){
//   if(!empty($date)){
//     $dateArra = explode('/',$date);
//     $enddate = $dateArra[1].'-'.$dateArra[0];
//     return date('Y-m-t',strtotime($dateArra[1].'-'.$dateArra[0].'-01'));
//   }    
// }

// function compare_dates($from,$to){
// 	if(strtotime($from)>strtotime($to)){
// 		return false;
// 	}else{
// 		return true;
// 	}
// }

function pass_strength($password){

	// Validate password strength
	$uppercase = preg_match('@[A-Z]@', $password);
	$lowercase = preg_match('@[a-z]@', $password);
	$number    = preg_match('@[0-9]@', $password);
	$specialChars = preg_match('@[^\w]@', $password);

	if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
	    return false;
	}else{
	    return true;
	}
}

/**
* [To encode string]
* @param string $str
*/
if(!function_exists('encoding')){
  function encoding($str){
    $one = serialize($str);
    $two = @gzcompress($one,9);
    $three = addslashes($two);
    $four = base64_encode($three);
    $five = strtr($four, '+/=', '-_.');
    return $five;
  }
}

/**
* [To decode string]
* @param string $str
*/

if (!function_exists('decoding')){
  function decoding($str){
    $one = strtr($str, '-_.', '+/=');
    $two = base64_decode($one);
    $three = stripslashes($two);
    $four = @gzuncompress($three);
    if ($four == '') {
      return "z1"; 
    } else {
      $five = unserialize($four);
      return $five;
    }
  }
}

if (!function_exists('check_branch_detail')){
  function check_branch_detail(){
    $ci = get_instance();
    $vendordata = $ci->session->userdata('vendordata');
    $data =  $ci->db->select('start_time,end_time,services')->from('printery_shop')->where("`user_id`=".$vendordata['user_id']." AND `delete_status`='0' AND `branch`='0' AND (`start_time` is NULL OR `end_time` IS NULL OR `services` IS NULL ) ")->get()->row_array();

    // echo $ci->db->last_query();
    // print_r($data);

    if(empty($data)){
      $return =  true;
    }else{
      if(!empty($data['start_time']) && !empty($data['end_time'])  && !empty($data['services']) ){
          $return =  true;
      }else{
          $return =  false;
      }
    }
    return $return ;
  }
}

function get_prodTotal($order_products){
  $prod_count = array_sum(array_map(function($item) { 
      return $item['price']*$item['qty']; 
  }, $order_products));

  return $prod_count;
}


function get_prod_total($order_products){
  $prod_count = array_sum(array_map(function($item) { 
      return $item['prod_price']*$item['prod_qty']; 
  }, $order_products));

  return $prod_count;
}

// /**
//  * create a encoded id for sequrity pupose 
//  * 
//  * @param string $id
//  * @param string $salt
//  * @return endoded value
//  */
// if (!function_exists('encode_id')) {

//     function encode_id($id, $salt) {
//         $ci = get_instance();
//         return $ci->encryption->encrypt($id);

//     }

// }

function get_print_type_data($name="",$id=""){
  $ci = get_instance();

  if(!empty($name)){
    return $ci->db->select('id,name')->from('print_type')->where("name LIKE '%".$name."%' ")->get()->row_array();
  }elseif(!empty($id)){
    return $ci->db->select('id,name')->from('print_type')->where("id=$id")->get()->row_array();
  }
}

function get_services_data($id="",$name=""){
  $ci = get_instance();

  if(!empty($name)){
    return $ci->db->select('id,name,description')->from('services')->where("name LIKE '%".$name."%'  AND status !='2'")->get()->row_array();
  }elseif(!empty($id)){
    return $ci->db->select('id,name,description')->from('services')->where("id=$id AND status !='2'")->get()->row_array();
  }
}

// /**
//  * decode the id which made by encode_id()
//  * 
//  * @param string $id
//  * @param string $salt
//  * @return decoded value
//  */
// if (!function_exists('decode_id')) {

//     function decode_id($id, $salt) {
//         $ci = get_instance();
//         return $ci->encryption->decrypt($id);
//     }

// }

// if(!function_exists('gmdate_to_mydate')){
//     function gmdate_to_mydate($gmdate,$localtimzone){
//     /* $gmdate must be in YYYY-mm-dd H:i:s format*/
//     date_default_timezone_set($localtimzone);
//     $timezone=date_default_timezone_get();
//     $userTimezone = new DateTimeZone($timezone);
//     $gmtTimezone = new DateTimeZone('GMT');
//     $myDateTime = new DateTime($gmdate, $gmtTimezone);
//     $offset = $userTimezone->getOffset($myDateTime);
//     return date("Y-m-d H:i:s", strtotime($gmdate)+$offset);
//     }
// }

    function myfunction($num)
    {
      $ci = get_instance();
        
        $copy_number = $ci->session->userdata('copy_number');
        $num['vendor_charge'] = round((floatval($num['vendor_charge'])*intval($copy_number)),"2");
        return($num);
    }
    


  function get_shop($id)
	{		
		$ci =& get_instance();
        $ci->db->select('*');           
        $ci->db->where('id',$id);
        $query = $ci->db->get('printery_shop');            
        $reslt = $query->row();			
        return $reslt->shop_name;        	
		
	}
	
	function get_notes($id)
	{		
		$ci =& get_instance();
        $ci->db->select('*');           
        $ci->db->where('id',$id);
        $query = $ci->db->get('notes');            
        $reslt = $query->row();			
        return $reslt->document;        	
		
	}
	
	function proposal_mail_to_vendor($vendors,$oid){
	    
	    $vids = implode(",",array_column($vendors,'id'));

        $ci =& get_instance();
        if(!empty($vendors)){
            
            $vArr = $ci->db->select('ps.id,ps.shop_name,ps.building,u.id as user_id,u.firstname,u.lastname,u.email,u.mobile')
            ->from('printery_shop as ps')
            ->join('users as u','u.id=ps.user_id','left')
            ->where("ps.id IN($vids)")->get()
            ->result_array();
    
        }
        foreach($vArr as $key=>$val){
            
            $subject = "Translation Order request received";
            $data['name'] = $val['firstname'].' '.$val['lastname'];
            $data['shop_name'] = $val['shop_name'];
            $data['link'] = site_url('vendor/trans_order_detail/').encoding($oid);
            $message = $ci->mail_template_view('mail/trans_request_vendor',$data);
            
            $ci->sendGridMail("",$val['email'],$subject,$message);
            //$ci->sendGridMail("","developermactosys@gmail.com",$subject,$message);
            
            
            
            $not_data[$key]['user_id'] = $val['user_id'];
            $not_data[$key]['title'] = 'New Translation order request received';
            $not_data[$key]['redirect'] = site_url('vendor/trans_order_detail').'/'.encoding($oid);
            $not_data[$key]['read_status'] = '0';            
        }
        
        //===vendor===//   
        $ci->generalmodel->insert_notification($not_data);
	}
	
	
 ?>