<?php
require_once('./application/third_party/Misc/Constantspay.php'); 
require_once('./application/third_party/Misc/PaymentHandler.php'); 
require_once('./application/helpers/ModelBindingHelper.php'); 
require_once('./application/libraries/HesabeCrypt.php'); 
//require_once 'Misc/Constants.php';
//require_once 'Misc/PaymentHandler.php';
//require_once 'Helpers/ModelBindingHelper.php';
//require_once 'Libraries/HesabeCrypt.php';

use Models\HesabeCheckoutResponseModel;

/**
 * This Class handles the form request to the checkout controller
 * and receive the response and displays the encrypted and decrypted data.
 *
 * @author Hesabe
 */
class Hesabelib  
{
    public $paymentApiUrl;
    public $secretKey;
    public $ivKey;
    public $accessCode;
    public $hesabeCheckoutResponseModel;
    public $modelBindingHelper;
    public $hesabeCrypt;

    public function __construct()
    {
        $this->paymentApiUrl = Constantspay::PAYMENT_API_URL;
        // Get all three values from Merchant Panel, Profile section
        $this->secretKey = Constantspay::MERCHANT_SECRET_KEY;  // Use Secret key
        $this->ivKey = Constantspay::MERCHANT_IV;              // Use Iv Key
        $this->accessCode = Constantspay::ACCESS_CODE;         // Use Access Code
        $this->hesabeCheckoutResponseModel = new HesabeCheckoutResponseModel();
        $this->modelBindingHelper = new ModelBindingHelper();
        $this->hesabeCrypt = new HesabeCrypt();   // instance of Hesabe Crypt library
    }

    /**
     * This function handles the form request and get the response
     *
     * @return void
     */


    /**
     * Redirect to payment gateway to complete the process
     *
     * @param string $token Encrypted data
     *
     * @return null [<description>]
     */
    public function redirectToPayment($token)
    {
        header("Location: $this->paymentApiUrl/payment?data=$token");
        exit;
    }

    /**
     * Process the response after the transaction is complete
     *
     * @return array De-serialize the decrypted response
     *
     */
    public function getPaymentResponse($responseData)
    {
        //Decrypt the response received in the data query string
        $decryptResponse = $this->hesabeCrypt::decrypt($responseData, $this->secretKey, $this->ivKey);

        //De-serialize the decrypted response
        $decryptResponseData = json_decode($decryptResponse, true);

        //Binding the decrypted response data to the entity model
        $decryptedResponse = $this->modelBindingHelper->getPaymentResponseData($decryptResponseData);

        //return decrypted data
        $myarr['message'] = $decryptedResponse->message ;
        $myarr['status'] = $decryptedResponse->status ;
        $myarr['response'] = $decryptedResponse->response ;
        return $myarr;
        //return $decryptedResponse;
    }

    /**
     * Process the response after the form data has been requested.
     *
     * @return array De-serialize the decrypted response
     *
     */
    public function getCheckoutResponse($response)
    {
        // Decrypt the response from the checkout API
        $decryptResponse = $this->hesabeCrypt::decrypt($response, $this->secretKey, $this->ivKey);

        if (!$decryptResponse) {
            $decryptResponse = $response;
        }

        // De-serialize the JSON string into an object
        $decryptResponseData = json_decode($decryptResponse, true);

        //Binding the decrypted response data to the entity model
        $decryptedResponse = $this->modelBindingHelper->getCheckoutResponseData($decryptResponseData);

        //return encrypted and decrypted data
        return [$response , $decryptedResponse];
    }
}
